Named object: "NoName1"
Tri-mesh, Vertices: 20    Faces: 20
Vertex list:
Vertex 0:  X:0     Y:-5.775     Z:27.72
Vertex 1:  X:-16.17     Y:3.465     Z:-13.86
Vertex 2:  X:0     Y:5.775     Z:-20.79
Vertex 3:  X:16.17     Y:3.465     Z:-13.86
Vertex 4:  X:-16.17     Y:-5.775     Z:-13.86
Vertex 5:  X:16.17     Y:-5.775     Z:-13.86
Vertex 6:  X:9.24     Y:3.465     Z:-27.72
Vertex 7:  X:-9.24     Y:3.465     Z:-27.72
Vertex 8:  X:9.24     Y:-5.775     Z:-27.72
Vertex 9:  X:-9.24     Y:-5.775     Z:-27.72
Vertex 10:  X:-2.31     Y:-3.003     Z:16.17
Vertex 11:  X:-11.55     Y:3.465     Z:-11.55
Vertex 12:  X:-2.31     Y:4.851     Z:-16.17
Vertex 13:  X:2.31     Y:-3.003     Z:16.17
Vertex 14:  X:2.31     Y:4.851     Z:-16.17
Vertex 15:  X:11.55     Y:3.465     Z:-11.55
Vertex 16:  X:-2.31     Y:1.155     Z:-27.7431
Vertex 17:  X:2.31     Y:1.155     Z:-27.7431
Vertex 18:  X:2.31     Y:-3.465     Z:-27.7431
Vertex 19:  X:-2.31     Y:-3.465     Z:-27.7431
Face list:
Face 0:    A:0 B:2 C:1  AB:1 BC:1 CA:1
Material: "r224g160b96a0"
Face 1:    A:3 B:2 C:0  AB:1 BC:1 CA:1
Material: "r224g160b32a0"
Face 2:    A:0 B:1 C:4  AB:1 BC:1 CA:1
Material: "r96g32b32a0"
Face 3:    A:3 B:0 C:5  AB:1 BC:1 CA:1
Material: "r96g32b32a0"
Face 4:    A:2 B:3 C:6  AB:1 BC:1 CA:1
Material: "r160g160b32a0"
Face 5:    A:2 B:7 C:1  AB:1 BC:1 CA:1
Material: "r160g160b32a0"
Face 6:    A:2 B:6 C:7  AB:1 BC:1 CA:1
Material: "r32g96b32a0"
Face 7:    A:6 B:3 C:8  AB:1 BC:1 CA:1
Material: "r160g96b32a0"
Face 8:    A:3 B:5 C:8  AB:1 BC:1 CA:1
Material: "r160g96b32a0"
Face 9:    A:7 B:4 C:1  AB:1 BC:1 CA:1
Material: "r160g96b32a0"
Face 10:    A:7 B:9 C:4  AB:1 BC:1 CA:1
Material: "r160g96b32a0"
Face 11:    A:9 B:7 C:6  AB:1 BC:1 CA:1
Material: "r96g96b96a0"
Face 12:    A:8 B:9 C:6  AB:1 BC:1 CA:1
Material: "r96g96b96a0"
Face 13:    A:18 B:16 C:17  AB:1 BC:1 CA:1
Material: "r224g32b32a0"
Face 14:    A:16 B:18 C:19  AB:1 BC:1 CA:1
Material: "r224g32b32a0"
Face 15:    A:9 B:0 C:4  AB:1 BC:1 CA:1
Material: "r96g96b96a0"
Face 16:    A:8 B:0 C:9  AB:1 BC:1 CA:1
Material: "r32g96b96a0"
Face 17:    A:8 B:5 C:0  AB:1 BC:1 CA:1
Material: "r96g96b96a0"
Face 18:    A:10 B:12 C:11  AB:1 BC:1 CA:1
Material: "r96g32b32a0"
Face 19:    A:13 B:15 C:14  AB:1 BC:1 CA:1
Material: "r96g32b32a0"
