Named object: "NoName1"
Tri-mesh, Vertices: 10    Faces: 10
Vertex list:
Vertex 0:  X:0     Y:2.5     Z:-7.5
Vertex 1:  X:10     Y:0     Z:-7.5
Vertex 2:  X:0     Y:-2.5     Z:-7.5
Vertex 3:  X:-10     Y:0     Z:-7.5
Vertex 4:  X:-5     Y:0     Z:7.5
Vertex 5:  X:5     Y:0     Z:7.5
Vertex 6:  X:-2.5     Y:1.5     Z:-7.51
Vertex 7:  X:2.5     Y:1.5     Z:-7.51
Vertex 8:  X:-2.5     Y:-1.5     Z:-7.51
Vertex 9:  X:2.5     Y:-1.5     Z:-7.51
Face list:
Face 0:    A:0 B:2 C:3  AB:1 BC:1 CA:1
Material: "r0g0b192a0"
Face 1:    A:0 B:1 C:2  AB:1 BC:1 CA:1
Material: "r0g0b192a0"
Face 2:    A:6 B:9 C:8  AB:1 BC:1 CA:1
Material: "r224g32b32a0"
Face 3:    A:6 B:7 C:9  AB:1 BC:1 CA:1
Material: "r224g32b32a0"
Face 4:    A:0 B:5 C:1  AB:1 BC:1 CA:1
Material: "r32g96b96a0"
Face 5:    A:0 B:4 C:5  AB:1 BC:1 CA:1
Material: "r48g112b112a0"
Face 6:    A:0 B:3 C:4  AB:1 BC:1 CA:1
Material: "r32g96b96a0"
Face 7:    A:1 B:5 C:2  AB:1 BC:1 CA:1
Material: "r32g96b96a0"
Face 8:    A:5 B:4 C:2  AB:1 BC:1 CA:1
Material: "r48g112b112a0"
Face 9:    A:4 B:3 C:2  AB:1 BC:1 CA:1
Material: "r32g96b96a0"
