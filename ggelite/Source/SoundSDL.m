/*
 *  SoundSDL.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: August 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <unistd.h>
#import <stdlib.h>
#import <sys/types.h>
#import <sys/stat.h>
#import <fcntl.h>
#import <sys/ioctl.h>
//#import <sys/soundcard.h>
#import <math.h>

#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSTask.h>
#import <Foundation/NSFileHandle.h>
#import <Foundation/NSException.h>
#import <Foundation/NSValue.h>

#import "SoundSDL.h"
#import "Metaobj.h"
#import "Resource.h"
#import "World.h"

SoundManager *soundManager;

#ifdef HAVE_MIXER
#import "SDL.h"
#import "SDL_mixer.h"

#define NBR_RESERVED_CHANNELS 3

@implementation SoundManager
- (void) dealloc
{
  NSEnumerator *en;
  NSString *val;
  NSParameterAssert(soundManager == self);
  [[NSNotificationCenter defaultCenter] removeObserver: self];
  en = [samples keyEnumerator];
  while((val = [en nextObject]) != nil )
    {
      NSValue *s;
      NSDebugMLLog(@"Sound", @"Destroying sample %@", val);
      s = [samples objectForKey: val];

      NSParameterAssert(s != nil);
      
      Mix_FreeChunk([s pointerValue]);
    }
  RELEASE(samples);
  soundManager = nil;
  Mix_CloseAudio();
  [super dealloc];
}

- init
{
  if( soundManager )
    {
      [super dealloc];
      return RETAIN(soundManager);
    }

  [super init];

  if( SDL_InitSubSystem(SDL_INIT_AUDIO) < 0 )
    {
      NSWarnMLog(@"Could not initialize audio");
      [super dealloc];
      return nil;
    }

  if( Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 1, 1024) < 0)
    {
      NSWarnMLog(@"Mix_OpenAudio failed");
      [super dealloc];
      return nil;
    }

  NSDebugMLLog(@"Sound", @"Initialization is a success");
  samples = [NSMutableDictionary new];

  Mix_ReserveChannels(NBR_RESERVED_CHANNELS);
  [[NSNotificationCenter defaultCenter]
    addObserver: self
    selector: @selector(loadingAGame:)
    name: GGNotificationAfterLoadingGame
    object: nil];
  
  soundManager = self;

  [self playMp3: @"tutu.mp3"];

  return self;
}

- (void) loadingAGame: (NSNotification *)aNotification
{
  Mix_HaltChannel(-1);
}

- (void) addSample: (Mix_Chunk *)chunk
	    forKey: key
{
  NSValue *val;

  val = [NSValue valueWithPointer: chunk];

  [samples setObject: val
	   forKey: key];
}

- (Mix_Chunk *) chunkForName: (NSString *) str
{
  NSValue *val;

  val = [samples objectForKey: str];
  
  if( val == nil )
    {
      Mix_Chunk *chunk;
      NSDebugMLLog(@"Sound", @"no sample %@, caching a new one", str);

      chunk = Mix_LoadWAV([[[Resource resourceManager] pathForSound: str] cString]);
      if( chunk == NULL )
	{
	  NSWarnMLog(@"Could not load %@", str);
	  return NULL;
	}

      Mix_VolumeChunk(chunk, MIX_MAX_VOLUME / 4);

      [self addSample: chunk
	    forKey: str];
      return chunk;
    }
  else
    {
      NSDebugMLLog(@"Sound", @"returning cached value");
      return [val pointerValue];
    }
}

- (void) playMp3: (NSString *)str
{
  Mix_Music *music;
  music = Mix_LoadMUS([str cString]);

  if( music == NULL )
    {
      NSWarnMLog(@"could not load music file %@", str);
      return;
    }

  if( Mix_PlayMusic(music, 0) < 0 )
    {
      NSWarnMLog(@"error while playing %@", str);
    }

  //should release music...
}

- (void) playFile: (NSString *)str
	     loop: (int) loop
	     from: (void *)val
{
  int i;
  int index;
  Mix_Chunk *chunk;
  if( val == NULL)
    index = -1;
  else
    {
      for(i = 0; i < NBR_RESERVED_CHANNELS; ++i)
	{
	  if( reserved[i] == NULL || !Mix_Playing(i) )
	    {
	      reserved[i] = val;
	      index = i;
	      break;
	    }
	}
      if( i == NBR_RESERVED_CHANNELS )
	{
	  NSWarnMLog(@"Too many channels allocated");
	  return;
	}
    }

  chunk = [self chunkForName: str];
  if( chunk == NULL ) return;
  
  NSDebugMLLog(@"Sound", @"Playing chunk %@", str);
  if(  Mix_PlayChannel(index, chunk, loop) < 0 )
    {
      NSWarnMLog(@"error while playing %@", str);
    }
}

- (void) stopPlayingFrom: (void *)val
{
  int i;
  NSParameterAssert(val);
  for(i = 0 ; i < NBR_RESERVED_CHANNELS; ++i)
    if( val == reserved[i] )
      {
	reserved[i] = NULL;
	Mix_HaltChannel(i);
      }
}

- (void) playFile: (NSString *)str
  //here we should cache the sample in a dictionary
  //clear everything when the object is deallocated
{
  [self playFile: str
	loop: 0
	from: NULL];
}

@end

#else
@implementation SoundManager
- init
{	
  RELEASE(self);
  return nil;
}

- (void) playFile: (NSString *)str
{
    
}

- (void) playMp3: (NSString *)str
{
    
}

- (void) stopPlayingFrom: (void *)val
{
    
}

- (void) playFile: (NSString *)str
             loop: (int) loop
             from: (void *)val
{
    
}

@end
#endif
