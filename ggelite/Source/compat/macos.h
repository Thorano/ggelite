//
//  macos.h
//  elite
//
//  Created by Frédéric De Jaeger on Sat May 15 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef MACOSX
BOOL GSDebugSet(NSString *mode);
NSString* GSDebugMethodMsg(id obj, SEL sel, const char *file, int line, NSString *fmt);
NSString* GSDebugFunctionMsg(const char *func, const char *file, int line, NSString *fmt);

static inline BOOL sel_eq(SEL s1, SEL s2){
    return s1 == s2;
}


#ifdef DEBUG
void GGLogInFile(int importance, NSString* domain, const char *func, const char *file, int line, NSString* fmt, ...);

#define NSDebugLLog(level, format, args...) GGLogInFile(1, level, __func__, __FILE__, __LINE__, format , ##args) 
#define NSDebugFLLog(level, format, args...) GGLogInFile(1, level, __func__, __FILE__, __LINE__, format , ##args) 
#define NSDebugMLLog(level, format, args...) GGLogInFile(1, level, __func__, __FILE__, __LINE__, format , ##args) 
#define NSDebugILLog(domain, format, args...) GGLogInFile(3, domain, __func__, __FILE__, __LINE__, format , ##args) 

#define NSWarnLog(format, args...) GGLogInFile(2, @"dflt", __func__, __FILE__, __LINE__, format , ##args) 
#define NSWarnFLog(format, args...) GGLogInFile(2, @"dflt", __func__, __FILE__, __LINE__, format , ##args) 
#define NSWarnMLog(format, args...) GGLogInFile(2, @"dflt", __func__, __FILE__, __LINE__, format , ##args) 

#elif 0
/**
<p>NSDebugLLog() is the basic debug logging macro used to display
 log messages using NSLog(), if debug logging was enabled at compile
 time and the appropriate logging level was set at runtime.
 </p>
 <p>Debug logging which can be enabled/disabled by defining GSDIAGNOSE
 when compiling and also setting values in the mutable set which
 is set up by NSProcessInfo. GSDIAGNOSE is defined automatically
 unless diagnose=no is specified in the make arguments.
 </p>
 <p>NSProcess initialises a set of strings that are the names of active
 debug levels using the '--GNU-Debug=...' command line argument.
 Each command-line argument of that form is removed from NSProcessInfos
 list of arguments and the variable part (...) is added to the set.
 This means that as far as the program proper is concerned, it is
 running with the same arguments as if debugging had not been enabled.
 </p>
 <p>For instance, to debug the NSBundle class, run your program with 
 '--GNU-Debug=NSBundle'
 You can of course supply multiple '--GNU-Debug=...' arguments to
 output debug information on more than one thing.
 </p>
 <p>NSUserDefaults also adds debug levels from the array given by the
 GNU-Debug key ... but these values will not take effect until the
 +standardUserDefaults method is called ... so they are useless for
 debugging NSUserDefaults itsself or for debugging any code executed
 before the defaults system is used.
 </p>
 <p>To embed debug logging in your code you use the NSDebugLLog() or
 NSDebugLog() macro.  NSDebugLog() is just NSDebugLLog() with the debug
 level set to 'dflt'.  So, to activate debug statements that use
 NSDebugLog(), you supply the '--GNU-Debug=dflt' argument to your program.
 </p>
 <p>You can also change the active debug levels under your programs control -
 NSProcessInfo has a [-debugSet] method that returns the mutable set that
 contains the active debug levels - your program can modify this set.
 </p>
 <p>Two debug levels have a special effect - 'dflt' is the level used for
 debug logs statements where no debug level is specified, and 'NoWarn'
 is used to *disable* warning messages.
 </p>
 <p>As a convenience, there are four more logging macros you can use -
 NSDebugFLog(), NSDebugFLLog(), NSDebugMLog() and NSDebugMLLog().
 These are the same as the other macros, but are specifically for use in
 either functions or methods and prepend information about the file, line
 and either function or class/method in which the message was generated.
 </p>
 */
#define NSDebugLLog(level, format, args...) \
do { if (GSDebugSet(level) == YES) \
    NSLog(format , ## args); } while (0)

/**
* This macro is like NSDebugLLog() but includes the name and location
 * of the function in which the macro is used as part of the log output.
 */
#define NSDebugFLLog(level, format, args...) \
do { if (GSDebugSet(level) == YES) { \
    NSString *fmt = GSDebugFunctionMsg( \
                                        __PRETTY_FUNCTION__, __FILE__, __LINE__, format); \
                                            NSLog(fmt , ## args); }} while (0)

/**
* This macro is like NSDebugLLog() but includes the name and location
 * of the <em>method</em> in which the macro is used as part of the log output.
 */
#define NSDebugMLLog(level, format, args...) \
do { if (GSDebugSet(level) == YES) { \
    NSString *fmt = GSDebugMethodMsg( \
                                      self, _cmd, __FILE__, __LINE__, format); \
                                          NSLog(fmt , ## args); }} while (0)


/**
<p>NSWarnLog() is the basic debug logging macro used to display
 warning messages using NSLog(), if warn logging was not disabled at compile
 time and the disabling logging level was not set at runtime.
 </p>
 <p>Warning messages which can be enabled/disabled by defining GSWARN
 when compiling.
 </p>
 <p>You can also disable these messages at runtime by supplying a
 '--GNU-Debug=NoWarn' argument to the program, or by adding 'NoWarn'
 to the user default array named 'GNU-Debug'.
 </p>
 <p>These logging macros are intended to be used when the software detects
 something that it not necessarily fatal or illegal, but looks like it
 might be a programming error.  eg. attempting to remove 'nil' from an
 NSArray, which the Spec/documentation does not prohibit, but which a
 well written progam should not be attempting (since an NSArray object
                                               cannot contain a 'nil').
 </p>
 <p>NB. The 'warn=yes' option is understood by the GNUstep make package
 to mean that GSWARN should be defined, and the 'warn=no' means that
 GSWARN should be undefined.  Default is to define it.
 </p>
 <p>To embed debug logging in your code you use the NSWarnLog() macro.
 </p>
 <p>As a convenience, there are two more logging macros you can use -
 NSWarnLog(), and NSWarnMLog().
 These are specifically for use in either functions or methods and
 prepend information about the file, line and either function or
 class/method in which the message was generated.
 </p>
 */

#define NSWarnLog(format, args...) \
do { if (GSDebugSet(@"NoWarn") == NO) { \
    NSLog(format , ## args); }} while (0)

/**
* This macro is like NSWarnLog() but includes the name and location of the
 * <em>function</em> in which the macro is used as part of the log output.
 */
#define NSWarnFLog(format, args...) \
do { if (GSDebugSet(@"NoWarn") == NO) { \
    NSString *fmt = GSDebugFunctionMsg( \
                                        __PRETTY_FUNCTION__, __FILE__, __LINE__, format); \
                                            NSLog(fmt , ## args); }} while (0)

/**
* This macro is like NSWarnLog() but includes the name and location of the
 * <em>method</em> in which the macro is used as part of the log output.
 */
#define NSWarnMLog(format, args...) \
do { if (GSDebugSet(@"NoWarn") == NO) { \
    NSString *fmt = GSDebugMethodMsg( \
                                      self, _cmd, __FILE__, __LINE__, format); \
                                          NSLog(fmt , ## args); }} while (0)



#else
#pragma mark 
#define NSDebugLLog(level, format, args...)
#define NSDebugFLLog(level, format, args...)
#define NSDebugMLLog(level, format, args...)
#define NSDebugILLog(domain, format, args...)

#define NSWarnLog(format, args...)
#define NSWarnFLog(format, args...)
#define NSWarnMLog(format, args...)

#endif

/**
* This macro is a shorthand for NSDebugLLog() using then default debug
 * level ... 'dflt'
 */
#define NSDebugLog(format, args...) NSDebugLLog(@"dflt", format , ##args)

/**
* This macro is a shorthand for NSDebugFLLog() using then default debug
 * level ... 'dflt'
 */
#define NSDebugFLog(format, args...) NSDebugFLLog(@"dflt", format , ##args)

/**
* This macro is a shorthand for NSDebugMLLog() using then default debug
 * level ... 'dflt'
 */
#define NSDebugMLog(format, args...) NSDebugMLLog(@"dflt", format , ##args)


@interface NSObject (GGGNUstep)
- (void) subclassResponsibility: (SEL) s;
@end

@interface NSString (GGGNUstep)
+ stringWithFormat: (NSString*) string
         arguments: (va_list)argList;
@end

#endif
