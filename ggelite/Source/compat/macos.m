//
//  macos.m
//  elite
//
//  Created by Frédéric De Jaeger on Sat May 15 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "macos.h"

static NSMutableSet *sDebugSet;
static BOOL sAll;

static void _initSet(void)
{
    NSProcessInfo* info = [NSProcessInfo processInfo];
    NSArray* args = [info arguments];
    sDebugSet = [NSMutableSet new];
    
    int ii;
    for ( ii = 0; ii < [args count]; ii++ ){
        NSString *arg = [args objectAtIndex: ii];
        NSRange aRange = [arg rangeOfString: @"="];
        if ( NSNotFound !=  aRange.location ){
            NSString *prefix = [arg substringToIndex: aRange.location];
            if ( [prefix isEqual: @"--GNU-Debug"] ){
                NSString *suffix = [arg substringFromIndex: aRange.location+1];
                if ( [suffix isEqual: @"all"] ){
                    NSLog(@"all logging enabled");
                    sAll = YES;
                }
                else{
                    NSLog(@"adding debug mode %@", suffix);
                    [sDebugSet addObject: suffix];
                }
            }
        }
    }
}


BOOL GSDebugSet(NSString *mode)
{
    if ( nil == sDebugSet ) _initSet();

    if ( sAll ) return YES;
    
    NSCParameterAssert(sDebugSet);
    return [sDebugSet containsObject: mode];
}

// this function comes from GNUstep
NSString*
GSDebugMethodMsg(id obj, SEL sel, const char *file, int line, NSString *fmt)
{
    NSString      *message;
    Class         cls = (Class)obj;
    char          c = '+';
    /*
    if ([obj isInstance] == YES)
    {
        c = '-';
        cls = [obj class];
    }
     */
    c = '-';
    cls = [obj class];
    NSString *fileName = [NSString stringWithUTF8String: file];
    fileName = [fileName lastPathComponent];
    message = [NSString stringWithFormat: @"File %@: %d. In [%@ %c%@]:\n%@",
        fileName, line, NSStringFromClass(cls), c, NSStringFromSelector(sel), fmt];
    return message;
}

NSString*
GSDebugFunctionMsg(const char *func, const char *file, int line, NSString *fmt)
{
    NSString *message;
    
    message = [NSString stringWithFormat: @"File %s: %d. In %s %@",
	file, line, func, fmt];
    return message;
}

void GGLogInFile(int level, NSString* domain, const char *func, const char *file, int line, NSString* fmt, ...)
{
    static NSFileHandle* output;
    static BOOL first = YES;
    if(first){
        first = NO;
        NSProcessInfo* info = [NSProcessInfo processInfo];
        NSArray* args = [info arguments];

        if([args containsObject:@"--logInFile"]){
            NSString* path = NSHomeDirectory();
            path = [path stringByAppendingPathComponent:@"Desktop/ggelite.decalog"];
            int fd = open([path fileSystemRepresentation], O_WRONLY | O_APPEND | O_CREAT 
                          | O_TRUNC, 0777);
            if(fd >= 0){
                NSLog(@"will log in %@", path);
                output = [[NSFileHandle alloc] initWithFileDescriptor:fd
                                                       closeOnDealloc:YES];
            }
            else{
                perror("error while opening log file:");
            }
        }
    }
    
    va_list ap;
    
    va_start (ap, fmt);
    if(!output){
        if(/*1 == level && */GSDebugSet(domain)){
            NSLogv(fmt,ap);            
        }
        else if(2 == level && !GSDebugSet(@"NoWarn")){
            NSLogv(fmt,ap);            
        }
    }
    else{
//        fmt = GSDebugFunctionMsg(func, file, line, fmt);
        NSString* levelAsString = nil;
        switch(level){
            case 1: levelAsString = @"INFO"; break;
            case 2: levelAsString = @"WARNING"; break;
            case 3: levelAsString = @"IMPORTANT"; break;
            case 4: levelAsString = @"ERROR"; break;
            default:break;
        }
        NSString* message = [NSString stringWithFormat:fmt
                                             arguments:ap];
        NSCalendarDate* now = [NSCalendarDate date];
        NSData* data = [message dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString* header = [NSString stringWithFormat:@"TIME: %@\nTHREAD: MAIN\nTYPE: %@\nLEVEL: %@\nLINE: %d\nFILE: %s\nFUNCTION: %s\nLENGTH: %d\n\n%@\n\n",
            [now descriptionWithCalendarFormat:@"%H:%M:%S.%F"],
            domain, levelAsString, line, file, func, [data length]+1, message];
        NSData* headerData = [header dataUsingEncoding:NSUTF8StringEncoding];
        [output writeData:headerData];
    }
    va_end (ap);
}


@implementation NSObject (GGGNUstep)
- (void) subclassResponsibility: (SEL) s
{
    [NSException raise: NSInternalInconsistencyException
                format: @"selector %@ is not recongnized by %@", NSStringFromSelector(s), [self class]];
}
@end

@implementation NSString (GGGNUstep)
+ stringWithFormat: (NSString*) string
         arguments: (va_list)argList
{
    return [[[self alloc] initWithFormat: string
                               arguments: argList] autorelease];
}
@end
