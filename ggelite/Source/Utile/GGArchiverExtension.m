//
//  GGArchiverExtension.m
//  elite
//
//  Created by Frédéric De Jaeger on 20/02/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "GGArchiverExtension.h"


@implementation NSCoder (GGEliteExtension)
- (void) gge_encodeBytes:(void*)ptr length:(int)size objCType:(const char *)type forKey:(NSString*)key
{
#if 0
    static NSMutableSet* seenTypes;
    if(nil == seenTypes){
        seenTypes = [[NSMutableSet alloc] init];
    }
    
    NSString* typeAsObj = [[NSString alloc] initWithUTF8String:type];
    if(![seenTypes containsObject:typeAsObj]){
        [seenTypes addObject:typeAsObj];
        NSLog(@"type = %@ (size=%d bytes)", typeAsObj, size);
    }
    [typeAsObj release];
#endif
    
    if(strcmp(type,"@") == 0){
        NSAssert(sizeof(id) == size, @"unexpected size");
        id *obj = ptr;
//        NSLog(@"encoding object: %@", *obj);
        [self encodeObject:*obj forKey:key];
    }
    else{
        NSAssert(strchr(type,'@') == NULL, @"no object hidden in compound allowed");
        NSData* data = [[NSData alloc] initWithBytes:ptr length:size];
        [self encodeObject:data forKey:key];
        [data release];
    }
}

- (void) gge_decodeBytesToPtr:(void*)ptr objCType:(const char *)type forKey:(NSString*)key
{
    id obj = [self decodeObjectForKey:key];
    if(strcmp(type, "@") == 0){
        id *objRef = ptr;
        *objRef = [obj retain];  // has to be retained for compatibility with NSCoder
    }
    else{
        NSData* data = obj;
        if(data){
            NSAssert([data isKindOfClass:[NSData class]], @"wrong decoding");
            memcpy(ptr,[data bytes],[data length]);
        }
        else {
            NSLog(@"no value for key \"%@\"", key);
        }
    }    
}

@end