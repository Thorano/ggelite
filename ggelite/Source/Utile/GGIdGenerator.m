//
//  GGIdGenerator.m
//  elite
//
//  Created by Frederic De Jaeger on 09/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGIdGenerator.h"


@implementation NSMutableIndexSet (GGIdGenerator)
- (int) nextAvailableIdentifier
{
    int ret;
    if([self count] == 0){
        ret = 0;        
    }
    else{
        unsigned max = [self lastIndex];
        NSMutableIndexSet* all = [[NSMutableIndexSet alloc] initWithIndexesInRange:NSMakeRange(0,max+1)];
        [all removeIndexes:self];
        if([all count] == 0){
            ret = max+1;
        }
        else{
            ret = [all firstIndex];
        }
        [all release];
    }
    NSAssert(ret != NSNotFound, @"bad value computed");
    [self addIndex:ret];
    
    return ret;
}

- (void) releaseIdentifier:(unsigned)identifier
{
    NSAssert([self containsIndex:identifier], @"unknonwn ID");
    [self removeIndex:identifier];
}

@end
