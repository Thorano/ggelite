//
//  GGArchiverExtension.h
//  elite
//
//  Created by Frédéric De Jaeger on 20/02/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#ifndef __GGArchiverExtension_h
#define __GGArchiverExtension_h 1


#import <Foundation/Foundation.h>

#import "GG3D.h"
@interface NSCoder (GGEliteExtension)
- (void) gge_encodeBytes:(void*)ptr length:(int)size objCType:(const char *)type forKey:(NSString*)key;
- (void) gge_decodeBytesToPtr:(void*)ptr objCType:(const char *)type forKey:(NSString*)key;
@end


#define GGEncode(var) [encoder gge_encodeBytes:&var length:sizeof(var) \
                                        objCType:@encode(typeof(var))\
                                                  forKey: @#var]
// by compatibility with the previous behavior of NSArchiver, when the type is anobject 
// it is retained.

#define GGDecode(var) [decoder gge_decodeBytesToPtr: &var\
                                              objCType:@encode(typeof(var))\
                                                  forKey: @#var]

#define GGEncodeObject(var) [encoder encodeObject:var\
                                           forKey:@#var]
#define GGDecodeObject(var) var = [decoder decodeObjectForKey:@#var]
#define GGDecodeRetainedObject(var) var = [[decoder decodeObjectForKey:@#var] retain]

#define GGEncodeBool(var) [encoder encodeBool:var\
                                       forKey:@#var]
#define GGDecodeBool(var) var = [decoder decodeBoolForKey:@#var]

#define GGEncodeDouble(var) [encoder encodeDouble:var\
                                           forKey:@#var]
#define GGDecodeDouble(var) var = [decoder decodeDoubleForKey:@#var]

#endif