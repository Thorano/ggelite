//
//  GGIdGenerator.h
//  elite
//
//  Created by Frederic De Jaeger on 09/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSMutableIndexSet (GGIdGenerator)
- (int) nextAvailableIdentifier;
- (void) releaseIdentifier:(unsigned)identifier;
@end
