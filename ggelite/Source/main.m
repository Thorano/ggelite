/*
 *  main.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef USE_SDL
#import "GGLoopSDLExtension.h"
#import <SDL/SDL.h>
#else
#import <AppKit/AppKit.h>
#endif
#import <stdio.h>
#import <stdlib.h>
#import <locale.h>

#import <Foundation/Foundation.h>
#import <Foundation/NSDebug.h>
#import "GGInput.h"
#import "GGSky.h"
#import "GGLoop.h"
#import "Metaobj.h"
#import "Resource.h"


int main( int argc,  char *argv[] )
{
    int i;
    NSAutoreleasePool *pool, *pool2;    
    wizard = NO;
    START;
    
    setlocale(LC_NUMERIC, "C");
    //  NSZombieEnabled = YES;
    
#ifdef DEBUG
    //  GSDebugAllocationActive(YES);
#endif
    pool = [NSAutoreleasePool new];
    
//    [Resource initResourceManager];
    
    for( i = 0; i < argc; ++i )
    {
        if( strcmp(argv[i], "--post-install") == 0 )
        {
            [GGSky postIntall];
            return 0;
        }
        else if( strcmp(argv[i], "-D") == 0 )
        {
            wizard = YES;
            NSLog(@"Starting in Wizard mode");
        }
    }
    
    //  setMode(CheckMarked | LogFree);
    
#ifdef USE_SDL
    GGLoop *ggLoop = [GGLoopSDL new];
#endif
    
    pool2 = [NSAutoreleasePool new];
    
    
#ifdef USE_SDL
    [ggLoop runLoop];
#else
    NSApplicationMain(argc, (const char **)argv);
#endif
    
    // this must be called while we still have a GL context.
    [Resource destroyResourceManager];
    
    RELEASE(pool2);
#ifdef USE_SDL
    RELEASE(ggLoop);
#endif
    
    RELEASE(pool);
    
    
    END;
#ifdef USE_SDL
    quit_ggelite(0);
#endif
    return 0;
}
