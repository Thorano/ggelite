/*
 *  InfoView.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSNotification.h>

#import "GGRepere.h"
#import "GGLoop.h"
#import "Metaobj.h"
#import "Tableau.h"
#import "Sched.h"
#import "World.h"
#import "GGWidget.h"
#import "GGShip.h"
#import "utile.h"
#import "GGInput.h"
#import "Item.h"
#import "Commandant.h"
#import "GGFont.h"
#import "ShipWin.h"
#import "GGConcreteShip.h"
#import "InfoView.h"
#import "Console.h"
#import "GGMenu.h"
#import "GGScrollView.h"
#import "Goodies.h"
#import "ContractView.h"
#import "TopView.h"

static const double foc = 60;


@interface ProtoInfo (Private)
- (void) setItemList: (NSArray *)goodies;
@end

@interface NSArray (GGExt)
- (NSArray *) ggmap: (NSObject *)obj
	     action: (SEL) s;
@end

@implementation NSArray (GGExt)
- (NSArray *) ggmap: (NSObject *)obj
	     action: (SEL) s
{
  int n = [self count];
  if( n > 0 )
    {
      id objs[n];
      int i;

      [self getObjects: objs];
      for(i = 0; i < n; ++i)
	objs[i] = [obj performSelector: s 
		       withObject:objs[i]];
      
      return [NSArray arrayWithObjects: objs
		      count: n];
    }
  else
    return [NSArray array];
}
@end

@interface InfoView (Private)
- (void) setInfoView: (Class) c;
@end


@implementation InfoView
- (void) setInfoView: (Class) c
{
  ProtoInfo *i;
  NSSize size = [self frame].size;
  i = [[c alloc] initWithSize: size];
  [info removeFromSuperview];
  ASSIGN(info, i);
  [self addSubview: i];
  RELEASE(i);
  [info hasMoveTo: self];
}


- (id) initWithFrame: (NSRect)frameRect
{
  NSSize size;
  FricInfo *fi;
  GGShip *ship;
  ShipView *s;
  //  int n;
  [super initWithFrame: frameRect];
  [self enableTextureBackground];

  size = frameRect.size;
  //  [self enableTextureBackground];
  

  [self setAutoresizingMask: GGNSViewWidthSizable | GGNSViewHeightSizable];
  //  [self setState: GUI_Info];



  s = [[ShipView alloc] initWithFrame: NSMakeRect(size.width - size.height/2-10,
						  size.height -
						  size.height/2-10,
						  size.height/2, 
						  size.height/2)];
  ship = [[GGShip alloc] initWithDictionary: [theCommandant property]];
  [s setShip: ship];
  RELEASE(ship);
  [self addSubview: s];
  shipView = s;
  RELEASE(s);

  fi = [[FricInfo alloc] initWithFrame: NSMakeRect(10, 0, 300, 100)];
  [self addSubview: fi];
  [fi updateFric];
  RELEASE(fi);


  [self setInfoView: [EquipmentInfoView class]];

  [[NSNotificationCenter defaultCenter]
    addObserver: self
    selector: @selector(cargoChanged:)
    name: GGNotificationShipCargoChange
    object: [theWorld commandant]];


  return self;
}

- (void) setSmallShipView: (BOOL) val
{
  int dim;
  NSSize size = [self frame].size;
  NSParameterAssert(shipView);

  if(val)
    dim = size.height / 5;
  else
    dim = size.height / 2;

  [shipView setFrame: NSMakeRect(size.width - dim - 10,
				 size.height - dim - 10,
				 dim, dim)];
}

- (void) cargoChanged: (NSNotification *) aNotification
{
  [info updateView];
}


- (void) dealloc
{
  RELEASE(info);
  [super dealloc];
}

- (void) openIn: (GGNSWindow *)w
{
  [super openIn: w];
  [_window makeFirstResponder: self];
}

- (void) setNextInfoView: (int) new
{
  new = new % 2;
  switch(new)
    {
    case 0:
      [self setInfoView: [EquipmentInfoView class]];
      break;
    case 1:
      [self setInfoView: [GoodiesInfoView class]];
      break;
    }
}

- (void) openInfoCallback: (GGBouton *)button
{
  [self setNextInfoView: [button tag]];
}


@end

@implementation ProtoInfo
- initWithSize: (NSSize) size
{
  return [self 	initWithFrame: 
		  NSMakeRect(10, 20, size.width/2,(int)(3*size.height/5))];
}

- (void) hasMoveTo: (InfoView *)top;
{
  [top setSmallShipView: NO];
}

- (void) updateView
{}

- (void) setItemList: (NSArray *)goodies
{
  int n;
  n = [goodies count];

  if( scroll )
    {
      [scroll removeFromSuperview];
      scroll = nil;
    }

  if(n > 0)
    {
      NSRect frame;
      GGMenu *menu;
      int i;
      id stocks[n];

      [goodies getObjects: stocks];
      frame = [self frame];
      scroll = [[GGScrollView alloc] 
		 initWithFrame: NSMakeRect(20, 20, 0.5*frame.size.width,
					   frame.size.height - 25)];
      [self addSubview: scroll];

      menu  = [GGMenu menu];
      
      for( i = 0; i < n; ++i)
      {
	if( [stocks[i] isKindOfClass: [ItemStock class]] )
	  {
	    ItemStock *is = stocks[i];
	    GGString *item;
	    item = [[GGString alloc] initWithFrame: 
				       NSMakeRect(10, 0, 10, 10)];
	    [item setString: 
		    [NSString stringWithFormat: @"%-20@    %d", 
			      is->item->name,
			      is->cargo]
		  withFont: [GGFont fixedFont]];
	    [menu addMenuItem: item]; 
	    RELEASE(item);
	  }
	else if ([stocks [i] isKindOfClass: [GGView class]] )
	  {
	    GGView *sv = stocks[i];
	    [menu addMenuItem: sv];
	  }
	else
	  NSWarnMLog(@"don't know what to do with %@", stocks[i]);
      }

      [menu endItem];

      [scroll setDocumentView: menu];
      RELEASE(scroll);
    }
}
@end


@implementation EquipmentInfoView
+ (NSString *) iconName
{
  return @"outils.png";
}

- (void) hasMoveTo: (InfoView *)top
{
  [[top titleView] updateString: @"Ship equipment"];
  [super hasMoveTo: top];
}

- (void) setInfo: (GGShip *)ship
{
  NSMutableArray *equip;
  NSArray *goodies;

  equip = [NSMutableArray new];

  goodies = [ship goodiesAndEquipment: equip];
  [self setItemList: equip];
  RELEASE(equip);
}

- (void) updateView
{
  [self setInfo: theCommandant];
}


- (id) initWithFrame: (NSRect)frameRect
{
  [super initWithFrame: frameRect];
  [self setInfo: theCommandant];
  val = 0;
  return self;
}

@end

@implementation GoodiesInfoView
+ (NSString *) iconName
{
  return @"ship0.jpg";
}

- (void) hasMoveTo: (InfoView *)top
{
  [[top titleView] updateString: @"Commercial goodies"];
  [super hasMoveTo: top];
}

- (GGView *) makeLargableObject:(ItemStock *)is
{	
  GGOption *opt = [GGOption optionWithStr: 
			      [NSString stringWithFormat: @"%-16@    %d", 
					is->item->name,
					is->cargo]
			    font: [GGFont fixedFont]];
  [opt setTarget: self];
  [opt setAction: @selector(largage:)];
  [opt setTag: is->item->arg1];
  return opt;
}

- (void) setInfo: (GGShip *)ship
{
  NSMutableArray *equip;
  NSArray *goodies;

  equip = [NSMutableArray new];

  goodies = [ship goodiesAndEquipment: equip];
  [self setItemList: [goodies ggmap: self 
			      action: @selector(makeLargableObject:)]];
  RELEASE(equip);
}

- (void) updateView
{
  [self setInfo: theCommandant];
}



- (void) largage: (GGBouton *)bo
{
  Item *item;
  NSDebugMLLog(@"Goodies", @"je largue de %@", bo);
  item = [Item goodiesWithArg: [bo tag]];
  [theCommandant removeItem: item];
  [self setInfo: theCommandant];
}


- (id) initWithFrame: (NSRect)frameRect
{
  [super initWithFrame: frameRect];
  [self setInfo: theCommandant];
  val = 1;
  return self;
}

@end

@implementation InfoManager
- init
{
  [super initWithTag: 2
	 window: theGGInput];

  infoView = [InfoView new];
  classes = [NSArray arrayWithObjects:
		       [EquipmentInfoView class],
		     [GoodiesInfoView class], 
		     [ContractView class],
		     nil];
  max = [classes count];
  RETAIN(classes);

  [infoView setManager: self];
  isopen = NO;

  return self;
}

- (NSString *) iconForIndex: (int)val
{
  id c;
  NSParameterAssert(val >= 0 && val < [classes count]);

  c = [classes objectAtIndex: val];

  if( [c conformsToProtocol: @protocol(IconsInfo)] )
    return [c iconName];
  else
    return nil;
}

- (void) invalidate
{
  //  [infoView close];
  DESTROY(infoView);
  [super invalidate];
}

- (void) dealloc
{
  RELEASE(infoView);
  RELEASE(classes);
  [super dealloc];
}

- (void) switchTo: (int) val
{
  id c;
  NSParameterAssert(classes);
  if (val < 0 || val >= [classes count] )
    {
      NSWarnMLog(@"val out of bound");
      return;
    }

  c = [classes objectAtIndex: val];
  [infoView setInfoView: c];

  if( !isopen )
    {
      isopen = YES;
      [infoView openIn: window];
    }
}

@end
