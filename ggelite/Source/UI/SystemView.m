/*
 *  SystemView.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: June 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>

#import "utile.h"
#import "SystemView.h"
#import "GGNode.h"
#import "System.h"
#import "World.h"
#import "GGSpaceObject.h"
#import "GGOrbiteur.h"
#import "GGRepere.h"
#import "GGInput.h"
#import "Tableau.h"
#import "Preference.h"
#import "Console.h"
#import "GGWidget.h"
#import "Commandant.h"
#import "FloatingView.h"
#import "GGEventDispatcher.h"


static const double foc = Focal;


@interface SystemView (Private)
- (void) centerRepereLocal;
- (void) updateProjection;
- (void) checkClosestOrbiteur;
- (void) _updatePositionCamera;
@end

@interface Orbite : GGSpaceObject
{
    ParametresOrbitaux 	prm;
    float			angle;
}
+ orbiteWithAngle: (float) _angle
             data: (ParametresOrbitaux *) pa;
@end

@implementation SystemView
+ (NSString *) iconName
{
    return @"systemview.png";
}

- (id) initWithFrame: (NSRect)frameRect
{
    System *currentSystem;
    [super initWithFrame: frameRect];
    [self enableTextureBackground];
    //  [self setState: GUI_Map];
    
    topNode = [GGNode new];
    [topNode setManager: self];
    cameraObj = [GGSpaceObject new];
    
    ship = [GGShip shipWithFileName: @"python.shp"];
    RETAIN(ship);
    [ship setVisible: YES];
    [ship setName:@"Commandant"];
    
    currentSystem = [theWorld currentSystem];
    
    mainStar = [[currentSystem mainStar] copy];
    [mainStar miseAJourSysteme];
    
    leRepereLocal = [GGRepere new];
    //  [leRepereLocal beginSchedule];
    [leRepereLocal setDefaultName: @"repere local"];
    [topNode addSubNode: leRepereLocal];
    
    initVect(Point(cameraObj), 0, 0, 2e11);
    initVect(Direction(cameraObj), 0, 0, -1);
    initVect(Haut(cameraObj), 0, 1, 0);
    [cameraObj normalise];
    [topNode addSubNode: cameraObj];
    [topNode addSubNode: ship];
    
    *Point(leRepereLocal) = *Point(cameraObj);
    [leRepereLocal reparent: cameraObj];
    
    
    fv = [[FloatingView alloc] initWithFrame: NSMakeRect(0, 0, 10, 10)];
    [fv setTransparency: 0.3];
    [self addSubview: fv];
    
    console = [[Console alloc]
	      initWithFrame: NSMakeRect(5, 5, 0.3*frameRect.size.width,
                                    0.3*frameRect.size.height)];
    [fv addSubview: console];
    
    [self centerRepereLocal];
    
    [self updateProjection];
    [self checkClosestOrbiteur];
    
    [theLoop fastSchedule: self
             withSelector: @selector(updateView)
                      arg: nil];
    [theLoop slowSchedule: self
             withSelector: @selector(checkClosestOrbiteur)
                      arg: nil];
    [theLoop slowSchedule: self
             withSelector: @selector(centerRepereLocal)
                      arg: nil];
    
    NSParameterAssert(theClosest != nil);
    
    distance = sqrt(distanceAbsolu2(theClosest, cameraObj));
    
    [[self titleView]
    updateString: @"Current System"];
    [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"MovementMapKeys"]];

    return self;
}

- (void) updateProjection
{
    NSRect frameWindow = [self frame];
    projectionParam.ratio = (double) frameWindow.size.width / 
        frameWindow.size.height;
    projectionParam.yFoc = tan((double)Focal/2.0/180*M_PI);
    projectionParam.xFoc = projectionParam.yFoc*projectionParam.ratio;
    projectionParam.zmin = 1;
    projectionParam.zmax = 10000;
}

- (void) openIn: (GGNSWindow *)w
{
    [super openIn: w];
    [(GGNSWindow*)_window makeFirstResponder: self];
}

- (void) dealloc
{
    [theLoop unSchedule: self];
    [ship invalidate];
    [mainStar invalidate];
    
    RELEASE(ship);
    RELEASE(console);
    RELEASE(fv);
    RELEASE(topNode);
    RELEASE(cameraObj);
    RELEASE(mainStar);
    RELEASE(leRepereLocal);
    NSDebugMLLog(@"SystemView", @"deallocation");
    [super dealloc];
}

- (void) centerRepereLocal
{
    if( prodScal(Point(cameraObj), Point(cameraObj)) > square(1e6) )
    {
        [leRepereLocal updateOrigine: cameraObj];
        //      NSDebugMLLog(@"SystemView", @"update Position");
    }
}

- (void) initGLBeforeDrawing
{
    Vect4D lightPosition = { {-1, 0, 1}, 0};
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    ggLightPosition(GL_LIGHT0, lightPosition);
    
    /*nécessaire pour avoir une ilumination correcte des objets proches.
        */
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,   (int) thePref.LightLocal);
    
    if( thePref.GLSmooth )
        glShadeModel(GL_SMOOTH);
    else
        glShadeModel(GL_FLAT);
}

- (void) myLockFocusInRect
{
    NSRect wrect;
    
    NSAssert(_window != nil, @"bad");
    
    wrect = [self convertRect: [self bounds] toView: nil];
    NSDebugLLog(@"GGView", @"Displaying rect \n\t%@\n\t window %p", 
                NSStringFromRect(wrect), _window);
    [_window setGLFrameInRect: wrect];
}


- (void) drawRect: (NSRect) _theRect
{
    //  printf("%d %d %d %d\n", x, y, w, h);
    NSRect theRect = [self bounds];
    ProjectionParam param = GGSetUpProjection(theRect,1,1e20,foc);
    
    param.label = YES;
    projectionParam = param;
    
    [self _updatePositionCamera];
    
    glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_VIEWPORT_BIT);
    glDepthFunc(GL_ALWAYS);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthRange(1, 1);
    [super drawRect: _theRect];
    glPopAttrib();
    
    glEnable(GL_SCISSOR_TEST);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthRange(0,1);
    
    
    [self myLockFocusInRect];
    
    glMatrixMode(GL_PROJECTION);
    GGSetupGLProjectionWithParam(&param);
    glMatrixMode(GL_MODELVIEW);
    
    [self initGLBeforeDrawing];
    [cameraObj drawGraphProjection: &param];
    glDisable(GL_LIGHTING);
    [_window initWindowGL];
    GLCHECK;
}

+ systemView
{
    return AUTORELEASE([self new]);
}

- (void) makeFatherCurrentOrbiteur
{
    GGSpaceObject 	*newNode;
    GGSpaceObject 	*oldNode;
    GGOrbiteur	*orbiteurPere = [theClosest orbiteurPere];
    
    if( orbiteurPere == nil )
        return;
    
    [theClosest stopBeingOrigineOfLocale];
    oldNode = [theClosest nodePere];
    NSParameterAssert(oldNode != nil);
    newNode = [oldNode nodePere];
    NSParameterAssert(newNode != nil);
    
    [newNode reparent: leRepereLocal];
    [newNode reparent: theClosest];
    
    [[theClosest sons] makeObjectsPerformSelector: @selector(endSchedule)];
    
    [oldNode removeFromSuperNode];
    
    theClosest = [theClosest orbiteurPere];
    NSParameterAssert(theClosest != nil);
    
    [theClosest startBeingMainLocale];
}

- (void) _addOrbite: (NSArray *)sons
             atNode: (GGNode *)newNode
{
    int n = [sons count];
    if( n > 0 )
    {
        GGOrbiteur *orbs[n];
        int i;
        
        [sons getObjects: orbs];
        for( i = 0; i < n; ++i)
        {
            Orbite *o = [Orbite orbiteWithAngle: [orbs[i] anomalie]
                                           data: [orbs[i] parametresOrbitaux]];
            NSDebugLLog(@"Orbite", @"radius %@ = %g", orbs[i],
                        [orbs[i] parametresOrbitaux]->a);
            [newNode addSubNode: o];
        }
    }
}

- (void) makeSonCurrentOrbiteur: (GGOrbiteur *)son
{
    GGNode 	*newNode;
    GGSpaceObject 	*currentNode;
    NSArray 	*sons;
    
    [theClosest stopBeingMainLocale];
    currentNode = [theClosest nodePere];
    newNode = [GGNode new];
    [newNode setName: [NSString stringWithFormat: @"repere translation from %@",
        son]];
    
    *Galileen(newNode) = *Galileen(son);
    
    [currentNode addSubNode: newNode];
    [newNode reparent: son];
    [newNode reparent: leRepereLocal];
    theClosest = son;
    
    [son startBeingOrigineOfLocale: newNode];
    sons = [son sons];
    [newNode addSubNodeFromArray: sons];
    [son beginSchedule];
    [sons makeObjectsPerformSelector: @selector(beginSchedule)];
    
    [self _addOrbite: sons
              atNode: newNode];
    
    RELEASE(newNode);
}

- (void) checkClosestOrbiteur
{
    GGOrbiteur *old = theClosest;
    if( theClosest == nil ) //first time
    {
        NSArray	*sons;
        GGOrbiteur *soleil = mainStar;
        theClosest = soleil;
        [topNode addSubNode: soleil];
        [soleil startBeingOrigineOfLocale: topNode];
        sons = [soleil sons];
        NSDebugLLog(@"SystemView", @"sons = %@", sons);
        [topNode addSubNodeFromArray: sons];
        [soleil beginSchedule];
        [sons makeObjectsPerformSelector: @selector(beginSchedule)];
        [self _addOrbite: sons
                  atNode: topNode];
    }
    else
    {
        Vect3D 		pos;
        GGOrbiteur	*son;
        //FIXME : source de bug pour le futur
        [cameraObj positionInRepere: [theClosest subSystemFrame]
                                 in: &pos];
        NSDebugMLLogWindow(@"World", @"position orb %@", stringOfVect(&pos));
        if( prodScal(&pos, &pos) > square([theClosest tailleSysteme]) )
            [self makeFatherCurrentOrbiteur];
        else if ((son = [theClosest closestFromRelative: &pos] ) != theClosest )
            [self makeSonCurrentOrbiteur: son];
        /* else do nothing */
    }
    if( currentOrbiteur == nil )
        currentOrbiteur = theClosest;
    if( theClosest != old && theClosest != nil )
    {
        [console clear];
        [console log: @"relative to @r%@", theClosest];
        [fv sizeToFit];
    }
}

- (GGSpaceObject *)camera
{
    return cameraObj;
}

- (void) centerView
{
    NSLog(@"Center View");
}

- (void) zoomIn
{
    state |= GGAccelere;
}

- (void) stopZoomIn
{
    state &= ~GGAccelere;
}

- (void) zoomOut
{
    state |= GGDecelere;
}

- (void) stopZoomOut
{
    state &= ~GGDecelere;
}

- (void) mouseDown: (Event *) pev
{
    if( buttonOfEvent(pev) == GG_BUTTON_RIGHT )
        _drag = YES;
    else if( buttonOfEvent(pev) == GG_BUTTON_LEFT )
    {
        NSPoint 		res 	= 
        [self convertPoint: mouseLocationOfEvent(pev)
                  fromView: nil];
        GGOrbiteur 	*orbCliq = nil;
        int 		n 	= [[theClosest sons] count];
        GGOrbiteur 	*lstobj[n+1];
        int 		distCliq = 10000;
        Camera 		c;
        GGGalileen 	gal;
        int 		i;
        
        [theClosest galileanTransformationTo: cameraObj
                                          in: &gal];
        
        c.modelView = gal.matrice;
        c.param = &projectionParam;
        
        lstobj[0] = theClosest;
        [[theClosest sons] getObjects: lstobj+1];
        
        NSDebugMLLog(@"SystemView", @"mouse %g %g", res.x, res.y);
        
        
        for(i = 0; i < n+1; ++i)
        {
            Vect3D screenPos;
            //	  if( getWinCoordinates(&c, Point(lstobj[i]), &screenPos) )
            if( [lstobj[i] windowsCoordinatesForCamera: cameraObj
                                             parameter: &projectionParam
                                                result: &screenPos] )
                
            {
                int temp = fabs(screenPos.x-res.x) + fabs(screenPos.y-res.y);
                NSDebugMLLog(@"SystemView", @"pos %@ %g %g", lstobj[i],
                             screenPos.x, screenPos.y);
                if( temp < 20 && temp < distCliq )
                {
                    distCliq = temp;
                    orbCliq = lstobj[i];
                }
            }
        }
        if ( orbCliq != nil )
        {
            currentOrbiteur = orbCliq;
        }
    }
}

- (void) mouseUp: (Event *)pev
{
    if( buttonOfEvent(pev) == GG_BUTTON_RIGHT )
        _drag = NO;
}

- (void) _updatePositionCamera
{
    GGReal taille;
    PetitGalileen pg;
    taille = 2*Dimension(currentOrbiteur);
    if( distance < taille )
        distance = taille;
    [currentOrbiteur positionSpeedRelativesTo: cameraObj
                                        decal: NULL
                                           in: &pg];
    
    addVect(Point(cameraObj), &pg.point, Point(cameraObj));
    addLambdaVect(Point(cameraObj), -distance, Direction(cameraObj),
                  Point(cameraObj));
}
- (void) mouseMoved: (Event *) pev
{
    if( _drag )
    {
        NSPoint delta = mouseMotionOfEvent(pev);
        int dx = delta.x;
        int dy = delta.y;
        addLambdaVect(Haut(cameraObj), -0.005*dx, Gauche(cameraObj),
                      Haut(cameraObj));
        addLambdaVect(Direction(cameraObj), 0.005*dy, Haut(cameraObj),
                      Direction(cameraObj));
        [cameraObj normalise];
    }
}

- (void) updateView
{
    if( state )
    {
        if( state & GGAccelere && distance > 500 )
            distance *= pow(10,realDeltaTime/2);
        if( state & GGDecelere )
            distance *= pow(10,-realDeltaTime/2);
    }
    if ( theCommandant != nil )
    {
        Vect3D pos;
        pos = [theCommandant getPositionAbsolu];
        
        *Point(ship) = pos;
    }
    else
    {
        NSWarnMLog(@"no main ship ?");
    }
    
}

- (GGRepere *)repere
{
    return leRepereLocal;
}

@end

@implementation Orbite
- initAngle: (float) _angle
       data: (ParametresOrbitaux *) pa;
{
    [self init];
    angle = _angle;
    prm = *pa;
    //we need angles in degree...
    
    prm.N *= 180.0/M_PI;
    prm.i *= 180.0/M_PI;
    prm.w *= 180.0/M_PI;
    prm.M *= 180.0/M_PI;
    
    _flags.graphical = YES;
    [self setVisible: YES];
    return self;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

- (BOOL) shouldCache
{
    return YES;
}

+ orbiteWithAngle: (float) _angle
             data: (ParametresOrbitaux *) pa
{
    Orbite *or = [self alloc];
    return AUTORELEASE([or initAngle: _angle
                                data: pa]);
}

#if 0
- (void) dealloc
{
    NSWarnMLog(@"on dégage ces orbites");
    [super dealloc];
}
#endif

- (void) rasterise: (Camera *)cam
{
    int i;
    //SPEEDUP !!
    glColor3f(0, 1, 0);
    glDisable(GL_LIGHTING);
    glPushMatrix();
    
    //build the rotation matrix from the orbital elements.  It's not very 
    //efficient, we should do like in GGOrbiteur.
    
    glRotatef(prm.N, 0, 0, 1);
    glRotatef(prm.i, 1, 0, 0);
    glRotatef(prm.w, 0, 0, 1);
    
    glTranslatef(-prm.a*prm.e, 0, 0);
    glScalef(prm.a, prm.a*sqrt(1-prm.e*prm.e), 0);
    
    glBegin(GL_LINE_STRIP);
    for( i = 1; i < NBRSEGMENTINCIRCLE; ++i )
    {
        float a = angle + i*2*M_PI/NBRSEGMENTINCIRCLE;
        glVertex2f(cos(a), sin(a));
    }
    glEnd();
    glPopMatrix();
    glEnable(GL_LIGHTING);
}

@end
