/* 	-*-ObjC-*- */
/*
 *  SystemViem.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: May 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __SystemView_h
#define __SystemView_h

#import "TopView.h"
#import "GGSky.h"

@class GGFont;
@class Console;
@class GGOrbiteur;
@class FloatingView;

@interface SystemView : TopView <WorldManager, IconsInfo>
{
  GGShip		*ship;
  GGNode		*topNode;
  GGSpaceObject		*cameraObj;
  GGOrbiteur		*mainStar;
  GGOrbiteur		*theClosest;
  GGOrbiteur		*currentOrbiteur;
  GGRepere		*leRepereLocal;
  Console		*console;
  FloatingView		*fv;
  ProjectionParam	projectionParam;
  int			state;
  GGReal			distance;
  BOOL			_drag;
}
+ systemView;
@end

#endif
