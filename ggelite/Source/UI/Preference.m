/*
 *  Preference.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <math.h>
#import <time.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSUserDefaults.h>
#import <Foundation/NSFileManager.h>
#import <Foundation/NSValue.h>
#import <Foundation/NSException.h>
#import <Foundation/NSArray.h>

#import "Metaobj.h"
#import "GGWidget.h"
#import "Preference.h"
#import "NSWindow.h"
#import "World.h"
#import "GGMenu.h"
#import "TopView.h"
#import "GGScrollView.h"
#import "Goodies.h"
#import "System.h"
#import "GGFont.h"

typedef struct __nameOption
{
    NSString 	*str;
    BOOL		*pval;
    id	        *obj;
    SEL		s;
    NSString	*notification;
    BOOL		default_value;
}NameOption;


UserPref thePref;

@interface OpenFileView : TopView
{
    GGScrollView *scroll;
    GGString *pathRep;
    NSString *path;
    NSMutableArray *array;
    id		delegate;
    SEL		sel;
}
- (void) beginSheetModalDelegate: _delegate
                  didEndSelector: (SEL) sel;
@end

@interface SaveView : OpenFileView
{
    GGString *input;
}
@end

@interface Pref (Private)
+ (Pref *)pref;
- (void) loadOption;
- (void) saveOption;
@end


static Pref *__thePref;

@implementation Pref
+ (Pref *)pref
{
    if(__thePref  == nil )
    {
        __thePref = [self new];
    }
    return __thePref;
}

+ (void) loadOption
{
    if( !__thePref )
        [Pref pref];
    [__thePref loadOption];
}


+ (void) saveOption
{
    if( !__thePref )
        [Pref pref];
    [__thePref saveOption];
}

+ (void) clean
{
    DESTROY(__thePref);
}

- init
{
    NameOption options[] = 
    {
    {@"GL_BLEND", &(thePref.GLBlend), NULL, NULL, nil, NO},
    {@"GL_TEXTURE_2D", &(thePref.GLTexture), NULL, NULL, nil, YES},
    {@"GL_SMOOTH", &(thePref.GLSmooth), NULL, NULL, nil, YES},
    {@"Enable Label", &(thePref.label), NULL, NULL, nil, YES},
    {@"Name Of Star", &(thePref.nameStar), NULL, NULL, nil, YES},
    {@"Stars", &(thePref.stars), NULL, NULL, nil, NO},
    {@"Frame Rate", &(thePref.frameRate), NULL, NULL, nil, NO},
    {@"Sound", &(thePref.sound), &theLoop, @selector(setSoundFlag:), nil
        , NO},
#ifdef DEBUG
    {@"Debug Memory", &(thePref.DebugMemoire), NULL, NULL, nil, NO},
    {@"Easy Pilot", &(thePref.easyPilot), NULL, NULL, nil, NO},
#endif
    {@"Mipmaping", &(thePref.GLMipmap), NULL, NULL, nil, YES},
    {@"Light @rLocale", &(thePref.LightLocal), NULL, NULL, nil, YES},
    {@"Small Texture", &(thePref.SmallTexture), NULL, NULL, 
        @"SmallTextureFlagChanged", YES},
    {@"Direction Hinter", &(thePref.DirectionHinter), NULL, NULL, nil, YES},
    {@"Full Screen", &(thePref.FullScreen), NULL, NULL, 
        @"ToggleFullScreen", NO},
    {@"High Quality", &(thePref.HighQuality), NULL, NULL, nil, YES},
    {@"Hide X Cursor", &(thePref.hideCursor), NULL, NULL, nil, YES},
    {@"Enable Joystick", &(thePref.enableJoystick), &theLoop,  @selector(enableJoystick:), nil, YES}
    };
    
    [super init];
    
    
    opts = NSZoneMalloc([self zone], sizeof(options));
    
    memcpy(opts, options, sizeof(options));
    
    nOption = sizeof(options)/sizeof(NameOption);
    
    return self;
}

- (void) dealloc
{
    NSZoneFree([self zone], opts);
    [super dealloc];
}

- (void) loadOption
{
    int i;
    NSUserDefaults		*def = [NSUserDefaults standardUserDefaults];
    NSDictionary *settings = [def dictionaryForKey: @"Settings"];
    NSDebugMLLog(@"Settings", @"Settings = %@", settings);
    for( i = 0; i< nOption; ++ i)
    {
        NSNumber *val;
        if( settings == nil || 
            (val = [settings objectForKey: opts[i].str]) == nil )
        {
            NSDebugMLLog(@"Settings", 
                         @"no settings for %@, revert to default value", 
                         opts[i].str);
            *opts[i].pval = opts[i].default_value;
        }
        else
        {
            *opts[i].pval = [val boolValue];
            NSDebugMLLog(@"Settings", 
                         @"%@ = %@ (%d)", opts[i].str, val, *opts[i].pval);
        }
    }
    
}

- (void) saveOption
{
    NSUserDefaults		*def = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dico = [NSMutableDictionary new];
    int i;
    
    for(i = 0; i<nOption; ++i)
    {
        NSNumber *val = [[NSNumber alloc] initWithBool: *opts[i].pval];
        [dico setObject: val
                 forKey: opts[i].str];
        RELEASE(val);
    }
    [def setObject: dico forKey: @"Settings"];
    [def synchronize];
    RELEASE(dico);
}

@end


@implementation UserPreference
- initUI
{
    GGOption 		*opt;
    GGMenu 		*menu;
    int i;
    
    [self addCloseButton];
    menu = [GGMenu menu];
    [menu setFrameOrigin: NSMakePoint(100,100)];
    
    
    for(i = __thePref->nOption-1; i>=0 ; --i)
    {
        opt = [GGOption optionWithStr: __thePref->opts[i].str];
        [opt setState: !!*(__thePref->opts[i].pval)];
        [opt setAction: @selector(setOption:)];
        [opt setTag: i];
        [opt setTarget: self];
        [menu addMenuItem: opt];
        NSDebugMLLog(@"Pref", @"%@ %d", __thePref->opts[i].str, 
                     *(__thePref->opts[i].pval));
    }
    [menu endItem];
    [[self contentView] addSubview: menu];
    
    {
        GGBouton *b;
        
        b = [GGBouton button];
        [b setTitle: @"Load Game"];
        [b setTarget: self];
        [b setAction: @selector(loadGame:)];
        
        [b setFrameOrigin: NSMakePoint(20,20)];
        
        [[self contentView] addSubview: b];
    }
    
    {
        GGBouton *b;
        
        b = [GGBouton button];
        [b setTitle: @"Save Game"];
        [b setTarget: self];
        [b setAction: @selector(saveGame:)];
        
        [b setFrameOrigin: NSMakePoint(250,20)];
        
        [[self contentView] addSubview: b];
    }
    
    //  [self setOpacity: 1.0];
    
    return self;
}

- (void) loadGame: obj
{
    OpenFileView *tv = [OpenFileView topview];
    
    [tv openIn: self];
    [tv beginSheetModalDelegate: self
                 didEndSelector: @selector(load:)];
}


- (void) saveGame: obj
{
    SaveView *tv = [SaveView topview];
    
    [tv openIn: self];
    [tv beginSheetModalDelegate: self
                 didEndSelector: @selector(save:)];
}

- (void) load: (NSString *) path
{
    if( path )
    {
        [theWorld loadGame: path];
    }
}


- (void) save: (NSString *) path
{
    if( path )
    {
        [theWorld saveGame: path];
    }
}

- init
{
    NSRect aRect = NSMakeRect(0, 0, WIDTH, HEIGHT);
    [self initWithFrame: aRect];
    [self initUI];
    return self;
}

- (void) opening
{
    NSDebugMLLog(@"Pref", @"window below %@", windowBelow);
    [theWorld setPaused: YES];
}


- (void) save
{
    [[Pref pref] saveOption];
}

- (void) closing
{
    NSDebugMLLog(@"Pref", @"closing");
    [theWorld setPaused: NO];
    [self save];
}


- (void) dealloc
{
    [self save];
    [super dealloc];
}

- (void) setOption: (GGBouton *)bou
{
    NameOption *opts;
    id obj;
    int state = !![bou state];
    int i = [bou tag];
    opts = [Pref pref]->opts;
    *opts[i].pval = state;
    if( opts[i].obj != NULL &&  (obj = *opts[i].obj) != nil)
        [obj performSelector: opts[i].s withObject: (id) state ];
    if( opts[i].notification != nil )
        [[NSNotificationCenter defaultCenter]
      postNotificationName: opts[i].notification 
                    object: self];
    
    NSDebugMLLog(@"Pref", @"i = %d %@ %d %@", i, opts[i].str, state,
                 opts[i].obj);
}

@end

@interface OpenFileView (GGPrivate)
- (void) set: (NSString*) _path;
@end

@implementation OpenFileView
- (void) set: (NSString*) _path
{
    NSEnumerator *enumerator;
    NSArray *_array;
    NSString *fileName;
    GGMenu *m;
    int n;
    int i;
    
    NSFileManager *fileManager;
    
    
err:
        
        ASSIGN(path, _path);
    [pathRep updateString: path];
    fileManager =  [NSFileManager defaultManager];
    
    _array = [fileManager directoryContentsAtPath: path];
    if( _array == nil )
    {
        _path = [fileManager currentDirectoryPath];
        goto err;
    }
    _array = [_array sortedArrayUsingSelector: @selector(caseInsensitiveCompare:)];
    ASSIGN(array, [NSMutableArray array]);
    
    [array addObject: @".."];
    
    
    enumerator = [_array objectEnumerator];
    
    while( (fileName = [enumerator nextObject]) != nil )
    {
        NSString *loc;
        NSDictionary *attr;
        NSString *type;
        
        loc = [path stringByAppendingPathComponent: fileName];
        attr = [fileManager 
	       fileAttributesAtPath: loc
                   traverseLink:  YES];
        
        NSAssert1(attr, @"erreur, %@", fileName);
        
        type = [attr objectForKey: NSFileType];
        
        if( [type isEqual: NSFileTypeDirectory] )
            [array addObject: fileName];
    }
    
    
    [array addObjectsFromArray: [_array pathsMatchingExtensions:
        [NSArray arrayWithObject: @"gge"]]];
    
    
    n = [array count];
    //  NSLog(@"%@ %@", path, _array);
    
    m = [GGMenu menu];
    
    for( i = 0; i < n; ++i)
    {
        [m addItemWithTitle: [array objectAtIndex: i]
                     target: self
                     action: @selector(gloubi:)
                        tag: (id)i];
    }
    
    [m endItem];
    [scroll setDocumentView: m];
    
}

- init
{
    NSRect aRect = NSMakeRect(0, 0, WIDTH,   HEIGHT);
    [self initWithFrame: aRect];
    return self;
}

- initWithFrame: (NSRect) frame
{
    NSUserDefaults		*def = [NSUserDefaults standardUserDefaults];
    NSString *_path;
    
    [super initWithFrame: frame];
    
    scroll = [[GGScrollView alloc] 
	     initWithFrame: NSMakeRect(50, 50, frame.size.width - 300,
                                   frame.size.height-120)];
    [self addSubview: scroll];
    
    pathRep = [GGString new];
    [pathRep setFrameOrigin: NSMakePoint(50, 50+frame.size.height-100)];
    [pathRep setString: @"blu"
              withFont: [GGFont fixedFont]];
    [self addSubview: pathRep];
    
    if( (_path = [def stringForKey: @"PathForSaving"]) != nil)
    {
        [self set: _path];
    } 
    else
        [self set: [[NSFileManager defaultManager] currentDirectoryPath]];
    
    RELEASE(scroll);
    
    return self;
}

- (void) beginSheetModalDelegate: _delegate
                  didEndSelector: (SEL) _sel
{
    delegate = _delegate;
    sel = _sel;
}


- (void) dealloc
{
    RELEASE(pathRep);
    RELEASE(array);
    RELEASE(path);
    [super dealloc];
}

- (void) close
{
    NSUserDefaults		*def = [NSUserDefaults standardUserDefaults];
    
    [def setObject: path
            forKey: @"PathForSaving"];
    
    //  [def synchronize];
    
    [super close];
}

- (void) gloubi: (GGBouton *) b
{
    NSDictionary *attr;
    NSString *type;
    
    NSString *fileName;
    int index = [b tag];
    fileName = [path stringByAppendingPathComponent: [array objectAtIndex: index]];
    fileName = [fileName stringByStandardizingPath];
    
    attr = [[NSFileManager defaultManager]
	   fileAttributesAtPath: fileName
               traverseLink: YES];
    
    NSAssert1(attr, @"probleme avec %@", fileName);
    
    type = [attr objectForKey: NSFileType];
    if([type isEqual: NSFileTypeDirectory] )
    {
        [self set: fileName];
    }
    else
    {
        NSLog(@"found %@", fileName);
        if( delegate && sel )
        {
            [delegate performSelector: sel
                           withObject: fileName];
        }
        [self close];
    }
}
@end


@implementation SaveView
- initWithFrame: (NSRect) frame
{
    [super initWithFrame: frame];
    
    input = [GGString new];
    [input setEditable: YES];
    [input setDelegate: self];
    [input setString: @""
            withFont: [GGFont fixedFont]];
    [input setFrameOrigin: NSMakePoint(50, 10)];
    [self addSubview: input];
    [input updateString: [path stringByAppendingString: @"/"]];
    
    return self;
}

- (void) set: (NSString *)_path
{
    [super set: _path];
    [input updateString: [_path stringByAppendingString: @"/"]];
}

- (void) textHasBeenValidated: (GGString *) str
{
    NSString *choice;
    NSString *ext;
    NSParameterAssert(str == input);
    choice = [str value];
    ext = [choice pathExtension];
    if (! [ext isEqualToString: @"gge"] )
        choice = [choice stringByAppendingPathExtension: @"gge"];
    NSLog(@"%@ has been choosen", choice);
    if( delegate && sel )
        [delegate performSelector: sel
                       withObject: choice];
    [self close];
}

- (void) dealloc
{
    RELEASE(input);
    [super dealloc];
}

// - (void) viewWillMoveToWindow: (GGNSWindow *)win
// {
//   [super viewWillMoveToWindow: win];
//   if( win != nil )
//     [win makeFirstResponder: input];
// }

@end
