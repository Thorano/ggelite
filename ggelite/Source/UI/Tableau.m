/*
 *  Tableau.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <math.h>
#import <time.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSException.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSScanner.h>
#import <Foundation/NSCalendarDate.h>
#import "Metaobj.h"
#import "GGLoop.h"
#import "GGSpaceObject.h"
#import "GGShip.h"
#import "GGRepere.h"
#import "utile.h"
#import "Tableau.h"
#import "Preference.h"
#import "GGOrbiteur.h"
#import "Commandant.h"
#import "GGFont.h"
#import "Radar.h"
#import "Console.h"
#import "GGInput.h"
#import "World.h"
#import "Sched.h"
#import "GGIcon.h"
#import "GGWidget.h"
#import "Clouds.h"

GGTableau *theTableau=nil;

#define  xOriginePower (15)
#define  yOriginePower (10+HauteurTableauBord)


void drawPower(float power)
{
    int i, incy, larg;
    
    static float effCoef = 0.0;
    static float elapsedTime = 0.0;
    float coef = 1.0;
    
    larg = 10;
    
    if( elapsedTime >= 0.1 )
    {
        elapsedTime = 0.0;
        if( effCoef + 0.1 <= power )
            effCoef += 0.1;
        else if( effCoef >= power )
            effCoef -= 0.1;
    }
    
    elapsedTime += deltaTime;
    
    for( i = 0, incy = 0; i < 9; ++ i, incy += 6)
    {
        float val = (float)(i)/10;
        if( val >= effCoef )
        {
            coef = 0.4;
        }
        glColor3f(coef * val, coef * val/2, coef * (0.5-val/2));
        glRecti(xOriginePower, yOriginePower+incy, xOriginePower+larg, 
                yOriginePower+incy+4);
        larg += 3*i;
    }
}


@implementation GGTableau
- init
{
    [super init];
    observeur = nil;
    closestOrbiteur = nil;
    theTableau = self;
    posCibleCorrect = NO;
    posDestCorrect = NO;
    posVitCorrect = NO;
    posDirCorrect = NO;
    posVitDestCorrect = NO;
    joyXPos = 0.0;
    joyYPos = 0.0;
    string = [StringArray new];
    font = RETAIN([GGFont fixedFont]);
    
    addToCheck(self);
    return self;
}

- (void) dealloc
{
    RELEASE(font);
    RELEASE(closestOrbiteur);
    RELEASE(observeur);
    RELEASE(string);
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [super dealloc];
}

- (void) setRectCible: (NSRect) rect 
		     dist: (float) dist;
{
    targetRect = rect;
    distCible = dist;
    posCibleCorrect = YES;
}

- (void) setJoyXPos: (float) val
{
    joyXPos = val;
}
- (void) setJoyYPos: (float) val
{
    joyYPos = val;
}

- (void) setClosestOrbiteur: (GGOrbiteur *)theOrb
{
    ASSIGN(closestOrbiteur, theOrb);
}

- (GGOrbiteur *) closestOrbiteur;
{
    return closestOrbiteur;
}


- (void) setObserveur: (GGSpaceObject *) obj
{
    ASSIGN(observeur, obj);
    [obj enregistre: self
               with: @selector(removeObserveurWithNotification:)];
}

- (void) removeObserveurWithNotification:  (NSNotification *) aNotification
{
    if( [aNotification object] == observeur )
        [self setObserveur: nil];
}

- (BOOL) saveChannel
{
    return YES;
}

- (id)replacementObjectForArchiver:(NSArchiver *)anArchiver
{
    return [GGArchiver ggArchiver: @"Tableau"];
}

- (void) setFrameRect: (NSRect) aRect
{
    frameRect = aRect;
}

- (void) clean
{
    [string clear];
}

- (void) drawTableau
{
    int width = frameRect.size.width;
    int height = frameRect.size.height;
    int i;
    GGShip *target = nil;
    GGSpaceObject *destination = nil;
    
    //  abort();
    NSDebugMLLogWindow(@"Tableau", @"w %d h %d\n", width, height);
    
    //  puts("caca");
    if( thePref.GLBlend )
        glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glColor4f(0.4,0.4,0.4,0.5);
    glRecti(1,height-200,210, height-10);
    
    [string clear];
    if( thePref.frameRate )
        [string addFormat: @"@r%g fps@w", [theLoop fps]];
#ifdef DEBUG
    {
        int count = [[theWorld currentShips] count];
        if( wizard )
            [string addFormat: @"nShip = @g%d@w", count];
    }
    
#endif
    [string addFormat: @"%g X", [theLoop timeSpeed]];
    if( observeur != nil )
    {
        double pourcent;      
        double maxShield;
        int modePilote = [observeur modePilote];
        target = [observeur target];
        destination = [observeur destination];
        switch(modePilote)
        {
            case Manuel:
                [string addFormat: @"Manual Mode"];
                break;
            case  AutoPilote:
                [string addFormat: @"Auto Pilot"];
                break;
            case Hinter:
                [string addFormat: @"Hinter Mode"];
                [string addFormat: @"wished speed : %g", 
                    [observeur speedWished]];
                break;
            default:
                break;
        }
        [string addFormat: @"fuel %g L", [observeur carburant]];
#ifdef DEVEL
        if( wizard )
        {
//            [string addFormat: stringOfVect2([observeur getPositionAbsolu])];
            [string addFormat: @"distance %5.5e", 
                sqrt(prodScal( Point(observeur), Point(observeur)))];
            [string addFormat: [NSString stringWithFormat: @"camera speed %.5e",
                sqrt(prodScal(Speed(observeur), 
                              Speed(observeur)))]];
            [string addFormat: @"current game time : %g", gameTime];
        }
#endif
        drawPower([(GGShip *)observeur power]);
        
        glColor3f(1,1,1);
        if ( (maxShield = [observeur maxShield]) > 0 )
        {
            pourcent = percent(Shield(observeur), maxShield);
            
            [string addFormat: 
                [NSString stringWithFormat: 
                                  @"Shield: %6.1f %%", pourcent]];
        }
        pourcent = percent(Hull(observeur), MaxHull(observeur));
        [string addFormat:
            [NSString stringWithFormat: @"Hull: %8.1f %%", pourcent]];
        
        
        if( target != nil )
        {
            if( [target isKindOfClass: [Cloud class]] )
            {
                if ( HasCloudAnalyzer(observeur) )
                {
                    Cloud *c = (Cloud *) target;
                    Tunnel *t = [c tunnel];
                    NSMutableString *str;
                    NSParameterAssert(t);
                    
                    str = [NSMutableString stringWithFormat: 
                               @"HyperCloud\nDeparture date: %@\nArrival date: %@\n",
                        stringFromGameDate([t departureDate]),
                        stringFromGameDate([t arrivalDate])];
                    if ( c == [t departureCloud] )
                        [str appendFormat: @"Destination: %@",
                            [theWorld starNameForIndex: t->destination]];
                    else
                    {
                        NSAssert(c == [t arrivalCloud],
                                 @"bad cloud");
                        
                        [str appendFormat: @"Source: %@",
                            [theWorld starNameForIndex: t->source]];
                    }
                    [[GGFont fixedFont] 
		    drawString: str
              atOrigin: NSMakePoint(width-400, height-10)];
                    
                }
                
            }
            else if ( HasInspector(observeur) && 
                      [target isKindOfClass: [GGShip class]])
            {
                GGShip *t = (GGShip *)target;
                NSString *str;
                char tmp[50];
                tmp[49] = 0; //just in case
                if( HasShield(t) )
                    snprintf(tmp,50,"%-5.1f %%", percent(Shield(t),
                                                         [t maxShield]));
                else
                    strcpy(tmp, "None");
                
                str = [NSString stringWithFormat:
                 @"%@\nClass %d Hyperdrive\nHull:    %-5.1f%%\nShield:  %s", 
                    [t modelName],
                    HyperDriveType(t),
                    percent(Hull(t), MaxHull(t)),
                    tmp];
                
                [[GGFont fixedFont]
		drawString: str
          atOrigin: NSMakePoint(width-200, height-10)];
                
            }
        }
        
    }
    
    
    if	( target != nil )
    {
        [string addFormat: 
            [NSString stringWithFormat: @"target :@r%@@w", target]];
        
        if( [target isKindOfClass: [GGShip class]] )
        {
            GGShip *val = (GGShip *)target;
            [string addFormat: @"shield : %g, hull : %g", 
                val->bouclier,
                val->endurance];
            [string addFormat: @"masse : %g", val->masse];
        }
        
        if( wizard )
        {
            [string addFormat: 
                [NSString stringWithFormat: @" pos : %@\nrel to @y%@@w", 
                    stringOfVect(Point(target)),
                    target->nodePere]];
        }
        
        [string addFormat: 
            [NSString stringWithFormat: @" distance : %.2fkm",
                distanceAbsolu(observeur, target)/1000.0]];
    }
    
    if( destination != nil )
    {
        [string addFormat: @"destination : @g%@@b", destination];
    }
    
    glDisable(GL_BLEND);
    
    
    [string displayAt: 10 and: height-20];
    
    
    if( closestOrbiteur != nil )
    {
        glColor3f(0.8, 0.8, 0.8);
        
        [font drawString:
            [NSString stringWithFormat: @"%@ : %2g km", 
                closestOrbiteur,
                distance/1000]
                atOrigin: NSMakePoint(width-150, HauteurTableauBord+10)];
        [font drawString:
            [NSString stringWithFormat: @"speed : %2g km/s", 
                speed/1000]
                atOrigin: NSMakePoint(width-150, HauteurTableauBord+20)];
        if( observeur != nil && [observeur nearPlanete] )
        {
            double hauteur = [observeur hauteurTerrain];
            double alt = [observeur altitude];
            [font drawString: [NSString stringWithFormat: 
                                      @"hauteur terrain : %5g m", 
                hauteur]
                    atOrigin: NSMakePoint(width-150, HauteurTableauBord+30)];
            [font drawString: [NSString stringWithFormat: @"altitude : %5g m", 
                alt]
                    atOrigin: NSMakePoint(width-150, HauteurTableauBord+40)];
        }
        
    }
    
    if( posCibleCorrect )
    {
        posCibleCorrect = NO;
        if( destIsTarget )
            glColor3f(1, 0, 1);
        else
            glColor3f(1, 0, 0);
        drawRectBorder(NSMinX(targetRect), NSMinY(targetRect), NSMaxX(targetRect), NSMaxY(targetRect));
        [font drawString: 
            [NSString stringWithFormat: @"%3g km", distCible/1000]
                atOrigin: NSMakePoint(NSMaxX(targetRect)+10, NSMinY(targetRect)-20)];
        destIsTarget = NO;
    }
    if( posDestCorrect )
    {
        posDestCorrect = NO;
        glColor3f(0, 1, 0.4);
        [font drawString: 
            [NSString stringWithFormat: 
				@"%.2f km", distDest/1000]
                atOrigin: NSMakePoint(NSMaxX(destRect)+10, NSMidY(destRect))];
        drawRectBorderWithRect(destRect);
    }
    if( posVitCorrect )
    {
        posVitCorrect = NO;
        glColor3f(1, 1, 1);
        glBegin(GL_LINES);
        glVertex2f(xVit - 2, yVit);
        glVertex2f(xVit - 10, yVit);
        glVertex2f(xVit + 2, yVit);
        glVertex2f(xVit + 10, yVit);
        glVertex2f(xVit, yVit - 2);
        glVertex2f(xVit, yVit - 10);
        glVertex2f(xVit, yVit + 2);
        glVertex2f(xVit, yVit + 10);
        glEnd();
    }
    
    if( posDirCorrect )
    {
        posDirCorrect = NO;
        glColor3f(1, 1, 1);
        glBegin(GL_LINES);
        glVertex2f(xDir-10, yDir-10);
        glVertex2f(xDir+10, yDir+10);
        glVertex2f(xDir+10, yDir-10);
        glVertex2f(xDir-10, yDir+10);
        glEnd();
    }
    
    if( posVitDestCorrect )
    {
        posVitDestCorrect = NO;
        glColor3f(1, 0, 0);
        glBegin(GL_LINES);
        glVertex2f(xVitDest - 2, yVitDest);
        glVertex2f(xVitDest - 10, yVitDest);
        glVertex2f(xVitDest + 2, yVitDest);
        glVertex2f(xVitDest + 10, yVitDest);
        glVertex2f(xVitDest, yVitDest - 2);
        glVertex2f(xVitDest, yVitDest - 10);
        glVertex2f(xVitDest, yVitDest + 2);
        glVertex2f(xVitDest, yVitDest + 10);
        glEnd();
    }
    
    glColor3f(0.1, 0.6, 0.1);
    glBegin(GL_LINES);
    glVertex2f(0.4*width, 0.6*height);
    glVertex2f(0.6*width, 0.6*height);
    glVertex2f(0.6*width, 0.6*height);
    glVertex2f(0.6*width, 0.4*height);
    for( i = 0; i < 8; ++i )
    {
        float x = 0.4+ (float)i*0.2/8;
        glVertex2f(x*width, 0.6*height);
        glVertex2f(x*width, (0.59)*height);
        glVertex2f(0.6*width, x*height);
        glVertex2f(0.59*width, x*height);
    }
    glEnd();
    glBegin(GL_TRIANGLES);
    {
        float x = (0.5+joyXPos/10)*width; 
        float y = (0.5+joyYPos/10)*height; 
        
        glVertex2f(x, 0.6*height);
        glVertex2f(x-5, 0.58*height);
        glVertex2f(x+5, 0.58*height);
        glVertex2f(0.6*width, y);
        glVertex2f(0.58*width, y+5);
        glVertex2f(0.58*width, y-5);
    }
    glEnd();
    
//    NSLog(@"will draw %g", gameTime);

}

- (GGSpaceObject *) observeur
{
    return observeur;
}

- (void) doIndicator: (ProjectionParam *)ppp
              camera: (GGSpaceObject *) camera
{
    GGSpaceObject *destination = nil;
    GGShip *target = nil;
    Camera c;
    Vect3D result;
    c.param = ppp;
    
    if (observeur)
    {
        Vect3D temp;
        destination = [observeur destination];
        target = [observeur target];
        [observeur rotationMatrixTo: camera
                                 in: &c.modelView];
        
        
        if( closestOrbiteur != nil )
        {
            PetitGalileen pg;
            [closestOrbiteur positionSpeedInRepere:  observeur
                                                in: &pg];
            speed = sqrt(prodScal(&pg.speed, &pg.speed));
            distance = sqrt(prodScal(&pg.point, &pg.point));
            if( speed > EPS )
            {
                mulScalVect(&pg.speed, -10/speed, &pg.speed);
                if( getWinCoordinates(&c, &pg.speed, &result) )
                {
                    xVit = result.x;
                    yVit = result.y;
                    posVitCorrect = YES;
                }
            }
        }
        
        initVect(&temp, 0, 0, 10);
        
        if( getWinCoordinates(&c, &temp, &result) )
        {
            xDir = result.x;
            yDir = result.y;
            posDirCorrect = YES;
        }
        
        if (target)
        {
            Vect3D posLoc;
            double dist;
            //SPEEDUP:
            [target positionInRepere: observeur
                                  in: &posLoc];
            dist = sqrt(prodScal(&posLoc, &posLoc));
            mulScalVect(&posLoc, 10/dist, &posLoc);
            NSRect rect = [target windowRectInCamera:camera projectionParam:ppp];
            if (!NSEqualRects(rect, NSZeroRect)) {
                [self setRectCible:rect dist:dist];
            }
        }
        
        if (destination )
        {
            PetitGalileen pg;
            GGReal tmp;
            
            if( destination == target )
            {
                destIsTarget = YES;
            }
            else
            {
                destIsTarget = NO;
                [destination positionSpeedInRepere: observeur
                                                in: &pg];
                distDest = sqrt(prodScal(&pg.point, &pg.point));
                //      mulScalVect(&pg.point, 10/distDest, &pg.point);
                // 	      if( getWinCoordinates(&c, &pg.point, &result) )
                destRect = [destination windowRectInCamera:camera projectionParam:ppp];
                if (!NSEqualRects(destRect, NSZeroRect)) {
                    posDestCorrect = YES;
                }

                if( (tmp = prodScal(&pg.speed, &pg.speed)) > EPS*EPS)
                {
                    tmp = sqrt(tmp);
                    mulScalVect(&pg.speed, -10/tmp, &pg.speed);
                    if( getWinCoordinates(&c, &pg.speed, &result) )
                    {
                        xVitDest = result.x;
                        yVitDest = result.y;
                        posVitDestCorrect = YES;
                    }
                }
            }
        }
    }
}
@end

@implementation StringArray
+ stringArray
{
    return AUTORELEASE([self new]);
}

- init
{
    [super init];
    theString = [NSMutableString new];
    theFont = RETAIN([GGFont fixedFont]);
    addToCheck(self);
    return self;
}

- (void) dealloc
{
    RELEASE(theString);
    RELEASE(theFont);
    [super dealloc];
}

- (void) addString: (NSString *) string
{
    [theString appendString: @"\n"];
    [theString appendString: string];
}

- (NSString*) content
{
    return [[theString copy] autorelease];
}

- (void) addFormat: (NSString *) format,...
{
    va_list ap;
    NSString *tmp;
    va_start(ap, format);
    if (format == nil)
        return ;
    else
        tmp = AUTORELEASE([[NSString alloc] initWithFormat: format arguments: ap]);
    va_end(ap);
    [self addString: tmp];
    return ;
}

- (BOOL) isEmpty
{
    return [theString length] == 0;
}

- (void)clear
{
    [theString setString: @""];
}

- (void) displayAt: (int) x and: (int) y
{
    NSPoint origin = NSMakePoint(x, y);
    glColor3f(1.0,1.0,1.0);
    [theFont drawString: theString
               atOrigin: origin];
}

- (void) setFont:(GGFont*)font
{
    ASSIGN(theFont, font);
}
@end

@interface ProgressBar : GGView
{
    float 	state;
    VectCol 	col;
}
- (float) state;
- (void) setState: (float) val;
- (void) setColor: (VectCol) col;
@end

@implementation ProgressBar
-  initWithFrame: (NSRect) rect
{
    [super initWithFrame: rect];
    state = 0.3;
    col = GGMakeCol(1, 0, 0, 1);
    return self;
}

- (float) state
{
    return state;
}

- (void) setState: (float) val
{
    state = val;
}  

- (void) setColor: (VectCol) _col
{
    col = _col;
}

- (void) drawRect: (NSRect)aRect
{
    NSRect rect = [self bounds];
    NSDebugMLLogWindow(@"blu", @"rect = %@", NSStringFromRect(rect));
    glDisable(GL_CULL_FACE);
    glColor3f(0,0,0.4);
    glRectf(NSMinX(rect), NSMinY(rect), NSMaxX(rect), NSMaxY(rect));
    
    glColor4v(&col);
    glRectf(NSMinX(rect)+1, NSMinY(rect)+1, NSMaxX(rect)-1, 
            state*(NSMaxY(rect)-1));
    //  glRecti(2, 2, 10, 10);
    glEnable(GL_CULL_FACE);
    GLCHECK;
}
@end


@implementation ViewTableau
- (void) _updateDate: (GGTimer *)timer
{
    GGShip *s;
    [dateString updateString: stringFromGameDate(gameTime)];
    s = [theWorld commandant];
    if( s )
        [fuelBar setState: Fuel(s)/MaxFuel(s)];
}

- init
{
    float x;
    Console *c;
    Radar *radar;
    [super initWithFrame: NSMakeRect(0, 0, WIDTH, HauteurTableauBord)];
    //  [self setBackgroundColor: GGMakeCol(0.1, 0.1, 0.5, 0.8)];
    radar = [[Radar alloc] initWithFrame: NSMakeRect(200, 0, 200, 100)];
    [self addSubview: radar];
    RELEASE(radar);
    
    {
        ProgressBar *bar;
        bar = [[ProgressBar alloc] initWithFrame: NSMakeRect(150, 20, 4, 50)];
        [self addSubview: bar];
        fuelBar = bar;
        RELEASE(bar);
    }
    
    c = [[Console alloc]
	initWithFrame: NSMakeRect(_frame.size.width-380, 2, 380, 
                              _frame.size.height-23)];
    
    //  [self setNextResponder: theGGInput];
    
    [self addSubview: c];
    [c setAutoresizingMask: GGNSViewMinXMargin];
    theConsole = c;
    RELEASE(c);
    
    dateString = RETAIN([GGGlyphLayout glyphLayoutWithString: @"blabla"
                              // 			       withFont: [GGFont fontWithFile: @"cu12.pcf"]]);
                                                    withFont: [GGFont fixedFont]]);
    //  [date setAlignement: TopAlignement];
    [self _updateDate: nil];
    
    timer = [GGTimer scheduledTimerWithTimeInterval: 1
                                             target: self
                                           selector: @selector(_updateDate:)
                                            repeats: YES];
    RETAIN(timer);
    
    [[NSNotificationCenter defaultCenter]
    addObserver: self
       selector: @selector(loadingAGame:)
           name: GGNotificationAfterLoadingGame
         object: nil];
    
    x = 3;
    
    {
        int i;
        for(i = 0; i < 10; ++i )
        {
            GGBouton *bouton;
            
            bouton = button[i] = [GGBouton button];
            [bouton setFrameOrigin: NSMakePoint(x, 3)];
            [bouton setBorder: NO];
            [bouton setTag: i+1];
            [bouton setTitle: [NSString stringWithFormat: @"F%d", i+1]
                    withFont: [GGFont defaultSmallFont]];
            [bouton setEnable: NO];
            [self addSubview: bouton];
            x += [bouton frame].size.width;
        }
    }
    
    
    [self setAutoresizingMask: GGNSViewWidthSizable];
    return self;
}

- (void) loadingAGame: obj
{
    [theConsole clear];
    [timer rescheduleForDate: gameTime + 1];
}


- (void) dealloc
{
    [timer invalidate];
    RELEASE(timer);
    RELEASE(dateString);
    [super dealloc];
}

- (void) drawRect: (NSRect) theRect
{
    NSRect rect = [self frame];
    glColor4f(0.2, 0.2, 0.25,0.4);
    glRectf(0,0 , NSWidth(rect), NSHeight(rect));
    glPolygonMode(GL_FRONT, GL_LINE);
    glColor3f(0.8, 0.5, 0);
    glRectf(0,0 , NSWidth(rect)-1, NSHeight(rect)-1);
    glPolygonMode(GL_FRONT, GL_FILL);
    glColor3f(0.5, 0.7, 0.7);
    //  glRecti(10, HauteurTableauBord-5, 20, HauteurTableauBord+5);
    [dateString drawAtPoint: NSMakePoint(400, 81)];
    GLCHECK;
    
}

- (Console *) console
{
    return theConsole;
}

- (void) buildIconBar: (GGIconAction *)ia
                   at: (int) start
                 size: (int) size
{
    int i;
    int x = 3;
    NSAssert(start + size <= 10, @"bad size");
    for(i = 0 ; i < 10; ++i)
    {
        GGBouton *bouton = button[i];
        [bouton setFrameOrigin: NSMakePoint(x, 3)];
        //FIXME: should optimize this.
        if ( i >= start && i - start < size ) 
        {
            int index = i - start;
            if ( ia[index].obj )
            {
                [bouton setIcon: [GGIcon iconWithName:    ia[index].icon]];
                [bouton setTarget: ia[index].obj];
                [bouton setAction: ia[index].action];
                [bouton setEnable: YES];
            }
            else
            {
                [bouton setIcon: nil];
                [bouton setTarget: nil];
                [bouton setEnable: NO];
            }
        }
        x += [bouton frame].size.width;
    }
}


@end

