/* 	-*-ObjC-*- */
/*
 *  GGMenu.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGMenu_h
#define __GGMenu_h

@class NSArray;
@class NSMutableArray;
@class NSData;

@class GGNSWindow;
@class GGView;
@class NSClipView;
@class NSImage;
@class NSCursor;

#import "NSView.h"

@interface GGMenu : GGView
{
  NSString 		*menuTitle;
  NSMutableArray	*items;
  float			ypos;
  BOOL			needUpdate;
}
+ menu;
- (void) endItem;
- (void) addMenuItem: (GGView *)aView;
- (void) addItemWithTitle: (NSString *) str
		   target: (id) obj
		   action: (SEL) aSel;
- (void) addItemWithTitle: (NSString *) str
		   target: (id) obj
		   action: (SEL) aSel
		      tag: (id) obj2;
@end

#endif

