/* 	-*-ObjC-*- */
/*
 *  StarView.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: May 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __StarView_h
#define __StarView_h

#import "TopView.h"
#import "GGSky.h"

#define MAXLOCALSTAR 1000
@class GGFont;
@class Console;
@class FloatingView;

@interface StarView : TopView <IconsInfo>
{
  Console		*textview;
  FloatingView		*fv;
  Star 			theStars[MAXLOCALSTAR];
  int			nStars;
  GLuint		compileListId;
  GLUquadricObj     	*data;
  GGFont		*fontStar, *fontAxe;
  GGGlyphLayout		*axex, *axey, *axez;
  GGMatrix4		modelView;
  //  int			xm, ym;
  Vect3D		orig;
  Vect3D		posCommandant;
  int			state;
  GGReal			distance;
  GGReal			range;
  BOOL			hasMoved;
  int			closest;
  BOOL			_drag;
  StarsZone		*current;
}
+ starView;
- (void) centerView;


@end

#endif
