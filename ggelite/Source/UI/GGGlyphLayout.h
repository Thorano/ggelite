/* 	-*-ObjC-*- */
/*
 *  GGGlyphLayout.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGGlyphLayout_h
#define __GGGlyphLayout_h

#import <Foundation/NSGeometry.h>

@class GGFont;
@class Texture;

#import "types.h"

typedef enum __alignement
{
  TopAlignement,
  CenterAlignement,
  BottomAlignement
}Alignement;

@class NSMutableData;
@class NSMutableString;

@interface GGGlyphLayout : NSObject
{
  GGFont 		*layoutFont;
  NSMutableData		*data;
  //  InterLeavedT2V3	*ilArray;
  int			nChunk;
  int			blockIndex;  //express in sizeof(InterLeavedT2V3)
  int 			maxWidth;
  NSRect		boundRect;
  NSMutableString	*string;
  Alignement		alignement;

  NSPoint 		current;
  BOOL			multiLigne, empty;
}
+ glyphLayoutWithString: (NSString *)s;
+ glyphLayoutWithString: (NSString *)s
	       withFont: (GGFont *)f;

- (InterLeavedT2V3 *)data;
- initWithFont: (GGFont *) font
	string: (NSString *) str;
- (void) updateString: (NSString *)str;
- (void) appendString: (NSString *)str;

- (void) fastDraw;
- (void) drawAtPoint: (NSPoint) aPoint;
- (NSRect) frameRect;
- (void) setAlignement: (Alignement) newAli;
- (void) clear;
- (void) setMaxWidth: (int) newWidth;
@end

@interface GGGlyphLayout (Extension)
//cosmetic method:  format and do appendString.
- (void) log: (NSString *) format,...;
@end


#endif
