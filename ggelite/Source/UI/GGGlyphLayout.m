/*
 *  GGGlyphLayout.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: April 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSException.h>
#import <Foundation/NSData.h>
#import <Foundation/NSString.h>
#import "Metaobj.h"
#import "GGGlyphLayout.h"
#import "GGFont.h"
#import "Texture.h"

#import "Tableau.h"

typedef struct __glyphChunk
{
  int 		next;
  VectCol	col;
}GlyphChunk;

static inline BOOL colWithChar(unsigned char c, VectCol *ret)
{
#define glColor3f(x, y, z) ret->r = x;ret->g=y;ret->b=z;ret->a=1
  switch(c)
    {
    case 'b':
      glColor3f(0, 0, 1);
      break;
    case 'r':
      glColor3f(1, 0, 0);
      break;
    case 'w':
      glColor3f(1, 1, 1);
      break;
    case 'B':
      glColor3f(0, 0, 0);
      break;
    case 'y':
      glColor3f(1, 1, 0);
      break;
    case 'g':
      glColor3f(0, 1, 0);
      break;
    case '@':
      return 0;
    default:
      return -1;
    }
  return 1;
#undef glColor3f
}


@implementation GGGlyphLayout
- (void) _clear
{
  GlyphChunk *block;
  blockIndex = 0;
  nChunk = 1;
  multiLigne = NO;
  current = NSZeroPoint;
  boundRect = NSZeroRect;
  [data setLength: 4*sizeof(InterLeavedT2V3)];

  block = [data mutableBytes];
  initRGBA(&block->col, 1, 1, 1, 1);
  block->next = 0;
  
  [string setString: @""];
  empty = YES;
}

- _appendString: (NSString *)str
{
  int i;
  int oldSize;
  int strlength = [str length];  
  unichar cStr[strlength];
  InterLeavedT2V3 *ila, *ilArray;
  GlyphChunk *block;


  [str getCharacters: cStr];
  oldSize = [data length];

  [data setLength: oldSize+4*(strlength+2)*sizeof(InterLeavedT2V3)];

  ilArray = [data mutableBytes];

  ila = ilArray + (oldSize/sizeof(InterLeavedT2V3));
  block = (GlyphChunk *)(ilArray + blockIndex);

  if( nChunk > 1 || block->next > 0 )
    {
      multiLigne = YES;
      current.y -= [layoutFont lineSpacing];
      current.x = 0;
    }
  
  for(i = 0; i < strlength; ++i)
    {
      int width;
      switch(cStr[i])
	{
	case  '\n':
	  multiLigne = YES;
	  current.y -= [layoutFont lineSpacing];
	  current.x = 0;
	  break;
	case ' ':
	  if (maxWidth > 0 && current.x > maxWidth) 
	    {
	      multiLigne = YES;
	      current.y -= [layoutFont lineSpacing];
	      current.x = 0;
	    }
	  else
	    current.x += [layoutFont spaceWidth];
	  break;
	case '@':
	  {
          VectCol temp = {
              0,0,0,0
          };
	    int res;
	    res = colWithChar(cStr[i+1], &temp);
	    if( res == 1 ) //standard escape
	      {
		//create a newBlock
		//		block->next = ila - ilArray;
		block = (GlyphChunk *)ila;

		block->next = 0;
		block->col = temp;
		ila += 4;
		nChunk++;
		i++;
		break;
	      }
	    else if( res == 0 ) // '@'
	      {
		i++;
		// don't break;
	      }
	    /*else print '@'*/
	  }
	default:
	  width = [layoutFont getGlyphInfo: ila
			forChar: cStr[i]
			atOrigin: current];
	  if( width > 0 )
	    {
	      empty = NO;
      
	      current.x += width;
	      ila += 4;
	      block->next += 4;
	    }

	  break;
	}

      if( boundRect.size.width < current.x  )
	boundRect.size.width = current.x;

    }

  [data setLength: ((unsigned char *)ila) - ((unsigned char *)ilArray)];
  blockIndex = ((InterLeavedT2V3 *)block) - ilArray;

  boundRect.size.height = -current.y+[layoutFont ascend] - [layoutFont descend];
  boundRect.origin.x = [layoutFont bearingX];
  boundRect.origin.y = current.y+[layoutFont descend];

  [string appendString: str];

  return self;
}


- initWithFont: (GGFont *) font
	string: (NSString *) str
{
  [super init];
  maxWidth = -1;
  data = [NSMutableData new];
  string = [NSMutableString new];
  ASSIGN(layoutFont, font);
  [self _clear];
  [self _appendString: str];
  [self setAlignement: BottomAlignement];

  addToCheck(self);


  return self;
}

- (void) updateString: (NSString *)str
{
  [self _clear];
  [self _appendString: str];
}

- (void) appendString: (NSString *)str
{
  [self _appendString: str];
}

- (void) clear
{
  [self _clear];
}

- (void) setAlignement: (Alignement) newAli
{
  alignement = newAli;
}

+ glyphLayoutWithString: (NSString *)s
	       withFont: (GGFont *)f 
{
  GGGlyphLayout *obj;
  obj = [self alloc];
  obj = [obj initWithFont: f
	     string: s];
  return AUTORELEASE(obj);
}

+ glyphLayoutWithString: (NSString *)s
{
  return [self glyphLayoutWithString: s
	       withFont: [GGFont defaultFont]];
}

- (void) setMaxWidth: (int) newWidth
{
  maxWidth = newWidth;
}


- (InterLeavedT2V3 *)data
{
  return [data mutableBytes];
}

- (void) dealloc
{
  RELEASE(data);
  RELEASE(layoutFont);
  RELEASE(string);
//    if( ilArray != NULL )
//      NSZoneFree([self zone], ilArray);
  [super dealloc];
}

- (void) drawAtPoint: (NSPoint) aPoint
{
  // FIXME:  Factorise ça ailleurs que cela ne soit fait qu'une seule fois
  if( empty ) return;
  [layoutFont selectFont];

  glPushMatrix();
  glTranslatef(aPoint.x, aPoint.y, 0);
  if( multiLigne )
    switch(alignement)
      {
      case TopAlignement:
	glTranslatef(0, -[layoutFont ascend], 0);
	break;
      case CenterAlignement:
	glTranslatef(0, boundRect.size.height / 2, 0);
	break;
      case BottomAlignement:
	glTranslatef(0, boundRect.size.height - [layoutFont ascend], 0);
	break;
      }
  else
    switch(alignement)
      {
      case TopAlignement:
	glTranslatef(0, -[layoutFont ascend], 0);
	break;
      case CenterAlignement:
	break;
      case BottomAlignement:
	glTranslatef(0, -[layoutFont descend], 0);
	break;
      }
    

  [self fastDraw];

  glPopMatrix();
  
  [layoutFont endSelectFont];

  NSDebugMLLogWindow(@"Glyph", @"%@ = %d", string, nChunk);
}

- (void) fastDraw
{
  int i;
  GlyphChunk *chunk;
  InterLeavedT2V3 *ila, *ilArray;

  if( empty ) return;

  ilArray = [self data];

  chunk = (GlyphChunk *)ilArray;
  ila = ilArray+4;
  for( i = 0; i < nChunk; ++i )
    {
      if( i > 0)
	glColor4fv((GGReal *)&chunk->col);
      glInterleavedArrays(GL_T2F_V3F, 0, ila);
      glDrawArrays(GL_QUADS, 0, chunk->next);
      ila += chunk->next;
      chunk = (GlyphChunk *)ila;
      ila += 4;
    }
}
//  {
//    glInterleavedArrays(GL_T2F_V3F, 0, ilArray);
//    glDrawArrays(GL_QUADS, 0, 4*size);
//  }


- (NSString *) description
{
  return string;
}

- (NSRect) frameRect
{
  return boundRect;
}
@end

@implementation GGGlyphLayout (Extension)
- (void) log: (NSString *) format,...
{
  va_list ap;
  NSString *str;

  va_start (ap, format);
  str = [[NSString alloc] initWithFormat: format
                               arguments: ap];
  [self appendString: str];
  [str release];
  va_end (ap);
}
@end
