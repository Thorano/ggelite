/* 	-*-ObjC-*- */
/*
 *  InfoView.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __InfoView_h
#define __InfoView_h

#import "TopView.h"

@class GGShip;
@class GGString;
@class GGScrollView;
@class ShipView;
@class InfoView;

@protocol ProtoInfo
- (void) hasMoveTo: (InfoView *)top;
- (void) updateView;
- initWithSize: (NSSize) aSize;
@end

@interface ProtoInfo : GGView <ProtoInfo>
{
  GGScrollView *scroll;
  
@public
  int val;
}
- (void) hasMoveTo: (InfoView *)top;
- (void) updateView;
@end

@interface InfoView : TopView
{
  ProtoInfo 	*info;
  ShipView	*shipView;
}
- (void) setSmallShipView: (BOOL) val;
@end


@interface EquipmentInfoView : ProtoInfo <IconsInfo>
{}
@end


@interface GoodiesInfoView : ProtoInfo <IconsInfo>
{}
@end

@interface InfoManager : TopViewManagerCommon
{
  InfoView 		*infoView;
  NSArray 		*classes;
  BOOL			isopen;
}
@end


#endif
