/* 	-*-ObjC-*- */
/*
 *  Preference.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGPreference_h
#define __GGPreference_h

@class GGBouton;

#import "NSWindow.h"


@interface UserPreference : GGNSWindow
{
}
@end

@interface Pref : NSObject
{
@public
  struct __nameOption *	opts;
  int			nOption;
}
+ (void) loadOption;
+ (void) saveOption;
+ (void) clean;
@end


typedef struct _UserPref
{
  BOOL		GLBlend;
  BOOL		GLTexture;
  BOOL		GLSmooth;
  BOOL		label;
  BOOL		nameStar;
  BOOL		stars;
  BOOL		frameRate;
  BOOL		sound;
  BOOL		GLMipmap;
  BOOL		LightLocal;
#ifdef DEBUG
  BOOL		DebugMemoire;
  BOOL		easyPilot;
#endif
  BOOL		SmallTexture;
  BOOL		DirectionHinter;
  BOOL		FullScreen;
  BOOL		HighQuality;
  BOOL		hideCursor;
  BOOL          enableJoystick;
}UserPref;

extern UserPref thePref;

#endif
