/* 	-*-ObjC-*- */
/*
 *  ContractView.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: April 2003
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>

#import "GGWidget.h"
#import "ContractView.h"
#import "GGFont.h"
#import "GGMenu.h"
#import "GGScrollView.h"
#import "contract.h"

@interface GGView (ContractItem)
+ itemWithContract: (NSArray *)c;
@end

@implementation GGView (ContractItem)
+ itemWithContract: (NSArray *)c
{
  GGView *v;
  GGFont *font = [GGFont fixedFont];
  GGString *str;
  NSParameterAssert(c && [c count] >= 3);
  v = [self alloc];
  [v initWithFrame: NSMakeRect(0, 0, 800, 30)];

  str = [GGString new];
  [str setString: [c objectAtIndex: 0]
       withFont: font];
  [str setFrameOrigin: NSMakePoint(10, 0)];
  [v addSubview: str];
  RELEASE(str);

  str = [GGString new];
  [str setString: [c objectAtIndex: 1]
       withFont: font];
  [str setFrameOrigin: NSMakePoint(130, 0)];
  [v addSubview: str];
  RELEASE(str);


  str = [GGString new];
  [str setString: [c objectAtIndex: 2]
       withFont: font];
  [str setFrameOrigin: NSMakePoint(270, 0)];
  [v addSubview: str];
  RELEASE(str);

  return AUTORELEASE(v);
}
@end


@implementation ContractView
+ (NSString *) iconName
{
  return @"contract.png";
}

- initWithSize: (NSSize) size
{
  return [self initWithFrame: NSMakeRect(10, 30, size.width-40, 
				  (int)(3*size.height/5))];
}

- (void) hasMoveTo: (InfoView *)top;
{
  [[top titleView] updateString: @"Contracts List"];
  [top setSmallShipView: YES];
}

- (void) updateView
{}


- (id) initWithFrame: (NSRect)frame
{
  NSArray *contracts;
  int n;
  [super initWithFrame: frame];

  contracts = [theCommandant contracts];
  NSParameterAssert(contracts);

  n = [contracts count];
  if(n > 0)
    {
      GGMenu *m;
      GGScrollView *scroll;
      int i;
      Complex_run_contract *stocks[n];
      [contracts getObjects: stocks];
      scroll = [[GGScrollView alloc] 
		 initWithFrame: NSMakeRect(0, 0, frame.size.width,
					   frame.size.height)];
      [self addSubview: scroll];

      m = [GGMenu menu];
      
      for( i = 0; i < n; ++i)
	{
	  NSArray *c;
	  GGView *q;
	  c = [(id)(stocks[i]) contract];
	  q = [GGView itemWithContract: c];
	  [m addMenuItem: q]; 
	}
      [m endItem];

      [scroll setDocumentView: m];
      RELEASE(scroll);
    }  


  return self;
}

@end
