/* 	-*-ObjC-*- */
/*
 *  GGFont.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __GGFont_h
#define __GGFont_h

@class Texture;

#import <Foundation/NSObject.h>
#import <Foundation/NSGeometry.h>
#import "GGGlyphLayout.h"

@class NSMutableDictionary;
@class NSMutableData;

typedef struct __glyphInfo
{
  GLfloat 	s1, t1, s2, t2;
  int		width, height, left, top;
  int		advanceX;
  short		defined;
}GlyphInfo;

@interface GGFont : NSObject
{
  Texture 		*texture;
  struct __glyphInfo	glyphInfo[256];
  int			ascend, descend;
  int			spaceWidth;
  int 			lineSpacing;
  struct FT_FaceRec_ 	*face;

  NSMutableDictionary	*glyphs;
  NSMutableData		*buf;
  int 			currentX;
  int 			currentY;
  int 			currentHeight;


  int (*drawMethod)(id, SEL, int, NSPoint);
}
//FIXME : tres foireux
+ defaultFont;
+ defaultButtonFont;
+ defaultOptionFont;
+ defaultTitleFont;
+ defaultSmallFont;
+ fixedFont;
+ unicodeFont;


+ (void) setDefaultFont: (GGFont *) f;
+ fontWithFile: (NSString *)aString
	atSize: (int) sizeWished;
+ fontWithFile: (NSString *)aString;
- (void) selectFont;
- (void) endSelectFont;
- (int) getGlyphInfo: (InterLeavedT2V3 [4]) ilArray
	      forChar: (unichar) ascii
	    atOrigin: (NSPoint) origin;
- (int) lineSpacing;
- (Texture *) texture;
- (int) spaceWidth;
- (int) ascend;
- (int) descend;
- (int) bearingX;
- (void) drawString: (NSString *) str
	   atOrigin: (NSPoint) origin;
@end

extern NSString *DefaultFontName;

#endif
