/*
 *  Board.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSNotification.h>

#import "Board.h"
#import "GGScrollView.h"
#import "GGWidget.h"
#import "GGInput.h"
#import "stationadmin.h"
#import "Console.h"
#import "GGMenu.h"
#import "Station.h"

@implementation BoardView
+ boardView
{
  return AUTORELEASE([[self alloc] init]);
}

+ (void) openNewBoard
{
  BoardView *board;

  board = [self boardView];
  [board openIn: theGGInput];
}

- (void) _loadContract
{
  GGMenu *_menu;
  int n, i, j;
  Station *station;
  Complex_station_manager *m;

  station = [theGGInput currentStation];
  NSParameterAssert(station);

  m = [station stationManager];
  smanager = m;

  [contracts setArray: [m contractsList]];
  _menu = [GGMenu menu];


  n = [contracts count];
  
  j = 0;
  for( i = 0; i < n; i++ )
    {
      GGOption *opt;
      NSString *str;
      Complex_run_contract *hd;

      hd =  [contracts objectAtIndex: i];
      if ( [hd isAlive] )
        {
          str = [(id)hd summaryMission];


          str = [NSString stringWithFormat: @"contract %d, %@", j, str];
          opt = [GGOption optionWithStr: str
                          width: 350];
          [opt setType: GGMomentaryPushButton];
          [opt setTag: i];
          //          [opt setFrameOrigin: NSMakePoint(10, j*50 + 10)];
          [opt setTarget: self];
          [opt setAction: @selector(buildContract:)];
          [_menu addMenuItem: opt];
          j++;
        }
      else
        NSWarnMLog(@"hd is dead : %@", hd);
    }
  [_menu endItem];
  [scroll setDocumentView: _menu];
}

- initWithFrame: (NSRect) aRect
{
  [super initWithFrame: aRect];

  scroll = [[GGScrollView alloc] initWithFrame: 
				   NSMakeRect(200, 10, 
					      NSWidth(aRect)-300,
					      NSHeight(aRect)-100)];
  contracts = [NSMutableArray new];

  [self addSubview: scroll];


  RELEASE(scroll);
  [self enableTextureBackground];

  [self _loadContract];
  

  return self;
}


- (void) dealloc
{
  //  [contracts makeObjectsPerformSelector: @selector(close)];
  RELEASE(contracts);
  RELEASE(missionView);
  RELEASE(questions);
  [super dealloc];
}

- (void) buildContract: obj
{
  TopView *tv;
  GGMenu *menu;
  GGOption *opt;
  NSRect frame;
  Complex_run_contract *hd;
  int i, n;

  /*decide wether we accept the player for this mission.
    (elite rank, reputation
  */

  /*Here we should create the context of this mission.  It should be a script 
    that will wait until the right moment, then it will generate someone
    entering the target system from nowhere, this guy should go inside the
    target spatioport and wait for another timer that will tell him to leave the
    station.  At this moment, the script put some hook in the game in order to
    pay back the player when he comes back to the system where he was given the
    contract.  If the mission is a failure, the script should detect it 
    and notify when the player also.  After all of this, the script terminates.

    It should be noted that the script is not owned by anybody.  That's not a 
    GGReal problem, maybe I should implement a GC to solve all those things at 
    once.
    
  */



  hd = [contracts objectAtIndex: [obj tag]];
  ASSIGN(questions, [(id)hd descriptionMission]);
  NSParameterAssert(questions);

  n = [questions count];

  
  tv = [TopView topview];
  [[tv titleView] updateString: @"Description of the mission"];
  [tv enableTextureBackground];
  menu = [GGMenu menu];
  frame = [tv frame];

  for( i = 0; i < n; i += 2)
    {
      opt = [GGOption optionWithStr: [questions objectAtIndex: i]
		      width: frame.size.width * 2 / 3];
      [opt setType: GGMomentaryPushButton];
      [opt setTarget: self];
      [opt setAction: @selector(answerQuestion:)];
      [opt setTag: i+1];
      [menu addMenuItem: opt];
      
    }
  opt  = [GGOption optionWithStr: @"accept mission"
		   width: frame.size.width * 2 / 3];
  [opt setType: GGMomentaryPushButton];
  [opt setTarget: self];
  [opt setAction: @selector(missionAccepted:)];
  [opt setTag: [obj tag]];
  [menu addMenuItem: opt];

  [menu endItem];
  [menu setFrameOrigin: NSMakePoint(frame.size.width/3, frame.size.height/4)];
  [tv addSubview: menu];


  [tv openIn: theGGInput];
  ASSIGN(missionView, tv);
}

- (void) answerQuestion: obj
{
  consoleLog(@"%@", [questions objectAtIndex: [obj tag]]);
}

- (void) missionAccepted: obj
{
  Complex_run_contract *hd;
  NSParameterAssert(missionView);

  hd = [contracts objectAtIndex: [obj tag]];
  NSParameterAssert(hd);
  [theCommandant addContract: hd];
  [(id)hd missionAccepted];
  [smanager removeContract: hd];
  consoleLog(@"mission accepted, remove the accepted mission from the list.");
  [missionView close];
  DESTROY(missionView);
  [self _loadContract];
}
  
@end
