/* 	-*-ObjC-*- */
/*
 *  ShipWin.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ShipWin_h
#define __ShipWin_h

#import "TopView.h"

@class GGShip;
@class GGString;

typedef enum __typeShipView
{
    TypeRotation,
    TypeXY,
    TypeYZ,
    TypeZX
}TypeShipView;

@interface ShipView : GGView
{
    GGShip 	*ennemi;
    float 	angle;
    TypeShipView	typeShipView;
    GGString	*typeProjection;
    Console	*details;
}
- (void) setShip: (GGShip *)shp;
- (void) circulateTypeProjection;
- (void) setExtendedInfo: (BOOL) f;
@end


@interface ShipWindow: TopView
{
    ShipView		*shipView;
    int 		index;
}
- (void) nextChoice;
- (void) prevChoice;
- (void) reload;
@end


#endif
