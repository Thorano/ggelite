/*
 *  GGMenu.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSArray.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>

#import "GGMenu.h"
#import "GGWidget.h"
#import "NSWindow.h"
#import "GGFont.h"

@implementation GGMenu
- initWithFrame: (NSRect)frameRect
{
  [super initWithFrame: frameRect];
  items = [NSMutableArray new];
  ypos = 0;
  needUpdate = NO;
  NSDebugMLLog(@"GGMenu", @"created");
  return self;
}

+ menu
{
  GGMenu *m;
  m = [self alloc];
  return AUTORELEASE([m initWithFrame: 
			  NSMakeRect(0, 0,10, 10)]);
}

- (void) dealloc
{
  RELEASE(items);
  [super dealloc];
}

- (void) addMenuItem: (GGView *)aView
{
  [items addObject: aView];
  needUpdate = YES;
}


- (void) _addMenuItem: (GGView *)aView
{
  NSRect aRect, itemRect;
  NSSize newSize;
  GGView *opt = aView;
  [opt setFrameOrigin: NSMakePoint(5, ypos)];
  [self addSubview: opt];

  itemRect = [opt frame];
  aRect = [self frame];

  if( itemRect.size.width + 10 > aRect.size.width )
    newSize.width = itemRect.size.width+10;
  else
    newSize.width = aRect.size.width;

  ypos += 1.2*(itemRect.size.height);
  ypos = floor(ypos);

  newSize.height = ypos;
  [self setFrameSize: newSize];
  NSDebugMLLog(@"GGMenu", @"size %@", NSStringFromSize(newSize));
}

- (void) endItem
{
  GGView *aView;
  NSEnumerator *enumerator;

  enumerator = [[self subviews] reverseObjectEnumerator];

  while((aView = [enumerator nextObject]) != nil )
    {
      [aView removeFromSuperview];
    }

  enumerator = [items reverseObjectEnumerator];

  while((aView = [enumerator nextObject]) != nil )
    {
      //      NSLog(@"adding %@", aView);
      [self _addMenuItem: aView];
    }
}

// - (void) addItemWithTitle: (NSString *) str
// 		   target: (id) obj
// 		   action: (SEL) aSel
// 		      tag: (id) obj2
// {
//   GGOption *opt = [GGOption optionWithStr: str];
//   [opt setTarget: obj];
//   [opt setAction: aSel];
//   [opt setType: GGMomentaryPushButton];
//   [opt setTag: (int) obj2];
//   [self addMenuItem: opt];
// }

- (void) addItemWithTitle: (NSString *) str
		   target: (id) obj
		   action: (SEL) aSel
		      tag: (id) obj2
{
  GGBouton *bo = [GGBouton button];
  [bo setTitle: str
      withFont: [GGFont defaultOptionFont]];
  [bo setTarget: obj];
  [bo setAction: aSel];
  [bo setTag: (int) obj2];
  [bo setBorder: NO];
  [self addMenuItem: bo];
}

- (void) addItemWithTitle: (NSString *) str
		   target: (id) obj
		   action: (SEL) aSel
{
  [self addItemWithTitle: str
	target: obj
	action: aSel
	tag: nil];
}


- (BOOL) isOpaque
{
  return YES;
}

@end
