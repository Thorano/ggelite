/*
 *  GGIcon.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSArray.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSValue.h>
#import <Foundation/NSDictionary.h>


#import "GGWidget.h"
#import "Metaobj.h"
#import "NSWindow.h"
#import "utile.h"
#import "Texture.h"
#import "GGGlyphLayout.h"
#import "GGIcon.h"
#import "Resource.h"
#import "GGImage.h"

@interface Texture (IconSupport)
+ loadIconSet;
@end

#define NAMERES @"TextureIcon"

typedef struct __iconData
{
  NSRect   rect;
  Texture *tex;
}IconData;

@interface IconManagerForTexture : NSObject
{
  Texture 		*tex;
  NSMutableArray        *textures;
  NSMutableDictionary	*dic;
  int x, y;
  int curHeight;
}
+ iconManagerForTexture;
- (IconData) rectForIconName: (NSString *) name;
- (Texture *) texture;
@end

@interface NSValue (IconData)
- (IconData) iconDataValue;
+ valueWithIconData: (IconData) icd;
@end

@implementation NSValue (IconData)
- (IconData) iconDataValue
{
  IconData icd;
  [self getValue: &icd];
  return icd;
}


+ valueWithIconData: (IconData) icd
{
  return [self value: &icd
               withObjCType: @encode(IconData)];
}
@end

#define SIZETEXT 256.0

@implementation IconManagerForTexture

static IconManagerForTexture *iconManagerForTexture;

+ iconManagerForTexture;
{
  if( iconManagerForTexture == nil )
    {
      iconManagerForTexture = [[self alloc] init];
      [[Resource resourceManager] addForCleaning: self];
    }

  return iconManagerForTexture;
}

+ (void) clean
{
  DESTROY(iconManagerForTexture);
}

- (void) _makenew
{
  Texture *t;
  t = [[Texture alloc] initWithData: NULL
		   width: (int)SIZETEXT
		   height: (int)SIZETEXT
		   channels: 3
		   mipmap: NO];
  ASSIGN(tex, t);
  RELEASE(t);
  [textures addObject: t];
  x = 0;
  y = 0;
  curHeight = 0;
}

- init
{
  [super init];
  dic = [NSMutableDictionary new];
  textures = [NSMutableArray new];

  addToCheck(self);
		   
  return self;
}

- (void) dealloc
{
  RELEASE(tex);
  RELEASE(dic);
  RELEASE(textures);
  [super dealloc];
}

- (IconData) loadSubTexture:(GGImage *)image
                     format:(GLint) format
{
  IconData icd;
  NSRect theRect;
  if( x+[image width] > SIZETEXT )
    {
      x=0;
      y += curHeight;
      curHeight = 0;
    }
      
  if (tex == nil || y+[image height] > SIZETEXT ||  x+[image width] > SIZETEXT )
    {
      NSDebugMLLog(@"GGIcon", @"Nouvelle texture");
      [self _makenew];
    }

  
  [tex setSubImage:image
            format: format
           xoffset: x
           yoffset: y];

  theRect = NSMakeRect((float)x/SIZETEXT,
		       (float)y/SIZETEXT,
		       (float)[image width]/SIZETEXT,
		       (float)[image height]/SIZETEXT);
  x += [image width];
  if( [image height] > curHeight )
    curHeight = [image height];

  icd.rect = theRect;
  icd.tex = tex;

  return icd;
}

- (IconData) rectForIconName: (NSString *) name
{
  int format;
  NSValue *rect;
  IconData icd;
  NSString *fileName;
  rect = [dic objectForKey: name];
  if( rect != nil )
    {
      return [rect iconDataValue];
    }

  fileName = [[Resource resourceManager] pathForTexture: name];
  NSAssert1(fileName, @"Could not load icon file %@: File not found", name);
  GGImage *subImage = [GGImage imageFromPath:fileName];

  NSAssert1(subImage != NULL, @"texture data file %@ not found or corrupted", fileName);

  if( [subImage ncomp] == 3) 
    format = GL_RGB;
  else if ([subImage ncomp] == 4 )
    {
      format = GL_RGBA;
    }
  else{
      [NSException raise: NSInternalInconsistencyException
                  format: @"cannot handle %d components in icons", [subImage ncomp]];
      format = -1; // just to remove a warning
  }
    

  icd = [self loadSubTexture: subImage
                      format: format];

  rect = [NSValue valueWithIconData: icd];
  [dic setObject: rect
       forKey: name];


  return icd;
}

- (Texture *) texture
{
  return tex;
}
@end

@implementation GGIcon
- initWithTexture: (Texture *) text
	     rect: (NSRect) aRect
	    scale: (float) scale
{
  [super init];

  ASSIGN(texture, text);
  width = SIZETEXT*aRect.size.width*scale;
  height = SIZETEXT*aRect.size.height*scale;
  xmin = NSMinX(aRect);
  ymin = NSMaxY(aRect);
  xmax = NSMaxX(aRect);
  ymax = NSMinY(aRect);

  
  addToCheck(self);
  return self;
}

+ iconWithName: (NSString *) name
	 scale: (float) scale
{
  IconData icd;
  GGIcon *icon;
  IconManagerForTexture *im;

  if( name == nil )
    name = @"cmdr0.jpg";

  im = [IconManagerForTexture iconManagerForTexture];

  icd = [im rectForIconName: name];
  
  NSDebugMLLog(@"Icon", @"icon = %@\ntexture = %@\nrect = %@\ntexid = %d", name,
               icd.tex, NSStringFromRect(icd.rect),
               [icd.tex idTexture]);

  icon = [self alloc];
  [icon initWithTexture: icd.tex
	rect: icd.rect
	scale: scale];
  return AUTORELEASE(icon);
}

+ iconWithName: (NSString *) name
{
  return [self iconWithName: name
	       scale: 1];
}

- (void) dealloc
{
  RELEASE(texture);
  [super dealloc];
}

- (void) drawAtPoint: (NSPoint) aPoint
{
  // FIXME:  Factorise ça ailleurs que cela ne soit fait qu'une seule fois
  //FIXME : cache everything.
  glPushAttrib(GL_ENABLE_BIT | GL_TEXTURE_BIT);
  glEnable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, [texture idTexture]);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
  glBegin(GL_QUADS);

  glTexCoord2f(xmin, ymin);
  glVertex2f(aPoint.x, aPoint.y);

  glTexCoord2f(xmax, ymin);
  glVertex2f(aPoint.x+width, aPoint.y);

  glTexCoord2f(xmax, ymax);
  glVertex2f(aPoint.x+width, aPoint.y+height);

  glTexCoord2f(xmin, ymax);
  glVertex2f(aPoint.x, aPoint.y+height);
  
  glEnd();

  glPopAttrib();
}

- (NSRect) frameRect
{
  return NSMakeRect(0, 0, width, height);
}

@end
