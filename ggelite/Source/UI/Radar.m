/*
 *  Radar.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <math.h>
#import <Foundation/NSArray.h>
#import "config.h"
#import "Metaobj.h"
#import "Radar.h"
#import "GGLoop.h"
#import "World.h"
#import "GGRepere.h"
#import "Tableau.h"
#import "utile.h"

#define MAXECH 30
#define WidthRadar 90

static void drawEllipse(GLenum mode, GGReal a, GGReal b)
{
    int i;
    glBegin(mode);
    for( i = 0; i < MAXECH; ++i )
    {
        double angle = (double)i/MAXECH * 2*M_PI;
        glVertex2f(a*cos(angle), b*sin(angle));
    }
    glEnd();
}

static MapCol mymap[] = 
{
    { 10e3, {0, 0, 1, 1} },
    { 22e3, {0, 1, 0, 1} },
    { 50e3, {1, 1, 0, 1} },
    { 88e3, {1, 0, 0, 1} },
    { 135e3, {1, 0, 1, 1} },
    { 180e3, {0, 1, 1, 1} },
    { 250e3, {0.5, 0.5, 0.5, 1} },
    { 300e3, {1, 1, 1, 1} }
};

static inline void setColorFromMasse(double masse)
{
    VectCol res;
    res = genColFromMapHeight(mymap, sizeof(mymap)/sizeof(MapCol), masse);
    glColor4v(&res);
}

@implementation Radar
- (void) _compile
{
    float width, height;
    width = _frame.size.width/2;
    height = _frame.size.height/2;
    compileListId = glGenLists(1);
    glNewList(compileListId, GL_COMPILE);
    glTranslatef(width, height, 0);
    glColor3f(0.3, 0.7, 0.6);
    //      glColor3f(1, 1, 1);
    drawEllipse(GL_LINE_LOOP, width, height);
    drawEllipse(GL_POINTS, width*2/3, height*2/3);
    drawEllipse(GL_LINE_LOOP, width/2, height/2);
    glBegin(GL_POINTS);
    glColor3f(1, 1, 1);
    glVertex2f(0, 0);
    glEnd();
    glEndList();
}

- initWithFrame: (NSRect) aRect
{
    [super initWithFrame: aRect];
    [self _compile];
    
    addToCheck(self);
    
    return self;
}

- (void) dealloc
{
    glDeleteLists(compileListId, 1);
    [super dealloc]; 
}



- (void) drawShipOnRadar
{
    float xcoef, ycoef;
    GGRepere 	*node = [theWorld repere];
    NSArray	*fils = [node subNodes];
    int		n = [fils count];
    GGSpaceObject *commandant = (GGSpaceObject *)[theWorld commandant];
    xcoef = _frame.size.width/2;
    ycoef = _frame.size.height/2;
    if( n > 1 ) /*there is at least the commander*/
    {
        GGSpaceObject *objs[n];
        int i;
        
        [fils getObjects: objs];
        for( i = 0; i < n; ++i)
        {
            Vect3D	pos;
            GGReal		coef;
            GGReal		n;
            GLfloat	x, y, z;
            if( objs[i] == commandant || Masse(objs[i]) < 100 )
                continue;
            
            [objs[i] positionInRepere: [theWorld camera]
                                   in: &pos];
            n = normeVect(&pos);
            coef = atan(n/5000)/(n*(M_PI/2));
            
            y = pos.z*coef*ycoef;
            x = -pos.x*coef*xcoef;
            z = pos.y*coef*ycoef;
            
            glColor3f(1, 0, 0);
            glBegin(GL_LINES);
            glVertex2f(x, y);
            glVertex2f(x, y+z);
            glEnd();
            
            setColorFromMasse(Masse(objs[i]));
            glRecti(x-2, (y+z-2), x+2, (y+z+2));
        }
    }
}

- (void) drawRect: (NSRect) theRect
{
    glCallList(compileListId);
    [self drawShipOnRadar];
}

@end

