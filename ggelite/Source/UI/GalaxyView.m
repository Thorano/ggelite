/*
 *  GalaxyView.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: July 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>

#import "GGRepere.h"
#import "GGLoop.h"
#import "Metaobj.h"
#import "Tableau.h"
#import "Sched.h"
#import "World.h"
#import "GGWidget.h"
#import "GGShip.h"
#import "utile.h"
#import "GGInput.h"
#import "Item.h"
#import "Commandant.h"
#import "GGFont.h"
#import "ShipWin.h"
#import "GGConcreteShip.h"
#import "GalaxyView.h"
#import "Console.h"
#import "System.h"
#import "GGEventDispatcher.h"

static const double foc = 60;



@implementation GalaxyView
+ (NSString *) iconName
{
    return @"galaxie.png";
}

- (void) updateString
{
    [str updateString: [NSString stringWithFormat: @"distance: %g", distance]];
}

- (id) initWithFrame: (NSRect)frameRect
{
    GGSky *sky;
    Star *stars;
    int indexSystemCommandant;
    int n;
    [super initWithFrame: frameRect];
    //  [self enableTextureBackground];
    //  [self setState: GUI_Map + 2];
    
    
    indexSystemCommandant = [[theWorld commandant] 
        indexSystem];
    
    sky 		= [theWorld sky];
    stars 	= [sky stars: &n];
    
    [self centerView];
    
    [self setAutoresizingMask: GGNSViewWidthSizable | GGNSViewHeightSizable];
    
    [theLoop fastSchedule: self
             withSelector: @selector(updateView)
                      arg: nil];
    
    str = [[GGString alloc]
	  initWithFrame: NSMakeRect(10,10,20,20)];
    
    [str setString:@"test"
          withFont: [GGFont fixedFont]];
    
    [self addSubview: str];
    
    [self updateString];
    
    [[self titleView]
    updateString: @"Galaxy"];
    
    [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"MovementMapKeys"]];

    return self;
}


+ galaxyView
{
    GalaxyView *sv;
    sv = [[self alloc] init];
    return AUTORELEASE(sv);
}

- (void) dealloc
{
    [theLoop unSchedule: self];
    RELEASE(str);
    
    [super dealloc];
}

- (void) openIn: (GGNSWindow *)w
{
    [super openIn: w];
    [_window makeFirstResponder: self];
}


- (void) myLockFocusInRect
{
    NSRect wrect;
    
    NSAssert(_window != nil, @"bad win");
    
    wrect = [self convertRect: [self bounds] toView: nil];
    NSDebugLLog(@"GGView", @"Displaying rect \n\t%@\n\t window %p", 
                NSStringFromRect(wrect), _window);
    [_window setGLFrameInRect: wrect];
}

typedef struct __2dcoord
{
    Vect3D pos;
    BOOL	valid;
}_coord2d;

- (void) drawRect: (NSRect) _theRect
{
    Star *stars;
    int n;
    int i;
    NSRect 		theRect 	= [self bounds];
    float  		aspect 		=theRect.size.width/theRect.size.height;
    Camera 		cam;
    ProjectionParam	param = GGSetUpProjection(theRect,1,10000,foc);
    GGMatrix4 		tempMat;
    float			xci, yci;
    
    xci		= 	floor(orig.x/400)*400;
    yci 		= 	floor(orig.y/400)*400;
    
    stars = [[theWorld sky] stars: &n];
    
    
    cam.param = &param;
    
    glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_VIEWPORT_BIT);
    glDepthFunc(GL_ALWAYS);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthRange(1, 1);
    [super drawRect: _theRect];
    glPopAttrib();
    
    
    glColor3f(0.8, 0.5, 0.7);
    
    glBegin(GL_LINE_LOOP);
    glVertex2f(2,2);
    glVertex2f(theRect.size.width-2, 2);
    glVertex2f(theRect.size.width-2,theRect.size.height-2);
    glVertex2f(2,theRect.size.height-2);
    glEnd();
    
    [self myLockFocusInRect];
    
    
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_DEPTH_TEST);
    
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    gluPerspective(foc, aspect, 1, 1e20);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    transposeMatriceStrange(&modelView, &tempMat);
    glLoadMatrix(&tempMat);
    glTranslatef(-modelView.position.x, -modelView.position.y,-
                 modelView.position.z);
    
    transposeMatrice(&modelView, &cam.modelView);
    
    
    glColor3f(1,1,1);
    glBegin(GL_POINTS);
    for(i = 0; i < n; ++i)
    {
        Vect3D v;
        initVect(&v, stars[i].xd, stars[i].yd, stars[i].zd);
        if( prodScal(&v, &v) <= 1e25)
        {
            
            glVertex3fv((GGReal *)&v);
        }
    }
    glEnd();
    
    
    glPopAttrib();
    
    [_window initWindowGL];
    
}

- (void) centerView
{
    orig = GGZeroVect;
    distance = 2000;
    initWithIdentityMatrix(&modelView);
    initVect(&modelView.direction, 0, 0, 1);
    initVect(&modelView.haut, 0, 1, 0);
    normaliseMatrice(&modelView);
    addLambdaVect(&orig, -distance, &modelView.direction, &modelView.position);
}

- (void) goLeft
{
    state |= GGLeft;
}

- (void) stopGoLeft
{
    state &= ~GGLeft;
}

- (void) goRight
{
    state |= GGRight;
}

- (void) stopGoRight
{
    state &= ~GGRight;
}

- (void) goUp
{
    state |= GGUp;
}

- (void) stopGoUp
{
    state &= ~GGUp;
}

- (void) goDown
{
    state |= GGDown;
}

- (void) stopGoDown
{
    state &= ~GGDown;
}

- (void) goDeeper
{
    state |= GGStrafLeft;
}

- (void) stopGoDeeper
{
    state &= ~GGStrafLeft;
}

- (void) goHigher
{
    state |= GGStrafRight;
}

- (void) stopGoHigher
{
    state &= ~GGStrafRight;
}

- (void) zoomIn
{
    state |= GGAccelere;
}

- (void) stopZoomIn
{
    state &= ~GGAccelere;
}

- (void) zoomOut
{
    state |= GGDecelere;
}

- (void) stopZoomOut
{
    state &= ~GGDecelere;
}


- (void) updateView
{
    if( state )
    {
        if( state & GGLeft )
            orig.x += 1000*realDeltaTime;
        if( state & GGRight )
            orig.x -= 1000*realDeltaTime;
        if( state & GGDown )
            orig.y -= 1000*realDeltaTime;
        if( state & GGUp )
            orig.y += 1000*realDeltaTime;
        if( state & GGAccelere && distance > 500 )
            distance /= exp(2*realDeltaTime);
        if( state & GGDecelere )
            distance *= exp(2*realDeltaTime);
        addLambdaVect(&orig, -distance, &modelView.direction, 
                      &modelView.position);
        
        [self updateString];
    }
}

- (void) mouseDown: (Event *) pev
{
    if( buttonOfEvent(pev) == GG_BUTTON_RIGHT )
        _drag = YES;
}

- (void) mouseUp: (Event *)pev
{
    if( buttonOfEvent(pev) == GG_BUTTON_RIGHT )
        _drag = NO;
}


- (void) mouseMoved: (Event *) pev
{
    if( _drag )
    {
        NSPoint delta = mouseMotionOfEvent(pev);
        int dx = delta.x;
        int dy = delta.y;
        //      NSLog(@"dx = %d, dy = %d", dx, dy);
        //        addLambdaVect(&modelView.direction, -0.005*dx, &modelView.droite,
        //  		    &modelView.direction);
        //        addLambdaVect(&modelView.direction, -0.005*dy, &modelView.haut,
        //  		    &modelView.direction);
        addLambdaVect(&modelView.haut, -0.005*dx, &modelView.gauche,
                      &modelView.haut);
        addLambdaVect(&modelView.direction, 0.005*dy, &modelView.haut,
                      &modelView.direction);
        normaliseMatrice(&modelView);
        addLambdaVect(&orig, -distance, &modelView.direction, &modelView.position);
    }
}

@end
