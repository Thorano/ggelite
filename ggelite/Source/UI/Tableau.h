/* 	-*-ObjC-*- */
/*
 *  Tableau.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Tableau_h
#define __Tableau_h

#import "config.h"
#import "NSView.h"
#import "GGInput.h"

@class NSMutableArray;
@class GGSpaceObject;
@class GGOrbiteur;
@class Commandant;
@class GGFont;
@class Radar;
@class Console;

@interface StringArray : NSObject
{
  GGFont		*theFont;
  NSMutableString	*theString;
}
+ stringArray;
- (void) addString: (NSString *) string;
- (void) addFormat: (NSString *) format,...;
- (void) clear;
- (void) displayAt: (int) x and: (int) y;
- (BOOL) isEmpty;
- (NSString*) content;

- (void) setFont:(GGFont*)font;
@end


@interface GGTableau : NSObject
{
@public
  Commandant		*observeur;
  GGOrbiteur		*closestOrbiteur;
  GGReal			distance, speed;
  StringArray		*string;
  GGFont		*font;

  NSRect		frameRect;
  
  NSRect        targetRect;
  float			distCible;


  NSRect        destRect;
  float			distDest;

  short int		xVit, yVit;

  short			xDir, yDir;

  short		       	xVitDest, yVitDest;

  float			joyXPos, joyYPos;
  BOOL			posCibleCorrect;
  BOOL			posDestCorrect;
  BOOL			posVitCorrect;
  BOOL			posDirCorrect;
  BOOL			posVitDestCorrect;
  BOOL			destIsTarget;
}

- (void) setJoyXPos: (float) val;
- (void) setJoyYPos: (float) val;
- (void) setFrameRect: (NSRect) aRect;
- (void) drawTableau;
- (void) setClosestOrbiteur: (GGOrbiteur *)theOrb;
- (GGOrbiteur *) closestOrbiteur;
- (void) setRectCible: (NSRect) rect 
		     dist: (float) dist;
- (void) setObserveur: (GGSpaceObject *) obj;
- (void) doIndicator: (ProjectionParam *)ppp
	      camera: (GGSpaceObject *) camera;

- (GGSpaceObject *) observeur;
- (void) clean;
@end

extern GGTableau *theTableau;
@class GGTimer;
@class GGGlyphLayout;
@class GGBouton;
@class ProgressBar;

typedef struct __iconAction
{
  id obj;
  SEL action;
  NSString *icon;
}GGIconAction;

@interface ViewTableau : GGView
{
  GGGlyphLayout		*dateString;
  Console 		*theConsole;
  GGTimer		*timer;
  GGBouton		*button[10];
  ProgressBar		*fuelBar;
}
- (Console *) console;
- (void) buildIconBar: (GGIconAction *)ia
		   at: (int) start
		 size: (int) size;
@end



#endif
