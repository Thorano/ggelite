/*
 *  StarView.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: May 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>

#import "GGRepere.h"
#import "GGLoop.h"
#import "Metaobj.h"
#import "Tableau.h"
#import "Sched.h"
#import "World.h"
#import "GGWidget.h"
#import "GGShip.h"
#import "utile.h"
#import "GGInput.h"
#import "Item.h"
#import "Commandant.h"
#import "GGFont.h"
#import "ShipWin.h"
#import "GGConcreteShip.h"
#import "StarView.h"
#import "Console.h"
#import "System.h"
#import "GGEventDispatcher.h"

static const double foc = 60;

static double setColor(float mag)
{
    double val;
    if( mag < 0.1 )
        return 1.1-mag;
    val = (2/mag);
    val = (val < 0 ? 0 : val);
    if( val >= 0.6 )
        val =  sqrt(val);
    //  return val;
    return (val > 1 ? 1 : val);
}

static inline double roundClosest(double val, int prec)
{
    int s = floor(log10(val)+1);
    double tp;
    s -= prec;
    if( s <= 0 )
        return rint(val);
    else
    {
        tp = rint(pow(10.0,s));
        return tp*rint(val/tp);
    }
}

static NSString *stringOfFloat(double _val)
{
    int s;
    double val;
    if( _val <= 1 )
        return @"Unknown";
    val = roundClosest(_val, 3);
    s = (int)(floor(log10(val)+1)) / 3 + 1;
    {
        char str[5*s];
        int dec[s];
        int i, j;
        int inc;
        for(i = 0; i < s && val != 0; ++i)
        {
            double tp;
            double newval;
            newval = floor(val/1000);
            tp = 1000 * newval;
            dec[i] = val - tp;
            val = newval;
        }
        i--;
        
        inc = sprintf(str, "%d", dec[i]);
        
        for(j = i-1; j>=0; --j)
        {
            int res;
            res = sprintf(str + inc, " %03d", dec[j]);
            inc += res;
        }
        
        return [NSString stringWithUTF8String: str];
    }
}

@interface StarView (Private)
- (void) _closestChange: (int) indClosest;
- (void) update_data:(BOOL) obj;
@end

@implementation StarView
+ (NSString *) iconName
{
    return @"map3d.png";
}

- (id) initWithFrame: (NSRect)frameRect
{
    GGSky *sky;
    Star *stars;
    int indexSystemCommandant;
    double xc, yc, zc;
    [super initWithFrame: frameRect];
    [self enableTextureBackground];
    //  [self setState: GUI_Map + 1];
    
    
    
    indexSystemCommandant = [[theWorld commandant] 
        indexSystem];
    range = [[theWorld commandant] rangeForHyperJump];
    
    sky 		= [theWorld sky];
    stars 	= [sky stars: NULL];
    
    
    NSParameterAssert(indexSystemCommandant>=0);
    xc = stars[indexSystemCommandant].xd;
    yc = stars[indexSystemCommandant].yd;
    zc = stars[indexSystemCommandant].zd;
    
    initVect(&posCommandant, xc, yc, zc);
    
    fontStar 	= RETAIN([GGFont fontWithFile: DefaultFontName
                                       atSize: 18]);
    
    fontAxe	= RETAIN([GGFont fontWithFile: DefaultFontName
                                   atSize: 12]);
    
    [GGFont setDefaultFont: fontAxe];
    
    axex = RETAIN([GGGlyphLayout glyphLayoutWithString: @"[Ox)"]);
    axey = RETAIN([GGGlyphLayout glyphLayoutWithString: @"[Oy)"]);
    axez = RETAIN([GGGlyphLayout glyphLayoutWithString: @"[Oz)"]);
    
    data = gluNewQuadric();
    gluQuadricNormals(data, GLU_SMOOTH);
    gluQuadricOrientation(data, GLU_OUTSIDE);
    gluQuadricTexture(data, GL_FALSE);
    compileListId = glGenLists(1);
    glNewList(compileListId, GL_COMPILE);
    gluSphere(data, 50, 10, 10);
    glEndList();
    
    
    [self setAutoresizingMask: GGNSViewWidthSizable | GGNSViewHeightSizable];
    
    [theLoop fastSchedule: self
             withSelector: @selector(updateView)
                      arg: nil];
    
    [theLoop slowSchedule: self
             withSelector: @selector(updateZone)
                      arg: nil];
    
    fv = [[FloatingView alloc] initWithFrame: NSMakeRect(0, 0, 10, 10)];
    
    textview = [[Console alloc] initWithFrame: 
        NSMakeRect(5, 5, 0.3*frameRect.size.width,
                   0.5*frameRect.size.height)];
    [textview setFont: [GGFont fixedFont]];
    [fv setTransparency: 0.3];
    [self addSubview: fv];
    [fv addSubview: textview];
    
    [self centerView];
    [self update_data: YES];
    
    
    [[self titleView]
    updateString: @"Neighbor stars"];
    
    [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"MovementMapKeys"]];
    
    //  initWithIdentityMatrix(&essai);
    
    return self;
}

- (void) encodeWithCoder: (NSCoder *) aCoder
{
    puts("arggggg");
}

- (void) update_data: (BOOL) first
{
    StarsZone *starszone;
    GGSky *sky;
    Star *stars;
    char *names;
    int n, i, index;
    int indexSystemCommandant;
    int indexLocalSystemCommandant=-1;
    
    sky 		= [theWorld sky];
    stars 	= [sky stars: &n];
    names 	= [sky names];
    indexSystemCommandant = [[theWorld commandant] 
        indexSystem];
    
    NSParameterAssert(indexSystemCommandant>=0);
    
    starszone = [sky starsZoneForVect: &orig];
    current = starszone;
    
    
    for( i = 0; i < nStars; ++i)
        DESTROY(theStars[i].stringLayout);
    
    
    if( starszone != nil)
    {
        NSDebugMLLog(@"StarsZone", @"dans cette zone : %d", starszone->size);
        for( i = 0, index = 0; i < starszone->size && index < MAXLOCALSTAR; ++i)
        {
            Star s = stars[starszone->indices[i]];
            Vect3D v;
            initVect(&v, s.xd, s.yd, s.zd);
            if( distance2Vect(&v, &orig) < square(3000))
                //square(s.xd-xc) + square(s.yd-yc) + square(s.zd-zc) <= square(3000) )
            {
                s.pos = v;
                //	      s.pos.y  *= -1;
                s.intensity = setColor(absoluteMagnitude(&s));
                s.stringLayout = RETAIN([GGGlyphLayout glyphLayoutWithString: 
                    [NSString stringWithUTF8String:
                        names+s.index]
                                                                    withFont: fontStar]);
                theStars[index] = s;
                if( s.indexStar == indexSystemCommandant )
                    indexLocalSystemCommandant = index;
                index++;
            }
	}
        nStars = index;
        if( index >= MAXLOCALSTAR)
            NSLog(@"problem, too much local star: %d", index);
        
        if (first)
            [self _closestChange: indexLocalSystemCommandant];
        else
        {
            closest = -1;
            hasMoved = YES;
        }
    }
    else
    {
        nStars = 0;
    }
    
}

+ starView
{
    StarView *sv;
    sv = [[self alloc] init];
    return AUTORELEASE(sv);
}

- (void) dealloc
{
    int i;
    [theLoop unSchedule: self];
    for(i = 0; i < nStars; ++i)
    {
        RELEASE(theStars[i].stringLayout);
    }
    RELEASE(axex);
    RELEASE(axey);
    RELEASE(axez);
    RELEASE(textview);
    RELEASE(fv);
    
    
    glDeleteLists(compileListId, 1);
    gluDeleteQuadric(data);
    RELEASE(fontStar);
    RELEASE(fontAxe);
    [super dealloc];
}

- (void) openIn: (GGNSWindow *)w
{
    [super openIn: w];
    [_window makeFirstResponder: self];
}

- (void) _closestChange: (int) indClosest
{
    System *sys;
    GGReal newDist;
    closest = indClosest;
    [[theWorld commandant] 
    setWishSystemIndex: theStars[indClosest].indexStar];
    sys = [System systemAt: &theStars[closest]
                  archiver: nil];
    
    newDist = sqrt(distance2Vect(&posCommandant, &theStars[indClosest].pos))/100;
    
    [textview clear];
    [textview log: @"range: %.2f light-years", range];
    [textview log: @"System\n@r%s@w", [[theWorld sky] cNameOf: 
        theStars[closest].indexStar]];
    [textview log: @"distance is %g LY", newDist];
    [textview log: @"[%d, %d]", (int) floor(theStars[closest].xd/100), 
	    (int) floor(theStars[closest].yd/100)];
    [textview log: @"economic type %@", [sys systemType]];
    [textview log: @"federation: %f", [sys federation]];
    [textview log: @"empire: %f", [sys empire]];
    [textview log: @"allegeance: %@", [sys allegeance]];	
    [textview log: @"population: %@", stringOfFloat([sys population])];
    if( newDist > range )
        [textview log: @"@rOut of range@w"];
    [fv sizeToFit];
}

- (void) myLockFocusInRect
{
    NSRect wrect;
    
    NSAssert(_window != nil, @"bad");
    
    wrect = [self convertRect: [self bounds] toView: nil];
    NSDebugLLog(@"GGView", @"Displaying rect \n\t%@\n\t window %p", 
                NSStringFromRect(wrect), _window);
    [_window setGLFrameInRect: wrect];
}

typedef struct __2dcoord
{
    Vect3D pos;
    BOOL	valid;
}_coord2d;

- (void) drawRect: (NSRect) _theRect
{
    int i;
    NSRect 		theRect 	= [self bounds];
    float  		aspect 		=theRect.size.width/theRect.size.height;
    GLfloat 		light_position[] = { -1, 1, 2, 0};
    Camera 		cam;

    _coord2d		coord[MAXLOCALSTAR];
    GGMatrix4 		tempMat;
    int			indClosest=0;
    float			distClosest=0;
    float			xci, yci, zci;
    
    xci		= 	rintf(orig.x/400)*400;
    yci 		= 	rintf(orig.y/400)*400;
    zci 		= 	rintf(orig.z/4000)*4000;
    
    ProjectionParam	param = GGSetUpProjection(theRect,1, 10000,foc);
    cam.param = &param;
    
    glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_VIEWPORT_BIT);
    glDepthFunc(GL_ALWAYS);
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glDepthRange(1, 1);
    [super drawRect: _theRect];
    glPopAttrib();
    
    
    
    //    [super drawRect: _theRect];
    //    glClear (GL_DEPTH_BUFFER_BIT);
    
    
    //    glClearColor(0.1, 0.1, 0.5, 1);
    //    glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
    
    glColor3f(0.8, 0.5, 0.7);
    
    glBegin(GL_LINE_LOOP);
    glVertex2f(2,2);
    glVertex2f(theRect.size.width-2, 2);
    glVertex2f(theRect.size.width-2,theRect.size.height-2);
    glVertex2f(2,theRect.size.height-2);
    glEnd();
    
    [self myLockFocusInRect];
    
    
    //glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    
    
    glPushAttrib(GL_ENABLE_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    gluPerspective(foc, aspect, 1, 10000);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    
    transposeMatriceStrange(&modelView, &tempMat);
    glLoadMatrix(&tempMat);
    glTranslatef(-modelView.position.x, -modelView.position.y,-
                 modelView.position.z);
    
    //   {
    //     GGMatrix4 temp;
    //     transposeMatrice(&modelView, &temp);
    //     produitMatrice(&temp, &essai, &cam.modelView);
    //   }
    
    transposeMatrice(&modelView, &cam.modelView);
    
    
    glColor3f(0, 1, 0);
    glPushMatrix();
    glTranslatef(xci, yci, zci);
    glBegin(GL_LINES);
    for( i = 1; i < 6; ++i)
    {
        glVertex2f((-i)*400, -3000);
        glVertex2f((-i)*400, 3000);
        glVertex2f((+i)*400, -3000);
        glVertex2f((+i)*400, 3000);
        glVertex2f(-3000, (-i)*400);
        glVertex2f(3000, (-i)*400);
        glVertex2f(-3000, (i)*400);
        glVertex2f(3000, (i)*400);
    }
    
    glColor3f(1, 1, 0);
    glVertex2f(0, -3000);
    glVertex2f(0, 3000);
    glVertex2f(-3000, 0);
    glVertex2f(3000, 0);
    
    glEnd();
    glPopMatrix();
    glBegin(GL_LINES);
    
    glColor3f(1, 0, 0);
    for(i = 0; i < nStars; ++i)
    {
        //       Vect3D temp;
        //       transformePoint4D(&essai, &theStars[i].pos, &temp);
        
        //       glVertex2fv((GGReal *)&temp);
        //       glVertex3fv((GGReal *)&temp);
        glVertex3f(theStars[i].pos.x, theStars[i].pos.y, zci);
        glVertex3fv((GGReal*)&theStars[i].pos);
    }
    glEnd();
    
    
    glColor3f(0.7, 0.7, 0.7);
    glEnable(GL_LIGHTING);
    glEnable(GL_RESCALE_NORMAL);
    glCullFace(GL_FRONT);
    if( hasMoved )
    {
        indClosest = -1;
        distClosest = 1e20;
    }
    //  glMultMatrix(&essai);
    for(i = 0; i < nStars; ++i)
    {
        if( getWinCoordinates(&cam, &theStars[i].pos, &coord[i].pos) )
        {
            glPushMatrix();
            glTranslatef(theStars[i].pos.x, theStars[i].pos.y, theStars[i].pos.z);
            glScalef(theStars[i].intensity, theStars[i].intensity,
                     theStars[i].intensity);
            glCallList(compileListId);
            glPopMatrix();
            coord[i].valid = YES;
            
            if( hasMoved )
            {
                float tmp = fabs(coord[i].pos.x-theRect.size.width/2) + 
                fabs(coord[i].pos.y-theRect.size.height/2);
                if( tmp < distClosest )
                {
                    indClosest = i;
                    distClosest = tmp;
                }
            }
        }
        else
            coord[i].valid = NO;
    }
    
    if( hasMoved )
    {
        hasMoved = NO;
        if( indClosest != closest && indClosest >= 0 )
            [self _closestChange: indClosest];
    }
    
    glCullFace(GL_BACK);
    glDisable(GL_LIGHTING);
    pushStandardRep(theRect.size);
    [fontStar selectFont];
    glColor3f(0, 1, 0);
    glDisable(GL_DEPTH_TEST);
    for(i = 0; i < nStars; ++i)
        if( coord[i].valid && theStars[i].stringLayout != nil && 
            ( coord[i].pos.z - theStars[i].intensity  < 0. || i == closest ))
        {
            glLoadIdentity();
            glTranslatef(coord[i].pos.x+10, coord[i].pos.y, 0);
            if( i == closest )
            {
                glColor3f(1, 1, 0);
                [theStars[i].stringLayout fastDraw];
                glColor3f(0, 1, 0);
            }
            else
                [theStars[i].stringLayout fastDraw];
        }
            
            [fontStar endSelectFont];
    
    glColor3f(1, 1, 1);
    
    [fontAxe selectFont];
    draw3DstringFast(&cam, 1000, 0, 0, axex);
    draw3DstringFast(&cam, 0, 1000, 0, axey);
    draw3DstringFast(&cam, 0, 0, 1000, axez);
    [fontAxe endSelectFont];
    popStandardRep();
    
    
    glPopAttrib();
    
    [_window initWindowGL];
    
}

#pragma mark -
#pragma mark action method

- (void) centerView
{
    initVect(&orig, posCommandant.x, posCommandant.y, 0);
    distance = 2000;
    initWithIdentityMatrix(&modelView);
    initVect(&modelView.direction, 0, 0, 1);
    initVect(&modelView.haut, 0, -1, 0);
    normaliseMatrice(&modelView);
    mulScalVect(&modelView.gauche, -1, &modelView.gauche);
    addLambdaVect(&orig, -distance, &modelView.direction, &modelView.position);
}

- (void) goLeft
{
    state |= GGLeft;
}

- (void) stopGoLeft
{
    state &= ~GGLeft;
}

- (void) goRight
{
    state |= GGRight;
}

- (void) stopGoRight
{
    state &= ~GGRight;
}

- (void) goUp
{
    state |= GGUp;
}

- (void) stopGoUp
{
    state &= ~GGUp;
}

- (void) goDown
{
    state |= GGDown;
}

- (void) stopGoDown
{
    state &= ~GGDown;
}

- (void) goDeeper
{
    state |= GGStrafLeft;
}

- (void) stopGoDeeper
{
    state &= ~GGStrafLeft;
}

- (void) goHigher
{
    state |= GGStrafRight;
}

- (void) stopGoHigher
{
    state &= ~GGStrafRight;
}

- (void) zoomIn
{
    state |= GGAccelere;
}

- (void) stopZoomIn
{
    state &= ~GGAccelere;
}

- (void) zoomOut
{
    state |= GGDecelere;
}

- (void) stopZoomOut
{
    state &= ~GGDecelere;
}

- (void) _obsHasMoved
{
    int i, indClosest;
    float min = 1e20;
    indClosest = -1;
    for( i = 0; i < nStars; ++i)
    {
        float tmp = fabs(theStars[i].pos.x - orig.x) + 
        fabs(theStars[i].pos.y - orig.y);
        if( tmp < min )
        {
            indClosest = i;
            min = tmp;
        }
    }
    if( indClosest != closest )
        [self _closestChange: indClosest];
}

- (void) updateZone
{
    StarsZone *newzone = [[theWorld sky] starsZoneForVect: &orig];
    
    if(newzone != current)
    {
        [self update_data: NO];
    }
}

- (void) updateView
{
    if( state )
    {
        if( state & GGLeft )
        {
            orig.x += 1000*realDeltaTime;
            hasMoved = YES;
        }
        if( state & GGRight )
        {
            orig.x -= 1000*realDeltaTime;
            hasMoved = YES;
        }
        if( state & GGDown )
        {
            orig.y += 1000*realDeltaTime;
            hasMoved = YES;
        }
        if( state & GGUp )
        {
            orig.y -= 1000*realDeltaTime;
            hasMoved = YES;
        }
        if( state & GGStrafLeft )
        {
            orig.z += 1000*realDeltaTime;
            hasMoved = YES;
        }
        if (state & GGStrafRight )
        {
            orig.z -= 1000*realDeltaTime;
            hasMoved = YES;
        }
        if( state & GGAccelere && distance > 500 )
            distance -= 1000*realDeltaTime;
        if( state & GGDecelere )
            distance += 1000*realDeltaTime;
        addLambdaVect(&orig, -distance, &modelView.direction, 
                      &modelView.position);
    }
}

- (void) mouseDown: (Event *) pev
{
    if( buttonOfEvent(pev) == GG_BUTTON_RIGHT )
        _drag = YES;
}

- (void) mouseUp: (Event *)pev
{
    if( buttonOfEvent(pev) == GG_BUTTON_RIGHT )
        _drag = NO;
}


- (void) mouseMoved: (Event *) pev
{
    if( _drag )
    {
        NSPoint delta = mouseMotionOfEvent(pev);
        int dx = delta.x;
        int dy = delta.y;
        addLambdaVect(&modelView.haut, -0.005*dx, &modelView.gauche,
                      &modelView.haut);
        addLambdaVect(&modelView.direction, -0.005*dy, &modelView.haut,
                      &modelView.direction);
        normaliseMatrice(&modelView);
        mulScalVect(&modelView.gauche, -1, &modelView.gauche);
        
        addLambdaVect(&orig, -distance, &modelView.direction, &modelView.position);
        
        
        
        //       addLambdaVect(&essai.haut, -0.005*dx, &essai.gauche,
        // 		    &essai.haut);
        //       addLambdaVect(&essai.direction, 0.005*dy, &essai.haut,
        // 		    &essai.direction);
        //       normaliseMatrice(&essai);
        //       NSLog(@"\n%@", stringOfMatrice(&essai));
        hasMoved = YES;
    }
}

@end
