#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSValue.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSData.h>
#import "GGFont.h"
#import "GGGlyphLayout.h"
#import "Metaobj.h"
#import "Texture.h"
#import "Metaobj.h"
#import "Resource.h"

#import <ft2build.h>
#import FT_FREETYPE_H

static FT_Library library;

#import "config.h"

NSString *DefaultFontName = GGDefaultFontName;

#define Glyphs (f->glyphs)

@implementation GGFont
+ (void) initialize
{
  static int first = 1;
  FT_Error error;

  if (first )
    {
      first = 0;
      error = FT_Init_FreeType(&library);

      NSAssert(! error, @"unable to init freetype library");
    }
}

#define SIZETEXTUREFONT 256

//typedef unsigned char ligneFont[SIZETEXTUREFONT];
static inline unsigned char getBit(unsigned char *buffer, int bit)
{
  /* there must be an assembler instruction to do this job...*/
  int index = bit >> 3;
  int bitIndex = bit & 7;
  return (buffer[index]&(1<<(7-bitIndex)) ? 1 : 0);
}

static inline int log2i(unsigned int l)
{
  /*there must exist a faster way!!*/
  int i;
  for( i = 1; l > i; i <<= 1);
  return i;
}

- (GlyphInfo) _loadGliph: (int) index
{
  unsigned char temp[100][100];
  GlyphInfo gi;
  FT_UInt glyph_index;
  FT_GlyphSlot slot;
  FT_Error error;

  NSAssert(index > 0, @"bad");

  //  NSLog(@"Loading glyph index %d ('%c')", index, index);

  slot = face->glyph;
  glyph_index = FT_Get_Char_Index(face, index);
  if( glyph_index == 0)
    {
      gi.defined = -1;
    }
  else
    {
      error = FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER);
      if( ! error && slot->metrics.height != 0)
	{
	  int k, l;
	  int pitch = slot->bitmap.pitch;
	  int index = 0;
	  if( currentX + slot->bitmap.width > SIZETEXTUREFONT )
	    {
	      currentX = 0;
	      currentY += currentHeight;
	      currentHeight = 0;
	      NSAssert(currentY+slot->bitmap.rows < SIZETEXTUREFONT, 
		       @"too small texture!!");
	    }

	  if( slot->bitmap.rows > currentHeight )
	    currentHeight = slot->bitmap.rows;

	  if( slot->bitmap.pixel_mode == ft_pixel_mode_grays )
	    for( l = 0; l < slot->bitmap.rows; ++l)
	      {
		memcpy(temp[l], 
		       slot->bitmap.buffer+index,
		       slot->bitmap.width);
		index += pitch;
	      }
	  else if( slot->bitmap.pixel_mode == ft_pixel_mode_mono )
	    for( l = 0; l < slot->bitmap.rows; ++l)
	      {
		for( k = 0; k < slot->bitmap.width; ++k)
		  {
		    temp[l][k] = 
		      255*getBit(slot->bitmap.buffer+index, k);
		  }
		index += pitch;
	      }
	  else
	    NSAssert(0, @"unknown pixel mode!!");

	  gi.s1 = (float) currentX/SIZETEXTUREFONT;
	  gi.s2 = (float) (currentX+slot->bitmap.width)/SIZETEXTUREFONT;
	  gi.width = slot->bitmap.width;
	  gi.height = slot->bitmap.rows;
	  gi.left = slot->bitmap_left;
	  gi.top = slot->bitmap_top;
	  gi.t1 = (float) currentY/SIZETEXTUREFONT;
	  gi.t2 = (float) (currentY+slot->bitmap.rows)/SIZETEXTUREFONT;
	  gi.advanceX = slot->advance.x>>6;
	  gi.defined = YES;

	  glPushClientAttrib(GL_CLIENT_PIXEL_STORE_BIT);
	  glBindTexture(GL_TEXTURE_2D, [texture idTexture]);
	  glPixelStorei(GL_UNPACK_ALIGNMENT,1);
	  glPixelStorei(GL_UNPACK_ROW_LENGTH, 100);
	  glTexSubImage2D(GL_TEXTURE_2D, 0,
			  currentX, currentY,
			  slot->bitmap.width,
			  slot->bitmap.rows, GL_ALPHA, 
			  GL_UNSIGNED_BYTE, &temp[0][0]);
			  
	  glPopClientAttrib();

	  if( slot->bitmap_top > ascend )
	    {
	      ascend = slot->bitmap_top;
	    }

	  if( slot->bitmap_top-slot->bitmap.rows < descend )
	    {
	      descend = slot->bitmap_top-slot->bitmap.rows;
	    }

	  currentX += slot->bitmap.width;
	  
	}
      else
	gi.defined = -1;
    }

  if( index < 256)
    {
      glyphInfo[index] = gi;
    }
  else
    {
      int indb;
      NSNumber *num = [NSNumber numberWithInt: index];
      NSNumber *val;
      indb = [buf length] / sizeof(GlyphInfo);

      val = [NSNumber numberWithInt: indb];
      [glyphs setObject: val
	      forKey: num];

      [buf appendBytes: &gi
	   length: sizeof(gi)];
    }
  return gi;
}

- initWithFile: (NSString *) fileName
	atSize: (int) sizeWished
{
  FT_Error error;
  int widthFace, heightFace;

  [super init];

  glyphs = [NSMutableDictionary new];
  buf = [NSMutableData new];

  face = NULL;


  widthFace = sizeWished;
  heightFace = sizeWished;

  NSDebugMLLog(@"GGFont", @"opening %@ atsize : %d", fileName,
	       sizeWished);
  error = FT_New_Face( library, [fileName fileSystemRepresentation], 0, &face);
  NSAssert1(!error, @"can't open font file %@", fileName);

#ifdef DEBUG
  if( face->num_fixed_sizes > 0)
  {
      int i;
      NSDebugMLLog(@"GGFont", @"there is fixed size glyphs : %d !", 
                   face->num_fixed_sizes);
      for(i = 0; i < face->num_fixed_sizes; ++i)
          NSDebugMLLog(@"GGFont", @"(%d %d)", face->available_sizes[i].width,
                       face->available_sizes[i].height);
      widthFace = face->available_sizes[0].width;
      heightFace = face->available_sizes[0].height;
      //      NSAssert(widthFace <= 16, @"width face > 16!!");
  }
  else
    NSDebugMLLog(@"GGFont", 
		@"no fixed size found, using %d %d", widthFace, heightFace);
#endif

  error = FT_Set_Pixel_Sizes(face, widthFace, heightFace);

  if( error)
    {
      NSAssert(face->num_fixed_sizes > 0, @"can't set font size");
      widthFace = face->available_sizes[0].width;
      heightFace = face->available_sizes[0].height;
      error = FT_Set_Pixel_Sizes(face, widthFace, heightFace);
      NSAssert(!error, @"can't set font size");
    }


  currentY = 0;
  currentX = 0;
  currentHeight = 0;

  error = FT_Select_Charmap(face, ft_encoding_unicode);

  if( error )
    {
      int n;
      NSDebugMLLog(@"FontEnc", @"%d available encoding", face->num_charmaps);
      for ( n = 0; n < face->num_charmaps; n++ )
	{
	  FT_CharMap charmap;
	  charmap = face->charmaps[n];
	  NSDebugMLLog(@"FontEnc", @"%d th  %d", n, charmap->encoding_id);
	}

      NSDebugMLLog(@"FontEnc", @"choosing default encoding %@", fileName);
    }
  else
    NSDebugMLLog(@"FontEnc", @"selecting Unicode encoding for %@", fileName);


  lineSpacing = face->size->metrics.height>>6;

  lineSpacing = (float) lineSpacing*0.8;

  NSDebugMLLog(@"GGFont", @"ascend = %d descend = %d", ascend, descend);
  NSDebugMLLog(@"GGFont", @"lineSpacing = %d spaceWidth = %d", 
	      lineSpacing, spaceWidth);

  texture = [Texture alloc];
  [texture initForFont: NULL
	   width: SIZETEXTUREFONT
	   height: SIZETEXTUREFONT];

  
  //just load a few glyphs to compute some metrics
  [self _loadGliph: 'M'];
  [self _loadGliph: 'g'];
  spaceWidth = glyphInfo['M'].width;

  drawMethod = (int (*)(id, SEL, int, NSPoint))
    [self methodForSelector:   @selector(_drawGlyph:atOrigin:)];

  addToCheck(self);
  return self;
}



- (void) selectFont
{
  glPushAttrib(GL_ENABLE_BIT);
  glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  //  glBlendFunc(GL_ONE, GL_ONE);

  glBindTexture(GL_TEXTURE_2D, [texture idTexture]);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
}

- (void) endSelectFont
{
  glDisable(GL_TEXTURE_2D);
  //  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  glPopClientAttrib();
  glPopAttrib();
}

+ fontWithFile: (NSString *)aString
	atSize: (int) sizeWished
{
  GGFont 	*aFont;
  id 		obj;
  NSString	*nameOfFont;

  if( sizeWished == -1 )
    sizeWished = 18;

  nameOfFont = [aString stringByAppendingFormat: @"%d", sizeWished];
  obj = [[Resource resourceManager] resourceWithName: nameOfFont];
  if( obj != nil )
    return obj;

  aFont = [self alloc];
  [aFont initWithFile: [[Resource resourceManager] pathForFont: aString]
	 atSize: sizeWished];
  [[Resource resourceManager] addResource: aFont
		      withName: nameOfFont];
  AUTORELEASE(aFont);
  return aFont;
}

+ fontWithFile: (NSString *) aString
{
  return [self fontWithFile: aString
	       atSize: -1];
}

+ (void) setDefaultFont: (GGFont *) f
{
  NSAssert(f != nil, @"bad");
  [[Resource resourceManager] setDefaultFont: f];
}

+ defaultFont
{
  GGFont *f;
  f = [[Resource resourceManager] defaultFont];

  if( f == nil )
    [[Resource resourceManager] setDefaultFont: [self fontWithFile: DefaultFontName]];

  return [[Resource resourceManager] defaultFont];
}

+ defaultButtonFont
{
  return [self fontWithFile: DefaultFontName
	       atSize: 18];
}

+ defaultTitleFont
{
  return [self fontWithFile: DefaultFontName
	       atSize: 18];
}

+ defaultOptionFont
{
  return [self fontWithFile: DefaultFontName
	       atSize: 16];
}

+ defaultSmallFont
{
  return [self fontWithFile: DefaultFontName
	       atSize: 10];
}

+ fixedFont
{
  return [self fontWithFile: Fixed7x14];
}

+ unicodeFont
{
  return [self fontWithFile: UnicodeFontName];
}

- (void) dealloc
{
  RELEASE(texture);
  RELEASE(glyphs);
  RELEASE(buf);
  if( face != NULL )
    FT_Done_Face(face);

  [super dealloc];
}

static GlyphInfo *getGlyphInfo(GGFont *f, int c)
{
  if( c < 256 )
    {
      if( f->glyphInfo[c].defined == 0 )
	[f _loadGliph: c];

      NSCAssert(f->glyphInfo[c].defined != 0, @"bad");

      if( f->glyphInfo[c].defined == -1 )
	return NULL;
      else
	return &f->glyphInfo[c];
    }
  else
    {
      NSValue *val = [NSNumber numberWithInt: c];
      NSNumber *res = [Glyphs objectForKey: val];
      GlyphInfo *ptr;

      NSCAssert(Glyphs, @"bad");
      if( res == nil )
	{
	  [f _loadGliph: c];
	  res = [Glyphs objectForKey: val];
	  NSCAssert(res, @"bad");
	}
	  
      ptr = (GlyphInfo *)[f->buf bytes];
      return ptr + [res intValue];
    }
}

- (int) getGlyphInfo: (InterLeavedT2V3 [4]) ilArray
	     forChar: (unichar) unicode
	    atOrigin: (NSPoint) origin
{
  float s1, t1, s2, t2;
  float x1, x2, y1, y2;
  GlyphInfo *ptr;

  int ascii = (int) unicode;

  ptr = getGlyphInfo(self, ascii);
  
  if( ptr == NULL )
    return 0;

  s1 = ptr->s1;
  s2 = ptr->s2;
  t1 = ptr->t1;
  t2 = ptr->t2;

  x1 = origin.x+ptr->left;
  x2 = origin.x+ptr->left + ptr->width;
  y1 = origin.y+ptr->top - ptr->height;
  y2 = origin.y+ptr->top;

  ilArray[0].s = s1;
  ilArray[0].t = t1;
  ilArray[0].x = x1;
  ilArray[0].y = y2;
  ilArray[0].z = 0;

  ilArray[1].s = s1;
  ilArray[1].t = t2;
  ilArray[1].x = x1;
  ilArray[1].y = y1;
  ilArray[1].z = 0;

  ilArray[2].s = s2;
  ilArray[2].t = t2;
  ilArray[2].x = x2;
  ilArray[2].y = y1;
  ilArray[2].z = 0;

  ilArray[3].s = s2;
  ilArray[3].t = t1;
  ilArray[3].x = x2;
  ilArray[3].y = y2;
  ilArray[3].z = 0;

  return ptr->advanceX;

}

- (Texture *) texture
{
  return texture;
}

- (int) lineSpacing
{
  //FIXME : should have a better line spacing
  return lineSpacing;
  //return ascend;
}

- (int) spaceWidth
{
  return spaceWidth;
}

- (int) ascend
{
  return ascend;
}

- (int) descend
{
  return descend;
}

- (int) bearingX
{
  return glyphInfo['M'].left;
}

- (int) _drawGlyph: (int) ascii
	  atOrigin: (NSPoint) origin
{
  GlyphInfo *ptr;
  float s1, t1, s2, t2;
  float x1, x2, y1, y2;

  ptr = getGlyphInfo(self, ascii);

  if( ptr == NULL )
    return 0;


  s1 = ptr->s1;
  s2 = ptr->s2;
  t1 = ptr->t1;
  t2 = ptr->t2;

  x1 = origin.x+ptr->left;
  x2 = origin.x+ptr->left + ptr->width;
  y1 = origin.y+ptr->top - ptr->height;
  y2 = origin.y+ptr->top;
  
  glBegin(GL_QUADS);
  glTexCoord2f(s1, t1);
  glVertex2f(x1, y2);

  glTexCoord2f(s1, t2);
  glVertex2f(x1, y1);

  glTexCoord2f(s2, t2);
  glVertex2f(x2, y1);


  glTexCoord2f(s2, t1);
  glVertex2f(x2, y2);
  glEnd();

  return ptr->advanceX;
}

static inline BOOL changeAttributeWithChar(unsigned char c)
{
  switch(c)
    {
    case 'b':
      glColor3f(0, 0, 1);
      break;
    case 'r':
      glColor3f(1, 0, 0);
      break;
    case 'w':
      glColor3f(1, 1, 1);
      break;
    case 'B':
      glColor3f(0, 0, 0);
      break;
    case 'y':
      glColor3f(1, 1, 0);
      break;
    case 'g':
      glColor3f(0, 1, 0);
      break;
    case '@':
      return 0;
    default:
      return -1;
    }
  return 1;
}

- (void) drawString: (NSString *) str
	   atOrigin: (NSPoint) origin
{
  int 			strlength = [str length];
  unichar 		cStr[strlength];
  int 			i;
  int			tmp;
  NSPoint 		current;
  SEL			s = @selector(_drawGlyph:atOrigin:);

  [str getCharacters: cStr];

  current = origin;

  [self selectFont];
  for(i = 0; i < strlength; ++i)
    {
      int width;
      switch(cStr[i])
	{
	case  '\n':
	  current.y -= [self lineSpacing];
	  current.x = origin.x;
	  break;
	case ' ':
	  current.x += [self spaceWidth];
	  break;
	case '@':
	  tmp = changeAttributeWithChar(cStr[i+1]);
	  if( tmp == 1 ) /* standard escapement*/
	    {
	      i++;
	      break;
	    }
	  else if ( tmp == 0 ) /* '@' escaped */
	    {
	      i++;  // but continue to next case
	    }
	  /* unknow, print '@', continue to next case
	   */

	default:
	  width = (*drawMethod)(self, s, cStr[i], current);
	  /*	  width = [self _drawGlyph: cStr[i]
			atOrigin: current];*/
	  //	  NSAssert(width > 0, @"char with width 0");
      
	  current.x += width;
	  break;
	}
    }

  
  [self endSelectFont];

}

- (void) bleuargh
{
  glPushAttrib(GL_ENABLE_BIT);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);

  glBindTexture(GL_TEXTURE_2D, [ texture  idTexture]);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);

  glBegin(GL_QUADS);

  glTexCoord2f(0, 1);
  glVertex2f(0, 0);

  glTexCoord2f(1, 1);
  glVertex2f(SIZETEXTUREFONT, 0);
  glTexCoord2f(1, 0);
  glVertex2f(SIZETEXTUREFONT, SIZETEXTUREFONT);
  glTexCoord2f(0, 0);
  glVertex2f(0, SIZETEXTUREFONT);
  glEnd();

  glDisable(GL_TEXTURE_2D);
  glPopAttrib();
}

@end
