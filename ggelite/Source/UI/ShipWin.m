/*
 *  ShipWin.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: April 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>

#import "GGRepere.h"
#import "GGLoop.h"
#import "Metaobj.h"
#import "Tableau.h"
#import "Sched.h"
#import "World.h"
#import "GGWidget.h"
#import "GGShip.h"
#import "utile.h"
#import "Item.h"
#import "Commandant.h"
#import "GGFont.h"
#import "ShipWin.h"
#import "GGConcreteShip.h"
#import "NSWindow.h"
#import "Resource.h"
#import "Texture.h"
#import "Console.h"
#import "GGEventDispatcher.h"


static const float scaleFactor = ScaleFactorForShipView;

@interface ShipWindow ()
- (int) loadAtIndex: (int) ind;

@end


@implementation ShipWindow
- init
{
    NSRect frame;
    [super init];
    //  [self setOpacity: 1.0];
    
    frame = [self frame];
    
    [self enableTextureBackground];
    
    shipView = [[ShipView alloc]
	       initWithFrame: NSMakeRect(10, 50, frame.size.width-20, 
                                     frame.size.height-70)];
    [shipView setExtendedInfo: YES];
    [self addSubview: shipView];
    //  [self addCloseButton];
    //  [self makeFirstResponder: shipView];
    [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"DebugShipViewKeys"]];
    [self loadAtIndex:5];
    
    return self;
}

- (int) loadAtIndex: (int) ind
{
    int size;
    GGShip *bis;  
    NSArray *ships = [[Resource resourceManager] ships];
    size = [ships count];
    if(ind >= size)
        ind = size-1;
    if(ind < 0 )
        ind = 0;
    bis = [GGShip shipWithFileName: [ships objectAtIndex: ind]];
    [shipView setShip: bis];
    return size;
}

- (void) nextChoice
{
    int size;
    index++;
    size = [self loadAtIndex: index];
    if (index >= size)
        index = -1;
}


- (void) prevChoice
{
    int size;
    index--;
    size = [self loadAtIndex: index];
    if (index <= 0)
        index = size;
}

- (void) reload
{
    [self loadAtIndex: index];
}

- (void) openIn: (GGNSWindow *)w
{
    [super openIn: w];
    NSAssert(_window != nil, @"bad");
    [_window makeFirstResponder: shipView];
}

- (void) dealloc
{
    RELEASE(shipView);
    [super dealloc];
}

@end

@interface ShipView()

- (void) rasterizeWithOffset:(double)offset;

@end


@implementation ShipView
- (id) initWithFrame: (NSRect)frameRect
{
    [super initWithFrame: frameRect];
    
    ennemi = nil;
    angle = 0;
    typeShipView = TypeRotation;
    
    //  [self nextChoice];
    typeProjection = [GGString new];
    [typeProjection setString: @"Rotation"
                     withFont: [GGFont fontWithFile: DefaultFontName
                                             atSize: 10]];
    [typeProjection setFrameOrigin: NSMakePoint(20, 20)];
    
    [self addSubview: typeProjection];
    
    [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"ShipViewKeys"]];
    return self;
}

- (void) dealloc
{
    RELEASE(typeProjection);
    RELEASE(ennemi);
    RELEASE(details);
    [super dealloc];
}

- (void) setExtendedInfo: (BOOL) f
{
    if(f)
    {
        if(details)
            return;
        details = [[Console alloc] initWithFrame:
            NSMakeRect(10,10, 500,200)];	
        [details setFont: [GGFont fixedFont]];
        [self addSubview: details];
    }
    else if(details)
    {
        [details removeFromSuperview];
        DESTROY(details);
    }
}


- (void) setShip: (GGShip *)shp
{
    ASSIGN(ennemi, shp);
    [ennemi setVisible: YES];
    if( details) 
        [ennemi logData: details];
}

- (void) myLockFocusInRect
{
    NSRect wrect;
    
    NSAssert(_window != nil, @"bad");
    
    wrect = [self convertRect: [self bounds] toView: nil];
    NSDebugLLog(@"GGView", @"Displaying rect \n\t%@\n\t window %p", 
                NSStringFromRect(wrect), _window);
    [_window setGLFrameInRect: wrect];
}

- (void) drawRect: (NSRect) _theRect
{
    NSRect theRect = [self frame];
    float  aspect = theRect.size.width / theRect.size.height;
    
    glColor3f(0.8, 0.5, 0.7);
    
    glBegin(GL_LINE_LOOP);
    glVertex2f(2,2);
    glVertex2f(theRect.size.width-2, 2);
    glVertex2f(theRect.size.width-2,theRect.size.height-2);
    glVertex2f(2,theRect.size.height-2);
    glEnd();
    
    if( ennemi == nil )
        return;
    [self myLockFocusInRect];
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    if( typeShipView == TypeRotation )
        gluPerspective(60, aspect, Dimension(ennemi), 10*Dimension(ennemi));
    else
        glOrtho(-aspect * scaleFactor*Dimension(ennemi), 
                aspect*scaleFactor*Dimension(ennemi),
                0-scaleFactor*Dimension(ennemi), scaleFactor*Dimension(ennemi), 
                Dimension(ennemi), 10*Dimension(ennemi));
    angle += realDeltaTime*10;
    glColorMask(1, 0, 0, 0);
    [self rasterizeWithOffset:-1];
    glColorMask(0, 1, 1, 0);
    [self rasterizeWithOffset:1];
    glColorMask(1, 1, 1, 1);

    [_window initWindowGL];
    GLCHECK;
}

- (void) rasterizeWithOffset:(double)offset 
{
    GLfloat light_position[] = { -1, 1, 2, 0};
    
    glDepthMask(GL_TRUE);
    glClear(GL_DEPTH_BUFFER_BIT);
    
    //glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    //  glEnable(GL_CULL_FACE);
    
    
    
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    switch( typeShipView )
    {
        case TypeXY:
            gluLookAt( 0, 0, -3*Dimension(ennemi), 
                       0, 0, 0, 
                       0, 1, 0);
            break;
        case TypeYZ:
            gluLookAt(-3*Dimension(ennemi), 0, 0,
                      0, 0, 0, 
                      0, 0, 1);
            break;
        case TypeRotation:
            gluLookAt( -3*Dimension(ennemi), 0, offset*Dimension(ennemi)/12,
                       0, 0, 0, // offset*Dimension(ennemi)/20, 
                       0, 1, 0);
            break;
        case TypeZX:
            gluLookAt( 0, 3*Dimension(ennemi), 0,
                       0, 0, 0, 
                       0, 0, 1);
            break;
    }
    
    
    if( typeShipView == TypeRotation )
    {
        glRotatef(angle, sin(angle/50), cos(angle/50), 0);
    }
    
    Camera cam;
    bzero(&cam, sizeof(cam));
    ProjectionParam pp = GGSetUpProjection([self bounds],Dimension(ennemi), 10*Dimension(ennemi),60);
    pp.ignoreClip = YES;

    cam.param = &pp;
    
    
    [ennemi drawWithCameraNoTransform: &cam
                                 from: nil];
    GLCHECK;
    
    glDisable(GL_LIGHTING);
    GLCHECK;
    
    
}

#ifdef DEBUG
- (void) mouseDown: (Event *) pev
{
    if( wizard )
    {
        double a, b;
        NSPoint pt = mouseLocationOfEvent(pev);
        NSPoint res = [self convertPoint: pt
                                fromView: nil];
        NSRect frame = [self frame];
        
        if( typeShipView == TypeRotation )
            return;
        
        a = 2*(res.x - frame.size.width/2.0) *
            Dimension(ennemi)*scaleFactor / frame.size.height;
        b = 2*(res.y - frame.size.height/2.0) / frame.size.height*
            Dimension(ennemi)*scaleFactor;
        
        NSLog(@"(%g, %g)", a, b);
    }
}
#endif

- (void) circulateTypeProjection
{
    switch(typeShipView)
    {
        case TypeRotation:
            typeShipView = TypeXY;
            [typeProjection updateString: @"XY"];
            break;
        case TypeXY:
            typeShipView = TypeYZ;
            [typeProjection updateString: @"YZ"];
            break;
        case TypeYZ:
            typeShipView = TypeZX;
            [typeProjection updateString: @"ZX"];
            break;
        case TypeZX:
            typeShipView = TypeRotation;
            [typeProjection updateString: @"Rotation"];
            break;
    }
}

@end

