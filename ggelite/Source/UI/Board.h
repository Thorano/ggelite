/* 	-*-ObjC-*- */
/*
 *  Board.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Board_h
#define __Board_h

#import "TopView.h"

GGBEGINDEC
@class Texture;
@class GGShip;
@class NSDictionary;
@class GGMenu;
@class GGScrollView;

/*
@interface Contract : NSObject
{
@public
  int		indexSystem;
  int		indexPort;
  double	date;
  NSString	*nameShip;
  NSDictionary  *dicoShip;
}
@end
*/
GGENDDEC


@interface BoardView : TopView
{
  TopView       *missionView;
  NSMutableArray *contracts;
  NSArray       *questions;
  id            smanager;  //NOT RETAINED
  //  GGMenu        *_menu;  // RETAINED
  GGScrollView  *scroll; // NOT RETAINED
}
+ boardView;
+ (void) openNewBoard;
@end

#endif
