/* 	-*-ObjC-*- */
#ifndef __Utile_h
#define __Utile_h

#import <math.h>
#import "GG3D.h"
#import "stdutile.h"


GGBEGINDEC
/*%%%
import stdutile ;
%%%*/


GGENDDEC

void displayOnGGEliteConsole(NSString* string, ...);

static inline double square(double val)
{
  return val*val;
}

static inline float GGClamp(float x)
{
  if( x > 1 )
    return 1;
  if (x < -1 )
    return -1;
  return x;
}

/*random variable :
 */

static inline double random_var(double mu, double sigma)
{
  return mu*(atanh(vrnd())*sigma + 1);
}

static inline unsigned nrnd(int i, int j)
{
  return i+irnd(j-i+1);
}

static inline BOOL proba(float f)
{
  return vrnd() <= f;
}





#ifndef M_PI
#define M_PI 3.1415926535
#endif

double ggmod(double x, double y);

static inline double to2pi(double x)
{
  return ggmod(x, 2*M_PI);
}

void writeRGBTofile(int width, int height, const unsigned char *data);

struct __star;
float absoluteMagnitude(struct __star *s);
GGReal omegaOrbite(GGReal radius, GGReal orbiteRadius);

void drawRectBorder(short int x1, short int y1, short int x2, short int y2);
static inline void drawRectBorderWithRect(NSRect aRect){drawRectBorder((short int)NSMinX(aRect), (short int)NSMinY(aRect), (short int)NSMaxX(aRect), (short int)NSMaxY(aRect));}

static inline ProjectionParam GGSetUpProjection(NSRect rect, float zNear, float zFar, float fovy){
    ProjectionParam pp;
    memset(&pp,0,sizeof(pp));
    pp.fovy = fovy;
    pp.x = rect.origin.x;
    pp.y = rect.origin.y;
    pp.width = rect.size.width;
    pp.height = rect.size.height;
    pp.zmin = zNear;
    pp.zmax = zFar;
    pp.ratio = rect.size.width/rect.size.height;
    pp.yFoc =  tan((double)fovy/2/180*M_PI);
    pp.xFoc = pp.ratio*pp.yFoc;
    return pp;
}

void GGMultProjectionWithParam(ProjectionParam *param);
void GGSetupGLProjectionWithParam(ProjectionParam *param);

#pragma mark -
#pragma mark Memory stuff

#define AllocType(size, type) (type*)malloc((size)*sizeof(type))
#define ReallocType(ptr, newsize, type) (type*)realloc(ptr, (newsize)*sizeof(type))
#ifdef DebugMem
void *__ggalloc(int size, char *chaine);
void checkMem(void);
#define ggalloc(size) __ggalloc((size), __FUNCTION__)
#else
void *ggalloc(int size);
#define checkMem()
#endif

#define ggabort() do{\
    fprintf(stderr, "will crash prog in %s\n%s line %d\n", __FILE__, __func__, __LINE__);\
    __ggabort();}while(0)
    

void __ggabort();
void ggretain(void *ptr);
void ggrelease(void *ptr);
void ggSetDestructor(void *ptr, void (*f)(void *));
#define ggdestroy(ptr) \
do{\
void *p;\
p = (ptr);\
ggrelease(p);\
(ptr) = NULL;}while(0)

#pragma mark -

typedef struct __mapcol
{
  float 	val;
  VectCol	couleur;
}MapCol;

VectCol genColFromMapHeight(const MapCol map[], int size, float param);


static MapCol __mapCol[] = 
{
  { 0, {  0, 0, 0.5, 1}},
  { 1, {  0, 0.6, 0, 1}},
  { 300, {  0.5, 0.5, 0.2, 1}},
  { 600, {  0.8, 0.5, 0.5, 1}},
  { 800, { 0.7, 0.7, 0.7, 1}}};

static inline VectCol genColFromHeight(float hight)
{
  return genColFromMapHeight(__mapCol, sizeof(__mapCol)/sizeof(MapCol), hight);
}
/*
unsigned char *rawImageFromJPGFileMalloc(const char *fileName, int *pwidth,
					 int *pheight, int *pncomp);
*/

void ggPlotBoxCol(Vect3D pts[8], VectCol col);
void ggPlotBox(Vect3D pts[8]);

#ifdef __OBJC__


@class GGSpaceObject;
@class GGGlyphLayout;
#import <Foundation/NSGeometry.h>
#import "GGArchiverExtension.h"

GGReal distanceAbsolu2(GGSpaceObject *o1, GGSpaceObject *o2);
void logArray(NSArray *array);


void printstring(void *font, const char *string);
void GGPrintString(void *font, NSString *str);
void pushStandardRep(NSSize size);
void popStandardRep();
void makeFrustum(Frustum *f, Camera *pcam);

GGBEGINDEC
NSString *stringFromGameDate(double val);
static inline GGReal distanceAbsolu(GGSpaceObject *o1, GGSpaceObject *o2);
GGENDDEC


static inline GGReal distanceAbsolu(GGSpaceObject *o1, GGSpaceObject *o2)
{
  return sqrt(distanceAbsolu2(o1, o2));
}

BOOL getWinCoordinates(const Camera *c, const Vect3D *vec, Vect3D *res);
NSRect frameForBoxInCamera(const Camera *c, GGBox box);
NSRect frameForBallInCamera(const Camera* c, Vect3D center, GGReal radius);
void draw3DstringFast(Camera *c, GGReal x, GGReal y, GGReal z, GGGlyphLayout *str);

void printGLError(id);
#ifdef DEBUG
#define GLCHECK do{\
    if ( GSDebugSet(@"GLCHECK") ){\
        printGLError(nil);\
    }}while(0)
#else
#define GLCHECK
#endif
#endif

#ifdef DEBUG
#define GGCondition(condition)			\
    do{\
    if( !(condition) ){\
       fprintf(stderr, "Condition %s failed in file %s line %d\n", #condition, \
       __FILE__, __LINE__); ggabort();}}while(0)

#define GGWarn(condition)			\
    do{\
    if( (condition) )\
       fprintf(stderr, "Condition %s failed in file %s line %d\n", #condition, \
       __FILE__, __LINE__); }while(0)
#else
#define GGCondition(condition)
#define GGWarn(condition)
#endif


#endif
