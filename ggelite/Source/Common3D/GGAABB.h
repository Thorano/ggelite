//
//  GGAABB.h
//  elite
//
//  Created by Frédéric De Jaeger on 6/25/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#ifndef __GGAABB_H
#define __GGAABB_H 1
#import "GG3D.h"

#define SPACE_DIM               3
#define TWO_POWER_SPACE_DIM     (2*2*2)

typedef struct __BoundingBox
{
    GGReal      min[SPACE_DIM];
    GGReal      max[SPACE_DIM];
}BoundingBox;

extern BoundingBox ZeroBox;

static inline void AABB_initWithMinMax(BoundingBox* box, Vect3D min, Vect3D max){
    box->min[0] = min.x;
    box->min[1] = min.y;
    box->min[2] = min.z;


    box->max[0] = max.x;
    box->max[1] = max.y;
    box->max[2] = max.z;
}

void ggTransformBox(BoundingBox *boxIn, GGMatrix4 *mat, BoundingBox *boxOut);
void AABB_enlarge(BoundingBox *box, float fact);
static inline void extendDomainByArray(BoundingBox* box, const GGReal  pointAsArray[SPACE_DIM])
{
    int i;
    
    for(i = 0; i < SPACE_DIM; ++i){
        if(box->min[i] > pointAsArray[i]){
            box->min[i] = pointAsArray[i];
        }
        if(box->max[i] < pointAsArray[i]){
            box->max[i] = pointAsArray[i];
        }
    }
}

static inline void extendDomainByPoint(BoundingBox* box, Vect3D point)
{
    pvect pointAsArray = ggVect3DToArray(&point);
    extendDomainByArray(box, pointAsArray);
}

static inline void AABB_extendBox(BoundingBox* boxInOut, const BoundingBox* otherBox)
{
    extendDomainByArray(boxInOut,  otherBox->min);
    extendDomainByArray(boxInOut,  otherBox->max);
}

static inline BOOL AABB_intersect(const BoundingBox box1,  const BoundingBox box2)
{
    int i;
    for(i = 0; i < SPACE_DIM; ++i){
        if(box1.min[i] > box2.min[i]){
            if(box2.max[i] < box1.min[i]){
                return NO;
            }
        }
        else{
            if(box1.max[i] < box2.min[i]){
                return NO;
            }
            
        }
    }
    
    return YES;
}

static inline BOOL AABB_contains(BoundingBox box, Vect3D point)
{
    pvect pointAsArray = ggVect3DToArray(&point);
    int i;
    for(i = 0; i < SPACE_DIM; ++i){
        if(pointAsArray[i] < box.min[i] || pointAsArray[i] > box.max[i]){
            return NO;
        }
    }
    return YES;
}

static inline void AABB_GetMinMax(BoundingBox* box, Vect3D* pMin, Vect3D* pMax)
{
    if(pMin){
        pMin->x = box->min[0];
        pMin->y = box->min[1];
        pMin->z = box->min[2];
    }
    
    if(pMax){
        pMax->x = box->max[0];
        pMax->y = box->max[1];
        pMax->z = box->max[2];
    }
}

#define SigXBelow 0x1
#define SigXIn    0x2
#define SigXAbove 0x4

#define SigYBelow 0x8
#define SigYIn    0x10
#define SigYAbove 0x20

#define SigZBelow 0x40
#define SigZIn    0x80
#define SigZAbove 0x100

#define SigInBox (SigXIn | SigYIn | SigZIn)
#define SigOutBox (SigXBelow | SigXAbove | SigYBelow | SigYAbove | SigZBelow | SigZAbove )
static inline uint16_t AABB_SignatureOfPoint(const BoundingBox* box, Vect3D v)
{
    uint16_t ret = 0; 
    if(v.x <= box->min[0]){
        ret = SigXBelow;
    }
    else if(v.x <= box->max[0]){
        ret = SigXIn;
    }
    else{
        ret = SigXAbove;
    }
    
    if(v.y <= box->min[1]){
        ret |= SigYBelow;
    }
    else if(v.y <= box->max[1]){
        ret |= SigYIn;
    }
    else{
        ret |= SigYAbove;
    }

    if(v.z <= box->min[2]){
        ret |= SigZBelow;
    }
    else if(v.z <= box->max[2]){
        ret |= SigZIn;
    }
    else{
        ret |= SigZAbove;
    }    
    
    return ret;
}

BOOL AABB_intersectPlane(const BoundingBox* box, GGPlan p);
BOOL AABB_intersectTriangle(const BoundingBox* box, Vect3D *triangle);

#endif