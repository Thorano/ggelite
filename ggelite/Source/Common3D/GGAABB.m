//
//  GGAABB.m
//  elite
//
//  Created by Frédéric De Jaeger on 6/25/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGAABB.h"

BoundingBox ZeroBox = {
{0, 0, 0},
{0, 0, 0},
};


void AABB_enlarge(BoundingBox *box, float fact)
{
    int i;
    for(i = 0; i < SPACE_DIM; ++i){
        GGReal diff = (box->max[i]-box->min[i])*fact/2.0;
        box->min[i] -= diff;
        box->max[i] += diff;
    }
}

void ggTransformBox(BoundingBox *boxIn, GGMatrix4 *mat, BoundingBox *boxOut)
{
    int i;
    for(i = 0; i < TWO_POWER_SPACE_DIM; ++i){
        int j;
        GGReal pos[SPACE_DIM];
        for(j = 0; j < SPACE_DIM; ++j){
            pos[j] = (i & (1<<j)) == 0 ? boxIn->min[j] : boxIn->max[j];
        }
        Vect3D transformPos;
        transformePoint4D(mat,ggArrayToVect3d(pos),&transformPos);
        
        if(i == 0){
            AABB_initWithMinMax(boxOut,transformPos,transformPos);
        }
        else{
            extendDomainByPoint(boxOut, transformPos);
        }
    }
}

BOOL AABB_intersectPlane(const BoundingBox* box, GGPlan p)
{
    // XXX can be optimized a lot.
    int i;
    BOOL flag = pointInPlane(&p,ggArrayToVect3d(box->min));
    for(i = 1; i < TWO_POWER_SPACE_DIM; ++i){
        int j;
        GGReal pos[SPACE_DIM];
        for(j = 0; j < SPACE_DIM; ++j){
            pos[j] = (i & (1<<j)) == 0 ? box->min[j] : box->max[j];
        }
        if(pointInPlane(&p, ggArrayToVect3d(pos)) != flag){
            return YES;
        }
    }
    return NO;
}

BOOL AABB_intersectTriangle(const BoundingBox* box, Vect3D *triangle)
{
    if((AABB_SignatureOfPoint(box, triangle[0]) & AABB_SignatureOfPoint(box, triangle[1]) & AABB_SignatureOfPoint(box, triangle[2]) & SigOutBox) != 0){
        return NO;
    }
    else{
        GGPlan p = mkPlanFromTriangles(triangle[0],triangle[1],triangle[2]);
        return AABB_intersectPlane(box, p);
    }
}