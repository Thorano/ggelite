//
//  GGSmallPlanete.h
//  elite
//
//  Created by Frédéric De Jaeger on 27/12/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGMobile.h"

@class GGBlockContext;
@class GGBlock;
@class GGProceduralLandscape;
@interface GGSmallPlanete : GGMobile {
    GGBlockContext*         _context;
    GGProceduralLandscape   *_landscape;
}

@end
