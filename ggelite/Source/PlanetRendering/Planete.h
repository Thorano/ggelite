/* 	-*-ObjC-*- */
/*
 *  Planete.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Modele_h
#define __Modele_h

#import "GGOrbiteur.h"

@class LocalPlanetBlock;
@class GGBlock;
@class GGBlockContext;

@interface ProtoPlanete : GGOrbiteur
{
  GGBlockContext *		_context;
  LocalPlanetBlock	*lb;  //not retained
  NSString		*textureName;
  Vect3D            	col;
  BOOL               	lightSource;
}
+ (GGBlockContext*) defaultContextForDebugging;
- (GGBlockContext*) blockContext;
- (NSArray*) makeTopLevelBlocks;
- (void) setLightSource: (BOOL) val;
- (void) setTexture: (Texture *)t;
- (void) drawSimple;

/* some private stuff */
- (void) _setupLocalPart;
- (void) _removeLocalPart;


#ifdef DEBUG
+ (void) reload;
+ (void) reloadTexture;
#endif
@end

@class GGNoise;
@interface Planete : ProtoPlanete
{
  GGReal          	radius;
    GGNoise         *_noise;
}
- (void) setRadius:(double)val;
@end

@interface Torus : ProtoPlanete
{
  GGReal          	radius;
}
@end

@class GGProceduralLandscape;
@interface Experience : Planete
{
    GGProceduralLandscape*          _landscape;
}
- (GGProceduralLandscape*) landscape;
@end

@interface LocalPlanetBlock : GGSpaceObject
{
    GGSolid*        _helper;
    GGBlockContext *context;
    Vect3D	col;
}
- (id) initWithBlockContext:(GGBlockContext *) _c;
+ localBlock: (GGBlockContext *) _c;

- (Vect3D)col;
- (void)setCol:(Vect3D)value;

@end

#if defined(DEBUG) && defined(HAVE_COCOA)
#import "GGDebugController.h"

@interface GGDebugController (Bump)
- (ProtoPlanete*) currentPlanete;
- (void) setCurrentPlanete:(ProtoPlanete*)value;
@end
#endif

#endif
