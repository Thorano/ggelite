/* 	-*-ObjC-*- */
/*
 *  GGOrbiteur.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGOrbiteur_h
#define __GGOrbiteur_h

#import "GGSpaceObject.h"
#import "GGNode.h"


@class Texture;
@class NSArray;
@class System;

GGBEGINDEC

typedef struct __parametresOrbitaux
{
  /*classical orbital elements :
   */
  GGReal			N; //longitude of the ascending node
  GGReal			i; //inclination to the ecliptic
  GGReal			w; //argument of the perihelion
  GGReal			a; //semi-major axis
  GGReal			e; //excentricity
  GGReal			M; //mean anomaly (at t = 0)

  GGReal			omegaOrbite;	// speed angulaire orbitale

  //  GGReal			orbiteRadius;	// rayon orbitale
  //  GGReal			l0;		// phase à t=0
  //  GGReal			l;		// phase courante

  /*intrinsic rotation of the body 
   */

  GGReal			inclinaison1;
  GGReal			inclinaison2;
  GGReal			omegaPropre;	// speed angulaire de révolution propre
  //  GGReal			phi;		// phase courante de révolution propre
}ParametresOrbitaux;

typedef enum __GGTypeOrbiteur
{
  GGStandard,
  GGLocal,
  GGLocalStrong,
}GGtypeOrbiteur;



@interface GGOrbiteur : GGSpaceObject
{
  //  Matrice4D	cacheRep;
  GGOrbiteur    	*pere;
  NSMutableArray	*fils;
  GGReal			tailleSysteme;
  /* paramètres orbitaux autour du père :
   * Les orbites sont toutes dans le plan de l'écliptique.
   * et circulaire avec le père au centre du cercle.
   */

  ParametresOrbitaux	orbite;

  Vect3D		_nord, _haut, _droit;
  Vect3D		i0, j0;
  GGReal			b;  //minor axes
  GGtypeOrbiteur	typeOrbiteur;
  BOOL			proche;
  BOOL			exactCalcul;
  @public
  System		*surroundingSystem;
  GGReal			distanceCamera2;
  GGReal			distanceLocalOrbiteur;
  GGReal			distanceGlobalOrbiteur;
  GGNode		*repereTranslation; //not retained
  GGSpaceObject		*repereRotatif;     //not retained
}
- init;  //designated initializer
- initSystemWithContentOfFile: (NSString *)fileName;
- initRandomSystemForStar: (System *)s;
- initWithDictionary: (NSDictionary *)dic
		name: (NSString *)_name;

- (void) setWithDictionary: (NSDictionary *)dic
		name: (NSString *)_name;
- setRandomSystemForStar: (System *)s;

- (GGReal) tailleSysteme;
- (NSArray *) getAllOrbiteur;
- (void) addSon: (GGOrbiteur *)aSon;
- (NSArray *)sons;
- (GGOrbiteur *) orbiteurPere;
- (GGOrbiteur *) visibleAncestor;

- (BOOL) isOriginOfNode;


- (BOOL) lightSource;

- (void) setOmegaOrbite: (GGReal) val;
- (void) setParametresOrbitaux: (ParametresOrbitaux*) _orbite;
- (ParametresOrbitaux *) parametresOrbitaux;
- (void) calculeTailleSysteme;
- (void) miseAJourSysteme;
- (void) getParametresOrbitaux: (ParametresOrbitaux *)pparam;
- closestFromRelative: (Vect3D *)pos;
- getClosestFrom: (Vect3D *) pV distance: (GGReal *)ret;
- (NSArray *) visibleOrbiteursGlobal;

- (void) startBeingLocal;
- (void) stopBeingLocal;

- (void) tryBeingSuperLocal;
- (void) startBeingOrigineOfLocale: (GGNode *)aNode;
- (void) stopBeingOrigineOfLocale;
- (void) stopBeingMainLocale;
- (void) startBeingMainLocale;

- (void) beginBeingSuperLocal;
- (void) endBeingSuperLocal;

- (void) cameraBeginBeingClose;

- (double) anomalie;

- (System *) surroundingSystem;
- (GGNode *) subSystemFrame;
@end
GGENDDEC

extern NSString *GGOrbiteurBeingSuperLocal;
extern NSString *GGOrbiteurEndBeingSuperLocal;
void debugOrbiteurWindow(NSString *name);
#endif
