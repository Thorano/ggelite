/*
 *  GGBump.m
 *
 *  Copyright (c) 2002 by Frédéric De Jaeger
 *  
 *  Date: January 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#import <Foundation/NSObject.h>
#import <Foundation/NSException.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDebug.h>
#import "GGBlock.h"
#import "utile.h"
#import "GG3D.h"
#import "perlin.h"
#import "Metaobj.h"


@implementation GGNoise
- (id) init
{
    self = [super init];
    if(self){
        _SCALEAlt = 1.0;
        _SCALEF = 1.0;
        _ALPHACOEF = 2.2;
        _NHARM = 5;
        _OCTAVE = 2.1;
        
    }
    return self;
}

+ (GGNoise*)noise
{
    return AUTORELEASE([[self alloc] init]);
}

- (double)scaleOutput {
    return _SCALEAlt;
}

- (void)setScaleOutput:(double)value {
    if (_SCALEAlt != value) {
        _SCALEAlt = value;
    }
}

- (double)scaleInput {
    return _SCALEF;
}

- (void)setScaleInput:(double)value {
    if (_SCALEF != value) {
        _SCALEF = value;
    }
}

- (double)alpha {
    return _ALPHACOEF;
}

- (void)setAlpha:(double)value {
    if (_ALPHACOEF != value) {
        _ALPHACOEF = value;
    }
}

- (int)nharmonic {
    return _NHARM;
}

- (void)setNharmonic:(int)value {
    if (_NHARM != value) {
        _NHARM = value;
    }
}

- (double)beta {
    return _OCTAVE;
}

- (void)setBeta:(double)value {
    if (_OCTAVE != value) {
        _OCTAVE = value;
    }
}

- (double)noiseAtXPos:(double)x
                 yPos:(double)y
                 zPos:(double)z
          gradientRef:(double*)grad
{
    double res;
    if(grad){
        res = _SCALEAlt*PerlinNoise3DWithGradscale(_SCALEF*x, _SCALEF*y, _SCALEF*z, _ALPHACOEF,
                                                   _OCTAVE, _NHARM, grad, 1);
        int i;
        for(i = 0; i < 3; ++i){
            grad[i] *= _SCALEAlt*_SCALEF;
        }
        
    }
    else{
        res = _SCALEAlt*PerlinNoise3D(_SCALEF*x, _SCALEF*y, _SCALEF*z, _ALPHACOEF,
                                      _OCTAVE, _NHARM);
    }
    
    return res;
}

- (double)noiseAtXPos:(double)x
                 yPos:(double)y
                 zPos:(double)z
          gradientRef:(double*)grad
             colorRef:(VectCol*)col
{
    double res = [self noiseAtXPos:x yPos:y zPos:z gradientRef:grad];
    if(col){
        *col = genColFromHeight(res);
    }
    return res;
}
- (VectCol) colorAtXPos:(double)x
                  yPos:(double)y
                  zPos:(double)z
{
    VectCol col;
    [self noiseAtXPos:x yPos:y zPos:z gradientRef:NULL colorRef:&col];
    return col;
}

@end

@implementation PerlinBump
- initWithNoise:(id<TerrainSource>)noise
{
    NSParameterAssert(noise);
    self = [super init];
    if(self){
        _noise = [noise retain];
        addToCheck(self);        
    }
    return self;
}

- init
{
    [self release];
    [NSException raise:NSInternalInconsistencyException format:@"should always use initWithNoise:"];
    return nil;
}

- (void) dealloc
{
    RELEASE(_noise);
    [super dealloc];
}

+ bumpWithNoise:(id<TerrainSource>)noise
{
    return AUTORELEASE([[self alloc] initWithNoise:noise]);
}

- (id<TerrainSource>)noise
{
    return _noise;
}

- (void)setNoise:(id<TerrainSource>)value
{
    if (_noise != value) {
        [_noise release];
        _noise = [value retain];
    }
}

- (void) dataAt: (double) x
            and: (double) y
             in: (VertexData *)pdata
       relative: (double *)orig
{
    double grad[3];
    Vect3D pos, norm;
    //  VectCol *col;
    //  col = &pdata->col;
    
    pos.x = 1000*x;
    pos.y = 1000*y;
    pos.z = 1000*[_noise noiseAtXPos:x yPos:y zPos:1.24 gradientRef:grad colorRef:NULL];

    //  pos.z = 1000*PerlinNoise2DWithGradscale(x, y, 2, 1.8, 10, grad, 1e-6);
    //    pos.z = 1000*PerlinNoise2DPeriodiqueXWithGradscale(x, y, 2.2, 15, 
    //  							  M_PI,
    //  							  grad, 
    //  							  scale);
    if( pos.z < 0)
    {
        pos.z = 0;
        initVect(&norm, 0, 0, 1);
    }
    else
    {
        norm.x = -grad[0];
        norm.y = -grad[1];
        norm.z = 1;
        normaliseVect(&norm);
    }
    *pdata = mkVertexData(norm,pos);
}

- (void) vertexAt: (double) x
              and: (double) y
         inDouble: (double [3])dest
{
    VertexData data;
    [self dataAt: x and: y in: &data relative: NULL];
    dest[0] = data.v.x;
    dest[1] = data.v.y;
    dest[2] = data.v.z;
}

@end

@interface PerlinBumpSphere (SpereBumpExtension)
- (void) dataAtTheta:(double)theta
                  phi:(double)phi
               result:(VertexData*)pdata
           relativeTo:(double*)rel;
- (void) vertexAtTheta:(double)theta
                   phi:(double)phi
                result:(double [3])res    ;
@end

@implementation PerlinBumpSphere (SpereBumpExtension)
- (void) dataAtTheta:(double)theta
                  phi:(double)phi
               result:(VertexData*)pdata
           relativeTo:(double*)rel
{
    double a, b, c;
    double h;
    double cost;
    double r;
    double grad[3];
    Vect3D grad2;
    Vect3D ur;
    Vect3D ut, uf;
    Vect3D n;
    Vect3D pos, norm;
    
    //  NSCParameterAssert(-M_PI/2 <= theta && theta <= M_PI/2 && 0 <= phi && 
    //		     phi <= 2*M_PI);
    
    
    cost = cos(theta);
    
    a = cost*cos(phi);
    b = cost*sin(phi);
    c = sin(theta);
    
    //    pdata->texCoord.x = a;
    //    pdata->texCoord.y = b;
    //    pdata->texCoord.z = c;
    
    r = radius;
    //   h=SCALEAlt*PerlinNoise2D(SCALEF*phi, SCALEF*theta, ALPHACOEF, OCTAVE, NHARM);
    //   grad[0] = 0;
    //   grad[1] = 0;
    //   grad[2] = 0;
    VectCol col;
    h = [_noise noiseAtXPos:radius*a yPos:radius*b zPos:radius*c gradientRef:grad colorRef:&col];
    if( h < 0 )
    {
        h = 0;
        initVect(&norm, a, b, c);
    }
    else
    {
        r += h;
        initVect(&grad2, radius*grad[0], radius*grad[1], radius*grad[2]);
        
        //we are sure there is no error here...
        //       if( theta == M_PI/2 ) 
        // 	{
        // 	  initVect(&ur, 0, 0, 1);
        // 	  initVect(&uf, 1, 0, 0);
        // 	  initVect(&ut, 0, 1, 0);
        // 	}
        //       else if( theta == -M_PI/2 )
        // 	{
        // 	  initVect(&ur, 0, 0, -1);
        // 	  initVect(&uf, 1, 0, 0);
        // 	  initVect(&ut, 0, -1, 0);
        // 	}
        //       else //useless because no singularity in the following computation.
        {
            initVect(&ur, a, b, c);
            initVect(&uf, -sin(phi), cos(phi), 0);
            initVect(&ut, -sin(theta)*cos(phi), -sin(theta)*sin(phi), cos(theta));
        }
        
        
        
        
        /* vecteur normal à la surface en coordonnées sphérique 
            (u_r, u_\phi, u_\theta)*/
        initVect(&n, r, -prodScal(&grad2, &uf), -prodScal(&grad2, &ut));
        
        mulScalVect(&ur, n.x, &norm);
        addLambdaVect(&norm, n.y, &uf, &norm);
        addLambdaVect(&norm, n.z, &ut, &norm);
        
        normaliseVect(&norm);
    }
    
    if(rel)
    {
        //we keep double precision as far as we can :
        pos.x = (r*a) - rel[0];
        pos.y = (r*b) - rel[1];
        pos.z = (r*c) - rel[2];
    }      
    else
    {
        pos.x = (r*a);
        pos.y = (r*b);
        pos.z = (r*c);
    }
    
    *pdata = mkVertexDataCol(norm,pos,col);
}

- (void) vertexAtTheta:(double)theta
                    phi:(double)phi
                 result:(double [3])res
{
    double a, b, c;
    double h;
    double cost;
    double r;
    
    NSCParameterAssert(-M_PI/2 <= theta && theta <= M_PI/2 && 0 <= phi && 
                       phi <= 2*M_PI);
    
    
    cost = cos(theta);
    
    a = cost*cos(phi);
    b = cost*sin(phi);
    c = sin(theta);
    
    r = radius;
    
    h = [_noise noiseAtXPos:radius*a yPos:radius*b zPos:radius*c gradientRef:NULL colorRef:NULL];
    
    if( h < 0 )
    {
        h = 0;
    }
    r += h;
    res[0] = (float)(r*a);
    res[1] = (float)(r*b);
    res[2] = (float)(r*c);
}
@end

@implementation PerlinBumpSphere
- (void) dataAt: (double) x
            and: (double) y
             in: (VertexData *)pdata
       relative: (double *)orig
{
    NSAssert(_noise, @"no noise here!");
    return [self dataAtTheta:y 
                            phi:x
                         result:pdata
                     relativeTo:orig];
//    return dataAt(radius, y, x, pdata, orig);
}

- (void) vertexAt: (double) x
              and: (double) y
         inDouble: (double [3])dest
{
    NSAssert(_noise, @"no noise here!");
    return [self vertexAtTheta:y phi:x result:dest];
//    return vertexAtInDouble(radius, y, x, dest);
}


- initWithNoise: (id<TerrainSource>) noise radius: (double) r
{
    self = [super initWithNoise:noise];
    if(self){
        radius = r;
    }
    return self;
}

+ bumpWithNoise:(id<TerrainSource>) noise radius: (double) r
{
    return AUTORELEASE([[self alloc] initWithNoise:noise radius: r]);
}

#pragma mark  TexGen Stuff
- (int) nCol
{
    return 3;
}

- (void) colorBoxForPoint: (double [3]) val
                       in: (unsigned char *) dest
{
    double n, r, s, t;
    
    r = val[0];
    s = val[1];
    t = val[2];
    n = sqrt(r*r+s*s+t*t);
    r /= n;
    s /= n;
    t /= n;
    VectCol col = [_noise colorAtXPos:radius*r yPos:radius*s zPos:radius*t];
    dest[0] = (unsigned char)255*col.r;
    dest[1] = (unsigned char)255*col.g;
    dest[2] = (unsigned char)255*col.b;
}

@end

#if 1
@implementation PerlinBumpCube
#define SIZECUBE 5000
- initWithNorm: (Vect3D )v
           dir: (Vect3D )d
           noise:(id<TerrainSource>)noise
{
    self = [super initWithNoise:noise];
    if(self){
        dirnorm = v;
        dirx = d;
        prodVect(&v,&d, &diry);
        _scaleFactor = SIZECUBE;
        _offset = 0.0;
    }
    return self;
}

- initWithNoise:(id<TerrainSource>)noise
{
    Vect3D v = {0, 0, 1};
    Vect3D w = {1, 0, 0};
    return [self initWithNorm: v
                          dir: w
                      noise:noise];
}

+ bumpWithNorm: (Vect3D)v
           dir: (Vect3D)w
           noise:(id<TerrainSource>)noise
{
    return AUTORELEASE([[self alloc] initWithNorm: v
                                              dir: w
                                              noise:noise]);
}

- (float)scaleFactor {
    return _scaleFactor;
}

- (void)setScaleFactor:(float)value {
    if (_scaleFactor != value) {
        _scaleFactor = value;
    }
}

- (float)offset {
    return _offset;
}

- (void)setOffset:(float)value {
    if (_offset != value) {
        _offset = value;
    }
}



- (void) dataAt: (double) x
            and: (double) y
             in: (VertexData *)pdata
       relative: (double *)orig
{
    double a, b, c;
    double res;
    double grad[3];
    Vect3D pos, norm;
    
    a = _offset*dirnorm.x+ _scaleFactor * (x*dirx.x+y*diry.x);
    b = _offset*dirnorm.y+ _scaleFactor * (x*dirx.y+y*diry.y);
    c = _offset*dirnorm.z+ _scaleFactor * (x*dirx.z+y*diry.z);
    res = [_noise noiseAtXPos:a yPos:b zPos:c gradientRef:grad colorRef:NULL];

    if ( orig )
        initVect(&pos, a-orig[0], b-orig[1], c-orig[2]);
    else
        initVect(&pos, a, b, c);
    
    if( res < 0)
    {
        norm = dirnorm;
        res = 0;
    }
    else
    {
        Vect3D grad2;
        Vect3D norm2;
        initVect(&grad2, grad[0], grad[1], grad[2]);
        initVect(&norm2, -prodScal(&grad2, &dirx), 
                 -prodScal(&grad2, &diry), 1);
        
        mulScalVect(&dirx, norm2.x, &norm);
        addLambdaVect(&norm, norm2.y, &diry, &norm);
        addLambdaVect(&norm, norm2.z, &dirnorm, &norm);
        normaliseVect(&norm);

        // compute the position
        addLambdaVect(&pos, res, &dirnorm, &pos);
    }
    *pdata = mkVertexData(norm,pos);
}

- (void) vertexAt: (double) x
              and: (double) y
         inDouble: (double [3])dest
{
    double a, b, c;
    double res;
    
    a = _offset*dirnorm.x+ _scaleFactor * (x*dirx.x+y*diry.x);
    b = _offset*dirnorm.y+ _scaleFactor * (x*dirx.y+y*diry.y);
    c = _offset*dirnorm.z+ _scaleFactor * (x*dirx.z+y*diry.z);
    res = [_noise noiseAtXPos:a yPos:b zPos:c gradientRef:NULL colorRef:NULL];
    
    dest[0] = a;
    dest[1] = b;
    dest[2] = c;
    
    if( res >= 0)
    {
        dest[0] += res * dirnorm.x;
        dest[1] += res * dirnorm.y;
        dest[2] += res * dirnorm.z;
    }
}

@end
#endif

@implementation PerlinBumpSphereTop
static void convertTopToSpherical(double x, double y, double res[2])
{
    double theta, phi;
    if( x+y <= 0)
    {
        if( y <= x) /*first sector*/
        {
            //we know y <= 0
            double fy;  // =fabs(y) = -y
            
            fy = -y;
            
            if( fy <= 1e-8 )
            {
                theta=M_PI/2;
                phi = 0;
            }
            else
            {
                theta = (M_PI/4.0)*(y+2.0);
                phi = (M_PI/4.0)*((x/fy)+1);
            }
        }
        else /*4th sector */
        {
            //we know x <= 0
            double fx;
            fx = -x;
            if( fx <= 1e-8 )
            {
                theta=M_PI/2;
                phi = 0;
            }
            else
            {
                theta = (M_PI/4.0)*(x+2.0);
                phi = (M_PI/4.0)*(7-(y/fx));
            }
        }
    }
    else 
    {
        if( y <= x ) /*second sector */
        {
            //we know x >= 0
            if( x <= 1e-8)
            {
                theta=M_PI/2;
                phi = 0;
            }
            else
            {
                theta = (M_PI/4.0)*(2.0-x);
                phi = (M_PI/4.0)*((y/x)+3);
            }
        }
        else  /*3rd*/
        {
            //y >= 0
            if( y <= 1e-8 )
            {
                theta=M_PI/2;
                phi = 0;
            }
            else
            {
                theta = (M_PI/4.0)*(2.0-y);
                phi = (M_PI/4.0)*(5-(x/y));
            }
        }
    }
    res[0] = theta;
    res[1] = phi;
}

- (void) dataAt: (double) x
            and: (double) y
             in: (VertexData *)pdata
       relative: (double *)orig
{
    double res[2];
    convertTopToSpherical(x, y, res);
    [self dataAtTheta:res[0] phi:res[1] result:pdata relativeTo:orig];
//    dataAt(radius, res[0], res[1], pdata, orig);
}

- (void) vertexAt: (double) x
              and: (double) y
         inDouble: (double [3])dest
{
    double spher[2];
    convertTopToSpherical(x, y, spher);
    [self vertexAtTheta:spher[0] phi:spher[1] result:dest];
//    vertexAtInDouble(radius, spher[0], spher[1], dest);
}

@end

@implementation PerlinBumpSphereBottom
static void convertBottomToSpherical(double x, double y, double res[2])
{
    double theta, phi;
    if( x+y <= 0)
    {
        if( y <= x) /*first sector*/
        {
            //we know y <= 0
            double fy;  // =fabs(y) = -y
            
            fy = -y;
            
            if( fy <= 1e-8 )
            {
                theta=-M_PI/2;
                phi = 0;
            }
            else
            {
                theta = -(M_PI/4.0)*(y+2.0);
                phi = (M_PI/4.0)*(7 - (x/fy));
            }
        }
        else /*4th sector */
        {
            //we know x <= 0
            double fx;
            fx = -x;
            if( fx <= 1e-8 )
            {
                theta=-M_PI/2;
                phi = 0;
            }
            else
            {
                theta = -(M_PI/4.0)*(x+2.0);
                phi = (M_PI/4.0)*(1+(y/fx));
            }
        }
    }
    else 
    {
        if( y <= x ) /*second sector */
        {
            //we know x >= 0
            if( x <= 1e-8)
            {
                theta=-M_PI/2;
                phi = 0;
            }
            else
            {
                theta = -(M_PI/4.0)*(2.0-x);
                phi = (M_PI/4.0)*(5-(y/x));
            }
        }
        else  /*3rd*/
        {
            //y >= 0
            if( y <= 1e-8 )
            {
                theta=-M_PI/2;
                phi = 0;
            }
            else
            {
                theta = -(M_PI/4.0)*(2.0-y);
                phi = (M_PI/4.0)*(3+(x/y));
            }
        }
    }
    res[0] = theta;
    res[1] = phi;
}

- (void) dataAt: (double) x
            and: (double) y
             in: (VertexData *)pdata
       relative: (double *)orig
{
    double spher[2];
    convertBottomToSpherical(x, y, spher);
    [self dataAtTheta:spher[0] phi:spher[1] result:pdata relativeTo:orig];
//    dataAt(radius, spher[0], spher[1], pdata, orig);
}


- (void) vertexAt: (double) x
              and: (double) y
         inDouble: (double [3])dest
{
    double spher[2];
    convertBottomToSpherical(x, y, spher);
    [self vertexAtTheta:spher[0] phi:spher[1] result:dest];
//    vertexAtInDouble(radius, spher[0], spher[1], dest);
}

@end

@implementation PerlinBumpTore
static double SCALEAlt = 1000;
static double ALPHACOEF = 2.19;
static int NHARM = 8;
static double OCTAVE = 2.1;


- initMajorRadius: (double) Mr
      minorRadius: (double) mr
{
    [super init];
    
    main_radius = Mr;
    minor_radius = mr;
    
    NSDebugMLLog(@"Torus", @"major = %g, minor = %g",
                 Mr, mr);
    
    return self;
}

+ torusMajorRadius: (double) Mr
       minorRadius: (double) mr
{
    return AUTORELEASE([[self alloc] initMajorRadius: Mr
                                         minorRadius: mr]);
}

#define COEFTORE 0.0004

- (void) dataAt: (double) phi
            and: (double) theta
             in: (VertexData *)pdata
       relative: (double *)orig
{
    double x1, y1;
    double x2, y2, z2;
    double t1;
    double h;
    double grad[3];
    Vect3D ur;
    Vect3D pos, norm;
    
    x1 = cos(phi);
    y1 = sin(phi);
    
    initVect(&ur, x1*cos(theta), y1*cos(theta), sin(theta));
    
    t1 = main_radius + cos(theta) * minor_radius;
    
    x2 = x1 * t1;
    y2 = y1 * t1;
    z2 = minor_radius * sin(theta);
    
    h=SCALEAlt*PerlinNoise3DWithGradscale(COEFTORE * x2, COEFTORE * y2, 
                                          COEFTORE * z2, 
                                          ALPHACOEF,
                                          OCTAVE, NHARM, grad, 1);
    
    
    if (h <= 0)
    {
        norm = ur;
    }
    else
    {
        Vect3D ut;
        Vect3D uf;
        Vect3D n;
        double t2, t3;
        Vect3D grad2;
        const double fact = SCALEAlt*COEFTORE;
        
        x2 += h*ur.x;
        y2 += h*ur.y;
        z2 += h*ur.z;
        
        t2 = (minor_radius+h);
        t3 = (main_radius + (minor_radius + h) * cos(theta));
        initVect(&grad2, fact * grad[0], fact * grad[1], fact * grad[2]);
        
        
        initVect(&uf, -sin(phi), cos(phi), 0);
        initVect(&ut, -sin(theta)*cos(phi), -sin(theta)*sin(phi), cos(theta));
        
        initVect(&n,
                 t3 *t2,
                 -t1 * t2 * prodScal(&grad2, &uf),
                 - t3 * minor_radius * prodScal(&grad2, &ut));
        
        mulScalVect(&ur, n.x, &norm);
        addLambdaVect(&norm, n.y, &uf, &norm);
        addLambdaVect(&norm, n.z, &ut, &norm);
        
        normaliseVect(&norm);
    }
    
    if( orig )
    {
        initVect(&pos, 
                 x2 - orig[0], 
                 y2 - orig[1], 
                 z2 - orig[2]);
    }
    else
        initVect(&pos, x2, y2, z2);
    //  NSLog(@"pos = %@ phi = %g theta = %g", stringOfVect(&pos), phi, theta);
    *pdata = mkVertexData(norm,pos);
}

- (void) vertexAt: (double) phi
              and: (double) theta
         inDouble: (double [3])dest
{
    double x1, y1;
    double x2, y2, z2;
    double t1;
    double h;
    Vect3D ur;
    
    x1 = cos(phi);
    y1 = sin(phi);
    
    initVect(&ur, x1*cos(theta), y1*cos(theta), sin(theta));
    
    t1 = main_radius + cos(theta) * minor_radius;
    
    x2 = x1 * t1;
    y2 = y1 * t1;
    z2 = minor_radius * sin(theta);
    
    h=SCALEAlt*PerlinNoise3D(COEFTORE * x2, COEFTORE * y2, 
                             COEFTORE * z2, 
                             ALPHACOEF,
                             OCTAVE, NHARM);
    
    
    if (h > 0)
    {
        x2 += h*ur.x;
        y2 += h*ur.y;
        z2 += h*ur.z;
    }
    
    dest[0] = (float) x2;
    dest[1] = (float) y2;
    dest[2] = (float) z2;
    //   {
    //     Vect3D pos;
    //     initVect(&pos, x2, y2, z2);
    //     NSLog(@"sopxcv = %@ phi = %g theta = %g", stringOfVect2(pos), phi, theta);
    //   }
}
@end
