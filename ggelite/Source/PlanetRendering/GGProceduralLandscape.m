//
//  GGProceduralLandscape.m
//  elite
//
//  Created by Frederic De Jaeger on 12/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGProceduralLandscape.h"
//#import "perlin.h"
#import "GGBump.h"
#import "GG3D.h"
#import "utile.h"


@implementation GGProceduralLandscape
- (id) init
{
    self = [super init];
    if(self){
        _continentalNoise = [[GGNoise alloc] init];
        [_continentalNoise setScaleInput:5e-7];
        [_continentalNoise setNharmonic:5];
        
        _mediumFrequency = [[GGNoise alloc] init];
        [_mediumFrequency setNharmonic:5];
        [_mediumFrequency setScaleInput:1.0/1e5];  // continental frequency is 100 km
        
        _highFrequency = [[GGNoise alloc] init];
        [_highFrequency setNharmonic:5];
        [_highFrequency setScaleInput:1.0/2e3];   // 2 km between mountains and hills.  Looks nice.
        
        _seaLimit = 0.1;
        _probaMountainFactor = 0.5;     // we must be above 0.8 to have a mountain.
        _probaMountainFactorFarSea = -1.0;
        _highestMountain = 9000;        // Everest like
        _highestValley = 2000;
        _highestHill = 300;
    }
    return self;
}

- (GGNoise *)continentalNoise {
    return _continentalNoise;
}

- (void)setContinentalNoise:(GGNoise *)value {
    if (_continentalNoise != value) {
        [_continentalNoise release];
        _continentalNoise = [value copy];
    }
}

- (GGNoise *)highFrequency {
    return [[_highFrequency retain] autorelease];
}

- (void)setHighFrequency:(GGNoise *)value {
    if (_highFrequency != value) {
        [_highFrequency release];
        _highFrequency = [value copy];
    }
}

- (double)seaLimit {
    return _seaLimit;
}

- (void)setSeaLimit:(double)value {
    NSLog(@"seaLimit = %g", value);
    if (_seaLimit != value) {
        _seaLimit = value;
    }
}

- (double)probaMountainFactor {
    return _probaMountainFactor;
}

- (void)setProbaMountainFactor:(double)value {
    if (_probaMountainFactor != value) {
        _probaMountainFactor = value;
    }
}

- (double)probaMountainFactorFarSea {
    return _probaMountainFactorFarSea;
}

- (void)setProbaMountainFactorFarSea:(double)value {
    if (_probaMountainFactorFarSea != value) {
        _probaMountainFactorFarSea = value;
    }
}

- (double)highestMountain {
    return _highestMountain;
}

- (void)setHighestMountain:(double)value {
    if (_highestMountain != value) {
        _highestMountain = value;
    }
}

- (double)highestValley {
    return _highestValley;
}

- (void)setHighestValley:(double)value {
    if (_highestValley != value) {
        _highestValley = value;
    }
}

- (double)highestHill {
    return _highestHill;
}

- (void)setHighestHill:(double)value {
    if (_highestHill != value) {
        _highestHill = value;
    }
}

#pragma mark -
static inline double  parabole(double input, double *derivative)
{
    if(input <= -1.0){
        *derivative = 0.0;
        return 0.0;
    }
    else{
        double tmp = (input + 1.0);
        *derivative = tmp/2.0;
        return tmp*tmp/4.0;
    }
}

- (double) altitudeAtX:(double)x
                     y:(double)y
                     z:(double)z
           gradientRef:(double*) gradientRef
        terrainTypeRef:(int*) terrainTypeRef
              colorRef:(VectCol*)colorRef
{
    TerrainType terraintype;
    double altitude;
    double continentalFactor = [_continentalNoise noiseAtXPos:x yPos:y zPos:z gradientRef:NULL];
    if(continentalFactor < _seaLimit){
        // we are in the ocean
        terraintype  = TerrainTypeSea;
        if(colorRef){
            *colorRef = GGMakeCol(0.1, 0.3, 0.6, 1);
        }
        
        if(gradientRef){
           gradientRef[0] = gradientRef[1] = gradientRef[2] = 0.0;            
        }
        altitude = 0.0;
    }
    else{
        double terrainTypeProbe = [_mediumFrequency noiseAtXPos:x yPos:y zPos:z gradientRef:NULL];
        double mountainProba = (_probaMountainFactorFarSea - _probaMountainFactor)*continentalFactor + _probaMountainFactor;
        
        double gradient[3];
        double rawAltitude = [_highFrequency noiseAtXPos:x yPos:y zPos:z gradientRef:gradientRef ? gradient : NULL];
        double tmp = terrainTypeProbe - mountainProba;
        double alpha;
#if 1
        if(tmp >= 0.1){
            alpha = 1;   // pure mountain
        }
        else if(tmp <= -0.1){
            alpha = 0;   // pure plain
        }
        else{
            alpha = (tmp + 0.1)/0.2;
            NSAssert(alpha >= 0, @"bad");
            NSAssert(alpha <= 1, @"bad too");
        }
#else
        if(tmp > 0){
            alpha = 1;
        }
        else{
            alpha = 0.0;
        }
#endif
        
        terraintype = TerrainTypeGrass;
        
        altitude = 0.0;
        Vect3D color = GGZeroVect;
        if(gradientRef){
            gradientRef[0] = gradientRef[1] = gradientRef[2] = 0;            
        }
        if(alpha > 0){
            // compute mountain part.
            static MapCol mapCol[] = 
                {
                { 0, {  0.03, 0.91, 0.12, 1}},  // valley are green
                { 0.3, {  0.3, 0.55, 0.12, 1}},    // then, dark green
                { 0.5, {  0.6, 0.6, 0.6, 1}}, // only rock (some grey)
                { 0.7, { 0.95, 0.86, 0.92, 1}}, // beginning of the snow
//                { 0, {  0.03, 0., 0., 1}},  // 
//                { 0.3, {  0.3, 0., 0., 1}},    
//                { 0.5, {  0.6, 0., 0., 1}}, // 
//                { 0.7, { 0.95, 0., 0., 1}}, // 
                };
            
            double derivativeCorrection;
            double newRawAlt = parabole(rawAltitude,&derivativeCorrection);  // have pick sharp.
            double mountainAltitude = (tmp + 0.2)/2.0 * (_highestMountain * newRawAlt + _highestValley);
            altitude += alpha * mountainAltitude;
            Vect3D tmpCol = vectOfCol(genColFromMapHeight(mapCol, sizeof(mapCol)/sizeof(MapCol), newRawAlt));
            addLambdaVect(&color,alpha,&tmpCol,&color);
            
            terraintype = TerrainTypeMountain;
            if(gradientRef){
                gradientRef[0] += (alpha)*_highestMountain*gradient[0];
                gradientRef[1] += (alpha)*_highestMountain*gradient[1];
                gradientRef[2] += (alpha)*_highestMountain*gradient[2];
            }
        }
        if(alpha < 1){
            // compute plain part.
            double plainAltitude = (1.0 + rawAltitude) * _highestHill;
            if(gradientRef){
                gradientRef[0] += (1-alpha)*_highestHill*gradient[0];
                gradientRef[1] += (1-alpha)*_highestHill*gradient[1];
                gradientRef[2] += (1-alpha)*_highestHill*gradient[2];
            }
            altitude += (1.0 - alpha) * plainAltitude;
            static MapCol mapCol[] = 
            {
            { -0.5, {  0.0, 0.43, 0.01, 1}},  // forest
            { 0.0, {  0.15, 0.91, 0.17, 1}},    // green grass
            { 0.1, {  0.87, 0.82, 0., 1}}, // desert
            { 0.9, {  0.87, 0.82, 0., 1}}, // desert
//            { -0.5, {  0.3, 0.4, 0.3, 1}},  // forest
//            { 0.1, {  0.3, 0.7, 0.3, 1}},    // green grass
//            { 0.7, {  0.3, 0.9, 0.3, 1}}, // desert
            };
            Vect3D tmpCol = vectOfCol(genColFromMapHeight(mapCol, sizeof(mapCol)/sizeof(MapCol), terrainTypeProbe));
            addLambdaVect(&color,1.0-alpha,&tmpCol,&color);
        }
        if(colorRef){
            *colorRef = colOfVect(color);
        }
    }
    
    if(terrainTypeRef){
        *terrainTypeRef = terraintype;
    }
    
    return altitude;
}

- (double)noiseAtXPos:(double)x
                 yPos:(double)y
                 zPos:(double)z
          gradientRef:(double*)grad
             colorRef:(VectCol*)col
{
    double h = [self altitudeAtX:x y:y z:z gradientRef:grad terrainTypeRef:NULL colorRef:col];
    return h;
}

- (VectCol) colorAtXPos:(double)x
                   yPos:(double)y
                   zPos:(double)z
{
    VectCol col;
    [self altitudeAtX:x y:y z:z gradientRef:NULL terrainTypeRef:NULL colorRef:&col];
    return col;
}

@end
