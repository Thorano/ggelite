//
//  GGBlockCollision.h
//  elite
//
//  Created by Frédéric on 27/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#ifndef __GGBlockCollision_h
#define __GGBlockCollision_h 1
#import <Foundation/Foundation.h>

#import "GGBlock.h"
#import "GGCollision.h"
#import "GGMMesh.h"
#import "GGSolid.h"

@interface GGBlock (CollisionProtocol) <GGCollisionNode>
- (GGReal) diameterAtIndex:(unsigned)i index:(unsigned)j size:(unsigned)size;
- (const struct __BoundingBox) tranformedBoundingBoxCacheForI:(unsigned)i j:(unsigned)j size:(unsigned)size;
@end

@interface GGSubBlockHelper : NSObject <GGCollisionNode>
{
    GGBlockCollision*           _owner;   // weak retain
    struct __GGSubBlockHelperInfo{
        unsigned xpos : 4;
        unsigned ypos : 4;
        unsigned size : 4;
    }_info;
}
- (id) initWithBlockCollision:(GGBlockCollision*)owner atXPos:(unsigned)xpos yPos:(unsigned)yPos size:(unsigned)size;

- (unsigned) xPos;
- (unsigned) yPos;
- (unsigned) size;

@end

typedef struct _GGSubBlockHelperRep
{
    @defs(GGSubBlockHelper);
} GGSubBlockHelperRep;

@interface GGBlockCollision : NSObject <GGCollisionNode> 
{
    GGBlock         *_owner; // weak reference.
    CacheVertex     _vertices[SIZEMIP*SIZEMIP];
    FaceIndices     _faces[2*BRANCHEMENT*BRANCHEMENT];
    GGPlan          _planes[2*BRANCHEMENT*BRANCHEMENT];
    unsigned        _numberOfVertices;
    unsigned        _numberOfFaces;
    unsigned        _generation;
    GGSubBlockHelperRep _subblocks[SIZEBOX][SIZEBOX];
    NSMutableArray  *_subNodes;
}
- (id) initWithBlock:(GGBlock*)block;
- (GGBlock*) block;
- (void) unsetup;

- (GGSubBlockHelper*) subHelperForXPos:(unsigned)xpos yPos:(unsigned)yPos size:(unsigned)size;
- (void) drawDebug;
@end

@interface GGSolidPlanet : GGSolid <GGCollisionNode>
{
    GGBlockContext         *_context; // weak reference.    
}
- (id) initWithContext:(GGBlockContext*)block;
- (id<GGCollisionNode>) topLevelCollisionNodeInManager:(GGCollisionManager*)manager;
@end

#endif