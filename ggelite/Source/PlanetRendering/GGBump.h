/* 	-*-ObjC-*- */
/*
 *  GGBump.h
 *
 *  Copyright (c) 2002 by Frédéric De Jaeger
 *  
 *  Date: January 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGBump_h
#define __GGBump_h

#import "Texture.h"

//#define USE_COL_IN_DATA 1

typedef struct __vertexData /* GL_T2F_N3F_V3F */
{
  //  Vect3D	texCoord;
  Vect3D	norm;
  Vect3D	v;
#ifdef USE_COL_IN_DATA
  Vect3D    col;
#endif
}VertexData;

static inline VertexData mkVertexData(Vect3D norm, Vect3D pos) {
#ifdef USE_COL_IN_DATA
    Vect3D col = {0, 0, 0};
#endif
    VertexData data = {
        norm, pos, 
#ifdef USE_COL_IN_DATA
        col,
#endif
    };
    return data;
}

static inline VertexData mkVertexDataCol(Vect3D norm, Vect3D pos, VectCol col) {
    VertexData data = {
        norm, pos, 
#ifdef USE_COL_IN_DATA
    {col.r, col.g, col.b},
#endif
    };
    return data;
}

@protocol GGBump <NSObject>
- (void) dataAt: (double) x
	    and: (double) y
	     in: (VertexData *)pdata
       relative: (double *)orig;
- (void) vertexAt: (double) x
	      and: (double) y
	 inDouble: (double [3])dest;
@end

@protocol TerrainSource <NSObject>
- (double)noiseAtXPos:(double)x
                 yPos:(double)y
                 zPos:(double)z
          gradientRef:(double*)grad
             colorRef:(VectCol*)col;
- (VectCol) colorAtXPos:(double)x
                   yPos:(double)y
                   zPos:(double)z;

@end

@interface GGNoise : NSObject <TerrainSource>
{
    double              _SCALEAlt;
    double              _SCALEF;
    double              _ALPHACOEF;
    unsigned            _NHARM;
    double              _OCTAVE;
}
+ (GGNoise*)noise;
- (double)scaleOutput;
- (void)setScaleOutput:(double)value;

- (double)scaleInput;
- (void)setScaleInput:(double)value ;

- (double)alpha ;
- (void)setAlpha:(double)value ;

- (int)nharmonic;
- (void)setNharmonic:(int)value;

- (double)beta ;
- (void)setBeta:(double)value;

- (double)noiseAtXPos:(double)x
                 yPos:(double)y
                 zPos:(double)z
          gradientRef:(double*)grad;
@end


@interface PerlinBump : NSObject <GGBump> 
{
    id<TerrainSource>            _noise;
}
+ bumpWithNoise:(id<TerrainSource>)noise;
- initWithNoise:(id<TerrainSource>)noise;
- (id<TerrainSource>)noise;
- (void)setNoise:(id<TerrainSource>)value;

- (void) dataAt: (double) x
	    and: (double) y
	     in: (VertexData *)pdata
       relative: (double *)orig;
- (void) vertexAt: (double) x
	      and: (double) y
	 inDouble: (double [3])dest;
@end


@interface PerlinBumpSphere : PerlinBump <TexInitializer>
{
  double	radius;
}
+ bumpWithNoise:(id<TerrainSource>) noise radius: (double) r;
@end

@interface PerlinBumpSphereTop : PerlinBumpSphere
@end

@interface PerlinBumpSphereBottom : PerlinBumpSphere
@end

@interface PerlinBumpTore : PerlinBump
{
  double 	main_radius, minor_radius;
}
+ torusMajorRadius: (double) Mr
       minorRadius: (double) mr;
@end

@interface PerlinBumpCube : PerlinBump
{
  Vect3D	dirnorm;
  Vect3D	dirx, diry;
  float     _scaleFactor;
  float     _offset;
}
- initWithNorm: (Vect3D )v
           dir: (Vect3D )d
           noise:(id<TerrainSource>)noise;
+ bumpWithNorm: (Vect3D)v
	   dir: (Vect3D)w
           noise:(id<TerrainSource>)noise;

- (float)scaleFactor;
- (void)setScaleFactor:(float)value;

- (float)offset;
- (void)setOffset:(float)value;

@end

#endif




