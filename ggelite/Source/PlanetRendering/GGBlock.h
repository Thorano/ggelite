/* 	-*-ObjC-*- */
/*
 *  GGBlock.h
 *
 *  Copyright (c) 2002 by Frédéric De Jaeger
 *  
 *  Date: January 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGBlock_h
#define __GGBlock_h

#import "types.h"

@class GGBlock;
@class GGScene;
@class Texture;

typedef struct __subblock
{
    GGBlock			*block;
    //  short				err;
    Vect3D			pos;
}SubBlock;

#define LNBRANCHEMENT 4
#define BRANCHEMENT (1<<LNBRANCHEMENT)
#define SIZEMIP (BRANCHEMENT+1)
#define SIZEEPS (LNBRANCHEMENT+2)
#define SIZEBOX (BRANCHEMENT*2)

typedef struct __boxf
{
    short int lomin, lomax, lamin, lamax, hmin, hmax;
}Boxf;

typedef struct __boxExt
{
    Boxf theBox;
    unsigned short roughness;
    unsigned char cache;
}BoxExt;


typedef struct __frame
{
    // all that stuff is done to have that property:
    // decal is the center of the box in the frame of drawing.
    // when the block is local,  decal is center of the box
    // in the local frame.
    // when the block is not local is the position
    // of the center of the box in the planet frame.
    GGReal			size;
    
    /* coordinate of the box in the parent */
    int			i, j;
    Vect3D		decal;   
}Frame;

#import "GGBump.h"

@class GGSolid;
@class GGBlockContext;

@class GGBlockCollision;

@interface GGBlock : NSObject
{
    /* position in the reality */
    double		xmin, ymin, width, height;
    GGBlockCollision    *_blockCollision;
    SubBlock		fils[BRANCHEMENT][BRANCHEMENT];
    
    @public
    /* data associates with this block */
    VertexData		data[SIZEMIP][SIZEMIP];
    BoxExt		boundingBox[SIZEBOX][SIZEBOX];
    
    /* contains all the context */
    GGBlockContext		*manager;

    
    /* pointer to adjacent blocks */
        GGBlock		*haut, *bas, *droite, *gauche;
        
        /* pointer to the father, NULL means we are the root */
        GGBlock		*father;
    id<GGBump>	bump;
    
    /*position of the block in space*/
    Frame			frame;

    @protected
    
    /* position of the center of the box in the planet coordinate.
        This is only used when the block is local.  With that, we can 
        update frame.
        */
    Vect3D		origin;  
    
    /* increment after every frame.  This is used to know if we 
        already compute some stuff for the current block and the current
        image
        */
    int			nFrame, frameCreation;
    
    /* current Level Of Detail of this block */
    /* >=0 classical LOD
        -1 means that one of the neighboor is split.
        -2 we are split ourself
        */
    int			lod;
    double		floatMipLevel;
    
    /* depth in the tree */
    int			depth;
    
    /* rugosity of the terrain, help to compute lod depending of the position
        of the observer */
    unsigned short	epsilon[SIZEEPS];
    
    /* cached value of the number of lateral sons that are split
        The problem is.  It's not possible to split a son if the neighboors are not
        enough splitted.  So we cache this information so that we can test 
        efficiently if we can split or not */
    unsigned short int	nhaut, nbas, ndroite, ngauche;
    unsigned short	nSonSplit; //total number of sons splitted.
    BOOL			local;
    BOOL            _invalidated;
    BOOL            _splitable;
}

+ topBlockWithManager:(GGBlockContext *)man
                 bump: (NSObject<GGBump> *)aBump
                   at: (double) _xmin
                  and: (double) _ymin
                width: (double) _width
               height: (double) _height;
- (void) rasterize;
- (id) bump;
- (BOOL) isLocal;
- (void) setIsLocal:(BOOL)val;

- (GGBlock*) bottomNeighBor;
- (GGBlock*) topNeighBor;
- (GGBlock*) rightNeighBor;
- (GGBlock*) leftNeighBor;

- (GGBox) boudingBox;


/*!
    @method     path
    @abstract   returns a sequence of index that localize the given node in the tree.
    @discussion The structure of the path should really be opaque.  
*/
- (NSMutableArray*) _createAncestorPath;
- (NSArray*) path;

#pragma mark -

+ (void) initGLContextBeforeDrawing: (Texture *)theTexture;
+ (void) finish;

#ifdef DEBUG
+ (void) displayBorder;
+ (void) freezeBlocks;
+ (void) resetStat;
+ (void) logStats;
- (void) printInfo;
- (void) checkSons;
- (void) drawSonBoxAt:(int)icur and:(int)jcur;
- (void) displayNormal;
#endif
@end

#ifdef DEBUG_COCOA
#import "GGDebugController.h"
@interface GGDebugController (Block)
@end
#endif
void secondPassBis(GGBlock *block);

#endif

double blockLod(GGBlock *block);

GGBlock* GGBlockGetSubBlock(GGBlock *block, unsigned i, unsigned j);