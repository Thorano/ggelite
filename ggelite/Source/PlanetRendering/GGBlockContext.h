//
//  GGBlockContext.h
//  elite
//
//  Created by Frédéric De Jaeger on 27/12/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#ifndef __GGBlockContext_h
#define __GGBlockContext_h 1

#import "types.h"

@class GGBlock;
@class GGScene;
@class Texture;
@class GGSolid;

/*!
    @class      GGBlockContext
    @abstract   contains all the logic to bridge the planet rendering algorithm with DOM structure of the game.
    @discussion Because a planet is huge, the mesh is usually represented by a global part (the whole planet)
  and a local part (around the camera).  Those two parts are two separate nodes in the tree.  The global node
 is rasterized first, then the local node.
*/
@interface GGBlockContext : NSObject 
{
    Texture		*texture;
    GGSolid     *solid;
    NSArray     *_topLevelBlocks;
    GGBox       _boudingBoxLocal;    // this is the bouding box of all the local blocks.  It does not need to be very accurate.
                                    // for the moment this is only needed when we need to know the size of the local area
                                    // to set up a correct depth range.
@public
    float       sizeSmallBlock;

    /*! @var posObs
    @abstract is the position of the camera in the current frame.  
    
    @discussion This can be the current frame
    of the local part or the global part.
    */
    Vect3D		posObs;
    
    /*! @var posLocalFrame
      @abstract  represents the position of the frame containing the local part.
      @discussion  It is only used
        to compute the Decal block parameter.  The position is measured in the frame of the data source.
        */
    Vect3D		posLocalFrame;
    
    int			nFrame;
    Frustum		frustum;
    NSMutableArray	*forLocal, *localStudy;
    NSMutableArray	*forGlobal;
    
#ifdef DEBUG    
    GGBlock         **currentStack;
    NSMutableArray  *_pathToDebugBlock;
    int             _currentDepth;
    int             sizeCurrentStack;
    GGBlock         *_fatherCurrent;
    GGBlock         *_currentBlock;
    GGBlock         *_closest;
    double          _distClosest;
    BOOL            _lookForClosest;
    int             _icur, _jcur;
#endif    
}
- (Texture *)texture;
- (void)setTexture:(Texture *)value;

- (float)sizeSmallBlock;
- (void)setSizeSmallBlock:(float)value;

- (GGSolid*) createDynamicalHelper;
- (GGSolid *)solid;
- (void)setSolid:(GGSolid *)value;

- (void) rasteriseGlobalPartWithCamera:(Camera *)cam
                            globalPart:(GGSpaceObject*) global;
- (void) rasteriseLocalPart:(GGSpaceObject*) local
                     camera:(Camera *)cam;
                     
- (GGBox) boudingBoxForLocalBlocks;

- (void) rasterizeLocalsAndClean;
- (void) rasterizeGlobalsAndClean;

- (void) setTopLevelBlocks:(NSArray*)blocks;
- (NSArray*) topLevelBlocks;

#ifdef DEBUG
- (void) checkBlocks;
#endif
@end

#endif