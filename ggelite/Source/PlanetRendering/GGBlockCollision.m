//
//  GGBlockCollision.m
//  elite
//
//  Created by Frédéric on 27/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGBlockCollision.h"
#import "GGSolid.h"
#import "GGBlockContext.h"

@implementation GGBlock (CollisionProtocol)
- (GGSolid*)solid
{
    NSAssert([manager solid], @"should have a solid there");
    return [manager solid];
}

- (GGReal) diameterAtIndex:(unsigned)i index:(unsigned)j size:(unsigned)size
{
    NSParameterAssert(size > 0);
    NSParameterAssert(i+size < SIZEMIP);
    NSParameterAssert(j+size < SIZEMIP);

    GGReal diameter = sqrt(distance2Vect(&data[i][j+size].v, 
                                         &data[i+size][j].v));
    
    return diameter;
}

- (GGReal) diameter
{
    GGReal diameter = sqrt(distance2Vect(&data[0][SIZEMIP-1].v, 
                                  &data[SIZEMIP-1][0].v));
    
    return diameter;
}

- (NSArray*)subNodes
{
    NSAssert(!_invalidated, @"requested for nodes while invalidated");
    NSMutableArray* ret = [NSMutableArray array];
    if(nil == _blockCollision){
        _blockCollision = [[GGBlockCollision alloc] initWithBlock: self];
    }

    if(nSonSplit < BRANCHEMENT*BRANCHEMENT){
        [ret addObject:_blockCollision];
    }
    
    int i;
    for(i = 0; i < BRANCHEMENT; ++i){
        int j;
        for(j = 0; j < BRANCHEMENT; ++j){
            if(fils[i][j].block){
                [ret addObject:fils[i][j].block];
            }
        }
    }
        
    return ret;
}

- (void) computeCacheTransformed
{
    
}

- (const struct __BoundingBox) tranformedBoundingBoxCacheForI:(unsigned)i j:(unsigned)j size:(unsigned)size
{
    BoundingBox boxRet; 
    GGReal xMin, xmax, yMin, ymax, zmin, zmax;
    Vect3D temp;
    Vect3D min, max;
    BoundingBox final;
    
    temp = frame.decal;
    
    Boxf *box = &boundingBox[(2*i)+size][(2*j) + size].theBox;
    xMin = box->lomin;
    xmax = box->lomax;
    
    yMin = box->lamin;
    ymax = box->lamax;
    
    zmin = box->hmin;
    zmax = box->hmax;
    
    initVect(&min, xMin, yMin, zmin);
    mulScalVect(&min, frame.size, &min);
    addVect(&temp, &min, ggArrayToVect3d(boxRet.min));

    initVect(&max, xmax, ymax, zmax);
    mulScalVect(&max, frame.size, &max);
    addVect(&temp, &max, ggArrayToVect3d(boxRet.max));
    
    GGSolid* solid = [manager solid];
    GGMatrix4* pMatrix = &solid->_position;
    
    ggTransformBox(&boxRet, pMatrix, &final);
    
    return final;
}

- (const struct __BoundingBox) tranformedBoundingBoxCache
{
    return [self tranformedBoundingBoxCacheForI:0 j:0 size:BRANCHEMENT];
}

- (void)_preComputeCacheVerticesIfNeeded
{
    
}

- (void) _preComputeCacheFacesIfNeeded
{
    
}

    // mesh access
- (unsigned) numberOfVertices
{
    return 0;
}

- (struct __cacheVertex*) verticesCache
{
    return NULL;
}

- (unsigned) numberOfFaces
{
    return 0;
}

- (GGPlan*) planeFacesCache
{
    return NULL;
}

- (FaceIndices*) faces
{
    return NULL;
}


- (BOOL) nodeIntersectBox:(const struct __BoundingBox*)box
{
    return YES;
}
#ifdef DEBUG
- (void) preDrawStuff{}
- (void) dontDraw{}

- (void) rasterizeIfNeeded{}
#endif
@end

@implementation GGSubBlockHelper
- (id) initWithBlockCollision:(GGBlockCollision*)owner atXPos:(unsigned)xpos yPos:(unsigned)ypos size:(unsigned)size
{
    _owner = owner;
    _info.xpos = xpos;
    _info.ypos = ypos;
    _info.size = size;
    return self;
}

- (unsigned) xPos
{
    return _info.xpos;
}

- (unsigned) yPos
{
    return _info.ypos;
}

- (unsigned) size
{
    return _info.size;
}

#pragma mark Protocol
- (GGSolid*)solid
{
    return [_owner solid];
}

- (GGReal) diameter
{
    return [[_owner block] diameterAtIndex:_info.xpos index:_info.ypos size:_info.size];
}

static void myAddSubStuff(GGBlockCollision* owner, unsigned i, unsigned j, NSMutableArray* result)
{
    GGBlock* block = [owner block];
    id<GGCollisionNode>  node = GGBlockGetSubBlock(block,i,j);
    if(nil == node){
        node = [owner subHelperForXPos:i yPos:j size:1];
    }
    NSCAssert(node, @"could not find a node");
    [result addObject:node];
}

- (NSArray*)subNodes
{
    if(_info.size >= 4){
        NSMutableArray* answer = [NSMutableArray arrayWithCapacity:4];
        // we don't handle the case where the current block has a non nil lod
        unsigned newSize = _info.size / 2;
        [answer addObject:[_owner subHelperForXPos:_info.xpos yPos:_info.ypos size:newSize]];
        [answer addObject:[_owner subHelperForXPos:_info.xpos+newSize yPos:_info.ypos size:newSize]];
        [answer addObject:[_owner subHelperForXPos:_info.xpos yPos:_info.ypos+newSize size:newSize]];
        [answer addObject:[_owner subHelperForXPos:_info.xpos+newSize yPos:_info.ypos+newSize size:newSize]];
        return answer;
    }
    else if (_info.size == 2) {
        NSMutableArray* answer = [NSMutableArray arrayWithCapacity:4];
        myAddSubStuff(_owner,_info.xpos,_info.ypos,answer);
        myAddSubStuff(_owner,_info.xpos+1,_info.ypos,answer);
        myAddSubStuff(_owner,_info.xpos,_info.ypos+1,answer);
        myAddSubStuff(_owner,_info.xpos+1,_info.ypos+1,answer);
        
        return answer;
    }
    else {
        NSAssert(_info.size == 1, @"bad size");
        NSAssert(GGBlockGetSubBlock([_owner block], _info.xpos, _info.ypos) == nil, @"inconsistency");
        return nil;
    }
}

    // for the bounding box computation.
    // XXX needs to be cleaned
- (void) computeCacheTransformed{}

- (const struct __BoundingBox) tranformedBoundingBoxCache
{
    return [[_owner block] tranformedBoundingBoxCacheForI:_info.xpos j:_info.ypos size:_info.size];
}

- (void)_preComputeCacheVerticesIfNeeded
{}
- (void) _preComputeCacheFacesIfNeeded
{}

    // mesh access
- (unsigned) numberOfVertices
{
    return _info.size == 1 ? 4 : 0;
}

- (struct __cacheVertex*) verticesCache
{
    return NULL;
}

- (unsigned) numberOfFaces
{
    return _info.size == 1 ? 2 : 0;
}

- (FaceIndices*) faces
{
    return NULL;
}

- (GGPlan*) planeFacesCache
{
    return NULL;
}

@end

@implementation GGBlockCollision
- (id) initWithBlock:(GGBlock*)block
{
    _owner = block;
    
    // initialize subblock
    unsigned size;
    for(size = 1; size <= BRANCHEMENT; size *= 2){
        unsigned x;
        for(x = 0; x < LNBRANCHEMENT; x+=size){
            unsigned y;
            for(y = 0; y < LNBRANCHEMENT; y += size){
                GGSubBlockHelper* helper = (GGSubBlockHelper*)&_subblocks[2*x+size][2*y+size];
                _subblocks[2*x+size][2*y+size].isa = [GGSubBlockHelper class];
                [helper initWithBlockCollision:self atXPos:x yPos:y size:size];
            }
        }
    }

    return self;
}

- (void) dealloc {
    NSAssert(nil == _subNodes, @"there should have been reset");
    [super dealloc];
}

- (GGBlock*) block
{
    return _owner;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"Mesh(%@)", _owner];
}


- (void) unsetup
{
    DESTROY(_subNodes);
    _owner = nil;
}

- (GGSubBlockHelper*) subHelperForXPos:(unsigned)xpos yPos:(unsigned)ypos size:(unsigned)size
{
    NSParameterAssert(size>0);
    NSParameterAssert(xpos+size<SIZEMIP);
    NSParameterAssert(ypos+size<SIZEMIP);
    GGSubBlockHelper* result = (GGSubBlockHelper*)&_subblocks[2*xpos+size][2*ypos+size];
    NSAssert(xpos == [result xPos], @"inconsistency");
    NSAssert(ypos == [result yPos], @"inconsistency");
    NSAssert(size == [result size], @"inconsistency");
    
    return result;
}

- (void) setSubNode:(NSArray*)value
{
    if(nil == _subNodes){
        _subNodes = [value mutableCopy];
    }
    else {
        [_subNodes setArray:value];
    }
}


- (GGSolid*)solid
{
    return [_owner solid];
}

- (GGReal) diameter
{
    return [_owner diameter];
}

- (NSArray*)subNodes
{
    return nil;
}

    // for the bounding box computation.
    // XXX needs to be cleaned
- (void) computeCacheTransformed
{
    
}

- (const struct __BoundingBox) tranformedBoundingBoxCache
{
    return [_owner tranformedBoundingBoxCache];
}

- (void) _computePlanetMesh
{
    if(GGCMGeneration != _generation){
        GGSolid* solid = [_owner->manager solid];
        GGMatrix4* pMatrix = solid ? &solid->_position : NULL;
        _generation = GGCMGeneration;
        NSAssert([_owner isLocal], @"block must be local");
        double flod = blockLod(_owner);
        int lod = (int)flod;
        int inc = 1<<lod;
        int lineWidth = (BRANCHEMENT/inc)+1;;
        _numberOfVertices = lineWidth*lineWidth;
        CacheVertex (*vertices)[lineWidth] = (void*)&_vertices[0];
        
        int i, irel;
        for(i = 0, irel=0; irel < lineWidth; i += inc, irel++){
            int j, jrel;
            for(j = 0, jrel = 0; jrel < lineWidth; j+=inc, jrel++){
                Vect3D tmpVerticePos;
                addVect(&_owner->data[i][j].v,&_owner->frame.decal,&tmpVerticePos);
                if(pMatrix){
                    transformePoint4D(pMatrix,&tmpVerticePos,&vertices[irel][jrel].pos);
                }
                else {
                    vertices[irel][jrel].pos = tmpVerticePos;
                }
            }
        }
        
        int facePerLine = lineWidth-1;
        unsigned nbrOfFaces = 0;
        NSAssert(facePerLine <= BRANCHEMENT, @"inconsistency");
        
        for(i = 0; i < facePerLine; i++){
            int j;
            for(j = 0; j < facePerLine; j++){
                // FIX that condition
                if(inc > 1 || GGBlockGetSubBlock(_owner, i, j) == nil){
                    NSCAssert(nbrOfFaces + 1 < 2*BRANCHEMENT*BRANCHEMENT, @"overflow");
                    FaceIndices* fi = &_faces[nbrOfFaces];
                    GGPlan* planPtr = &_planes[nbrOfFaces];
                    Vect3D *A, *B, *C;
                    fi[0].A = i*lineWidth+j;
                    A = &_vertices[fi[0].A].pos;
                    fi[0].B = (i+1)*lineWidth+j;
                    B = &_vertices[fi[0].B].pos;
                    fi[0].C = i*lineWidth+j+1;
                    C = &_vertices[fi[0].C].pos;
                    planPtr[0] = mkPlanFromTriangles(*A,*B,*C);

                    fi[1].A = (i+1)*lineWidth+j;
                    A = &_vertices[fi[0].A].pos;
                    fi[1].B = (i+1)*lineWidth+j+1;
                    B = &_vertices[fi[0].B].pos;
                    fi[1].C = i*lineWidth+j+1;
                    C = &_vertices[fi[0].C].pos;
                    planPtr[1] = mkPlanFromTriangles(*A,*B,*C);
                    nbrOfFaces += 2;
                }
            }
        }   
        _numberOfFaces = nbrOfFaces;     
    }
}

- (void) drawDebug
{
//    rasterizeTrianglesForNode(self);
}

- (void)_preComputeCacheVerticesIfNeeded
{
    [self _computePlanetMesh];
}

- (void) _preComputeCacheFacesIfNeeded
{
    [self _computePlanetMesh];    
}
    // mesh access
- (unsigned) numberOfVertices
{
    NSAssert(GGCMGeneration == _generation, @"expired cache");
    return _numberOfVertices;
}

- (struct __cacheVertex*) verticesCache
{
    NSAssert(GGCMGeneration == _generation, @"expired cache");
    return _vertices;
}

- (unsigned) numberOfFaces
{
    NSAssert(GGCMGeneration == _generation, @"expired cache");
    return _numberOfFaces;
}

- (FaceIndices*) faces
{
    NSAssert(GGCMGeneration == _generation, @"expired cache");
    return _faces;
}

- (GGPlan*) planeFacesCache
{
    NSAssert(GGCMGeneration == _generation, @"expired cache");
    return _planes;
//    [NSException raise:NSInternalInconsistencyException format:@"deprecated"];
//    return NULL;
}

#ifdef DEBUG
- (void) preDrawStuff;
{
    
}

- (void) dontDraw
{
    
}

- (void) rasterizeIfNeeded
{
    
}
#endif

@end

@implementation GGSolidPlanet
- (id) initWithContext:(GGBlockContext*)block
{
    self = [super init];
    if(self){
        _context = block;
        [self setInfiniteMass];
    }
    
    return self;
}

- (id<GGCollisionNode>) topLevelCollisionNodeInManager:(GGCollisionManager*)manager
{
    return self;
}

- (GGSolid*)solid
{
    return self;
}

- (GGReal) diameter
{
    return 1e10;
}

- (NSArray*)subNodes
{
    return _context->localStudy;
}

    // for the bounding box computation.
    // XXX needs to be cleaned
- (void) computeCacheTransformed{
}

- (const struct __BoundingBox) tranformedBoundingBoxCache
{
    static const BoundingBox res = {
    {-1e10, -1e10, -1e10},
    {1e10, 1e10, 1e10},
    };
    return res;
}

- (void)_preComputeCacheVerticesIfNeeded
{
    
}

- (void) _preComputeCacheFacesIfNeeded
{
    
}

    // mesh access
- (unsigned) numberOfVertices
{
    return 0;
}

- (struct __cacheVertex*) verticesCache
{
    return NULL;
}

- (unsigned) numberOfFaces
{
    return 0;
}

- (FaceIndices*) faces
{
    return NULL;
}

- (GGPlan*) planeFacesCache
{
    return NULL;
}

#ifdef DEBUG
- (void) preDrawStuff{
    
}
- (void) dontDraw
{
    
}

- (void) rasterizeIfNeeded{
    
}
#endif

@end