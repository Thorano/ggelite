//
//  GGProceduralLandscape.h
//  elite
//
//  Created by Frederic De Jaeger on 12/08/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GG3D.h"
#import "GGBump.h"
// input : 3D coordinates.

typedef enum __TerrainType
{
    TerrainTypeSea = 0,
    TerrainTypeGrass,
    TerrainTypeMountain,
}TerrainType;

@class GGNoise;

@interface GGProceduralLandscape : NSObject <TerrainSource>{
    GGNoise*                _continentalNoise;
    GGNoise*                _mediumFrequency;
    GGNoise*                _highFrequency;
    
    double                  _seaLimit;
    
    // define the  probability to NOT have a mountain on the cost.
    double                  _probaMountainFactor;
    double                  _probaMountainFactorFarSea;
    
    double                  _highestMountain;
    double                  _highestValley;
    double                  _highestHill;
}

- (GGNoise *)continentalNoise;
- (void)setContinentalNoise:(GGNoise *)value;

- (GGNoise *)highFrequency;
- (void)setHighFrequency:(GGNoise *)value;

- (double)seaLimit;
- (void)setSeaLimit:(double)value;

- (double)probaMountainFactor;
- (void)setProbaMountainFactor:(double)value;

- (double)probaMountainFactorFarSea;
- (void)setProbaMountainFactorFarSea:(double)value;

- (double)highestMountain;
- (void)setHighestMountain:(double)value;

- (double)highestValley;
- (void)setHighestValley:(double)value;

- (double)highestHill;
- (void)setHighestHill:(double)value;


- (double) altitudeAtX:(double)x
                     y:(double)y
                     z:(double)z
           gradientRef:(double*) gradientRef
        terrainTypeRef:(int*) terrainTypeRef
              colorRef:(VectCol*)colorRef;
@end
