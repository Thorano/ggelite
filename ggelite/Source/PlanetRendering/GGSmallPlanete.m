//
//  GGSmallPlanete.m
//  elite
//
//  Created by Frédéric De Jaeger on 27/12/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGSmallPlanete.h"

#import "GGBlockContext.h"
#import "GGBlock.h"
#import "GGLog.h"
#import "utile.h"
#import "GGRepere.h"
#import "GGProceduralLandscape.h"

@interface GGSmallPlanete (ReallyPrivate)
- (NSArray*) makeTopBlocks;
@end

@implementation GGSmallPlanete
- (void) _ggsp_setupIvar
{
    _context = [[GGBlockContext alloc] init];
    [_context setTopLevelBlocks:[self makeTopBlocks]];
}

- (id) init
{
    self = [super init];
    if(self){
        [self _ggsp_setupIvar];
        [self setVisible:YES];
        [self setName:@"ExpModel"];
        _flags.graphical = YES;
        masse = 1e9;
        
    }
    return self;
}

- (void) encodeWithCoder:(NSCoder*)encoder
{
    [super encodeWithCoder:encoder];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    [self _ggsp_setupIvar];
    return self;
}

- copyWithZone: (NSZone *)zone
{
    [NSException raise: NSInternalInconsistencyException
                format: @"%@ should not be copied", self];
    return nil;
}

- (void) dealloc {
    [_context release];
    [_landscape release];
    [super dealloc];
}

- (GGSolid*) createDynamicalHelper
{
    return [_context createDynamicalHelper];
}

- (BOOL) shouldCache
{
    return NO;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

- (BOOL) castShadow
{
	return NO;
}

- (id<TerrainSource>) landscape
{
   if(nil == _landscape){
       _landscape = [GGProceduralLandscape new];
       [_landscape setSeaLimit:-1];
   }
   
   return _landscape;
//    GGNoise* noise =  [GGNoise noise];
//    [noise setScaleInput:1.0/500.0];
//    [noise setScaleOutput:500.0];
//    [noise setAlpha:2.19];
//    [noise setNharmonic:4];
//    [noise setBeta:2.1];
//    
//    return noise;
}

- (NSArray*) makeTopBlocks
{
    id<TerrainSource> noise = [self landscape];
    PerlinBumpCube *  bump = [PerlinBumpCube bumpWithNoise:noise];
    [bump setScaleFactor:1000];
    [_context setSizeSmallBlock:3];
    
    GGBlock* _b1 = [GGBlock topBlockWithManager: _context
                                        bump: bump
                                          at: -1
                                         and: -1
                                       width: 2
                                      height: 2];
    [_b1 setIsLocal:YES];
    return [NSArray arrayWithObject:_b1];
}

- (void) rasterise: (Camera *)cam
{
    glColor3f(1,1,1);
    {        
        NSDebugMLLogWindow(@"blaa", @"%@", stringOfMatrice(Matrice(self)));
        
        makeFrustum(&_context->frustum, cam);
        
        transposeTransformePoint4D(&cam->modelView, &GGZeroVect, &_context->posObs);
        _context->nFrame ++;
        
        NSDebugMLLogWindow(@"GGBlock", @"nFrame = %d pos : %@\n%@\n%@",
                           _context->nFrame, stringOfVect(&cam->modelView.position),
                           stringOfVect(&_context->posObs), stringOfVect(Point(self)));
        
        [GGBlock initGLContextBeforeDrawing: [_context texture]];
        
        [_context->localStudy removeAllObjects];
        [[_context topLevelBlocks] makeObjectsPerformSelector:@selector(rasterize)];
        NSAssert(0 == [_context->localStudy count], @"we should not have that");
        
        // this is needed by the collision engine.
        [_context->localStudy addObjectsFromArray:[_context topLevelBlocks]];
        
        NSAssert([_context->forGlobal count] == 0, @"no global stuff created");
        
        [_context rasterizeLocalsAndClean];
        [GGBlock finish];
        //  rasterizeArrayAndClean(_context->forLocal);
    }
}
@end
