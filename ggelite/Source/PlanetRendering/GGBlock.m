/*
 *  GGBlock.m
 *
 *  Copyright (c) 2002 by Frédéric De Jaeger
 *  
 *  Date: January 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#define GL_GLEXT_PROTOTYPES

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <math.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSException.h>
#import <Foundation/NSString.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSDebug.h>
#import "GGBlock.h"
#import "utile.h"
#import "GG3D.h"
#import "perlin.h"
#import "Metaobj.h"
//#import "GGScene.h"
#import "Tableau.h"
#import "Texture.h"
#import "GGLoop.h"
#import "Planete.h"
#import "GGBlockCollision.h"
#import "GGBlockContext.h"

#import GLPath(glext.h)

/**
There are two kinds of block:
 - The local one.  They are small and close to the camera.  Thus, there 
 should be drawn in the local frame, with the depth test on.
 -  The other.  They are big structures and should not be in the 
 local frame.
 
 The algorithm processes in two passes.  
 During the first pass, we go through the global blocks and computes lods
 and stuffs like that.  When we encounter a local block (if we go deep, and
                                                         the observer is close to the surface of the planet, this will happen), we
 add it to context->localStudy.  They will be process later.  The idea is 
 we must compute another frustum for local study.  Thus we accumulate all
 the local blocks and process them after.
 
 Then we process all the accumulated blocks in context->localStudy (It is 
                                                                    still the first pass).
 
 Each time a block may be visible, it is add to context->forGlobal or 
 context->forLocal (whether it is local or not) for the second pass.
 
 
 During the second pass, we first scan the global blocks and rasterize them.
 Then, when the local frame is to be rasterized, scan for the local blocks
 and rasterize them.
 
 
 */


//#define HardDebug

#define CheckIndex(i, j) NSCParameterAssert(0 <= (i) && (i) < BRANCHEMENT && (j)>= 0 && (j) < BRANCHEMENT)

#ifdef DEBUG

// ivar to collect stats.
static int nTriangle;
static int nNode;
static int nPointInFrustum;
static int nBoxInFrustum;
static int nPass1, nPass2;
static int nPlanTest;

static inline void ggcheck(int i, int j)
{
    NSCParameterAssert(0 <= i && i < SIZEMIP && j >= 0 && j < SIZEMIP);
}
#else
#define ggcheck(i,j)
#endif

#undef Haut
#undef Gauche

#define Bas (block->bas)
#define Haut (block->haut)
#define Droite (block->droite)
#define Gauche (block->gauche)
#define Xmin (block->xmin)
#define Ymin (block->ymin)
#define Xmax (Xmax+Width)
#define Ymax (Ymin+Height)
#define Height (block->height)
#define Width (block->width)
#define Fils (block->fils)
#define Father (block->father)
#define Depth (block->depth)
#define Lod (block->lod)
#define Epsilon  (block->epsilon)
#define Nhaut (block->nhaut)
#define Nbas (block->nbas)
#define Ndroite (block->ndroite)
#define Ngauche (block->ngauche)
#define NSonSplit (block->nSonSplit)
#define Data (block->data)
#define Longitude (block->frame.rep.direction)
#define Latitude (block->frame.rep.gauche)
#define Altitude (block->frame.rep.haut)
#define BoundingBox(i, j)  (block->boundingBox[i][j].theBox)
#define BoundingBoxExt(i, j)  (block->boundingBox[i][j])
#define Bump (block->bump)
#define Local (block->local)
#define Manager (block->manager)
#define Origin (block->origin)
#define Decal (block->frame.decal)
#define VertexModel(i,j) (ggcheck(i,j), &Data[i][j].v)
#define Splitable (block->_splitable)
#define fatherCurrent (block->manager->_fatherCurrent)
#define icur (block->manager->_icur)
#define jcur (block->manager->_jcur)
#define lookForClosest (block->manager->_lookForClosest)
#define distClosest (block->manager->_distClosest)
#define closest (block->manager->_closest)
#define currentBlock (block->manager->_currentBlock)

#define LNSIZEMIP LNBRANCHEMENT

#ifdef DEBUG

@interface GGBlock (Debug)
- (void) checkSons;
- (void) checkTree: (GGBlock*) pere
                at: (int) i
               and: (int) j;
@end
#endif

static float lodConst = 30;
static const float constLocality = 60e3;

//static const float constLocality = 10;
static void drawBorder(GGBlock *block);

static inline int fatherIndex(int x)
{
    register int i;
    NSCParameterAssert(x > 0 && x < SIZEBOX);
    
    for(i = 1; (i & x) == 0; i <<= 1 );
    
    return (x ^ i) | (i<<1);
}

static Boxf  boxForPoint(Frame *f, Vect3D *pt)
{
    Boxf b;
    Vect3D tmp;
    tmp = *pt;
    divScalVect(&tmp, f->size, &tmp);
    NSCParameterAssert(fabs(tmp.x) < SHRT_MAX &&
                       fabs(tmp.y) < SHRT_MAX &&
                       fabs(tmp.z) < SHRT_MAX );
    b.lomin = b.lomax = tmp.x;
    b.lamin = b.lamax = tmp.y;
    b.hmin = b.hmax = tmp.z;
    
    return b;
}

static inline void improveBoxFrame(Boxf *b,  Frame *f, Vect3D *p)
{
    Vect3D tmp;
    short int x, y, z;
    tmp = *p;
    divScalVect(&tmp, f->size, &tmp);
    
    NSCParameterAssert(fabs(tmp.x) < SHRT_MAX &&
                       fabs(tmp.y) < SHRT_MAX &&
                       fabs(tmp.z) < SHRT_MAX );
    
    
    x = tmp.x;
    y = tmp.y;
    z = tmp.z;
    
    if( x > b->lomax )
        b->lomax = x;
    if( x < b->lomin )
        b->lomin = x;
    
    if( y > b->lamax )
        b->lamax = y;
    if( y < b->lamin )
        b->lamin = y;
    
    if( z > b->hmax )
        b->hmax = z;
    if( z < b->hmin )
        b->hmin = z;
}


static inline BOOL improveInt(short int *a, short int  *b, 
                              short int a2, short int  b2)
{
    BOOL ret = NO;
    if( a2 < *a )
    {
        *a = a2;
        ret = YES;
    }
    if( b2 > *b )
    {
        *b = b2;
        ret = YES;
    }
    return ret;
}

static inline BOOL improveBoxByBox(Boxf *b, Boxf *q)
{
    BOOL ret1, ret2, ret3;
    ret1 = improveInt(&b->lomin, &b->lomax, q->lomin, q->lomax);
    ret2 = improveInt(&b->lamin, &b->lamax, q->lamin, q->lamax);
    ret3 = improveInt(&b->hmin, &b->hmax, q->hmin, q->hmax);
    return ret1||ret2||ret3;
}

static inline GGReal distanceBlock(const Vect3D* a, const Vect3D* b)
{
    return sqrt(distance2Vect(a,b));
}

#define aDroite 1
#define aGauche 2
#define aHaut 3
#define aBas 4
#define aDevant 5
#define aDeriere 6
#define aDedans 7

#ifdef DEBUG
#define ggpointInPlane(a,b) (nPlanTest++,pointInPlane((a),(b)))
#else
#define ggpointInPlane pointInPlane
#endif

static inline int pointInFrustum(Frustum *f, Vect3D *pt)
/*return the signature of the point relative to the given Frustum
stored into reg
*/
{
    int res=0;
    if( ! ggpointInPlane(&f->droite, pt) )
        res |=  aDroite;
    if( ! ggpointInPlane(&f->gauche, pt) )
        res |=  aGauche;
    if( ! ggpointInPlane(&f->haut, pt) )
        res |=   aHaut;
    if( ! ggpointInPlane(&f->bas, pt) )
        res |=   aBas;
    if( ! ggpointInPlane(&f->devant, pt) )
        res |=   aDevant;
    
#ifdef DEBUG
    nPointInFrustum++;
#endif
    
    return (res ? res : aDedans);
}


static inline int boxInFrustum(Frustum *f, Vect3D pts[8])
{
    int res;
    int i;
#ifdef DEBUG
    nBoxInFrustum++;
#endif
    res = pointInFrustum(f, &pts[0]);
    for(i = 1; i<8; ++i)
    {
        res &= pointInFrustum(f, &pts[i]);
        if( res == 0 )
            return 0;
    }
    if( res & aDedans )
        return 1;
    else
        return -1;
}

/*compute the position of the box relative to the plane
use spatial coherency in order to answer the question with at most
one scalar product
return 1 if the box is completely inside
return -1 if the box is completely outside
else return 0 
*/
static inline int boxInPlane(GGPlan *p, Vect3D pts[8], unsigned index)
{
    if( ggpointInPlane(p, &pts[index]) )
        return 1;
    else if( ! ggpointInPlane(p, &pts[(~index)&7]) )
        return -1;
    else
        return 0;
}

/*compute the position of the box relative to the frustum.
the box is supposed to be axis aligned */
static inline int signatureForBox(Frustum *f, Vect3D pts[8], 
                                  unsigned char cache)
{
    BOOL ok = YES;
    int tmp;
#define TestBox(p,index,sig)  \
    tmp = boxInPlane(&p, pts, index);\
        if( tmp < 0 )\
            return sig;\
                else if (tmp == 0 )\
                    ok = NO
                    
                    switch(cache)
                    {
                        case aDroite:
                            TestBox(f->droite, f->idro, aDroite);
                            TestBox(f->gauche, f->igau, aGauche);
                            TestBox(f->haut, f->ihau, aHaut);
                            TestBox(f->bas, f->ibas, aBas);
                            TestBox(f->devant, f->idev, aDevant);
                            break;
                        case aGauche:
                            TestBox(f->gauche, f->igau, aGauche);
                            TestBox(f->droite, f->idro, aDroite);
                            TestBox(f->haut, f->ihau, aHaut);
                            TestBox(f->bas, f->ibas, aBas);
                            TestBox(f->devant, f->idev, aDevant);
                            break;
                        case aHaut:
                            TestBox(f->haut, f->ihau, aHaut);
                            TestBox(f->droite, f->idro, aDroite);
                            TestBox(f->gauche, f->igau, aGauche);
                            TestBox(f->bas, f->ibas, aBas);
                            TestBox(f->devant, f->idev, aDevant);
                            break;
                        case aBas:
                            TestBox(f->bas, f->ibas, aBas);
                            TestBox(f->droite, f->idro, aDroite);
                            TestBox(f->gauche, f->igau, aGauche);
                            TestBox(f->haut, f->ihau, aHaut);
                            TestBox(f->devant, f->idev, aDevant);
                            break;
                        case aDevant:
                        default:
                            TestBox(f->devant, f->idev, aDevant);
                            TestBox(f->droite, f->idro, aDroite);
                            TestBox(f->gauche, f->igau, aGauche);
                            TestBox(f->haut, f->ihau, aHaut);
                            TestBox(f->bas, f->ibas, aBas);
                            break;
                    }
                    
                    if( ok )
                        return aDedans;
                    else
                        return 0;
}

static inline void initConcreteBox(Frame *frame, Boxf *box, Vect3D qts[8])
/*local*/
{
    GGReal xmin, xmax, ymin, ymax, zmin, zmax;
    Vect3D pts[8];
    int i;
    
    xmin = box->lomin;
    xmax = box->lomax;
    
    ymin = box->lamin;
    ymax = box->lamax;
    
    zmin = box->hmin;
    zmax = box->hmax;
    
    initVect(&pts[0], xmin, ymin, zmin);
    initVect(&pts[1], xmin, ymin, zmax);
    initVect(&pts[2], xmin, ymax, zmin);
    initVect(&pts[3], xmin, ymax, zmax);
    initVect(&pts[4], xmax, ymin, zmin);
    initVect(&pts[5], xmax, ymin, zmax);
    initVect(&pts[6], xmax, ymax, zmin);
    initVect(&pts[7], xmax, ymax, zmax);
    for(i = 0; i < 8; ++i )
    {
        mulScalVect(&pts[i], frame->size, &pts[i]);
        addVect(&frame->decal, &pts[i], &qts[i]);
        //      transformePoint4D(&frame->rep, &pts[i], &qts[i]);
    }
}


static inline int boxInFrustumCache(int val)
{
    if( val == aDedans )
        return 1;
    else if( val )
        return -1;
    else
        return 0;
}

static inline int boxInFrustumFrame(Frustum *frustum, Frame *frame,
                                    BoxExt *box)
{
    Vect3D pts[8];
#ifdef DEBUG
    nBoxInFrustum++;
#endif
    initConcreteBox(frame, &box->theBox, pts);
    box->cache = signatureForBox(frustum, pts, box->cache);
    return boxInFrustumCache(box->cache);
}

static inline Vect3D centerOfBox(const Frame *f, const Boxf *b)
/* give a local local coordinate
*/
{
    Vect3D res;
    initVect(&res, (b->lomin+b->lomax)/2, (b->lamin+b->lamax)/2, 
             (b->hmin+b->hmax)/2);
    mulScalVect(&res, f->size, &res);
    return res;
}

// return the bouding box in the frame of drawing.
static inline GGBox boxForBoxf(const Frame *f, const Boxf *b)
{
    GGBox ret;
    Vect3D tmp = mkVect(b->lomin, b->lamin, b->hmin);
    addLambdaVect(&f->decal, f->size, &tmp, &ret.min);
    tmp = mkVect(b->lomax, b->lamax, b->hmax);
    addLambdaVect(&f->decal, f->size, &tmp, &ret.max);
    return ret;
}

@implementation GGBlock


static Boxf boundingBoxForSon(GGBlock *block, int i, int j)
{
    Frame *f = &block->frame;
    Boxf b;
    
    b = boxForPoint(f, &Data[i][j].v);
    improveBoxFrame(&b, f, &Data[i+1][j].v);
    improveBoxFrame(&b, f, &Data[i][j+1].v);
    improveBoxFrame(&b, f, &Data[i+1][j+1].v);
    return b;
}

static void calcBox(GGBlock *block)
{
    int lod, i,  j;
    for( i = 0; i < BRANCHEMENT; ++ i){
        for( j = 0; j < BRANCHEMENT; ++ j)
        {
            BoundingBox(1+2*i, 1+2*j) = boundingBoxForSon(block, i, j);
            Fils[i][j].pos = centerOfBox(&block->frame, &BoundingBox(1+2*i, 1+2*j));
        }
    }
    
    for(lod = 4; lod <= SIZEBOX; lod <<= 1 ){
        int lod2, lod4;
        lod2 = lod>>1;
        lod4 = lod>>2;
        for( i = lod2; i < SIZEBOX; i+= lod )
            for( j = lod2; j < SIZEBOX; j+= lod )
            {
                BoundingBox(i,j) = BoundingBox(i-lod4, j-lod4);
                improveBoxByBox(&BoundingBox(i,j), &BoundingBox(i-lod4, j+lod4));
                improveBoxByBox(&BoundingBox(i,j), &BoundingBox(i+lod4, j+lod4));
                improveBoxByBox(&BoundingBox(i,j), &BoundingBox(i+lod4, j-lod4));
            }
    }
}

- (void) boxFromSonAt: (int) i0
                  and: (int) j0
                   in: (Boxf *)res
{
    Vect3D temp;
    GGBlock *son = fils[i0][j0].block;
    Vect3D min, max;
    Boxf *box;
    box = &son->boundingBox[BRANCHEMENT][BRANCHEMENT].theBox;
    initVect(&min, box->lomin, box->lamin, box->hmin);
    initVect(&max, box->lomax, box->lamax, box->hmax);
    
    if( !local )
    {
        if( son->local )
            temp = son->origin;
        else
            SetZeroVect(temp);
    }
    else{
        NSAssert(son->local, @"son should be local if we are");
    }
    diffVect(&son->origin, &origin, &temp);

    mulScalVect(&min, son->frame.size, &min);
    addVect(&temp, &min, &min);
    
    mulScalVect(&max, son->frame.size, &max);
    addVect(&temp, &max, &max);
    {
        *res = boxForPoint(&frame, &min);
        improveBoxFrame(res, &frame, &max);
    }
}

- (void) propagateBoxFrom: (int) i0
                      and: (int) j0
{
    Boxf temp;
    Boxf *box;
    int i = 2*i0+1;
    int j = 2*j0+1;
    
    [self boxFromSonAt: i0
                   and: j0
                    in: &temp];
    box = &temp;
    
    while( i < SIZEBOX && j < SIZEBOX )
    {
        if (improveBoxByBox(&boundingBox[i][j].theBox,box) )
        {
            box = &boundingBox[i][j].theBox;
            i = fatherIndex(i);
            j = fatherIndex(j);
        }
        else
            return;
    }
    [father propagateBoxFrom: frame.i
                         and: frame.j];
}

static inline float triangleRoughness(Vect3D *A, Vect3D *B, Vect3D *C)
{
    float res;
    Vect3D AB, AC;
    Vect3D tmp;
    mkVectFromPoint(A, B, &AB);
    mkVectFromPoint(A, C, &AC);
    prodVect(&AB,&AC,&tmp);
    res = prodScal(&tmp, &tmp)/prodScal(&AB,&AB);
    return sqrt(res);
}

static float localRoughness(GGBlock *block, int i, int j, int size)
{
    float max;
    float tmp;
    
#define Triangle(i,j) (&Data[i][j].v)
    
    max = triangleRoughness(Triangle(i-size, j-size), Triangle(i+size, j-size),
                            Triangle(i, j-size));
    
    tmp = triangleRoughness(Triangle(i-size, j+size), Triangle(i+size, j+size),
                            Triangle(i, j+size));
    max = (tmp > max ? tmp : max);
    
    tmp = triangleRoughness(Triangle(i-size, j-size), Triangle(i-size, j+size),
                            Triangle(i-size, j));
    max = (tmp > max ? tmp : max);
    
    tmp = triangleRoughness(Triangle(i+size, j-size), Triangle(i+size, j+size),
                            Triangle(i+size, j));
    max = (tmp > max ? tmp : max);
    
    tmp = triangleRoughness(Triangle(i-size, j-size), Triangle(i+size, j+size),
                            Triangle(i, j));
    max = (tmp > max ? tmp : max);
    
    tmp = triangleRoughness(Triangle(i-size, j+size), Triangle(i+size, j-size),
                            Triangle(i, j));
    max = (tmp > max ? tmp : max);
#undef Triangle
    
    return max;
}

static void calcRoughness(GGBlock *block)
{
    int lod, i,  j, nlod;
    float max = 0;
    for( lod = 4, nlod = 1; lod <= SIZEBOX; lod *= 2, nlod++ )
    {
        int lod2, lod4;
        lod2 = lod>>1;
        lod4 = lod>>2;
        for( i = lod2; i < SIZEBOX; i+= lod )
            for( j = lod2; j < SIZEBOX; j+= lod )
            {
                float tmp, tmp2, d;
                BoxExt *box;
                Vect3D pos;
                Vect3D pos2;
                
                tmp = localRoughness(block, i>>1, j>>1, lod4)/block->frame.size;
                pos = centerOfBox(&block->frame, &BoundingBox(i,j));
                
#define  SubRough(i,j)\
                box = &BoundingBoxExt((i), (j)); \
                    pos2 = centerOfBox(&block->frame, &box->theBox); \
                        d = distanceBlock(&pos, &pos2); \
                            tmp2 = d/(block->frame.size*lodConst) + box->roughness; \
                                tmp = (tmp2>tmp ? tmp2 : tmp);\
                                    NSCParameterAssert(tmp < 60000)
                                    
                                    
                                    SubRough(i-lod4, j-lod4);
                                SubRough(i+lod4, j+lod4);
                                SubRough(i-lod4, j+lod4);
                                SubRough(i+lod4, j-lod4);
                                
                                BoundingBoxExt(i,j).roughness = tmp;
                                
                                tmp += distanceBlock(&pos, &GGZeroVect)/(block->frame.size*lodConst);
                                max = (tmp > max ? tmp : max);
            }
                NSCParameterAssert(nlod < SIZEEPS);
        NSCAssert1(max < 60000, @"t = %g", max);
        Epsilon[nlod] = (unsigned short)max;
        max *= 1.1;
    }
    Epsilon[SIZEEPS-1] = (Epsilon[SIZEEPS-2]*3)>>1;
}

static inline float roughness(VertexData dataTemp[SIZEMIP][SIZEMIP], 
                              Vect3D *mid,
                              int i, int j)
{
    float max;
    float tmp;
    
#define Triangle(i,j) (&dataTemp[i][j].v)
    
    max = triangleRoughness(Triangle(i-1, j-1), Triangle(i, j), mid);
    
    tmp = triangleRoughness(Triangle(i-1, j), Triangle(i, j-1), mid);
    max = (tmp > max ? tmp : max);
    
#undef Triangle
#undef size
    
    return max;
}

static float sEpsilon = 22000.0/(BRANCHEMENT*30);

static inline void addForNorm(Vect3D* base, Vect3D* v1, Vect3D* v2, Vect3D* res)
{
    Vect3D tmp1, tmp2, tmp;
    diffVect(v2,base,&tmp2);
    diffVect(v1,base,&tmp1);
    prodVect(&tmp1,&tmp2,&tmp);
    addVect(res,&tmp,res);
}

#pragma mark init

static BOOL RecomputeNormal = YES;
static void initBlock(GGBlock *block)
{
    double *rel;
    double reld[3];
    int i, j;
    float scale;
    float err0;
    float diameter;
    float maxRoughSon = 0;
    SEL aSel;
    
    double xinc, yinc;
    double xinc2, yinc2;
    void (*GetPosNormalCol)(id, SEL, double, double, VertexData *,   double *);
    scale = Width/(BRANCHEMENT*3);
    
    xinc = Width/BRANCHEMENT;
    yinc = Height/BRANCHEMENT;
    
    xinc2 = xinc/2;
    yinc2 = yinc/2;
    
    aSel = @selector(dataAt:and:in:relative:);
    
    GetPosNormalCol = (void (*)(id, SEL, double, double, VertexData *,  double *))
        [(id)Bump methodForSelector: aSel];
    
    [Bump vertexAt: Xmin+(SIZEMIP>>1)*xinc
               and: Ymin+(SIZEMIP>>1)*yinc
          inDouble: reld];
    
    //   GetPosNormalCol(Bump, aSel, Xmin+(SIZEMIP>>1)*xinc, Ymin+(SIZEMIP>>1)*yinc,
    // 		  &Data[SIZEMIP>>1][SIZEMIP>>1], NULL);
    GetPosNormalCol(Bump, aSel, Xmin+(BRANCHEMENT)*xinc, Ymin, 
                    &Data[SIZEMIP-1][0], NULL);
    GetPosNormalCol(Bump, aSel, Xmin, Ymin+BRANCHEMENT*yinc, 
                    &Data[0][SIZEMIP-1], NULL);
    //  Pos = Data[SIZEMIP>>1][SIZEMIP>>1].v;
    diameter = sqrt(distance2Vect(&Data[0][SIZEMIP-1].v, 
                                  &Data[SIZEMIP-1][0].v));
    if(diameter < Manager->sizeSmallBlock){
        Splitable = NO;
    }
    else{
        Splitable = YES;
    }
    Local = ( diameter <= constLocality ? YES : NO);
    block->frame.size = diameter/10000;
    initVect(&Origin, reld[0], reld[1], reld[2]);
    rel = reld;
    if( Local )
    {
        diffVect(&Origin, &Manager->posLocalFrame, &Decal);
    }
    else
    {
        Decal = Origin;
    }
    
    for( i = 0; i < SIZEMIP; ++i )
    {
        double x, y;
        x = Xmin+i*xinc;
        y = Ymin+i*yinc;
        GetPosNormalCol(Bump, aSel, x, Ymin, &Data[i][0], rel);
        GetPosNormalCol(Bump, aSel, Xmin, y, &Data[0][i], rel);
    }
    
    
    // this will produce a value of 45 with is good as the lower limit
    // for a splig.
    err0 = sEpsilon;
    
    
    for( i = 1; i < SIZEMIP; ++i){
        for( j = 1; j< SIZEMIP; ++j)
        {
            float err, tmp;
            double x, y;
            VertexData temp;
            
            err = err0;
            x = Xmin+i*xinc;
            y = Ymin+j*yinc;
            GetPosNormalCol(Bump, aSel, x, y, &Data[i][j], rel);
            GetPosNormalCol(Bump, aSel, x-xinc2, y-yinc2, &temp, rel);
            tmp = roughness(Data, &temp.v, i, j)/block->frame.size;
            if(tmp > err)
                err = tmp;
            NSCAssert3(err < 60000, @"roughness = %g, %@ %@", err, 
                       stringOfVect(&Data[i][j].v), stringOfVect(&temp.v));
            //	Fils[i-1][j-1].err = err;
            //err *= 1.5;
            BoundingBoxExt(2*(i-1)+1,2*(j-1)+1).roughness = err;
            
            err += distanceBlock(&GGZeroVect, &temp.v)/(block->frame.size*lodConst);
            maxRoughSon = (err > maxRoughSon ? err : maxRoughSon);
        }
    }
// this piece of code computes the gradient of the surface directly.    
#if 1
    if(RecomputeNormal)
    for(i = 0; i < SIZEMIP; ++i){
        for(j = 0; j< SIZEMIP; ++j)
        {
            Vect3D nord, sud, est, ouest;
            if(i>0){
                diffVect(VertexModel(i-1,j),VertexModel(i,j),&ouest);
            }
            else{
                VertexData temp;
                GetPosNormalCol(Bump, aSel, Xmin - xinc, Ymin+j*yinc, &temp, rel);
                diffVect(&temp.v,VertexModel(i,j),&ouest);
            }
            
            if(i < SIZEMIP-1){
                diffVect(VertexModel(i+1,j),VertexModel(i,j),&est);
            }
            else{
                VertexData temp;
                GetPosNormalCol(Bump, aSel, Xmin + SIZEMIP*xinc, Ymin+j*yinc, &temp, rel);
                diffVect(&temp.v,VertexModel(i,j),&est);
            }

            if(j>0){
                diffVect(VertexModel(i,j-1),VertexModel(i,j),&sud);
            }
            else{
                VertexData temp;
                GetPosNormalCol(Bump, aSel, Xmin + i*xinc, Ymin-yinc, &temp, rel);
                diffVect(&temp.v,VertexModel(i,j),&sud);
            }
            
            if(j < SIZEMIP-1){
                diffVect(VertexModel(i,j+1),VertexModel(i,j),&nord);
            }
            else{
                VertexData temp;
                GetPosNormalCol(Bump, aSel, Xmin + i*xinc, Ymin + SIZEMIP*yinc, &temp, rel);
                diffVect(&temp.v,VertexModel(i,j),&nord);
            }
            
            Vect3D tmp;
            Vect3D* norm = &Data[i][j].norm;
            prodVect(&est,&nord,norm);

            prodVect(&nord,&ouest,&tmp);
            addVect(norm,&tmp,norm);

            prodVect(&ouest,&sud,&tmp);
            addVect(norm,&tmp,norm);

            prodVect(&sud,&est,&tmp);
            addVect(norm,&tmp,norm);

            normaliseVect(norm);
        }
    }
#endif
    calcBox(block);
    calcRoughness(block);
    
    Epsilon[0] = maxRoughSon;
    
    for( i = 1; i < SIZEEPS && Epsilon[0] > Epsilon[i]; ++i)
        Epsilon[i] = Epsilon[0];
    
}

- (void) _commonInit
{
    int i, j;
    for( i = 0; i < BRANCHEMENT; ++i)
        for(j = 0; j < BRANCHEMENT; ++j)
            fils[i][j].block = nil;
    droite = nil;
    gauche = nil;
    haut = nil;
    bas = nil;
    
    nhaut = 0;
    nbas = 0;
    ndroite = 0;
    ngauche = 0;
    nSonSplit = 0;
    
    lod = 0;
    nFrame = 0;
    
    //  SetZeroVect(pos);
    
    addToCheck(self);
}

- initTopWithManager: (GGBlockContext *)man
                bump: (NSObject<GGBump> *)aBump
                  at: (double) _xmin
                 and: (double) _ymin
               width: (double) _width
              height: (double) _height
{
    
    [super init];
    [self _commonInit];
    
    manager = man;
    father = nil;
    depth = 0;
    xmin = _xmin;
    ymin = _ymin;
    width = _width;
    height = _height;
    frameCreation = man->nFrame;
    ASSIGN(bump, aBump);
    
    /* This should compute all the data for the block,  the epsilon[i] for every
        lod*/
    initBlock(self);
    return self;
}

- (id) retain
{
    NSAssert(!_invalidated, @"retain while invalidated");
    return [super retain];
}

+ topBlockWithManager:(GGBlockContext *)man
                 bump: (NSObject<GGBump> *)aBump
                   at: (double) _xmin
                  and: (double) _ymin
                width: (double) _width
               height: (double) _height
{
    GGBlock *block;
    block = [[self alloc]  initTopWithManager: man
                                         bump: aBump
                                           at: _xmin
                                          and: _ymin
                                        width: _width
                                       height: _height];
    
    return AUTORELEASE(block);
}

- initWithFather: (GGBlock *) theFather
              at: (int) i0
             and: (int) j0
{
    double xminf, widthf, yminf, heightf;
    
    [super init];
    [self _commonInit];
    
    manager = theFather->manager;
    frameCreation = manager->nFrame;
    xminf = theFather->xmin;
    widthf = theFather->width;
    
    yminf = theFather->ymin;
    heightf = theFather->height;
    
    xmin = xminf+((double)i0)/(BRANCHEMENT)*widthf;
    ymin = yminf+((double)j0)/(BRANCHEMENT)*heightf;
    width = widthf/BRANCHEMENT;
    height = heightf/BRANCHEMENT;
    
    frame.i = i0;
    frame.j = j0;
    
    
    /* we connect the newly created node to it's father */
    father = theFather;
    depth = theFather->depth+1;
    local = theFather->local;
    
    ASSIGN(bump, theFather->bump);
    
    
    /* This should compute all the data for the block,  the epsilon[i] for every
        lod*/
    initBlock(self);
    
    return self;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"Block([%3g, %3g, %3g, %3g], %d)", xmin, xmin+width, ymin, ymin+height, depth];
}

- blockSonAt: (int) i0
         and: (int) j0
{
    GGBlock *block;
    
    block = [GGBlock allocWithZone: [self zone]];
    return AUTORELEASE([block initWithFather: self
                                          at: i0
                                         and: j0]);
}

GGBlock* GGBlockGetSubBlock(GGBlock *block, unsigned i, unsigned j)
{
    NSCAssert1(i < BRANCHEMENT, @"out of bound index %d", i);
    NSCAssert1(j < BRANCHEMENT, @"out of bound index %d", j);
    return Fils[i][j].block;
}

- (void) invalidateBlock
{
    /*déstruction sauvage récursive*/
    int i, j;
    for(i = 0; i < BRANCHEMENT; ++i)
        for(j = 0; j < BRANCHEMENT; ++j)
            if (fils[i][j].block)
                DESTROY(fils[i][j].block);
    DESTROY(bump);
    [_blockCollision unsetup];
    DESTROY(_blockCollision);
    _invalidated = YES;
}

- (void) dealloc
{
    [self invalidateBlock];
    
    [super dealloc];
}

- (id) bump
{
    return bump;
}

- (BOOL) isLocal
{
    return local;
}

- (void) setIsLocal:(BOOL)val
{
    local = val;
}

- (GGBox) boudingBox
{
    Boxf box = boundingBox[SIZEBOX/2][SIZEBOX/2].theBox;
    return boxForBoxf(&frame, &box);
}

#pragma mark -
static int canSplit(GGBlock *block, int i, int j)
{
    CheckIndex(i, j);
    NSCParameterAssert(Lod < -1);
    
    if(!block->_splitable){
        return 0;
    }
    //   if( Depth == 0 )
    //     return 1;
    
    if( i == 0 )
    {
        if( Gauche == NULL ) // || Gauche->lod > 0)
            return 0;
    }
    if( i == BRANCHEMENT-1)
    {
        if( Droite == NULL ) //|| Droite->lod > 0 )
            return 0;
    }
    if( j == 0 )
    {
        if( Bas == NULL ) // || Bas->lod > 0 )
            return 0;
    }
    if (j == BRANCHEMENT-1)
    {
        if( Haut == NULL ) // || Haut->lod > 0 )
            return 0;
    }
    return 1;
}


//static int nbrOfNeighboorSons(GGBlock *block, GGBlock *neighboor)
//{
//    int ret = 0;
//    if( neighboor == Haut )
//    {
//        ret = Nhaut;
//    }
//    else if( neighboor == Droite )
//    {
//        ret = Ndroite;
//    }
//    else if (neighboor == Gauche )
//    {
//        ret = Ngauche;
//    }
//    else if (neighboor == Bas )
//    {
//        ret = Nbas;
//    }
//    else
//    {
//        [NSException raise: NSInternalInconsistencyException
//                    format: @"not in neighboorhood"];
//    }
//    
//    return ret;
//}

/*split, for a block that's not a top node */

static GGBlock *getFilsFromNeighBoor(GGBlock *block, GGBlock *neighboor, 
                                     int i, BOOL cw, GGBlock ***loopback)
{
    GGBlock *ret;
    if( neighboor == Haut )
    {
        ret = Fils[cw ? i : BRANCHEMENT-1-i][BRANCHEMENT-1].block;
        if(ret)
            *loopback = &ret->haut;
    }
    else if( neighboor == Droite )
    {
        ret = Fils[BRANCHEMENT-1][cw ? BRANCHEMENT-1-i : i].block;
        if(ret)
            *loopback = &ret->droite;
    }
    else if (neighboor == Gauche )
    {
        ret =  Fils[0][cw ? i : BRANCHEMENT-1-i].block;
        if(ret)
            *loopback = &ret->gauche;
    }
    else if (neighboor == Bas )
    {
        ret =  Fils[cw ? BRANCHEMENT-1-i : i][0].block;
        if(ret)
            *loopback = &ret->bas;
    }
    else
    {
        [NSException raise: NSInternalInconsistencyException
                    format: @"not in neighboorhood"];
        ret = nil;
    }
    return ret;
}

typedef enum __neighBoor
{
    NgbGauche,
    NgbDroite,
    NgbHaut,
    NgbBas
}NeighBoorLoc;

static inline GGBlock *getNeighboor(GGBlock *block, int i, int j, 
                                    NeighBoorLoc loc, GGBlock ***loopBack)
{
    GGBlock *pneigh = nil;
    switch(loc)
    {
        case NgbGauche:
            if( i > 0 )
            {
                pneigh = Fils[i-1][j].block;
                if( pneigh )
                    *loopBack = &pneigh->droite;
            }
            else 
            {
                if (Gauche != NULL )
                {
                    //	  pneigh = Gauche->fils[BRANCHEMENT-1][j].block;
                    pneigh = getFilsFromNeighBoor(Gauche, block, j, NO, loopBack);
                    //	      NSCParameterAssert( pneigh == Gauche->fils[BRANCHEMENT-1][j].block);
                }
                else
                    pneigh = NULL;
            }
            break;
            
        case NgbDroite:
            if( i < BRANCHEMENT-1 )
            {
                pneigh = Fils[i+1][j].block;
                if( pneigh )
                    *loopBack = &pneigh->gauche;
            }
            else
            { 
                if (Droite != NULL )
                {
                    pneigh = getFilsFromNeighBoor(Droite, block, j, YES, loopBack);
                    //	      NSCParameterAssert(pneigh == Droite->fils[0][j].block);
                }
                else
                    pneigh = NULL;
            }
            break;
            
        case NgbBas:
            if( j > 0 )
            {
                pneigh = Fils[i][j-1].block;
                if( pneigh )
                    *loopBack = &pneigh->haut;
            }
            else
            {
                if (Bas != NULL )
                {
                    pneigh = getFilsFromNeighBoor(Bas, block, i, YES, loopBack);
                    //	      NSCParameterAssert(pneigh == Bas->fils[i][BRANCHEMENT-1].block);
                }
                else
                    pneigh = NULL;
            }
            break;
            
        case NgbHaut:
            if( j < BRANCHEMENT-1 )
            {
                pneigh = Fils[i][j+1].block;
                if( pneigh )
                    *loopBack = &pneigh->bas;
            }
            else
            {
                if (Haut != NULL )
                {
                    pneigh = getFilsFromNeighBoor(Haut, block, i, NO, loopBack);
                    //	      NSCParameterAssert(pneigh == Haut->fils[i][0].block);
                }
                else
                    pneigh = NULL;
            }
            break;
        default:
            NSCParameterAssert(0);
    }
    return pneigh;
}


static void splitCase(GGBlock *block, int i, int j)
{
    GGBlock *son;
    GGBlock *pneigh;
    GGBlock **loopBack;
    CheckIndex(i, j);
    NSCParameterAssert(Fils[i][j].block == NULL);
    
#ifdef DEBUG
    [block willChangeValueForKey:@"subNodes"];
#endif
    son = [block blockSonAt: i and: j];
    
    
    ASSIGN(Fils[i][j].block, son);
    NSonSplit++;
    
    /* connect to the left neighboor */
    pneigh = getNeighboor(block, i, j, NgbGauche, &loopBack);
    if( i == 0 )
    {
        Ngauche++;
    }
    son->gauche = pneigh;
    if( pneigh != NULL )
    {
        if( pneigh->lod <= -2 )
            son->lod = -1;
        *loopBack = son;
        //      pneigh->droite = son;
    }
    
    /* connect to the right neighboor */
    pneigh = getNeighboor(block, i, j, NgbDroite, &loopBack);
    if( i >= BRANCHEMENT-1 )
    {
        Ndroite++;
    }
    son->droite = pneigh;
    if( pneigh != NULL )
    {
        if( pneigh->lod <= -2 )
            son->lod = -1;
        
        *loopBack = son;
        //      pneigh->gauche = son;
    }
    
    
    /* connect to the bottom neighboor */
    pneigh = getNeighboor(block, i, j, NgbBas, &loopBack);
    if( j == 0 )
    {
        Nbas++;
    }
    son->bas = pneigh;
    if( pneigh != NULL )
    {
        if( pneigh->lod <= -2 )
            son->lod = -1;
        
        *loopBack = son;
        //      pneigh->haut = son;
    }
    
    /* connect to the top neighboor */
    pneigh = getNeighboor(block, i, j, NgbHaut, &loopBack);
    if( j >= BRANCHEMENT-1 )
    {
        Nhaut++;
    }
    son->haut = pneigh;
    if( pneigh != NULL )
    {
        if( pneigh->lod <= -2 )
            son->lod = -1;
        
        *loopBack = son;
        //      pneigh->bas = son;
    }
    
    //finish initialisation
    [block propagateBoxFrom: i
                        and: j];
#ifdef DEBUG
    [block didChangeValueForKey:@"subNodes"];
#endif
    
}

/*this function computes the lod of a block.  It may decide to destroy a 
given subblock for any reason or to split the block (in this case, the 
                                                     return value is -1).

return value :
0..MaxLod :  the level of detail if unsplit
-1 : if split.
    */


static BOOL splitable = YES;

static BOOL onePerFrame = YES;

#ifdef DEBUG

static int GG_TRIANGLES=GL_TRIANGLES;
static int GG_TRIANGLE_STRIP= GL_TRIANGLE_STRIP;
static int GG_TRIANGLE_FAN= GL_TRIANGLE_FAN;

static BOOL frozen = NO;
static BOOL border = NO;
+ (void) freezeBlocks
{
    frozen = !frozen;
    NSLog(@"freezeBlocks:  %d", frozen);
}
#else
#define  GG_TRIANGLES 		GL_TRIANGLES
#define  GG_TRIANGLE_STRIP 	GL_TRIANGLE_STRIP
#define  GG_TRIANGLE_FAN	GL_TRIANGLE_FAN
#endif

static BOOL trySplit(GGBlock *block, int i, int j)
{
    NSCParameterAssert(Fils[i][j].block == nil);
#ifdef DEBUG
    if( frozen )
        return NO;
#endif
    if( splitable && canSplit(block, i, j) )
    {
        Vect3D temp;
        float d, err;
        addVect(&Fils[i][j].pos, &Decal, &temp);
        d = distanceBlock(&temp, &block->manager->posObs);
        err = (d/(lodConst*block->frame.size));
        //        if( i <= 2 && j <= 2)
        //  	NSDebugLLogWindow(@"GGBlock", @"(%d,%d) : %g", i, j, err);
        
#ifdef DEBUG
        if( block == fatherCurrent && i == icur && j == jcur )
            NSDebugLLogWindow(@"Split", @"manhattan = %g\nerr = %g, f.err = %i\nframe.size = %g", 
                              (double)d, (double)err, 
                              (int)(BoundingBoxExt(2*i+1,2*j+1).roughness),
                              (double)block->frame.size);
#endif
        
        if( err < BoundingBoxExt(2*i+1,2*j+1).roughness )
        {
            if( onePerFrame )
                splitable = NO;//can split only once per frame
            NSDebugLLog(@"GGBlock", @"split %d,%d %g", i, j, err);
            splitCase(block, i, j);
            NSCParameterAssert(Fils[i][j].block != nil);
            return YES;
        }
    }
    return NO;
}

static BOOL tryUnsplit(GGBlock *block, int i, int j)
{
    GGBlock *pneighg = nil, *pneighd = nil;
    GGBlock *pneighh = nil, *pneighb = nil;
    GGBlock **loopBackg;
    GGBlock **loopBackh;
    GGBlock **loopBackd;
    GGBlock **loopBackb;
    unsigned short int *dec1 = NULL, *dec2=NULL;
    CheckIndex(i, j);
    NSCParameterAssert(Fils[i][j].block != nil);
    
    
    pneighg = getNeighboor(block, i, j, NgbGauche, &loopBackg);
    if( i == 0 )
    {
        dec1 = &Ngauche;
    }
    if( pneighg != nil && pneighg->lod < -1 )
        return NO;
    
    pneighd = getNeighboor(block, i, j, NgbDroite, &loopBackd);
    if( i >= BRANCHEMENT-1 )
    {
        dec1 = &Ndroite;
    }
    if( pneighd != nil && pneighd->lod < -1 )
        return NO;
    
    pneighb = getNeighboor(block, i, j, NgbBas, &loopBackb);
    if( j == 0 )
    {
        dec2 = &Nbas;
    }
    if( pneighb != nil && pneighb->lod < -1 )
        return NO;
    
    
    pneighh = getNeighboor(block, i, j, NgbHaut, &loopBackh);
    if( j >= BRANCHEMENT-1 )
    {
        dec2 = &Nhaut;
    }
    if( pneighh != nil && pneighh->lod < -1 )
        return NO;
    
#ifdef DEBUG
    [block willChangeValueForKey:@"subNodes"];
#endif
    /* Now we know we can remove this subblock */
    
    if( dec1 )
        (*dec1)--;
    if( dec2 )
        (*dec2)--;
    
    NSonSplit--;
    
    /* we erase the crossed link */
    if( pneighg )
    {
        //      NSCParameterAssert(loopBackg == &pneighg->droite);
        *loopBackg = nil;
        //    pneighg->droite = nil;
    }
    if( pneighd )
    {
        //      NSCParameterAssert(loopBackd == &pneighd->gauche);
        *loopBackd = nil;
        //pneighd->gauche = nil;
    }
    if( pneighb )
    {
        //      NSCParameterAssert(loopBackb == &pneighb->haut);
        *loopBackb = nil;
        //pneighb->haut = nil;
    }
    if( pneighh )
    {
        //      NSCParameterAssert(loopBackh == &pneighh->bas);
        *loopBackh = nil;
        //pneighh->bas = nil;
    }
    
    [Fils[i][j].block invalidateBlock];
    DESTROY(Fils[i][j].block);
#ifdef DEBUG
    [block didChangeValueForKey:@"subNodes"];
#endif
    return YES;
}

/* try to merge the block and return YES if successful.
*/
static BOOL tryMerge(GGBlock *block)  
{
    int i, j;
    BOOL res = YES;
    
    
    if( Lod >= 0 ) // no sons should exist
    {
        NSCParameterAssert(NSonSplit == 0);
        return YES;
    }
#ifdef DEBUG
    if( frozen )
        return NO;
#endif
    
    // FIXME : do a recursive descent.  More efficient in case there're few
    // sons split
    for( i = 0; i < BRANCHEMENT && NSonSplit > 0; ++i)
        for( j = 0; j < BRANCHEMENT && NSonSplit > 0; ++j)
            if( Fils[i][j].block != nil && !tryUnsplit(block, i, j) )
                res = NO;
    
    if( res )  //we don't have any sons now.  
    {
        Lod = -1;
        if( Gauche != nil && Gauche->lod < -1 )
            return NO;
        if( Droite != nil && Droite->lod < -1 )
            return NO;
        if( Haut != nil && Haut->lod < -1 )
            return NO;
        if( Bas != nil && Bas->lod < -1 )
            return NO;
        return YES;
    }
    else
        return NO;
}

/* to know wether a case is splittable or not
force the log of the neighboor to -1 because we are split.
*/

static void updateNeighboor(GGBlock *block)
{
    //      if( ! (Droite && Gauche && Bas && Haut ) )
    //	return;
    
    if (Droite && Droite->lod > -1 )
        Droite->lod = -1;
    if( Gauche && Gauche->lod > -1 )
        Gauche->lod = -1;
    if( Haut && Haut->lod > -1 )
        Haut->lod = -1;
    if( Bas && Bas->lod > -1 )
        Bas->lod = -1;
    Lod = -2;
}


double blockLod(GGBlock *block)
{
    BOOL canMerge = YES;
    int newLod;
    double tol;
    float d2;
    if( block == nil )
        return LNBRANCHEMENT;
#ifdef DEBUG
    if( frozen )
        return block->floatMipLevel;
#endif
    
    //   if( block->frameCreation == block->manager->nFrame )
    //     return LNSIZEMIP;
    
    if( block->nFrame < block->manager->nFrame )
    {
        
        //some init that should be done once per frame.
        // we are sure this will be the first method of the object to be called
        //so we put those initialization here.
        if( Local )
        {
            diffVect(&Origin, &Manager->posLocalFrame, &Decal);
        }
        newLod = Lod;
        //compute coordinates in the same frame as the camera :
        d2 = (distanceBlock(&block->manager->posObs, &Decal));
#ifdef DEBUG
        if( lookForClosest && d2 < distClosest )
        {
            closest = block;
            distClosest = d2;
        }
#endif
        
        tol = d2 / (lodConst*block->frame.size);
        while(newLod >= 0 && tol < Epsilon[newLod] )
        {
            /*  	  if( t1 ) */
            /*  	    fprintf(stderr, "bijump %d\n", t1); */
            /*  	  t1++; */
            newLod--;
        }
        
        while( newLod+1 < SIZEEPS && tol > Epsilon[newLod+1] )
        {
            /*  	  if( t1 ) */
            /*  	    fprintf(stderr, "bijump %d\n", t1); */
            /*  	  t1++; */
            newLod++;
        }
        
        
        if( newLod < 0 )
        {
            block->floatMipLevel = 0.0;
            if( Lod >= -1 ) //notify neighboor...
                updateNeighboor(block);
            //	  Lod = -2;
        }
        else 
        {
            if( !tryMerge(block) )  // we couldn't merge...
            {
                block->floatMipLevel = 0.0;
                canMerge = NO;
            }
            else if( newLod < 1 )  //newLod == 0
            {
                block->floatMipLevel = 0.0;
                Lod = newLod;
            }
            else if( newLod >= LNSIZEMIP+1 )
            {
                block->floatMipLevel = LNSIZEMIP;
                Lod = LNSIZEMIP;
            }
            else
            {
                double alpha;
                
                alpha = (tol - (double)Epsilon[newLod])/(Epsilon[newLod+1]-Epsilon[newLod]);
                block->floatMipLevel = newLod-1+alpha;
                Lod = newLod-1;
            }
        }
        block->nFrame = block->manager->nFrame;
#ifdef DEBUG
        if( block == currentBlock )
        {
            NSDebugLLogWindow(@"Lod", @"posLocalFrame : %@ Decal %@\norigin: %@",
                              stringOfVect(&Manager->posLocalFrame),
                              stringOfVect(&Decal),
                              stringOfVect(&Origin));
            NSDebugLLogWindow(@"Lod", @"canMerge = %d", canMerge);
            NSDebugLLogWindow(@"Lod", @"d2 = %g", d2);
            NSDebugLLogWindow(@"Lod", @"tol = %g", tol);
            NSDebugLLogWindow(@"Lod", @"Lod = %d newLod = %d", Lod, newLod);
            NSDebugLLogWindow(@"Lod", @"floatMipLevel = %g", (double)block->floatMipLevel);
            NSDebugLLogWindow(@"Lod", @"eps : %d, %d, %d, %d, %d %d",
                              Epsilon[0],Epsilon[1],Epsilon[2],Epsilon[3],
                              Epsilon[4],Epsilon[5]);
            
        }
#endif
    }
    
    return (Lod < 0 ? 0.0 : block->floatMipLevel);
}

static void ggInterleavedArray(VertexData *data)
{
    //  glTexCoordPointer(3, GL_FLOAT, sizeof(VertexData), &data->texCoord);
    glNormalPointer(GL_FLOAT, sizeof(VertexData), &data->norm);
    glEnableClientState(GL_NORMAL_ARRAY);

    glVertexPointer(3, GL_FLOAT, sizeof(VertexData), &data->v);
    glEnableClientState(GL_VERTEX_ARRAY);

#ifdef USE_COL_IN_DATA
    glColorPointer(3,GL_FLOAT,sizeof(VertexData),&data->col);
    glEnableClientState(GL_COLOR_ARRAY);
#else
    glDisableClientState(GL_COLOR_ARRAY);
#endif
    
    glDisableClientState(GL_EDGE_FLAG_ARRAY);
    glDisableClientState(GL_INDEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/* this is one of the most horrible procedure I've ever written
It draws the geodata and preforms geomorphing.  There are some 
macros (but subprocedure would've been more convenient but it's not
        ANSI).  I use the name "alpha" for the blending factor.  I trick with 
the scoping of name so that alpha is not always the same variable.  
So the code looks cleaner
*/

#if 0
//old macro useless now
#define Vertex(i,j) do{\
    ggcheck(i,j);\
        glColor4v(&Data[i][j].col);\
            glNormal3v(&Data[i][j].norm);\
                glVertex3v(&Data[i][j].v);\
}while(0)
#endif

#define Vertex(i,j) do{\
    ggcheck(i,j);\
        glArrayElement((i)*SIZEMIP+(j));\
}while(0)

#define VertexAux(i) do{\
    ggcheck(i,0);\
        glNormal3v(&dataAux[i].norm);\
            glVertex3v(&dataAux[i].v);}while(0)


#ifdef HardDebug
#define GGVertex(i,j) do{\
                ggcheck(i,j);\
                    glColor3f(1,0,0);\
                        glNormal3f(0, 0, 1);\
                            glVertex3v(&Data[i][j].v);\
            }while(0)

#define Vertex2(i,j) do{\
                ggcheck(i,j);\
                    glColor3f(1, 0, 0);\
                        glNormal3v((GGReal *)&dataVert[i][j].norm);\
                            glVertex3fv((GLfloat *)&dataVert[i][j].v);\
            }while(0)

#define Vertex3(i,j) do{\
                ggcheck(i,j);\
                    glColor3f(1, 0, 1);\
                        glNormal3v((GGReal *)&dataVert[i][j].norm);\
                            glVertex3fv((GLfloat *)&dataVert[i][j].v);\
            }while(0)

#define Vertex4(i,j) do{\
                ggcheck(i,j);\
                    glColor3f(1, 1, 0);\
                        glNormal3v((GGReal *)&dataVert[i][j].norm);\
                            glVertex3fv((GLfloat *)&dataVert[i][j].v);\
            }while(0)

#define Vertex5(i,j) do{\
                ggcheck(i,j);\
                    glColor3f(0, 1, 1);\
                        glNormal3v((GGReal *)&dataVert[i][j].norm);\
                            glVertex3fv((GLfloat *)&dataVert[i][j].v);\
            }while(0)



#else

#define Vertex2 Vertex
#define Vertex3 Vertex
#define Vertex4 Vertex
#define Vertex5 Vertex
#endif

#define SetVert(a, b) do{\
    ggcheck(a,b);\
    dataVert[a][b] = block->data[a][b];}\
    while(0)

#define MoyenneReel(_res, _a, _b, _c) do{\
    _res = alpha*((_a)+(_b))/2 + (1-alpha)*(_c);}while(0)

#define MoyenneCouleur(res, u, v, w) do{\
    MoyenneReel((res).r, (u).r, (v).r, (w).r);\
    MoyenneReel((res).g, (u).g, (v).g, (w).g);\
    MoyenneReel((res).b, (u).b, (v).b, (w).b);}while(0)


#if 0
    #define MoyenneTexture(res, u, v, w) do{\
    MoyenneReel((res).s, (u).s, (v).s, (w).s);\
    MoyenneReel((res).t, (u).t, (v).t, (w).t);}while(0)
#endif

#define MoyenneTexture(res, u, v, w) do{\
    res.texCoord = w.texCoord;}while(0)




#define MoyenneVect(res, u, v, w) do{\
    Vect3D tmp;\
    addVect(&u, &v, &tmp);\
    mulScalVect(&tmp, (alpha)/2, &tmp);\
    addLambdaVect(&tmp, 1-alpha, &w, &res);}while(0)


#ifdef USE_COL_IN_DATA
#define Moyenne(i, j, k, l, m, n) do{\
    ggcheck(i,j);\
    ggcheck(k,l);\
    ggcheck(m,n);\
    MoyenneVect(dataVert[i][j].col, dataVert[k][l].col,\
    dataVert[m][n].col, block->data[i][j].col);\
    MoyenneVect(dataVert[i][j].norm, dataVert[k][l].norm,\
    dataVert[m][n].norm, block->data[i][j].norm);\
    MoyenneVect(dataVert[i][j].v, dataVert[k][l].v,\
    dataVert[m][n].v, block->data[i][j].v);\
    }while(0)
#else
#define Moyenne(i, j, k, l, m, n) do{\
    ggcheck(i,j);\
    ggcheck(k,l);\
    ggcheck(m,n);\
    MoyenneVect(dataVert[i][j].norm, dataVert[k][l].norm,\
    dataVert[m][n].norm, block->data[i][j].norm);\
    MoyenneVect(dataVert[i][j].v, dataVert[k][l].v,\
    dataVert[m][n].v, block->data[i][j].v);\
    }while(0)
#endif

#define MoyenneAux(i, j, k, l, m, n, o) do{\
    ggcheck(o,0);\
    ggcheck(i,j);\
    ggcheck(k,l);\
    ggcheck(m,n);\
    MoyenneVect(dataAux[o].norm, dataVert[k][l].norm,\
    dataVert[m][n].norm, block->data[i][j].norm);\
    MoyenneVect(dataAux[o].v, dataVert[k][l].v,\
    dataVert[m][n].v, block->data[i][j].v);\
    }while(0)

static void rasteriseBlock(GGBlock *block)
{	
    int i, j;
    int inc;
    float floatMip, floatMipVoisin;
    float alpha;
    float theAlpha;
    int _miplod, _miplodVoisin;
    int	incvoisin;
    //  VectCol col;
    GGBlock 	*voisin;
    VertexData	_dataVert[SIZEMIP][SIZEMIP];
    VertexData	(*dataVert)[SIZEMIP];
    VertexData	dataAux[SIZEMIP];
    
    
    floatMip = blockLod(block);
    _miplod = (int) floatMip;
    theAlpha = alpha = floatMip - _miplod;
    
    inc = 1<<_miplod;
    
#ifdef DEBUG
    if(block == block->manager->_currentBlock){
        NSDebugMLLogWindow(@"Block", @"rasterizing the current block");
    }
#endif
    
    if( alpha > 0 )
    {
        dataVert = &(_dataVert[0]);
        SetVert(0, 0);
        for( j = 2*inc; j < SIZEMIP; j+= 2*inc )
        {
            SetVert(0, j);
            Moyenne(0, j-inc, 0, j-(2*inc), 0, j);
        }
        for( i = 2*inc; i < SIZEMIP; i += 2*inc )
        {
            SetVert(i, 0);
            Moyenne(i-inc, 0, i-(2*inc), 0, i, 0);
            for( j = 2*inc; j < SIZEMIP; j += 2*inc)
            {
                SetVert(i, j);
                Moyenne(i, j-inc, i, j-(2*inc), i, j);
                Moyenne(i-inc, j, i-(2*inc), j, i, j);
                Moyenne(i-inc, j-inc, i-(2*inc), j, i, j-(2*inc));
                /*    	      Moyenne(i-inc, j-inc, i-(2*inc), j-(2*inc), i, j); */
            }
        }
    }
    else
    {
        dataVert = &Data[0];
    }
    
    
    //  glInterleavedArrays(GL_T2F_N3F_V3F, 0, dataVert);
    ggInterleavedArray(&dataVert[0][0]);
    // glLockArraysExt(0, SIZEMIP*SIZEMIP);
    
    
#ifdef DEBUG
    nTriangle += SQ(BRANCHEMENT/inc);
#endif
    
    if( _miplod == LNSIZEMIP ) //lod maximal
    {
        NSDebugLLogWindow(@"GGBlock", @"level %d", _miplod);
        glBegin(GG_TRIANGLE_STRIP);{
            Vertex(0,0);
            Vertex(SIZEMIP-1,0);
            Vertex(0,SIZEMIP-1);
            Vertex(SIZEMIP-1,SIZEMIP-1);
        }glEnd();
        // glUnlockArraysEXT();
        return;
    }
    
    voisin = Gauche;
    if( (floatMipVoisin = blockLod(voisin)) <= floatMip )
    {
        glBegin(GG_TRIANGLES);
        Vertex2(0, 0);
        Vertex2(inc, inc);
        Vertex2(0, inc);
        glEnd();
        
        glBegin(GG_TRIANGLE_STRIP);
        for( j = inc; j < SIZEMIP-inc; j += inc )
        {
            Vertex2(0,j);
            Vertex2(inc,j);
        }
        Vertex2(0, j);
        glEnd();
        
    }
    else
    {
        _miplodVoisin = (int)floatMipVoisin;
        
        if( _miplodVoisin == LNSIZEMIP )
        {
            glBegin(GG_TRIANGLE_FAN);{
                int i;
                Vertex2(0, 0);
                for( i = inc; i <= (SIZEMIP>>1); i += inc )
                    Vertex2(inc, i);
            }glEnd();
            
            glBegin(GG_TRIANGLE_FAN);{
                int i;
                Vertex2(0, SIZEMIP-1);
                for( i = SIZEMIP>>1; i < SIZEMIP-inc; i += inc )
                    Vertex2(inc, i);
            }glEnd();
            
            glBegin(GG_TRIANGLE_FAN);{
                Vertex2(0, 0);
                Vertex2(inc, SIZEMIP>>1);
                Vertex2(0, SIZEMIP-1);
            }glEnd();
        }
        else
        {
            alpha = floatMipVoisin - _miplodVoisin;
            incvoisin = 1<<_miplodVoisin;
            
            /*      we just recompute de midpoint with the new blending factor
             it has the same name */
            dataAux[0] = dataVert[0][0];
            for( i = 2*incvoisin; i < SIZEMIP;  i += 2*incvoisin )
            {
                dataAux[i] = dataVert[0][i];
                MoyenneAux(0, i-incvoisin, 0, i-(2*incvoisin), 0, i, i-incvoisin);
            }
            
            for(i = incvoisin; i < SIZEMIP-2*incvoisin; i+=incvoisin )
            {
                glBegin(GG_TRIANGLE_FAN);
                //	      Vertex2(0, i);
                VertexAux(i);
                for(j = 0; j < incvoisin; j+= inc)
                    Vertex2(inc, i+j);
                Vertex2(inc, i+j);
                VertexAux(i+j);
                //	      Vertex2(0, i+j);
                glEnd();
            }
            
            glBegin(GG_TRIANGLE_FAN);
            Vertex2(0,0);
            for(j = inc; j < incvoisin; j+=inc)
                Vertex2(inc, j);
            Vertex2(inc, j);
            //	  Vertex2(0, j);
            VertexAux(j);
            glEnd();
            
            i = SIZEMIP-1-incvoisin;
            glBegin(GG_TRIANGLE_FAN);
            //	  Vertex2(0,i);
            VertexAux(i);
            for(j = 0; j < incvoisin; j+=inc)
                Vertex2(inc, i+j);
            VertexAux(i+j);
            // 	  Vertex2(0, i+j);
            glEnd();
            
        }
    }
    
    voisin = Droite;
    if(  (floatMipVoisin = blockLod(voisin)) <= floatMip )
    {
        glBegin(GG_TRIANGLES);
        Vertex3(SIZEMIP-1-inc, SIZEMIP-1-inc);
        Vertex3(SIZEMIP-1, SIZEMIP-1-inc);
        Vertex3(SIZEMIP-1, SIZEMIP-1);
        glEnd();
        
        glBegin(GG_TRIANGLE_STRIP);
        Vertex3(SIZEMIP-1, 0);
        for( j = inc; j < SIZEMIP-inc; j += inc )
        {
            Vertex3(SIZEMIP-1,j);
            Vertex3(SIZEMIP-1-inc,j);
        }
        glEnd();
    }
    else
    {
        _miplodVoisin = (int)floatMipVoisin;
        if( _miplodVoisin == LNSIZEMIP )
        {
            glBegin(GG_TRIANGLE_FAN);{
                int i;
                Vertex3(SIZEMIP-1, 0);
                for( i = SIZEMIP>>1; i >= inc; i -= inc )
                    Vertex3(SIZEMIP-1-inc, i);
            }glEnd();
            
            glBegin(GG_TRIANGLE_FAN);{
                int i;
                Vertex3(SIZEMIP-1, SIZEMIP-1);
                for( i = SIZEMIP-1-inc; i >= SIZEMIP>>1; i -= inc )
                    Vertex3(SIZEMIP-1-inc, i);
            }glEnd();
            
            glBegin(GG_TRIANGLE_FAN);{
                Vertex3(SIZEMIP-1, 0);
                Vertex3(SIZEMIP-1, SIZEMIP-1);
                Vertex3(SIZEMIP-1-inc, SIZEMIP>>1);
            }glEnd();
        }
        else
        {
            alpha = floatMipVoisin - _miplodVoisin;
            
            incvoisin = 1<<_miplodVoisin;
            
            /*      we just recompute de midpoint with the new blending factor
             it has the same name */
            dataAux[0] = dataVert[SIZEMIP-1][0];
            for( i = 2*incvoisin; i < SIZEMIP; i += 2*incvoisin)
            {
                dataAux[i] = dataVert[SIZEMIP-1][i];
                MoyenneAux(SIZEMIP-1, i-incvoisin, SIZEMIP-1, 
                i-(2*incvoisin), SIZEMIP-1, i, i-incvoisin);
            }
            
            for(i = incvoisin; i < SIZEMIP-2*incvoisin; i+=incvoisin )
            {
                glBegin(GG_TRIANGLE_FAN);
                VertexAux(i);
                VertexAux(i+incvoisin);
                for(j = incvoisin; j >= 0; j-= inc)
                    Vertex3(SIZEMIP-1-inc, i+j);
                glEnd();
            }
            
            glBegin(GG_TRIANGLE_FAN);
            VertexAux(0);
            VertexAux(incvoisin);
            for(j = incvoisin; j >= inc; j-=inc)
                Vertex3(SIZEMIP-1-inc, j);
            glEnd();
            
            i = SIZEMIP-1-incvoisin;
            glBegin(GG_TRIANGLE_FAN);
            VertexAux(i);
            VertexAux(SIZEMIP-1);
            for(j = incvoisin-inc; j >= 0; j-=inc)
                Vertex3(SIZEMIP-1-inc, i+j);
            glEnd();
            
        }      /*draw left border */
    }
    
    voisin = Haut;
    if( (floatMipVoisin = blockLod(voisin)) <= floatMip )
    {
        glBegin(GG_TRIANGLES);
        Vertex4(0, SIZEMIP-1);
        Vertex4(inc, SIZEMIP-1-inc);
        Vertex4(inc, SIZEMIP-1);
        glEnd();
        
        glBegin(GG_TRIANGLE_STRIP);
        for( j = inc; j < SIZEMIP-inc; j += inc )
        {
            Vertex4(j, SIZEMIP-1);
            Vertex4(j, SIZEMIP-1-inc);
        }
        Vertex4(j, SIZEMIP-1);
        glEnd();
        
    }
    else
    {
        _miplodVoisin = (int)floatMipVoisin;
        if( _miplodVoisin == LNSIZEMIP )
        {
            glBegin(GG_TRIANGLE_FAN);{
                int i;
                Vertex4(0, SIZEMIP-1);
                for( i = inc; i <= (SIZEMIP>>1); i += inc )
                    Vertex4(i, SIZEMIP-1-inc);
            }glEnd();
            
            glBegin(GG_TRIANGLE_FAN);{
                int i;
                Vertex4(SIZEMIP-1, SIZEMIP-1);
                for( i = SIZEMIP>>1; i < SIZEMIP-inc; i += inc )
                    Vertex4(i, SIZEMIP-1-inc);
            }glEnd();
            
            glBegin(GG_TRIANGLE_FAN);{
                Vertex4(0, SIZEMIP-1);
                Vertex4(SIZEMIP>>1, SIZEMIP-1-inc);
                Vertex4(SIZEMIP-1, SIZEMIP-1);
            }glEnd();
        }
        else
        {
            alpha = floatMipVoisin - _miplodVoisin;
            
            
            incvoisin = 1<<_miplodVoisin;
            
            /*      we just recompute de midpoint with the new blending factor
             it has the same name */
            dataAux[0] = dataVert[0][SIZEMIP-1];
            for( i = 2*incvoisin; i < SIZEMIP;  i += 2*incvoisin)
            {
                dataAux[i] = dataVert[i][SIZEMIP-1];
                MoyenneAux(i-incvoisin, SIZEMIP-1, i-(2*incvoisin), 
                SIZEMIP-1, i, SIZEMIP-1, i-incvoisin);
            }
            
            
            for(i = incvoisin; i < SIZEMIP-2*incvoisin; i+=incvoisin )
            {
                glBegin(GG_TRIANGLE_FAN);
                VertexAux(i);
                for(j = 0; j < incvoisin; j+= inc)
                    Vertex4(i+j, SIZEMIP-1-inc);
                Vertex4(i+j, SIZEMIP-1-inc);
                VertexAux(i+j);
                glEnd();
            }
            
            glBegin(GG_TRIANGLE_FAN);
            VertexAux(0);
            for(j = inc; j < incvoisin; j+=inc)
                Vertex4(j, SIZEMIP-1-inc);
            Vertex4(j, SIZEMIP-1-inc);
            VertexAux(j);
            glEnd();
            
            i = SIZEMIP-1-incvoisin;
            glBegin(GG_TRIANGLE_FAN);
            VertexAux(i);
            for(j = 0; j < incvoisin; j+=inc)
                Vertex4(i+j, SIZEMIP-1-inc);
            VertexAux(i+j);
            glEnd();
            
        }
    }
    
    voisin = Bas;
    if(  (floatMipVoisin = blockLod(voisin)) <= floatMip )
    {
        glBegin(GG_TRIANGLES);
        Vertex5(SIZEMIP-1-inc, inc);
        Vertex5(SIZEMIP-1-inc, 0);
        Vertex5(SIZEMIP-1, 0);
        glEnd();
        
        glBegin(GG_TRIANGLE_STRIP);
        Vertex5(0, 0);
        for( j = inc; j < SIZEMIP-inc; j += inc )
        {
            Vertex5(j, 0);
            Vertex5(j, inc);
        }
        glEnd();
    }
    else
    {
        _miplodVoisin = (int)floatMipVoisin;
        if( _miplodVoisin == LNSIZEMIP )
        {
            glBegin(GG_TRIANGLE_FAN);{
                int i;
                Vertex5(0, 0);
                for( i = SIZEMIP>>1; i >= inc; i -= inc )
                    Vertex5(i, inc);
            }glEnd();
            
            glBegin(GG_TRIANGLE_FAN);{
                int i;
                Vertex5(SIZEMIP-1, 0);
                for( i = SIZEMIP-1-inc; i >= SIZEMIP>>1; i -= inc )
                    Vertex5(i, inc);
            }glEnd();
            
            glBegin(GG_TRIANGLE_FAN);{
                Vertex5(0, 0);
                Vertex5(SIZEMIP-1, 0);
                Vertex5(SIZEMIP>>1, inc);
            }glEnd();
        }
        else
        {      
            alpha = floatMipVoisin - _miplodVoisin;
            
            
            incvoisin = 1<<_miplodVoisin;
            
            /*      we just recompute de midpoint with the new blending factor
             it has the same name */
            dataAux[0] = dataVert[0][0];
            for( i = 2*incvoisin; i < SIZEMIP;  i += 2*incvoisin)
            {
                dataAux[i] = dataVert[i][0];
                MoyenneAux(i-incvoisin, 0, i-(2*incvoisin), 0, i, 0, i-incvoisin);
            }
            
            for(i = incvoisin; i < SIZEMIP-2*incvoisin; i+=incvoisin )
            {
                glBegin(GG_TRIANGLE_FAN);
                VertexAux(i);
                VertexAux(i+incvoisin);
                for(j = incvoisin; j >= 0; j-= inc)
                    Vertex5(i+j, inc);
                glEnd();
            }
            
            glBegin(GG_TRIANGLE_FAN);
            VertexAux(0);
            VertexAux(incvoisin);
            for(j = incvoisin; j >= inc; j-=inc)
                Vertex5(j, inc);
            glEnd();
            
            i = SIZEMIP-1-incvoisin;
            glBegin(GG_TRIANGLE_FAN);
            VertexAux(i);
            VertexAux(SIZEMIP-1);
            for(j = incvoisin-inc; j >= 0; j-=inc)
                Vertex5(i+j, inc);
            glEnd();
            
        }
    }
    
    for( i = inc; i < SIZEMIP-2*inc; i += inc )
    {
        //      glBegin(GL_LINE_STRIP);
        glBegin(GG_TRIANGLE_STRIP);
        for( j = inc; j < SIZEMIP-inc; j += inc )
        {
            Vertex(i,j);
            Vertex(i+inc,j);
        }
        glEnd();
    }
    // glUnlockArraysEXT();
    
#if defined(HardDebug)
    glBegin(GL_LINE_STRIP);
    for( i = 0; i < SIZEMIP; ++i )
        GGVertex(i, 0);
    for( i = 0; i < SIZEMIP; ++i )
        GGVertex(SIZEMIP-1, i);
    for( i = SIZEMIP-1; i >= 0; --i )
        GGVertex(i, SIZEMIP-1);
    for( i = SIZEMIP-1; i >= 0; --i )
        GGVertex(0, i);
    glEnd();
    glEnable(GL_DEPTH_TEST);
#endif
#undef Vertex
}

#pragma mark pass
/* clipStatus is the status of the father, so if not 0, it is inherited
by children
*/
static inline BOOL boxVisibilityStatus(GGBlock *block, unsigned short int i, 
                                       unsigned short int j, 
                                       signed char  clipStatus, BOOL visited)
{
    if( clipStatus == 1 )
    {
        BoundingBoxExt(i,j).cache = aDedans;
        return clipStatus;
    }
#ifdef DEBUG
    if( frozen )
        return boxInFrustumCache(BoundingBoxExt(i, j).cache);
#endif
    if( visited )
        return boxInFrustumCache(BoundingBoxExt(i, j).cache);
    else
        return boxInFrustumFrame(&block->manager->frustum, &block->frame,
                                 &BoundingBoxExt(i, j));
}

//static inline void subFirstPassNoClip;
static void firstPass(GGBlock *block, signed char  clipStatus);

static void subFirstPass(GGBlock *block, int x, int y, int size, 
                         signed char clipStatus)
{
    int size2 = size>>1;
    signed char tmp;
    if( (tmp = boxVisibilityStatus(block, x+size2, y+size2, clipStatus, NO)) >= 0 )
    {
        if( size == 2 )
        {
            int i, j;
            i = x>>1;
            j = y>>1;
            if( Fils[i][j].block || trySplit(block, i,j) )
                /*we draw recursively this son */
            {
                NSCParameterAssert(Fils[i][j].block!=nil);
                if( !Local && Fils[i][j].block->local )
                    [Manager->localStudy addObject: Fils[i][j].block];
                else
                    firstPass(Fils[i][j].block, tmp);
            }
        }
        else
        {
            subFirstPass(block, x, y, size2, tmp);
            subFirstPass(block, x+size2, y, size2, tmp);
            subFirstPass(block, x, y+size2, size2, tmp);
            subFirstPass(block, x+size2, y+size2, size2, tmp);
        }
    }
}

static void firstPass(GGBlock *block, signed char  clipStatus)
{
    NSCParameterAssert(block != nil);
#ifdef DEBUG
    nPass1++;
#endif
    //    if( !splitable )
    //      {
    //        return;
    //      }  //we visit everybody
    
    /* that will update critical data like "Decal" that is very important for local blocks.
    */
    blockLod(block);
    
    if( NSonSplit < BRANCHEMENT*BRANCHEMENT )
    {
        if( Local )
            [Manager->forLocal addObject: block];
        else
            [Manager->forGlobal addObject: block];
    }
    if( Lod == -2) /*We may have sons*/
    {
        subFirstPass(block, 0, 0, SIZEBOX, clipStatus);
    }
}

static void subSecondPassBis(GGBlock *block, int x, int y, int size, 
                             signed char clipStatus)
{
    int size2 = size>>1;
    signed char tmp;
    if( (tmp = boxVisibilityStatus(block, x+size2, y+size2, clipStatus, YES)) >= 0 )
    {
        if( size == 2 )
        {
            int i, j;
            i = x>>1;
            j = y>>1;
            if( !Fils[i][j].block )
            {
                
#ifdef USE_COL_IN_DATA
#define Vertex(i,j) do{\
    glNormal3v((GGReal *)&Data[i][j].norm);\
    glColor4v(&Data[i][j].col);\
		glVertex3v(&Data[i][j].v);}while(0)
#else
#define Vertex(i,j) do{\
            glNormal3v((GGReal *)&Data[i][j].norm);\
                    glVertex3v(&Data[i][j].v);}while(0)
#endif
            
            if( (i == 0 && Gauche == nil) || 
                (i == BRANCHEMENT-1 && Droite == nil) ||
                (j == 0 && Bas == nil) ||
                (j == BRANCHEMENT-1 && Haut == nil) )
                return;

	      glBegin(GL_TRIANGLE_STRIP);
	      Vertex(i,j);
	      Vertex(i+1,j);
	      Vertex(i,j+1);
	      Vertex(i+1,j+1);
	      glEnd();
#undef Vertex
            }

        }
      else
      {
          subSecondPassBis(block, x, y, size2, tmp);
          subSecondPassBis(block, x+size2, y, size2, tmp);
          subSecondPassBis(block, x, y+size2, size2, tmp);
          subSecondPassBis(block, x+size2, y+size2, size2, tmp);
      }
    }
}


/*
 alReady visited is YES if we were here during the first pass.
 That way, we can use the cached result for the clipping test.  Otherwise
 we have to compute it
 */
void secondPassBis(GGBlock *block)
{
    NSCParameterAssert(block != nil);
#ifdef DEBUG
    
    nPass2++;
#endif
    glPushMatrix();
    glTranslatef(Decal.x, Decal.y, Decal.z);
    if( 1 /*Local*/ )
    {
        Vect4D p = {{1, 0, 0}, 0};
        Vect4D q = {{0, 1, 0}, 0};
        Vect4D r = {{0, 0, 1}, 0};
        p.w = Origin.x;
        q.w = Origin.y;
        r.w = Origin.z;
        glTexGenfv(GL_S, GL_OBJECT_PLANE, (GGReal *)&p);
        glTexGenfv(GL_T, GL_OBJECT_PLANE, (GGReal *)&q);
        glTexGenfv(GL_R, GL_OBJECT_PLANE, (GGReal *)&r);
        
    }
    if( Lod >= -1) /*We don't have any sons*/
    {
        /*we draw ourself quite simply...
        */
        rasteriseBlock(block);
        
        
        /*pay attention to the neighboor, too avoid T junction.  Should be
        quite easy*/
    }
    else
    {
        subSecondPassBis(block, 0, 0, SIZEBOX, 0);
        if( Droite == nil || Gauche == nil || Haut == nil || Bas == nil )
            drawBorder(block);
    }
//    if( Local )
        glPopMatrix();
}


#if 1
#define Vertex(i,j) do{\
    ggcheck(i,j);\
        glArrayElement((i)*SIZEMIP+(j));\
}while(0)
#endif

#if 0
#define Vertex(i,j) do{\
    ggcheck(i,j);\
        glColor3f(1, 0, 0);\
            glNormal3v(&Data[i][j].norm);\
                glVertex3v(&Data[i][j].v);\
}while(0)
#endif
static void drawBorder(GGBlock *block)
{
    
    int i;
    //  glInterleavedArrays(GL_T2F_N3F_V3F, 0, &Data[0][0]);
    ggInterleavedArray(&Data[0][0]);
    
    if( !Gauche )
    {
        int deb, fin;
        deb = (Bas ? 0 : 1);
        fin = (Haut ? SIZEMIP : SIZEMIP-1);
        
        glBegin(GG_TRIANGLE_FAN);
        Vertex(0,0);
        for(i = deb; i <= (SIZEMIP>>1); ++i )
            Vertex(1,i);
        glEnd();
        
        glBegin(GG_TRIANGLE_FAN);
        Vertex(0,SIZEMIP-1);
        for(i = SIZEMIP>>1; i < fin; ++i )
            Vertex(1,i);
        glEnd();
        
        glBegin(GG_TRIANGLES);
        Vertex(0,0);
        Vertex(1, SIZEMIP>>1);
        Vertex(0, SIZEMIP-1);
        glEnd();
    }
    
    if( !Droite )
    {
        int deb, fin;
        deb = (Bas ? 0 : 1);
        fin = (Haut ? SIZEMIP : SIZEMIP-1);
        
        glBegin(GG_TRIANGLE_FAN);
        Vertex(SIZEMIP-1,0);
        for(i = SIZEMIP>>1; i >= deb; --i )
            Vertex(SIZEMIP-2,i);
        glEnd();
        
        glBegin(GG_TRIANGLE_FAN);
        Vertex(SIZEMIP-1,SIZEMIP-1);
        for(i = fin-1; i >= SIZEMIP>>1; --i )
            Vertex(SIZEMIP-2,i);
        glEnd();
        
        glBegin(GG_TRIANGLES);
        Vertex(SIZEMIP-1,0);
        Vertex(SIZEMIP-1,SIZEMIP-1);
        Vertex(SIZEMIP-2, SIZEMIP>>1);
        glEnd();
    }
    
    if( !Haut )
    {
        int deb, fin;
        deb = (Gauche ? 0 : 1);
        fin = (Droite ? SIZEMIP : SIZEMIP-1);
        
        glBegin(GG_TRIANGLE_FAN);
        Vertex(0,SIZEMIP-1);
        for(i = deb; i <= (SIZEMIP>>1); ++i )
            Vertex(i,SIZEMIP-2);
        glEnd();
        
        glBegin(GG_TRIANGLE_FAN);
        Vertex(SIZEMIP-1,SIZEMIP-1);
        for(i = SIZEMIP>>1; i < fin; ++i )
            Vertex(i, SIZEMIP-2);
        glEnd();
        
        glBegin(GG_TRIANGLES);
        Vertex(0,SIZEMIP-1);
        Vertex(SIZEMIP>>1, SIZEMIP-2);
        Vertex(SIZEMIP-1, SIZEMIP-1);
        glEnd();
    }
    
    if( !Bas )
    {
        int deb, fin;
        deb = (Gauche ? 0 : 1);
        fin = (Droite ? SIZEMIP : SIZEMIP-1);
        
        glBegin(GG_TRIANGLE_FAN);
        Vertex(0,0);
        for(i = SIZEMIP>>1; i >= deb; --i )
            Vertex(i, 1);
        glEnd();
        
        glBegin(GG_TRIANGLE_FAN);
        Vertex(SIZEMIP-1,0);
        for(i = fin-1; i >= SIZEMIP>>1; --i )
            Vertex(i, 1);
        glEnd();
        
        glBegin(GG_TRIANGLES);
        Vertex(0,0);
        Vertex(SIZEMIP-1, 0);
        Vertex(SIZEMIP>>1, 1);
        glEnd();
    }
    
#undef Vertex
}

- (GGBlock*) bottomNeighBor
{
    return bas;
}

- (GGBlock*) topNeighBor
{
    return haut;
}

- (GGBlock*) rightNeighBor
{
    return droite;
}

- (GGBlock*) leftNeighBor
{
    return gauche;
}

- (NSMutableArray*) _createAncestorPath
{
    NSMutableArray* answer;
    if (father) {
        answer = [father _createAncestorPath];
        [answer addObject:[NSNumber numberWithInt:frame.i]];
        [answer addObject:[NSNumber numberWithInt:frame.j]];
    }
    else {
        NSArray* topBlocks = [manager topLevelBlocks];
        NSAssert(topBlocks, @"no topblocks!");
        unsigned pos = [topBlocks indexOfObjectIdenticalTo:self];
        
        NSAssert(pos != NSNotFound, @"we are top and not in tops!");
        answer = [[NSMutableArray alloc] init];
        [answer addObject:[NSNumber numberWithInt:pos]];
    }
    return answer;
}

- (NSArray*) path
{
    return [[self _createAncestorPath] autorelease];
}


#pragma mark -
#pragma mark debugging stuff
#ifdef DEBUG
- (void) checkSons
{
    int k, l, count;
    count = 0;
    for( k = 0; k < BRANCHEMENT; ++k)
        for( l = 0; l < BRANCHEMENT; ++l )
            if( fils[k][l].block )
            {
                [fils[k][l].block checkTree: self
                                         at: k
                                        and: l];
                count++;
            }
                
                NSParameterAssert(count == nSonSplit);
    NSParameterAssert(count == 0 || lod == -2);
    
    if( lod == -2 )
    {
        NSParameterAssert(gauche == nil || gauche->lod <= -1);
        NSParameterAssert(droite == nil || droite->lod <= -1);
        NSParameterAssert(haut == nil || haut->lod <= -1);
        NSParameterAssert(bas == nil || bas->lod <= -1);
    }
    
    count = 0;
    for( k = 0; k < BRANCHEMENT; ++k)
        if( fils[k][BRANCHEMENT-1].block ) count++;
    NSParameterAssert(count == nhaut);
    
    count = 0;
    for( k = 0; k < BRANCHEMENT; ++k)
        if( fils[k][0].block ) count++;
    NSParameterAssert(count == nbas);
    
    count = 0;
    for( k = 0; k < BRANCHEMENT; ++k)
        if( fils[0][k].block ) count++;
    NSParameterAssert(count == ngauche);
    
    count = 0;
    for( k = 0; k < BRANCHEMENT; ++k)
        if( fils[BRANCHEMENT-1][k].block ) count++;
    NSParameterAssert(count == ndroite);
    nNode++;
    
    //   NSParameterAssert(droite == nil || droite->gauche == self);
    //   NSParameterAssert(gauche == nil || gauche->droite == self);
    //   NSParameterAssert(haut == nil || haut->bas == self);
    //   NSParameterAssert(bas == nil || bas->haut == self);
}

- (void) checkTree: (GGBlock*) pere
                at: (int) i
               and: (int) j
{
    NSParameterAssert(pere == father);
    if(pere != nil)
    {
        GGBlock **dummy;
        NSParameterAssert(droite == getNeighboor(pere, i, j, NgbDroite, &dummy));
        NSParameterAssert(! droite || *dummy == self);
        NSParameterAssert(gauche == getNeighboor(pere, i, j, NgbGauche, &dummy));
        NSParameterAssert(!gauche || *dummy == self);
        NSParameterAssert(haut == getNeighboor(pere, i, j, NgbHaut, &dummy));
        NSParameterAssert(!haut || *dummy == self);
        NSParameterAssert(bas == getNeighboor(pere, i, j, NgbBas, &dummy));
        NSParameterAssert(!bas || *dummy == self);
        //       NSParameterAssert(droite == [pere getSonAt: i+1
        // 					and: j]);
        //       NSParameterAssert(gauche == [pere getSonAt: i-1
        // 					and: j]);
        //       NSParameterAssert(haut == [pere getSonAt: i
        // 					and: j+1]);
        //       NSParameterAssert(bas == [pere getSonAt: i
        // 					and: j-1]);
    }
    [self checkSons];
}

+ (float) epsilon
{
    return sEpsilon;
}

+ (void) setEpsilon:(float)val
{
    sEpsilon = val;
}

+ (void) displayBorder
{
    border = !border;
    NSLog(@"border = %d", border);
}

- (void) displayNormal
{
    int thelod;
    int i, j, inc;
    GGReal coef = frame.size*300;
    thelod = (lod >= 0 ? lod : 0);
    inc = (1<<thelod);
    
    //  NSDebugMLLogWindow(@"Lod", @"theLod = %d inc = %d", thelod, inc);
    
    glDisable(GL_LIGHTING);
    glColor3f(1, 0, 0);
    
    if( local )
    {
        glPushMatrix();
        glTranslatef(frame.decal.x, frame.decal.y, frame.decal.z);
    }
    glBegin(GL_LINES);
    for(i = 0; i < SIZEMIP; i+= inc )
        for(j = 0; j < SIZEMIP; j+=inc )
        {
            Vect3D pt;
            glVertex3v(&data[i][j].v);
            addLambdaVect(&data[i][j].v, coef, &data[i][j].norm, &pt);
            glVertex3v(&pt);
        }
            glEnd();
    if( local )
        glPopMatrix();
    
}

- (void) printInfo
{
    int k, l;
    int i0=-1, j0=-1;
    NSDebugMLLogWindow(@"Block", @"block %s at [%d][%d] = %@", [self isLocal] ? "local" : "global", frame.i, frame.j,
                       self);
    
    NSDebugMLLogWindow(@"Block", @"gauche = %@\ndroite = %@\nhaut=%@\nbas=%@",
                       gauche, droite, haut, bas);
    
    NSDebugMLLogWindow(@"Block", @"eps : %d, %d, %d, %d, %d %d",
                       epsilon[0],epsilon[1],epsilon[2],epsilon[3],epsilon[4],
                       epsilon[5]);
    
    for( k = 0; k < BRANCHEMENT; ++k)
        for( l = 0; l < BRANCHEMENT; ++l )
            if( fils[k][l].block )
            {
                i0 = k;
                j0 = l;
            }
                
    NSDebugMLLogWindow(@"Block", @"floatlod =  %#.3g (%d,%d)", floatMipLevel, i0, j0);
    NSDebugMLLogWindow(@"Block", @"lod =  %d", lod);
}

+ (void) logStats
{
    NSDebugMLLogWindow(@"Triangle", @"triangle = @r%d@w\nnnode = %d nb=%d", 
                       nTriangle,	nNode, nBoxInFrustum);
    NSDebugMLLogWindow(@"Triangle", @"first pass @r%d@w\nsecond pass @r%d@w",
                       nPass1, nPass2);
    NSDebugMLLogWindow(@"Triangle", @"nPlanTest = %d", nPlanTest);
    NSDebugMLLogWindow(@"Triangle", @"mem = %d", [self class]->instance_size*
                       nNode);
}

- (void) drawSonBoxAt:(int)i and:(int)j
{
    Vect3D pts[8];
    initConcreteBox(&frame, &boundingBox[2*i+1][2*j+1].theBox, pts);
    
    NSDebugMLLogWindow(@"Block", @"block at [%d][%d] = %@", i, j,
                       fils[i][j].block);
    NSDebugMLLogWindow(@"Block", @"epsFather : %d, %d, %d, %d, %d %d",
                       epsilon[0],epsilon[1],epsilon[2],epsilon[3],epsilon[4],
                       epsilon[5]);
    NSDebugMLLogWindow(@"Block", @"floatlodFather = %#.3g", floatMipLevel);
    NSDebugMLLogWindow(@"BoxClip", @"sig = %d %d", 
                       signatureForBox(&manager->frustum,pts, 0),
                       boxInFrustum(&manager->frustum, pts));
    
    ggPlotBox(pts);
}
#endif

- (void) rasterize
{
    firstPass(self, 0);
    //  secondPass(self, 0, YES);
}

+ (void) initGLContextBeforeDrawing: (Texture *)theTexture
{
    splitable = YES;
    glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT | GL_POLYGON_BIT |
                 GL_TEXTURE_BIT);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
    //  glColor3f(0.7, 0.7, 0.7);
    
#ifdef DEBUG 
    if( !border &&  theTexture != nil )
#else
        if( theTexture != nil )
#endif
        {
            Vect4D p = {{1, 0, 0}, 0};
            Vect4D q = {{0, 1, 0}, 0};
            Vect4D r = {{0, 0, 1}, 0};
            glEnable(GL_TEXTURE_CUBE_MAP);
            glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
            glTexGenfv(GL_S, GL_OBJECT_PLANE, (GGReal *)&p);
            glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
            glTexGenfv(GL_T, GL_OBJECT_PLANE, (GGReal *)&q);
            glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
            glTexGenfv(GL_R, GL_OBJECT_PLANE, (GGReal *)&r);
            glEnable(GL_TEXTURE_GEN_S);
            glEnable(GL_TEXTURE_GEN_T);
            glEnable(GL_TEXTURE_GEN_R);
            
            glBindTexture(GL_TEXTURE_CUBE_MAP, [theTexture idTexture]);
            glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
        }
            
            
#ifdef DEBUG
            glPolygonMode(GL_FRONT, (border ? GL_LINE : GL_FILL));
#endif
#ifdef HardDebug
    glDisable(GL_LIGHTING);
#endif
    
}

+ (void) finish
{
    glPopAttrib();
    
}

#ifdef DEBUG
+ (void) resetStat
{
    nTriangle = 0;
    nNode = 0;
    nPointInFrustum = 0;
    nBoxInFrustum = 0;
    nPass2 = nPass1 = 0;
    nPlanTest=0;
}

#endif
@end

#if defined(DEBUG) && defined(HAVE_COCOA)

@implementation GGDebugController (Block)
- (void) reloadPlanet:(id)sender
{
    [Planete reload];
}

- (float) epsilon
{
    return [GGBlock epsilon];
}

- (void) setEpsilon:(float)val
{
    [GGBlock setEpsilon:val];
}

- (BOOL) recomputeNormal
{
    return RecomputeNormal;
}

- (void) setRecomputeNormal:(BOOL)val
{
    RecomputeNormal = val;
}
@end
#endif

#pragma mark -
#pragma mark already visited

/* The following code looks dead for a very long time.  I don't remember how it worked.  Apparently, during the second pass, we were going down through the whole hierarchy.   Now, we only handle the current block.  The first pass has already put in a queue the list of all the blocks to be rendered. 
*/
#if 0
static void secondPass(GGBlock *block, signed char clipStatus, BOOL visited);

static void subSecondPass(GGBlock *block, int x, int y, int size, 
                          signed char clipStatus,
                          BOOL visited)
{
    int size2 = size>>1;
    signed char tmp;
    if( (tmp = boxVisibilityStatus(block, x+size2, y+size2, clipStatus, visited)) >= 0 )
    {
        if( size == 2 )
        {
            int i, j;
            i = x>>1;
            j = y>>1;
            if( Fils[i][j].block )
                /*we draw recursively this son */
            {
                secondPass(Fils[i][j].block, tmp, visited);
            }
            else
            {
                
#define Vertex(i,j) do{\
    glNormal3v(&Data[i][j].norm);\
		glVertex3v(&Data[i][j].v);}while(0)
            
            if( (i == 0 && Gauche == nil) || 
                (i == BRANCHEMENT-1 && Droite == nil) ||
                (j == 0 && Bas == nil) ||
                (j == BRANCHEMENT-1 && Haut == nil) )
                return;

	      glBegin(GL_TRIANGLE_STRIP);
	      Vertex(i,j);
	      Vertex(i+1,j);
	      Vertex(i,j+1);
	      Vertex(i+1,j+1);
	      glEnd();
#undef Vertex
            }

        }
      else
      {
          subSecondPass(block, x, y, size2, tmp, visited);
          subSecondPass(block, x+size2, y, size2, tmp, visited);
          subSecondPass(block, x, y+size2, size2, tmp, visited);
          subSecondPass(block, x+size2, y+size2, size2, tmp, visited);
      }
    }
}

/*
 alReady visited is YES if we were here during the first pass.
 That way, we can use the cached result for the clipping test.  Otherwise
 we have to compute it
 */
static void secondPass(GGBlock *block, signed char clipStatus, BOOL visited)
{
    NSCParameterAssert(block != nil);
#ifdef DEBUG
    
    nPass2++;
#endif
    visited = visited && AlreadyVisited;
    if( Lod >= -1) /*We don't have any sons*/
    {
        /*we draw ourself quite simply...
        */
        rasteriseBlock(block);
        
        
        /*pay attention to the neighboor, too avoid T junction.  Should be
        quite easy*/
    }
    else
    {
        subSecondPass(block, 0, 0, SIZEBOX, clipStatus, visited);
        if( Droite == nil || Gauche == nil || Haut == nil || Bas == nil )
            drawBorder(block);
    }
}
#endif
