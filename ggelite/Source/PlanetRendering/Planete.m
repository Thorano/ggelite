/*
 *  Planete.m
 *
 *  Copyright (c) 2002 by Frédéric De Jaeger
 *  
 *  Date: February 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

//#import <X11/keysym.h>
#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <math.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSException.h>
#import <Foundation/NSArray.h>
#import "GGInput.h"
#import "Planete.h"
#import "GGRepere.h"
#import "utile.h"
#import "GG3D.h"
#import "Tableau.h"
#import "Commandant.h"
#import "perlin.h"
#import "GGBlock.h"
#import "Texture.h"
#import "Preference.h"
#import "Station.h"
#import "System.h"
#import "GGBlockCollision.h"
#import "GGBlockContext.h"


#ifdef DEBUG
static ProtoPlanete *sCurrentPlanet = nil;
#endif

#if defined(DEBUG) && defined(HAVE_COCOA)
#import "GGDebugController.h"

@implementation GGDebugController (Bump)
- (ProtoPlanete*) currentPlanete
{
    return sCurrentPlanet;
}
- (void) setCurrentPlanete:(ProtoPlanete*)value
{
    NSLog(@"current is %@", value);
    sCurrentPlanet = value;
}

- (void) reloadTexture:(id)sender
{
    [Planete reloadTexture];
}

@end
#endif

#define PRECISION 20

@implementation LocalPlanetBlock
- (id) initWithBlockContext:(GGBlockContext *) _c
{
    self = [super init];
    if(self){
        context = [_c retain];;
        _flags.graphical = YES;
        [self setVisible: YES];
    }
    
    return self;
}

+ localBlock: (GGBlockContext *) _c
{
    LocalPlanetBlock *obj;
    obj = [[self alloc] initWithBlockContext:_c];
    
    return AUTORELEASE(obj);
}

- copyWithZone: (NSZone *)zone
{
    [NSException raise: NSInternalInconsistencyException
                format: @"%@ should not be copied", self];
    return nil;
}

- (void) dealloc {
    [context release];
    [super dealloc];
}


- (Vect3D)col {
    return col;
}

- (void)setCol:(Vect3D)value {
    col = value;
}

- (BOOL) shouldCache
{
    return NO;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

- (void) rasterise: (Camera *)cam
{
    [context rasteriseLocalPart:self camera:cam];
}

- (GGBox) boundingBox
{
    
    GGBox box = [context boudingBoxForLocalBlocks];
    box = transformeBox(Matrice(self), box);
    
    return box;
}

- (void) changeLocalCoordinates: (GGGalileen *) gal
{
    NSDebugMLLog(@"Scene", @"change of repere local, we do nothing");
}

- (GGSolid*) createDynamicalHelper
{
    return [context createDynamicalHelper];
}

@end


@implementation ProtoPlanete
+ (GGBlockContext*) defaultContextForDebugging
{
#if DEBUG_COCOA
    return [[[GGDebugController controller] currentPlanete] blockContext];
#else
    return nil;
#endif
}



- (void) _initProtoPlanete
{
    _context = [[GGBlockContext alloc] init];
}

- (void) setWithDictionary: (NSDictionary *)dic
                      name: (NSString *)_name
{
    NSString *val;
    [super setWithDictionary: dic
                        name: _name];
    
    
    if( (val = [dic objectForKey:@"LightSource"]) != nil )
    {
        NSDebugMLLog(@"dico", @"LightSource = %@", val);
        [self setLightSource: [val intValue]];
    }
    
    if( (val = [dic objectForKey:@"texture"]) != nil )
    {
        NSDebugMLLog(@"dico", @"texture = %@", val);
        if( thePref.GLTexture)
        {
            ASSIGN(textureName, val);
        }
    }
    
}

- init
{
    Vect3D _col = { 0.8, 0.8, 0.8 };
    self = [super init];
    
    
    col = _col;
    
    [self _initProtoPlanete];
    
    return self;
}


- (id) copyWithZone: (NSZone *)zone
{
    ProtoPlanete *copy;
    copy = [super copyWithZone: zone];
    copy->lb = nil;
    copy->_context = nil;
    copy->textureName = [textureName copy];
    [copy _initProtoPlanete];
    return copy;
}


- (void) dealloc
{
    [_context release];
    RELEASE(textureName);
    [super dealloc];
}

- (GGBlockContext*) blockContext
{
    return _context;
}

- (BOOL) lightSource
{
    return lightSource;
}

- (void) setLightSource: (BOOL) val
{
    lightSource = val;
}

- (void) cameraBeginBeingClose
{
    if( textureName )
        [self setTexture: [Texture cubeMapTextureWithFileName:  textureName]];
}


- (void) cameraStopBeingClose
{
    [self setTexture: nil];
}

- (void) startBeingMainLocale
{
    [super startBeingMainLocale];
#ifdef DEBUG
#ifdef HAVE_COCOA
    [[GGDebugController controller] setCurrentPlanete:self];
#else
#error caca
    sCurrentPlanet = self;    
#endif
#endif

}

- (void) stopBeingMainLocale
{
    [super stopBeingMainLocale];
#ifdef DEBUG
#ifdef HAVE_COCOA
    [[GGDebugController controller] setCurrentPlanete:nil];
#else
    sCurrentPlanet = nil;    
#endif
#endif    
}

- (void) setTexture: (Texture *)t
{
    [_context setTexture:t];
    [self invalidCache];
}

- (BOOL) shouldCache
{
    return [_context topLevelBlocks] == nil;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

- (void) beginBeingSuperLocal
{
    GGNode *aNode;
    repereRotatif = aNode = AUTORELEASE([GGNode new]);
    [repereRotatif setDefaultName: 
        [NSString stringWithFormat: @"repereRotatif attache a %@",
            self]];
    *Galileen(repereRotatif) = *Galileen(self);
    NSAssert(repereTranslation, @"we should have repereTranslation at that moment");
    [repereTranslation addSubNode: repereRotatif];
    NSAssert([repereRotatif nodePere] == [self nodePere], 
             @"bad");
    
    [aNode reparent: self];
    initWithIdentityTransfo(Galileen(self));
    SetZeroVect(*Omega(self));
    
    [super beginBeingSuperLocal];
    
    [aNode setMode: GGInertialRotationalMode];
    
    //we don't need to retain it
    NSAssert(lb == nil, @"bad");
    [self _setupLocalPart];
}

- (void) _setupLocalPart
{
    if(lb){
        return;
    }
    
    lb = [LocalPlanetBlock localBlock:_context];
    [lb setCol:col];
    
    [[manager repere] addSubNode: lb];
    [_context setTopLevelBlocks:[self makeTopLevelBlocks]];
    [self invalidCache];    
}


- (void) endBeingSuperLocal
{
    GGNode *aNode = (GGNode *)repereRotatif;
    NSAssert(repereRotatif != nil, @"bad");
    NSAssert(lb != nil, @"bad");
    [self _removeLocalPart];
    [aNode removeFromSuperNodeAndReparentSons];
    repereRotatif = nil;
    [super endBeingSuperLocal];
    [theCommandant setNearPlanete: NO];
}

- (void) _removeLocalPart
{
    if(nil == lb){
        return;
    }
    
    [lb removeFromSuperNode];
    lb = nil;    
    [_context setTopLevelBlocks:nil];
    [self invalidCache];
}


- (void) rasterise: (Camera *)cam
{
    glColor3f(col.x, col.y, col.z);
    if( [self shouldCache] )
    {
        [self drawSimple];
    }
    else{
        [_context rasteriseGlobalPartWithCamera:cam globalPart:self];
    }
}

- (NSArray*) makeTopLevelBlocks
{
    [self subclassResponsibility: _cmd];
    return nil;
}

- (void) drawSimple
{
    [self subclassResponsibility: _cmd];
}

#ifdef DEBUG
- (id) noise
{
    return nil;
}

- (id) localNoise
{
    return nil;
}

- (void) reload
{
    if(lb){
        [_context setTopLevelBlocks:[self makeTopLevelBlocks]];
        [self invalidCache];
    }
}

- (void) reloadTexture
{
    
}

+ (void) reload
{
    [sCurrentPlanet reload];
}

+ (void) reloadTexture
{
    [sCurrentPlanet reloadTexture];
}

#endif
@end


@implementation Planete
static double SCALEAlt = 1000;
static double SCALEF = 1.0/1000.0;
static double ALPHACOEF = 2.19;
static int NHARM = 8;
static double OCTAVE = 2.1;

- (id<TerrainSource>) landscape
{
    if(nil == _noise){
        GGNoise* noise =  [GGNoise noise];
        [noise setScaleInput:SCALEF];
        [noise setScaleOutput:SCALEAlt];
        [noise setAlpha:ALPHACOEF];
        [noise setNharmonic:NHARM];
        [noise setBeta:OCTAVE];
        _noise = [noise retain];
    }
    
    return _noise;
}

- (NSArray*) makeTopLevelBlocks
{
    GGBlock		*b1, *b2, *b3, *b4, *b5, *b6;
    id<TerrainSource> noise = [self landscape];
    PerlinBump *  bump = [PerlinBumpSphere bumpWithNoise:noise radius:radius];
    
    b1 = ([GGBlock topBlockWithManager: _context
                                        bump: bump
                                          at: 0
                                         and: -M_PI/4
                                       width: M_PI/2
                                      height: M_PI/2]);
    b2 = ([GGBlock topBlockWithManager: _context
                                        bump: bump
                                          at: M_PI/2
                                         and: -M_PI/4
                                       width: M_PI/2
                                      height: M_PI/2]);
    b3 = ([GGBlock topBlockWithManager: _context
                                        bump: bump
                                          at: M_PI
                                         and: -M_PI/4
                                       width: M_PI/2
                                      height: M_PI/2]);
    b4 = ([GGBlock topBlockWithManager: _context
                                        bump: bump
                                          at: 3*M_PI/2
                                         and: -M_PI/4
                                       width: M_PI/2
                                      height: M_PI/2]);
    
    bump = [PerlinBumpSphereTop bumpWithNoise:noise radius:radius];
    b5 = ([GGBlock topBlockWithManager: _context
                                        bump: bump
                                          at: -1
                                         and: -1
                                       width: 2
                                      height: 2]);
    
    bump = [PerlinBumpSphereBottom bumpWithNoise:noise radius:radius];
    b6 = ([GGBlock topBlockWithManager: _context
                                        bump: bump
                                          at: -1
                                         and: -1
                                       width: 2
                                      height: 2]);
    
    b1->gauche = b4;
    b2->gauche = b1;
    b3->gauche = b2;
    b4->gauche = b3;
    
    b1->droite = b2;
    b2->droite = b3;
    b3->droite = b4;
    b4->droite = b1;
    
    b1->haut = b5;
    b2->haut = b5;
    b3->haut = b5;
    b4->haut = b5;
    
    b5->bas = b1;
    b5->droite = b2;
    b5->haut = b3;
    b5->gauche = b4;
    
    b1->bas = b6;
    b2->bas = b6;
    b3->bas = b6;
    b4->bas = b6;
    
    b6->bas = b4;
    b6->droite = b3;
    b6->haut = b2;
    b6->gauche = b1;
    
    NSArray* topBlocks = [NSArray arrayWithObjects:
        b1, b2, b3, b4, b5, b6, nil];
    
    NSDebugMLLog(@"Sphere", @"%@ builded", self);
    
    return topBlocks;
}

- (void) __privatePlanetePostInit
{
    distanceLocalOrbiteur = SQ(2*radius);
    distanceGlobalOrbiteur = SQ(3*radius);
    dimension = radius;
    if(0 == masse){
        // if it was not set
        masse = 4.0/3.0*M_PI*radius*radius*radius*1000;
    }
    
    CollisionCounter(self) = -1;
}


- (void) setWithDictionary: (NSDictionary *)dic
                      name: (NSString *)_name
{
    NSString *val;
    [super setWithDictionary: dic
                        name: _name];
    
    if( (val = [dic objectForKey:@"radius"]) != nil )
    {
        NSDebugMLLog(@"dico", @"radius = %@", val);
        radius = [val doubleValue];
    }
    
    
    [self __privatePlanetePostInit];
    
}

- init
{
    [super init];
    
    
    radius = 1e3;
    
    [self __privatePlanetePostInit];
    
    return self;
}

- (void) setRadius:(double)val
{
    radius = val;
    [self __privatePlanetePostInit];
}

- (id) copyWithZone: (NSZone *)zone
{
    Planete *copy;
    copy = [super copyWithZone: zone];
    copy->_noise = nil;
    return copy;
}

- (int) setRandomSatAt: (GGReal) orbiteRadius
               station: (int) nstation
{
    int i;
    GGReal orbiteRadiusStation;
    NSString *texName[] = {
        @"callisto.jpg",
        @"charon.jpg",
        @"europa.jpg",
        @"moon.jpg",
        @"neptune.jpg"};
    ParametresOrbitaux prm =
    {
        0,
        0,
        0,
        
        0.,
        0,
        0
    };
    prm.a = orbiteRadius;
    radius = (vrnd()*5+1)*1e6;
    prm.omegaPropre = (vrnd() * 2 + 1)*2e-5;
    prm.M = vrnd()*2*M_PI;
    [self setParametresOrbitaux: &prm];
    ASSIGN(textureName, texName[irnd(sizeof(texName)/sizeof(NSString *))]);
    
    orbiteRadiusStation = radius*4;
    
    nstation = nstation > 2 ? 2 : nstation;
    
    for( i = 0; i < nstation && orbiteRadiusStation < orbiteRadius / 300; ++i )
    {
        Station *s;
        s = [Station new];
        [s setRandomStationAtRadius:    orbiteRadiusStation];
        [s setOmegaOrbite: omegaOrbite(radius, orbiteRadiusStation)];
        [self addSon: s];
        RELEASE(s);
        orbiteRadiusStation *= 2+5*vrnd();
    }    
    
    [self __privatePlanetePostInit];
    
    return nstation;
}

- (int) setRandomPlaneteAt: (GGReal) _orbiteRadius
                 telurique: (BOOL) telurique
                   station: (int) nstation
{
    int cs = 0;
    int nSat, i;
    NSString *texName[] = {
        @"callisto.jpg",
        @"charon.jpg",
        @"europa.jpg",
        @"moon.jpg",
        @"neptune.jpg"};
    ParametresOrbitaux prm =
    {
        0,
        0,
        0,
        
        0.,
        0,
        0
    };
    float orbiteRadius;
    prm.a = _orbiteRadius;
    if( telurique )
    {
        radius = 5e6*(vrnd() + 2);
        nSat = nrnd(0,3);
    }
    else
    {
        radius = (vrnd()*10+1)*5e6;
        nSat = nrnd(0, 6);
    }
    prm.omegaPropre = (vrnd() * 2 + 1)*2e-5;
    prm.M = vrnd()*2*M_PI;
    [self setParametresOrbitaux: &prm];
    ASSIGN(textureName, texName[irnd(sizeof(texName)/sizeof(NSString *))]);
    
    //  nSat = irnd(3*log(radius)/log(1e6));
    
    orbiteRadius = radius*10;
    
    if( 1 || (nstation && proba(1/(nSat+1)) ))
    {
        Station *s;
        s = [Station new];
        [s setRandomStationAtRadius:    4*radius];
        [s setOmegaOrbite: omegaOrbite(radius, orbiteRadius)];
        [self addSon: s];
        RELEASE(s);
        cs ++;
    }
    
    for( i = 0; i < nSat && orbiteRadius < (_orbiteRadius/300.0); ++i )
    {
        Planete *p;
        p = [Planete new];
        [p setName: [NSString stringWithFormat: @"%@ %c", self, 'a'+i]];
        cs += [p setRandomSatAt: orbiteRadius
                        station: nrnd(0,nstation)];
        [p setOmegaOrbite: omegaOrbite(radius, orbiteRadius)];
        [self addSon: p];
        RELEASE(p);
        orbiteRadius *= 1.5+0.5*vrnd();
        
    }
    
    [self __privatePlanetePostInit];
    
    return cs;
}

- setRandomSystemForStar: (System *)s
    /*use Titus-Bode law to generate random system
    */
{
    int nstation;
    float pop;
    GGReal a = 0.4;
    int n;
    int i;
    GGReal	orbiteRadius;
    [super setRandomSystemForStar: s];
    radius = (5*vrnd()+0.3)*1e9;
    [self setLightSource: YES];
    
    pop = [s population];
    
    if( pop  < 1000 )
        n = nrnd(0,5);
    else
        n = nrnd(5,5);
    
    if (pop <= 1 )
        nstation = 1;
    else 
        nstation = pop / 10000 + 1;
    
    orbiteRadius = (1 + vrnd())*5e10;
    
    for( i = 0; i < n; ++i )
    {
        Planete *p;
        p = [Planete new];
        [p setName: [NSString stringWithFormat: @"%@-%d", self, i+1]];
        nstation -= 
            [p setRandomPlaneteAt: a*orbiteRadius
                        telurique: proba(1.0/(1.0+i))
                          station: nstation];
        if( nstation < 0 )
            nstation = 0;
        [p setOmegaOrbite: omegaOrbite(radius, orbiteRadius)];
        [self addSon: p];
        RELEASE(p);
        //      orbiteRadius *= 1.8+5*vrnd();
        
        if( i > 0 )
            a = 0.4+(0.3)*(2<<(i-1));
        else
            a = 0.7;
    }
    
    [self __privatePlanetePostInit];
    return self;
}

- (GGReal) proximite
{
    return 2*radius;
}

- (void) drawSimple
{
    GLUquadricObj *data;
    
    data = gluNewQuadric();
    gluQuadricNormals(data, GLU_SMOOTH);
    gluQuadricOrientation(data, GLU_OUTSIDE);
    
    
    [GGBlock initGLContextBeforeDrawing: [_context texture]];
    if( lightSource )
    {
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);
    }
    
    gluSphere(data, radius, PRECISION, PRECISION);
    gluDeleteQuadric(data);
    [GGBlock finish];
}

#ifdef DEBUG
- (id) noise
{
    return _noise;
}
#endif
@end

@implementation Torus
static int mod3(int n)
{
    NSCParameterAssert(-2 <= n);
    if( n < 0 )
        return n+3;
    else if (n < 3)
        return n;
    else
        return n%3;
}

- (NSArray*) makeTopLevelBlocks
{
    GGBlock 		*blocks[3][3];
    int i, j;
    const double c2pi3 = (2.0 * M_PI / 3.0);
    NSObject<GGBump> *  bump;
    bump = [PerlinBumpTore torusMajorRadius: radius
                                minorRadius: radius/10.0];
    
    for(i = 0; i < 3; ++i)
        for(j = 0; j < 3; ++j)
            blocks[i][j] = ([GGBlock topBlockWithManager: _context
                                                          bump: bump
                                                            at: i * c2pi3
                                                           and: j * c2pi3
                                                         width: c2pi3
                                                        height: c2pi3]);
    
    
    NSDebugMLLog(@"Torus", @"%@ builded", self);
    
    for( i = 0; i < 3; ++i)
        for(j = 0; j < 3; ++j)
        {
            blocks[i][j]->droite = blocks[mod3(i+1)][j];
            blocks[i][j]->gauche = blocks[mod3(i-1)][j];
            blocks[i][j]->haut = blocks[i][mod3(j+1)];
            blocks[i][j]->bas = blocks[i][mod3(j-1)];
        }
            return [NSArray arrayWithObjects:&blocks[0][0] count:9];
}

- (void) __privatePlanetePostInit
{
    distanceLocalOrbiteur = SQ(2*radius);
    distanceGlobalOrbiteur = SQ(3*radius);
    dimension = radius;
    masse = 4.0/3.0*M_PI*SQ(radius)*1000;
    
    CollisionCounter(self) = -1;
}


- (void) setWithDictionary: (NSDictionary *)dic
                      name: (NSString *)_name
{
    NSString *val;
    [super setWithDictionary: dic
                        name: _name];
    
    if( (val = [dic objectForKey:@"radius"]) != nil )
    {
        NSDebugMLLog(@"dico", @"radius = %@", val);
        radius = [val doubleValue];
    }
    
    
    [self __privatePlanetePostInit];
    
}

- init
{
    [super init];
    
    
    radius = 1e6;
    
    [self __privatePlanetePostInit];
    NSLog(@"Torus is here");
    
    return self;
}

- (GGReal) proximite
{
    return 2*radius;
}

- (void) drawSimple
{
    GLUquadricObj *data;
    
    data = gluNewQuadric();
    gluQuadricNormals(data, GLU_SMOOTH);
    gluQuadricOrientation(data, GLU_OUTSIDE);
    
    
    [GGBlock initGLContextBeforeDrawing: [_context texture]];
    if( lightSource )
    {
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);
    }
    
    gluSphere(data, radius, PRECISION, PRECISION);
    gluDeleteQuadric(data);
    [GGBlock finish];
}

@end

#import "GGProceduralLandscape.h"

@implementation Experience
- (id) init
{
    self = [super init];
    if(self){
        _landscape = [GGProceduralLandscape new];
    }
    return self;
}

- (void) dealloc
{
    RELEASE(_landscape);
    [super dealloc];
}

- (id) copyWithZone: (NSZone *)zone
{
    [_landscape retain];
    return [super copyWithZone:zone];
}

- (GGProceduralLandscape*) landscape
{
    return _landscape;
}

- (void) loadTexture
{
    PerlinBumpSphere* sphereGen = [PerlinBumpSphere bumpWithNoise:_landscape radius:radius];
    [self setTexture: [Texture boxTexture:sphereGen]];
}

- (void) cameraBeginBeingClose
{
    [super cameraBeginBeingClose];
    [self loadTexture];
}

- (void) reload
{
    [super reload];
}

- (void) reloadTexture
{
    [self loadTexture];
}

- (id) noise
{
    return [_landscape continentalNoise];
}

- (id) localNoise
{
    return [_landscape highFrequency];
}


- (id) proceduralGenerator
{
    return _landscape;
}
@end


