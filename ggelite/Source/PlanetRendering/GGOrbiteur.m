/*
 *  GGOrbiteur.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 Various notifications
 
 -tryBeingSuperLocal:   called sometimes to know when the local frame
 is close to the current orbiteur.  Is so, reparent the
 local frame as a son of repereAbsolu
 -tryBeingSemiLocal:    when the orbiteur is super local and the local frame
 gets farther, disband the super local status and reparent
 the local frame as a son of repereTranslation.  This is
 the reverse action as the previous notification.
 -  updatePosition :   called every frame to update graphical states
 or sometimes.  It depends on the distance of the orbiteur to the local 
 frame.
 -updateOrbite:         called sometimes to update orbital parameters
 -thingsToDoWhenIdle:   called sometimes to decide how often one should call
 updateOrbite
 
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSEnumerator.h>
#import <Foundation/NSNotification.h>


#import "GGOrbiteur.h"
#import "GGRepere.h"
#import "GGLoop.h"
#import "Metaobj.h"
#import "Tableau.h"
#import "Texture.h"
#import "Sched.h"
#import "World.h"
#import "utile.h"
#import "Preference.h"
#import "Station.h"
#import "Planete.h"
#import "GGSky.h"
#import "System.h"

NSString *GGOrbiteurBeingSuperLocal = @"GGOrbiteurBeingSuperLocal";
NSString *GGOrbiteurEndBeingSuperLocal = @"GGOrbiteurEndBeingSuperLocal";


@interface GGOrbiteur (PrivateMethod)
- (void) addToArray: (NSMutableArray *)array;
@end

#define SQ(x) ((x)*(x))
static const float tolForExactCalcul = SQ(0.00005);

@implementation GGOrbiteur
- initWithDictionary: (NSDictionary *)dic
                name: (NSString *)_name
{
    [self init];
    [self setWithDictionary: dic
                       name: _name];
    return self;
}

- (void) setWithDictionary: (NSDictionary *)dic
                      name: (NSString *)_name
{
    NSString *val;
    ParametresOrbitaux prm =
    {
        0, // N
        0, // i
        0, // w
        0, // a
        0, // e
        0, // M
        
        0.,//omegaorbite
        0, //i1
        0, //i2
        0  //omegapropre
    };
    
    
    [self setName: _name];
    
    NSDebugMLLog(@"dico", @"creation of %@", _name);
    
    if ((val = [dic objectForKey:@"demiaxe"]) != nil )
    {
        prm.a = [val doubleValue]*150e9;
        NSDebugMLLog(@"dico", @"demiaxe = %g", prm.a);
    }
    
    if((val = [dic objectForKey:@"Masse"]) != nil){
        masse = [val doubleValue];
    }
    
    if ((val = [dic objectForKey:@"a"]) != nil )
    {
        prm.a = [val doubleValue]*150e9;
        NSDebugMLLog(@"dico", @"demiaxe = %g", prm.a);
    }
    
    if( (val = [dic objectForKey:@"MeanLongitude"]) != nil )
    {
        NSDebugMLLog(@"dico", @"phase = %@", val);
        prm.M = [val doubleValue]/180*M_PI;
    }
    
    if( (val = [dic objectForKey:@"omegaorbite"]) != nil )
    {
        prm.omegaOrbite = [val doubleValue];
        NSDebugMLLog(@"dico", @"omegaorbite = %g", prm.omegaOrbite);
    }
    
    if( (val = [dic objectForKey:@"DailyMotion"]) != nil )
    {
        prm.omegaOrbite = [val doubleValue]/180*M_PI/(24*3600);
        NSDebugMLLog(@"dico", @"omegaorbite = %g", prm.omegaOrbite);
    }
    
    if( (val = [dic objectForKey:@"i"]) != nil )
    {
        NSDebugMLLog(@"dico", @"inclination = %@", val);
        prm.i = [val doubleValue]/180*M_PI;
    }
    if( (val = [dic objectForKey:@"N"]) != nil )
    {
        NSDebugMLLog(@"dico", @"N = %@", val);
        prm.N = [val doubleValue]/180*M_PI;
    }
    if( (val = [dic objectForKey:@"w"]) != nil )
    {
        NSDebugMLLog(@"dico", @"w = %@", val);
        prm.w = [val doubleValue]/180*M_PI;
    }
    if( (val = [dic objectForKey:@"e"]) != nil )
    {
        NSDebugMLLog(@"dico", @"excentricity = %@", val);
        prm.e = [val doubleValue];
    }
    if( (val = [dic objectForKey:@"M"]) != nil )
    {
        NSDebugMLLog(@"dico", @"M = %@", val);
        prm.M = [val doubleValue]/180*M_PI;
    }
    
    if( (val = [dic objectForKey:@"inclinaison1"]) != nil )
    {
        NSDebugMLLog(@"dico", @"inclinaison1 = %@", val);
        prm.inclinaison1 = [val doubleValue];
    }
    if( (val = [dic objectForKey:@"inclinaison2"]) != nil )
    {
        NSDebugMLLog(@"dico", @"inclinaison2 = %@", val);
        prm.inclinaison2 = [val doubleValue];
    }
    if( (val = [dic objectForKey:@"omegapropre"]) != nil )
    {
        NSDebugMLLog(@"dico", @"omegapropre = %@", val);
        prm.omegaPropre = [val doubleValue];
    }
    
    
    [self setParametresOrbitaux: &prm];
    [self setVisible: NO];
    
    if( (val = [dic objectForKey: @"satellite"]) != nil )
    {
        NSDictionary *satDic = (NSDictionary *)val;
        NSEnumerator *en = [satDic keyEnumerator];
        id key;
        while( (key = [en nextObject]) )
        {
            NSString *nameSat = (NSString *)key;
            NSDictionary *subDic = [satDic objectForKey: key];
            NSString *type = [subDic objectForKey: @"type"];
            GGOrbiteur *sat;
            if (type != nil )
            {
                if ( [type isEqualToString: @"Station"] )
                {
                    sat = [Station alloc];
                }
                else if( type != nil && [type isEqualToString: @"Modele"] )
                {
                    sat = [Experience alloc];
                }
                else
                {
                    sat = [Planete alloc];
                }
            }
            else
                sat = [Planete alloc];
            [sat 	initWithDictionary: subDic
                                  name: nameSat];
            [self addSon: sat];
            RELEASE(sat);
        }
    }
}

- setRandomSystemForStar: (System *)s
{
    int index = [s index];
    GGSky *sky = [theWorld sky];
    [self setName: [NSString stringWithUTF8String: [sky cNameOf: index]]];
    return self;
}

- initRandomSystemForStar: (System *) s
{
    [self init];
    return [self setRandomSystemForStar: s];
}

- initSystemWithContentOfFile: (NSString *)fileName
{
    NSDictionary *dic = [NSDictionary dictionaryWithContentsOfFile: fileName];
    NSAssert(fileName, @"no file name given");
    
    if( dic == nil )
    {
        NSWarnMLog(@"can't init solar System with file %@", fileName);
        RELEASE(self);
        return nil;
    }
    else
    {
        NSArray *keys = [dic allKeys];
        if( [keys count] != 1 )
        {
            NSWarnMLog(@"inconsistent dictionary");
            RELEASE(self);
            return nil;
        }
        else
        {
            NSString *namePlan = (NSString *)[keys objectAtIndex: 0];
            return [self initWithDictionary: 
                (NSDictionary *)[dic objectForKey: 
                    namePlan]
                                       name: namePlan];
        }
    }
}

- (void) invalidate
{
    [fils makeObjectsPerformSelector: @selector(invalidate)];
    [super invalidate];
}

- (void) dealloc
{
    RELEASE(fils);
    [super dealloc];
}


/* We assume that the copy is just needed for the orbital map.  Thus, some 
variable are cleared for safety.
*/
- (id) copyWithZone: (NSZone *)zone
{
    int n;
    GGOrbiteur *copy = [super copyWithZone: zone];
    copy->fils = [NSMutableArray new];
    copy->pere = nil;
    copy->repereTranslation = nil;
    copy->repereRotatif = nil;
    copy->exactCalcul = 0;
    copy->typeOrbiteur = GGStandard;
    copy->proche = NO;
    copy->surroundingSystem = nil;
    n = [fils count];
    if( n > 0 )
    {
        GGOrbiteur *tab[n];
        int i;
        [fils getObjects: tab];
        for( i = 0; i < n; ++i)
        {
            GGOrbiteur *copyFils = [tab[i] copyWithZone: zone];
            [copy  addSon: copyFils];
            RELEASE(copyFils);
        }
    }
    return copy;
}


- init
{
    ParametresOrbitaux planete =
    {
        0, // N
        0, // i
        0, // w
        0, // a
        0, // e
        0, // M
        
        0.,//omegaorbite
        0, //i1
        0, //i2
        0  //omegapropre
    };
    
    [super init];
    tailleSysteme = 1e9;
    fils = [NSMutableArray new];
    pere = nil;
    exactCalcul = 0;
    distanceCamera2 = 0;
    
    typeOrbiteur = GGStandard;
    
    
    _flags.graphical = YES;
    [self setParametresOrbitaux: &planete];
    [self setVisible: NO];
    return self;
}

- (void) reallyDestroy
{
    NSAssert(0, @"can't destroy a planet...");
}

- (void) looseMainSystem
{
    [fils makeObjectsPerformSelector: @selector(looseMainSystem)];
    [super looseMainSystem];
}


- (GGReal) tailleSysteme
{
    return tailleSysteme;
}


- (void) setOmegaOrbite: (GGReal) val
{
    orbite.omegaOrbite = val;
}

- (id)replacementObjectForArchiver:(NSArchiver *)anArchiver
{
    return [GGArchiver  ggArchiver: 
        [NSString  stringWithFormat: @"orb:%@", [self name]]];
}


- (void) encodeWithCoder: (NSCoder *) encoder
{
}

- (void) setParametresOrbitaux: (ParametresOrbitaux*) _orbite
{
    Vect3D x = {1, 0, 0};
    Vect3D y = {0, 1, 0};
    Vect3D z = {0, 0, 1};
    Vect3D tmp1, tmp2, norm, t4;
    orbite = *_orbite;
    
    NSAssert(orbite.e < 1, @"can't deal with non elliptical orbit");
    
    b = orbite.a*sqrt(1-orbite.e*orbite.e);
    
    //compute the vector i0 and j0 that caracterize the orbit
    
    initVect(&tmp1, cos(orbite.N), sin(orbite.N), 0);
    prodVect(&tmp1, &z, &tmp2);
    
    mulScalVect(&z, cos(orbite.i), &norm);
    addLambdaVect(&norm, sin(orbite.i), &tmp2, &norm);
    
    prodVect(&norm, &tmp1, &t4);
    
    mulScalVect(&tmp1, cos(orbite.w), &i0);
    addLambdaVect(&i0, sin(orbite.w), &t4, &i0);
    
    prodVect(&norm, &i0, &j0);
    
    mulScalVect(&x, cos(orbite.inclinaison2), &tmp1);
    addLambdaVect(&tmp1, sin(orbite.inclinaison2), &y, &tmp1);
    mulScalVect(&z, cos(orbite.inclinaison1), &tmp2);
    addLambdaVect(&tmp2, sin(orbite.inclinaison1), &tmp1, &_nord);
    
    mulScalVect(&z, -sin(orbite.inclinaison1), &tmp2);
    addLambdaVect(&tmp2, cos(orbite.inclinaison1), &tmp1, &_haut);
    
    prodVect(&_nord, &_haut, &_droit);
}

- (ParametresOrbitaux *) parametresOrbitaux
{
    return &orbite;
}


- getAncestor
{
    if( pere != nil ) return [pere getAncestor];
    else return self;
}

- (BOOL) lightSource
{
    return NO;
}

- (void) addSon: (GGOrbiteur *)aSon
{
    [fils addObject: aSon];
    aSon->pere = self;
}

- (NSArray *)sons
{
    return fils;
}

- (GGOrbiteur *) orbiteurPere
{
    return pere;
}

- (GGOrbiteur *) visibleAncestor
{
    GGOrbiteur *orb = self;
    while(!orb->_flags.visible)
    {
        NSParameterAssert(self->nodePere == nil);
        orb = orb->pere;
        NSParameterAssert(orb != nil);
    }
    NSParameterAssert(orb->nodePere != nil);
    return orb;
}

- (BOOL) isOriginOfNode
{
    return repereTranslation != nil;
}

- closestFromRelative: (Vect3D *)pos
{
    int n = [fils count];
    if( n > 0 )
    {
        GGOrbiteur *filsTab[n];
        int i;
        [fils getObjects: filsTab];
        
        for( i = 0; i < n; ++i )
        {
            Vect3D *posFils;
            posFils = Point(filsTab[i]);
            
            if( distance2Vect(posFils, pos) <= square([filsTab[i] tailleSysteme]))
                return filsTab[i];
            //	  NSDebugMLLogWindow(@"GGOrbiteur", @"dist to %@  : %@", filsTab[i],
            //			     stringOfVect(posFils) );
        }
    }
    return self;
}

- getClosestFrom: (Vect3D *) pV distance: (GGReal *)ret
{
    Vect3D pSelf;
    GGReal val;
    if( _flags.visible == NO )
        return nil;
    pSelf = [self getPositionAbsolu];
    val = distance2Vect(pV, &pSelf);
    if( val >= SQ(tailleSysteme) )
    {
        return nil;
    }
    else
    {
        int n = [fils count], i;
        GGOrbiteur *closest = nil;
        GGOrbiteur *filsTab[n];
        GGReal min = 1e30;
        
        [fils getObjects: filsTab];
        
        for( i = 0; i < n; ++i )
        {
            GGReal tmp;
            GGOrbiteur *tmp2;
            if( (tmp2 = [filsTab[i] getClosestFrom: pV 
                                          distance: &tmp]) != nil && tmp < min )
            {
                closest = tmp2;
                min = tmp;
            }
        }
        if( closest != nil )
        {
            *ret = min;
            return closest;
        }
        else
        {
            *ret = sqrt(val);
            return self;
        }
    }
}

- (void) addUncles: (NSMutableArray *)tab
{
    [tab addObjectsFromArray: fils];
    if( pere != nil )
        [pere addUncles: tab];
    else
        [tab addObject: self];
}

- (NSArray *) visibleOrbiteursGlobal
{
    NSMutableArray *theResult;
    
    theResult = [NSMutableArray array];
    
    if( pere != nil )
        [pere addUncles: theResult];
    
    NSDebugMLLog(@"GGOrbiteur", @"visible globale = %@",
                 [theResult componentsJoinedByString: @", "]);
    [theResult removeObject: self];
    return theResult;
}

- (NSArray *) getAllOrbiteur
{
    NSMutableArray *array = [NSMutableArray array];
    [self addToArray: array];
    return array;
}

- (void) beginBeingSuperLocal
{  
    NSDebugMLLog(@"GGOrbiteur", 
                 @"on passe en coordonnees centrees sur %@", self);
    NSAssert(repereTranslation != nil, @"repereTranslation == nil");
    //  NSAssert(repereAbsolu != nil, @"repereAbsolu != nil");
    
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGOrbiteurBeingSuperLocal
                  object: self];
    
    [[theLoop slowScheduler] 
    deleteTarget: self
    withSelector: @selector(tryBeingSuperLocal)];
    [theLoop slowSchedule: self
             withSelector: @selector(tryBeingSemiLocal)
                      arg: nil];
    typeOrbiteur = GGLocalStrong;
    NSDebugMLLog(@"GGOrbiteur", @"matrice %@\n%@", self, 
                 stringOfMatrice(Matrice(self)));
}

- (void) tryBeingSuperLocal
{
    NSAssert1(typeOrbiteur != GGLocalStrong, 
              @"%@ is already local", [self name]);
    distanceCamera2 = distanceAbsolu2([manager camera], self);
    if( distanceCamera2 < distanceLocalOrbiteur )
        [self beginBeingSuperLocal];
    
}

- (void) endBeingSuperLocal
{
    NSDebugMLLog(@"GGOrbiteur",
                 @"on repasse en coordonnees normale", self);
    
    [[theLoop slowScheduler] 
    deleteTarget: self
    withSelector: @selector(tryBeingSemiLocal)];
    [theLoop slowSchedule: self
             withSelector: @selector(tryBeingSuperLocal)
                      arg: nil];
    typeOrbiteur = GGLocal;
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGOrbiteurEndBeingSuperLocal
                  object: self];
}

- (void) tryBeingSemiLocal
{
    NSAssert1(typeOrbiteur == GGLocalStrong, @"%@ is not local", [self name]);
    //  NSAssert(repereAbsolu != nil, @"repereAbsolu == nil");
    
    if( distanceAbsolu2([manager camera], self) > distanceGlobalOrbiteur )
        [self endBeingSuperLocal];
}

- (void) updateOrbite
{
    GGGalileen gal;
    Vect3D tmp;
    GGReal cosf, sinf, phi;
    
    gal.matrice.nul1 = gal.matrice.nul2 = gal.matrice.nul3 = 0;
    gal.matrice.unite = 1;
    
    if( pere != nil )
    {
        Vect3D relPos, relVit;
        GGReal l;
        
        l = orbite.M + gameTime*orbite.omegaOrbite;
        mulScalVect(&i0, orbite.a*(cos(l)-orbite.e), &relPos);
        addLambdaVect(&relPos, b*sin(l), &j0, &relPos);
        //       initVect(&relPos, orbite.orbiteRadius*cos(l),
        // 	       orbite.orbiteRadius*sin(l), 0);
		
        mulScalVect(&i0, -orbite.omegaOrbite*orbite.a*sin(l), &relVit);
        addLambdaVect(&relVit, orbite.omegaOrbite*b*cos(l), &j0, &relVit);
        
        //       initVect(&relVit, -psi*sin(l),
        // 	       psi*cos(l), 0);
        gal.matrice.position = relPos;
        gal.speed = relVit;
        //      addVect(&relPos, Point(pere), &gal.matrice.position);
        //      addVect(&relVit, Speed(pere), &gal.speed);
    }
    else
    {
        SetZeroVect(gal.matrice.position);
        SetZeroVect(gal.speed);
    }
    
    phi = gameTime*orbite.omegaPropre;
    cosf = cos(phi);
    sinf = sin(phi);
    mulScalVect(&_haut, cosf, &tmp);
    addLambdaVect(&tmp, sinf, &_droit, &(gal.matrice.gauche));
    mulScalVect(&_haut, -sinf, &tmp);
    addLambdaVect(&tmp, cosf, &_droit, &(gal.matrice.haut));
    gal.matrice.direction = _nord;
    
    if( repereTranslation != nil )
    {
        *Point(repereTranslation) = gal.matrice.position;
        *Speed(repereTranslation) = gal.speed;
        
        if( repereRotatif != nil )
        {
            *Matrice(repereRotatif) = gal.matrice;
            SetZeroVect(*Point(repereRotatif));
            SetZeroVect(*Speed(repereRotatif));
            initVect(Omega(repereRotatif), 0, 0, orbite.omegaPropre);
            //FIXME : just in case
            initWithIdentityTransfo(Galileen(self));
        }
        else
        {
            *Matrice(self) = gal.matrice;
            initVect(Omega(self), 0, 0, orbite.omegaPropre);
            SetZeroVect(*Point(self));
            SetZeroVect(*Speed(self));
        }	
    }
    else
    {
        *Galileen(self) = gal;
        initVect(Omega(self), 0, 0, orbite.omegaPropre);
    }
}

- (Vect3D) getPositionAbsolu
{
    if( _flags.visible )
        return [super getPositionAbsolu];
    else
        return [pere getPositionAbsolu];
}

- (Vect3D) getPositionSpeedAbsolues: (Vect3D *)pspeed
{
    if( _flags.visible )
        return [super getPositionSpeedAbsolues: pspeed];
    else
        return [pere getPositionSpeedAbsolues: pspeed];
}

- (void) galileanTransformationTo: (GGSpaceObject *)obj
                               in: (GGGalileen *)pgal
{
    if( _flags.visible )
        [super galileanTransformationTo: obj
                                     in: pgal];
    else
        [pere galileanTransformationTo: obj
                                    in: pgal];
}

- (void) positionSpeedRelativesTo: (GGSpaceObject *)obj
                            decal: (const Vect3D *)val
                               in: (PetitGalileen *)pgl
{
    if( _flags.visible )
        [super positionSpeedRelativesTo: obj
                                  decal: val
                                     in: pgl];
    else
    {
        NSDebugMLLogWindow(@"GGOrbiteur", @"invisible %@", self);
        [pere positionSpeedRelativesTo: obj
                                 decal: val
                                    in: pgl];
    }
}

- (void) cameraBeginBeingClose
{
}

- (void) cameraStopBeingClose
{
}


#define HARDEXACTCALCUL 2
#define EXACTCALCUL 1

- (void) thingsToDoWhenIdle
{     
    GGReal d2, davg;
    GGReal distance;
    
    distance = distanceAbsolu2([manager camera], self);
    d2 = distance*tolForExactCalcul;
    davg = prodScal(Speed(self), Speed(self))*deltaTime*deltaTime;
    //  NSDebugLLogWindow(@"GGOrbiteur", @"%@ %g %g", self, d2, davg);
    if( d2 < davg && !exactCalcul)
    {
        exactCalcul = YES;
        NSDebugMLLog(@"GGOrbiteur", @"commute to updatePosition for %@", self);
        [self setMode: GGInertialMode];
    }
    else if( d2 >= davg && exactCalcul )
    {
        exactCalcul = NO;
        NSDebugMLLog(@"GGOrbiteur", @"disband to updatePosition for %@", self);
        if( typeOrbiteur == GGStandard )
            [self setMode: 0];
    }
    
    if( distance < square(500*dimension) ) 
    {
        //      NSDebugMLLogWindow(@"GGOrbiteur", @"%@ is close", self);
        if ( ! proche)
        {
            proche = YES;
            /* Maybe notification are more apropriate...
            */
            [self cameraBeginBeingClose];
        }
    }
    else
    {
        //      NSDebugMLLogWindow(@"GGOrbiteur", @"%@ is not close", self);
        if( proche )
        {
            proche = NO;
            [self cameraStopBeingClose];
        }
    }
}	

- (void) miseAJourSysteme
{
    [self updateOrbite];
    [fils makeObjectsPerformSelector: @selector(miseAJourSysteme)];
}

- (void) getParametresOrbitaux: (ParametresOrbitaux *)pparam
{
    *pparam = orbite;
}

- (void) calculeTailleSysteme
{
    int n = [fils count];
    tailleSysteme = dimension;
    if( n > 0 )
    {
        GGOrbiteur *tab[n];
        int i;
        [fils getObjects: tab];
        for( i = 0; i < n; ++i )
        {
            GGReal tmp;
            ParametresOrbitaux param;
            [tab[i] calculeTailleSysteme];
            [tab[i] getParametresOrbitaux: &param];
            tmp = [tab[i] tailleSysteme];
            tmp += param.a;
            if( tmp > tailleSysteme )
                tailleSysteme = tmp;
        }
    }
    else
        tailleSysteme *= 2;
    tailleSysteme *= 10;
    //  NSDebugMLLog(@"GGOrbiteur", @"tailleSysteme = %e", tailleSysteme);
}

- (void) beginSchedule
{
    [super beginSchedule];
    [self setVisible: YES];
    [self updateOrbite];
    [theLoop slowSchedule: self
             withSelector: @selector(updateOrbite)
                      arg: nil];
    [theLoop slowSchedule: self
             withSelector: @selector(thingsToDoWhenIdle)
                      arg: nil];
}

- (void) endSchedule
{
    Sched *slow;
    
    [super endSchedule];
    [self setVisible: NO];
    
    slow = [theLoop slowScheduler];
    
    [slow deleteTarget: self 
          withSelector: @selector(updateOrbite)];
    [slow deleteTarget: self 
          withSelector: @selector(thingsToDoWhenIdle)];
    
    //just in case
    [slow deleteTarget: self 
          withSelector: @selector(tryBeingSemiLocal)];
    [slow deleteTarget: self 
          withSelector: @selector(tryBeingSuperLocal)];
    [[theLoop fastScheduler] deleteTarget: self];
}

- (void) startBeingOrigineOfLocale: (GGNode *)objjj
{
    //  NSAssert1(typeOrbiteur == GGStandard, 
    //	    @"%@ is not GGStandard", self);
    assert(typeOrbiteur == GGStandard);
    NSAssert2(typeOrbiteur == GGStandard, @"typeOrbiteur != GGStandard for %@ val is %d", 
              self, typeOrbiteur);
    NSAssert4(repereRotatif == nil && 
              (repereTranslation == nil ||
               repereTranslation == [self nodePere]),
              @"%@\n%@\n%@\n%@", repereRotatif, repereTranslation, self, [self nodePere]); 
    typeOrbiteur = GGLocal;
    repereTranslation = [self nodePere];
    [fils makeObjectsPerformSelector: @selector(startBeingLocal)];
    
    [self setMode: GGInertialMode | GGInertialRotationalMode];
    [self startBeingMainLocale];
}

- (void) startBeingMainLocale
{
    if( manager == theWorld )
        [theLoop slowSchedule: self
                 withSelector: @selector(tryBeingSuperLocal)
                          arg: nil];
}

- (void) stopBeingMainLocale
{
    if( typeOrbiteur == GGLocalStrong )
        [self endBeingSuperLocal];
    [fils makeObjectsPerformSelector: @selector(stopBeingLocal)];
    [[theLoop slowScheduler] deleteTarget: self 
                             withSelector: @selector(tryBeingSuperLocal)];
    //just in case, we remove this one too :
    [[theLoop slowScheduler] deleteTarget: self 
                             withSelector: @selector(tryBeingSemiLocal)];
    
}

- (void) stopBeingOrigineOfLocale
{
    [self stopBeingMainLocale];
    [self setMode: 0];
    typeOrbiteur = GGStandard;
    repereTranslation = nil;
}

- (void) startBeingLocal
{
}

- (void) stopBeingLocal
{
}

- (double) anomalie
{
    return orbite.M + gameTime*orbite.omegaOrbite;
}

- (System *) surroundingSystem
{
    if( surroundingSystem == nil )
    {
        NSParameterAssert(pere != nil);
        return [pere surroundingSystem];
    }
    else
        return surroundingSystem;
}

- (GGNode *) subSystemFrame
{
    NSAssert1(repereTranslation != nil, @"repereTranslation is nil for %@",
              self);
    return repereTranslation;
}

@end

@implementation GGOrbiteur (PrivateMethod)
- (void) addToArray: (NSMutableArray *)array
{
    [array addObject: self];
    [fils makeObjectsPerformSelector: @selector(addToArray:)
                          withObject: array];
}

- signature
{
    return [NSString stringWithFormat: @"orb:%d:%@",
        [[self surroundingSystem] index],
        self];
}

void debugOrbiteurWindow(NSString *name)
{
    GGOrbiteur *orb = [theWorld orbiteurByName: name];
    if (orb )
        NSDebugLLogWindow(@"GGOrbiteur2", @"%@ typeCoord : %d pos : %@ vir : %@",
                          orb, orb->typeOrbiteur,
                          stringOfVect(Point(orb)), stringOfVect(Speed(orb)));
    
}
@end

