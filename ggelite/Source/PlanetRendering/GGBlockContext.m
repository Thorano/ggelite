//
//  GGBlockContext.m
//  elite
//
//  Created by Frédéric De Jaeger on 27/12/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GGBlockContext.h"
#import "GGBlockCollision.h"
#import "GGSpaceObject.h"
#import "GGRepere.h"
#import "utile.h"
#import "GGLog.h"
#import "GGBlock.h"

static void rasterizeArrayAndClean(NSMutableArray *array)
{
    int n;
    n = [array count];
    NSDebugLLogWindow(@"Scene", @"nblock = %d", n);
    if( n > 0)
    {
        GGBlock *blocks[n];
        int i;
        
        [array getObjects: blocks];
        for(i = 0; i < n; ++i)
            secondPassBis(blocks[i]);
    }
    [array removeAllObjects];
}

@interface GGBlockContext (Private)
#ifdef DEBUG
- (void)debugBefore;
- (void)debugAfter;
- (void) debugAfterGlobal;
- (void) debugAfterLocal;
- (void) _moveCurrentTo:(GGBlock*)newCurrent;
#endif
@end


@implementation GGBlockContext
- (id) init
{
    self = [super init];
    if(self){
        forLocal = [NSMutableArray new];
        localStudy = [NSMutableArray new];
        forGlobal = [NSMutableArray new];
        sizeSmallBlock = 2000;  // we don't split block smaller than 500
        _boudingBoxLocal.min = GGZeroVect;
        _boudingBoxLocal.max = GGZeroVect;
    }
    return self;
}

- (void) dealloc {
    [_topLevelBlocks release];
    [forLocal release];
    [localStudy release];
    [forGlobal release];
    [solid release];
    [texture release];

#ifdef DEBUG        
    [_pathToDebugBlock release];
#endif
    [super dealloc];
}


- (Texture *)texture {
    return texture;
}

- (void)setTexture:(Texture *)value {
    if (texture != value) {
        [texture release];
        texture = [value retain];
    }
}

- (float)sizeSmallBlock {
    return sizeSmallBlock;
}

- (void)setSizeSmallBlock:(float)value {
    if (sizeSmallBlock != value) {
        sizeSmallBlock = value;
    }
}



- (GGSolid*) createDynamicalHelper
{
    if([self solid]==nil){
        GGSolidPlanet* planete = [[GGSolidPlanet alloc] initWithContext:self];
        [self setSolid:planete];
        [planete release];
    }
    
    return [self solid];
}

- (GGSolid *)solid {
    return solid;
}

- (void)setSolid:(GGSolid *)value {
    if (solid != value) {
        [solid release];
        solid = [value retain];
    }
}

- (void) setTopLevelBlocks:(NSArray*)blocks
{
    if(_topLevelBlocks != blocks){
        [_topLevelBlocks release];
        _topLevelBlocks = [blocks copy];
    }
}

- (NSArray*) topLevelBlocks
{
    return _topLevelBlocks;
}

- (void) rasterizeLocalsAndClean
{
    rasterizeArrayAndClean(forLocal);
}

- (void) rasterizeGlobalsAndClean
{
    rasterizeArrayAndClean(forGlobal);
}

#ifdef DEBUG
- (void) checkBlocks
{
    [[self topLevelBlocks] makeObjectsPerformSelector:@selector(checkSons)];
}
#endif

- (void) rasteriseGlobalPartWithCamera:(Camera *)cam
                            globalPart:(GGSpaceObject*) global
{
    //compute the position of the camera in object coordinates;
    
    
    makeFrustum(&frustum, cam);
    
    transposeTransformePoint4D(&cam->modelView, &GGZeroVect, &posObs);
    nFrame ++;
    
    NSDebugMLLogWindow(@"GGBlock", @"nFrame = %d pos : %@\n%@\n%@",
                       nFrame, stringOfVect(&cam->modelView.position),
                       stringOfVect(&posObs), stringOfVect(Point(global)));
    
#ifdef DEBUG
    [self debugBefore];
#endif
    [GGBlock initGLContextBeforeDrawing: [self texture]];
    
    [localStudy removeAllObjects];
    
    [[self topLevelBlocks] makeObjectsPerformSelector:@selector(rasterize)];
    
#ifdef DEBUG
    [self checkBlocks];
#endif
    
    [self rasterizeGlobalsAndClean];
    [GGBlock finish];
#ifdef DEBUG
    [self debugAfterGlobal];
//    if( ! lb)
//        [GGBlock debugAfter];
#endif
    //  rasterizeArrayAndClean(forLocal);
}

- (void) rasteriseLocalPart:(GGSpaceObject*) local
                     camera:(Camera *)cam
{
    /*Here is what's happen there.  The method rasteriseBlocks just pre computes visibility stuff
    according to the current frustum.  But the frustum is not the same depending on the 
    locality of the block.  Thus, we defer the computation of the local blocks and do that
    with the correct frustum.  That is just horrible.
    
    The question is.  Could we just handle localStudy in the object that lives in the local frame?
    It depends on whether the first pass for local blocks needs to be done before the secondpass for global blocks start.  I have the feeling that we don't need the information computed during the first pass for local blocks when we rasterize global blocks.  That depends on the heuristic to decide when a blocks is local.  That will work provided we keep the property:
    
    All the cousins of a global block are also global.
    
    */
    GGSpaceObject *obj = [local nodePere];
    posLocalFrame = *Point(obj);

    NSDebugMLLogWindow(@"GGBlock", @"local %d\n%@", [localStudy count], stringOfMatrice(Matrice(local)));
    
    // We need the position of the camera in the local frame
    // So we look for the transformation matrix that has been already
    // computed.
    transposeTransformePoint4D(&cam->modelView, 
                               &GGZeroVect, 
                               &posObs);
    makeFrustum(&frustum, cam);
    _boudingBoxLocal.min = GGZeroVect;
    _boudingBoxLocal.max = GGZeroVect;
    if(localStudy && [localStudy count] > 0){
        GGBlock* block = (GGBlock*)[localStudy objectAtIndex:0];
        _boudingBoxLocal = [block boudingBox];
        int n = [localStudy count];
        for(int i = 1; i < n; ++i){
            block = (GGBlock*)[localStudy objectAtIndex:i];
            GGBox tmp = [block boudingBox];
            _boudingBoxLocal = unionBox(&_boudingBoxLocal, &tmp);
        }
    }

    [localStudy makeObjectsPerformSelector:@selector(rasterize)];

//    glColor3fv((GGReal*) &col);
    [GGBlock initGLContextBeforeDrawing: [self texture]];
    
    [self rasterizeLocalsAndClean];
    
    [GGBlock finish];
#ifdef DEBUG
    [self debugAfterLocal];
    [self debugAfter];
#endif
}

- (GGBox) boudingBoxForLocalBlocks
{
    return _boudingBoxLocal;
}

#ifdef DEBUG

- (void) displayBorder
{
    [GGBlock displayBorder];
}

- goUp
{
    if(_currentBlock){
        [self _moveCurrentTo:[_currentBlock topNeighBor]];
    }
    return nil;
}

- goDown
{
    if(_currentBlock){
        [self _moveCurrentTo:[_currentBlock bottomNeighBor]];
    }
    return nil;
}

- goLeft
{ 
    if(_currentBlock){
        [self _moveCurrentTo:[_currentBlock leftNeighBor]];
    }
    return nil;
}

- goRight
{ 
    if(_currentBlock){
        [self _moveCurrentTo:[_currentBlock rightNeighBor]];
    }
    return nil;
}

- goLevelD
{
    _currentDepth++;
    return nil;
}

- goLevelU
{  
    if(_currentDepth > 0){
        _currentDepth--;
    }
    return nil;
}

- lookForClosest
{
    _lookForClosest = YES;
    return nil;
}

#endif
@end

@implementation GGBlockContext (Private)
#if DEBUG
- (void) computePath
{
    _fatherCurrent = nil;
    _currentBlock = nil;
    if(_pathToDebugBlock && [_pathToDebugBlock count] > 0){
        NSNumber* topIndexNumber = [_pathToDebugBlock objectAtIndex:0];
        int topIndex = [topIndexNumber intValue];
        NSAssert(topIndex >= 0 && topIndex < [_topLevelBlocks count], @"bad first index");
        _fatherCurrent = [_topLevelBlocks objectAtIndex:topIndex];
        
        NSAssert(([_pathToDebugBlock count] & 0x1) != 0, @"must be odd");
        int count = [_pathToDebugBlock count];
        int i;
        int icur = -1;
        int jcur = -1;
        GGBlock* nextBlock = nil;
        for(i = 1; i < count; i+=2){
            icur = [[_pathToDebugBlock objectAtIndex:i] intValue];
            jcur = [[_pathToDebugBlock objectAtIndex:i+1] intValue];
            NSAssert(icur >= 0 && icur < BRANCHEMENT, @"bad i");
            NSAssert(jcur >= 0 && jcur < BRANCHEMENT, @"bad j");
            nextBlock = GGBlockGetSubBlock(_fatherCurrent, icur, jcur);
            if(nextBlock && (i>>1)<_currentDepth){
                _fatherCurrent = nextBlock;
            }
            else {
                break;
            }
        }
        _currentBlock = nextBlock;  // can be nil.
        
        if(i < count){
            // in that case, we are sure that icur and jcur are set
            NSAssert(icur >= 0, @"impossible");
            NSAssert(jcur >= 0, @"impossible");
            _icur = icur;
            _jcur = jcur;
        }
        else {
            _icur = 0;
            _jcur = 0;
        }
    }
    else if([_topLevelBlocks count] > 0){
        _fatherCurrent = [_topLevelBlocks objectAtIndex:0];
        _icur = 0;
        _jcur = 0;
    }
    
    
#if 0
    // old code to migrate
    if( icur < 0 )
    {
        if( fatherCurrent->gauche != nil )
        {
            fatherCurrent = fatherCurrent->gauche;
            icur += BRANCHEMENT;
        }
        else
            icur = 0;
    }
    
    if( icur >= BRANCHEMENT )
    {
        if( fatherCurrent->droite != nil )
        {
            fatherCurrent = fatherCurrent->droite;
            icur -= BRANCHEMENT;
        }
        else
            icur = BRANCHEMENT-1;
    }
    
    if( jcur < 0 )
    {
        if( fatherCurrent->bas != nil )
        {
            fatherCurrent = fatherCurrent->bas;
            jcur += BRANCHEMENT;
        }
        else
            jcur = 0;
    }
    
    if( jcur >= BRANCHEMENT )
    {
        if( fatherCurrent->haut != nil )
        {
            fatherCurrent = fatherCurrent->haut;
            jcur -= BRANCHEMENT;
        }
        else
            jcur = BRANCHEMENT-1;
    }
    
    currentBlock = fatherCurrent->fils[icur][jcur].block;
    NSDebugMLLogWindow(@"BlCur", @"%@ %d %d %@", fatherCurrent,
                       icur, jcur, currentBlock);
#endif
}

// guaranted to be called before the main rendering process, either local or not.
- (void)debugBefore
{
    [GGBlock resetStat];
    if( _lookForClosest )
    {
        _distClosest = 1e100;
        _closest = nil;
    }
}

- (void) debugAfterGlobal
{
    if( wizard )
    {
        [self computePath];
        if( _currentBlock != nil && ![_currentBlock isLocal] )
            [_currentBlock displayNormal];  
        if( _fatherCurrent != nil && ![_fatherCurrent isLocal] )
            [_fatherCurrent drawSonBoxAt:_icur and:_jcur];
    }
}

- (void) debugAfterLocal
{
    if( wizard )
    {
        [self computePath];
        if( _currentBlock != nil && [_currentBlock isLocal] ){
            [_currentBlock displayNormal];  
//            [_currentBlock->_blockCollision drawDebug];
        }
        if( _fatherCurrent != nil && [_fatherCurrent isLocal] ){
            [_fatherCurrent drawSonBoxAt:_icur and:_jcur];
//            [_fatherCurrent->_blockCollision drawDebug];
        }
    }
}

- (void)debugAfter
{
    [self computePath];
    if( _lookForClosest )
    {
        _lookForClosest = NO;
        if( _closest != nil )
        {
            [self _moveCurrentTo:_closest];
            int size = [_pathToDebugBlock count];
            _currentDepth = (size - 1)/2;
        }
    }
    [_currentBlock printInfo];
    [GGBlock logStats];
}

- (void) _moveCurrentTo:(GGBlock*)newCurrent
{
    if(newCurrent){
        [_pathToDebugBlock release];
        _pathToDebugBlock = [newCurrent _createAncestorPath];
    }
}


#endif
@end
