//
//  GGNSEvent.m
//  elite
//
//  Created by Frederic De Jaeger on 17/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGEvent.h"
#import "GGOpenGLView.h"
#import "utile.h"
#import "GGMacJoystick.h"

void ggMakeEventFromCocoEvent(NSEvent* event, Event *pResult)
{
    bzero(pResult,sizeof(Event));
    pResult->originalEvent = event;
    pResult->joyElement = NULL;
    
}

#ifndef USE_SDL

const int GG_BUTTON_LEFT = 0;
const int GG_BUTTON_MIDDLE = 2;
const int GG_BUTTON_RIGHT = 1;
const int GG_BUTTON_WHEELUP = 4;
const int GG_BUTTON_WHEELDOWN = 5;

const int GG_SHIFT_MASK = NSShiftKeyMask;
const int GG_CONTROL_MASK = NSControlKeyMask;
const int GG_COMMAND_MASK = NSCommandKeyMask;
const int GG_ALT_MASK = NSAlternateKeyMask;

NSString* stringKeyForEvent(Event *_pev, BOOL* basicTextPtr)
{
    NSEvent* pev = _pev->originalEvent;
    BOOL basicText = NO;
    NSString* mod = @"";
    NSString* key = nil;
    
    unsigned int modifier = [pev modifierFlags];
    
    if( modifier & NSShiftKeyMask ){
        mod = @"S-";
    }
    if( modifier & NSControlKeyMask ){
        mod = [@"C-" stringByAppendingString:mod];
    }
    if( modifier & NSAlternateKeyMask ){
        mod = [@"M-" stringByAppendingString:mod];
    }
    
    NSString* characters = [pev charactersIgnoringModifiers];
    
    if([characters length] > 0){
        unichar aChar = [characters characterAtIndex:0];
        
        switch(aChar){
            // only match non-ascii keys
            
            case '\n':
                key = @"RET";
                break;
            case ' ':
                key = @"space";
                break;
            case 27:
                key = @"ESC";
                break;
            case '\t':
                key = @"TAB";
                break;
            case NSF1FunctionKey:
                key = @"F1";
                break;
            case NSF2FunctionKey:
                key = @"F2";
                break;
            case NSF3FunctionKey:
                key = @"F3";
                break;
            case NSF4FunctionKey:
                key = @"F4";
                break;
            case NSF5FunctionKey:
                key = @"F5";
                break;
            case NSF6FunctionKey:
                key = @"F6";
                break;
            case NSF7FunctionKey:
                key = @"F7";
                break;
            case NSF8FunctionKey:
                key = @"F8";
                break;
            case NSF9FunctionKey:
                key = @"F9";
                break;
            case NSF10FunctionKey:
                key = @"F10";
                break;
            case NSF11FunctionKey:
                key = @"F11";
                break;
            case NSF12FunctionKey:
                key = @"F12";
                break;
            case NSF13FunctionKey:
                key = @"F13";
                break;
            case NSLeftArrowFunctionKey:
                key = @"left";
                break;
            case NSRightArrowFunctionKey:    
                key = @"right";
                break;
            case NSDownArrowFunctionKey:
                key = @"down";
                break;
            case NSUpArrowFunctionKey:
                key = @"up";
                break;
            case NSPageUpFunctionKey:
                key = @"pageup";
                break;
            case NSPageDownFunctionKey:
                key = @"pagedown";
                break;
            case NSHomeFunctionKey:
                key = @"home";
                break;
            case NSEndFunctionKey:
                key = @"end";
                break;
            default:
                if( (aChar & 0xFF80) == 0){
                    key = [characters lowercaseString];
                    basicText = YES;
                }
                else{
                    NSLog(@"this should not happen");
                    ggabort();
                }
                break;
        }
    }
    else{
        NSLog(@"no chars in event");
        ggabort();
    }
    
    NSString* ret = [mod stringByAppendingString:key];
    NSDebugFLLog(@"Event", @"Event is %@", ret);
    
    if(basicTextPtr){
        *basicTextPtr = basicText;
    }
    
    return ret;
}

int buttonOfEvent(Event *_pev)
{
    if(_pev->joyElement){
        return GGJoystickButton(_pev);
    }
    NSEvent* pev = _pev->originalEvent;
//    NSLog(@"[pev buttonNumber] = %d", [pev buttonNumber]);
    return [pev buttonNumber];
}

NSPoint mouseLocationOfEvent(Event *_pev)
{
    return NSMakePoint(_pev->x, _pev->y);
}

NSPoint mouseMotionOfEvent(Event *_pev)
{
    NSEvent* pev = _pev->originalEvent;
    
    return NSMakePoint([pev deltaX], [pev deltaY]);
}

float deltaXOfEvent(Event *_pev)
{
    NSEvent* pev = _pev->originalEvent;
    return [pev deltaX];
}

float deltaYOfEvent(Event *_pev)
{
    NSEvent* pev = _pev->originalEvent;
    return [pev deltaY];
}

float deltaZOfEvent(Event *_pev)
{
    NSEvent* pev = _pev->originalEvent;
    return [pev deltaZ];
}

BOOL isADragMove(Event *_pev)
{
    NSEvent* pev = _pev->originalEvent;
    switch([pev type]){
        case NSLeftMouseDragged:
        case NSRightMouseDragged:
        case NSOtherMouseDragged:
            return YES;
        default:
            return NO;
    }
}

GGEventType typeOfEvent(Event *_pev)
{
    if(_pev->joyElement){
        GGEventType val =  eventTypeForElement(_pev);
        NSLog(@"val = %d", val);
        return val;
    }
    
    NSEvent* pev = _pev->originalEvent;
    switch([pev type]){
        case NSLeftMouseDown:
        case NSRightMouseDown:
        case NSOtherMouseDown:
            return GG_MOUSEBUTTONDOWN;

        case NSLeftMouseUp:
        case NSRightMouseUp:
        case NSOtherMouseUp:
            return GG_MOUSEBUTTONUP;

        case NSMouseMoved:
        case NSLeftMouseDragged:
        case NSRightMouseDragged:
        case NSOtherMouseDragged:
            return GG_MOUSEMOTION;
            
        case NSScrollWheel:
            return GG_SCROLLWHEEL;

        case NSKeyDown:
            return GG_KEYDOWN;
        case NSKeyUp:
            return GG_KEYUP;
        default:
            return GG_OTHER_EVENT;
    }
}

unsigned modifiersOfEvent(Event *_pev)
{
    NSEvent* pev = _pev->originalEvent;
    return [pev modifierFlags];
}
#endif