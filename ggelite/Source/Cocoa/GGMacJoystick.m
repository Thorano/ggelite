//
//  GGMacJoystick.m
//  elite
//
//  Created by Frédéric De Jaeger on 7/11/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGMacJoystick.h"

#import <IOKit/hid/IOHIDUsageTables.h>

#import "GGOpenGLView.h"
#import "GGMainLoop.h"

static GGMacJoystick* macJoy;


@implementation GGMacJoystick
static pRecElement HIDLookupElementWithCookie(pRecDevice pSearchDevice, void *cookie)
{
    pRecElement element = HIDGetFirstDeviceElement(pSearchDevice,kHIDElementTypeInput);
    while(element){
        if(element->cookie == cookie){
            return element;
        }
        element = HIDGetNextDeviceElement(element,kHIDElementTypeInput);
    }
    
    return NULL;
}

- (void) gotEvent:(pRecDevice)device
           result:(IOReturn) result
{
    NSLog(@"was called with %d", result);
    IOHIDEventStruct   event;
    while(HIDGetEvent(device,(void*)&event)){
        pRecElement element = HIDLookupElementWithCookie(device, event.elementCookie);
        char usageName[255];
        HIDGetUsageName(element->usagePage, element->usage, usageName);
        NSLog(@"element name = %s (0x%x, 0x%x)", usageName, element->usagePage, element->usage);
        NSAssert(element, @"cannot get element");
        long val = HIDGetElementValue(device,element);
        Event ev;
        bzero(&ev,sizeof(ev));
        ev.joyElement = element;
        ev.joyValue = val;
        NSLog(@"got an event: %d", val);      
        [_delegate handleJoystickEvent:&ev];
   }
}

static void  myCallBack(void * target, IOReturn result, void * refcon, void * sender)
{
    pRecDevice device = target;
    [macJoy gotEvent:device
              result:result];
}

- (id) init
{
    self = [super init];
    if(self){
        NSAssert(nil == macJoy, @"can only instantiate just one guy");
        macJoy = self;
        NSLog(@"building device List");
        HIDBuildDeviceList(kHIDPage_GenericDesktop, kHIDUsage_GD_Joystick);
        NSLog(@"there are %d devices", (int)HIDCountDevices());
        _device = HIDGetFirstDevice ();
        if(_device){
            NSLog(@"name = %s", _device->product);
            NSLog(@"nbraxe = %d\nin=%d\nout=%d", _device->axis, HIDCountDeviceElements(_device, kHIDElementTypeInput), HIDCountDeviceElements(_device, kHIDElementTypeOutput));   
            
            pRecElement element = HIDGetFirstDeviceElement(_device,kHIDElementTypeInput);
            BOOL hasElement = NO;
            while(element!=NULL){
                char usageName[255];
                HIDGetUsageName(element->usagePage, element->usage, usageName);
                NSLog(@"element name = %s (0x%x, 0x%x)", usageName, element->usagePage, element->usage);
                BOOL monitor = NO;
                switch(element->usagePage){
                    case kHIDPage_GenericDesktop:
                        monitor = YES;
//                        switch(element->usage){
//                            case kHIDUsage_GD_X:
//                            case kHIDUsage_GD_Y:
////                            case kHIDUsage_GD_Z:
//                                monitor = YES;
//                                break;
//                        }
                        break;
                    case kHIDPage_Button:
                        monitor = YES;
                        break;
                    case kHIDPage_Simulation:
                        monitor = YES;
                        break;
                }
                if(monitor){
                    NSLog(@"monitoring it");
                    BOOL tmp = hasElement;
                    hasElement = YES;
                    IOReturn code = HIDQueueElement(_device,element);
                    NSAssert(code == kIOReturnSuccess, @"bad queue");
                    if(code != kIOReturnSuccess){
                        NSLog(@"could not queue: %d", code);
                        hasElement = tmp;
                    }
                } 
                element = HIDGetNextDeviceElement(element,kHIDElementTypeInput);
            }
            if(hasElement){
                NSLog(@"set callback");
                HIDSetQueueCallback(_device, myCallBack);
            }
            else{
                NSLog(@"could not find it");
//                HIDQueueDevice(_device);
//                HIDSetQueueCallback(_device, myCallBack);
            }
        }
        else{
            NSLog(@"no device");
        }
    }
    
    return self;
}

- (void) releaseJoystick
{
    NSLog(@"releasing the device list");
    HIDReleaseDeviceList();
    macJoy = nil;
}

- (void) setDelegate:(id<GGMacJoystickDelegate>)aDelegate
{
    _delegate = aDelegate;
}

@end

int axeOfEvent(Event* pev)
{
    pRecElement element = pev->joyElement;
    NSCAssert(element != NULL, @"not a joystick event");
    switch(element->usagePage){
        case kHIDPage_GenericDesktop:
            switch(element->usage){
                case kHIDUsage_GD_X:
                    return 0;
                case kHIDUsage_GD_Y:
                    return 1;
                case kHIDUsage_GD_Z:
                    return 5;
            }
            break;
        case kHIDPage_Button:
            return -1;
        case kHIDPage_Simulation:
            switch(element->usage){
                case kHIDUsage_Sim_Rudder:
                    return 2;
                case kHIDUsage_Sim_Throttle:
                    return 3;
            }
    }
    return -1;
}

double  normalizedValue(Event* pev)
{
    pRecElement element = pev->joyElement;
    NSCAssert(element != NULL, @"not a joystick event");
    
    double calValue = HIDCalibrateValue(pev->joyValue,element);
    
    NSCAssert(element->max - element->min > 0, @"degenerated element");
    return calValue / (element->max - element->min);
}

int GGJoystickButton(Event* pev)
{
    pRecElement element = pev->joyElement;
    NSCAssert(element != NULL, @"not a joystick event");
    return element->usage;
}

GGEventType eventTypeForElement(Event* pev)
{
    pRecElement element = pev->joyElement;
    switch(element->usagePage){
        case kHIDPage_GenericDesktop:
            switch(element->usage){
                case kHIDUsage_GD_X:
                case kHIDUsage_GD_Y:
                case kHIDUsage_GD_Z:
                    return GG_JOYSTICKAXIS;
            }
            break;
        case kHIDPage_Button:
            return pev->joyValue ? GG_MOUSEBUTTONDOWN : GG_MOUSEBUTTONUP;
        case kHIDPage_Simulation:
            return GG_JOYSTICKAXIS;
    }
    
    NSWarnLog(@"unknown event %d %d!!", element->usagePage, element->usage);
    return GG_OTHER_EVENT;
    
}