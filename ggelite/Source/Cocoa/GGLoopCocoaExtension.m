//
//  GGLoopCocoaExtension.m
//  elite
//
//  Created by Frederic De Jaeger on 27/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "GGLoopCocoaExtension.h"
#import "Resource.h"
#import "GGLoop.h"
#import "GGOpenGLView.h"
#import "GGEventDispatcher.h"
#import "World.h"
#import "GGInput.h"
#import "GGMacJoystick.h"

#ifdef DEBUG
#import "Planete.h"
#import "GGDebugController.h"
#endif

#define FUNKY_EXP 0
#if FUNKY_EXP
#import "GGExperimentalWindow.h"
#else
#import "GGEliteController.h"
#endif

@implementation GGEliteCocoaBridge
- (void) awakeFromNib
{
    _previousPosition = NSMakePoint(0,0);
    _joystick = [[GGMacJoystick alloc] init];
    [_joystick setDelegate:self];
}

#pragma mark NSApp delegate method
- (void)applicationWillFinishLaunching:(NSNotification *)aNotification
{
    [_mainView clearView];
#ifdef DEBUG
    GGDebugController* controller = [GGDebugController  controller];
    [controller showWindow:self];
#endif
    _timer = [[NSTimer timerWithTimeInterval:1.0/20.0
                                      target:self
                                    selector:@selector(advanceTime:)
                                    userInfo:nil
                                     repeats:YES] retain];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:(id)kCFRunLoopCommonModes];

//    NSLog(@"gl context = %@", [NSOpenGLContext currentContext]);
    [theLoop beforeRunningLoop];
#if FUNKY_EXP
    _mainController = [[GGExperimentalWindow alloc] init];
    [_mainController open]; 
#else   
    _mainController = [[GGEliteController alloc] init];
    [_mainController startup];
#endif
    [theLoop runOnce];
}

- (BOOL)application:(NSApplication *)theApplication
           openFile:(NSString *)filename
{
    NSString* extension = [filename pathExtension];
    if(theWorld && [extension isEqual:@"gge"]){
        [theWorld loadGame:filename];
        return YES;
    }
#ifdef DEBUG
    if([extension isEqual:@"bld"] || [extension isEqual:@"gg3d"]){
        NSLog(@"will open %@", filename);
        [theGGInput loadDynamicScneneAtPath:filename];
        return YES;
    }
#endif
    else{
        return NO;
    }
}

#pragma mark -

- (void) advanceTime:(NSTimer*)timer
{
    [theLoop runOnce];
    
    NSAssert(_mainView, @"no main view");
    [_mainView setNeedsDisplay:YES];
    //    [self drawRect:[self bounds]];
}

- (void) displayGL
{
    [theLoop displayGL];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    [_mainController release];
    _mainController = nil;
    [Resource destroyResourceManager];
    [_joystick releaseJoystick];
    [_joystick release];
    _joystick = nil;
}

#pragma mark event dispatch
- (BOOL) _commonEventDispatch:(Event*)event
{
    NSPoint mouseLocation = [_window mouseLocationOutsideOfEventStream];
    mouseLocation = [_mainView convertPoint:mouseLocation fromView:nil];
    event->x = mouseLocation.x;
    event->y = mouseLocation.y;
    event->deltax = event->x - _previousPosition.x;
    event->deltay = event->y - _previousPosition.y;
    _previousPosition = mouseLocation;
    
    return [theLoop dispatchEvent:event];
}

- (void) handleJoystickEvent:(Event *)pEvent
{
    NSLog(@"receive event through delegation");
    [self _commonEventDispatch:pEvent];
}

- (BOOL) tryDispatchCocoaEvent:(NSEvent*)event
{
    Event ggevent;
    ggMakeEventFromCocoEvent(event, &ggevent);
    return [self _commonEventDispatch:&ggevent];
}

#pragma mark IBActions
- (IBAction) save:(id)sender
{
    NSSavePanel* panel = [NSSavePanel savePanel];
    [panel setCanCreateDirectories: YES];
    [panel setAllowedFileTypes:[NSArray arrayWithObject:@"gge"]];
    [panel setAllowsOtherFileTypes: NO];
    int runResult = [panel runModal];
    if ( runResult  == NSOKButton){
        NSString* fileName = [panel filename];
        NSParameterAssert(fileName);
        NSLog(@"saving the file:%@", fileName);
        [theWorld saveGame: fileName];
    }
}

- (IBAction) load:(id)sender
{
    NSOpenPanel* panel = [NSOpenPanel openPanel];
    [panel setAllowsMultipleSelection:NO];
    [panel setCanChooseDirectories:NO];
    
    if ([panel runModalForTypes: [NSArray arrayWithObject:@"gge"]] == NSOKButton ){
        NSArray* files = [panel filenames];
        if ( [files count] >= 1 ){
            NSString* filePath = [files objectAtIndex:0];
            NSLog(@"will load %@", filePath);
            [theWorld loadGame: filePath];
        }
    }
}


@end
