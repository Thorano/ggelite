//
//  GGLoopCocoaExtension.h
//  elite
//
//  Created by Frederic De Jaeger on 27/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGLoop.h"
#import "GGMacJoystick.h"

@class NSTimer;
@class GGOpenGLView;

@class GGMacJoystick;
@interface GGEliteCocoaBridge : NSObject <GGMacJoystickDelegate>{
    IBOutlet  NSWindow*         _window;
    IBOutlet  GGOpenGLView*     _mainView;
    
    NSPoint                     _previousPosition;
    GGMacJoystick*              _joystick;
    NSTimer*                    _timer;
	
	id							_mainController;
}
- (BOOL) tryDispatchCocoaEvent:(NSEvent*)event;

- (void) displayGL;
- (IBAction) save:(id)sender;
- (IBAction) load:(id)sender;
@end

