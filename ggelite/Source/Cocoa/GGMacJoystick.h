//
//  GGMacJoystick.h
//  elite
//
//  Created by Frédéric De Jaeger on 7/11/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "HID_Utilities_External.h"
#import "GGEvent.h"

@protocol GGMacJoystickDelegate 
- (void) handleJoystickEvent:(struct __Event *)pEvent;
@end
@interface GGMacJoystick : NSObject {
    pRecDevice                  _device;
    id<GGMacJoystickDelegate>   _delegate;
}
- (void) releaseJoystick;

- (void) setDelegate:(id<GGMacJoystickDelegate>)aDelegate;
@end

GGEventType eventTypeForElement(Event* pev);
int GGJoystickButton(Event* pev);