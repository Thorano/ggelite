//
//  GGOpenGLView.m
//  elite
//
//  Created by Frederic De Jaeger on 28/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "GGLoopCocoaExtension.h"
#import "Resource.h"
#import "GGLoop.h"
#import "GGOpenGLView.h"


@implementation GGOpenGLView
#pragma mark -
#pragma mark init and such
+ (NSOpenGLPixelFormat*) basicPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,	// double buffered
        NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)16, // 16 bit depth buffer
        (NSOpenGLPixelFormatAttribute)nil
    };
    NSOpenGLPixelFormat* ret =  [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
    NSParameterAssert(ret);
    NSDebugMLLog(@"GL", @"pixel format: %@", ret);
    
    return ret;
}

-(id) initWithFrame: (NSRect) frameRect
{
	NSOpenGLPixelFormat * pf = [GGOpenGLView basicPixelFormat];
    NSDebugMLLog(@"GL", @"init with %@", NSStringFromRect(frameRect));
    
	self = [super initWithFrame: frameRect pixelFormat: pf];
    return self;
}

- (void) prepareOpenGL
{
    GLint swapInt = 1;
    
    [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval]; // set to vbl sync
}


- (void) resizeGL
{
	NSRect rectView = [self bounds];
	
	// ensure camera knows size changed
	if ((HEIGHT != rectView.size.height) ||
	    (WIDTH != rectView.size.width)) {
        NSDebugMLLog(@"GL", @"changing to size %@", NSStringFromSize(rectView.size));
        gg_resize_screen( rectView.size.width, rectView.size.height);
        
		glViewport (0, 0, WIDTH, HEIGHT);
	}
}

- (void) clearView
{
    [[self openGLContext] makeCurrentContext];
    [self resizeGL];
    glClearColor(0, 0, 0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    [[self openGLContext] flushBuffer];
}

- (void) awakeFromNib
{
    [self prepareOpenGL];
}




- (void)drawRect:(NSRect)aRect
{
    NSParameterAssert(theLoop);
//    if(!_inited){
//        _inited = YES;
//        [[self openGLContext] makeCurrentContext];
//        [theLoop beforeRunningLoop];
//        
//        [theLoop runOnce];
//    }
//    else{
//        [[self openGLContext] makeCurrentContext];
//        
//    }
	[self resizeGL]; // forces projection matrix update (does test for size changes)
                     //    glDrawBuffer(GL_BACK);
    
    [eliteBridge displayGL];
        [[self openGLContext] flushBuffer];
//    glFlush();
}

- (void) update // window resizes, moves and display changes (resize, depth and display config change)
{
    NSDebugMLLog(@"GL", @"update called");
    [super update];
}

- (IBAction) hello:(id)sender
{
    NSLog(@"hello");
}

#pragma mark -
#pragma mark Event Handling
-(void)keyDown:(NSEvent *)theEvent
{
//    GGNSWindow *topWin = [theLoop windowFirstResponder];
    
}

- (void)keyUp:(NSEvent *)theEvent
{
    
}

- (void)mouseMoved:(NSEvent *)theEvent
{
    
}

- (void)mouseDown:(NSEvent *)theEvent // trackball
{
    
}

- (void)mouseUp:(NSEvent *)theEvent
{
    
}

- (void)rightMouseDown:(NSEvent *)theEvent // pan
{
    
}

- (void)rightMouseUp:(NSEvent *)theEvent
{
    
}
@end

@implementation MasterWindow : NSWindow
- (void) awakeFromNib
{
    [self setAcceptsMouseMovedEvents:YES];
}

- (void) sendEvent:(NSEvent*) event
{
    NSDebugMLLog(@"NSEvent", @"got %@", event);
    if(![eliteBridge tryDispatchCocoaEvent:event]){
    }
    [super sendEvent:event];

//    NSLog(@"got event: %@", event);
//    [super sendEvent:event];
}

- (void)noResponderFor:(SEL)eventSelector
{
    
}

@end

