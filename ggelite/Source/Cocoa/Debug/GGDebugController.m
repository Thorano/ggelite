//
//  GGDebugController.m
//  elite
//
//  Created by Frederic De Jaeger on 09/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGDebugController.h"

BOOL controlDisable;

@implementation GGDebugController
static GGDebugController* sBlockController;
+ (GGDebugController*) controller
{
    if(nil == sBlockController){
        sBlockController = [[self alloc] init];
    }
    return sBlockController;
}

- (id) init
{
    return [super initWithWindowNibName:@"GGDebug"];
}

- (GGDynamicDebugController*) dynamicController
{
    (void)[self window]; // force the loading of the nib.
    return dynamicController;
}

- (void) awakeFromNib
{
    [debugPanel setBecomesKeyOnlyIfNeeded:YES];
}

- (void) setLogContent:(NSString*)content
{
    [console setString:content];
}

- (IBAction) toogleControl:(id)sender
{
    controlDisable = ([sender state] == NSOnState);
    NSLog(@"control is %d", controlDisable);
}

@end
