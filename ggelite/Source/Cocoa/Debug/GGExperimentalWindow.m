//
//  GGExperimentalWindow.m
//  elite
//
//  Created by Frédéric De Jaeger on 11/22/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGExperimentalWindow.h"
#import "GGWidget.h"
#import "GGNode.h"
#import "GGMenu.h"
#import "GGInteractive3DView.h"
#import "GGEliteBridge.h"
#import "GGRepere.h"
#import "GGBlock.h"
#import "GGBlockContext.h"
#import "GGSmallPlanete.h"
#import "Resource.h"
#import "GGShip.h"
#import "GGGravitation.h"
#import "GGEventDispatcher.h"
#import "GGLog.h"
#import "Planete.h"
#import "GGProceduralLandscape.h"
#import "GGShader.h"
#import "utile.h"

@interface GGTruc : GGMobile
{
    GGNoise*    _noise;
    Texture*    _texture;
    Texture*    _normaleTexture;
}
- (GGNoise*) noise;

@end

#define Height 350.0
@implementation GGExperimentalWindow
- (void) _setup
{
#if 0
    NSArray *ships = [[Resource resourceManager] ships];
    GGSpaceObject* ship = [GGShip shipWithFileName: [ships objectAtIndex: 0]];
    NSLog(@"did load %@", ship);
    [[_view repere] addSubNode:ship];
    [ship setVisible:YES];
#elif 0
    GGEliteBridge* bridge = [[GGEliteBridge alloc] init];
    [[_view root] addSubNode:bridge];
    [bridge loadFileAtPath:nil];
    [bridge release];
#else
    GGShip* ship = [GGShip shipWithFileName:@"vaisseau.shp"];
    initVect(Direction(ship),1,0,0);
    initVect(Haut(ship),0,0,1);
    initVect(Gauche(ship),0,1,0);
    NSAssert(ship, @"coult not load ship");
    initVect(Point(ship),10,0, Height);
    [ship setVisible:YES];
    [[_view repere] addSubNode:ship];
    [[_view repere] setZMin:-1 zMax:-1];
    
    initVect(Point([_view ligthSource]),0,50,Height+200);
//    [_view setRotateLight:YES];
    GGSolid* solid = [ship dynamicalHelper];
//    [solid setMasse:10];
//    [solid setHomogeneousInertia:500];
    [solid addGravity];
    
#if 0
    /*cube*/
    ship = [GGShip shipWithFileName:@"vaisseau.shp"];
    [ship setModel:[GGModel modelWithName:@"Duplo.bld"
                                       scale:100]];
    initVect(Point(ship),0,0,-100);
    [ship setVisible:YES];
    ship->masse = 1e10;
    [[_view repere] addSubNode:ship];
//    [ship beginSchedule];
    solid = [ship dynamicalHelper];
    [solid setInfiniteMass];
#else
    GGSmallPlanete* obj = [[GGSmallPlanete alloc] init];
    [[_view repere] addSubNode:obj];
    [obj release];
#endif
    
    initVect(Point([_view camera]),-40,0, Height);
#endif
}

- (void) _planetSetup
{
    Experience* p = [[Experience alloc] init];
    [p setRadius:1e7];

    [[p landscape]setSeaLimit: -0.1];
    [[_view root] addSubNode:p];
    [p _setupLocalPart];
    [p release];
    [p setVisible:YES];
    
    [p cameraBeginBeingClose];
#ifdef HAVE_COCOA
    [[GGDebugController controller] setCurrentPlanete:p];
#endif
    [[_view root] setOriginOrbiteur:p];
    
    Vect3D v = {0, 0, 1};
    normaliseVect(&v);
    mulScalVect(&v,1e7+5e3,&v);
    *Point([_view repere]) = v;

    initVect(Point([_view camera]),-40,0,300);
    GGShip* ship = [GGShip shipWithFileName:@"vaisseau.shp"];
    NSAssert(ship, @"coult not load ship");
    initVect(Point(ship),10,0, 300);
    [ship setVisible:YES];
    [[_view repere] addSubNode:ship];
    //    [ship beginSchedule];
//    GGSolid* solid = [ship dynamicalHelper];
    //    NSLog(@"masse:%g\ninertial:\n%@", solid->_masse, stringOfMatrice(&solid->_inertial));
    //    [solid setMasse:10];
    //    [solid setHomogeneousInertia:500];
//    [solid addGravity];
    
}

- (void) _textureSetup
{
    GGTruc* truc = [[GGTruc alloc] init];
    [[_view repere] addSubNode:truc];
    [truc setVisible:YES];
    [[GGDebugController controller] setCurrentPlanete:(id)truc]; 
    [truc release];
    initVect(Point([_view camera]),-40,0,50);
    [_view setLookAt:mkVect(5, 5, 0)];
    [_view setRotateLight:YES];
}

- initWithFrame:(NSRect)rect
{
    self = [super initWithFrame:rect];
    if(self){
//        [self addCloseButton];
        _view = [[GGInteractive3DView alloc] initWithFrame:NSMakeRect(10,30,rect.size.width-20,rect.size.height-30)];
        [[self contentView] addSubview:_view];
        [self _setup];
//        [self _planetSetup];
//        [self _textureSetup];
        
        _wizardDispatcher = [[GGEventDispatcher dispatcherWithName:@"WizardKeys"] retain];
        _globalDispatcher = [[GGEventDispatcher dispatcherWithName:@"GlobalKeys"] retain];
        _logger = [[GGLog alloc] init];
        [GGLog setLogger:_logger];
    }

    return self;
}

- init
{
    NSRect aRect = NSMakeRect(0, 0, WIDTH, HEIGHT);
    return [self initWithFrame: aRect];
}

- (void) dealloc
{
    [_blockContext release];
    [_view release];
    [_wizardDispatcher release];
    [_logger release];
    [super dealloc];
}

- (void) drawLog
{
    [self initWindowGL];
    [_logger logAndCleanAtPoint:NSMakePoint(250,frameWindow.size.height-20)];
}

- (void) display
{
    if( !open )
        return;
    [super display];
    [self drawLog];
}

- (void) setOption:(id)sender
{
    NSLog(@"huhu");
}

- (void) setTimeSpeedWithNumber:(NSNumber*)aNumber
{
    NSLog(@"set speed to %@", aNumber);
    [theLoop setTimeSpeed:[aNumber floatValue]];
}

- (void) keyDown: (Event*)theEvent
{
    if(_wizardDispatcher && [_wizardDispatcher dispatchKeyDownWithEvent:theEvent target:[ProtoPlanete defaultContextForDebugging]]){
        // it's ok, it was for the block.
    }
    else if(_wizardDispatcher && [_wizardDispatcher dispatchKeyDownWithEvent:theEvent target:[GGBlock class]]){
    }
    else if(_globalDispatcher && [_globalDispatcher dispatchKeyDownWithEvent:theEvent target:self]){
    }
    else{
        [super keyDown:theEvent];
    }
}

@end


@implementation GGTruc
- (void) dealloc
{
    NSLog(@"ggtruc dying");
    [_noise release];
    [_texture release];
    [_normaleTexture release];
    [super dealloc];
}

- (BOOL) shouldCache
{
    return NO;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

#define SIZETEXTURE 512
- (Texture*) texture
{
    GGNoise* noise = [self noise];
    if(nil == _texture){
        unsigned char* data = malloc(SIZETEXTURE*SIZETEXTURE*3); // RGB
        unsigned char* normal = malloc(SIZETEXTURE*SIZETEXTURE*3); // RGB
        for(int i = 0; i < SIZETEXTURE; ++i){
            for(int j = 0; j < SIZETEXTURE; ++j){
                int offset = (SIZETEXTURE*i + j)*3;
                double grad[3];
                VectCol col;
                double h = [noise noiseAtXPos:(double)i/(double)SIZETEXTURE
                            yPos:(double)j/(double)SIZETEXTURE 
                             zPos:0
                              gradientRef:grad colorRef:&col];
                data[offset] = (unsigned char)(col.r*255);
                data[offset+1] = (unsigned char)(col.g*255);
                data[offset+2] = (unsigned char)(col.b*255);
                Vect3D norm;
                if(h >= 0){
                    norm = mkVect(-grad[0]/1000, -grad[1]/1000, 1.0);
//                    norm = mkVect(-grad[0], -grad[1], 1.0);
                }else {
                    norm = mkVect(0, 0, 1);
                    data[offset] = (unsigned char)128;
                    data[offset+1] = (unsigned char)200;
                    data[offset+2] = (unsigned char)200;

                }

                normaliseVect(&norm);
                divScalVect(&norm, 2, &norm);
                norm.x += 0.5;
                norm.y += 0.5;
                norm.z += 0.5;
                normal[offset] = (unsigned char)(norm.x*255);
                normal[offset+1] = (unsigned char)(norm.y*255);
                normal[offset+2] = (unsigned char)(norm.z*255);
            }
        }
        _texture = [[Texture alloc] initWithData:data width:SIZETEXTURE height:SIZETEXTURE channels:3 mipmap:YES];
        _normaleTexture = [[Texture alloc] initWithData:normal width:SIZETEXTURE height:SIZETEXTURE channels:3 mipmap:YES];
        free(data);
        free(normal);
    }
    
    return _texture;
}

- (Texture*) normaleTexture
{
    (void)[self texture];
    return _normaleTexture;
}

- (GGNoise*) noise{
    if(nil == _noise){
        _noise = [[GGNoise alloc] init];
        [_noise setScaleInput:4];
        [_noise setScaleOutput:1000];
    }
    return _noise;
}

- (void) rasterise: (Camera *)cam
{
    Texture* t = [self texture];
    Texture* n = [self normaleTexture];
    GGShader* shader = [GGShader bumpShader];
    glPushAttrib(GL_ENABLE_BIT);
//    glDisable(GL_LIGHTING);
    [shader attach];
    glEnable(GL_TEXTURE_2D);
    glLightf(GL_LIGHT0,GL_SPOT_EXPONENT,6.0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, [t idTexture]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, [n idTexture]);
	GLCHECK;
	//    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
    [shader setTangent:mkVect(1, 0, 0)];
    [shader setBinormal:mkVect(0, 1, 0)];
    glBegin(GL_QUADS);
    {
        glNormal3f(0, 0, 1);
        glTexCoord2f(0, 0);
        glVertex3f(0, 0, 0);

        glTexCoord2f(1, 0);
        glVertex3f(10, 0, 0);

        glTexCoord2f(1, 1);
        glVertex3f(10, 10, 0);

        glTexCoord2f(0, 1);
        glVertex3f(0, 10, 0);
    }
    glEnd();
    glDisable(GL_TEXTURE_2D);
    [shader detach];
    glPopAttrib();
}

//- (void) _rasterise: (Camera *)cam
//{
//    GGShader* shader = [GGShader bumpShader];
//    [shader initLazy];
//    [shader renderFrame];
//}

#pragma mark method used as a planet by the debug controller
- (id) localNoise
{
    return nil;
}

- (void) reload
{
}

- (id) blockContext
{
    return nil;
}

- (void) reloadTexture
{
    ASSIGN(_texture, nil);
    ASSIGN(_normaleTexture, nil);
}


@end