//
//  GGExperimentalWindow.h
//  elite
//
//  Created by Frédéric De Jaeger on 11/22/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "NSWindow.h"

@class GGInteractive3DView;
@class GGBlockContext;
@class GGSmallPlanete;
@class GGLog;

@interface GGExperimentalWindow : GGNSWindow {
    GGInteractive3DView*        _view;
    GGBlockContext *     _blockContext;
    GGEventDispatcher*  _wizardDispatcher;
    GGEventDispatcher*  _globalDispatcher;

    GGLog               *_logger;
}

@end
