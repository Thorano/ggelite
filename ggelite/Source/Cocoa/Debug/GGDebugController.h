//
//  GGDebugController.h
//  elite
//
//  Created by Frederic De Jaeger on 09/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class GGDynamicSimulator, GGDynamicDebugController;
@interface GGDebugController : NSWindowController
{
    IBOutlet GGDynamicDebugController*       dynamicController;
    IBOutlet NSPanel*                       debugPanel;
    IBOutlet NSTextView*                    console;
}
+ (GGDebugController*) controller;
- (GGDynamicDebugController*) dynamicController;
- (void) setLogContent:(NSString*)content;

- (IBAction) toogleControl:(id)sender;
@end


extern BOOL controlDisable;