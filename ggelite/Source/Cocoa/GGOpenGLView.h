//
//  GGOpenGLView.h
//  elite
//
//  Created by Frederic De Jaeger on 28/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "GGEvent.h"

@class GGEliteCocoaBridge;

@interface GGOpenGLView : NSOpenGLView
{
    BOOL                        _inited;
    
    IBOutlet  id                _menuItem;
    IBOutlet GGEliteCocoaBridge*        eliteBridge;
}
+ (NSOpenGLPixelFormat*) basicPixelFormat;

- (id) initWithFrame: (NSRect) frameRect;
- (void) awakeFromNib;
- (void) clearView;

- (IBAction) hello:(id)sender;
@end

@interface MasterWindow : NSWindow
{
    IBOutlet NSView*                    _mainView;
    IBOutlet GGEliteCocoaBridge*        eliteBridge;
}
@end

struct __Event{
    NSEvent*    originalEvent;
    struct recElement*      joyElement;
    long                    joyValue;
    int         x, y;
    int         deltax, deltay;
};

void ggMakeEventFromCocoEvent(NSEvent* event, Event *pResult);
