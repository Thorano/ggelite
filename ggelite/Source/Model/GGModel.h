//
//  GGModel.h
//  elite
//
//  Created by Frederic De Jaeger on 01/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#ifndef __GGModel_h
#define __GGModel_h 1

#import <Foundation/Foundation.h>

#import "GG3D.h"

typedef struct __triangle
{
    Vect3D	A, B, C;
    Vect3D	pos, vit;
    Vect3D	rotation;
    Vect3D	normal;
    VectCol	couleur;
    double	lifeTime;
    double	totalLife;
}Triangle;

@class ModelBlender;
#import "ModelBlend.h"

@interface GGModel : NSObject {

}
+ (ModelBlender*)modelWithName: (NSString *) name
                          properties: (NSDictionary *)dico;
+ (ModelBlender*)modelWithName: (NSString *) name;
+ (ModelBlender*)modelWithName: (NSString *) name
                               scale: (float)scaleFactor;

+ (ModelBlender*)modelWithPath: (NSString *) path
                    properties: (NSDictionary *)dico;

@end

#endif
