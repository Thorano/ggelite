//
//  GGModelX3D.h
//  elite
//
//  Created by Frederic De Jaeger on 01/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@class ModelBlender;
@interface GGModelX3D : NSObject {

}
+ (ModelBlender*)modelWithPath:(NSString *) path
                    properties:(NSDictionary*)properties;
@end
