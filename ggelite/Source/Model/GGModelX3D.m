//
//  GGModelX3D.m
//  elite
//
//  Created by Frederic De Jaeger on 01/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGModelX3D.h"

#import "Texture.h"
#import "GGMMesh.h"
#import "GGModel.h"

@interface NSXMLElement (GGEPrivateExtension)
- (NSXMLElement*) onlyElementForName:(NSString*)name;
- (double) doubleForAttribute:(NSString*)name;
- (VectCol) colorInElementWithName:(NSString*)name;
@end

@implementation GGModelX3D
+ (GGMatrix4) transformationForObjectNode:(NSXMLElement*)blenderObject
{
    GGMatrix4 ret;
    initWithIdentityMatrix(&ret);
    pmat pret = (pmat)&ret;
    NSXMLElement* trans = [blenderObject onlyElementForName:@"Transform"];
    if(nil == trans){
        return ret;
    }
    
    int i, j;
    for(i = 0; i < 4; ++i){
        for(j = 0; j < 4; ++j){
            NSXMLNode* attribute = [trans attributeForName:[NSString stringWithFormat:@"a%d%d", i, j]];
            NSAssert2(attribute, @"could not find attribute coef at %d %d", i, j);
            NSString* strVal = [attribute stringValue];
            pret[i][j] = [strVal doubleValue];
        }
    }
    
    return ret;
}

+ (Vect3D) positionFromNode:(NSXMLElement*)aNode
{
    Vect3D ret;
    ret.x = [aNode doubleForAttribute:@"x"];
    ret.y = [aNode doubleForAttribute:@"y"];
    ret.z = [aNode doubleForAttribute:@"z"];
    
    return ret;
}

+ (Vect3D) normalFromNode:(NSXMLElement*)aNode
{
    Vect3D ret;
    ret.x = [aNode doubleForAttribute:@"nx"];
    ret.y = [aNode doubleForAttribute:@"ny"];
    ret.z = [aNode doubleForAttribute:@"nz"];
    
    return ret;
}

+ (GGMMesh*)buildMeshFromMeshNode:(NSXMLElement*)meshNode
                     materialsBag:(NSMutableDictionary*)matByName
{
    NSXMLNode* nameAttr = [meshNode attributeForName:@"DEF"];
    NSAssert(nameAttr != nil, @"mesh node should have a name");
    GGMMesh* mesh = [[GGMMesh alloc] initWithName:[nameAttr stringValue]];
    {
        NSXMLElement* verticesNode = [meshNode onlyElementForName:@"Vertices"];
        NSArray* allVertices = [verticesNode elementsForName:@"V"];
        NSEnumerator* verticesEnum = [allVertices objectEnumerator];
        NSXMLElement* aVerticeNode;
        while((aVerticeNode = [verticesEnum nextObject])!=nil){
            Vect3D pos = [self positionFromNode:aVerticeNode];
            Vect3D norm = [self normalFromNode:aVerticeNode];
            double u = [aVerticeNode doubleForAttribute:@"u"];
            double v = [aVerticeNode doubleForAttribute:@"v"];
            [mesh addVertex:pos
                     normal:norm
                 uTextCoord:(float)u
                 vTextCoord:(float)v];
        }
    }
    VertexDataTex* vertices = [mesh vertices];
    {
        NSXMLElement* facesNode = [meshNode onlyElementForName:@"Faces"];
        NSArray* allFaces = [facesNode elementsForName:@"F"];
        NSEnumerator* faceEnum = [allFaces objectEnumerator];
        NSXMLElement* aFaceNode;
        while((aFaceNode = [faceEnum nextObject])!=nil){
            short maxFace = 0;
            short vals[5];
            NSXMLNode* aNode;
            GLfloat uv[5][2];
            BOOL hasTexture = [aFaceNode attributeForName:@"U0"] != nil;
            for(;;){
                aNode = [aFaceNode attributeForName:[NSString stringWithFormat:@"A%d", maxFace]];
                if(aNode){
                    uv[maxFace][0] = [aFaceNode doubleForAttribute:[NSString stringWithFormat:@"U%d", maxFace]];
                    uv[maxFace][1] = 1.0 - [aFaceNode doubleForAttribute:[NSString stringWithFormat:@"V%d", maxFace]];
                    vals[maxFace] = [[aNode stringValue] intValue];
                    maxFace++;
                }
                else{
                    break;
                }
            }
            NSAssert1(maxFace == 3, @"cannot handle any other kind of vertex per face: %d", maxFace);
            aNode = [aFaceNode attributeForName:@"M"];
            NSAssert(aNode, @"face should have a material");
            short materialIndex = [[aNode stringValue] intValue];
            Vect3D norm = normalFace(vertices[vals[0]].pos,
                                     vertices[vals[1]].pos,
                                     vertices[vals[2]].pos);
            normaliseVect(&norm);
            [mesh addTriangleWithAIndex:vals[0]
                                 BIndex:vals[1]
                                 CIndex:vals[2]
                                 normal:norm
                                 smooth:NO
                          materialIndex:materialIndex
                             hasTexture:hasTexture
                     textureCoordinates:uv];
        }
        
    }
    
    
    {
        NSXMLElement* appearanceNode = [meshNode onlyElementForName:@"Appearance"];
//        NSLog(@"got %@", appearanceNode);
        NSArray* allMaterials = [appearanceNode elementsForName:@"Material"];
        NSEnumerator* matEnum = [allMaterials objectEnumerator];
        NSXMLElement* aMatNode;
        while((aMatNode = [matEnum nextObject])!=nil){
            NSXMLNode* aNode;
            aNode = [aMatNode attributeForName:@"USE"];
            if(aNode){
                NSString* materialName = [aNode stringValue];
                NSValue* value = [matByName objectForKey:materialName];
                Material mat;
                [value getValue:&mat];
                [mesh addMaterial:mat];
            }
            else{
                aNode = [aMatNode attributeForName:@"DEF"];
                NSString* materialName = [aNode stringValue];
                NSAssert(materialName, @"a material should have a name");
                Material mat;

                VectCol      diffuseColor = [aMatNode colorInElementWithName:@"color"];
                VectCol      specCol = [aMatNode colorInElementWithName:@"specularColor"];
                GLfloat      ambiance = [aMatNode doubleForAttribute:@"ambientIntensity"];
                GLfloat      emit = [aMatNode doubleForAttribute:@"emit"];
                GLfloat      specExp = [aMatNode doubleForAttribute:@"shininess"];
                NSArray*     tmp = [aMatNode elementsForName:@"ImageTexture"];
                if(tmp && [tmp count] > 0){
                    NSXMLElement* textureBlock = [tmp objectAtIndex:0];
                    NSString* fileName = [[textureBlock attributeForName:@"url"] stringValue];
                    [mesh setTexture:[Texture textureWithFileName:fileName]];
                    NSString* bumpFile = [[[fileName stringByDeletingPathExtension] stringByAppendingString:@"_bump"] stringByAppendingPathExtension:[fileName pathExtension]];
                    [mesh setBumpTexture:[Texture textureWithFileName:bumpFile]];
                }
                mat = [mesh addMaterialWithColor:diffuseColor
                                         ambiant:ambiance
                                   specularColor:specCol
                                specularExponent:specExp
                                            emit:emit];
                NSValue* value = [[NSValue alloc] initWithBytes:&mat
                                                       objCType:@encode(Material)];
                [matByName setObject:value forKey:materialName];
                [value release];
            }
        }
        
    }
    
    
    return [mesh autorelease];
}

+ (ModelBlender*)modelWithPath:(NSString *) path
                    properties:(NSDictionary*)properties
{
    NSError* error = nil;
    ModelBlender* model = nil;
    
//    NSLog(@"should parse gg3d file at %@", path);
    NSXMLDocument* document = [[NSXMLDocument alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path]
                                                                   options:0
                                                                     error:&error];
    if(nil == document){
        NSWarnMLog(@"could not parse document at path %@ because of %@", path, error);
        goto exit;
    }
    
    NSXMLElement *rootNode = [document rootElement];
    if(nil == rootNode){
        NSWarnMLog(@"no root node for %@", document);
        goto exit;
    }
    
    NSArray* scenes = [rootNode elementsForName:@"Scene"];
    if([scenes count] == 0){
        NSWarnMLog(@"no Scene in the file %@", path);
        goto exit;
    }
    
    NSXMLElement* scene = [scenes objectAtIndex:0];
    if([scenes count] > 1){
        NSWarnMLog(@"file with more than one scene.  Will only pick the first");
    }
    
    model = [[ModelBlender alloc] init];
    NSArray* topNodes = [scene children];
    NSXMLElement* blenderObject;
    NSEnumerator* childerEnum = [topNodes objectEnumerator];
    NSMutableDictionary* meshByName = [NSMutableDictionary dictionaryWithCapacity:[scene childCount]];
    NSMutableDictionary* matByName =  [NSMutableDictionary dictionaryWithCapacity:[scene childCount]];
    while((blenderObject = [childerEnum nextObject])!=nil){
        if(![[blenderObject name] isEqualToString:@"Object"]){
            NSDebugMLLog(@"GG3D", @"skipping %@", [blenderObject name]);
            continue;
        }
        NSString* objectName = [[blenderObject attributeForName:@"DEF"] stringValue];
        NSDebugMLLog(@"GG3D", @"object name = %@ %@", [blenderObject name], objectName);
        NSXMLElement* meshNode = [blenderObject onlyElementForName:@"Mesh"];
        NSAssert(meshNode, @"there should be a mesh");
        NSString* meshName = [[meshNode attributeForName:@"USE"] stringValue];
        GGMMesh* mesh = nil;
        if(meshName){
            mesh = [meshByName objectForKey:meshName];
            if(nil == mesh){
                [NSException raise:NSInvalidArgumentException
                            format:@"object %@ use the mesh with name %@ but this one was not previously defined",
                    objectName, meshName];
            }
        }
        else{
            meshName = [[meshNode attributeForName:@"DEF"] stringValue];
            NSAssert(meshName, @"we should have a meshname here");
            mesh = [self buildMeshFromMeshNode:meshNode
                                  materialsBag:matByName];
            [meshByName setObject:mesh forKey:meshName];
        }
        
        GGMatrix4 transformation = [self transformationForObjectNode:blenderObject];
        GGMNode* node = [[GGMNode alloc] initWithMesh:mesh
                                            transform:&transformation];
        [node setParentName:[[blenderObject attributeForName:@"parent"] stringValue]];
        [node setObjectName:objectName];
        [model addMeshNode:node withName:objectName];
        [node release];
    }
    [model finalizeAfterLoading];
    
exit:
        [document release];
    [model autorelease];
    return model;
}
@end

@implementation NSXMLElement(GGEPrivateExtension)
- (NSXMLElement*) onlyElementForName:(NSString*)name
{
    NSArray* elements = [self elementsForName:name];
    NSAssert1([elements count] <= 1, @"too many elements for the name %@", name);
    
    if([elements count] == 1){
        NSXMLElement* element = [elements objectAtIndex:0];
        NSAssert([element isKindOfClass:[NSXMLElement class]], @"object is not an XML element");
        
        return element;
    }
    else{
        return nil;
    }
}

- (double) doubleForAttribute:(NSString*)name
{
    NSXMLNode* aNode = [self attributeForName:name];
    if(aNode){
        double ret = [[aNode stringValue] doubleValue];
        return ret;
    }
    else{
        return 0.0;
    }
}

- (VectCol) colorInElementWithName:(NSString*)name
{
    VectCol col;
    initRGBA(&col,0,0,0,1);
    NSXMLElement* subElement = [self onlyElementForName:name];
    if(subElement){
        col.r = [subElement doubleForAttribute:@"r"];
        col.g = [subElement doubleForAttribute:@"g"];
        col.b = [subElement doubleForAttribute:@"b"];
    }
    
    return col;
}
@end
