/* 	-*-ObjC-*- */
/*
 *  Model3DMF.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Model3DSMF_h
#define __Model3DSMF_h
#import "GG3D.h"
#import "parseASC.h"
#import "GGMMesh.h"

@class NSString;
@class NSMutableDictionary;
@class Texture;

@interface Model3DMF : NSObject
{
  GLuint		idTexture;
  NSString 		*nameObject;
  VertexDataTex		*vertices;
  Face			*faces;
  int 			nvert, nface;
  Vect3D		min, max;	
  BOOL			norm_per_vertex;
}
+ modelWithFileName: (NSString *) name;
+ modelWithFileName: (NSString *) name
	 dictionary: (NSDictionary *)dico;
- (void) rasterise;
- (Vect3D *) boundingBox: (Vect3D *)pax;
@end

void skipSpace(const char **buffer);
void getWord(char word[50], const char **buffer);
int skipToEndOfLine(const char **buffer);
void skipUntilLineBegin(const char **buffer, const char *begin);


#endif
