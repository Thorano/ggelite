/* 	-*-ObjC-*- */
/*
 *  parseASC.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __parseASC_h
#define __parseASC_h
#import "GG3D.h"
#import "Explosion.h"

@class NSString;
@class NSMutableDictionary;


typedef struct __face
{
  short int 	A, B, C, materialIndex;
  Vect3D	normal;
  unsigned short	colorIndex;
  BOOL 		AB, BC, CA;
  BOOL		smooth;
}Face;

@interface ModelASC : NSObject
{
  NSString 		*nameObject;
  Vect3D		*vertices;
  Face			*faces;
  int 			nvert, nface;
  Vect3D		min, max;

  NSMutableData*                _colors;
  NSMutableDictionary*          _nameToColorIndex;
}
- (id) initWithPath:(NSString*)path
         properties:(NSDictionary*)properties;
- (int) getTriangles: (Triangle **) tg;
- (void) rasterise;
- (Vect3D *) boundingBox: (Vect3D *)pax;
@end

void skipUntilWord(const char **buffer, const char *word);
void skipSpace(const char **buffer);
void getWord(char word[50], const char **buffer);
int skipToEndOfLine(const char **buffer);
void skipUntilLineBegin(const char **buffer, const char *begin);
extern int numLigne;

#endif
