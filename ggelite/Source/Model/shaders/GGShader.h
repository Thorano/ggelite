//
//  GGShader.h
//  elite
//
//  Created by Frédéric De Jaeger on 01/07/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "GG3D.h"
#import GLPath(glext.h)


@class GGShadowShader;

@interface GGShader : NSObject {
@public
    BOOL        _inited;
	GLhandleARB _program_object;
    GLuint      bumpmap_texture;
}
+ (GGShader*) bumpShader;
+ (GGShadowShader*) shadowShader;

// must be override by concrete shaders
- (void) lazyLoadShader;
    
- (BOOL) attach;
- (BOOL) detach;


- (void) setTangent:(Vect3D)v;
- (void) setBinormal:(Vect3D)v;

@end

@interface GGShadowShader : GGShader
- (void) setEpsilon:(GLfloat)eps;
@end


static inline void GGSetBinormalFromNormal(GGShader* shader, Vect3D norm)
{
    Vect3D tmp;
    double fx, fy;
    fx = fabs(norm.x);
    fy = fabs(norm.y);
    if(fx > fy){
        tmp = mkVect(0,1,0);
    }
    else {
        tmp = mkVect(1,0,0);
    }
    
    Vect3D tangent;
    prodVect(&norm,&tmp,&tangent);
    Vect3D binormal;
    prodVect(&tangent,&norm,&binormal);
	ggVertexAttrib3v(glGetAttribLocationARB(shader->_program_object, "Tangent"), tangent);
	ggVertexAttrib3v(glGetAttribLocationARB(shader->_program_object, "Binormal"), binormal);
    glNormal3v(&norm);
}

