//
// bumpmap.vert: Vertex shader for bump mapping in surface local coordinates
//
// Author: Randi Rost
//
// Copyright (c) 2002-2006 3Dlabs Inc. Ltd.
//
// See 3Dlabs-License.txt for license information
//

varying vec3 viewDir;     // interpolated surface local coordinate view direction
varying vec3 ggNorm;     // interpolated surface local coordinate view direction

varying vec3 lightPos;
varying vec3 vertexPos;
varying vec3 textureCoord;

void main(void)
{
    // Do standard vertex stuff
    vec4 vertexEye = gl_ModelViewMatrix * gl_Vertex;
    vec4 textureCoord4 = gl_TextureMatrix[0] * vertexEye;
    textureCoord = textureCoord4.xyz / textureCoord4.w;
    gl_Position  = ftransform();
    vertexPos = vertexEye.xyz;
    gl_TexCoord[0] = gl_MultiTexCoord0;
    gl_FrontColor = gl_Color;
    gl_BackColor = gl_Color;
    ggNorm = normalize(gl_NormalMatrix * gl_Normal);
    lightPos = gl_LightSource[0].position.xyz;
    viewDir = normalize(vec3(gl_ModelViewMatrix * gl_Vertex));

}
