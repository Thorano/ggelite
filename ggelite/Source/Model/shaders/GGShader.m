//
//  GGShader.m
//  elite
//
//  Created by Frédéric De Jaeger on 01/07/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "GGShader.h"
#import "Resource.h"
#import "utile.h"

@interface GGBumpShader : GGShader
- (void) initLazy;
- (void) renderFrame;
@end

@interface GGShader(Private)
- (BOOL) _loadVertexShader: (NSData *) vertexString fragmentShader: (NSData *) fragmentString;
@end

@implementation GGShader
static GGBumpShader*   sBumpShader;
static GGShadowShader* sShadowShader;
+ (GGShader*) bumpShader
{
    if(nil == sBumpShader){
        sBumpShader = [[GGBumpShader alloc] init];
        [[Resource resourceManager] addForCleaning:self];
    }
    
    return sBumpShader;
}

+ (GGShadowShader*) shadowShader
{
    if(nil == sShadowShader){
        sShadowShader = [[GGShadowShader alloc] init];
        [[Resource resourceManager] addForCleaning:self];
    }
    
    return sShadowShader;
}

+ (void) clean
{
    DESTROY(sBumpShader);
    DESTROY(sShadowShader);
}

- (void) dealloc
{
	if (_program_object) {
		glDeleteObjectARB(_program_object);
		_program_object = NULL;
	}
    
    [super dealloc];
}

- (void) _setupIfNeeded
{
    if(_inited){
        return;
    }
    
    _inited = YES;
    [self lazyLoadShader];
}

- (void) lazyLoadShader
{
    [self doesNotRecognizeSelector:_cmd];
}



- (BOOL) attach
{
    [self _setupIfNeeded];
    if(NULL == _program_object){
        NSLog(@"could not attach fragment shader");
        return NO;
    }
    
    glUseProgramObjectARB(_program_object);
    return YES;
}

- (BOOL) detach
{
    glUseProgramObjectARB(NULL);
    return YES;
}

- (void) setTangent:(Vect3D)v
{
    ggVertexAttrib3v(glGetAttribLocationARB(_program_object, "Tangent"), v);
}

- (void) setBinormal:(Vect3D)v
{
    ggVertexAttrib3v(glGetAttribLocationARB(_program_object, "Binormal"), v);
}

@end


@implementation GGShader(Private)
static NSString* getInfoLog(GLhandleARB obj)
{
    GLint infologLength = 0;
    GLsizei charsWritten  = 0;
    char *infoLog;
    NSString* ret = nil;
	
    glGetObjectParameterivARB(obj, GL_OBJECT_INFO_LOG_LENGTH_ARB,
                              &infologLength);
	
    if (infologLength > 0)
    {
		infoLog = (char *)malloc(infologLength);
		glGetInfoLogARB(obj, infologLength, &charsWritten, infoLog);
        ret = [[[NSString alloc] initWithBytes:infoLog length:infologLength-1 encoding:NSASCIIStringEncoding] autorelease];
		free(infoLog);
    }
    
    return ret;
}



- (BOOL) _loadVertexShader: (NSData *) vertexString fragmentShader: (NSData *) fragmentString
{
    GLhandleARB vertex_shader, fragment_shader;
    const GLcharARB *vertex_string, *fragment_string;
    GLint length;
    GLint vertex_compiled, fragment_compiled;
    GLint linked;
    
    /* Delete any existing program object */
    if (_program_object) {
        glDeleteObjectARB(_program_object);
        _program_object = NULL;
    }
    
    /* Load and compile both shaders */
    if (vertexString) {
        vertex_shader   = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
        vertex_string   = (GLcharARB *) [vertexString bytes];
        length = [vertexString length];
        glShaderSourceARB(vertex_shader, 1, &vertex_string, &length);
        glCompileShaderARB(vertex_shader);
        glGetObjectParameterivARB(vertex_shader, GL_OBJECT_COMPILE_STATUS_ARB, &vertex_compiled);
        /* TODO - Get info log */
    } else {
        vertex_shader   = NULL;
        vertex_compiled = 1;
    }
    
    if (fragmentString) {
        fragment_shader   = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
        fragment_string   = [fragmentString bytes];
        length = [fragmentString length];
        glShaderSourceARB(fragment_shader, 1, &fragment_string, &length);
        glCompileShaderARB(fragment_shader);
        glGetObjectParameterivARB(fragment_shader, GL_OBJECT_COMPILE_STATUS_ARB, &fragment_compiled);
        /* TODO - Get info log */
    } else {
        fragment_shader   = NULL;
        fragment_compiled = 1;
    }
    
    /* Ensure both shaders compiled */
    BOOL destroyShaders = NO;
    
    if(!vertex_compiled){
        destroyShaders = YES;
        NSString* msg = getInfoLog(vertex_shader);
        NSLog(@"could not compile vertex shader:\n%@", msg);
    }
    else if(vertex_shader){
        NSLog(@"could compile vertex shader:\n%@", getInfoLog(vertex_shader));
    }
    
    if(!fragment_compiled){
        destroyShaders = YES;
        NSString* msg = getInfoLog(fragment_shader);
        NSLog(@"could not compile fragment shader:\n%@", msg);
    }
    else if(fragment_shader){
        NSLog(@"could compile fragment shader:\n%@", getInfoLog(fragment_shader));
    }
    
    if (destroyShaders) {
        if (vertex_shader) {
            glDeleteObjectARB(vertex_shader);
            vertex_shader   = NULL;
        }
        if (fragment_shader) {
            glDeleteObjectARB(fragment_shader);
            fragment_shader = NULL;
        }
        NSLog(@"failed to compiler someone");
        return 1;
    }
    
    /* Create a program object and link both shaders */
    _program_object = glCreateProgramObjectARB();
    if (vertex_shader != NULL)
    {
        glAttachObjectARB(_program_object, vertex_shader);
        glDeleteObjectARB(vertex_shader);   /* Release */
    }
    if (fragment_shader != NULL)
    {
        glAttachObjectARB(_program_object, fragment_shader);
        glDeleteObjectARB(fragment_shader); /* Release */
    }
    glLinkProgramARB(_program_object);
    glGetObjectParameterivARB(_program_object, GL_OBJECT_LINK_STATUS_ARB, &linked);
    /* TODO - Get info log */
    
    if (!linked) {
        NSLog(@"could not link:\n%@", getInfoLog(_program_object));        
        glDeleteObjectARB(_program_object);
        _program_object = NULL;
        return 1;
    }
    
    return 0;
}

@end

@implementation GGBumpShader
- (void) lazyLoadShader
{
    NSData* fragmentShaderData = [[Resource resourceManager] dataForResource:@"Bumpmap.frag" inDirectory:nil];
    NSData* vertexShaderData = [[Resource resourceManager] dataForResource:@"Bumpmap.vert" inDirectory:nil];
    if(fragmentShaderData && vertexShaderData){
        [self _loadVertexShader:vertexShaderData fragmentShader:fragmentShaderData];
        printGLError(nil);        
        
        glBindAttribLocationARB(_program_object, 4, "Tangent");
        glBindAttribLocationARB(_program_object, 5, "Binormal");
        glLinkProgramARB(_program_object);
        
        /* Setup uniforms */
        glUseProgramObjectARB(_program_object);
        glUniform1iARB(glGetUniformLocationARB(_program_object, "NormalMap"), 0);
        glUniform1iARB(glGetUniformLocationARB(_program_object, "ColorMap"), 1);
        glUniform3fARB(glGetUniformLocationARB(_program_object, "BaseColor"), 0.8, 0.7, 0.3);
        glUniform1fARB(glGetUniformLocationARB(_program_object, "SpecularFactor"), 0.7);
        glUniform1fARB(glGetUniformLocationARB(_program_object, "DiffuseFactor"), 2.0);
        glUniform3fARB(glGetUniformLocationARB(_program_object, "LightPosition"), 0.0, 0.0, 5.0);
    }
}


NSBitmapImageRep *LoadImage(NSString *path, int shouldFlipVertical)
{
	NSBitmapImageRep *bitmapimagerep;
	NSImage *image;
	image = [[[NSImage alloc] initWithContentsOfFile: path] autorelease];
	bitmapimagerep = [[NSBitmapImageRep alloc] initWithData:[image TIFFRepresentation]];
	
	if (shouldFlipVertical)
	{
		int bytesPerRow, lowRow, highRow;
		unsigned char *pixelData, *swapRow;
		
		bytesPerRow = [bitmapimagerep bytesPerRow];
		pixelData = [bitmapimagerep bitmapData];
        
		swapRow = (unsigned char *)malloc(bytesPerRow);
		for (lowRow = 0, highRow = [bitmapimagerep pixelsHigh]-1; lowRow < highRow; lowRow++, highRow--)
		{
			memcpy(swapRow, &pixelData[lowRow*bytesPerRow], bytesPerRow);
			memcpy(&pixelData[lowRow*bytesPerRow], &pixelData[highRow*bytesPerRow], bytesPerRow);
			memcpy(&pixelData[highRow*bytesPerRow], swapRow, bytesPerRow);
		}
		free(swapRow);
	}
    
	return bitmapimagerep;
}



- (void) initLazy
{
    if(_inited) return;
    /* Setup GLSL */
    {
        NSString *string;
        NSBitmapImageRep *bitmapimagerep;
        NSRect rect;
        
        
        /* normal texture */
        glGenTextures(1, &bumpmap_texture);
        string = @"/Developer/Examples/OpenGL/Cocoa/GLSLShowpiece/Textures/face_norm.jpg";
        
        bitmapimagerep = LoadImage(string, 0);
        rect = NSMakeRect(0, 0, [bitmapimagerep pixelsWide], [bitmapimagerep pixelsHigh]);
        
        glBindTexture(GL_TEXTURE_2D, bumpmap_texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, rect.size.width, rect.size.height, 0,
                     (([bitmapimagerep hasAlpha])?(GL_RGBA):(GL_RGB)), GL_UNSIGNED_BYTE, 
                     [bitmapimagerep bitmapData]);
        
        /* Load vertex and fragment shader */
        [self _setupIfNeeded];
        if (NULL == _program_object){
            NSLog(@"Failed to load Bumpmap");            
        }
    }
}


- (void) renderFrame
{
    if(NULL == _program_object){
        NSLog(@"can't render");
        return;
    }
    glUseProgramObjectARB(_program_object);
    GLCHECK;	
    glBindTexture(GL_TEXTURE_2D, bumpmap_texture);
    GLCHECK;
    glVertexAttrib3f(glGetAttribLocationARB(_program_object, "Tangent"), 0.0, 1.0, 0.0);
    glVertexAttrib3f(glGetAttribLocationARB(_program_object, "Binormal"), 1.0, 0.0, 0.0);
    
    glBegin(GL_QUADS);
    glNormal3f(0.0, 0.0, 1.0);
    glTexCoord2f(0.0, 1.0);
    glVertex2f(-0.5, -0.5);
    glTexCoord2f(1.0, 1.0);
    glVertex2f( 0.5, -0.5);
    glTexCoord2f(1.0, 0.0);
    glVertex2f( 0.5,  0.5);
    glTexCoord2f(0.0, 0.0);
    glVertex2f(-0.5,  0.5);
    glEnd();
    
    glUseProgramObjectARB(NULL);
    GLCHECK;
}

@end

@implementation GGShadowShader
- (void) lazyLoadShader
{
    NSData* fragmentShaderData = [[Resource resourceManager] dataForResource:@"Shadow.frag" inDirectory:nil];
    NSData* vertexShaderData = [[Resource resourceManager] dataForResource:@"Shadow.vert" inDirectory:nil];
    if(fragmentShaderData && vertexShaderData){
        [self _loadVertexShader:vertexShaderData fragmentShader:fragmentShaderData];
        printGLError(nil);        
        if(_program_object){
        glLinkProgramARB(_program_object);
        
        /* Setup uniforms */
        glUseProgramObjectARB(_program_object);
		
        glUniform1iARB(glGetUniformLocationARB(_program_object, "ShadowMap"), 1);
        glUniform1fARB(glGetUniformLocationARB(_program_object, "SpecularFactor"), 0.3);
        glUniform1fARB(glGetUniformLocationARB(_program_object, "DiffuseFactor"), 0.7);
        glUniform1fARB(glGetUniformLocationARB(_program_object, "EpsilonDecal"), 0.01);
        }
    }
}

- (void) setEpsilon:(GLfloat)eps
{
    [self _setupIfNeeded];
    if(_program_object){
        glUniform1fARB(glGetUniformLocationARB(_program_object, "EpsilonDecal"), eps);
    }
}

@end