//
// bumpmap.frag: Fragment shader for bump mapping in surface local coordinates
//
// Author: Randi Rost
//
// Copyright (c) 2002-2006 3Dlabs Inc. Ltd.
//
// See 3Dlabs-License.txt for license information
//

//uniform sampler2DShadow ShadowMap;
uniform sampler2D ShadowMap;
uniform float DiffuseFactor;
uniform float SpecularFactor;
uniform float EpsilonDecal;

varying vec3 viewDir;     // interpolated surface local coordinate view direction
varying vec3 ggNorm;     // interpolated surface local coordinate view direction

varying vec3 lightPos;
varying vec3 vertexPos;
varying vec3 textureCoord;

vec3 standardPixelLighting ()
{
    vec3 r;
    float intensity;
    float spec;
    float d;
    vec3 lightDir = lightPos - vertexPos;
    lightDir = normalize(lightDir);
    float lightScalNorm = dot(lightDir, ggNorm);

    // Fetch normal from normal map
    intensity = max(lightScalNorm, 0.0) * DiffuseFactor;

    // Compute specular reflection component
    d = 2.0 * lightScalNorm;
    r = d*ggNorm;
    r = lightDir - r;
    spec = pow(max(dot(r, viewDir), 0.0) , 6.0) * SpecularFactor;
    intensity += min(spec, 1.0);

     // Compute color value
    return clamp(gl_Color.rgb * intensity, 0.0, 1.0);
}

float evaluateDepthMap(float x, float y)
{
    vec2 toto =  vec2(x*EpsilonDecal, y*EpsilonDecal);
    vec4 colorZ = texture2D(ShadowMap, textureCoord.xy+toto);
//    vec4 colorZ = texture2D(ShadowMap, textureCoord.xy);
    // we substract 0.01 to avoid z-fighting
    return colorZ.x >= textureCoord.z  ? 1.0 : 0.5;
}

void main (void)
{
    vec3 color;
    float coef = evaluateDepthMap(0.0, 0.0);
//    float coef = evaluateDepthMap(-0.5, -0.5);
//    coef += evaluateDepthMap(-0.5, +0.5);
//    coef += evaluateDepthMap(+0.5, +0.5);
//    coef += evaluateDepthMap(+0.5, -0.5);
//    coef /= 4.0;
    color = coef*standardPixelLighting();

//    vec4 depthCompare = shadow2D(ShadowMap, textureCoord);
//    color = color*depthCompare.x + vec3(1,0,0)*(1.0 - depthCompare.x);
    
    // Write out final fragment color
    gl_FragColor = vec4(color, 1.0);
//    gl_FragColor = depthCompare;
    
}
