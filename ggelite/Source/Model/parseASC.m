/*
 *  parseASC.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/Foundation.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSZone.h>
#import <Foundation/NSException.h>
#import <stdlib.h>
#import <string.h>
#import "Metaobj.h"
#import "GG3D.h"
#import "parseASC.h"
#import "GGSpaceObject.h"
#import "utile.h"
#import "Resource.h"
#import "Explosion.h"
#import "GGMMesh.h"
#import "ModelBlend.h"

NSString *EndOfString=@"myEndOfString";
NSString *WordTooBig=@"myWordTooBig";

//#define RAISEENDFOFSTRING     abort()
#define RAISEENDFOFSTRING     [NSException raise: EndOfString  \
                                          format: @"Unexpected End Of File"]

int numLigne;


void skipSpace(const char **buffer)
{
    while( **buffer != '\0' && isspace( **buffer ) )
    {
        if( **buffer == '\n' || **buffer == '\r' )
            numLigne++;
        (*buffer)++;
    }
    if( **buffer == '\0' )
        //    abort();
        RAISEENDFOFSTRING;
}

void getWord(char word[50], const char **buffer)
{
    int i = 0;
    skipSpace(buffer);
    while( **buffer != '\0' && isgraph(**buffer) && i < 49 )
        word[i++] = *((*buffer)++);
    if( i == 49 )
    {
        [NSException raise: WordTooBig format: @""];
    }
    word[i] = '\0';
    //  fprintf(stderr, "str : '%s'\n", word);
}

//  void getIdent(char word[50], const char **buffer)
//  {
//    skipSpace(buffer);

//  }

int skipToEndOfLine(const char **buffer)
{
    while( **buffer != '\0' && **buffer != '\n'  )
        (*buffer)++;
    if( **buffer == '\n' || **buffer == '\n' )
    {
        (*buffer)++;
        numLigne++;
        return 0;
    }
    else /* end of file */
        return -1;
}

void skipUntilLineBegin(const char **buffer, const char *begin)
{
    int len = strlen(begin);
    while( strncasecmp(*buffer, begin, len) != 0 )
    {
        if( skipToEndOfLine(buffer) < 0 )
            RAISEENDFOFSTRING;
    }
}

void skipUntilWord(const char **buffer, const char *word)
{
    char buf[50];
    while(1)
    {
        getWord(buf, buffer);
        if( strcasecmp(buf, word) == 0 )
            return;
    }
}


@interface  ModelASC (Private)
- (void) parseASC: (NSString *)name;
- (void) _setOptionWithDictionary: (NSDictionary *)dico;
- (void) calcBoundingBox;
- (void) initNormal;
- (GGMMesh*) computeMesh;
@end

@implementation ModelASC
- init
{
    [super init];
    nameObject = nil;
    vertices = NULL;
    faces = NULL;
    addToCheck(self);
    return self;
}

- (id) initWithPath:(NSString*)path
         properties:(NSDictionary*)properties
{
    self = [self init];
    if(self){
        [self parseASC: path];
        [self _setOptionWithDictionary: properties];
        [self calcBoundingBox];
        [self initNormal];
        
#if 1
        /* all the mesh we have in that format obey the convention that the main axe of the 
            ship is the z axis.
            */
        GGMMesh* mesh = [self computeMesh];
        GGMatrix4 tr = {
        {0, 1, 0}, 0,
        {0, 0, 1}, 0,
        {1, 0, 0}, 0,
        {0, 0, 0}, 1,
        };
        GGMNode* node = [[GGMNode alloc] initWithMesh:mesh transform:&tr];
        ModelBlender* modelBlender = [[ModelBlender alloc] initWithNode:node];
        [node release];
        [self release];
        
        return modelBlender;
#endif
    }
    
    return self;
}

- (void) dealloc
{
    RELEASE(nameObject);
    if( vertices != NULL )
        NSZoneFree([self zone], vertices);
    if( faces != NULL )
        NSZoneFree([self zone], faces);
    RELEASE(_colors);
    RELEASE(_nameToColorIndex);
    [super dealloc];
}


- (int) getCouleurIndexFromString: (NSString *)str
{
    VectCol cl;
    NSNumber* index;
    if(_nameToColorIndex == nil){
        _nameToColorIndex = [NSMutableDictionary new];
    }
    
    if( (index = [_nameToColorIndex objectForKey: str]) != nil )
    {
        return [index intValue];
    }
    else
    {
        const char *chaine = [str UTF8String];
        unsigned int val;
        int r, g, b;
        cl.a = 1;
        if( sscanf(chaine, "r%dg%db%d", &r, &g, &b) == 3 )
        {
            cl.r = (float)r/255;
            cl.g = (float)g/255;
            cl.b = (float)b/255;
        }
        else if (sscanf(chaine, "#%x", &val) == 1 )
        {
            cl.r = (float) ((val>>16) & 0xff) / 255;
            cl.g = (float) ((val >> 8) & 0xff) / 255;
            cl.b = (float) ((val) & 0xff) / 255;
        }
        else
        {
            NSWarnMLog(@"can't parse %@", str);
            cl.r = vrnd();
            cl.g = vrnd();
            cl.b = vrnd();
        }
        if(nil == _colors){
            _colors = [[NSMutableData alloc] initWithCapacity:10*sizeof(VectCol)];
        }
        
        int currentIndex = [_colors length] / sizeof(VectCol);
        [_colors appendBytes:&cl length:sizeof(cl)];
        index = [[NSNumber alloc] initWithInt:currentIndex];
        [_nameToColorIndex setObject: index forKey: str];
        RELEASE(index);
        return currentIndex;
    }
}

- (void) initNormal
{
    int i;
    for( i = 0; i < nface; ++i)
    {
        Vect3D tp1, tp2, tp3;
        Face theFace = faces[i];
        mkVectFromPoint(&(vertices[theFace.A]), &(vertices[theFace.B]), &tp1);
        mkVectFromPoint(&(vertices[theFace.A]), &(vertices[theFace.C]), &tp2);
        
        prodVect(&tp1, &tp2, &tp3);
        normaliseVect(&tp3);
        faces[i].normal = tp3;
    }
}

- (void) calcBoundingBox
{
    int i;
    Vect3D _max = { -1e20, -1e10, -1e20 };
    Vect3D _min = { 1e20, 1e10, 1e20 };
    
    min = _min;
    max = _max;
    
    for( i = 0; i < nvert; ++i )
    {
        Vect3D pos = vertices[i];
        
        min.x = (min.x < pos.x ? min.x : pos.x);
        min.y = (min.y < pos.y ? min.y : pos.y);
        min.z = (min.z < pos.z ? min.z : pos.z);
        
        max.x = (max.x > pos.x ? max.x : pos.x);
        max.y = (max.y > pos.y ? max.y : pos.y);
        max.z = (max.z > pos.z ? max.z : pos.z);
    }
    
    NSDebugMLLog(@"parseur", @"min = %@, max = %@", stringOfVect2(min),
                 stringOfVect2(max));
}

- (int) getTriangles: (Triangle **) pt
{
    int i;
    Triangle *t;
    *pt = malloc(nface *  sizeof(Triangle));
    const VectCol *colors = [_colors bytes];
    
    for(i = 0; i < nface; ++i)
    {
        Vect3D G;
        Face f = faces[i];
        
        t = (*pt) + i;
        t->A = vertices[f.A];
        t->B = vertices[f.B];
        t->C = vertices[f.C];
        
        G = t->A;
        addVect(&G, &t->B, &G);
        addVect(&G, &t->C, &G);
        divScalVect(&G, 3, &G);
        
        diffVect(&t->A, &G, &t->A);
        diffVect(&t->B, &G, &t->B);
        diffVect(&t->C, &G, &t->C);
        
        t->pos = G;
        t->normal = f.normal;
        t->couleur = colors[f.colorIndex];
    }
    
    
    return nface;
}

- (Vect3D *) boundingBox: (Vect3D *)pax
{
    *pax = max;
    return &min;
}

- (void) rasterise
{
    int i;
    //  glDisable(GL_CULL_FACE);
    glBegin(GL_TRIANGLES);
#if 1
    const VectCol* colors = [_colors bytes];
    for (i = 0; i < nface; ++i)
    {
        Face theFace = faces[i];
        VectCol col = colors[faces[i].colorIndex];
        glColor3f(col.r,
                  col.g,
                  col.b);
        glNormal3f(faces[i].normal.x,
                   faces[i].normal.y,faces[i].normal.z);
        glVertex3f(vertices[theFace.A].x,
                   vertices[theFace.A].y,
                   vertices[theFace.A].z);
        glVertex3f(vertices[theFace.B].x,
                   vertices[theFace.B].y,
                   vertices[theFace.B].z);
        glVertex3f(vertices[theFace.C].x,
                   vertices[theFace.C].y,
                   vertices[theFace.C].z);
    }
#endif
#if 0
    glColor3d(0.8, 0.8, 0.8);
    glVertex3d(1, 0, 0);
    glVertex3d(0, 1, 0);
    glVertex3d(0, 0, 1);
#endif
    glEnd();
    //  glEnable(GL_CULL_FACE);
}


- (void) parseASC: (NSString *)name
{
    NSData *data = [NSData dataWithContentsOfFile:name];
    
    if( data == nil )
    {
        [NSException raise: @"FileNotFound" format: @""];
    }
    else
    {
        const char *buffer = [data bytes];
        char word[50];
        char *ptr;
        int res, i;
        
        skipUntilLineBegin(&buffer, "Named object");
        
        getWord(word, &buffer);
        getWord(word, &buffer);
        getWord(word, &buffer);
        
        ptr = strchr(word+1, '"');
        NSAssert(ptr != NULL, @"no Quote");
        
        
        *ptr='\0';
        
        nameObject = [[NSString alloc] initWithUTF8String: word+1];
        NSDebugMLLog(@"parseur", @"nom : %@", nameObject);
        skipToEndOfLine(&buffer);
        res = sscanf(buffer, "Tri-mesh, Vertices: %d Faces: %d", &nvert, &nface);
        NSAssert(res == 2, @"can't parse nvert, nface");
        NSDebugMLLog(@"parseur", @"nvert = %d nface = %d", nvert, nface);
        
        vertices = NSZoneMalloc([self zone], sizeof(Vect3D)*nvert);
        faces = NSZoneMalloc([self zone], sizeof(Face)*nface);
        
        skipToEndOfLine(&buffer);
        
        skipUntilLineBegin(&buffer, "Vertex list:");
        skipToEndOfLine(&buffer);
        
        for( i = 0; i < nvert; ++i )
        {
            double t1, t2, t3;
            skipUntilLineBegin(&buffer, "Vertex");
            if( sscanf(buffer, "Vertex %*d: X:%lf Y:%lf Z:%lf", &t1, &t2, &t3) < 3 )
            {
                NSLog(@"merdouille %d", i);
                [NSException raise:@"SyntaxError" format: @"vertices expected"];
            }
            vertices[i].x = t1;
            vertices[i].y = t2;
            vertices[i].z = t3;
            skipToEndOfLine(&buffer);
        }
        
        skipUntilLineBegin(&buffer, "Face list:");
        skipToEndOfLine(&buffer);
        
        
        for( i = 0; i < nface; ++i )
        {
            int ab, bc, ca;
            skipUntilLineBegin(&buffer, "Face");
            if( sscanf(buffer, 
                       "Face %*d:    A:%hd B:%hd C:%hd AB:%d BC:%d CA:%d",
                       &(faces[i].A),&(faces[i].B),&(faces[i].C),
                       &ab, &bc, &ca) < 6 )
            {
                NSLog(@"chie level %d, %s", i, buffer);
                [NSException raise:@"SyntaxError" format: @"face expected"];
            }
            else
            {
                faces[i].AB = !!ab;
                faces[i].BC = !!bc;
                faces[i].CA = !!ca;
                NSAssert(0<= faces[i].A && faces[i].A < nvert, 
                         @"vertex out of range");
                NSAssert(0<= faces[i].B && faces[i].B < nvert, 
                         @"vertex out of range");
                NSAssert(0<= faces[i].C && faces[i].C < nvert, 
                         @"vertex out of range");
            }
            
            skipToEndOfLine(&buffer);
            
            faces[i].colorIndex = [self getCouleurIndexFromString:@"r255g255b255"];
            for(;;) {
                if( strncasecmp(buffer, "Face", 4) == 0 )
                    goto endOfLoop;
                else if( strncasecmp(buffer, "Material", 8) == 0 )
                    break;
                if( skipToEndOfLine(&buffer) < 0 )
                    goto endOfLoop;
            }
            
            skipUntilLineBegin(&buffer, "Material");
            {	      
                char *ptr2;
                NSString *tex;
                ptr = strchr(buffer, '"');
                NSAssert(ptr != NULL, @"no Quote");
                ptr2 = strchr(ptr+1, '"');
                NSAssert(ptr2 != NULL, @"no Quote");
                *ptr2 = '\0';
                buffer = ptr2+1;
                tex = [NSString stringWithUTF8String: ptr+1];
                faces[i].colorIndex = [self  getCouleurIndexFromString:tex];
                //	      NSLog(@"texture : %@", tex);
            }
            skipToEndOfLine(&buffer);
endOfLoop:
                ;
        }
        
    }
}

- (void) _setOptionWithDictionary: (NSDictionary *)dico
{
    NSString *val;
    if( dico == nil )
        return;
    
    if( (val = [dico objectForKey:@"Scale"]) != nil )
    {
        GGReal fact;
        fact = [val doubleValue];
        if( fact <= 0 )
            NSWarnMLog(@"negative scale factor !!");
        else
        {
            int i;
            NSDebugMLLog(@"parseur", @"scaling model by %g", (double) fact);
            for( i = 0; i < nvert; ++i)
            {
                vertices[i].x *= fact;
                vertices[i].y *= fact;
                vertices[i].z *= fact;
            }
        }
    }
}

- (GGMMesh*) computeMesh
{
    VertexDataTex* someVertices = AllocType(nvert, VertexDataTex);
    FaceBlend* someFaces = AllocType(nface, FaceBlend);
    int nmat = [_colors length]/sizeof(VectCol);
    Material* someMaterials = AllocType(nmat, Material);
    
    int i;
    for(i = 0; i < nvert; ++i){
        someVertices[i].pos = vertices[i];
    }
    
    for(i = 0; i < nface; ++i){
        someFaces[i].smooth = NO;
        someFaces[i].hasTexture = NO;
        someFaces[i].A = faces[i].A;
        someFaces[i].B = faces[i].B;
        someFaces[i].C = faces[i].C;
        someFaces[i].AB = faces[i].AB;
        someFaces[i].BC = faces[i].BC;
        someFaces[i].CA = faces[i].CA;
        someFaces[i].materialIndex = faces[i].colorIndex;
        someFaces[i].normal = faces[i].normal;
    }
    
    const VectCol *colors = [_colors bytes];
    
    for(i = 0; i < nmat; ++i){
        someMaterials[i].diffuseCol = colors[i];
        someMaterials[i].ambCol = GGMakeCol(0.2,0.2,0.2,1);
        someMaterials[i].specCol = GGMakeCol(0.2,0.2,0.2,1);
        someMaterials[i].specExp = 30;
        someMaterials[i].emitCol = GGMakeCol(0., 0., 0.,1);
    }
    
    GGMMesh* mesh = [[GGMMesh alloc] initWithName:nameObject
                                         vertices:someVertices
                                 numberOfVertices:nvert
                                            faces:someFaces
                                    numberOfFaces:nface
                                        materials:someMaterials
                                numberOfMaterials:nmat];
    free(someVertices);
    free(someFaces);
    free(someMaterials);
    return [mesh autorelease];
}

// - (NSString *)description
// {
//   int i;
//   NSMutableString *str = [NSMutableString string];

//   [str appendString: @"Vertices\n"];
//   for( i = 0; i < nvert; ++i )
//     [str appendFormat: @"A: %g B: %g C: %g\n", vertices[i].x,
// 	 vertices[i].y, vertices[i].z];

//   [str appendString: @"Faces\n"];
//   for( i = 0; i < nface; ++i)
//     [str appendFormat: @"A:%hd B:%hd C:%hd\n", faces[i].A,
// 	 faces[i].B,faces[i].C];
//   return str;
// }
@end
