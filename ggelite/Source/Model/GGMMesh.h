//
//  GGMMesh.h
//  elite
//
//  Created by Frederic De Jaeger on 25/03/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//


#import "GG3D.h"
#import "GGAABB.h"

#define MAXMATPERMESH 20

typedef struct __vertex
{
    Vect3D              pos;
    Vect3D              normal;
    float               u,v;
}VertexDataTex;

typedef struct __cacheVertex
{
//    unsigned int        index;
    Vect3D              pos;
}CacheVertex;

typedef struct 
{
    short int 	A, B, C, materialIndex;
    Vect3D	normal;
    VectCol	couleur;
    GLfloat       uv[3][2];
    BOOL 		AB, BC, CA;
    BOOL		smooth;
    BOOL        hasTexture;
}FaceBlend;

typedef struct 
{
    VectCol      ambCol;
    VectCol      diffuseCol;
    VectCol      specCol;
    GLfloat      specExp;
    VectCol      emitCol;      
}Material;

@class Texture;

@interface GGMMesh : NSObject
{
    NSMutableData		*_verticesData;
    NSMutableData       *_facesData;
    NSMutableData       *_materialData;
    Texture             *texture;
    Texture             *_bumpTexture;
    NSString            *name;
    
}
+ meshWithName: (NSString *)s
       nVertex: (int) v
         nFace: (int) f;
- (id) initWithName:(NSString *)s
           vertices:(VertexDataTex*)vertices
   numberOfVertices:(int)numberOfVertices
              faces:(FaceBlend*)faces
      numberOfFaces:(int)numberOfFaces
          materials:(Material*)materials
  numberOfMaterials:(int)numberOfMaterials;

- (id) initWithName:(NSString*)name;

- (void) addVertex:(Vect3D)pos
            normal:(Vect3D)norm;
- (void) addVertex:(Vect3D)pos
            normal:(Vect3D)norm
        uTextCoord:(float)u
        vTextCoord:(float)v;
- (void) addTriangleWithAIndex:(short)A
                        BIndex:(short)B
                        CIndex:(short)C
                        normal:(Vect3D)norm
                        smooth:(BOOL)smooth
                 materialIndex:(short)materialIndex
                    hasTexture:(BOOL)hasTexture
            textureCoordinates:(GLfloat[3][2])uv;
- (Material) addMaterialWithColor:(VectCol)diffuseCol
                          ambiant:(GLfloat)ambiant
                    specularColor:(VectCol)specCol
                 specularExponent:(GLfloat)specExp
                             emit:(GLfloat)emit;

- (void) addMaterial:(Material)aMaterial;

- (void) setTexture: (Texture *) t;
- (void) setBumpTexture:(Texture*)t;

- (NSString*) name;
- (int) numberOfFaces;
- (int) numberOfVertices;

- (VertexDataTex*) vertices;
- (FaceBlend*)faces;


- (void) rasterizeMesh;

- (void) applyScaleFactor:(double)factor;
- (void) getTriangleIn:(struct __triangle*)trianglePtr;

- (void) computeBoudingBoxIn:(BoundingBox*)box;
- (void) computeVerticesAtIndices:(NSIndexSet*)indexSet
                 forTransformation:(GGMatrix4*)tranformation
                        inResults:(CacheVertex*)results;
- (void) computeFaceEquationsForIndices:(NSIndexSet*)indexSet
                      forTransformation:(GGMatrix4*)tranformation
                              inResults:(GGPlan*)results;
@end

// we keep that object immutable
/* the tree structure is only used for experimental stuff.  Models loaded from file have a flat stuff.
*/
@interface GGMNode : NSObject
{
    GGMNode             *_parent;  // not retained

    NSMutableArray      *_sons;
    NSDictionary*       _properties;
    
    GGMMesh*            _data;

    GGMatrix4           _transformation;
    BOOL                _keepOrientation;

    // only used during the parsing.
    NSString            *_parentName;
    NSString            *_objectName;
}
- (id) initWithMesh:(GGMMesh*)mesh
          transform:(GGMatrix4*)pmat;
- (id) initWithMesh:(GGMMesh*)mesh;

/*returns a new mesh node by applying the transformation
*/
- (GGMNode*) meshNodeWithTransformation:(GGMatrix4*)pmat;

- (void) setParentName:(NSString*)val;
- (NSString*) parentName;

- (NSString *)objectName;
- (void)setObjectName:(NSString *)value;

- (GGMMesh*) mesh;
- (void) computeVerticesAtIndices:(NSIndexSet*)indexSet
                 forTransformation:(GGMatrix4*)tranformation
                        inResults:(CacheVertex*)results;

- (void) computeFaceEquationsForIndices:(NSIndexSet*)indexSet
                      forTransformation:(GGMatrix4*)tranformation
                              inResults:(GGPlan*)results;

- (void) getTransformation:(GGMatrix4*)ret;
- (BOOL) orientationFlipped;
- (Vect3D) position;

- (void) setProperties:(NSDictionary*)val;

- (void) improveBoundingBox:(BoundingBox*)boxPtr;

- (void) rasterise: (Camera *)cam;
- (void) getTriangleIn:(struct __triangle*)trianglePtr;
- (double) getUnit; // only apply if the mesh is a "Unit"

// That one mutates the object.  It is only used close to a constructor
- (void) applyScaleFactor:(double)factor;


#pragma mark -
#pragma mark stuff for the bridge
- (void) translateByVect:(Vect3D*)v;

- (void) setParent:(GGMNode*)val;
- (GGMNode*)parent;
- (void) addSubNode:(GGMNode*) aSon;
- (GGMNode*) firstSon;
- (NSArray*) subMeshNodes;
- (void) removeAllSons;


@end