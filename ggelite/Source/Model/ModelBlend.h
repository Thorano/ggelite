/* 	-*-ObjC-*- */
/*
 *  ModelBlend.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: September 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ModelBlend_h
#define __ModelBlend_h
#import "GG3D.h"
@class NSString;
@class NSMutableDictionary;
@class Texture, GGMMesh, GGMNode;

@interface ModelBlender : NSObject
{
    NSMutableArray          *tab;
    Vect3D                  min, max;
    NSMutableDictionary     *_objectByName;
}
+ modelWithFileName: (NSString *) name;
+ modelWithFileName: (NSString *) name
	 dictionary: (NSDictionary *)dico;

- (ModelBlender*) transformModel:(GGMatrix4*)pmat;

- (id) init;
- (id) initWithNode:(GGMNode*)node;
- (id) initWithFileAtPath:(NSString*)path;
- (void) rasterise: (Camera *)cam;
- (Vect3D *) boundingBox: (Vect3D *)pax;
- (int) getTriangles: (Triangle **) pt;


// XXX not efficient
- (void) addMeshNode:(GGMNode*)node
            withName:(NSString*)name;

- (NSArray*) nodes;
- (NSArray*) topLevelObjects;

- (void) finalizeAfterLoading;
@end

Vect3D normalFace(Vect3D a, Vect3D b, Vect3D c);
#endif
