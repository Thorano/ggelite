//
//  GGMMesh.m
//  elite
//
//  Created by Frederic De Jaeger on 25/03/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGMMesh.h"

#import "Metaobj.h"
#import "Texture.h"
#import "GGLog.h"
#import "utile.h"
#import "GGModel.h"
#import "GGShader.h"

@implementation GGMMesh
- initWithName: (NSString *)s
       nVertex: (int) v
         nFace: (int) f
{
    [super init];
    
    _verticesData = [[NSMutableData alloc] initWithCapacity:v*sizeof(VertexDataTex)];
    _facesData = [[NSMutableData alloc] initWithCapacity:f*sizeof(FaceBlend)];
    _materialData = [[NSMutableData alloc] initWithCapacity:3*sizeof(Material)];
    ASSIGN(name, s);
    
    addToCheck(self);
    return self;
}

- (id) initWithName:(NSString*)s
{
    name = [s copy];
    return self;
}

- (id) initWithName:(NSString *)s
           vertices:(VertexDataTex*)someVertices
   numberOfVertices:(int)numberOfVertices
              faces:(FaceBlend*)someFaces
      numberOfFaces:(int)numberOfFaces
          materials:(Material*)materials
  numberOfMaterials:(int)numberOfMaterials
{
    self = [self initWithName:s
                      nVertex:numberOfVertices
                        nFace:numberOfFaces];
    if(nil == self){
        return nil;
    }
    NSAssert(_verticesData, @"no vertices");
    [_verticesData appendBytes:someVertices length:numberOfVertices*sizeof(VertexDataTex)];
    [_facesData appendBytes:someFaces length:numberOfFaces*sizeof(FaceBlend)];
    [_materialData appendBytes:materials length:numberOfMaterials*sizeof(Material)];
    
    return self;
}

- (void) dealloc
{
    [_verticesData release];
    [_facesData release];
    [_materialData release];
    RELEASE(texture);
    RELEASE(_bumpTexture);
    RELEASE(name);
    
    [super dealloc];
}

- (void) setTexture: (Texture *) t
{
    ASSIGN(texture, t);
}

- (void) setBumpTexture:(Texture*)t
{
    ASSIGN(_bumpTexture, t);
}

- (NSString*) name
{
    return name;
}

+ meshWithName: (NSString *)s
       nVertex: (int) v
         nFace: (int) f;
{
    GGMMesh *m;
    m = [self alloc];
    m = [m initWithName: s
                nVertex: v
                  nFace: f];
    return AUTORELEASE(m);
}

- (NSString *) description
{
    return [NSString stringWithFormat: @"GGMMesh(%@)", name];
}

- (void) addVertex:(Vect3D)pos
            normal:(Vect3D)norm
        uTextCoord:(float)u
        vTextCoord:(float)v
{
    VertexDataTex vert;
    vert.pos = pos;
    vert.normal = norm;
    vert.u = u;
    vert.v = v;
    if(nil == _verticesData){
        _verticesData = [[NSMutableData alloc] initWithCapacity:20*sizeof(vert)];
    }
    [_verticesData appendBytes:&vert length:sizeof(vert)];
}

- (void) addVertex:(Vect3D)pos
            normal:(Vect3D)norm
{
    [self addVertex:pos
             normal:norm
         uTextCoord:0
         vTextCoord:0];
}


- (void) addTriangleWithAIndex:(short)A
                        BIndex:(short)B
                        CIndex:(short)C
                        normal:(Vect3D)norm
                        smooth:(BOOL)smooth
                 materialIndex:(short)materialIndex
                    hasTexture:(BOOL)hasTexture
            textureCoordinates:(GLfloat[3][2])uv
{
    FaceBlend face;
    face.A = A;
    face.B = B;
    face.C = C;
    face.materialIndex = materialIndex;
    face.normal = norm;
    initRGBA(&face.couleur,0,0,0,1);
    face.AB = 0;
    face.BC= 0 ;
    face.CA= 0 ;
    face.smooth = smooth;
    face.hasTexture = hasTexture;
    memcpy(face.uv,uv,sizeof(face.uv));
    
    if(nil == _facesData){
        _facesData = [[NSMutableData alloc] initWithCapacity:10*sizeof(face)];
    }
    [_facesData appendBytes:&face length:sizeof(face)];
}

- (Material) addMaterialWithColor:(VectCol)diffuseCol
                          ambiant:(GLfloat)ambiant
                    specularColor:(VectCol)specCol
                 specularExponent:(GLfloat)specExp
                             emit:(GLfloat)emit
{
    Material mat;
    mat.diffuseCol = diffuseCol;
    mat.ambCol = GGMakeCol(diffuseCol.r*ambiant,diffuseCol.g*ambiant,diffuseCol.b*ambiant,diffuseCol.a);
    mat.emitCol = GGMakeCol(diffuseCol.r*emit,diffuseCol.g*emit,diffuseCol.b*emit,diffuseCol.a);
    mat.specCol = specCol;
    mat.specExp = specExp;
    
    [self addMaterial:mat];
    return mat;
}

//static NSString *stringOfCol(VectCol col){
//    return [NSString stringWithFormat:@"(r=%g, g=%g, b=%g, a=%g)", col.r, col.g, col.b, col.a];
//}
//
- (void) addMaterial:(Material)aMaterial
{
//    NSLog(@"adding a material:\nambCol=%@\ndiffCol=%@\nspecCol=%@\nspecExp=%g\nemitCol=%@",
//          stringOfCol(aMaterial.ambCol),stringOfCol(aMaterial.diffuseCol),stringOfCol(aMaterial.specCol),
//          aMaterial.specExp,stringOfCol(aMaterial.emitCol));
    if(nil == _materialData){
        _materialData = [[NSMutableData alloc] initWithCapacity:5*sizeof(aMaterial)];
    }
    [_materialData appendBytes:&aMaterial length:sizeof(aMaterial)];
}

- (int) numberOfVertices
{
    return [_verticesData length] / sizeof(VertexDataTex);
}

- (int) numberOfFaces
{
    return [_facesData length] / sizeof(FaceBlend);
}

- (int) numberOfMaterials
{
    return [_materialData length] / sizeof(Material);
}

- (VertexDataTex*) vertices
{
    return [_verticesData mutableBytes];
}

- (FaceBlend*)faces
{
    return [_facesData mutableBytes];
}

- (Material*) materials
{
    return [_materialData mutableBytes];
}

- (void) computeVerticesAtIndices:(NSIndexSet*)indexSet
                 forTransformation:(GGMatrix4*)tranformation
                        inResults:(CacheVertex*)results
{
    unsigned int index;

    index = [indexSet firstIndex];
    int i = 0;
    VertexDataTex* vertices = [self vertices];
    while(index != NSNotFound){
//        results[i].index = index;
        NSParameterAssert(index < [self numberOfVertices]);
        NSAssert(i < [indexSet count], @"bad iteration");
        transformePoint4D(tranformation,&vertices[index].pos,&results[i].pos);
        i++;
        index = [indexSet indexGreaterThanIndex:index];        
    }
}

- (void) computeFaceEquationsForIndices:(NSIndexSet*)indexSet
                      forTransformation:(GGMatrix4*)tranformation
                              inResults:(GGPlan*)results
{
    unsigned int index;
    index = [indexSet firstIndex];
    int i = 0;
    FaceBlend* faces = [self faces];
    VertexDataTex* vertices = [self vertices];
    while(index != NSNotFound){
        NSParameterAssert(index < [self numberOfFaces]);
        Vect3D normal;
        Vect3D positionVertex;
        NSAssert(i < [indexSet count], @"bad iteration");
        
        produitMatriceVecteur3D(tranformation, &faces[index].normal, &normal);
        transformePoint4D(tranformation,&vertices[faces[index].A].pos, &positionVertex);
        results[i] = mkPlanFromPointAndNormal(positionVertex,normal);
        i++;
        index = [indexSet indexGreaterThanIndex:index];        
    }
    NSAssert(i == [indexSet count], @"hoho");
}

- (Vect3D) vertexMean
{
    int i;
    Vect3D res;
    SetZeroVect(res);
    int nvert = [self numberOfVertices];
    VertexDataTex* vertices = [self vertices];
    for(i = 0; i < nvert; ++i){
        addVect(&res,&vertices[i].pos,&res);
    }
    
    divScalVect(&res,nvert,&res);
    
    return res;
}

- (void) translateMeshByVect:(Vect3D*)vect
{
    int i;
    int nvert = [self numberOfVertices];
    VertexDataTex* vertices = [self vertices];
    for(i = 0; i < nvert; ++i){
        addVect(&vertices[i].pos,vect,&vertices[i].pos);
    }
}

- (void) applyScaleFactor:(double)factor
{
    int i;
    int nvert = [self numberOfVertices];
    VertexDataTex* vertices = [self vertices];
    for(i = 0; i < nvert; ++i){
        mulScalVect(&vertices[i].pos,factor,&vertices[i].pos);
    }
}

- (void) computeBoudingBoxIn:(BoundingBox*)box
{
    int i;
    int nvert = [self numberOfVertices];
    if(0 == nvert){
        AABB_initWithMinMax(box,GGZeroVect,GGZeroVect);
    }
    else{
        VertexDataTex* vertices = [self vertices];
        Vect3D pos = vertices[0].pos;
        AABB_initWithMinMax(box,pos,pos);
        
        for( i = 1; i < nvert; ++i )
        {
            extendDomainByPoint(box, vertices[i].pos);
        }    
    }
}



#define MBCheck(i) do{\
    GGCondition(0<= (i) && (i) < nvert);\
}while(0)

#define Plot(i)   do{MBCheck(i);\
if(!shader)glNormal3v(&vertices[i].normal);else GGSetBinormalFromNormal(shader, vertices[i].normal);\
    glVertex3v(&vertices[i].pos);}while(0)

- (void) rasterizeMesh
{
    int j, n;
    short curmat = -1;
    int nface = [self numberOfFaces];
#ifdef DEBUG
    int nvert = [self numberOfVertices];
#endif
    FaceBlend* faces = [self faces];
    VertexDataTex* vertices = [self vertices];
    Material* mats = [self materials];
    NSDebugMLLogWindow(@"Mesh", @"rasterizing %@", [self name]);

    n = nface;
    BOOL    hasTexture;
    GGShader* shader = nil;
//    glDisable(GL_CULL_FACE);
    if ( texture)
    {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, [ texture idTexture]);
        glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
        GLCHECK;
        hasTexture = YES;
        if(_bumpTexture){
//            shader = [GGShader bumpShader];
//            [shader attachBumpShader];
        }
    }
    else{
        hasTexture = NO;
    }
    GLCHECK;
    
    for(j = 0; j < n; ++j )
    {
        FaceBlend *f = &faces[j];
        
        if (curmat != f->materialIndex )
        {
            //                    NSLog(@"new material %d %d", curmat, f->materialIndex);
            Material *mat;
            curmat = f->materialIndex;
            mat = &mats[curmat];
            
            glColor4v(&mat->diffuseCol);
            glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (GGReal *)&mat->ambCol);
            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (GGReal *)&mat->specCol);
            glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GGReal *)&mat->emitCol);
            glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mat->specExp);
            GLCHECK;
        }
        
        
        glBegin(GL_TRIANGLES);
        
        if( f->smooth )
        {
            if(hasTexture && f->hasTexture) glTexCoord2v(&f->uv[0]);
            Plot(f->A);
            if(hasTexture && f->hasTexture) glTexCoord2v(&f->uv[1]);
            Plot(f->B);
            if(hasTexture && f->hasTexture) glTexCoord2v(&f->uv[2]);
            Plot(f->C);
        }
        else
        {
            if(shader){
                GGSetBinormalFromNormal(shader, f->normal);
            }
            else {
                glNormal3v(&f->normal);                
            }

            if(hasTexture && f->hasTexture) glTexCoord2v(&f->uv[0]);
            MBCheck(f->A);
            glVertex3v(&vertices[f->A].pos);
            
            if(hasTexture && f->hasTexture) glTexCoord2v(&f->uv[1]);
            MBCheck(f->B);
            //    glVertex3f(-0.5,4.5,-11);
            glVertex3v(&vertices[f->B].pos);
            
            if(hasTexture && f->hasTexture) glTexCoord2v(&f->uv[2]);
            MBCheck(f->B);
            //    glVertex3f(0.,4.5,+11);
            glVertex3v(&vertices[f->C].pos);
            
        }
        glEnd();
        GLCHECK;
    }
    //            NSLog(@"ok");
    if(shader){
        [shader detach];
        shader = nil;
    }
    if( texture )
    {
        glDisable(GL_TEXTURE_2D);
    }
    GLCHECK;
    
}

- (void) getTriangleIn:(Triangle*)trianglePtr
{
    int j;
    int nface = [self numberOfFaces];
    FaceBlend* faces = [self faces];
    VertexDataTex* vertices = [self vertices];
    Material* mats = [self materials];
    for( j = 0; j < nface; ++j)
    {
        Vect3D G;
        FaceBlend f = faces[j];
        Triangle *t = trianglePtr + j;

        t = trianglePtr + j;
        t->A = vertices[f.A].pos;
        t->B = vertices[f.B].pos;
        t->C = vertices[f.C].pos;
        
        G = t->A;
        addVect(&G, &t->B, &G);
        addVect(&G, &t->C, &G);
        divScalVect(&G, 3, &G);
        
        diffVect(&t->A, &G, &t->A);
        diffVect(&t->B, &G, &t->B);
        diffVect(&t->C, &G, &t->C);
        
        t->pos = G;
        t->normal = f.normal;
        t->couleur = mats[f.materialIndex].emitCol;
    }
}
@end

#pragma mark -

#ifdef DEBUG
static BOOL isCorrectTransform(GGMatrix4* pmat)
{
    GGReal diagFactor = prodScal(&pmat->gauche,&pmat->gauche);
    
    if(diagFactor < 1e-3){
        return NO;
    }
    
    GGReal tmp = prodScal(&pmat->haut,&pmat->haut);
    if(fabs(tmp / diagFactor - 1) > 1e-2){
        return NO;
    }
    
    tmp = prodScal(&pmat->direction,&pmat->direction);
    if(fabs(tmp / diagFactor - 1) > 1e-2){
        return NO;
    }
    
    tmp = prodScal(&pmat->haut,&pmat->gauche);
    if(tmp / diagFactor > 1e-3){
        return NO;
    }
    
    tmp = prodScal(&pmat->haut,&pmat->direction);
    if(tmp / diagFactor > 1e-3){
        return NO;
    }
    
    tmp = prodScal(&pmat->gauche,&pmat->direction);
    if(tmp / diagFactor > 1e-3){
        return NO;
    }
    
    return YES;
}
#define CHECK_CORRECT do{NSAssert(isCorrectTransform(&_transformation), @"matrix is not correct");}while(0)
#else
#define CHECK_CORRECT 
#endif

@implementation GGMNode
- (id) initWithMesh:(GGMMesh*)mesh
          transform:(GGMatrix4*)pmat;
{
    _data = [mesh retain];
    if(pmat){
        _transformation = *pmat;
    }
    else{
        initWithIdentityMatrix(&_transformation);
    }
    _keepOrientation = determinant(&_transformation) > 0;
    CHECK_CORRECT;
    
    return self;
}

- (id) initWithMesh:(GGMMesh*)mesh
{
    return [self initWithMesh:mesh transform:NULL];
}

- (void) dealloc
{
    RELEASE(_parentName);
    RELEASE(_objectName);
    RELEASE(_sons);
    RELEASE(_properties);
    RELEASE(_data);
    
    [super dealloc];
}

- (NSString*) description
{
    if(_objectName){
        return _objectName;
    }
    else{
        return [super description];
    }
}

- (GGMNode*) meshNodeWithTransformation:(GGMatrix4*)pmat
{
    NSAssert(_sons == nil, @"not supported for non flat stuff");
    GGMatrix4 res;
    produitMatrice(pmat,&_transformation,&res);
    GGMNode* transformedNode = [[GGMNode alloc] initWithMesh:_data transform:&res];
    [transformedNode setObjectName:[self objectName]];
    return [transformedNode autorelease];
}


#pragma mark -
- (GGMMesh*) mesh
{
    return _data;
}

- (void) setTransformation:(GGMatrix4*)mat
{
    _transformation = *mat;
    _keepOrientation = determinant(&_transformation) > 0;
    CHECK_CORRECT;
}

- (void) getTransformation:(GGMatrix4*)ret
{
    *ret = _transformation;
}

- (BOOL) orientationFlipped
{
    return !_keepOrientation;
}

- (Vect3D) position
{
    return _transformation.position;
}

- (void) translateByVect:(Vect3D*)v
{
    addVect(v,&_transformation.position,&_transformation.position);
}

- (void) setParentName:(NSString*)val
{
    ASSIGN(_parentName, val);
}

- (NSString*) parentName
{
    return _parentName;
}

- (NSString *)objectName {
    return _objectName;
}

- (void)setObjectName:(NSString *)value {
    if (_objectName != value) {
        [_objectName release];
        _objectName = [value copy];
    }
}

- (void) setParent:(GGMNode*)val;
{
    _parent = val;
}

- (GGMNode*)parent;
{
    return _parent;
}

- (void) setProperties:(NSDictionary*)val
{
    if(_properties != val){
        RELEASE(_properties);
        _properties = [val copy];
    }
}

#pragma mark node stuff

- (void) addSubNode:(GGMNode*) aSon;
{
    if(nil == _sons){
        _sons = [NSMutableArray new];
    }
    [_sons addObject:aSon];
    [aSon setParent:self];
}

- (void) removeAllSons
{
    [_sons makeObjectsPerformSelector:@selector(setParent:)
                           withObject:nil];
    [_sons removeAllObjects];
}

- (NSArray*) subMeshNodes
{
    return _sons;
}

- (GGMNode*) firstSon
{
    if(_sons && [_sons count] > 0){
        return [_sons objectAtIndex:0];
    }
    else{
        return nil;
    }
}

#pragma mark -

- (void) applyScaleFactor:(double)factor
{
    GGMatrix4 mat;
    initWithIdentityMatrix(&mat);
    pmat matPtr = (pmat)&mat;
    matPtr[0][0] = factor;
    matPtr[1][1] = factor;
    matPtr[2][2] = factor;
    
    GGMatrix4 tmp;
    produitMatrice(&mat,&_transformation,&tmp);
    _transformation = tmp;
//    [_data applyScaleFactor:factor];
}

- (void) improveBoundingBox:(BoundingBox*)boxPtr
{
    BoundingBox box;
    BoundingBox meshBox;
    [[self mesh] computeBoudingBoxIn:&meshBox];
    ggTransformBox(&meshBox, &_transformation, &box);
    AABB_extendBox(boxPtr, &box);
}

- (void) rasterise: (Camera *)cam
{
    glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
    glEnable(GL_NORMALIZE);
    
    if(!_keepOrientation){
        glFrontFace(GL_CW);
    }
    glPushMatrix();
    glMultMatrix(&_transformation);
    [[self mesh] rasterizeMesh];
    glPopMatrix();
    glPopAttrib();
}

- (void) getTriangleIn:(Triangle*)trianglePtr
{
    [[self mesh] getTriangleIn:trianglePtr];
}

- (double) getUnit
{
    NSAssert([[[self mesh] name] isEqualToString:@"Unit"], @"only valid on mesh of types Unit");
    int nbrVertices = [[self mesh] numberOfVertices];
    NSAssert(nbrVertices <= 2, @"unit can have two vertices at most");
    double scale;
    GGReal n;
    NSParameterAssert(nbrVertices > 0);
    CacheVertex vertices[2];
    NSIndexSet* two = [[NSIndexSet alloc] initWithIndexesInRange:NSMakeRange(0,nbrVertices)];
    [self computeVerticesAtIndices:two
                 forTransformation:NULL
                         inResults:vertices];
    [two release];
    if(nbrVertices == 1){
        // old legacy not robust way
        n = normeVect(&vertices[0].pos);
    }
    else{
        Vect3D pos;
        diffVect(&vertices[0].pos,&vertices[1].pos,&pos);
        n = normeVect(&pos);
    }
    
    NSParameterAssert(n > 1e-5);
    scale = 1/n;
    NSDebugMLLog(@"parseur", @"scale factor = %g", scale);
    NSLog(@"scale factor = %g", scale);
    
    return scale;
}


- (void) computeVerticesAtIndices:(NSIndexSet*)indexSet
                 forTransformation:(GGMatrix4*)tranformation
                        inResults:(CacheVertex*)results
{
    GGMatrix4 *effectivePtr;
    GGMatrix4 effectiveMatrix;
    if(NULL == tranformation){
        effectivePtr = &_transformation;
    }
    else{
        effectivePtr = &effectiveMatrix;
        produitMatrice(tranformation,&_transformation,&effectiveMatrix);
    }
    [_data computeVerticesAtIndices:indexSet
                   forTransformation:effectivePtr
                          inResults:results];
}

- (void) computeFaceEquationsForIndices:(NSIndexSet*)indexSet
                      forTransformation:(GGMatrix4*)tranformation
                              inResults:(GGPlan*)results
{
    GGMatrix4 effectiveMatrix;
    produitMatrice(tranformation,&_transformation,&effectiveMatrix);
    [_data computeFaceEquationsForIndices:indexSet
                        forTransformation:&effectiveMatrix
                               inResults:results];
}
@end