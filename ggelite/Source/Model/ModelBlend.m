/*
 *  ModelBlend.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: September 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#import <Foundation/NSZone.h>
#import <Foundation/NSException.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSArray.h>
#import "GGModel.h"
#import "parseASC.h"
#import "ModelBlend.h"
#import "Metaobj.h"
#import "utile.h"
#import "GGFont.h"
#import "Texture.h"
#import "Preference.h"
#import "GG3D.h"
#import "GGAABB.h"
#import "Texture.h"
#import "GGMMesh.h"

#define SyntaxError(s) [NSException raise:@"SyntaxError" \
                                   format: @"file %s line %d\nreason: %@",\
    __FILE__, __LINE__, s];


Vect3D normalFace(Vect3D a, Vect3D b, Vect3D c)
{
    Vect3D ab, ac;
    Vect3D res;
    
    mkVectFromPoint(&a, &b, &ab);
    mkVectFromPoint(&a, &c, &ac);
    prodVect(&ab, &ac, &res);
    
    return res;
}

static int myfastScanf(const char *ptr, const char *format, ...)
{
    char line[501];
    int i;
    for(i = 0; i < 500 && ptr[i] != '\n'; ++i){
        line[i] = ptr[i];
    }
    line[i] = '\0';
    NSCAssert(i < 500, @"line too big");
    va_list ap;
    va_start(ap, format);
    int ret = vsscanf(line, format, ap);
    va_end(ap);
    return ret;
}

static void parseMatrice(const char **ptr, GGMatrix4 *pmatr, float scaleFactor)
{
#if 0
    GGMatrix4 rot = {
    {0, 0, scaleFactor}, 0,
    {scaleFactor, 0, 0}, 0,
    {0, scaleFactor, 0}, 0,
    {0, 0, 0}, 1
    };
#else
    GGMatrix4 rot = {
    {scaleFactor, 0, 0}, 0,
    {0, scaleFactor, 0}, 0,
    {0, 0, scaleFactor}, 0,
    {0, 0, 0}, 1
    };
#endif    
    
    GGMatrix4 mat;
    pmat blu = (pmat)&mat;
    int i;
    
    for(i = 0; i < 4; ++i)
    {
        int ind;
        double x, y, z, t;
        if ( myfastScanf(*ptr, " [%d] [%lf, %lf, %lf, %lf]", &ind, &x, &y, &z, &t) != 5 || 
             ind != i )
            SyntaxError(@"parsing matrices");
        blu[i][0] = x;
        blu[i][1] = y;
        blu[i][2] = z;
        blu[i][3] = t;
        skipToEndOfLine(ptr);
    }
    produitMatrice(&rot, &mat, pmatr);
    //  *pmatr = mat;
}

@interface GGMMesh (BlenderParsing)
- (const char *) parseMesh: (const char *) buffer
            transformation:(GGMatrix4*)transformation
                     nvert:(int)nvert
                     nface:(int)nface;
@end

@implementation GGMMesh (BlenderParsing)
- (const char *) parseMesh: (const char *) buffer
            transformation:(GGMatrix4*)transformation
                     nvert:(int)nvert
                     nface:(int)nface
{
    int i;
    double u, v;
    char chaine[50];
    int nmat;
//    GGMatrix4 mat = *transformation;
//    BOOL det = determinant(&mat) < 0;
    BOOL hasText = NO;
    BOOL hasWarn = NO;
    
    for( i = 0; i < nvert; ++i)
    {
        int probe;
        double x, y, z;
        Vect3D pos;
        Vect3D norm;
        if (myfastScanf(buffer, " [%d] [%lf, %lf, %lf]", &probe, &x, &y, &z) != 4 )
        {
            [NSException raise:@"SyntaxError" format: @"parsing vertices for %d", i];
        }
        NSParameterAssert(probe == i);
        
        Vect3D tmp;
        initVect(&tmp, x, y, z);
        pos = tmp;
//        transformePoint4D(&mat, &tmp, &pos);
        //       initVect(&vertices[i].pos, x, y, z);
        skipToEndOfLine(&buffer);
        if (myfastScanf(buffer, " VNormals: [%lf, %lf, %lf]", &x, &y, &z) != 3 )
        {
            NSLog(@"No normal");
            SetZeroVect(norm);
        }
        else
        {
            initVect(&tmp, x, y, z);
            norm = tmp;
//            produitMatriceVecteur3D(&mat, &tmp, &norm);
            // 	  if ( sign )
            // 	    mulScalVect(&tmp, -1.0, &tmp);
            normaliseVect(&norm);
            //	  initVect(&tmp, x, y, z);
        }
        [self addVertex:pos normal:norm];
        skipToEndOfLine(&buffer);
        //       if( myfastScanf(buffer, " Colors: [%lf", &r) != 1 )
        // 	{
        // 	  //	  NSLog(@"No colors");
        // 	}
        skipToEndOfLine(&buffer);
    }
    
    VertexDataTex* vertices = [self vertices];

    skipUntilLineBegin(&buffer, "Faces");
    skipToEndOfLine(&buffer);
    for (i = 0; i < nface; ++i )
    {
        int pr, pr2;
        int a, b, c;
        
        if( myfastScanf(buffer, " [%d] Material [%d] ", &pr, &pr2) != 2 )
        {
            abort();
            [NSException raise:@"SyntaxError" format: @"parsing face material"];
        }
        NSParameterAssert(pr == i);
        
        short materialIndex = pr2;
        Vect3D normal;
        
        skipUntilWord(&buffer, "Vertices:");
        if ( myfastScanf(buffer, " (%d) %d, %d, %d", &pr, &a, &b, &c) != 4 )
        {
            //           if ( myfastScanf(buffer, " (%d) %d, %d, %d", &pr, &a, &b, &c) != 4 )
            abort();
            [NSException raise:@"SyntaxError" format: @"parsing face"];
        }
        
        
        {
            Vect3D norm = normalFace(vertices[a].pos, 
                                     vertices[b].pos, 
                                     vertices[c].pos);
            Vect3D tmp;
            tmp = vertices[a].normal;
            addVect(&vertices[b].normal, &tmp, &tmp);
            addVect(&vertices[c].normal, &tmp, &tmp);
            if( 0   /*det  */ )
            {
                int tmp;
                
                tmp = b;
                b = c;
                c = tmp;
                mulScalVect(&norm, -1.0, &norm);
            }
            normaliseVect(&norm);
            normal = norm;
        }
        
        short A = a;
        short B = b;
        short C = c;
        BOOL hasTexture = NO;
        GLfloat uv[3][2];
        
        skipUntilWord(&buffer, "TexCoords:");
        
        if ( myfastScanf(buffer, " [%lf, %lf]", &u, &v) == 2 )
        {
            int k;
            hasTexture = YES;
            uv[0][0] = u;
            uv[0][1] = v;
            //          NSLog(@"\nu = %g, v = %g", u, v);
            for(k = 1; k <= 2; ++k)
            {
                skipUntilWord(&buffer, ":");
                if (myfastScanf(buffer, " [%lf, %lf]", &u, &v) != 2 )
                {
                    [NSException raise: @"SyntaxError" 
                                format: @"while parsing textures coordinates"];
                }
                uv[k][0] = u;
                uv[k][1] = v;
            }
        }
        else
        {
            int k;
            hasTexture = NO;
            for(k = 0; k < 3; ++k)
            {
                uv[k][0] = 0.0;
                uv[k][1] = 0.0;
            }
        }
        
        BOOL smooth;
        
        skipUntilWord(&buffer, "Smooth");
        getWord(chaine, &buffer);
        if( strcmp(chaine, "true") == 0 )
            smooth = YES;
        else
            smooth = NO;
        [self addTriangleWithAIndex:A
                             BIndex:B
                             CIndex:C
                             normal:normal
                             smooth:smooth
                      materialIndex:materialIndex
                         hasTexture:hasTexture
                 textureCoordinates:uv];
        skipUntilWord(&buffer, "Texture");
        skipSpace(&buffer);
        if ( *buffer == '"' )
        {
            char *ptr;
            const char *buffers = buffer+1;
            getWord(chaine, &buffers);
            ptr = strchr(chaine, '"');
            NSAssert(ptr, @"No quote");
            *ptr = 0;
            if (i == 0)
            {
                NSString *textureName = [NSString stringWithUTF8String: chaine]; 
                
                if (textureName == nil)
                {
                    NSLog(@"chaine = %s", chaine);
                    abort();
                }
                hasText = YES;
                [self setTexture: [Texture textureWithFileName: textureName ]];
                if (!texture)
                    NSWarnMLog(@"Cannot load %@", textureName);
            }
            else
            {
                if ( !hasText )
                    [NSException raise: @"SyntaxError"
                                format: @"Texture expected for every faces"];
            }
        }
        else
        {
            getWord(chaine, &buffer);
            if ( strcmp(chaine, "None") != 0 )
            {
                fprintf(stderr, "chaine = <<\n%s\n>>\n", chaine);
                SyntaxError(@"None expected if no texture name found");
            }
            if( i == 0 )
            {
                hasText = NO;
                NSParameterAssert(texture == nil);
            }
            else
            {
                if ( hasText && !hasWarn)
                {
                    NSWarnMLog(@"Texture expected for every faces");
                    hasWarn = YES;
                }
            }          
        }
        skipToEndOfLine(&buffer);
    }
    
    
    if( myfastScanf(buffer, "materials: (%d)", &nmat) != 1 )
    {
        [NSException raise:@"SyntaxError" format: @"material expected"];
    }
    NSAssert(nmat <= MAXMATPERMESH, @"too many materials in the mesh");
    for ( i = 0; i < nmat; ++i )
    {
        Vect3D tmp;
        double r, g, b;
        double amb, sa, sb, sc, emit, specExp, specI;
        skipUntilWord(&buffer, "RGBColor:");
        if ( myfastScanf(buffer, " [%lf, %lf, %lf]", &r, &g, &b) != 3 )
        {
            [NSException raise:@"SyntaxError" format: @"RGB color expected"];
        }
        
        skipUntilWord(&buffer, "Ambience:");
        if ( myfastScanf(buffer, " %lf", &amb) != 1 )
        {
            SyntaxError(@"\"Ambience:\" expected");
        }
        
        skipUntilWord(&buffer, "Emit:");
        if ( myfastScanf(buffer, " %lf", &emit) != 1 )
        {
            SyntaxError(@"\"Emit:\" expected");
        }
        
        
        skipUntilWord(&buffer, "SpecExp:");
        if ( myfastScanf(buffer, " %lf", &specExp) != 1 )
        {
            SyntaxError(@"\"SpecExp:\" expected");
        }
        if(specExp>128.0){
            NSWarnLog(@"spec too big, over 128: %g", specExp);
            specExp = 128.0;
        }
        
        skipUntilWord(&buffer, "SpecVal:");
        if ( myfastScanf(buffer, " %lf", &specI) != 1 )
        {
            SyntaxError(@"\"SpecVal:\" expected");
        }
        
        skipUntilWord(&buffer, "SpecRGB:");
        if ( myfastScanf(buffer, " [%lf, %lf, %lf]", &sa, &sb, &sc) != 3 )
        {
            [NSException raise:@"SyntaxError" format: @"SpecRGB color expected"];
        }
        
        tmp = mkVect(r, g, b);
        Vect3D specCol = mkVect(sa,sb,sc);
        mulScalVect(&specCol,specI,&specCol);
        
        [self addMaterialWithColor:colOfVect(tmp)
                           ambiant:amb
                     specularColor:colOfVect(specCol)
                  specularExponent:specExp
                              emit:emit];
        
        skipUntilWord(&buffer, "Mirror");
        skipToEndOfLine(&buffer);
        NSDebugMLLog(@"Blender", @"\ntmp = %@, specCol = %@", 
                     stringOfVect2(tmp), stringOfVect2(specCol)
                     );
    }

    return buffer;
}
@end


@implementation ModelBlender
static id parseValue(const char *buffer)
{
    id  ret = nil;
    if(*buffer == '<'){
        buffer++;
        char *endType = strchr(buffer, '>');
        if(endType){
            int sizeType = endType - buffer;
            NSCParameterAssert(sizeType > 0);
            char type[50];
            NSCParameterAssert(sizeType < 50);
            memcpy(type,buffer,sizeType);
            type[sizeType] = '\0';
            NSLog(@"type = %s", type);
            buffer = endType+1;
            char *endValue = strchr(buffer,'\n');
            if(endValue){
                int length = endValue - buffer;

                NSString* valueAsString = 
                    [[[NSString alloc] initWithBytes:buffer
                                              length:length
                                            encoding:NSUTF8StringEncoding] autorelease];
                NSLog(@"got val = %@", valueAsString);
                if(strcmp(type, "BOOL") == 0){
                    ret = [NSNumber numberWithInt:[valueAsString intValue]];
                }
                else if(strcmp(type,"STRING") == 0){
                    ret = valueAsString;
                }
                else if(strcmp(type,"FLOAT") == 0){
                    ret = [NSNumber numberWithDouble:[valueAsString doubleValue]];
                }
                else if(strcmp(type,"INT") == 0){
                    ret = [NSNumber numberWithInt:[valueAsString intValue]];
                }
                else{
                    NSLog(@"unknown type %s", type); 
                }
            }
        }
    }
    if(nil == ret){
//        NSLog(@"can't parse %s", );
        SyntaxError(@"cannot parse value")
    }
    
    return ret;
}

- (const char *) parseProperties:(const char *)buffer
                 inDictionaryPtr:(NSMutableDictionary**) dicoPtr
{
    NSMutableDictionary* properties;
    if(dicoPtr){
        properties = *dicoPtr;
    }
    else{
        properties = nil;
    }
    while(strncmp(buffer,"END:Properties", 14) != 0){
        const char *startKey = buffer;
        char *mid = strchr(buffer,':');
        if(mid && mid - startKey > 0){
            buffer=mid+1;
            id val = parseValue(buffer);
            NSString* key = [[NSString alloc] initWithBytes:startKey
                                                     length:mid-startKey
                                                   encoding:NSUTF8StringEncoding];
            if(val){
                NSLog(@"%@ -> %@", key, val);
                if(nil == properties){
                    properties = [NSMutableDictionary dictionaryWithCapacity:3];
                }
                [properties setObject:val forKey:key];
            }
            else{
                NSLog(@"could not parse the value for the key %@", key);
            }
            [key release];
    
        }
        skipToEndOfLine(&buffer);        
    }
    
    if(dicoPtr){
        *dicoPtr = properties;
    }
    
    return buffer;
}

- (const char *) parseMesh: (const char *) buffer
                dictionary: (NSDictionary *) dico
{
    NSString *val;
    float scaleFactor;
    char chaine[50];
    const char *temp;
    int nvert, nface;
    GGMatrix4 mat;
    GGMMesh *mesh;
    
    scaleFactor = 1.0;
    if ((val = [dico objectForKey: @"Scale"]) != nil )
    {
        scaleFactor = [val doubleValue];
        if( scaleFactor <= 0 )
            NSWarnMLog(@"negative scale factor !!");
    }
    skipToEndOfLine(&buffer);
    if( myfastScanf(buffer, "# %s", chaine) != 1 )
    {
        [NSException raise:@"SyntaxError" format: @"name of mesh expected"];
        
    }
    skipUntilLineBegin(&buffer, "Transform");
    skipToEndOfLine(&buffer);
    
    
    parseMatrice(&buffer, &mat, scaleFactor);
    skipUntilLineBegin(&buffer, "Vertices");
    
    if( myfastScanf(buffer, "Vertices: (%d)", &nvert) != 1 )
    {
        [NSException raise:@"SyntaxError" format: @"parsing vertices numbers"];
    }
    
    skipToEndOfLine(&buffer);
    temp = buffer;
    skipUntilLineBegin(&temp, "Faces");
    
    if( myfastScanf(temp, "Faces: (%d)", &nface) != 1 )
    {
        [NSException raise:@"SyntaxError" format: @"parsing faces numbers"];
    }
    
    mesh = [GGMMesh meshWithName: [NSString stringWithUTF8String: chaine]
                   nVertex: nvert
                     nFace: nface];
    GGMNode* meshNode = [[GGMNode alloc] initWithMesh:mesh transform:&mat];
    
    if(nil == _objectByName){
        _objectByName = [NSMutableDictionary new];
    }
    [_objectByName setObject:meshNode forKey:[mesh name]];
    
    [tab addObject: meshNode];
    RELEASE(meshNode);  // now retained by tab
    buffer = [mesh parseMesh:buffer
              transformation:&mat
                       nvert:nvert
                       nface:nface];
    
    char parentName[51];
    if(myfastScanf(buffer, "parent: \"%50[^\"]", parentName) == 1){
        NSString* parentN = [[NSString alloc] initWithUTF8String:parentName];
        NSLog(@"got parent %@", parentN);
        [meshNode setParentName:parentN];
        [parentN release];
        skipToEndOfLine(&buffer);
    }
    else if(strcmp(buffer, "parent") == 0){
        NSLog(@"empty parent");
        skipToEndOfLine(&buffer);
    }
    else{
        NSLog(@"no parent at all");
    }
    
    if(strncmp(buffer, "BEGIN:Properties", 16) == 0){
        NSMutableDictionary* prop = nil;
        skipToEndOfLine(&buffer);

        buffer = [self parseProperties:buffer
                       inDictionaryPtr:&prop];
        [meshNode setProperties:prop];
    }
    skipUntilLineBegin(&buffer, "Mesh ends.");
    skipToEndOfLine(&buffer);
    return buffer;
}

- (void) calcBoundingBox
{
    NSEnumerator *en;
    GGMNode *unit = nil;
    Vect3D _max = { -1e20, -1e10, -1e20 };
    Vect3D _min = { 1e20, 1e10, 1e20 };
    GGReal scale = 1.0;
    
    min = _min;
    max = _max;
    en = [tab objectEnumerator];
    BoundingBox box;
    GGMNode* node;
    AABB_initWithMinMax(&box,min,max);
    while( (node = [en nextObject]) != nil )
    {
        GGMMesh *mesh = [node mesh];

        if ([[mesh name] isEqualToString: @"Unit"])
        {
            NSDebugMLLog(@"parseur", @"find Unit in file");
            unit = node;
        }
        else
            NSDebugMLLog(@"parseur", @"name mesh = %@", [mesh name]);
    }
    
    
    if (unit)
    {
        scale = [unit getUnit];
        NSLog(@"got unit %g", scale);
        [tab removeObjectIdenticalTo: unit];
    }
    else
        NSDebugMLLog(@"parseur", @"no unit in file");
    
    
    
    
    en = [tab objectEnumerator];
    
    while( (node = [en nextObject]) != nil )
    {
        if(scale!=1) [node applyScaleFactor:scale];
        [node improveBoundingBox:&box];
    }

    AABB_GetMinMax(&box, &min, &max);
    NSDebugMLLog(@"parseur", @"min = %@, max = %@", stringOfVect2(min),
                 stringOfVect2(max));
}

- (NSArray*) topLevelObjects
{
    [tab makeObjectsPerformSelector:@selector(removeAllSons)];
    NSEnumerator* objectEnum = [tab objectEnumerator];

    
    GGMNode* meshNode;
    NSMutableArray* result = [NSMutableArray arrayWithCapacity:[tab count]];
    while((meshNode = [objectEnum nextObject])!=nil){
        GGMNode* parentNode = nil;
        if([meshNode parentName]){
            parentNode = [_objectByName objectForKey:[meshNode parentName]];
            [parentNode addSubNode:meshNode];
        }
        
        if(!parentNode){
            //this is a top level object
            NSLog(@"%@ is toplevel", meshNode);
            [result addObject:meshNode];
        }
    }
    
    return result;
}

- (void) parseData: (NSString *) name
        dictionary: (NSDictionary *)dico
{
    NSData *data = [NSData dataWithContentsOfFile:name];
    
    if( data == nil )
    {
        [NSException raise: @"FileNotFound" format: @"%@ not found", name];
    }
    else
    {
        const char *buffer = [data bytes];
        BOOL cont = YES;
        
        
        while(cont)
        {
            NS_DURING
                skipUntilLineBegin(&buffer, "#mesh");
            NS_HANDLER
                cont = NO;
            NS_ENDHANDLER
            if ( cont ){
                buffer = [self parseMesh: buffer
                              dictionary: dico];
            }
        }
    }
    [self calcBoundingBox];
}

- (void) dealloc
{
    RELEASE(tab);
    RELEASE(_objectByName);
    
    [super dealloc];
}

- (id) init
{
    tab = [[NSMutableArray alloc] init];
    
    addToCheck(self);
    return self;
}

- (id) initWithNode:(GGMNode*)node
{
    self = [self init];
    if(self){
        NSAssert(tab, @"badly initialized");
        [tab addObject:node];
        [self calcBoundingBox];
    }
    
    return self;
}

- (id) initWithNodes:(NSArray*) nodes
{
    self = [self init];
    [tab addObjectsFromArray:nodes];
    [self calcBoundingBox];
    
    return self;
}

- (ModelBlender*) transformModel:(GGMatrix4*)pmat
{
    NSMutableArray* newNodes = [[NSMutableArray alloc] initWithCapacity:[tab count]];
    int i, n = [tab count];
    
    for(i = 0; i < n; ++i){
        GGMNode* node = [tab objectAtIndex:i];
        [newNodes addObject:[node meshNodeWithTransformation:pmat]];
    }
    
    ModelBlender* ret = [[ModelBlender alloc] initWithNodes:newNodes];
    [newNodes release];
    return [ret autorelease];
}


- initWithFile: (NSString *) name
    dictionary: (NSDictionary *)dico
{
    self = [self init];
    if(nil == self){
        return self;
    }

    tab = [NSMutableArray new];
    NSWarnMLog(@"Parsing of %@", name);
    
    
    NS_DURING
    {
        [self parseData: name
             dictionary: dico];
    }
    NS_HANDLER
        NSWarnMLog(@"ca chie grave avec %@ ligne %d\nname:%@ reason:\n%@", name, numLigne,
                   [localException name],
                   [localException reason]);
        RELEASE(self);
        NS_VALUERETURN(nil, id);
    NS_ENDHANDLER
    
    return self;
}

- (id) initWithFileAtPath:(NSString*)path
{
    return [self initWithFile:path
                   dictionary:nil];
}

+ modelWithFileName: (NSString *)str
         dictionary: (NSDictionary *)dico
{
    ModelBlender *model;
    model = [self alloc];
    model = [model initWithFile: str
                     dictionary: dico];
    return AUTORELEASE(model);
}


+ modelWithFileName: (NSString *) name
{
    return [self modelWithFileName: name
                        dictionary: nil];
}

- (NSArray*) nodes;
{
    return tab;
}

- (void) addMeshNode:(GGMNode*)node
            withName:(NSString*)name
{
    [tab addObject:node];
    if(name){
        if(nil == _objectByName){
            _objectByName = [[NSMutableDictionary alloc] initWithCapacity:5];
        }
        [_objectByName setObject:node forKey:name];
    }
}

- (void) finalizeAfterLoading
{
    [self calcBoundingBox];
}

- (void) rasterise: (Camera *)cam
{
    int nMesh = [tab count];
    
    if(nMesh > 0)
    {
        GGMNode *nodes[nMesh];
        
        int i;
        
        [tab getObjects: nodes];
        for(i = 0; i < nMesh; ++i){
            [nodes[i] rasterise:cam];
        }
        
    }
    GLCHECK;
    
}

- (Vect3D *) boundingBox: (Vect3D *)pax
{
    *pax = max;
    return &min;
}

- (int) getTriangles: (Triangle **) pt
{
    int n = [tab count];
    
    if( n > 0 )
    {
        int i;
        int index;
        int ntr;
        GGMNode *nodes[n];
        
        [tab getObjects: nodes];
        ntr = 0;
        for( i = 0; i < n; ++i )
        {
            GGMMesh* mesh = [nodes[i] mesh];
            ntr += [mesh numberOfFaces];
        }
        
        
        *pt = malloc(ntr *  sizeof(Triangle));
        index = 0;
        
        for(i = 0; i < n; ++i)
        {
            [nodes[i] getTriangleIn:*pt + index];
            GGMMesh *mesh = [nodes[i] mesh];
            index += [mesh numberOfFaces];
            NSParameterAssert(index < ntr);            
            
        }
        return ntr;
    }
    else
        return 0;
}
@end
