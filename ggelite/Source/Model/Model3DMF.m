/*
 *  Model3DS.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#import <Foundation/NSZone.h>
#import <Foundation/NSException.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSDebug.h>
#import "GGModel.h"
#import "Model3DMF.h"
#import "Metaobj.h"
#import "utile.h"
#import "GGFont.h"
#import "Texture.h"
#import "Preference.h"



// static inline void improve(Vect3D *min, Vect3D *max, Lib3dsVector v)
// {

//   if( v[0] < min->x )
//     min->x = v[0];
//   if( v[0] > max->x )
//     max->x = v[0];

//   if( v[1] < min->y )
//     min->y = v[1];
//   if( v[1] > max->y )
//     max->y = v[1];

//   if( v[2] < min->z )
//     min->z = v[2];
//   if( v[2] > max->z )
//     max->z = v[2];

// }



@interface Model3DMF (GGPrivate)
- (void) calcBoundingBox;
@end

@implementation Model3DMF
unsigned short readHex(char c)
{
  switch(c)
    {
    case '0': return 0;
    case '1': return 1;
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    case 'A': return 0xa;
    case 'B': return 0xb;
    case 'C': return 0xc;
    case 'D': return 0xd;
    case 'E': return 0xe;
    case 'F': return 0xf;
    default: 
      abort();
    }
}

unsigned char readchar(const char *ptr)
{
  return readHex(*(ptr+1)) | (readHex(*(ptr)) << 4);
}

unsigned short readshort(const char *ptr)
{
  unsigned char c1, c2;
  c1 = readchar(ptr);
  c2 = readchar(ptr+2);
  return (c1 << 8) | c2;
}

unsigned short repack(unsigned short val)
{
  unsigned short r, g, b;
  r = (val >> 10) & 0x1f;
  g = (val >> 5) & 0x1f;
  b = (val ) & 0x1f;

  return (r << 11) | (g << 6) | b;
}

static void readf(unsigned short *ptr, int dim, const char **scan)
{
  int i, j;
  *scan += 2;

  for( j = 0; j < dim; ++ j)
    {
      for(i = 0; i < dim; ++ i)
	{
	  if( **scan == '\n' || **scan == '\r')
	    {
	      skipToEndOfLine(scan);
	      *scan += 2;
	    }
	  ptr[(dim-1-j) * dim + i] = repack(readshort(*scan));
	  *scan += 4;
	}
    }
  //  fprintf(stderr, "%d lignes lue\n", nl);
}



- (void) parse3DMF: (NSString *)name
{
  NSData *data = [NSData dataWithContentsOfFile: name];

  if( data == nil )
    {
      [NSException raise: @"FileNotFound" format: @""];
    }
  else
    {
      const char *buffer = [data bytes];
      const char *temp;
      int res, i;
      int bidon;

      skipUntilWord(&buffer, "Container");
      skipUntilWord(&buffer, "TriMesh");

      skipSpace(&buffer);

      NSParameterAssert(*buffer == '(');
      skipToEndOfLine(&buffer);

      skipSpace(&buffer);

      res = sscanf(buffer, "%d %d %d %d %d %d", &nface, &bidon, &bidon, &bidon,
		   &nvert, &bidon);
      
      NSAssert1(res == 6, @"can't parse nvert, nface, res = %d", res);
      NSDebugMLLog(@"parseur", @"nvert = %d nface = %d", nvert, nface);

      vertices = NSZoneMalloc([self zone], sizeof(VertexDataTex)*nvert);
      faces = NSZoneMalloc([self zone], sizeof(Face)*nface);

      skipToEndOfLine(&buffer);
      
      skipUntilWord(&buffer, "triangles");

      skipToEndOfLine(&buffer);

      for( i = 0; i < nface; ++i )
	{
	  int t1, t2, t3;
	  if( sscanf(buffer, "%d %d %d", &t1, &t2, &t3) < 3 )
	    {
	      NSLog(@"merdouille %d", i);
	      [NSException raise:@"SyntaxError" format: @"face expected"];
	    }
	  faces[i].colorIndex = 0;
	  faces[i].A = t1;
	  faces[i].B = t2;
	  faces[i].C = t3;
	  skipToEndOfLine(&buffer);
	  NSAssert(0<= faces[i].A && faces[i].A < nvert, 
		   @"vertex out of range");
	  NSAssert(0<= faces[i].B && faces[i].B < nvert, 
		   @"vertex out of range");
	  NSAssert(0<= faces[i].C && faces[i].C < nvert, 
		   @"vertex out of range");
	}

      skipUntilWord(&buffer, "vertices");
      skipToEndOfLine(&buffer);


      for( i = 0; i < nvert; ++i )
	{
	  double t1, t2, t3;
	  if( sscanf(buffer, "%lf %lf %lf", &t1, &t2, &t3) < 3 )
	    {
	      NSLog(@"chie level %d, %s", i, buffer);
	      [NSException raise:@"SyntaxError" format: @"vertex expected"];
	    }
	  else
	    {
	      initVect(&vertices[i].pos, t1, t2, t3);
	    }

	  skipToEndOfLine(&buffer);

	}
      skipUntilWord(&buffer, "triangle");	
      skipUntilWord(&buffer, "normals");
      skipToEndOfLine(&buffer);
      for( i = 0; i < nface; ++i)
	{
	  double t1, t2, t3;
	  if( sscanf(buffer, "%lf %lf %lf", &t1, &t2, &t3) < 3 )
	    {
	      NSLog(@"chie level %d, %s", i, buffer);
	      [NSException raise:@"SyntaxError" format: @"face expected"];
	    }
	  else
	    {
	      initVect(&faces[i].normal, t1, t2, t3);
	      normaliseVect(&faces[i].normal);
	      //	      NSLog(@"norm[%d] = %@", i, stringOfVect2(faces[i].normal));
	      skipToEndOfLine(&buffer);

	    }
	}

      temp = buffer;
      NS_DURING
	{
	  skipUntilWord(&buffer, "vertex");
	  norm_per_vertex = YES;

	  skipToEndOfLine(&buffer);
	  for( i = 0; i < nvert; ++i)
	    {
	      double t1, t2, t3;
	      if( sscanf(buffer, "%lf %lf %lf", &t1, &t2, &t3) < 3 )
		{
		  NSLog(@"chie level %d, %s", i, buffer);
		  [NSException raise:@"SyntaxError" format: @"face expected"];
		}
	      else
		{
		  initVect(&vertices[i].normal, t1, t2, t3);
		  normaliseVect(&vertices[i].normal);
		  skipToEndOfLine(&buffer);
		  //	      NSLog(@"norm[%d] = %@", i, stringOfVect2(vertices[i].normal));
		}
	    }
	}
      NS_HANDLER
	{
	  norm_per_vertex = NO;
	  buffer = temp;
	  NSDebugMLLog(@"Model3DMF", @"No per-vertex normal");
	}
      NS_ENDHANDLER

     	

      skipUntilWord(&buffer, "coordinates");
      NSDebugMLLog(@"Model3DMF", @"parsing uv coordinated");

      skipToEndOfLine(&buffer);
      for( i = 0; i < nvert; ++i)
	{
	  double t1, t2;
	  if( sscanf(buffer, "%lf %lf", &t1, &t2) < 2 )
	    {
	      NSLog(@"chie level %d, %s", i, buffer);
	      [NSException raise:@"SyntaxError" format: @"uv expected"];
	    }
	  else
	    {
	      vertices[i].u = t1;
	      vertices[i].v = t2;
	      skipToEndOfLine(&buffer);
	      //	      NSLog(@"norm[%d] = %@", i, stringOfVect2(vertices[i].normal));
	    }
	}

      skipUntilWord(&buffer, "PixmapTexture");
      skipToEndOfLine(&buffer);
      {
	unsigned short *bla;
	int width, height;
	res = sscanf(buffer, "%d %d", &width, &height);

	NSAssert(res == 2, @"Cannot parse texture dimension");

	NSAssert(width == height && width > 0, @"bad dimension");
	bla = malloc(width * height * sizeof(unsigned short));
	skipToEndOfLine(&buffer);

	readf(bla, width, &buffer);

	glGenTextures(1,&idTexture);
	NSDebugMLLog(@"Model3DMF", @"width = %d, t = %d", width, idTexture);
	glBindTexture(GL_TEXTURE_2D, idTexture);

	if( thePref.GLMipmap )
	  {
	    GLenum gluerr;

	    if((gluerr=gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, width, 
					 height, GL_RGB, 
					 GL_UNSIGNED_SHORT_5_6_5,
					 bla))) 
	      {
		NSWarnMLog(@"GLULib%s\n",gluErrorString(gluerr));
		exit(-1);
	      }

	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			    GL_LINEAR_MIPMAP_LINEAR);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	  }
	else
	  {
	    glTexImage2D(GL_TEXTURE_2D, 		// target
			 0,			// mipmap level
			 GL_RGB,		// Format interne (je panne rien !!)
			 width, 			// largeur
			 height, 			// hauteur
			 0,			// bord ?
			 GL_RGB,		// Format de texture (type)
			 GL_UNSIGNED_SHORT_5_6_5, 	// encoding
			 // 		     GL_UNSIGNED_BYTE, 	// encoding
			 bla			// data
			 );
	    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
			    GL_NEAREST);
	    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
	  }

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);


	free(bla);

      }
    }
}

- initWithFile: (NSString *)str
    dictionary: (NSDictionary *)dico
{
  NS_DURING
    {
      numLigne = 0;
      [self parse3DMF: str];
    }
  NS_HANDLER
    {
      NSWarnMLog(@"ca chie grave avec %@ ligne %d :%@ %@", str, numLigne,
		 [localException name],
		 [localException reason]);
      RELEASE(self);
      return nil;
    }
  NS_ENDHANDLER

  initVect(&max, -100, -100, -100);
  initVect(&min, 100, 100, 100);
  [self calcBoundingBox];
  //  compute_box(&min, &max, file);
  NSDebugMLLog(@"TDS", @"min = %@, max %@", stringOfVect(&min),
	       stringOfVect(&max));

  addToCheck(self);
  return self;
}

- (void) rasterise
{
  int i;
  //  glDisable(GL_CULL_FACE);
  glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT);
  glShadeModel(GL_SMOOTH);
  glDisable(GL_CULL_FACE);
  glEnable(GL_NORMALIZE);
  glEnable(GL_TEXTURE_2D);

  //  glBindTexture(GL_TEXTURE_2D, [[[GGFont fixedFont] texture] idTexture]);
   glBindTexture(GL_TEXTURE_2D, idTexture);
   //  glBindTexture(GL_TEXTURE_2D, [myTexture idTexture]);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);

  glColor3f(1, 1, 1);

  if( norm_per_vertex )
    {
      glShadeModel(GL_SMOOTH);
      glBegin(GL_TRIANGLES);
#define Vertex(i) do{              \
    Vect3D norm = vertices[i].normal;                            \
    glNormal3f(norm.x, norm.y, norm.z);          \
    glTexCoord2f(vertices[i].u, vertices[i].v);      \
    glVertex3v(&(vertices[i].pos));             \
    }while(0)
      for (i = 0; i < nface; ++i)
	{
	  Face theFace = faces[i];
	  //      glNormal3v((GGReal *)(&theFace.normal));
	  //      NSLog(@"norm %d = %@", i , stringOfVect2(theFace.normal));
	  //      glNormal3f(vrnd()-.5, vrnd()-.5, vrnd()-.5);
	  Vertex(theFace.A);
	  Vertex(theFace.B);
	  Vertex(theFace.C);
	}
#undef Vertex
      glEnd();
    }
  else
    {
      glShadeModel(GL_FLAT);
      glBegin(GL_TRIANGLES);
#define Vertex(i) do{              \
    glTexCoord2f(vertices[i].u, vertices[i].v);      \
    glVertex3v(&(vertices[i].pos));             \
    }while(0)

      for (i = 0; i < nface; ++i)
	{
	  Face theFace = faces[i];
	  glNormal3v((GGReal *)(&theFace.normal));
	  Vertex(theFace.A);
	  Vertex(theFace.B);
	  Vertex(theFace.C);
	}
#undef Vertex
      glEnd();
    }
  glPopAttrib();
  //  glEnable(GL_CULL_FACE);
}

- (void) calcBoundingBox
{
  int i;
  Vect3D _max = { -1e20, -1e10, -1e20 };
  Vect3D _min = { 1e20, 1e10, 1e20 };
		 
  min = _min;
  max = _max;

  for( i = 0; i < nvert; ++i )
    {
      Vect3D pos = vertices[i].pos;

      min.x = (min.x < pos.x ? min.x : pos.x);
      min.y = (min.y < pos.y ? min.y : pos.y);
      min.z = (min.z < pos.z ? min.z : pos.z);

      max.x = (max.x > pos.x ? max.x : pos.x);
      max.y = (max.y > pos.y ? max.y : pos.y);
      max.z = (max.z > pos.z ? max.z : pos.z);
    }

  NSDebugMLLog(@"parseur", @"min = %@, max = %@", stringOfVect2(min),
	       stringOfVect2(max));
}


- (void) dealloc
{
  if( idTexture > 0 )
    glDeleteTextures(1, &idTexture);
  if( vertices != NULL )
    NSZoneFree([self zone], vertices);
  if( faces != NULL )
    NSZoneFree([self zone], faces);

  [super dealloc];
}

- (Vect3D *) boundingBox: (Vect3D *)pax
{
  *pax = max;
  return &min;
}


+ modelWithFileName: (NSString *)str
	 dictionary: (NSDictionary *)dico
{
  Model3DMF *model;
  model = [self alloc];
  model = [model initWithFile: str
		 dictionary: dico];
  return AUTORELEASE(model);
}


+ modelWithFileName: (NSString *)str
{
  return [self modelWithFileName: str
	       dictionary: nil];
}

@end
