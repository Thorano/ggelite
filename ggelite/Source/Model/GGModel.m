//
//  GGModel.m
//  elite
//
//  Created by Frederic De Jaeger on 01/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGModel.h"
#import "parseASC.h"
#import "Resource.h"
#import "Model3DS.h"
#import "Model3DMF.h"
#import "ModelBlend.h"
#import "GGModelX3D.h"

@implementation GGModel
static Class model3DSClass;

+ (void) load3DSBundle
{
#ifdef HAVE_LIB3DS
    model3DSClass = [Model3DS class];
#else
    NSBundle *theBundle;
    
    theBundle = [NSBundle bundleWithPath: [[[Resource resourceManager] bundlePath]
					  stringByAppendingPathComponent: 
        @"Model3DS.bundle"]];
    model3DSClass = [theBundle classNamed: @"Model3DS"];
#endif
}

+ (ModelBlender*)modelWithName: (NSString *) name
                               scale: (float)scaleFactor
{
    NSDictionary* properties = [[NSDictionary alloc] initWithObjectsAndKeys:
        [NSNumber numberWithFloat:scaleFactor], @"Scale",
        nil];
    ModelBlender* model = [self modelWithName:name 
                                         properties:properties];
    [properties release];
    
    return model;
}

+ (ModelBlender*)modelWithPath: (NSString *) path
                      properties: (NSDictionary *)dico
{
    NSString *ext;
    ModelBlender* m = nil;
    if(nil == dico){
        m = [[Resource resourceManager] resourceWithName: path];
    }
    if( m != nil )
    {
        NSDebugMLLog(@"Model", @"find a cached version of %@", path);
        return m;
    }
    NS_DURING
        numLigne = 0;
        ext = [path pathExtension];
        if( [ext isEqualToString: @"asc"] )
        {
            m = [[[ModelASC alloc] initWithPath:path
                                     properties:dico] autorelease];
        }
        else if ( [ext  isEqualToString: @"3DMF"] ) 
        {
            m = [Model3DMF modelWithFileName: path
                              dictionary: dico];
        }
        else if ( [ext  isEqualToString: @"3ds"] ||  
                  [ext  isEqualToString: @"3DS"] )
        {
            if( model3DSClass == nil )
            {
                [self load3DSBundle];
                if( model3DSClass == nil )
                {
                    NSWarnMLog(@"Unable to load 3DS Bundle");
                    return nil;
                }
                NSDebugMLLog(@"Bundle", @"Model3ds succesfully loaded");
            }
            m = [model3DSClass modelWith3DStudioFile: path
                                        withDictionary: dico];
        }
        else if ( [ext isEqualToString: @"bld"] )
        {
            m = [ModelBlender modelWithFileName: path
                                     dictionary: dico];
        }
        else if([ext isEqualToString:@"gg3d"])
        {
            m = [GGModelX3D modelWithPath:path
                               properties:dico];
        }
        else
        {
            NSWarnMLog(@"unknow file format %@", path);
            return nil;
        }
    NS_HANDLER
        NSWarnMLog(@"ca chie grave avec %@ ligne %d :\n%@", path, numLigne,
                   localException);
        NS_VALUERETURN(nil, id);
    NS_ENDHANDLER
    if ( m != nil && nil == dico)
    {
        // don't store in cache if non trivial properties.
        [[Resource resourceManager] addResource: m
                                       withName: path];
    }
    return m;
}

+ (ModelBlender*)modelWithName: (NSString *) name
                    properties: (NSDictionary *)dico
{
    NSString *path;
    path = [[Resource resourceManager] pathForModel:  name];
    return [self modelWithPath:path
                    properties:dico];
}

+ (ModelBlender*)modelWithName: (NSString *) name
{
    return [self modelWithName: name
                    properties: nil];
}

@end
