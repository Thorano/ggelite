/*
 *  Model3DS.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSDebug.h>
#import <lib3ds/file.h>
#import <lib3ds/chunk.h>
#import <lib3ds/node.h>
#import <lib3ds/mesh.h>
#import <lib3ds/vector.h>
#import <lib3ds/quat.h>
#import <lib3ds/matrix.h>
#import <lib3ds/material.h>
#import <lib3ds/camera.h>
#import "Model3DS.h"
#import "Metaobj.h"
#import "utile.h"


static void
rasteriseNodeAndCompile(Lib3dsFile *file, Lib3dsNode *node)
{
    Lib3dsNode *subNode;
    for (subNode=node->childs; subNode!=0; subNode=subNode->next) {
        rasteriseNodeAndCompile(file, subNode);
    }
    
    if (!node->user.d) {
        float s[1];
        unsigned p;
        Lib3dsMatrix invMeshMatrix;
        Lib3dsMesh *mesh=lib3ds_file_mesh_by_name(file, node->name);
        ASSERT(mesh);
        if (!mesh) {
            return;
        }
        
        node->user.d=glGenLists(1);
        glNewList(node->user.d, GL_COMPILE);
        
        lib3ds_matrix_copy(invMeshMatrix, mesh->matrix);
        lib3ds_matrix_inv(invMeshMatrix);
        
        for (p=0; p<mesh->faces; ++p) {
            Lib3dsFace f=mesh->faceL[p];
            Lib3dsMaterial *mat=0;
            if (f.material[0]) {
                mat=lib3ds_file_material_by_name(file, f.material);
            }
            if (mat) {
                s[0]=1.0;
                glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient);
                glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
                glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
                glMaterialf(GL_FRONT, GL_SHININESS, 11.0-0.1*mat->shininess);
            }
            else {
                Lib3dsRgba a={0.2, 0.2, 0.2, 1.0};
                Lib3dsRgba d={0.8, 0.8, 0.8, 1.0};
                Lib3dsRgba s={0.0, 0.0, 0.0, 1.0};
                glMaterialfv(GL_FRONT, GL_AMBIENT, a);
                glMaterialfv(GL_FRONT, GL_DIFFUSE, d);
                glMaterialfv(GL_FRONT, GL_SPECULAR, s);
                glMaterialf(GL_FRONT, GL_SHININESS, 0.0);
            }
            {
                int i;
                Lib3dsVector v[3];
                for (i=0; i<3; ++i) {
                    lib3ds_vector_transform(v[i], invMeshMatrix, mesh->pointL[f.points[i]].pos);
                }
                glBegin(GL_TRIANGLES);
                glNormal3fv(f.normal);
                glVertex3fv(v[0]);
                glVertex3fv(v[1]);
                glVertex3fv(v[2]);
                glEnd();
            }
        }
        
        glEndList();
    }
    
    if (node->user.d) {
        Lib3dsObjectData *d=&node->data.object;
        glPushMatrix();
        glMultMatrixf(&node->matrix[0][0]);
        glTranslatef(-d->pivot[0], -d->pivot[1], -d->pivot[2]);
        glCallList(node->user.d);
        glPopMatrix();
    }
}

static void
rasteriseNode(Lib3dsFile *file, Lib3dsNode *node)
{
    Lib3dsNode *subNode;
    for (subNode=node->childs; subNode!=0; subNode=subNode->next) {
        rasteriseNode(file, subNode);
    }
    
    {      float s[1];
        unsigned p;
        Lib3dsMatrix invMeshMatrix;
        Lib3dsMesh *mesh=lib3ds_file_mesh_by_name(file, node->name);
        ASSERT(mesh);
        if (!mesh) {
            return;
        }
        
        
        lib3ds_matrix_copy(invMeshMatrix, mesh->matrix);
        lib3ds_matrix_inv(invMeshMatrix);
        
        for (p=0; p<mesh->faces; ++p) {
            Lib3dsFace f=mesh->faceL[p];
            Lib3dsMaterial *mat=0;
            if (f.material[0]) {
                mat=lib3ds_file_material_by_name(file, f.material);
            }
            if (mat) {
                s[0]=1.0;
                glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient);
                glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
                glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
                glMaterialf(GL_FRONT, GL_SHININESS, 11.0-0.1*mat->shininess);
            }
            else {
                Lib3dsRgba a={0.2, 0.2, 0.2, 1.0};
                Lib3dsRgba d={0.8, 0.8, 0.8, 1.0};
                Lib3dsRgba s={0.0, 0.0, 0.0, 1.0};
                glMaterialfv(GL_FRONT, GL_AMBIENT, a);
                glMaterialfv(GL_FRONT, GL_DIFFUSE, d);
                glMaterialfv(GL_FRONT, GL_SPECULAR, s);
                glMaterialf(GL_FRONT, GL_SHININESS, 0.0);
            }
            {
                int i;
                Lib3dsVector v[3],n;
                for (i=0; i<3; ++i) {
                    lib3ds_vector_transform(v[i], invMeshMatrix, mesh->pointL[f.points[i]].pos);
                }	
                lib3ds_vector_transform(n, invMeshMatrix, f.normal);
                glBegin(GL_TRIANGLES);
                glNormal3fv(f.normal);
                //	    glNormal3fv(n);
                glVertex3fv(v[0]);
                glVertex3fv(v[1]);
                glVertex3fv(v[2]);
                glEnd();
            }
        }
        
    }
}

static inline void improve(Vect3D *min, Vect3D *max, Lib3dsVector v)
{
    
    if( v[0] < min->x )
        min->x = v[0];
    if( v[0] > max->x )
        max->x = v[0];
    
    if( v[1] < min->y )
        min->y = v[1];
    if( v[1] > max->y )
        max->y = v[1];
    
    if( v[2] < min->z )
        min->z = v[2];
    if( v[2] > max->z )
        max->z = v[2];
    
}

static void
bouding_box(Vect3D *min, Vect3D *max, Lib3dsFile *file, Lib3dsNode *node)
{
    Lib3dsNode *subNode;
    for (subNode=node->childs; subNode!=0; subNode=subNode->next) {
        bouding_box(min, max, file, subNode);
    }
    
    {      
        unsigned p;
        Lib3dsMatrix invMeshMatrix;
        Lib3dsMesh *mesh=lib3ds_file_mesh_by_name(file, node->name);
        ASSERT(mesh);
        if (!mesh) {
            return;
        }
        
        
        lib3ds_matrix_copy(invMeshMatrix, mesh->matrix);
        lib3ds_matrix_inv(invMeshMatrix);
        
        for (p=0; p<mesh->faces; ++p) {
            Lib3dsFace f=mesh->faceL[p];
            {
                int i;
                Lib3dsVector v[3];
                for (i=0; i<3; ++i) {
                    lib3ds_vector_transform(v[i], invMeshMatrix, mesh->pointL[f.points[i]].pos);
                    improve(min, max, v[i]);
                }	
            }
        }
    }	
}

static void compute_box(Vect3D *min, Vect3D *max, Lib3dsFile *file)
{
    Lib3dsNode *p;
    
    for (p=file->nodes; p!=0; p=p->next) {
        bouding_box(min, max, file, p);
    }
}


static void destroy(void *_f)
{
    Lib3dsFile *file;
    file = (Lib3dsFile *)_f;
    lib3ds_file_free(file);
}

static void rasterise(void *_f)
{
    Lib3dsFile *file;
    Lib3dsNode *p;
    file = (Lib3dsFile *)_f;
    
    glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT);
    glDisable(GL_COLOR_MATERIAL);
    glDisable(GL_CULL_FACE);
    glEnable(GL_NORMALIZE);
    for (p=file->nodes; p!=0; p=p->next) {
        rasteriseNode(file, p);
    }
    glPopAttrib();
}


static void * parse3ds(const char *filename)
{
    Lib3dsFile *file;
    file=lib3ds_file_load(filename);
    if (!file) {
        return NULL;
    }
    
    return(void*)file;
}


@implementation Model3DS
- initWith3DStudioFile: (NSString *)str
        withDictionary: (NSDictionary *)dico
{
    NSString *val;
    file = parse3ds([str cString]);
    if( file == NULL )
    {
        NSWarnMLog(@"Unable to load 3DS file %@", str);
        RELEASE(self);
        return nil;
    }
    initVect(&max, -100, -100, -100);
    initVect(&min, 100, 100, 100);
    compute_box(&min, &max, file);
    NSDebugMLLog(@"TDS", @"min = %@, max %@", stringOfVect(&min),
                 stringOfVect(&max));
    scale = 1;
    if( dico != nil  && (val = [dico objectForKey:@"Scale"]) != nil )
    {
        scale = [val floatValue];
    }
    
    
    if( dico != nil  && (val = [dico objectForKey:@"Swap"]) != nil )
    {
        swap = [val boolValue];
        if( swap )
        {
            GGMatrix4 val = { {0, 0, 0}, 0, 
            {0, 0, 0}, 0, 
            {0, 0, 0}, 0, 
            {0, 0, 0}, 1};
            pmat pp = (pmat) &val;
            
            pp[0][1] = pp[1][2] = pp[2][0] = 1;
            
            mat = val;
            
        }
    }
    
    addToCheck(self);
    return self;
}

- (void) dealloc
{
    if( file )
        destroy(file);
    [super dealloc];
}

- (void) rasterise
{
    if( swap )
        glMultMatrix(&mat);
    if( scale != 1 )
        glScalef(scale, scale, scale);
    rasterise(file);
}

- (Vect3D *) boundingBox: (Vect3D *)pax
{
    *pax = max;
    return &min;
}


+ modelWith3DStudioFile: (NSString *)str
         withDictionary: (NSDictionary *)dico
{
    Model3DS *model;
    model = [self alloc];
    model = [model initWith3DStudioFile: str
                         withDictionary: dico];
    return AUTORELEASE(model);
}

@end
