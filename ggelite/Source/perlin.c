/* Coherent noise function over 1, 2 or 3 dimensions */
/* (copyright Ken Perlin) */

#import <stdlib.h>
#import <stdio.h>
#import <math.h>
#import <assert.h>
#import <limits.h>
#import <float.h>
#import "perlin.h"

#define B 0x100
#define BM 0xff
#define N (INT_MAX)
#define NP 12   /* 2^N */
#define NM 0xfff

#define s_curve(t) ( t * t * (3. - 2. * t) )
#define ds(t) (6*(t)*(1-(t)))
#define lerp(t, a, b) ( a + t * (b - a) )
#define setup(i,b0,b1,r0,r1)\
        r0 = ggmodf(vec[i],&t);\
        t += INT_MAX;\
        b0 = ((unsigned int)t) & BM;\
        b1 = (b0+1) & BM;\
        r1 = r0 - 1.;
#define at2(rx,ry) ( rx * q[0] + ry * q[1] )
#define at3(rx,ry,rz) ( rx * q[0] + ry * q[1] + rz * q[2] )

static int p[B + B + 2];
static double g3[B + B + 2][3];
static double g2[B + B + 2][2];
static double g1[B + B + 2];
static int start = 1;

static inline double ggmodf(double val, double *pintegerPart)
{
  double rem, integerPart;
  rem = modf((double)val, &integerPart);
  if( rem < 0 )
    {
      *pintegerPart = integerPart-1;
      return rem+1;
    }
  else
    {
      *pintegerPart = integerPart;
      return rem;
    }
}

static void normalize2(double v[2])
{
   double s;

   s = sqrt(v[0] * v[0] + v[1] * v[1]);
   v[0] = v[0] / s;
   v[1] = v[1] / s;
}

static void normalize3(double v[3])
{
   double s;

   s = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
   v[0] = v[0] / s;
   v[1] = v[1] / s;
   v[2] = v[2] / s;
}


static void init(void)
{
   int i, j, k;

   for (i = 0 ; i < B ; i++) {
      p[i] = i;
      g1[i] = (double)((random() % (B + B)) - B) / B;

      for (j = 0 ; j < 2 ; j++)
         g2[i][j] = (double)((random() % (B + B)) - B) / B;
      normalize2(g2[i]);

      for (j = 0 ; j < 3 ; j++)
         g3[i][j] = (double)((random() % (B + B)) - B) / B;
      normalize3(g3[i]);
   }

   while (--i) {
      k = p[i];
      p[i] = p[j = random() % B];
      p[j] = k;
   }

   for (i = 0 ; i < B + 2 ; i++) {
      p[B + i] = p[i];
      g1[B + i] = g1[i];
      for (j = 0 ; j < 2 ; j++)
         g2[B + i][j] = g2[i][j];
      for (j = 0 ; j < 3 ; j++)
         g3[B + i][j] = g3[i][j];
   }
}


double noise1(double arg)
{
   int bx0, bx1;
   double rx0, rx1, sx, t, u, v, vec[1];

   vec[0] = arg;
   if (start) {
      start = 0;
      init();
   }

   setup(0,bx0,bx1,rx0,rx1);

   sx = s_curve(rx0);
   u = rx0 * g1[ p[ bx0 ] ];
   v = rx1 * g1[ p[ bx1 ] ];

   return(lerp(sx, u, v));
}

double noise1Per(double arg, int periode)
{
   int bx0, bx1;
   double rx0, rx1, sx, t, u, v, vec[1];

   vec[0] = arg;
   if (start) {
      start = 0;
      init();
   }

   setup(0,bx0,bx1,rx0,rx1);

   bx0 = bx0 % periode;
   bx0 = (bx0 < 0 ? bx0+periode : bx0);
   bx1 = bx1 % periode;
   bx1 = (bx1 < 0 ? bx1+periode : bx1);

   sx = s_curve(rx0);
   u = rx0 * g1[ p[ bx0 ] ];
   v = rx1 * g1[ p[ bx1 ] ];

   return(lerp(sx, u, v));
}

double noise2(double vec[2])
{
   int bx0, bx1, by0, by1, b00, b10, b01, b11;
   double rx0, rx1, ry0, ry1, *q, sx, sy, a, b, t, u, v;
   int i, j;

   if (start) {
      start = 0;
      init();
   }

   setup(0, bx0,bx1, rx0,rx1);
   setup(1, by0,by1, ry0,ry1);

   i = p[ bx0 ];
   j = p[ bx1 ];

   b00 = p[ i + by0 ];
   b10 = p[ j + by0 ];
   b01 = p[ i + by1 ];
   b11 = p[ j + by1 ];

   sx = s_curve(rx0);
   sy = s_curve(ry0);

   q = g2[ b00 ] ; u = at2(rx0,ry0);
   q = g2[ b10 ] ; v = at2(rx1,ry0);
   a = lerp(sx, u, v);

   q = g2[ b01 ] ; u = at2(rx0,ry1);
   q = g2[ b11 ] ; v = at2(rx1,ry1);
   b = lerp(sx, u, v);

   return lerp(sy, a, b);
}

double noise2PeriodiqueX(double vec[2], unsigned int periode)
{
   int bx0, bx1, by0, by1, b00, b10, b01, b11;
   double rx0, rx1, ry0, ry1, *q, sx, sy, a, b, t, u, v;
   int i, j;

   if (start) {
      start = 0;
      init();
   }

   setup(0, bx0,bx1, rx0,rx1);
   setup(1, by0,by1, ry0,ry1);

   bx0 = bx0 % periode;
   bx0 = (bx0 < 0 ? bx0+periode : bx0);
   bx1 = bx1 % periode;
   bx1 = (bx1 < 0 ? bx1+periode : bx1);


   i = p[ bx0 ];
   j = p[ bx1 ];

   b00 = p[ i + by0 ];
   b10 = p[ j + by0 ];
   b01 = p[ i + by1 ];
   b11 = p[ j + by1 ];

   sx = s_curve(rx0);
   sy = s_curve(ry0);

   q = g2[ b00 ] ; u = at2(rx0,ry0);
   q = g2[ b10 ] ; v = at2(rx1,ry0);
   a = lerp(sx, u, v);

   q = g2[ b01 ] ; u = at2(rx0,ry1);
   q = g2[ b11 ] ; v = at2(rx1,ry1);
   b = lerp(sx, u, v);

   return lerp(sy, a, b);
}

double noise2PeriodiqueXY(double vec[2], unsigned int periode)
{
   int bx0, bx1, by0, by1, b00, b10, b01, b11;
   double rx0, rx1, ry0, ry1, *q, sx, sy, a, b, t, u, v;
   int i, j;

   if (start) {
      start = 0;
      init();
   }

   setup(0, bx0,bx1, rx0,rx1);
   setup(1, by0,by1, ry0,ry1);

   bx0 = bx0 % periode;
   bx0 = (bx0 < 0 ? bx0+periode : bx0);
   bx1 = bx1 % periode;
   bx1 = (bx1 < 0 ? bx1+periode : bx1);

   by0 = by0 % periode;
   by0 = (by0 < 0 ? by0+periode : by0);
   by1 = by1 % periode;
   by1 = (by1 < 0 ? by1+periode : by1);
   


   i = p[ bx0 ];
   j = p[ bx1 ];

   b00 = p[ i + by0 ];
   b10 = p[ j + by0 ];
   b01 = p[ i + by1 ];
   b11 = p[ j + by1 ];

   sx = s_curve(rx0);
   sy = s_curve(ry0);

   q = g2[ b00 ] ; u = at2(rx0,ry0);
   q = g2[ b10 ] ; v = at2(rx1,ry0);
   a = lerp(sx, u, v);

   q = g2[ b01 ] ; u = at2(rx0,ry1);
   q = g2[ b11 ] ; v = at2(rx1,ry1);
   b = lerp(sx, u, v);

   return lerp(sy, a, b);
}


double noise2WithGrad(double vec[2], double grad[2])
{
   int bx0, bx1, by0, by1, b00, b10, b01, b11;
   double rx0, rx1, ry0, ry1, *q, sx, sy, a, b, t, u, v;
   double dax, day, dbx, dby, dsx, dsy;
   int i, j;

   if (start) {
      start = 0;
      init();
   }

   setup(0, bx0,bx1, rx0,rx1);
   setup(1, by0,by1, ry0,ry1);

   i = p[ bx0 ];
   j = p[ bx1 ];

   b00 = p[ i + by0 ];
   b10 = p[ j + by0 ];
   b01 = p[ i + by1 ];
   b11 = p[ j + by1 ];

   sx = s_curve(rx0);
   sy = s_curve(ry0);
   dsx = ds(rx0);
   dsy = ds(ry0);
   
   grad[0] = grad[1] = 0;

   q = g2[ b00 ] ; u = at2(rx0,ry0);
   dax = -dsx*u + (1-sx)*q[0];
   day = (1-sx)*q[1];

   q = g2[ b10 ] ; v = at2(rx1,ry0);
   dax += dsx*v+sx*q[0];
   day += sx*q[1];
   a = lerp(sx, u, v);

   q = g2[ b01 ] ; u = at2(rx0,ry1);
   dbx = -dsx*u + (1-sx)*q[0];
   dby = (1-sx)*q[1];

   q = g2[ b11 ] ; v = at2(rx1,ry1);
   dbx += dsx*v+sx*q[0];
   dby += sx*q[1];
   b = lerp(sx, u, v);

   grad[0] = lerp(sy, dax, dbx);
   grad[1] = dsy*(b-a) + lerp(sy, day, dby);

   return lerp(sy, a, b);
}


ggdouble noise3(ggdouble vec[3])
{
   int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
   ggdouble rx0, rx1, ry0, ry1, rz0, rz1, sy, sz, a, b, c, d, t, u, v;
   double *q;
   int i, j;

   if (start) {
      start = 0;
      init();
   }

   setup(0, bx0,bx1, rx0,rx1);
   setup(1, by0,by1, ry0,ry1);
   setup(2, bz0,bz1, rz0,rz1);

   i = p[ bx0 ];
   j = p[ bx1 ];

   b00 = p[ i + by0 ];
   b10 = p[ j + by0 ];
   b01 = p[ i + by1 ];
   b11 = p[ j + by1 ];

   t  = s_curve(rx0);
   sy = s_curve(ry0);
   sz = s_curve(rz0);

   q = g3[ b00 + bz0 ] ; u = at3(rx0,ry0,rz0);
   q = g3[ b10 + bz0 ] ; v = at3(rx1,ry0,rz0);
   a = lerp(t, u, v);

   q = g3[ b01 + bz0 ] ; u = at3(rx0,ry1,rz0);
   q = g3[ b11 + bz0 ] ; v = at3(rx1,ry1,rz0);
   b = lerp(t, u, v);

   c = lerp(sy, a, b);

   q = g3[ b00 + bz1 ] ; u = at3(rx0,ry0,rz1);
   q = g3[ b10 + bz1 ] ; v = at3(rx1,ry0,rz1);
   a = lerp(t, u, v);

   q = g3[ b01 + bz1 ] ; u = at3(rx0,ry1,rz1);
   q = g3[ b11 + bz1 ] ; v = at3(rx1,ry1,rz1);
   b = lerp(t, u, v);

   d = lerp(sy, a, b);

   return lerp(sz, c, d);
}


ggdouble noise3WithGrad(ggdouble vec[3], double grad[3])
{
  
   int bx0, bx1, by0, by1, bz0, bz1, b00, b10, b01, b11;
   ggdouble rx0, rx1, ry0, ry1, rz0, rz1, sx, sy, sz, a, b, c, d, u, v;
   ggdouble t, dsx, dsy, dsz;
   double grad1[3], grad2[3];
   double *q;
   int i, j;

   if (start) {
      start = 0;
      init();
   }

   setup(0, bx0,bx1, rx0,rx1);
   setup(1, by0,by1, ry0,ry1);
   setup(2, bz0,bz1, rz0,rz1);

   i = p[ bx0 ];
   j = p[ bx1 ];

   b00 = p[ i + by0 ];
   b10 = p[ j + by0 ];
   b01 = p[ i + by1 ];
   b11 = p[ j + by1 ];

   sx  = s_curve(rx0);
   sy = s_curve(ry0);
   sz = s_curve(rz0);

   dsx = ds(rx0);
   dsy = ds(ry0);
   dsz = ds(rz0);

   grad[0] = grad[1] = grad[2] = 0.0;

   q = g3[ b00 + bz0 ] ; u = at3(rx0,ry0,rz0);
   grad1[0] = q[0]*(1-sx) - dsx*u;
   grad1[1] = q[1]*(1-sx);
   grad1[2] = q[2]*(1-sx);

   q = g3[ b10 + bz0 ] ; v = at3(rx1,ry0,rz0);
   grad1[0] += q[0]*sx + dsx*v;
   grad1[1] += q[1]*sx;
   grad1[2] += q[2]*sx;

   a = lerp(sx, u, v);
   grad1[0] *= (1-sy);
   grad1[1] *= (1-sy);
   grad1[2] *= (1-sy);

   q = g3[ b01 + bz0 ] ; u = at3(rx0,ry1,rz0);
   grad2[0] = q[0]*(1-sx) - dsx*u;
   grad2[1] = q[1]*(1-sx);
   grad2[2] = q[2]*(1-sx);

   q = g3[ b11 + bz0 ] ; v = at3(rx1,ry1,rz0);
   grad2[0] += q[0]*sx + dsx*v;
   grad2[1] += q[1]*sx;
   grad2[2] += q[2]*sx;

   b = lerp(sx, u, v);
   grad2[0] *= sy;
   grad2[1] *= sy;
   grad2[2] *= sy;

   c = lerp(sy, a, b);
   grad1[0] += grad2[0];
   grad1[1] += grad2[0]+dsy*(b-a);
   grad1[2] += grad2[0];

   grad[0] = (1-sz)*grad1[0];
   grad[1] = (1-sz)*grad1[1];
   grad[2] = (1-sz)*grad1[2];

   q = g3[ b00 + bz1 ] ; u = at3(rx0,ry0,rz1);
   grad1[0] = q[0]*(1-sx) - dsx*u;
   grad1[1] = q[1]*(1-sx);
   grad1[2] = q[2]*(1-sx);

   q = g3[ b10 + bz1 ] ; v = at3(rx1,ry0,rz1);
   grad1[0] += q[0]*sx + dsx*v;
   grad1[1] += q[1]*sx;
   grad1[2] += q[2]*sx;

   a = lerp(sx, u, v);
   grad1[0] *= (1-sy);
   grad1[1] *= (1-sy);
   grad1[2] *= (1-sy);


   q = g3[ b01 + bz1 ] ; u = at3(rx0,ry1,rz1);
   grad2[0] = q[0]*(1-sx) - dsx*u;
   grad2[1] = q[1]*(1-sx);
   grad2[2] = q[2]*(1-sx);

   q = g3[ b11 + bz1 ] ; v = at3(rx1,ry1,rz1);
   grad2[0] += q[0]*sx + dsx*v;
   grad2[1] += q[1]*sx;
   grad2[2] += q[2]*sx;

   b = lerp(sx, u, v);
   grad2[0] *= sy;
   grad2[1] *= sy;
   grad2[2] *= sy;


   d = lerp(sy, a, b);
   grad1[0] += grad2[0];
   grad1[1] += grad2[0]+dsy*(b-a);
   grad1[2] += grad2[0];

   grad[0] += sz*grad1[0];
   grad[1] += sz*grad1[1];
   grad[2] += sz*grad1[2] + dsz*(d-c);

   return lerp(sz, c, d);
}



/* --- My harmonic summing functions - PDB --------------------------*/

/*
   In what follows "alpha" is the weight when the sum is formed.
   Typically it is 2, As this approaches 1 the function is noisier.
   "beta" is the harmonic scaling/spacing, typically 2.
*/

double PerlinNoise1D(double x,double alpha,double beta,int n)
{
   int i;
   double val,sum = 0;
   double p,scale = 1;

   p = x;
   for (i=0;i<n;i++) {
      val = noise1(p);
      sum += val / scale;
      scale *= alpha;
      p *= beta;
   }
   return(sum);
}

double PerlinNoise2DPeriodiqueX(double x,double y,double alpha, int n,
				double xper)
{
   int i;
   double val,sum = 0;
   double p[2],scale = 1;
   const double beta = 2.0;

   unsigned int per = (int) floor(xper);

   p[0] = (x/xper)*per;
   p[1] = y;
   for (i=0;i<n;i++) {
      val = fabs(noise2PeriodiqueX(p, per));
/*        val = noise2PeriodiqueX(p, per); */
      sum += val / scale;
      scale *= alpha;
      p[0] *= beta;
      p[1] *= beta;
      per <<= 1;
   }
   return(sum);
}

double PerlinNoise2DPeriodiqueXWithGradscale(double x,double y,
					     double alpha,
					     int n, double xper,
					     double grad[2], double scale)
{
  double val;

  val = PerlinNoise2DPeriodiqueX(x, y, alpha,n, xper);
  grad[0] = (PerlinNoise2DPeriodiqueX(x+scale, y, alpha,n, xper)-val)/scale;
  grad[1] = (PerlinNoise2DPeriodiqueX(x, y+scale, alpha,n, xper)-val)/scale;
  return val;
}


double PerlinNoise2D(double x,double y,double alpha,double beta,int n)
{
   int i;
   double val,sum = 0;
   double p[2],scale = 1;

   p[0] = x;
   p[1] = y;
   for (i=0;i<n;i++) {
      val = noise2(p);
      sum += val / scale;
      scale *= alpha;
      p[0] *= beta;
      p[1] *= beta;
   }
   return(sum);
}

double PerlinNoise2DWithGradscale_fuck(double x,double y,double alpha,double beta,
				  int n, double grad[2], double scale)
{
  double val;

  val = PerlinNoise2D(x, y, alpha, beta,n);
  grad[0] = (PerlinNoise2D(x+scale, y, alpha, beta,n)-val)/scale;
  grad[1] = (PerlinNoise2D(x, y+scale, alpha, beta,n)-val)/scale;
  return val;
}


double PerlinNoise2DWithGradscale(double x,double y,double alpha,double beta,
				  int n, double grad[2], double _scale)
{
   int i;
   double val,sum = 0;
   double p[2],scale = 1;
   double scaleg = 1;
   double grtp[2];

   p[0] = x;
   p[1] = y;
   grad[0] = grad[1] = 0;
   for (i=0;i<n;i++) {
      val = noise2WithGrad(p, grtp);
      grad[0] += grtp[0]*scaleg;
      grad[1] += grtp[1]*scaleg;
      sum += val / scale;
      scale *= alpha;
      scaleg = beta/alpha;
      p[0] *= beta;
      p[1] *= beta;
   }
   return(sum);
}

ggdouble PerlinNoise3D(ggdouble x,ggdouble y,ggdouble z,double alpha,double beta,int n)
{
   int i;
   ggdouble val,sum = 0;
   ggdouble p[3],scale = 1;

   p[0] = x;
   p[1] = y;
   p[2] = z;
   for (i=0;i<n;i++) {
      val = noise3(p);
      sum += val / scale;
      scale *= alpha;
      p[0] *= beta;
      p[1] *= beta;
      p[2] *= beta;
   }
   return(sum);
}

ggdouble PerlinNoise3DWithGradscale(ggdouble x,ggdouble y, ggdouble z,
				  double alpha,double beta,
				  int n, double grad[3], double _scale)
{
   int i;
   ggdouble val,sum = 0;
   ggdouble p[3],scale = 1;
   ggdouble scaleg = 1;
   double grtp[3];

   p[0] = x;
   p[1] = y;
   p[2] = z;
   grad[0] = grad[1] = grad[2] = 0;
   for (i=0;i<n;i++) {
      val = noise3WithGrad(p, grtp);
      grad[0] += grtp[0]*scaleg;
      grad[1] += grtp[1]*scaleg;
      grad[2] += grtp[2]*scaleg;
      sum += val / scale;
      scale *= alpha;
      scaleg = beta/alpha;
      p[0] *= beta;
      p[1] *= beta;
      p[2] *= beta;
   }
   return(sum);
}
