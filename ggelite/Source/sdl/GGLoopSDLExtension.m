//
//  GGLoopSDLExtension.m
//  elite
//
//  Created by Frederic De Jaeger on 17/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <SDL/SDL.h>
#import "GGLoopSDLExtension.h"
#import "Console.h"
#import "Preference.h"
#import "stdutile.h"
#import "GGEventDispatcher.h"
#import "GGEliteController.h"

void quit_ggelite( int code )
{
    /*
     * Quit SDL so we can release the fullscreen
     * mode and restore the previous video settings,
     * etc.
     */
    SDL_Quit( );
    
    /* Exit program. */
    exit( code );
}


static void createGlContext()
{
    /* Information about the current video settings. */
    const SDL_VideoInfo* info = NULL;
    /* Dimensions of our window. */
    int width = 0;
    int height = 0;
    /* Color depth in bits of our window. */
    int bpp = 0;
    /* Flags we will pass into SDL_SetVideoMode. */
    int flags = 0;
    
    /* First, initialize SDL's video subsystem. */
    if( SDL_Init( SDL_INIT_VIDEO| SDL_INIT_JOYSTICK | SDL_INIT_NOPARACHUTE ) < 0 ) {
        /* Failed, exit. */
        fprintf( stderr, "Video initialization failed: %s\n",
                 SDL_GetError( ) );
        quit_ggelite( 1 );
    }
    
    /* Let's get some video information. */
    info = SDL_GetVideoInfo( );
    
    if( !info ) {
        /* This should probably never happen. */
        fprintf( stderr, "Video query failed: %s\n",
                 SDL_GetError( ) );
        quit_ggelite( 1 );
    }
    
    /*
     * Set our width/height to 640/480 (you would
                                        * of course let the user decide this in a normal
                                        * app). We get the bpp we will request from
     * the display. On X11, VidMode can't change
     * resolution, so this is probably being overly
     * safe. Under Win32, ChangeDisplaySettings
     * can change the bpp.
     */
    width = WIDTH;
    height = HEIGHT;
    bpp = info->vfmt->BitsPerPixel;
    
    /*
     * Now, we want to setup our requested
     * window attributes for our OpenGL window.
     * We want *at least* 5 bits of red, green
     * and blue. We also want at least a 16-bit
     * depth buffer.
     *
     * The last thing we do is request a double
     * buffered window. '1' turns on double
     * buffering, '0' turns it off.
     *
     * Note that we do not use SDL_DOUBLEBUF in
     * the flags to SDL_SetVideoMode. That does
     * not affect the GL attribute state, only
     * the standard 2D blitting setup.
     */
    SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    
    /*
     * We want to request that SDL provide us
     * with an OpenGL window, in a fullscreen
     * video mode.
     *
     * EXERCISE:
     * Make starting windowed an option, and
     * handle the resize events properly with
     * glViewport.
     */
    flags = SDL_OPENGL | (thePref.FullScreen ? SDL_FULLSCREEN : 0);
    //flags = SDL_OPENGL | SDL_FULLSCREEN;
    
    /*
     * Set the video mode
     */
    if( (screen = SDL_SetVideoMode( width, height, bpp, flags )) == 0 ) {
        /* 
        * This could happen for a variety of reasons,
         * including DISPLAY not being set, the specified
         * resolution not being available, etc.
         */
        fprintf( stderr, "Video mode set failed: %s\n",
                 SDL_GetError( ) );
        quit_ggelite( 1 );
    }
    if ( thePref.hideCursor )
        SDL_ShowCursor(SDL_DISABLE);
    SDL_WM_SetCaption("ggelite", "ggelite");
    SDL_EnableUNICODE(1);
    
}



SDL_Surface *screen;

@implementation GGLoopSDL 
#pragma mark -
#pragma mark method subclassed from GGLoop
- (BOOL) hasJoystick
{
    return YES;
}


- (void) initJoystickSystem
{
    int njoy, i;
    njoy = SDL_NumJoysticks();
    for( i = 0; i < njoy; ++ i )
    {
        NSDebugMLLog(@"Joystick", @"joystick %d %s", i+1, SDL_JoystickName(i));
    }
    if( njoy > 0 )
    {
        SDL_JoystickEventState(SDL_ENABLE);
        joy=SDL_JoystickOpen(0);
        NSParameterAssert(joy != NULL);
        NSDebugMLLog(@"Joystick", 
                     @"Number of Axes: %d\n", SDL_JoystickNumAxes(joy));
        NSDebugMLLog(@"Joystick", 
                     @"Number of Buttons: %d\n", SDL_JoystickNumButtons(joy));
        NSDebugMLLog(@"Joystick", 
                     @"Number of Balls: %d\n", SDL_JoystickNumBalls(joy));
    }
    else
    {
        joy = NULL;
        NSDebugMLLog(@"Joystick", @"No Joystick found");
    }
}

- (void) enableJoystick: (BOOL) val
{
    NSDebugMLLog(@"Joystick", @"Call with %d", val);
    if ( val && !joy )
    {
        NSDebugMLLog(@"Joystick", @"Joystick initialisation");
        [self initJoystickSystem];
        if ( joy == NULL )
            systemLog(@"Could not init the joystick");
    }
    else if( !val && joy )
    {
        NSDebugMLLog(@"Joystick", @"Joystick is disable");
        SDL_JoystickClose(joy);
        joy = NULL;
        [input clearJoystickState];
    }
}

- (void) dealloc
{
    if( joy != NULL )
    {
        SDL_JoystickClose(joy);
        joy = NULL;
    }
    
    [super dealloc];
}

- (void) beforeRunningLoop
{
    createGlContext();
    [super beforeRunningLoop];
	_gameController = [[GGEliteController alloc] init];
	[_gameController startup];
}

- (void) toggleFullscreen: (NSNotification *) aNotification
{
    SDL_WM_ToggleFullScreen(screen);
}

- (void) beginGrab
{
    SDL_WM_GrabInput(SDL_GRAB_ON);
    [super beginGrab];
}

- (void) endGrab
{
    SDL_WM_GrabInput(SDL_GRAB_OFF);
    [super endGrab];
}


-(int) handleEvent: (Event *)_pevent
{    
    SDL_Event* pevent = _pevent->originalEvent;
    switch (pevent->type) {
        case SDL_KEYDOWN:
            if([_dispatcher dispatchKeyDownWithEvent:_pevent target:self]){
                return 1;
            }
            break;
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
            xMouse = pevent->button.x;
#if 0 && defined(MACOSX)
            yMouse = HEIGHT-1-pevent->button.y;
#else
            yMouse = pevent->button.y;
#endif
            return 0;
        case SDL_MOUSEMOTION:
            xMouse = pevent->motion.x;
#if 0 && defined( MACOSX)
            yMouse = HEIGHT-1-pevent->motion.y;
#else
            yMouse = pevent->motion.y;
#endif
            
            if( grab )
            {
                NSDebugMLLog(@"Motion", @"%d %d %d %d", pevent->motion.x,
                             pevent->motion.y, pevent->motion.xrel, 
                             pevent->motion.yrel);
                if( abs(xMouse-WIDTH/2) + abs(yMouse-HEIGHT/2) > 100 )
                {
                    SDL_WarpMouse(WIDTH/2, HEIGHT/2);
                }
                if( abs(pevent->motion.xrel) + abs(pevent->motion.yrel) > 50 )
                {
                    return 1;
                }
            }
                return 0;
        case SDL_VIDEORESIZE:
            gg_resize_screen( pevent->resize.w, pevent->resize.h);
            return 1;
        case SDL_QUIT:
            [self stopMainLoop];
            return 1;
    }
    return 0;
}


- (int) dispatchEvent: (SDL_Event *) pev
{
    Event ggevent;
    ggevent.originalEvent = pev;
    ggevent.x = xMouse;
    ggevent.y = yMouse;

    if( [self handleEvent:&ggevent] )
        return 1;
    if( window != nil ){
        return [[window windowOnTop] handleEvent:&ggevent];
    }
    
    return 0;
}

- (void) dispatchEventIfNeeded
{
    SDL_Event event;
    while ( SDL_PollEvent(&event) > 0 && cont)
    {
        [self dispatchEvent: &event];
    }
    
    if( pollMode == WaitForEvent )
    {
        SDL_WaitEvent(&event );
        [self dispatchEvent: &event];
    }
}

@end