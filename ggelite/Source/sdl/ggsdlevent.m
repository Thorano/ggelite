/*
 *  ggsdlevent.c
 *  elite
 *
 *  Created by Frederic De Jaeger on 13/07/05.
 *  Copyright 2005 __MyCompanyName__. All rights reserved.
 *
 */

#import <SDL/SDL.h>
#import "GGEvent.h"
#import "GGLoopSDLExtension.h"
#import "utile.h"

#ifdef USE_SDL

const int GG_BUTTON_LEFT = SDL_BUTTON_LEFT;
const int GG_BUTTON_MIDDLE = SDL_BUTTON_MIDDLE;
const int GG_BUTTON_RIGHT = SDL_BUTTON_RIGHT;
const int GG_BUTTON_WHEELUP = SDL_BUTTON_WHEELUP;
const int GG_BUTTON_WHEELDOWN = SDL_BUTTON_WHEELDOWN;

NSString* stringKeyForEvent(Event *_pev, BOOL* basicTextPtr)
{
    SDL_Event* pev = _pev->originalEvent;
    BOOL basicText = NO;
    NSCParameterAssert(pev->type ==  SDL_KEYDOWN ||
                       pev->type ==  SDL_KEYUP);
    
    NSString* mod = @"";
    if( pev->key.keysym.mod & KMOD_SHIFT ){
        mod = @"S-";
    }
    if( pev->key.keysym.mod & KMOD_CTRL ){
        mod = [@"C-" stringByAppendingString:mod];
    }
    if( pev->key.keysym.mod & KMOD_META ){
        mod = [@"M-" stringByAppendingString:mod];
    }
    
    
    NSString* key = nil;
    switch(pev->key.keysym.sym){
        // only match non-ascii keys
        
        case SDLK_RETURN:
            key = @"RET";
            break;
        case SDLK_SPACE:
            key = @"space";
            break;
        case SDLK_ESCAPE:
            key = @"ESC";
            break;
        case SDLK_TAB:
            key = @"TAB";
            break;
        case SDLK_F1:
            key = @"F1";
            break;
        case SDLK_F2:
            key = @"F2";
            break;
        case SDLK_F3:
            key = @"F3";
            break;
        case SDLK_F4:
            key = @"F4";
            break;
        case SDLK_F5:
            key = @"F5";
            break;
        case SDLK_F6:
            key = @"F6";
            break;
        case SDLK_F7:
            key = @"F7";
            break;
        case SDLK_F8:
            key = @"F8";
            break;
        case SDLK_F9:
            key = @"F9";
            break;
        case SDLK_F10:
            key = @"F10";
            break;
        case SDLK_F11:
            key = @"F11";
            break;
        case SDLK_F12:
            key = @"F12";
            break;
        case SDLK_F13:
            key = @"F13";
            break;
        case SDLK_LEFT:
            key = @"left";
            break;
        case SDLK_RIGHT:    
            key = @"right";
            break;
        case SDLK_DOWN:
            key = @"down";
            break;
        case SDLK_UP:
            key = @"up";
            break;
        case SDLK_PAGEUP:
            key = @"pageup";
            break;
        case SDLK_PAGEDOWN:
            key = @"pagedown";
            break;
        case SDLK_HOME:
            key = @"home";
            break;
        case SDLK_END:
            key = @"end";
            break;
        case SDLK_KP0:
            key = @"KP0";
            break;
        case SDLK_KP1:
            key = @"KP1";
            break;
        case SDLK_KP2:
            key = @"KP2";
            break;
        case SDLK_KP3:
            key = @"KP3";
            break;
        case SDLK_KP4:
            key = @"KP4";
            break;
        case SDLK_KP5:
            key = @"KP5";
            break;
        case SDLK_KP6:
            key = @"KP6";
            break;
        case SDLK_KP7:
            key = @"KP7";
            break;
        case SDLK_KP8:
            key = @"KP8";
            break;
        case SDLK_KP9:
            key = @"KP9";
            break;
        case SDLK_KP_PERIOD:
            key = @"KPperiod";
            break;
        case SDLK_KP_DIVIDE:
            key = @"KPdivide";
            break;
        case SDLK_KP_MULTIPLY:
            key = @"KPmultiply";
            break;
        case SDLK_KP_MINUS:
            key = @"KPminus";
            break;
        case SDLK_KP_PLUS:
            key = @"KPplus";
            break;
        case SDLK_KP_ENTER:
            key = @"KPenter";
            break;
        case SDLK_KP_EQUALS:
            key = @"KPequals";
            break;
        default:
            if( (pev->key.keysym.unicode & 0xFF80) == 0){
                key = [NSString stringWithFormat:@"%c", 
                    pev->key.keysym.sym & 0x7F];
                basicText = YES;
            }
            else{
                NSLog(@"this should not happen");
                ggabort();
            }
            break;
    }
    
    NSString* ret = [mod stringByAppendingString:key];
    NSDebugFLLog(@"Event", @"Event is %@", ret);
    
    if(basicTextPtr){
        *basicTextPtr = basicText;
    }

    return ret;
}

int buttonOfEvent(Event *_pev)
{
    SDL_Event* pev = _pev->originalEvent;
    return pev->button.button;
}

NSPoint mouseLocationOfEvent(Event *_pev)
{
    return NSMakePoint(_pev->x, _pev->y);
/*    SDL_Event* pev = _pev->originalEvent;
    switch(pev->type){
        case SDL_MOUSEBUTTONDOWN:
        case SDL_MOUSEBUTTONUP:
            return NSMakePoint(pev->button.x, pev->button.y);
        case SDL_MOUSEMOTION:
            return NSMakePoint(pev->motion.x,pev->motion.y);
        default:
            [NSException raise:NSInternalInconsistencyException
                        format:@"No location from this kind of event"];
            return NSMakePoint(0, 0);
    }
*/
}

NSPoint mouseMotionOfEvent(Event *_pev)
{
    SDL_Event* pev = _pev->originalEvent;

    if(SDL_MOUSEMOTION!=pev->type){
        [NSException raise:NSInternalInconsistencyException
                    format:@"event should be of type mouse motion"];
    }
    
    return NSMakePoint(pev->motion.xrel,pev->motion.yrel);
}

GGEventType typeOfEvent(Event *_pev)
{
    SDL_Event* pev = _pev->originalEvent;
    switch(pev->type){
        case SDL_KEYDOWN:
            return GG_KEYDOWN;
        case SDL_KEYUP:
            return GG_KEYUP;
        case SDL_MOUSEBUTTONDOWN:
            return GG_MOUSEBUTTONDOWN;
        case SDL_MOUSEBUTTONUP:
            return GG_MOUSEBUTTONUP;
        case SDL_MOUSEMOTION:
            return GG_MOUSEMOTION;
        case    SDL_JOYAXISMOTION:
            return GG_JOYSTICKAXIS;
        case    SDL_JOYHATMOTION:
            return GG_JOYSTICKHAT;
        case    SDL_JOYBUTTONDOWN:
            return GG_JOYSTICKBUTTONDOWN;
        case    SDL_JOYBUTTONUP:
            return GG_JOYSTICKBUTTONUP;
        case    SDL_JOYBALLMOTION:
        default:
            return GG_OTHER_EVENT;
    }
}

int axeOfEvent(Event* _pev)
{
    SDL_Event* pev = _pev->originalEvent;
    NSCAssert(pev->type == SDL_JOYAXISMOTION, @"event must be an axis motion to have an axe");
    return pev->jaxis.axis;
}

double  normalizedValue(Event* _pev)
{
    SDL_Event* pev = _pev->originalEvent;
    NSCAssert(pev->type == SDL_JOYAXISMOTION, @"event must be an axis motion for the normalized value");
    double ret = (pev->jaxis.value + 32768.0L)/65535.0L;
    return pev->jaxis.axis;
}

#endif