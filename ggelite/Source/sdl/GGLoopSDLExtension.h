//
//  GGLoopSDLExtension.h
//  elite
//
//  Created by Frederic De Jaeger on 17/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <SDL/SDL.h>
#import "GGLoop.h"

@class GGEliteController;
@interface GGLoopSDL : GGLoop
{
    struct _SDL_Joystick  *joy;
	GGEliteController*		_gameController;
}
@end

void quit_ggelite( int code );

struct __Event{
    SDL_Event*      originalEvent;
    int             x, y;  //location in our coordinate system.
};
