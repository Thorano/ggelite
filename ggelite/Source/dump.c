#import <stdio.h>
#import <stdlib.h>

#import "perlin.h"
#import "utile.h"

static int width = 256;
static int height = 256;

/*  static MapCol mapCol[] = */
/*  { */
/*    { -800, {  0, 0.7, 0.5, 1}}, */
/*    { -400, {  1, 0, 0.0, 1}}, */
/*    { 0, {  0, 0, 0.5, 1}}, */
/*    { 1, {  0, 0.6, 0, 1}}, */
/*    { 300, {  0.5, 0.5, 0.2, 1}}, */
/*    { 600, {  0.8, 0.5, 0.5, 1}}, */
/*    { 800, { 0.7, 0.7, 0.7, 1}}}; */

/* static MapCol mapCol[] = */
/* { */
/*   { -1000, {  0.8, 0.8, 0.7, 1}}, */
/*   { 0, {  0.1, 0.1, 0.3, 1}}, */
/*   { 1400, {  0.5, 0.8, 0.9, 1}} */
/* }; */

static MapCol mapCol[] =
{
  { -1000, {  0.7, 0.7, 0.7, 1}},
  { -700, {  0.2, 0.2, 0.2, 1}},
  { 0, {  0.3, 0.3, 0.3, 1}},
  { 100, {  0.9, 0.9, 0.9, 1}},
  { 200, {  0.3, 0.3, 0.3, 1}},
  { 700, {  0.2, 0.2, 0.2, 1}},
  { 1400, {  0.5, 0.8, 0.9, 1}}
};

double GGPerlinNoise2DPeriodiqueXY(double x,double y,double alpha, int n,
				  double xper)
{
   int i;
   double val,sum = 0;
   double p[2],scale = 1;
   const double beta = 2.0;

   unsigned int per = (int) floor(xper);

   p[0] = (x/xper)*per;
   p[1] = y;
   for (i=0;i<n;i++) {
/*       val = fabs(noise2PeriodiqueX(p, per)); */
        val = (noise2PeriodiqueXY(p, per)); 
      sum += val / scale;
      scale *= alpha;
      p[0] *= beta;
      p[1] *= beta;
      per <<= 1;
   }
   return(sum);
}

static inline void putRGB(VectCol v, unsigned char *ptr)
{
  ptr[0] = 255*v.r;
  ptr[1] = 255*v.g;
  ptr[2] = 255*v.b;
}

static int count;

static void outPutPixel(unsigned char r, unsigned char g, 
			unsigned char b, FILE *f)
{
  fprintf(f, "%d %d %d", (int)r, (int)g, (int)b);

  if( ++count > 4 )
    {
      count = 0;
      fprintf(f, "\n");
    }
  else
    fprintf(f, " ");
}

void writeRGBTofile(int width, int height, const unsigned char *data)
{
  int i, j;
  static int indFile = 1;

  indFile++;
  count = 0;

  fprintf(stdout, "P3\n");
  fprintf(stdout, "# CREATOR: XV Version 3.10a  Rev: 12/29/94 (PNG patch 1.2)\n");
  fprintf(stdout, "# CREATOR: XV Version 3.10a  Rev: 12/29/94  Quality = 75, Smoothing = 0\n");
  fprintf(stdout, "%d %d\n255\n", width, height);


  for( i = 0; i < height; ++i)
    for( j = 0; j < width; ++j)
      {
	int index = 3*(width*(height -1 - i)+j);
	outPutPixel(data[index], data[index+1], data[index+2], stdout);
      }

}
static inline VectCol moyenne(float val, VectCol c2, VectCol c1)
{
  VectCol ret;
  ret.r = val*c1.r + (1-val)*c2.r;
  ret.g = val*c1.g + (1-val)*c2.g;
  ret.b = val*c1.b + (1-val)*c2.b;
  ret.a = val*c1.a + (1-val)*c2.a;
  return ret;
}

VectCol genColFromMapHeight(MapCol map[], int size, int param)
{
  int i;
  for( i = 0; i < size; ++i )
    {
      if( param <= map[i].val )
	{
	  if( i > 0 )
	    {
	      float alpha;
	      alpha = ((float)param - map[i-1].val) / 
		((float)(map[i].val - map[i-1].val));
	      return moyenne(alpha, map[i-1].couleur, map[i].couleur);
	    }
	  else
	    return map[0].couleur;
	}
    }
  return map[size-1].couleur;
}


int main(void)
{
  int i, j;
  unsigned char *data = malloc(3*width*height);
  for( i = 0; i < height; ++i)
    for( j = 0; j < width; ++j)
      {
	double x, y;
	double res;
	int index = 3*(width*(height -1 - i)+j);
	x = j/(double)width;
	y = i/(double)height;

	res = sin(6*M_PI*(x+y)+3*GGPerlinNoise2DPeriodiqueXY(10*x, 10*y, 2, 10, 10));

	putRGB(genColFromMapHeight(mapCol, sizeof(mapCol)/sizeof(MapCol), 1000*res),
	       data + index);
	
      }

  writeRGBTofile(width, height, data);
  
  free(data);
  return 1;
}
