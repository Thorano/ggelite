/*
 *  GGConcreteShip.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSNotification.h>
#import "GG3D.h"
#import "GGInput.h"
#import "GGConcreteShip.h"
#import "utile.h"
#import "parseASC.h"
#import "Texture.h"
#import "Tableau.h"
#import "Station.h"
#import "World.h"
#import "Sched.h"
#import "ia.h"
#import "bad.h"
#import "Commandant.h"
#import "Resource.h"
#import "missile.h"

@implementation GGShip (GGConcreteShip) /*it is a missile, now*/
+ missileFrom: (GGShip *) source
       target: (GGSpaceObject *) _target
{
  id t;
  GGReal size;
  GGShip *unShip;
  NSString *name;
  Complex_Autopilot *cap;

  NSDictionary *dic;
  name = [[Resource resourceManager] pathForModel:   @"nv500.shp"];
  dic = [NSDictionary dictionaryWithContentsOfFile: name];

  NSAssert([source isLocal], @"bad");

  unShip = [[self alloc] 
             initWithDictionary: dic];
  AUTORELEASE(unShip);
  *Galileen(unShip) = *Galileen(source);

  //    unShip = [self spaceObjectAt: source];

  size = Dimension(source) + Dimension(unShip);

  addLambdaVect(Point(unShip), -1.5*size, Haut(unShip), Point(unShip));
  addLambdaVect(Speed(unShip), 4, Direction(unShip), Speed(unShip));

  [unShip setTarget: _target];

  [[source nodePere] addSubNode: unShip];
  [theWorld addShip: unShip];
  [unShip beginSchedule];

  if ( _target == nil || [_target isKindOfClass: [GGShip class]] )
    {
      t = _target;
    }
  else
    t = nil;

  cap = [Complex_manage_missile manage_missile_missile: unShip
                                  target: t];
  /* NO continuation because it will destroy us */
  [unShip setIA: cap];
  [unShip setName: @"Missile"];

  return unShip;
}

- (void) setDefaultIA: (GGShip *)ship
{
  if( ship == nil )
    {
      Complex_Stupid_IA *blu;
      blu = [Complex_Stupid_IA allocInstance];
      [self setIA: blu];
      [blu runWitharg1: self];
      RELEASE(blu);
    }
  else
    {
      Complex_attack *blu;

      blu = [Complex_attack allocInstance];
      [self setIA: blu];
      [blu runWitharg1: self
	   arg2: ship];
      RELEASE(blu);
    }
}

- (void) setIAJump
{
  ExtendedProcedure *d;

  d = [Complex_Continue_IA Continue_IA_ship: self
			   arrival_date: gameTime
			   jump: YES];
  [self setIA: d];
}
			 
@end
