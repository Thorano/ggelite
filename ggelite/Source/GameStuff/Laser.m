/*
 *  Laser.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import "GGLoop.h"
#import "Laser.h"
#import "GGRepere.h"
#import "utile.h"
#import "Sched.h"
#import "Preference.h"
#import "World.h"
#import "Tableau.h"
#import "SoundSDL.h"

#import <math.h>
#import <time.h>


@implementation Laser
- initLaserAt: (GGSpaceObject *) obj
        power: (double) pow
     lifeTime: (float) lt
{
    [super init];
    _flags.graphical = YES;
    canon = obj;
    [obj enregistreForRemoving: self
                          with: @selector(removeCanonWithNotification:)];
    collisionCounter = -1;
    if( lt > 0 )
    {
        timer = [GGTimer scheduledTimerWithTimeInterval: lt
                                                 target: self
                                               selector: @selector(timeToDie:)
                                                repeats: NO];
        RETAIN(timer);
        [soundManager playFile: @"layseroi.wav"];
    }
    else
        [soundManager playFile: @"layseroi.wav"
                          loop: -1
                          from: self];
    
    power = pow;
    if( pow < 3e6 )
        initRGBA(&col, 1, 0, 0, 0.5);
    else if (pow < 10e6)
        initRGBA(&col, 0, 1, 1, 0.5);
    else
        initRGBA(&col, 1, 1, 1, 0.5);
    
    return self;
}

- (void) encodeWithCoder: (NSCoder *)encoder
{
    [super encodeWithCoder: encoder];
    GGEncode(col);
    GGEncode(power);
    GGEncode(canon);
    GGEncode(timer);
}

-  initWithCoder: (NSCoder *) decoder
{
    [super initWithCoder:  decoder];
    GGDecode(col);
    GGDecode(power);
    GGDecodeObject(canon);
    GGDecode(timer);
    
    return self;
}

- (void) removeCanonWithNotification: (NSNotification *)aNotification
{
#ifndef NS_BLOCK_ASSERTIONS
    id obj = [aNotification object];
    NSAssert(obj == canon, @"bad");
#endif
    [self destroy];
}

+ fireLaserAt: (GGSpaceObject *) obj
        power: (float) pow
     lifeTime: (double) lt
{
    Laser *laser;
    laser = [self alloc];
    laser = [laser initLaserAt: obj
                         power: pow
                      lifeTime: lt];
    [obj  addSubNode: laser];
    [laser enterUniverse];
    [laser setVisible: YES];
    [laser beginSchedule];
    [theWorld manageLaser: laser];
    return AUTORELEASE(laser);
}

- (void) destroy
{
    if(timer) [timer invalidate];  //just in case
    DESTROY(timer);
    [soundManager stopPlayingFrom: self];
    [super destroy];
}

- (void) timeToDie: (GGTimer *) aTimer
{
    [self destroy];
}

- (BOOL) shouldCache
{
    //  return YES;
    return NO;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

- (void) rasterise
{
    NSDebugMLLogWindow(@"Laser", @"on dessine");
    glColor4fv((GLfloat*)&col);
    
    glPushAttrib(GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_LIGHTING);
    glDisable(GL_CULL_FACE);
    glDepthMask(GL_FALSE);
    if( thePref.GLBlend )
        glEnable(GL_BLEND);
    glBegin(GL_QUADS);
    glVertex3f(1, -10, 0);
    glVertex3f(-1, -10, 0);
    glVertex3f(-1e3, 0, 1e6);
    glVertex3f(+1e3, 0, 1e6);
    glEnd();
    
    glPopAttrib();
}

- (float) power
{
    return power;
}
@end

