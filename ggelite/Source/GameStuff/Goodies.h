/* 	-*-ObjC-*- */
/*
 *  Goodies.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Goodies_h
#define __Goodies_h

#import "NSView.h"

@class Item;
@class GGString;
@class ItemStock;

@protocol Quantity 
- (void) update;
- (void) setManager: obj
 withSelectorForBuy: (SEL) aSelBuy
 andSelectorForSell: (SEL) aSelSell;
- (ItemStock *) itemStock;
@end

@interface FricInfo : GGView
{
  GGString 	*fric, *masseFret;
}
- (void) updateFric;
@end

@interface Quantity : GGView <Quantity>
{
  GGString	*str;
  GGString	*prix1, *prix2;
  GGString	*masse;
  ItemStock	*theItem;
  id		target;
  SEL		selBuy;
  SEL		selSell;
  GGBouton	*temp;
  BOOL		state;
}
+ quantiteWithItem: (ItemStock *) it;
- (void) setPrixAchat: (int) prixA
	    prixVente: (int) prixV;
- (void) setManager: obj
 withSelectorForBuy: (SEL) aSelBuy
 andSelectorForSell: (SEL) aSelSell;
- (ItemStock *) itemStock;
@end

@interface QuantityFood : GGView <Quantity>
{
  GGString 	*str;
  GGString 	*price;
  GGString	*stock;
  GGString	*cargo;
  ItemStock	*theItem;
  id		target;
  SEL		selBuy;
  SEL		selSell;
}
+ quantiteWithItem: (ItemStock *) it;
- (void) setManager: obj
 withSelectorForBuy: (SEL) aSelBuy
 andSelectorForSell: (SEL) aSelSell;
- (ItemStock *) itemStock;
@end

@interface MarketPlace : GGView
{
  GGMenu	*menu;
  FricInfo	*fricInfo;
}
- initWithGoodies: (NSArray *) goodies
	  manager: (Class) c;
- initWithGoodies: (NSArray *) goodies;
@end

@interface NSString (GGExt)
+ euro;
@end

#endif
