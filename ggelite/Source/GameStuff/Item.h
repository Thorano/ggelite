/* 	-*-ObjC-*- */
/*
 *  Item.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Item_h
#define __Item_h

#include "types.h"
GGBEGINDEC

@class System;

typedef enum __itemtype
{
  LaserPulsion,
  LaserRayon,
  Hyperdrive,
  Missile,
  Mine,
  BombeEnergie,
  ECM,
  ShieldGenerator,
  Inspector,
  HullRepair,
  CloudAnalyzer,
  OtherEquip,
  GoodiesBegining,
  Slaves,
  Animals,
  Living,
  Hydrogen,
  Computers,
  Foods,
  Gems,
  Narcotics,
  Other
}ItemType;

typedef enum __Equipment
  {
    XB74_Proximity_Mine = 1,
    Energy_Bomb = 2,
    KLT60_Homing_Missile = 3,
    LV111_Smart_Missile = 4,
    NN500_Naval_Missile = 5,
    MV1_Assault_Missile = 6,
    MV2_Assault_Missile = 7,
    Missile_Viewer = 8,
    Laser_Cooling_Booster = 9,
    Laser_1mw_Pulse = 10,
    Laser_5mw_Pulse = 11,
    Laser_1mw_Beam = 12,
    Laser_4mw_Beam = 13,
    Laser_20mw_Beam = 14,
    Laser_100mw_Beam = 15,
    Sm_Plasma_Accel = 16,
    Auto_Targetter = 17,
    Lg_Plasma_Accel_blu = 18,
    Mining_Lsr = 19,
    MB4_Mining_Machine = 20,
    Auto_Refueler = 21,
    Atmospheric_Shielding = 22,
    Cargo_Bay_Life_Support = 23,
    Energy_Booster_Unit = 24,
    Escape_Capsule = 25,
    Extra_Passenger_Cabin = 26,
    Fuel_Scoop = 27,
    Cargo_Scoop_Converter = 28,
    Tractor_Beam_C_Scoop = 29,
    Hull_Auto_Repair_Sys = 30,
    Shield_Generator = 31,
    Chaff_Dispenser = 32,
    ECM_System = 33,
    Naval_ECM_System = 34,
    Automatic_Pilot = 35,
    Navigation_Computer = 36,
    Combat_Computer = 37,
    Radar_Mapper = 38,
    Scanner = 39,
    Hyper_Cloud_Analizer = 40,
    Interplanetary_Drive = 41,
    Class_1_Hyperdrive = 42,
    Class_2_Hyperdrive = 43,
    Class_3_Hyperdrive = 44,
    Class_4_Hyperdrive = 45,
    Class_5_Hyperdrive = 46,
    Class_6_Hyperdrive = 47,
    Class_7_Hyperdrive = 48,
    Class_8_Hyperdrive = 49,
    Class_1_Military_Drive = 50,
    Class_2_Military_Drive = 51,
    Class_3_Military_Drive = 52
  }Equipment;

@interface Item : NSObject <NSCopying>
{
  @public
  ItemType		type;
  int			arg1, arg2, arg3;
  float			masse;
  NSString		*name;
}
+ itemWithName: (NSString *) aName
	  type: (int) aType
	   arg: (int) aArg;
+ itemWithName: (NSString *) aName
	  type: (int) aType
	   arg: (int) aArg
	 masse: (int) m;
- (void) setItemName: (NSString *)_name;

+ equipmentWithArg: (int) val;
+ goodiesWithArg: (int) val;
+ hydrogen;
+ hyperDriveCivil: (int) class;
- (BOOL) oncePerShip;
@end

@interface ItemStock : NSObject <NSCopying>
{
  @public
  Item *item; //retained
  int			prixAchat, prixVente;
  int			cargo, stock;
}

+ itemStockWithItem: (Item *)i;
+ itemStockWithName: (NSString *) aName
	  type: (int) aType
	   arg: (int) aArg
     prixAchat: (int) aPrixAchat
     prixVente: (int) aPrixVente;
+ itemStockWithName: (NSString *) aName
	       type: (int) aType
		arg: (int) aArg
	  prixAchat: (int) aPrixAchat
	  prixVente: (int) aPrixVente
	      masse: (int) m
	      stock: (int) q;
+ (NSArray *) itemList;
+ (NSArray *) comItemList;
+ (void) setSystemGoods: (System *) sys;
@end

int powerLaserFromId(int val);
int hyperDriveTypeFromId(int val);

GGENDDEC

#endif
