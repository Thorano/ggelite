/*
 *  Goodies.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSData.h>


#import "Metaobj.h"
#import "Tableau.h"
#import "World.h"
#import "NSWindow.h"
#import "GGWidget.h"
#import "GGShip.h"
#import "utile.h"
#import "parseASC.h"
#import "GGMenu.h"
#import "Item.h"
#import "Commandant.h"
#import "Goodies.h"
#import "GGFont.h"
#import "Console.h"
#import "GGScrollView.h"

static NSString *euro = @"euro";
static BOOL euroInit = NO;

@implementation NSString (GGExt)
+ euro
{
  if( !euroInit )
    [FricInfo class]; //this call initialize
  NSParameterAssert(euroInit);
  return euro;
}
@end

@implementation FricInfo
+ (void) initialize
{
  if( self == [FricInfo class] )
    {
      //what a pain!!
      char *euroutf8 = "€";
      euro = [[NSString alloc] initWithData:
				 [NSData dataWithBytes: euroutf8 length: strlen(euroutf8)]
			       encoding: NSUTF8StringEncoding];
      euroInit = YES;
      //we don't clean this string.  That's bad but I really don't care
    }
}

-  initWithFrame: (NSRect) aRect
{
  [super initWithFrame: aRect];
  fric = [GGString new];
  [fric setFrameOrigin: NSMakePoint(10, 15)];
  [fric setString: @"blu"
	withFont: [GGFont fixedFont]];

  masseFret = [GGString new];
  [masseFret setFrameOrigin: NSMakePoint(10, 0)];
  [masseFret setString: @"blu"
	withFont: [GGFont fixedFont]];

  
  [self addSubview: fric];
  [self addSubview: masseFret];

  return self;
}

- (void) updateFric
{
  NSRect aRect;
  int usedCargo;
  int totalCargo;
  NSMutableString *s;

  usedCargo = (theCommandant->masseFret - theCommandant->capacity +
	       theCommandant->capacityFret) / 1e3;
  totalCargo = theCommandant->capacityFret/1e3;
  [fric updateString: 
	  [NSString stringWithFormat: @"@bCargo@w      %4d: %4d    @bCash@w", 
		    usedCargo, totalCargo]];
  s = [NSString stringWithFormat: 
			 @"@bCabine@w                   %g %@",
		       theCommandant->fric, euro];
  [masseFret updateString: s];

  aRect = [fric frame];
  aRect = NSUnionRect(aRect, [masseFret frame]);
  [self setFrameSize: NSMakeSize(NSMaxX(aRect), NSMaxY(aRect))];
}

- (void) dealloc
{
  RELEASE(fric);
  RELEASE(masseFret);
  [super dealloc];
}
@end

@implementation MarketPlace
- initWithGoodies: (NSArray *) goodies
	  manager: (Class) c
{
  int n;
  NSRect frame;

  [self initWithFrame: NSMakeRect(0,40, VirtualWidth-100, VirtualHeight-100)];
  frame = [self frame];


  n = [goodies count];
  if( n > 0 )
    {
      GGMenu *m;
      GGScrollView *scroll;
      int i;
      ItemStock *stocks[n];
      [goodies getObjects: stocks];
      scroll = [[GGScrollView alloc] 
		 initWithFrame: NSMakeRect(50, 50, frame.size.width - 70,
					   frame.size.height-90)];
      [self addSubview: scroll];

      menu = m = [GGMenu menu];
      
      for( i = 0; i < n; ++i)
	{
	  GGView<Quantity> *q;
	  stocks[i]->cargo = [theCommandant nbrItem: stocks[i]->item];
	  q = [c quantiteWithItem: stocks[i]];
	  [q setManager: self
	     withSelectorForBuy: @selector(buyFrom:)
	     andSelectorForSell: @selector(sellFrom:)];
	  [m addMenuItem: q]; 
	}
      [m endItem];

      [scroll setDocumentView: m];
      RELEASE(scroll);
    }

  fricInfo = [[FricInfo alloc] initWithFrame: NSMakeRect(5, 5, 300,100)];
  [self addSubview: fricInfo];
  [fricInfo updateFric];

  return self;
}

- initWithGoodies: (NSArray *) goodies
{
  return [self initWithGoodies: goodies
	       manager: [Quantity class]];
}

- (void) viewWillMoveToWindow: (GGNSWindow *)win
{
  [super viewWillMoveToWindow: win];
  if( win != nil )
    [win makeFirstResponder: menu];
}

- (void) dealloc
{
  RELEASE(fricInfo);
  [super dealloc];
}


- (void) buyFrom: (GGView<Quantity> *)qte
{
  ItemStock *it;
  ShipErrorCode code;
  it = [qte itemStock];
  if( it->stock <= 0 )
    consoleLog(@"We don't have `%@' right now, please come back later", 
	       it->item->name);
  else if( (code = [theCommandant buyItem: it]) == NoError )
    {
      it->cargo++;
      it->stock--;
      [qte update];
      [fricInfo updateFric];
      //      NSWarnMLog(@"fric %g", theCommandant->fric);
    }
  else
    consoleLog(shipMsgFromErrorCode(code));
}

- (void) sellFrom:  (GGView<Quantity> *)qte
{
  ItemStock *it;
  ShipErrorCode code;
  it = [qte itemStock];
  if( (code = [theCommandant sellItem: it]) == NoError )
    {
      it->cargo--;
      it->stock++;
      [qte update];
      [fricInfo updateFric];
      //      NSWarnMLog(@"fric %g", theCommandant->fric);
    }
  else
    consoleLog(shipMsgFromErrorCode(code));
}
@end



@implementation Quantity
- (void) setSellButton
{
  if( !state)
    {
      [self addSubview: temp];
      state = YES;
    }
}

- (void) removeSellButton
{
  if(state)
    {
      [temp removeFromSuperview];
      state = NO;
    }
}

- initWithName: (NSString *)name
	  item: (ItemStock *)i
{
  GGBouton *qu_bo;
  [self initWithFrame: NSMakeRect(0, 0, 700, 30)];

  ASSIGN(theItem, i);

  qu_bo = AUTORELEASE([GGBouton new]);
  [qu_bo setFrame: NSMakeRect(5, 5, 15, 15)];
  //  [qu_bo setType: NSToggleButton];
  [qu_bo setTarget: self];
  [qu_bo setAction: @selector(buy)];
  [self addSubview: qu_bo];

  temp = qu_bo = [GGBouton new];
  [qu_bo setFrame: NSMakeRect(25, 5, 15, 15)];
  //  [qu_bo setType: NSToggleButton];
  [qu_bo setTarget: self];
  [qu_bo setAction: @selector(sell)];

  if( i->cargo > 0 )
    [self setSellButton];
    

  str = [GGString new];
  [str setFrame: NSMakeRect(50, 5, 200, 20)];
  [str setString: name];
  [self addSubview: str];

  prix1 = [GGString new];
  [prix1 setString: @"---"];
  [prix1 setFrame: NSMakeRect(350, 5, 70, 20)];
  [self addSubview: prix1];

  prix2 = [GGString new];
  [prix2 setString: @"---"];
  [prix2 setFrame: NSMakeRect(450, 5, 70, 20)];
  [self addSubview: prix2];

  masse = [GGString new];
  [masse setString: @""];
  [masse setFrame: NSMakeRect(550, 5, 50, 20)];
  [self addSubview: masse];

  return self;
}

- initWithItem: (ItemStock *)it
{
  [self initWithName: it->item->name
	item: it];
  [self setPrixAchat: it->prixAchat
	prixVente: it->prixVente];
  NSDebugLLog(@"Quantite", @"%d", it->cargo);
  if( it->item->masse > 0 )
    [masse setString: [NSString stringWithFormat: @"%d", 
				((int)(it->item->masse))/1000]];
  return self;
}

- (void) setManager: obj
 withSelectorForBuy: (SEL) aSelBuy
 andSelectorForSell: (SEL) aSelSell
{
  target = obj;
  selBuy = aSelBuy;
  selSell = aSelSell;
}

- (void) update
{
  if( theItem->cargo > 0 )
    [self setSellButton];
  else
    [self removeSellButton];
//   [stock setString: [NSString stringWithFormat: @"%dt", theItem->stock]];
//   [cargo setString: [NSString stringWithFormat: @"%dt", theItem->cargo]];
}


- (void) setPrixAchat: (int) prixA
	    prixVente: (int) prixV
{
  [prix1 setString: [NSString stringWithFormat: @"%d", prixA]];
  [prix2 setString: [NSString stringWithFormat: @"%d", prixV]];
}

- (void) buy
{
//   if( theItem->stock > 0 )//so was 0
    {
      [target performSelector: selBuy
	      withObject: self];
    }
//   else
//     {
//       consoleLog(@"We don't have `%@' right now, please come back later", 
// 		 theItem->item->name);
//     }
}

- (void) sell
{
  [target performSelector: selSell
	  withObject: self];
}

- (ItemStock *) itemStock
{
  return theItem;
}

- (void) dealloc
{
  RELEASE(temp);
  RELEASE(str);
  RELEASE(prix1);
  RELEASE(prix2);
  RELEASE(theItem);
  RELEASE(masse);
  [super dealloc];
}

+ quantiteWithItem: (ItemStock *) it
{
  return AUTORELEASE([[self alloc] initWithItem: it]);
}

@end


@implementation QuantityFood
- initWithItem: (ItemStock *)i
{
  GGBouton *qu_bo;
  [self initWithFrame: NSMakeRect(0, 0, 700, 30)];

  ASSIGN(theItem, i);

  qu_bo = AUTORELEASE([GGBouton new]);
  [qu_bo setFrame: NSMakeRect(5, 5, 15, 15)];
  //  [qu_bo setType: NSToggleButton];
  [qu_bo setTarget: self];
  [qu_bo setAction: @selector(buy)];
  [self addSubview: qu_bo];

  qu_bo = AUTORELEASE([GGBouton new]);
  [qu_bo setFrame: NSMakeRect(25, 5, 15, 15)];
  //  [qu_bo setType: NSToggleButton];
  [qu_bo setTarget: self];
  [qu_bo setAction: @selector(sell)];
  [self addSubview: qu_bo];
    

  str = [GGString new];
  [str setFrame: NSMakeRect(50, 5, 200, 20)];
  [str setString: i->item->name];
  [self addSubview: str];

  price = [GGString new];
  [price setString: [NSString stringWithFormat: @"%6d %@", i->prixAchat,
			      euro]
	 withFont: [GGFont fixedFont]
   ];
  [price setFrame: NSMakeRect(350, 5, 70, 20)];
  [self addSubview: price];

  stock = [GGString new];
  [stock setString: @"---"
	 withFont: [GGFont fixedFont]];
  [stock setFrame: NSMakeRect(450, 5, 70, 20)];
  [self addSubview: stock];

  cargo = [GGString new];
  [cargo setString: @"---"
	 withFont: [GGFont fixedFont]];
  [cargo setFrame: NSMakeRect(550, 5, 70, 20)];
  [self addSubview: cargo];

  [self update];

  return self;
}

- (void) update
{
  if( theItem->stock > 0 )
    [stock updateString: [NSString stringWithFormat: @"%6dt", theItem->stock]];
  else	
    [stock updateString: @"      -"];
  if( theItem->cargo > 0 )
    [cargo updateString: [NSString stringWithFormat: @"%6dt", theItem->cargo]];
  else
    [cargo updateString: @"      -"];
}


- (void) setManager: obj
 withSelectorForBuy: (SEL) aSelBuy
 andSelectorForSell: (SEL) aSelSell
{
  target = obj;
  selBuy = aSelBuy;
  selSell = aSelSell;
}

- (void) buy
{
  [target performSelector: selBuy
	  withObject: self];
}

- (void) sell
{
  [target performSelector: selSell
	  withObject: self];
}

- (ItemStock *) itemStock
{
  return theItem;
}

- (void) dealloc
{
  RELEASE(str);
  RELEASE(price);
  RELEASE(cargo);
  RELEASE(stock);
  RELEASE(theItem);
  [super dealloc];
}

+ quantiteWithItem: (ItemStock *) it
{
  return AUTORELEASE([[self alloc] initWithItem: it]);
}

@end
