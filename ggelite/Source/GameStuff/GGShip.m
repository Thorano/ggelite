/*
 *  GGShip.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 Various notifications
 
 - (void) updateShipData :  called sometimes for fuel and so on
 - (void) updatePosition : called every frame to update graphical states
 - tryGlobalCoordinates \   called sometimes to manage the reference frame
 - tryLocalCoordinates  /   of the ship
 */

#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <math.h>
#import <time.h>

#import "GGShader.h"
#import "GGLoop.h"
#import "GGSpaceObject.h"
#import "GGInput.h"
#import "GGRepere.h"
#import "Metaobj.h"
#import "utile.h"
#import "Tableau.h"
#import "Sched.h"
#import "World.h"
#import "Laser.h"
#import "GGModel.h"
#import "GGModel.h"
#import "Item.h"
#import "GGConcreteShip.h"
#import "Resource.h"
#import "Console.h"
#import "Preference.h"
#import "autopilote.h"
#import "lasercommand.h"
#import "SoundSDL.h"
#import "Commandant.h"
#import "GGSolid.h"

#if DEBUG_COCOA
#import "GGDebugController.h"
#endif

static const float PowerOverConso = 300*20e3*100;
static const float PowerOverType = 300*20e3;
static const float MaxAngularAccel = 1.0;

NSString *GGNotificationShipLeaveCurrentSystem = 
@"GGNotificationShipLeaveCurrentSystem";
NSString *GGNotificationShipEnterCurrentSystem =
@"GGNotificationShipEnterCurrentSystem";

NSString *GGNotificationWishedDirection =
@"GGNotificationWishedDirection";
NSString *GGNotificationShipCargoChange =
@"GGNotificationShipCargoChange";
NSString *GGNotificationShipDoHyperJump = @"GGNotificationShipDoHyperJump";

//NSString *GGNotificationShipAdvanceInTime

@interface GGShip (Private)
- (BOOL) _loadModel: (NSString *) name
         dictionary: (NSDictionary *) dico;
- (void) _setModel: (ModelBlender*) _model;
@end

static inline float distMaxJump(int n, float m)
{
    return (n)*(n) / m * 300e3;
}


@implementation GGShip
- (void) _GGShipDefaultInitVar
{
    masseCoque = 5e3;
    capacity = 25e3;
    hyperDriveType = 1;
    pylonRocket = 2;
    pylonLaser = 1;
    carburant = 1000;
    fric = 1000;
    currentMissile = -1;
    nbr_shield_generator = 0;
    gAcc = 10;
    gFrein = 3;
    initVect(&posReacteur1, -3.32, 0.44, -2.9);
    initVect(&posReacteur2, 3.32, 0.44, -2.9);
    initVect(&inertie, 1, 1, 1);
    
    //FIXME maybe it should not be here
}

- (void) _updateInternalData
{
    /* Compute things according to some goodies...
    */
        
    masse = masseFret + masseCoque;
    //  gAcc = gFrein = PowerOverType * hyperDriveType/masse;
    consommation = PowerOverType * hyperDriveType / PowerOverConso;
    if(model){
        
        Vect3D *pmin, min, max;
        pmin = [model boundingBox:&max];
        min = *pmin;

        Vect3D delta;
        diffVect(&max,&min,&delta);
        
        inertie.x = masse*(SQ(delta.y) + SQ(delta.z))/12.0;
        inertie.y = masse*(SQ(delta.x) + SQ(delta.z))/12.0;
        inertie.z = masse*(SQ(delta.x) + SQ(delta.y))/12.0;
        NSDebugMLLog(@"GGEnemi", @"%@:inertial vector = %@", self, stringOfVect2(inertie));
    }
}

- (void) _computeInternalData
{
    capacityFret = capacity;
    endurance = masseCoque;
    bouclier = 0.0;
    timeLaserRelaxation = 1.0;
    [self _updateInternalData];
}


- (void) _setDriveType: (int) blu
{
    Item *it = [Item hyperDriveCivil: blu];
    [self addItem: it];
}


- init
{
    [super init];
    [self _GGShipDefaultInitVar];
    [self _computeInternalData];
    
    equipementDico = [NSMutableDictionary new];
    xMouvement = yMouvement = rolMouvement = 0.0;
    power = 0.0;
    angleManche = 0;
    speedRotation = 1.0;
    powerRotation = 1.0;
    _sflags.invincible = NO;
    wishDir = *Direction(self);
    _cosam = 1;
    _sinam = 0;
    lastTime = -1.0;
    timeStartFire = -1.0;
    
    modeInertie = 0;
    
    
    return self;
}

- (void) reload
{
    NSString *val;
    if( (val = [property objectForKey: @"Modele"] ) != nil )
    {
        NSLog(@"Reloading of %@", val);
        [[Resource resourceManager] removeResourceForName: val];
        [self _loadModel: val
              dictionary: property];
    }
    [super reload];
}

- (void) setDefaultEquipment
    /* for NPC only */
{
    [self addItem: [Item hydrogen]
            count: 300 ];
}  


- (void) setVarWithDictionary: (NSDictionary *)dico
{
    NSString *val;
    
    if( dico == nil )
    {
        NSWarnMLog(@"setVarWithDictionary call with nil");
    }
    else
    {
        if( (val = [dico objectForKey: @"MasseCoque"] ) != nil )
        {
            masseCoque = [val doubleValue];
        }
        
        if( (val = [dico objectForKey: @"Capacite"] ) != nil )
        {
            capacity = [val doubleValue];
        }
        
        if( (val = [dico objectForKey: @"PylonRocket"] ) != nil )
        {
            pylonRocket = [val intValue];
        }
        
        if( (val = [dico objectForKey: @"Gun"] ) != nil )
        {
            pylonLaser = [val intValue];
        }
        
        if( (val = [dico objectForKey: @"MainAcc"] ) != nil )
        {
            gAcc = [val doubleValue]*10;  /*measured in g */
        }
        
        if( (val = [dico objectForKey: @"RetroAcc"] ) != nil )
        {
            gFrein = [val doubleValue]*10;
        }
        
        
        if( (val = [dico objectForKey: @"Modele"] ) != nil )
        {
            if ( ![self _loadModel: val
                        dictionary: dico ] )
            {
                NSString *val2;
                if ( (val2 = [dico objectForKey: @"AlternateModele"] ) != nil  &&
                     ![self _loadModel: val2
                            dictionary: dico] )
                {
                    NSWarnMLog(@"Unable to find model %@, neither %@, loading a basic one", val, val2);
                    [self _loadModel:  @"caiman.asc"
                          dictionary: dico];
                }
            }
        }
        
        
        if( (val = [dico objectForKey: @"ModelName"] ) != nil )
        {
            ASSIGN(modelName, val);
        }
        
        [self _computeInternalData];
        if( (val = [dico objectForKey: @"HyperDriveType"] ) != nil )
        {
            int n;
            n = [val intValue];
            if( n > 0 )
                [self _setDriveType: n];
        }
    }
}

- initWithDictionary: (NSDictionary *)dico
{
    
    [self init];
    [self setVarWithDictionary: dico];
    ASSIGN(property, dico);
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
#define GGEncodeFlag(f) [encoder encodeInt:(unsigned int)(_sflags.f) forKey:@#f]
    
    [super encodeWithCoder: encoder];
    [encoder encodeObject:equipementDico forKey:@"equipment"];
    GGEncode(property);
    [encoder encodeObject:target forKey:@"target"];
    GGEncode(ia);
    GGEncode(resultante);
    GGEncode(momentum);
    GGEncode(modeInertie);
    GGEncode(angleManche);
    GGEncode(_cosam);
    GGEncode(_sinam);
    GGEncode(speedRotation);
    GGEncode(powerRotation);
    GGEncode(power);
    GGEncode(xMouvement);
    GGEncode(yMouvement);
    
    GGEncode(endurance);
    GGEncode(bouclier);
    GGEncode(carburant);
    GGEncode(fric);
    
    GGEncode(currentLaser);
    GGEncode(currentMissile);
    {
        int i;
        for(i = 0; i < MAXLASER; ++i){
            [encoder encodeObject:laserGun[i] forKey:[NSString stringWithFormat:@"laser[%d]", i]];
        }
    }
    
    GGEncode(timeStartFire);
    GGEncode(laser);
    GGEncode(laserManager);
    
    GGEncode(lastTime);
    GGEncode(wishDir);
    GGEncode(pousse);
    
    GGEncodeFlag(invincible);
    GGEncode(indexSystem);
    GGEncode(eliteLevel);
}

- (void) _restoreEquipment: (NSDictionary *)dico
{
    ItemStock *is;
    NSEnumerator *en;
    NSParameterAssert(dico);
    
    en = [dico objectEnumerator];
    while( (is=[en nextObject]) != nil )
    {
        ShipErrorCode err;
        if( (err = [self addItem: is->item
                           count: is->cargo]) != NoError)
        {
            NSWarnMLog(@"hum %@ : %@", shipMsgFromErrorCode(err), is->item);
        }
    }
}

- (id)initWithCoder:(NSCoder *)decoder
{
#define GGDecodeFlag(f) _sflags.f = [decoder decodeIntForKey:@#f]
    GGShip *aTarget;
    NSDictionary *dico;
    
    [super initWithCoder: decoder];
    [self _GGShipDefaultInitVar];
    
    equipementDico = [NSMutableDictionary new];
    dico = [decoder decodeObjectForKey:@"equipment"];
    
    GGDecode(property);
    
    [self setVarWithDictionary: property];  //will load the model
    
    aTarget = [decoder decodeObjectForKey:@"target"];  //we don't retain.
    [self setTarget: aTarget];
    
    [self _restoreEquipment: dico];
    
    GGDecode(ia);
    GGDecode(resultante);
    GGDecode(momentum);
    GGDecode(modeInertie);
    GGDecode(angleManche);
    GGDecode(_cosam);
    GGDecode(_sinam);
    GGDecode(speedRotation);
    GGDecode(powerRotation);
    GGDecode(power);
    GGDecode(xMouvement);
    GGDecode(yMouvement);
    
    GGDecode(endurance);
    GGDecode(bouclier);
    GGDecode(carburant);
    GGDecode(fric);
    
    GGDecode(currentLaser);
    GGDecode(currentMissile);
    {
        int i;
        for(i = 0; i < MAXLASER; ++i){
            laserGun[i] = [[decoder decodeObjectForKey:[NSString stringWithFormat:@"laser[%d]", i]] retain];
        }
    }
    
    GGDecode(timeStartFire);
    GGDecode(laser);
    GGDecode(laserManager);
    
    GGDecode(lastTime);
    GGDecode(wishDir);
    GGDecode(pousse);
    
    GGDecodeFlag(invincible);
    GGDecode(indexSystem);
    GGDecode(eliteLevel);
    
    return self;
}


- (id) copyWithZone: (NSZone *)zone
{
    NSAssert(0, @"copy with a ship is forbidden");
    ggabort();
    return nil;
}

- (NSDictionary *) property
{
    return property;
}

- (BOOL) isCommandant
{      
    return NO;
}

- (void) earnMoney: (double) amount
{
    NSParameterAssert(amount >= 0);
    
    fric += amount;
}

- (ShipErrorCode) buyItem: (ItemStock *)it
{
    ShipErrorCode code;
    if( it->prixAchat <= fric )
    {
        if( (code = [self addItem: it->item]) == NoError )
        {
            fric -= it->prixAchat;
            return NoError;
        }
        else
            return code;
    }
    else
    {
        return NoMoney;
    }
}

- (ShipErrorCode) sellItem: (ItemStock *)it
{
    ShipErrorCode code;
    code = [self removeItem: it->item];
    
    if( code != NoError )
        return code;
    else
    {
        fric += it->prixVente;
        return NoError;
    }
}

- (void) _addItemIntoDictionary: (Item *)it
{
    ItemStock *stock;
    
    stock = [equipementDico objectForKey: it];
    
    if(stock == nil)
    {
        stock = [ItemStock itemStockWithItem: it];
        [equipementDico setObject: stock
                           forKey: it];
    }
    stock->cargo++;
}

- (ShipErrorCode) addItem: (Item *)it
{
    int i;
    NSDebugMLLog(@"addItem", @"obj %@ %d", it, it->type);
    if( [self nbrItemOfType: it->type] > 0 && [it oncePerShip] )
        return AlreadyThere;
    NSDebugMLLog(@"addItem", @"masse %g, item %g, capa %g",
                 masseFret, it->masse, capacity);
    if( it->masse + masseFret > capacity )
    {
        NSDebugMLLog(@"addItem", @"no place left");
        return NoPlaceLeft;
    }
    switch(it->type)
    {
        case Inspector:
            if ( _sflags.hasInspector )
                return AlreadyThere;
            _sflags.hasInspector = YES;
            break;
        case ShieldGenerator:
            nbr_shield_generator ++;
            break;
        case LaserPulsion:
            NSAssert(it->arg2 >= 0 && it->arg2 < pylonLaser,
                     @"bad");
            if( laserGun[it->arg2] == nil )
            {
                ASSIGN(laserGun[it->arg2], it);
                //FIXME: not necessary
                timeLaserRelaxation = 1.0;
            }
                else
                {
                    return GunBusy;
                }
                break;
        case LaserRayon:
            NSAssert(it->arg2 >= 0 && it->arg2 < pylonLaser,
                     @"bad");
            if( laserGun[it->arg2] == nil )
            {
                ASSIGN(laserGun[it->arg2], it);
                //FIXME: not necessary
                timeLaserRelaxation = 0.0;
            }
                else
                {
                    return GunBusy;
                }
                break;
        case Missile:
            for( i = 0; i < pylonRocket; ++i )
            {
                if( missiles[i] == nil )
                {
                    ASSIGN(missiles[i], it);
                    nbrMissile++;
                    break;
                }
            }
            if( i == pylonRocket )
                return MissilePylonBusy;
            break;
        case Hyperdrive: 
            if( _sflags.hasHyperDrive )
                return AlreadyThere;
            _sflags.hasHyperDrive = YES;
            hyperDriveType = hyperDriveTypeFromId(it->arg1);
            maxFuel = 2000*hyperDriveType;
            carburant = 0;
            break;
        case CloudAnalyzer:
            if( _sflags.hasCloudAnalyzer )
                return AlreadyThere;
            _sflags.hasCloudAnalyzer = YES;
            break;
        case Mine:
            break;
        case BombeEnergie:
            break;
        case ECM:
            break;
        case OtherEquip:
            switch(it->arg1)
            {
                case Auto_Refueler:
                    _sflags.hasAutoRefuel = YES;
                    break;
            }
            break;
        default:
            break;
    }
    if( it->type < GoodiesBegining )
        capacityFret -= it->masse;
    [self _addItemIntoDictionary: it];
    NSDebugMLLog(@"addItem", @"masse = %f", it->masse);
    masseFret += it->masse;
    [self _updateInternalData];
    
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationShipCargoChange
                  object: self];
    return NoError;
}

- (ShipErrorCode) addItem: (Item *)item
                    count: (int) n
{
    int i;
    for(i = 0; i < n; ++i)
    {
        ShipErrorCode code;
        code = [self addItem: item];
        if( code != NoError )
            return code;
    }
    return NoError;
}

- (ShipErrorCode) addEquipment: (enum __Equipment) it
{
    return [self addItem: [Item equipmentWithArg: it]];
}

- (ShipErrorCode) removeItem: (Item *)it
{
    int i;
    ItemStock *is;
    
    is = [equipementDico objectForKey: it];
    if( is == nil )
        return EquipmentNotFound;
    
    NSParameterAssert(is->cargo > 0);
    is->cargo--;
    
    if( is->cargo == 0 )
        [equipementDico removeObjectForKey: it];
    
    masseFret -=  it->masse;
    
    if( it->type < GoodiesBegining )
        capacityFret += it->masse;
    
    [self _updateInternalData];
    
    switch(it->type)
    {
        case Inspector:
            _sflags.hasInspector = NO;
            break;
        case ShieldGenerator:
            nbr_shield_generator --;
            break;
        case Hyperdrive:
            _sflags.hasHyperDrive = NO;
            hyperDriveType = 0;
            carburant = 0;
        case CloudAnalyzer:
            _sflags.hasCloudAnalyzer = NO;
            break;
        case LaserRayon:
        case LaserPulsion:
            for( i = 0; i < MAXLASER; ++i)
            {
                if( [laserGun[i] isEqual: it] )
                {
                    DESTROY(laserGun[i]);
                    break;
                }
            }
            break;
        case Missile:
            for( i = 0; i < MAXMISSILE; ++i)
            {
                if( [missiles[i] isEqual: it] )
                {
                    DESTROY(missiles[i]);
                    nbrMissile--;
                    break;
                }
            }
            break;
        case OtherEquip:
            switch(it->arg1)
            {
                case Auto_Refueler:
                    _sflags.hasAutoRefuel = NO;
                    break;
            }
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationShipCargoChange
                  object: self];
    
    return NoError;
}


- (ShipErrorCode) removeItem: (Item *)item
                       count: (int) n
{
    int i;
    for(i = 0; i < n; ++i)
    {
        ShipErrorCode code;
        code = [self removeItem: item];
        if( code != NoError )
            return code;
    }
    return NoError;
}


- (int) nbrItem: (Item *)it
{
    ItemStock *is;
    is = [equipementDico objectForKey: it];
    
    if( is == nil )
        return 0;
    else
        return is->cargo;
}


- (int) nbrItemOfType: (ItemType)type
{
    NSEnumerator *en;
    int ret = 0;
    ItemStock *is;
    en = [equipementDico objectEnumerator];
    
    while( (is=[en nextObject]) != nil )
    {
        NSParameterAssert(is->cargo > 0);
        if( is->item->type == type )
            ret += is->cargo;
    }
    return ret;
}

- (NSArray *) goodiesAndEquipment: (NSMutableArray *)equip
{
    ItemStock *is;
    NSMutableArray *result;
    NSEnumerator *en;
    result = [NSMutableArray array];
    
    en = [equipementDico objectEnumerator];
    
    while( (is=[en nextObject]) != nil )
    {
        if( is->item->type < GoodiesBegining )
            [equip addObject: is];
        else
        {
            NSParameterAssert(is->item->type > GoodiesBegining &&
                              is->item->type <= Other);
            [result addObject: is];
        }
    }
    return result;
}

- (float) rangeForHyperJump
{
    int nH = [self nbrItem: [Item hydrogen]];
    float coef;
    coef = nH / (hyperDriveType*hyperDriveType);
    if (nH > hyperDriveType ) nH = hyperDriveType;
    return distMaxJump(hyperDriveType, masseCoque+capacity) * nH /
        hyperDriveType;
}

static NSString *randomMatricule(void)
{
    return [NSString stringWithFormat: 
        @"%c%c-%d %c",'A'+irnd(26), 'A'+irnd(26), irnd(10000),
        'A'+irnd(26)];
}

+ shipWithFileName: (NSString *)name
{
    NSDictionary *dic;
    name = [[Resource resourceManager] pathForModel:   name];
    dic = [NSDictionary dictionaryWithContentsOfFile: name];
    if( dic == nil )
    {
        NSWarnMLog(@"can't init ship with %@", name);
        return nil;
    }
    else
    {
        GGShip *aShip = [self alloc];
        [aShip initWithDictionary: dic];
        [aShip setName: randomMatricule()];
        //[aShip setName: [aShip modelName]];
        return AUTORELEASE(aShip);
    }
}

- (ExtendedProcedure *) ia
{
    return ia;
}

- (void) setIA: (ExtendedProcedure*)aIa
{
    [ia invalidate];
    RETAIN(ia);
    AUTORELEASE(ia);
    ASSIGN(ia, aIa);
}

- (GGShip *) target
{
    return target;
}

- (void) setTarget: (GGSpaceObject *) newTarget
{
    if( newTarget == target )
        return;
    if( tracked && target != nil && [target isKindOfClass: [GGMobile class]] )
    {
        [(GGMobile *)target setTrack: NO];
    }
    
    target =  (GGShip *)newTarget;
    [newTarget enregistreForRemoving: self
                                with: @selector(removeTargetWithNotification:)];
    
    if( tracked && target != nil && [target isKindOfClass: [GGMobile class]] )
    {
        [(GGMobile *)target setTrack: YES];
    }
}

- (void) notifyAttackFrom: (GGShip *)ship
{
}

- (void) notify: (NSString *) format,...
{
}

- (void) removeTargetWithNotification:  (NSNotification *) aNotification
{
    if( [aNotification object] == target )
    {
        [self setTarget: nil];
        [self notify: @"Target lost!!"];
    }
}

- (void) setEliteLevel: (int) level
{
    NSParameterAssert(0 <= level && level < 30);
    eliteLevel = level;
}

- (int) eliteLevel
{
    return eliteLevel;
}

- (void) invalidate
{
    DESTROY(model);
    [self setIA: nil];
    [laserManager invalidate];
    [super invalidate];
}

- (void) dealloc
{
    int i;
    //FIXME
    //Just in case, it should be already be done.
    [ia invalidate];
    [laserManager invalidate];
    
    for( i = 0; i < MAXMISSILE; ++i )
        RELEASE(missiles[i]);
    for( i = 0; i <MAXLASER ; ++i)
        RELEASE(laserGun[i]);
    RELEASE(modelName);
    RELEASE(model);
    RELEASE(ia);
    RELEASE(equipementDico);
    RELEASE(laser);
    RELEASE(property);
    RELEASE(laserManager);
    [super dealloc];
}

- (void) destroy
{
    //  [ia setShip: nil];
    /*this is very bad because the ia could invoke this method*/
    [ia invalidate];
    [laserManager invalidate];
    [super destroy];
}

- (void) reallyVanish
{
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationRemoveFromCurrent 
                  object: self];
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationVanishing
                  object: self];
    
    [self invalidate];
}


- (void) vanish
{
    [ia invalidate];
    [laserManager invalidate];
    [theWorld addToKillPool: self
                   selector: @selector(reallyVanish)];
}

- (float) power
{
    return power;
}

- (void) setPower: (float) newPower
{
    _flags.stateChange = YES;
    NSDebugMLLogWindow(@"Power", @"carburant = %g", carburant);
    if( carburant <= 0 && ![self autoRefuel])
    {
        power = 0.0;
        return;
    }
    NSDebugMLLogWindow(@"Power", @"after test", carburant);
    if( newPower > 1 )
        power = 1.0;
    else if (newPower < 0)
        power = 0.0;
    else
        power = newPower;
}

- (void) looseTarget: (GGSpaceObject *)obj
{
    [self stopAutoPilot];
    [self notify: @"target %@ lost!", obj];
    [self setTarget: nil];
}


- (void) stopEngine
{
    [self stopAutoPilot];
    [self stop];
}

- (void) stop
{
    modeInertie = 0;
    [self setPower: 0.0];
}

- (void) stopAutoPilot
{
    [self stop];
}

- (void) setXMouvement: (float) x
{
    xMouvement = x;
}

- (void) setYMouvement: (float) y
{
    yMouvement = y;
}

- (void) setRolMouvement: (float) val
{
    rolMouvement = val;
}

- (void) goLeft
{
    modeInertie |= GGLeft;
}

- (void) stopGoLeft
{
    modeInertie &= ~GGLeft;
}

- (void) goRight
{
    modeInertie |= GGRight;
}

- (void) stopGoRight
{
    modeInertie &= ~GGRight;
}

- (void) goUp
{
    modeInertie |= GGUp;
}

- (void) stopGoUp
{
    modeInertie &= ~GGUp;
}

- (void) goDown
{
    modeInertie |= GGDown;
}

- (void) stopGoDown
{
    modeInertie &= ~GGDown;
}

- (void) rolLeft
{
    modeInertie |= GGStrafLeft;
}
- (void) stopRolLeft
{
    modeInertie &= ~GGStrafLeft;
}

- (void) rolRight
{
    modeInertie |= GGStrafRight;
}
- (void) stopRolRight
{
    modeInertie &= ~GGStrafRight;
}

- (void) accelere
{
    modeInertie |= GGAccelere;
    [self setPower: 1.0];
}

- (void) stopAccelere
{
    modeInertie &= ~GGAccelere;
    [self setPower: 0.0];
}

- (void) decelere
{
    modeInertie |= GGDecelere;
    [self setPower: 1.0];
}

- (void) stopDecelere
{
    modeInertie &= ~GGDecelere;
    [self setPower: 0.0];
}


- (void) startManche
{
    modeInertie |= GGManche;
}

- (void) stopManche
{
    modeInertie &= ~GGManche;
}


- (void) setPousse: (Vect3D *)dir
{
    pousse = *dir;
    modeInertie |= GGPousse;
}

- (void) stopPousse
{
    modeInertie &= ~GGPousse;
    [self setPower: 0.0];
}

- (void) setAngleManche: (float) alpha
{
    angleManche = alpha;
    _cosam = cos(alpha);
    _sinam = sin(alpha);
}

- (void) startWishDirection
{
    modeInertie |= GGWishDirection;
}

- (void) stopWishDirection
{
    modeInertie &= ~GGWishDirection;
}

- (void) setWishDirection: (Vect3D *)vect
              speedManche:(float) val
    /* this imply starWishDirection*/
{
    wishDir = *vect;
    normaliseVect(&wishDir);
    modeInertie |= GGWishDirection;
    [self setSpeedManche: val];
}

- (void) setSpeedManche: (float) val
{
    if( val < 0 )
        val = 0;
    else if( val > 1 )
        val = 1;
    powerRotation = val;
}

- (void) _adjustMatrixWithDirection: (Vect3D *) pdir
                           orthoDir: (Vect3D *) pv
                               cosb: (float) cosb
                               sinb: (float) sinb
{
    Vect3D 		axe, haut;
    GGReal			x, y;
    
    
    prodVect(Direction(self), pv, &axe);
    
    /*Direction(self), v, axe forment un repère orthonormé*/
    
    x = prodScal(Haut(self), &axe);
    y = prodScal(Haut(self), pv);
    mulScalVect(&axe, x, &haut);
    addLambdaVect(&haut, cosb*y, pv, &haut);
    addLambdaVect(&haut, -sinb*y, Direction(self), &haut);
    
    *Direction(self) = *pdir;
    *Haut(self) = haut;
    
    [self normalise];
}

#pragma mark UpdatePostion
- (void) updatePosition
{
    BOOL isCommandant = NO;
    float x, y, rol;
    Vect3D wishedOmega = GGZeroVect;
    
    x = y = rol = 0;
#define LogCommandant(fmt,  args...) do{if(1 || self == theCommandant) NSDebugMLLogWindow(@"UpdateCommandant", fmt , ##args);}while(0)
    
    if( modeInertie & GGManche )
    {
        Vect3D 		v;
        GGReal			beta;
        GGReal			cosb, sinb;
        beta = speedRotation*realDeltaTime;
        if( beta < 1 )
            /* we do nothing otherwise because it turns too fast
            */
        {
            LogCommandant(@"beta = %g", beta);
            Vect3D		newDir;
            mulScalVect(Haut(self), _cosam, &v);
            addLambdaVect(&v, _sinam, Gauche(self), &v);
            
            /*	  [theTableau addLogString: [NSString stringWithFormat:@"angle=%e",
                angleManche]];

[theTableau addLogString: [NSString stringWithFormat:@"|v|=%e",
		  prodScal(&v, &v)]];
*/
            cosb = cos(beta);
            sinb = sin(beta);
            mulScalVect(Direction(self), cosb, &newDir);
            addLambdaVect(&newDir, sinb, &v, &newDir);
            
            [self _adjustMatrixWithDirection: &newDir
                                    orthoDir: &v
                                        cosb: cosb
                                        sinb: sinb];
        }
        else{
            LogCommandant(@"turn too fast");
        }
    }
    if( modeInertie & GGWishDirection )
    {
        Vect3D 	tmp, v;
        GGReal 		prodWishDir;
        GGReal		beta;
        GGReal		cosb, sinb;
        
        prodVect(&wishDir, Direction(self), &tmp);
        prodWishDir = prodScal(&tmp, &tmp);
        
        if( prodWishDir <= EPS )
        {
            *Direction(self) = wishDir;
            _flags.majRepere = YES;
            [self stopWishDirection];
            [[NSNotificationCenter defaultCenter]
	    postNotificationName: GGNotificationWishedDirection	
                      object: self];
        }
        else
        {
            beta = powerRotation*speedRotation*deltaTime;
            prodVect(Direction(self), &tmp, &v);
            normaliseVect(&v);
            if( beta > 2 )
            {
                cosb = prodScal(&wishDir, Direction(self));
                sinb = prodScal(&wishDir, &v);
                [self _adjustMatrixWithDirection: &wishDir
                                        orthoDir: &v
                                            cosb: cosb
                                            sinb: sinb];
            }
            else
            {
                GGReal tmpcos;
                Vect3D tdir;
                cosb = cos(beta);
                if( cosb < (tmpcos = prodScal(&wishDir, Direction(self))) )
                {
                    cosb = tmpcos;
                    sinb = prodScal(&wishDir, &v);
                    tdir = wishDir;
                }
                else
                {
                    sinb = sin(beta);
                    mulScalVect(Direction(self), cosb, &tdir);
                    addLambdaVect(&tdir, sinb, &v, &tdir);
                }
                [self _adjustMatrixWithDirection: &tdir
                                        orthoDir: &v
                                            cosb: cosb
                                            sinb: sinb];
            }
        }
    }
    if( modeInertie & GGLeft )
        x -= 1.0;
    if( modeInertie & GGRight )
        x += 1.0;
    if( modeInertie & GGUp )
        y += 1.0;
    if( modeInertie & GGDown )
        y -= 1.0;
    if( modeInertie & GGAccelere )
        addLambdaVect(Pousse(self), power*gAcc, Direction(self), Pousse(self));
    if( modeInertie & GGDecelere )
        addLambdaVect(Pousse(self), -power*gFrein, Direction(self), Pousse(self));
    if( modeInertie & GGStrafLeft )
        rol += 1;
    if( modeInertie & GGStrafRight )
        rol -= 1;
    if( modeInertie & GGPousse )
        addLambdaVect(Pousse(self), power*gAcc, &pousse, Pousse(self));
    
    x += xMouvement;
    y -= yMouvement;
    rol -= rolMouvement;
    
    if( fabs(x) > 0.01 )
    {
        x = GGClamp(x);
        wishedOmega.y = -x;
        isCommandant = YES;
    }
    if( fabs(y) > 0.01 )
    {
        y = GGClamp(y);
        wishedOmega.x = y;
        isCommandant = YES;
    }
    
    if( fabs(rol) > 0.01 )
    {
        rol = GGClamp(rol);
        wishedOmega.z = -rol;
        isCommandant = YES;
    }
    
    if(isCommandant){
        divScalVect(&wishedOmega,[theLoop timeSpeed],&wishedOmega);
    }
    
#if DEBUG_COCOA
    if(!controlDisable)
#endif
    {
        Vect3D temp;
        double delta = (isCommandant ? realDeltaTime : deltaTime);
        
        mulScalVect(Pousse(self), deltaTime, &temp);
        addVect(Speed(self), &temp, Speed(self));
        
        if( thePref.DirectionHinter )
        {
            GGReal normeDiff;
            diffVect(&wishedOmega, Omega(self), &temp);
            normeDiff = prodScal(&temp, &temp);
            if( normeDiff > EPS*EPS)
            {
                GGReal normeInertie = normeSup(&inertie);
                GGReal maxPossibleMomentum = normeInertie * MaxAngularAccel;
                LogCommandant(@"correction because %g\n NI=%g", normeDiff, normeInertie);
                LogCommandant(@"inertie = \n%@", stringOfVect(&inertie));

                //		normeDiff = sqrt(normeDiff);
                //		divScalVect(&temp, normeDiff, &temp);
                dualProduct(&temp, &inertie, &temp);
                normeDiff = prodScal(&temp, &temp);
                LogCommandant(@"inertie = \n%@\nnormeDiff = %g", stringOfVect(&inertie),
                              normeDiff);
                if( normeDiff <= square(delta * maxPossibleMomentum)){
                    LogCommandant(@"almost");
//                    *Omega(self) = wishedOmega;
                    divScalVect(&temp,delta,&temp);
                }
                else{
                    LogCommandant(@"far");
                    mulScalVect(&temp, maxPossibleMomentum/sqrt(normeDiff),
                                &temp);
                }
                addVect(Momentum(self), &temp, Momentum(self));
            }
            else{
                LogCommandant(@"set to %@", stringOfVect(Omega(self)));
                *Omega(self) = wishedOmega;
            }
        }
        else
            addVect(Momentum(self), &wishedOmega, Momentum(self));
        
        initVect(&temp, delta*(Momentum(self)->x)/inertie.x,
                 delta*(Momentum(self)->y)/inertie.y,
                 delta*(Momentum(self)->z)/inertie.z);
        
        addVect(Omega(self), &temp, Omega(self));
        
    }
    SetZeroVect(*Pousse(self));
    SetZeroVect(*Momentum(self));
    
    
    [super updatePosition];
}

- (double) maxShield
{
    return 5e3*nbr_shield_generator;
}

- (BOOL) autoRefuel
{
    if ( ( _sflags.hasAutoRefuel || self != (id)[theWorld commandant] ) && 
         [self removeItem: [Item hydrogen]] == NoError )
    {
        carburant += 1000;
        return YES;
    }
    else
        return NO;
}

- (void) updateShipData
{
    NSDebugMLLogWindow(@"Carburant", @"%g %g", lastTime, gameTime);
    if( lastTime >= 0 )
    {
        double diffTime = gameTime - lastTime;
        if( carburant > 0)
            carburant -= power*consommation*diffTime;
        
        if( carburant <= 0 )
        {
            if( ![self autoRefuel] &&  power > 0 )
            {
                [self notify: @"No more fuel"];
                [self setPower: 0];
                carburant = 0;
            }
        }
        if ( nbr_shield_generator > 0)
        {
            double max = [self maxShield];
            
            bouclier += nbr_shield_generator * diffTime * (5e3/30);
            if (bouclier > max )
                bouclier = max;
            
        }
    }
    lastTime = gameTime;
}

- (double) carburant
{
    return carburant;
}

- (void) setCarburant: (double) val
{
    carburant = val;
}

- (BOOL) lockMissile
{
    if( nbrMissile == 0 )
    {
        [self notify: @"No more Missile to lock"];
        return NO;
    }
    else if( currentMissile >= 0 && missiles[currentMissile] != nil )
    {
        [self notify: @"missile %d unlock", currentMissile+1];
        currentMissile = -1;
        return NO;
    }
    else
    {
        int i;
        for(i = 0; i < MAXMISSILE; ++i)
        {
            if( missiles[i] != nil )
            {
                currentMissile = i;
                [self notify: @"missile %d lock", currentMissile+1];
                break;
            }
        }
        NSParameterAssert(i < MAXMISSILE);
        return YES;
    }
}

- (GGShip *) fireMissile
{
    GGShip 		*unShip;
    ShipErrorCode		code;
    
    if( currentMissile < 0 || missiles[currentMissile] == nil )
    {
        consoleLog(@"no locked missile");
        return nil;
    }
    
    code = [self removeItem: missiles[currentMissile]];
    
    NSAssert(code == NoError, @"bad");
    
    currentMissile = -1;
    
    
    unShip = [GGShip missileFrom: self
                          target: target];
    
    [soundManager playFile: @"Rocket1.wav"];
    
    return unShip;
}

- (void) startFireLaser
{
    NSDebugMLLog(@"Laser", @"start firing laser %g", timeLaserRelaxation);
    
    if( laserGun[0] == nil )
        return;
    if( timeLaserRelaxation > 0 )
    { 
        if(laserManager == nil)
        {
            Complex_manage_laser *ml;
            ml = [Complex_manage_laser allocInstance];
            [ml runWitharg1: self
                       arg2: 1];
            laserManager = ml;
        }
    }
    else if (!laser)
    {
        [laser destroy];
        ASSIGN(laser, [Laser fireLaserAt: self
                                   power: 1e6*powerLaserFromId(laserGun[0]->arg1)
                                lifeTime: 0]);
    }
    
}

- (void) stopFireLaser
{
    NSDebugMLLog(@"Laser", @"stop firing laser %@", laser);
    [laser destroy];
    DESTROY(laser);
    if( laserManager )
    {
        [laserManager invalidate];
        AUTORELEASE(laserManager);
        laserManager = nil;
    }
}

- (void) fireLaserShort
{
    if( timeStartFire + timeLaserRelaxation < gameTime )
    {
        [Laser fireLaserAt: self
                     power: 1e6*powerLaserFromId(laserGun[0]->arg1)
                  lifeTime: 0.2];
        timeStartFire = gameTime;
    }
}

- (float) energyForExplosion
{
    NSString *val;
    if( (val = [property objectForKey: @"EnergyExplosion"] ) != nil )
    {
        return [val floatValue];
    }
    else
        return masse;
}

- (void) doExplosion
{
    if( _flags.alive ) /*we are sure that a ship will explode only once !!!*/
    {
        NSDebugMLLog(@"Collision", @"%@ BOOOOOOMMM!", self);
        [self destroy];
        if( [self isLocal] )
            [theWorld genereExplosionFrom: self
                               withEnergy: [self energyForExplosion]];
    }
}

- (void) chocWithEnergy: (GGReal) val
{
    if( _sflags.invincible || endurance == 0)
        return;
    NSDebugMLLog(@"Collisions", @"%@ Energie du choc : %g\nendurance %g", 
                 self, val, endurance);
    
    //  NSDebugMLLog(@"Collision", @"%@ dans la gueule", self);
    if( val < bouclier )
        bouclier -= val;
    else
    {
        val -= bouclier;
        bouclier = 0;
        if( val < endurance )
            endurance -= val;
        else
        {
            endurance = 0;
            [self doExplosion];
        }
    }
}


- (void) setMasseCoque: (float) theMasse
{
    masse = theMasse;
}

- (void) setInvincibility: (BOOL) flag
{
    _sflags.invincible = flag;
}

- (void) enterUniverse
{
    [theLoop slowSchedule:self
             withSelector: @selector(updateShipData)
                      arg: nil];
    [super enterUniverse];
}

- (BOOL) shouldDrawMoreThings
{
    return YES;
}

- (void) rasteriseMoreThings
{
    GLUquadricObj *quad = gluNewQuadric();
    gluQuadricNormals(quad, GLU_NONE);
    glPushAttrib(GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);
    
    glDisable(GL_LIGHTING);
    glDisable(GL_CULL_FACE);
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(1, 1, 1);
    glVertex3f(posReacteur1.x, posReacteur1.y, posReacteur1.z);
    glColor3f(0.4+0.4*power, 0.4+0.4*power, 0.6+0.4*power);
    glVertex3f(posReacteur1.x-0.2, posReacteur1.y-0.2, posReacteur1.z);
    glVertex3f(posReacteur1.x-0.2, posReacteur1.y+0.2, posReacteur1.z);
    glVertex3f(posReacteur1.x+0.2, posReacteur1.y+0.2, posReacteur1.z);
    glVertex3f(posReacteur1.x+0.2, posReacteur1.y-0.2, posReacteur1.z);
    glVertex3f(posReacteur1.x-0.2, posReacteur1.y-0.2, posReacteur1.z);
    glEnd();
    
    glBegin(GL_TRIANGLE_FAN);
    glColor3f(1, 1, 1);
    glVertex3f(posReacteur2.x, posReacteur2.y, posReacteur2.z);
    glColor3f(0.4+0.4*power, 0.4+0.4*power, 0.6+0.4*power);
    glVertex3f(posReacteur2.x-0.2, posReacteur2.y-0.2, posReacteur2.z);
    glVertex3f(posReacteur2.x-0.2, posReacteur2.y+0.2, posReacteur2.z);
    glVertex3f(posReacteur2.x+0.2, posReacteur2.y+0.2, posReacteur2.z);
    glVertex3f(posReacteur2.x+0.2, posReacteur2.y-0.2, posReacteur2.z);
    glVertex3f(posReacteur2.x-0.2, posReacteur2.y-0.2, posReacteur2.z);
    glEnd();
    
    
    glEnable(GL_BLEND);
    glEnable(GL_CULL_FACE);
    glDepthMask(GL_FALSE);
    
    if( power >= 0.05 )
    {
        glBegin(GL_TRIANGLE_FAN);
        glColor4f(0.4, 0.4, 1.0, 0.0);
        glVertex3f(posReacteur1.x, posReacteur1.y, posReacteur1.z-60*power);
        glColor4f(0.4+0.4*power, 0.4+0.4*power, 0.6+0.4*power, 0.8);
        glVertex3f(posReacteur1.x-0.2, posReacteur1.y-0.2, posReacteur1.z);
        glVertex3f(posReacteur1.x-0.2, posReacteur1.y+0.2, posReacteur1.z);
        glVertex3f(posReacteur1.x+0.2, posReacteur1.y+0.2, posReacteur1.z);
        glVertex3f(posReacteur1.x+0.2, posReacteur1.y-0.2, posReacteur1.z);
        glVertex3f(posReacteur1.x-0.2, posReacteur1.y-0.2, posReacteur1.z);
        glEnd();
        
        glBegin(GL_TRIANGLE_FAN);
        glColor4f(0.4, 0.4, 1.0, 0.0);
        glVertex3f(posReacteur2.x, posReacteur2.y, posReacteur2.z-60*power);
        glColor4f(0.4+0.4*power, 0.4+0.4*power, 0.6+0.4*power, 0.8);
        glVertex3f(posReacteur2.x-0.2, posReacteur2.y-0.2, posReacteur2.z);
        glVertex3f(posReacteur2.x-0.2, posReacteur2.y+0.2, posReacteur2.z);
        glVertex3f(posReacteur2.x+0.2, posReacteur2.y+0.2, posReacteur2.z);
        glVertex3f(posReacteur2.x+0.2, posReacteur2.y-0.2, posReacteur2.z);
        glVertex3f(posReacteur2.x-0.2, posReacteur2.y-0.2, posReacteur2.z);
        glEnd();
        
    }
    
    glColor4f(0.7, 0.7, 1, 0.2*power);
    glPushMatrix();
    glTranslatef(posReacteur1.x, posReacteur1.y, posReacteur1.z);
    gluSphere(quad, 1, 10, 10);
    glPopMatrix();
    glPushMatrix();
    glTranslatef(posReacteur2.x, posReacteur2.y, posReacteur2.z);
    gluSphere(quad, 1, 10, 10);
    glPopMatrix();
    
    glPopAttrib();
    gluDeleteQuadric(quad);
}

- (void) rasterise: (Camera *)cam
{
#if 0
    static GGShader* shader = nil;
    if(nil == shader){
    shader = [GGShader bumpShader];
    [shader initLazy];
    }
    [shader renderFrame];
#else
    [model rasterise:cam];
#endif
    GLCHECK;
}

- (void) addMomentum: (Vect3D *)v
          resultante: (Vect3D *)w
{
    addVect(Momentum(self), v, Momentum(self));
    addVect(Pousse(self), w, Pousse(self));
}

- (void) addResultante: (Vect3D *)v
{
    addVect(Pousse(self), v, Pousse(self));
}

- (int) indexSystem
{
    return indexSystem;
}

- (void) setIndexSystem: (int) index
{
    if ( indexSystem >= 0 && index < 0 )
    {
        [[NSNotificationCenter defaultCenter]
        postNotificationName: GGNotificationShipDoHyperJump
                      object: self];
    }
    indexSystem = index;
}

- (void) setTrack: (BOOL) val
{
    [super setTrack: val];
    if( target != nil && [target isKindOfClass: [GGMobile class]])
        [(GGMobile *) target setTrack: val];
}


- (NSString *) modelName
{
    return modelName;
}

- (void) logData: details
{
    GGShip *ship = self;
    [details clear];
    [details log:@"ship'id:           %@", ship];
    [details log:@"model:             %@", [ship modelName]];
    [details log:@"Hull mass:         %-.5g t", ship->masseCoque/1000.0];
    [details log:@"Capacity:          %-.5g t", ship->capacity/1000.0];
    [details log:@"Hyper Drive:       type %d", ship->hyperDriveType];
    [details log:@"Manufacturer:      %@", 
        [ship->property objectForKey: @"Manufacturer"]];
    [details log:@"Designer:          %@", 
        [ship->property objectForKey: @"Designer"]];
    [details log:@"Crew:              %@", 
        [ship->property objectForKey: @"Crew"]];
    [details log:@"Gun Mounting:      %d", ship->pylonLaser];
    [details log:@"Missile Pylons:    %d", ship->pylonRocket];
    [details log:@"MAIN THRUSTER ACCELERATION:  %.2g g", ship->gAcc/10.0];
    [details log:@"Retro THRUSTER ACCELERATION:  %.2g g", ship->gFrein/10.0];
    [details log:@"@rHyperdrive Ranges:@w"];
    {
        int i;
        NSMutableString *str, *str2;
        str = [NSMutableString string];
        str2 = [NSMutableString string];
        for(i = 1; i <= 8; ++i)
        {
            GGReal d;
            Item *item = [Item hyperDriveCivil: i];
            [str appendFormat: @"Class %d  ", i];
            NSDebugMLLog(@"Item", @"%@ %f %d", item, item->masse, item->arg1);
            if ( capacity > item->masse )
                d = distMaxJump(i, masseCoque + capacity);
            else
                d = 0;
            
            [str2 appendFormat: @"%6.2f   ", d ];
        }
        [details log: str];
        [details log: str2];
    }
}

- (ModelBlender*) model
{
    return model;
}

- (void) setModel: (ModelBlender*) _model
{
    [self _setModel:_model];
    [self _updateInternalData];
}


- (GGSolid*) createDynamicalHelper
{
    NSAssert([self dynamicalHelper] == nil, @"cannot have an helper when creating one");
    ModelBlender* aModel = [self model];
    GGSolid* helper = nil;
    if(aModel){
        helper = [[[GGSolid alloc] init] autorelease];
        [helper setModel:aModel];
        [helper setMasse:masse];
        helper->_position = repere.matrice;
        [helper setInertialVector:inertie];
    }
    
    return helper;
}
@end

@implementation GGShip (Private)

- (BOOL) _loadModel: (NSString *) val
         dictionary: (NSDictionary *) dico
{
    ModelBlender* m = [GGModel modelWithName: val
                                     properties: dico];
    if( m == nil )
        return NO;
    [self _setModel: m];
    ASSIGN(modelName, [val stringByDeletingPathExtension]);
    return YES;
}

- (void) _setModel: (ModelBlender*) _model
{
    GGMatrix4 mat = {
    {0, 0, 1},0,
    {1, 0, 0},0,
    {0, 1, 0},0,
    {0, 0, 0},1,    
    };
    ModelBlender* tmp = [_model transformModel:&mat];
    ASSIGN(model, tmp);
    if( model != nil ){
        Vect3D *min, max;
        
        min = [model boundingBox: &max];
        initVect(&posReacteur1, -3.32, 0.44, min->z-1);
        initVect(&posReacteur2, 3.32, 0.44, min->z-1);

        /* compute size of object */

        GGReal r;
        NSDebugMLLog(@"GGEnemi", @"chargement ok de %@", self);
        
        r = fabs(min->x);
        r = (r > fabs(min->y) ? r : fabs(min->y));
        r = (r > fabs(min->z) ? r : fabs(min->z));
        r = (r > fabs(max.x) ? r : fabs(max.x));
        r = (r > fabs(max.y) ? r : fabs(max.y));
        r = (r > fabs(max.z) ? r : fabs(max.z));
        NSDebugMLLog(@"GGEnemi", @"radius = %g", r);
        dimension = r;
    }
}

float timeForJump(GGReal dist, GGShip *ship)
{
    int type = HyperDriveType(ship);
    
    
    return (7*24*3600)*dist / distMaxJump(type, ship->masseCoque + 
                                          ship->capacity);
}

@end

NSString *shipMsgFromErrorCode(ShipErrorCode code)
{
    switch(code)
    {
        case NoError: 
            return @"";
        case NoMoney:
            return @"Not enought Money";
        case GunBusy:
            return @"No Laser gun slot available";
        case MissilePylonBusy:
            return @"No rocket slot available";
        case   NoPlaceLeft:	
            return @"No space left";
        case AlreadyThere:
            return @"You already have this item";
        case  EquipmentNotFound:
            return @"You don't have that";
        default:
            return @"Impossible, do a bug report!!!";
    }
}

