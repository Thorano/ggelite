/*
 *  Station.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSNotification.h>


#import "Station.h"
#import "GGRepere.h"
#import "GGLoop.h"
#import "Metaobj.h"
#import "Tableau.h"
#import "Texture.h"
#import "Sched.h"
#import "World.h"
#import "NSWindow.h"
#import "GGWidget.h"
#import "GGShip.h"
#import "GGConcreteShip.h"
#import "utile.h"
#import "parseASC.h"
#import "GGMenu.h"
#import "Item.h"
#import "Commandant.h"
#import "Goodies.h"
#import "GGFont.h"
#import "ShipWin.h"
#import "GGInput.h"
#import "System.h"
#import "Console.h"
#import "Resource.h"
#import "Board.h"
//#import "scripts/station.h"

#define SQ(x) ((x)*(x))

static NSString *bible[] = 
{
@"Judah",
@"Samuel",
@"Solomon",
@"Joshua",
@"Peter",
@"Abraham",
@"Hezekiah",
@"Ephraim",
@"Jeroboam",
@"Moab",
@"Isaac",
@"Jehoshaphat",
@"Esther",
@"Eleazar",
@"Nebuchadnezzar",
@"Zedekiah",
@"Abimelech",
@"Elisha",
@"Timothy",
@"Rehoboam",
@"Laban",
@"Zadok",
@"Samaria",
@"Balaam",
@"Zechariah",
@"Rachel",
@"Nathan",
@"Nebuchadrezzar",
@"Jehoiakim",
@"Ishmael",
@"Micah",
@"Benaiah",
@"Caleb",
@"Balak",
@"Nehemiah",
@"Gedaliah",
@"Johanan",
@"Baasha",
@"Hazael",
@"Shemaiah",
@"Bethel",
@"Damascus",
@"Adonijah",
@"Barnabas",
@"Hilkiah",
@"Shaphan",
@"Ahithophel",
@"Micaiah",
@"Ahikam",
@"Nebuzaradan",
@"Cain",
@"Sennacherib",
@"Hamath",
@"Rabshakeh",
@"Elkanah",
@"Timotheus",
@"Miriam",
@"Eliakim",
@"Sanballat",
@"Menahem",
@"Eliashib",
@"Ashdod",
@"Hazor",
@"Meshullam",
@"Zacharias",
@"Baalhanan",
@"Baalim",
@"Nathanael",
@"Gamaliel",
@"Goliath",
@"Ashkelon",
@"Husham",
@"Anakims",
@"Chenaanah",
@"Achbor",
@"Ashtaroth",
@"Ziklag",
@"Mahanaim",
@"Elnathan",
@"Esarhaddon",
@"Haggith",
@"Gallio",
@"Jether",
@"Ikkesh",
@"Mizpah",
@"Nicodemus",
@"Nimrim",
@"Adriel",
@"Zipporah",
@"Julius",
@"Rockwell",
@"Achor",
@"Baalhermon",
@"Mahlon",
@"Jehoaddan",
@"Shalmaneser",
@"Shuppim",
@"Laadan",
@"Jokneam",
@"Charity",
@"Emerald",
@"Shavsha",
@"Baalis",
@"Eshtaol",
@"Kartan",
@"Ashteroth",
@"Memucan",
@"Kirhareseth",
@"Jattir",
@"Jahzah",
@"Rebecca",
@"Shearjashub",
@"Azrikam",
@"Kibzaim",
@"Sargon",
@"Cyrenius",
@"Shaaph",
@"Engannim",
@"Elihoreph",
@"Elimelech",
@"Damascenes",
@"Andronicus",
@"Gebim",
@"Bileam",
@"Kirharaseth",
@"Sabaoth",
@"Jozachar",
@"Nephthalim",
@"Achaz",
@"Bernice",
@"Hothan",
@"Onam",
@"Castor",
@"Amasiah",
@"Jokshan",
@"Siddim",
@"Ammizabad",
@"Gabriel",
@"Jaakan",
@"Tryphena",
@"Tristan",
@"Hammon",
@"Galatia",
@"Naamathite",
@"Silvanus",
@"Kiriathaim",
@"Demetrius",
@"Joanna",
@"Ebenezer",
@"Astaroth",
@"Azor",
@"Zaanan",
@"Ithream",
@"Capernaum",
@"Bethhoglah",
@"Shammoth",
@"Sedona",
@"Shamir",
@"Nicolas",
@"Sirion",
@"Taphath",
@"Rekem",
@"Daberath",
@"Dionysius",
@"Baalberith",
@"Gaddiel",
@"Elzabad",
@"Joshaphat",
@"Marcus",
@"Iscariot",
@"Demas",
@"Phygellus",
@"Hermogenes",
@"Betharbel",
@"Jeshurun",
@"Fitzgerald",
@"Calisto",
@"Hammothdor"
};

static NSString *typestation[]= {
  @"Fortress %@",
  @"Camp %@",
  @"%@'s Station",
  @"%@  Town",
  @"%@  City",
  @"New %@",
  @"Old %@",
  @"%@ Village",
  @"%@",
  @"%@ Citadel",
  @"%@ Relay",
};

NSString *GGNotificationShipEnteredStation=
@"GGNotificationShipEnteredStation";

NSString *GGNotificationPlayerEnteredStation=
@"GGNotificationPlayerEnteredStation";
NSString *GGNotificationPlayerLeaveStation=
@"GGNotificationPlayerLeaveStation";


@interface StationWindow : TopView
{
  Station *station;
}
+ stationView: (Station *)s;
@end

@interface RepairView : TopView
{
  GGShip *ship;
}
+ repairView;
@end

@interface NewShipWindow: TopView
{
  Station 		*station;
  ShipView		*shipView;
  Console		*details;
  int			current;
}
+ shipViewWith: (Station *) s;
@end

@interface Station (PrivateMethod)
- (void) endSession;
@end

@implementation Station
- (void) calculeTailleSysteme
{
  tailleSysteme = 10000e3;
}

- init
{
  [super init];
  distanceLocalOrbiteur = SQ(100e3);
  distanceGlobalOrbiteur = SQ(500e3);
  dimension = 1e3;
  masse = 10e6;
  ships = [NSMutableArray new];

  return self;
}

- (void) setWithDictionary: (NSDictionary *)dic
		      name: (NSString *)_name
{
  [super setWithDictionary: dic
	 name: _name];

  modelName = RETAIN([dic objectForKey:@"Modele"]);
  alternateModelName = RETAIN([dic objectForKey:@"AlternateModele"]);
  ASSIGN(dico, dic);
}

- (void) setRandomStationAtRadius: (GGReal) r
{
  NSString *models[] =
    {
      @"station.3ds",
      @"StationV.3ds"
    };
  ParametresOrbitaux prm =
  {
    0,
    0,
    0,

    0.,
    0,
    0
  };
  prm.a = r;
  prm.omegaPropre = 2e-1;
  prm.M = vrnd()*2*M_PI;

  [self setParametresOrbitaux: &prm];

#define randomel(tab)  (tab[irnd(sizeof(tab))/sizeof(NSString *)])

  ASSIGN(modelName, randomel(models));
  ASSIGN(alternateModelName, @"station3.asc");
  {
    [self setName: [NSString stringWithFormat: randomel(typestation), 
			     randomel(bible)]];
  }
}

- (void) dealloc
{
  RELEASE(visiteur);
  RELEASE(aModel);
  RELEASE(modelName);
  RELEASE(alternateModelName);
  RELEASE(dico);
  RELEASE(ships);
  [super dealloc];
}

- (id) copyWithZone: (NSZone *)zone
{
  Station *copy = [super copyWithZone: zone];
  copy->visiteur = nil;
  RETAIN(copy->modelName);
  RETAIN(copy->alternateModelName);
  RETAIN(copy->aModel);
  RETAIN(copy->dico);
  copy->ships = [NSMutableArray new];
  return copy;
}

- (Complex_station_manager *) stationManager
{
  return [theWorld stationManagerFor: self];
}


- (BOOL) manageCollision
{
  return YES;
}

- (void)  createModel
{
  if( modelName != nil )
    {
      ModelBlender* m = [GGModel modelWithName: modelName
                                         properties: dico];
      ASSIGN(aModel, m);
      //      NSDebugMLLog(@"Station", @"path = %@", path);
      NSDebugMLLog(@"dico", @"modele = %@", modelName);
    }

  if( aModel == nil && alternateModelName != nil )
    {
      ModelBlender* m = [GGModel modelWithName: alternateModelName
                                          properties: dico];
      ASSIGN(aModel, m);
      //      NSDebugMLLog(@"Station", @"path = %@", path);
      NSDebugMLLog(@"dico", @"modele = %@", alternateModelName);
    }
  [self invalidCache];
}


- (void) collisionOccursWith: (GGSpaceObject *) other
{
  NSDebugMLLog(@"Station", @"collision with %@", other);
  if( other == (GGSpaceObject *)[theWorld commandant] )
    {
      GGShip *ship = (GGShip *)other;
      [ship stopEngine];
      [ship centerObject];
      [ship notify: @"welcome to %@", [self name]];
      ASSIGN(visiteur, ship);
      [theGGInput setInStation: self];
      [theWorld setCheckCollision: NO];
      [[NSNotificationCenter defaultCenter]
	postNotificationName: GGNotificationPlayerEnteredStation
	object: [self signature]
	userInfo: nil];
    }
  else if ([other isKindOfClass: [GGShip class]])
    [self pnjEnteringStation: (GGShip *)other];
}

- (void) pnjEnteringStation: (GGShip *) other
{
  NSDictionary *aDic;
  aDic = [NSDictionary dictionaryWithObject: self
		       forKey: @"station"];
  [ships addObject: other];
  [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationShipEnteredStation	
    object: other
    userInfo: aDic];
  [other removeFromSuperNode];
  [other looseMainSystem];
  [other enregistre: self
	 with: @selector(removeWithNot:)];
}


- (void) removeWithNot: (NSNotification *) not
{
  id obj = [not object];
  int index;

  index = [ships indexOfObjectIdenticalTo: obj];

  if(index != NSNotFound)
    {
      [ships removeObjectAtIndex: index];
    }
}
  

- (void) openView
{
  StationWindow *sw;
  sw = [StationWindow stationView: self];
  [sw openIn: theGGInput];
}

- (void) ejectShip: (GGShip *) ship
{
  Vect3D v;
  [self positionInRepere: [ship nodePere]
	in: &v];
  addLambdaVect(&v, 2*dimension, Direction(self), Point(ship));
  *Direction(ship) = *Direction(self);
  *Haut(ship) = *Haut(self);
  [ship normalise];
  
  mulScalVect(Direction(self), 10, Speed(ship));
}

- (void) pnjLeavingStation: (GGShip *)ship
{
  int index;

  [[[self visibleAncestor] nodePere] addSubNode: ship];
  
  [self ejectShip: ship];
  [theWorld makeCurrent: ship];
  index = [ships indexOfObjectIdenticalTo: ship];
  NSParameterAssert(index != NSNotFound);
  [ships removeObjectAtIndex: index];

}

- (void) endSession
{
  [self ejectShip: visiteur];
  DESTROY(visiteur);
  [theGGInput setInStation: nil];
  [theWorld setCheckCollision: YES];
  [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationPlayerLeaveStation	
    object: self];
}

- (void) restoreState: (SystemArchiver *)archiver
{
  NSArray *a;
  NSParameterAssert(archiver);
  a = [archiver dataForStation: self];
  if( a != nil )
    {
      NSDebugMLLog(@"Station", @"find something : %@", a);
      NSParameterAssert([a isKindOfClass: [NSArray class]]);
      [ships setArray: a];
    }
}

- (void) saveState: (SystemArchiver *)archiver
{
  if( [ships count] > 0 )
    [archiver setData: ships
	      station: self];
}

- (void) cameraBeginBeingClose
{
  [self createModel];
}

- (void) cameraStopBeingClose
{
  DESTROY(aModel);
  [self invalidCache];
}

- (void) beginBeingSuperLocal
{
  GGRepere *rep;
  rep = [manager repere];
  [super beginBeingSuperLocal];
  [rep translateNodeTo: self];
  [rep reparent: self];
  rep->shouldMove = NO;
}

- (void) endBeingSuperLocal
{
  GGRepere *rep;
  rep = [manager repere];
  [repereTranslation reparent: self];
  [super endBeingSuperLocal];
  rep->shouldMove = YES;
  //  [repereTranslation reparent: [theWorld repere]];
}

- (void) rasterise: (Camera *)cam
{
    [aModel rasterise:cam];
}


- (NSArray *) shipMaterialToSel
{
  return [[self surroundingSystem] shipMaterialToSel];
}

- (NSArray *) goodiesToSel
{
  return [[self surroundingSystem] goodiesToSel];
}

@end

@implementation StationWindow
- __initStationWith: (Station *)s
{
  GGMenu *m;

  station = s;
  //  [self setOpacity: 1.0];

  [[self titleView]
    updateString: [NSString stringWithFormat: @"Welcome to %@",  s]];

  [self enableTextureBackground];

  [GGFont setDefaultFont: [GGFont fontWithFile: DefaultFontName
				  atSize: 18]];
  m = [GGMenu menu];
  [m setFrameOrigin: NSMakePoint(300, 50)];

  [m addItemWithTitle: @"Stockmarket"
     target: self
     action: @selector(openBourseWindow)];

  [m addItemWithTitle: @"Shipyard\n[You may upgrade you ship, contact the local police]"
     target: self
     action: @selector(openShipYard)];

  [m addItemWithTitle: @"Bulletin Board"
     target: [BoardView class]
     action: @selector(openNewBoard)];

  [m addItemWithTitle: @"Launch Request"
     target: self
     action: @selector(launch)];

  [m endItem];
  [self  addSubview: m];
  //  [self setState: GUI_StationView];

  //  [self addCloseButton];

  return self;
}

+ stationView: (Station *) s
{
  StationWindow *sw;
  sw = (StationWindow *)[self new];
  AUTORELEASE(sw);
  return [sw __initStationWith: s];
}


- (void) launch
{
  [station endSession];
  station = nil;
  [self close];
}

- (void) openChantier
{
  MarketPlace *place;
  NSArray *goodies;

  TopView *win = [TopView topview];
  [[win titleView]
    updateString: @"Shipyard"];

  [win enableTextureBackground];

  goodies = [station shipMaterialToSel];

  place = [[MarketPlace alloc] initWithGoodies: goodies];

  [win addSubview: place];
  RELEASE(place);
  //  [theGGInput addTopView: win];
  //  [win setState: GUI_StationView];
  [win openIn: theGGInput];
}

- (void) openShipYard
{
  GGMenu *m;
  TopView *win = [TopView topview];
  [[win titleView] updateString: [NSString stringWithFormat: @"%@ Shipyard",
					   station]];
  [win enableTextureBackground];

  //  [self setOpacity: 1.0];

  m = [GGMenu menu];
  [m setFrameOrigin: NSMakePoint(300, 50)];

  [m addItemWithTitle: @"Upgrade\n[Buy ship equipment]"
     target: self
     action: @selector(openChantier)];
  [m addItemWithTitle: @"Repair and Servicing"
     target: self
     action: @selector(repair)];
  [m addItemWithTitle: @"New and Reconditioned ships\n[Ships part exchanged]"
     target: self
     action: @selector(openNewShipWindow)];

  [m endItem];
  [win  addSubview: m];
  
  //  [win setState: GUI_StationView];
  [win openIn: theGGInput];
}

- (void) nothing
{
}

- (void) repair
{
  RepairView *v = [RepairView repairView];
  [v openIn: theGGInput];
}

- (void) openNewShipWindow
{
  TopView *win = [NewShipWindow shipViewWith: station];
  //  [theGGInput addTopView: win];
  [win openIn: theGGInput];
}

- (void) openBourseWindow
{
  MarketPlace *place;
  NSArray *goodies;

  TopView *win = [TopView topview];
  [[win titleView]
    updateString: @"Stock Market"];
  [win enableTextureBackground];

  goodies = [station goodiesToSel];

  place = [[MarketPlace alloc] initWithGoodies: goodies
			       manager: [QuantityFood class]];
  [win addSubview: place];
  RELEASE(place);

  //  [win setState: GUI_StationView];
  [win openIn: theGGInput];
  //  [theGGInput addTopView: win];
}

- (void) dealloc
{
  NSDebugMLLog(@"Station", @"deallocing %@", self);
  [super dealloc];
}



@end

@implementation RepairView
+ repairView
{
  return AUTORELEASE([[self alloc] init]);
}

- (void) repairOnce: obj
  /*very hacky, it is bad but it works.  It is functional, in a sense*/
{
  RepairView *view;
  Hull(ship) += 1000;
  Hull(ship) = Hull(ship) < MaxHull(ship) ? Hull(ship) :
    MaxHull(ship);
  [self close];
  view = [RepairView repairView];
  [view openIn: theGGInput];
}

- init
{
  GGShip *visiteur = [theWorld commandant];
  [super init];
  [[self titleView] updateString: @"Repair"];
  [self enableTextureBackground];
  ship = visiteur;

  if ( Hull(visiteur) < MaxHull(visiteur) )
    {
      double damage;
      GGOption *opt;
      GGString *str;

      damage = MaxHull(visiteur) - Hull(visiteur);
      str = [GGString new];
      [str setFrameOrigin: NSMakePoint(50, 300)];
      [str setString: [NSString stringWithFormat: @"damage : %.2f t of hull destroyed", damage / 1000.0]];
      [self addSubview: str];
      RELEASE(str);

      opt = [GGOption optionWithFormat: @"Repair 1t of Hull for 250 %@", 
		      [NSString euro]];
      [opt setTarget: self];
      [opt setAction: @selector(repairOnce:)];
      [opt setFrameOrigin: NSMakePoint(50, 250)];
      [self addSubview: opt];
  
    }
  else
    {
      GGString *str;
      NSRect rect;

      rect = [self frame];
      str = [GGString  new];
      [str setFrameOrigin: NSMakePoint(NSWidth(rect)/2, NSHeight(rect)/2)];
      [str setString: @"Nothing to Repair"
	   withFont: [GGFont fontWithFile: DefaultFontName
			     atSize: 24]];

      [self  addSubview: str];
      RELEASE(str);
    }
  //  [self setState: GUI_StationView];
  return self;
}
@end

@implementation NewShipWindow
- initNewShipViewWith: (Station *)s
{
  int i, n;
  NSRect shipFrame = {{ 20, 20 }, {200, 200}};
  NSSize aSize;
  NSArray *ships;

  [super init];
  [[self titleView]
    updateString: @"Choose a ship to buy"];
  [self enableTextureBackground];
  aSize = [self frame].size;
  station = s;
  current = -1;

  ships = [[Resource resourceManager] ships];
  n = [ships count];
  for( i = 0; i < n; ++i)
    {
      GGOption *option = [GGOption optionWithStr: [ships objectAtIndex: i]];
      [option setFrameOrigin: NSMakePoint(aSize.width-200, aSize.height - 40-
					  (i*20))];
      [option setTarget: self];
      [option setAction: @selector(chooseShip:)];
      [option setTag: i];
      [self addSubview: option];
    }


  shipView = [[ShipView alloc]
	       initWithFrame: shipFrame];
  [self addSubview: shipView];

  details = [[Console alloc] 
	      initWithFrame: NSMakeRect(20,250,550,210)];
  [details setFont: [GGFont fixedFont]];
  //  [details setTransluentBackground: YES];
  [self addSubview: details];
  //  [self setState: GUI_StationView];
  
  return self;
}

- (void) chooseShip: (GGBouton *)bo
{
  NSArray *ships = [[Resource resourceManager] ships];
  int n = [ships count];
  int i = [bo tag];
  int j;
  GGShip *ship;

  NSParameterAssert(0<= i && i < n);
  if( current == i )
    {
      [bo setState: YES];
      return;
    }
  
  for( j = 0; j < n; ++j )
    {
      if( i == j)
	continue;
      [bo setState: NO];
    }
  ship = [GGShip shipWithFileName: [ships objectAtIndex: i]];
  [shipView setShip: ship];
  [ship logData: details];
}

+ shipViewWith: (Station *) s
{
  NewShipWindow *sw;
  sw = (NewShipWindow *)[self alloc];
  return AUTORELEASE([sw initNewShipViewWith: s]);
}

- (void) dealloc
{
  RELEASE(shipView);
  RELEASE(details);
  [super dealloc];
}

- (void) close
{
  station = nil;
  [super close];
}


@end


  
