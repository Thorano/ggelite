/*
 *  Flare.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <math.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>

#import "Flare.h"
#import "utile.h"
#import "Tableau.h"
#import "Texture.h"
#import "Metaobj.h"

static GLfloat red[3] = {1.0, 0.8, 0.5};
static GLfloat green[3] = {0.0, 1.0, 0.0};
static GLfloat blue[3] = {0.0, 0.0, 1.0};

static inline FlareElem
set_flare(int type, float location, float scale, GLfloat color[3], 
	  float colorScale)
{
  FlareElem ret;

  ret.type = type;
  ret.pos = location;
  ret.size = scale;
  ret.couleur.r = color[0] * colorScale;
  ret.couleur.g = color[1] * colorScale;
  ret.couleur.b = color[2] * colorScale;
  ret.couleur.a = 1;

  return ret;
}


@implementation Flare
- init
{
  int i;
  [super init];

  for( i = 0; i < NTEXTUREFLARE; ++i)
    {
      NSString *fileName = [NSString stringWithFormat: 
				       @"Flare%i.jpg", i+1];
      textures[i] = RETAIN([Texture textureWithFileName: fileName
				    mipmap: NO]);
    }
//        flareElems[i].couleur.r=0.8*(1+0.1*vrnd());
//        flareElems[i].couleur.g=0.7*(1+0.1*vrnd());
//        flareElems[i].couleur.b=0.7*(1+0.1*vrnd());
//        flareElems[i].couleur.a=0.6*(1+0.1*vrnd());
//        flareElems[i].couleur.r=0.4+0.3*vrnd();;
//        flareElems[i].couleur.g=0;
//        flareElems[i].couleur.b=0;
//        flareElems[i].couleur.a=0.6*(1+0.1*vrnd());

//        flareElems[i].pos =2*vrnd()-1;
//        flareElems[i].size = vrnd();
//        NSDebugMLLog(@"Flare", @"%g %g %g %g", flareElems[i].couleur.r,
//  		   flareElems[i].couleur.g, flareElems[i].couleur.b,
//  		   flareElems[i].couleur.a);

  flareElems[0] = set_flare(-1, 1.0f, 0.3f, blue, 1.0);
  flareElems[1] = set_flare(-1, 1.0f, 0.2f, green, 1.0);
  flareElems[2] = set_flare(-1, 1.0f, 0.25f, red, 1.0);

  /* Flares */
  flareElems[3] = set_flare(2, 1.3f, 0.04f, red, 0.6);
  flareElems[4] = set_flare(3, 1.0f, 0.1f, red, 0.4);
  flareElems[5] = set_flare(1, 0.5f, 0.2f, red, 0.3);
  flareElems[6] = set_flare(3, 0.2f, 0.05f, red, 0.3);
  flareElems[7] = set_flare(0, 0.0f, 0.04f, red, 0.3);
  flareElems[8] = set_flare(5, -0.25f, 0.07f, red, 0.5);
  flareElems[9] = set_flare(5, -0.4f, 0.02f, red, 0.6);
  flareElems[10] = set_flare(5, -0.6f, 0.04f, red, 0.4);
  flareElems[11] = set_flare(5, -1.0f, 0.03f, red, 0.2);

  addToCheck(self);
  return self;
}

- (void) dealloc
{
  int i;
  for( i = 0; i < NTEXTUREFLARE; ++i)
    RELEASE(textures[i]);
  [super dealloc];
}

//  static void drawMinableDisque(FlareElem f, float coef)
//  {
//    //SPEEDUP:
//    int i;
//    f.couleur.a *= coef;
//    //f.couleur.a = 1;
//    glBegin(GL_TRIANGLE_FAN);
//    glColor4fv((GGReal*)&(f.couleur));
//    //  glColor4f(1, 1, 1, 0.5);
//    glVertex2f(0, 0);
//    for(i = 0; i <= 10; ++i)
//      glVertex2f(0.1*cos(i*M_PI/5)*f.size, 0.1*sin(i*M_PI/5)*f.size);
//    glEnd();
//  }

- (void) drawSquare: (int) i
	       coef: (float) coef
{
  GGReal size = flareElems[i].size;
  VectCol col = flareElems[i].couleur;
  col.r *= coef;
  col.g *= coef;
  col.b *= coef;
  glColor3fv((GGReal*)&(col));
  glBindTexture(GL_TEXTURE_2D, [textures[flareElems[i].type] idTexture]);
  glBegin(GL_QUADS);
  glTexCoord2f(0, 0);
  glVertex2f(-size, -size);
  glTexCoord2f(1, 0);
  glVertex2f(size, -size);
  glTexCoord2f(1, 1);
  glVertex2f(size, size);
  glTexCoord2f(0, 1);
  glVertex2f(-size, size);
  glEnd();
}

- (void) drawFlaresAt: (GGReal) x
		  and: (GGReal) y
{
  int i;
  GGReal dist;
  GLfloat depth;

  dist = sqrt(x*x + y*y);
  if( dist > 1)
    dist = 1/dist;
  else
    dist = 1;

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();


  glReadPixels((x+1)*WIDTH/2, (y+1)*HEIGHT/2, 1, 1, GL_DEPTH_COMPONENT,
	       GL_FLOAT, &depth);

  NSDebugMLLogWindow(@"Flare", @"depth = %g %d %d", 
		     (double)depth, (int)((x+1)*WIDTH/2), 
		     (int)((y+1)*HEIGHT/2));
  

  if( depth >= 0.96 )
    NSDebugMLLogWindow(@"Flare", @"depth pas d'occlusion");
  else
    {
      NSDebugMLLogWindow(@"Flare", @"depth occlusion");
      return;
    }



  glPushAttrib(GL_ENABLE_BIT);
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
  glBlendFunc(GL_ONE, GL_ONE);
  
  for( i = 0; i < NBRFLARES; ++i)
    {
      if( flareElems[i].type < 0)
	continue;
      glPushMatrix();
      glTranslatef(flareElems[i].pos*x, flareElems[i].pos*y, 0);
      [self drawSquare: i 
	    coef: dist];
      //      drawMinableDisque(flareElems[i], dist);
      glPopMatrix();
    }

  glDisable(GL_TEXTURE_2D);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  glPopAttrib();
}

@end
