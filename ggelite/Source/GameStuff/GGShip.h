/* 	-*-ObjC-*- */
/*
 *  GGShip.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGShip_h
#define __GGShip_h

#import "GGMobile.h"
#import "Item.h"
#import "GGModel.h"

GGBEGINDEC

#define GGLeft 0x1
#define GGRight 0x2
#define GGUp 0x4
#define GGDown 0x8
#define GGIncTime 0x10
#define GGDecTime 0x20
#define GGAccelere 0x40
#define GGDecelere 0x80
#define GGManche 0x100
#define GGWishDirection 0x200
#define GGStrafLeft 0x400
#define GGStrafRight 0x800
#define GGPousse 0x1000
#define GGLaser 0x2000


@class GGRepere;
@class NSMutableDictionary;
@class Laser;
@class Item;
@class ItemStock;
@class NSNotification;
@class GGTask;
@class ExtendedProcedure;

#define GACC(x) ((x)->gAcc)
#define GFREIN(x) ((x)->gFrein)
#define Pousse(x) (&((x)->resultante))
#define Momentum(x) (&((x)->momentum))
#define Shield(x) ((x)->bouclier)
#define HasInspector(x) ((x)->_sflags.hasInspector)
#define HyperDriveType(x) ((x)->hyperDriveType)
#define HasShield(x) ((x)->nbr_shield_generator > 0)
#define HasCloudAnalyzer(x) ((x)->_sflags.hasCloudAnalyzer)
#define Hull(x) ((x)->endurance)
#define MaxHull(x) ((x)->masseCoque)
#define Fuel(x) ((x)->carburant)
#define MaxFuel(x) ((x)->maxFuel)

#define MAXLASER 5
#define MAXMISSILE 10



/*%%%
import GGMobile;
import Item;
%%%*/

typedef enum __shipErrorCode
{
    NoError = 0,
    NoMoney,
    GunBusy,
    MissilePylonBusy,
    NoPlaceLeft,
    EquipmentNotFound,
    AlreadyThere
}ShipErrorCode;


@interface GGShip : GGMobile
{
    @public
    NSMutableDictionary 	*equipementDico;
    NSString		*modelName;
    NSDictionary		*property;
    ModelBlender        *model;
    GGShip		*target;  //NOT retained anymore, can cause loop.
    float			gAcc, gFrein;
    ExtendedProcedure	 *ia;
    Vect3D		resultante, momentum;
    Vect3D		inertie;
    int           	modeInertie;
    float			angleManche, _cosam, _sinam;
    float			speedRotation;
    float			powerRotation;
    float			power;
    float			xMouvement, yMouvement, rolMouvement;
    float			masseCoque;
    float			capacity;
    float 		capacityFret;
    float			masseFret;
    
    int			hyperDriveType;
    int			pylonRocket;
    int			pylonLaser;
    
    double		bouclier, endurance;
    int			nbr_shield_generator;
    double		carburant;	// Need some precision
    double		maxFuel;
    double		fric;
    float			consommation;
    
    Item			*missiles[MAXMISSILE];
    
    int			currentLaser;
    int			currentMissile;
    int			nbrMissile;
    Item			*laserGun[MAXLASER];
    double		timeStartFire;
    double		timeLaserRelaxation;
    Laser			*laser;
    ExtendedProcedure	*laserManager;
    
    double		lastTime;
    Vect3D		wishDir;
    Vect3D		pousse;
    
    Vect3D		posReacteur1, posReacteur2;
    
    /*index of current system:*/
    int				indexSystem;
    
    struct __ship_flag{
        unsigned 	invincible:1;
        unsigned	hasInspector:1;
        unsigned	hasHyperDrive:1;
        unsigned	hasCloudAnalyzer:1;
        unsigned	hasAutoRefuel:1;
    }_sflags;
    char			eliteLevel;
    
}
/*designated initializer:
*/
- init;
+ shipWithFileName: (NSString *)name;
- (void) setDefaultEquipment;
- (void) _computeInternalData;
- (BOOL) isCommandant;

- (NSDictionary *) property;
- (NSString *) modelName;
- (void) logData: details;
- (NSArray *) goodiesAndEquipment: (NSMutableArray *)equip;
- (ModelBlender*) model;
- (void) setModel: (ModelBlender*) _model;


- (void) earnMoney: (double) amount;
- (ShipErrorCode) buyItem: (ItemStock *)it;
- (ShipErrorCode) sellItem: (ItemStock *)it;
- (ShipErrorCode) addEquipment: (enum __Equipment) it;

- (ShipErrorCode) addItem: (Item *)item
                    count: (int) n;
- (ShipErrorCode) addItem: (Item *)it;
- (ShipErrorCode) removeItem: (Item *)item
                       count: (int) n;
- (ShipErrorCode) removeItem: (Item *)it;
- (int) nbrItem: (Item *)it;
- (int) nbrItemOfType: (ItemType)type;
- (double) maxShield;
- (void) vanish;

- (float) rangeForHyperJump;
- (int) indexSystem;
- (void) setIndexSystem: (int) index;

- (ExtendedProcedure *) ia;
- (void) setIA: (ExtendedProcedure*)aIa;

- (GGShip *) target;
- (void) setTarget: (GGSpaceObject *) newTarget;
- (void) setEliteLevel: (int) level;
- (int) eliteLevel;

- (void) notify: (NSString *) format,...;
- (void) updatePosition;
- (void) notifyAttackFrom: (GGShip *)ship;

- (void) setPower: (float) newPower;
- (float) power;

- (void) looseTarget: (GGSpaceObject *)obj;

- (void) stopEngine;
- (void) stopAutoPilot;
- (void) stop;
- (void) setXMouvement: (float) val;
- (void) setYMouvement: (float) val;
- (void) setRolMouvement: (float) val;
- (void) goLeft;
- (void) stopGoLeft;
- (void) goRight;
- (void) stopGoRight;
- (void) goUp;
- (void) stopGoUp;
- (void) goDown;
- (void) stopGoDown;
- (void) rolLeft;
- (void) stopRolLeft;
- (void) rolRight;
- (void) stopRolRight;
    //- (void) genMove: (int) dx and: (int) dy;
- (void) accelere;
- (void) stopAccelere;
- (void) decelere;
- (void) stopDecelere;
- (void) startManche;
- (void) stopManche;

- (void) setPousse: (Vect3D *)dir;
- (void) stopPousse;
- (void) setAngleManche: (float) alpha;
- (void) startWishDirection;
- (void) stopWishDirection;
- (void) setWishDirection: (Vect3D *)vect
              speedManche:(float) val;
- (void) setSpeedManche: (float) val;

- (BOOL) lockMissile;
- (GGShip *) fireMissile;
- (void) startFireLaser;
- (void) stopFireLaser;
- (void) fireLaserShort;

- (void) doExplosion;
- (float) energyForExplosion;
- (void) setMasseCoque: (float) theMasse;
- (void) setInvincibility: (BOOL) flag;

- (double) carburant;
- (void) setCarburant: (double) val;
- (BOOL) autoRefuel;


- (void) addMomentum: (Vect3D *)v
          resultante: (Vect3D *)w;
- (void) addResultante: (Vect3D *)v;
@end

/*%%%
inline GFREIN(o : GGShip) : Float {o.gFrein}
inline GACC(o : GGShip) : Float {o.gAcc}
%%%*/



GGENDDEC

//extern NSString *GGLaserItem;
//extern NSString *

extern NSString *GGSwitchToLocal;
extern NSString *GGSwitchToGlobal;
extern NSString *GGNotificationShipLeaveCurrentSystem;
extern NSString *GGNotificationShipEnterCurrentSystem;
extern NSString *GGNotificationShipCargoChange;
extern NSString *GGNotificationShipDoHyperJump;


NSString *shipMsgFromErrorCode(ShipErrorCode code);
float timeForJump(GGReal dist, GGShip *ship);

#endif
