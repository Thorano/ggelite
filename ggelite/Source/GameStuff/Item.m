/*
 *  Item.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#import <Foundation/NSObject.h>
#import <Foundation/NSException.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSCoder.h>
#import <Foundation/NSDebug.h>
#import "Metaobj.h"
#import "Item.h"
#import "GGSpaceObject.h"
#import "utile.h"
#import "System.h"

int powerLaserFromId(int val)
{
  switch(val)
    {
    case 10: return 1;
    case 11: return 5;
    case 12: return 1;
    case 13: return 4;
    case 14: return 20;
    case 15: return 100;
    default: NSCAssert(0, @"bad");
      break;
    }
  return 0;
}

int hyperDriveTypeFromId(int val)
{
  switch(val)
    {
    case 41: return 0;
    case 42: return 1;
    case 43: return 2;
    case 44: return 3;
    case 45: return 4;
    case 46: return 5;
    case 47: return 6;
    case 48: return 7;
    case 49: return 8;
      // military
    case 50: return -1;
    case 51: return -2;
    case 52: return -3;
    default: NSCAssert(0, @"bad");return -1;
    }
}


BOOL needLiveness(int val)
{
  switch(val)
    {
    case  79:
    case 80: return YES;
    default: return NO;
    }
}


typedef struct __ItemData
{
  ItemType	type;
  int 		index;
  NSString 	*name;
  int		masse;
  int		price;
  int		frequency;
}ItemData;

static ItemData itemLst[] = {
  {Mine, 1, @"XB74 Proximity Mine", 1, 27, 0},
  {BombeEnergie, 2, @"Energy Bomb", 4, 17000, Technique},
  {Missile, 3, @"KLT60 Homing Missile", 1, 45, 0},
  {Missile, 4, @"LV111 Smart Missile", 1, 63, 0},
  {Missile, 5, @"NN500 Naval Missile", 1, 90, Technique},
  {Missile, 6, @"MV1 Assault Missile", 1, 117, Technique},
  {Missile, 7, @"MV2 Assault Missile", 1, 144, HighTech},
  {OtherEquip, 8, @"Missile Viewer", 0, 45, 0},
  {OtherEquip, 9, @"Laser Cooling Booster", 1, 360, Technique},
  {LaserPulsion, 10, @"1mw Pulse Laser-red", 1, 810, 0},
  {LaserPulsion, 11, @"5mw Pulse Laser-yel", 3, 2700, 0},
  {LaserRayon, 12, @"1mw Beam Laser-red", 5, 6300, 0},
  {LaserRayon, 13, @"4mw Beam Laser-amb", 20, 11000, Technique},
  {LaserRayon, 14, @"20mw Beam Laser-wht", 75, 22000, Technique},
  {LaserRayon, 15, @"100mw Beam Lsr-m blu", 200, 54000, HighTech},
  {LaserRayon, 16, @"Sm Plasma Accel-L blu", 500, 270000, HighTech | Uncommom},
  {OtherEquip, 17, @"Auto Targetter", 0, 72, 0},
  {LaserRayon, 18, @"Lg Plasma Accel-L blu", 900, 540000, HighTech | Rare},
  {LaserRayon, 19, @"30mw Mining Lsr-m blu", 10, 10000, HighTech},
  {OtherEquip, 20, @"MB4 Mining Machine", 30, 5850, Populated},
  {OtherEquip, 21, @"Auto Refueler", 1, 90, Technique},
  {OtherEquip, 22, @"Atmospheric Shielding", 1, 180, 0},
  {OtherEquip, 23, @"Cargo Bay Life Support", 1, 540, Technique},
  {OtherEquip, 24, @"Energy Booster Unit", 5, 9900, Technique},
  {OtherEquip, 25, @"Escape Capsule", 5, 18000, Technique},
  {OtherEquip, 26, @"Extra Passenger Cabin", 5, 1350, Populated},
  {OtherEquip, 27, @"Fuel Scoop", 6, 3150,  Technique},
  {OtherEquip, 28, @"Cargo Scoop Converter", 2, 3600, Technique},
  {OtherEquip, 29, @"Tractor Beam C Scoop", 1, 8550, Technique},
  {HullRepair, 30, @"Hull Auto Repair Sys.", 40, 15000, HighTech},
  {ShieldGenerator, 31, @"Shield Generator", 4, 2250, Technique},
  {OtherEquip, 32, @"Chaff Dispenser", 1, 72, 0},
  {ECM, 33, @"E.C.M. System", 2, 1080, 0},
  {ECM, 34, @"Naval E.C.M. System", 2, 13000, Technique},
  {OtherEquip, 35, @"Automatic Pilot", 1, 1260, 0},
  {OtherEquip, 36, @"Navigation Computer", 0, 162, 0},
  {OtherEquip, 37, @"Combat Computer", 0, 270, 0},
  {Inspector, 38, @"Radar Mapper", 1, 900, 0},
  {OtherEquip, 39, @"Scanner", 1, 630, 0},
  {CloudAnalyzer, 40, @"Hyper Cloud Analizer", 1, 1575, Technique},
  {Hyperdrive, 41, @"Interplanetary Drive", 4, 4500, 0},
  {Hyperdrive, 42, @"Class 1 Hyperdrive", 10, 7200, 0},
  {Hyperdrive, 43, @"Class 2 Hyperdrive", 25, 13000, 0},
  {Hyperdrive, 44, @"Class 3 Hyperdrive", 45, 27000, Technique},
  {Hyperdrive, 45, @"Class 4 Hyperdrive", 80, 54000, Technique},
  {Hyperdrive, 46, @"Class 5 Hyperdrive", 150, 108000, Technique},
  {Hyperdrive, 47, @"Class 6 Hyperdrive", 250, 216000, HighTech},
  {Hyperdrive, 48, @"Class 7 Hyperdrive", 400, 432000, HighTech},
  {Hyperdrive, 49, @"Class 8 Hyperdrive", 600, 864000, HighTech | Uncommom},
  {Hyperdrive, 50, @"Class 1 Military Drive", 6, 22000, HighTech},
  {Hyperdrive, 51, @"Class 2 Military Drive", 12, 45000, HighTech},
  {Hyperdrive, 52, @"Class 3 Military Drive", 24, 90000, HighTech | Rare}
};



typedef struct __ItemData2
{
  ItemType	type;
  NSString 	*name;
  int		basic_price;
  int		index;

  /*contains characteristic of the system that the producer of this item
    should satisfy:
    for example, Narcotics should be produce in system with a mafia.
    robots should be produced in system with a high technology
  */
  int		producer;  

  /* contains characteristic of the system that are interested in this item.
     
  also contains some flags describing the product.  This information should 
  probably stored in another field.
   */
  int		customer;
}CommerceItem;

static CommerceItem com[] = 
  {
    {Other, @"Water", 2,53, 0, 0},
    {Other, @"Liquid Oxygen", 10,54, Technique, 0},
    {Other, @"Grain", 30,55, Agricole, Technique},
    {Other, @"Fruits and Vegetables", 30,56, Agricole, Technique},
    {Other, @"Animal Meat", 90,57, Agricole, Technique | Populated},
    {Other, @"Synthetic Meat", 30,58, Technique, Technique},
    {Other, @"Liquor", 300,59, Agricole, Drugs},
    {Other, @"Medicines", 400,60, Technique, Agricole},
    {Other, @"Fertilizer", 30,61, Technique, Agricole},
    {Other, @"Luxury Goods", 3000,62, 0,Populated | Technique | Luxury},
    {Other, @"Heavy Plastics", 100,63, Technique, 0},
    {Other, @"Metal Alloys", 100,64, Technique, 0},
    {Other, @"Precious Metals", 1000,65, 0, Luxury},
    {Other, @"Gem Stones", 2000,66, 0, Luxury | Rare},
    {Other, @"Minerals", 10,67, 0, Technique},
    {Other, @"Hydrogen Fuel", 3,68, 0, VeryCommon},
    {Other, @"Military Fuel", 10,69, Technique, 0},
    {Other, @"Industrial Parts", 50,70, Technique, Agricole},
    {Other, @"Computers", 300,71, HighTech, Technique},
    {Other, @"Air Processors", 100,72, Technique, 0},
    {Other, @"Farm Machinery", 90,73, Technique, Agricole},
    {Other, @"Roboters", 400,74, HighTech, Technique | Agricole},
    {Other, @"Radioactives", -10,75, 0, 0},
    {Other, @"Rubbish", -3,76, 0, 0},
    {Other, @"Narcotics", 500,77, Mafia, Luxury | Illegal | Populated},
    {Other, @"Animal Skins", 300,78, Agricole, Luxury},
    {Living, @"Live Animals", 100,79, Agricole, Luxury},
    {Living, @"Slaves", 200,80, 0, Populated | Luxury | Illegal},
    {Other, @"Hand Weapons", 500,81, Technique, Illegal},
    {Other, @"Battle Weapons", 1000,82, HighTech, Illegal},
    {Other, @"Nerve Gas", 800,83, HighTech | Mafia, Illegal}
  };


@implementation Item
- initWithName: (NSString *) aName
	  type: (int) aType
	   arg: (int) aArg
	 masse: (int) m
{
  [self init];
  ASSIGN(name, aName);
  masse = m;
  type = aType;
  arg1 = aArg;
  addToCheck(self);
  return self;
}

- copyWithZone: (NSZone *) z
{
  return RETAIN(self);
}

- (BOOL) oncePerShip
{
  if (type >= GoodiesBegining )
    return NO;
  switch(type)
    {
    case  LaserPulsion:
    case  LaserRayon:
    case  Missile:
    case  Mine:
    case  ShieldGenerator:
      return NO;
    case  Hyperdrive:
    case  BombeEnergie:
    case  ECM:
    case  Inspector:
    case  HullRepair:
    case  CloudAnalyzer:
    case  OtherEquip:
    default:
      return YES;
    }
}

+ equipmentWithArg: (int) val
{
  NSParameterAssert(1<= val && val <= sizeof(itemLst)/sizeof(ItemData));
  val--;
  
  return [self itemWithName: itemLst[val].name
	       type: itemLst[val].type
	       arg: itemLst[val].index
	       masse: itemLst[val].masse*1000];
	       
}

+ goodiesWithArg: (int) val
{
  int i;
  NSParameterAssert(sizeof(itemLst) / sizeof(ItemData) + 1 <= val && 
		    val <= sizeof(itemLst) / sizeof(ItemData) + 1 +
		    sizeof(com)/sizeof(CommerceItem));

  i = val - (sizeof(itemLst) / sizeof(ItemData) + 1);
  return [self itemWithName: com[i].name
	       type: com[i].type
	       arg: com[i].index
	       masse: 1000];
}

+ hydrogen
{
  return [self itemWithName:@"Hydrogen"
	       type: Hydrogen
	       arg: 68];
}

+ hyperDriveCivil: (int) class
{
  int arg = 41 + class;
  NSParameterAssert(1 <= class && class <= 8);
  return [self equipmentWithArg: arg];
}

- (void) dealloc
{
  RELEASE(name);
  [super dealloc];
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
  GGEncode(type);
  GGEncode(arg1);
  GGEncode(arg2);
  GGEncode(arg3);
  GGEncode(masse);
  GGEncode(name);
}

- (id)initWithCoder:(NSCoder *)decoder
{
  GGDecode(type);
  GGDecode(arg1);
  GGDecode(arg2);
  GGDecode(arg3);
  GGDecode(masse);
  GGDecode(name);
  return self;
}


- (void) setItemName: (NSString *)_name
{
  ASSIGN(name, _name);
}

- (BOOL) isEqual: (id) object
{
  if([object isKindOfClass: [Item class]] )
    {
      Item *obj;
      obj = (Item *)object;
      return (obj->arg1 == arg1);
    }
  else
    {
      NSWarnMLog(@"Comparing %@ with item %@", object, self);
      return NO;
    }
}

- (unsigned) hash
{
  return arg1;
}

+ itemWithName: (NSString *) aName
	  type: (int) aType
	   arg: (int) aArg
	 masse: (int) m
{
  Item *i = [self alloc];
  [i initWithName: aName
     type: aType
     arg: aArg
     masse: m];
  return AUTORELEASE(i);
}

+ itemWithName: (NSString *) aName
	  type: (int) aType
	   arg: (int) aArg
{
  return [self itemWithName:aName
	       type: aType
	       arg: aArg
	       masse: 1000];
}

- (NSString *) description
{
  if( name != nil )
    return name;
  else
    return [super description];
}
@end

@implementation ItemStock
/*designated initializer
 */
- initWithItem: (Item *)i
     prixAchat: (int) aPrixAchat
     prixVente: (int) aPrixVente
      stock: (int) q
{
  [super init];
  ASSIGN(item, i);
  prixAchat = aPrixAchat;
  prixVente = aPrixVente;
  stock = q;
  addToCheck(self);
  return self;
}

- (void) dealloc
{
  RELEASE(item);
  [super dealloc];
}
  
- copyWithZone: (NSZone *)zone
{
  ItemStock *i;

  i = (ItemStock *)NSCopyObject(self, 0, zone);

  i->item = [item copyWithZone: zone];
  return i;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
  GGEncode(item);
  GGEncode(prixAchat);
  GGEncode(prixVente);
  GGEncode(stock);
  GGEncode(cargo);
}

- (id)initWithCoder:(NSCoder *)decoder
{
  GGDecode(item);
  GGDecode(prixAchat);
  GGDecode(prixVente);
  GGDecode(stock);
  GGDecode(cargo);
  return self;
}


- initWithName: (NSString *) aName
	  type: (int) aType
	   arg: (int) aArg
     prixAchat: (int) aPrixAchat
     prixVente: (int) aPrixVente
	 masse: (int) m
      stock: (int) q
{
  Item *i;
  i = [Item itemWithName: aName
	    type: aType
	    arg: aArg
	    masse: m];
  return [self initWithItem: i
	       prixAchat: aPrixAchat
	       prixVente: aPrixVente
	       stock: q];
}

+ itemStockWithItem: (Item *)i
{
  return AUTORELEASE([[self alloc] initWithItem: i
				   prixAchat: 0
				   prixVente: 0
				   stock: 0]);
}

+ itemStockWithName: (NSString *) aName
	       type: (int) aType
		arg: (int) aArg
	  prixAchat: (int) aPrixAchat
	  prixVente: (int) aPrixVente
{
  return [self itemStockWithName: aName
	       type: aType
	       arg: aArg
	       prixAchat: aPrixAchat
	       prixVente: aPrixVente
	       masse: 1000
	       stock: 0];
}


+ itemStockWithName: (NSString *) aName
	       type: (int) aType
		arg: (int) aArg
	      price: (int) p
	      stock: (int) q
{
  return AUTORELEASE([[self alloc] initWithName: aName
		       type: aType
		       arg: aArg
		       prixAchat: p
		       prixVente: p
		       masse: 1000
		       stock: q]);
}


+ itemStockWithName: (NSString *) aName
	       type: (int) aType
		arg: (int) aArg
	  prixAchat: (int) aPrixAchat
	  prixVente: (int) aPrixVente
	      masse: (int) m
	   stock: (int) q
{
  return AUTORELEASE([[self alloc] initWithName: aName
		       type: aType
		       arg: aArg
		       prixAchat: aPrixAchat
		       prixVente: aPrixVente
		       masse: m
		       stock: q]);
}

+ (void) setSystemGoods: (System *) sys
{
  int i;
  SystemSpeciality spe;
  NSMutableArray *equips;
  NSMutableArray *goodies;
  NSMutableArray *illegal;

  NSParameterAssert(sys);
  spe = [sys systemSpeciality];
  
  equips = [NSMutableArray new];
  goodies = [NSMutableArray new];
  illegal = [NSMutableArray new];

  for( i = 0; i < sizeof(com)/sizeof(CommerceItem); ++i)
    {
      int coef = 1;
      CommerceItem data = com[i];
      ItemStock *itemStock;

      if( (((~data.producer) | spe) & SystemAttributeMask) ==SystemAttributeMask
	  && proba(0.6) )
	{
	  float intensity = vrnd();
	  if ((data.customer & VeryCommon) != 0 )
	    coef = 100;

	  if( ((data.customer & Uncommom) && proba(0.2)) ||
	      ((data.customer & Rare) && proba(0.05)) ||
	      (data.customer & (Uncommom | Rare)) == 0
	      )
	    {
	      itemStock = [self itemStockWithName: data.name
			    type: data.type
			    arg: data.index
			    price: (1-0.5*intensity)*data.basic_price
			    stock: 100*intensity*coef];
	    }
	  else
	    {
	      itemStock = [self itemStockWithName: data.name
			    type: data.type
			    arg: data.index
			    price: data.basic_price
			    stock: 3*intensity*coef];
	    }
	  [goodies addObject: itemStock];
	  
	  continue;
	}

      else if( (((~data.customer) | spe) & SystemAttributeMask) == SystemAttributeMask)
	{
	  float price;

	  price = data.basic_price;
	  if( data.customer & Uncommom )
	    price *= 2;
	  else if( data.customer & Rare )
	    price *= 4;

	  itemStock = [self itemStockWithName: data.name
			type: data.type
			arg: data.index
			price: price*1.1
			stock: irnd(5)];

	  goto add;
	}
      else 
	{
	  itemStock = [self itemStockWithName: data.name
			type: data.type
			arg: data.index
			price: data.basic_price*0.9
			stock: irnd(3)];
	}
    add:
      
      if( ((data.customer & Illegal) && ((spe & Mafia) == 0)) || 
	  ((data.customer & Luxury) &&  (spe & Integrist)) )
	{
	  itemStock->prixAchat *= 2;
	  [illegal addObject: itemStock];
	}
      else
	[goodies addObject: itemStock];
    }

  [sys setGoodies: goodies
       equipment: [self itemList]
       illegal: illegal];

  RELEASE(equips);
  RELEASE(goodies);
  RELEASE(illegal);
}

+ (NSArray *) itemList
{
  int i;
  NSMutableArray *tmp = [NSMutableArray array];
  for( i = 0; i < sizeof(itemLst)/sizeof(ItemData); ++i)
    [tmp addObject: [self itemStockWithName: itemLst[i].name
			  type: itemLst[i].type
			  arg: itemLst[i].index
			  prixAchat: itemLst[i].price
			  prixVente: itemLst[i].price
			  masse: 1000*itemLst[i].masse
			  stock: 100000]];
  return tmp;
}

+ (NSArray *) comItemList
{
  int i;
  NSMutableArray *tmp = [NSMutableArray array];
  for( i = 0; i < sizeof(com)/sizeof(CommerceItem); ++i)
    [tmp addObject: [self itemStockWithName: com[i].name
			  type: com[i].type
			  arg: com[i].index
			  prixAchat: com[i].basic_price
			  prixVente: com[i].basic_price
			  masse: 1000
			  stock: irnd(20)]];
  return tmp;
}
@end
