/* 	-*-ObjC-*- */
/*
 *  Station.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Station_h
#define __Station_h

#import "GGOrbiteur.h"
#import "GGModel.h"

GGBEGINDEC
/*%%%
import GGOrbiteur;
%%%*/

@class Texture;
@class GGShip;
@class NSDictionary;
@class Complex_station_manager;

@interface Station : GGOrbiteur
{
  GGShip 		*           visiteur;
  NSMutableArray            *ships;
  ModelBlender*             aModel;
  NSString 		*modelName;
  NSString		*alternateModelName;
  NSDictionary		*dico;
}
- (NSArray *) shipMaterialToSel;
- (NSArray *) goodiesToSel;
- (void) openView;
- (void) setRandomStationAtRadius: (GGReal) r;
- (void) pnjEnteringStation: (GGShip *)ship;
- (void) pnjLeavingStation: (GGShip *)ship;
- (Complex_station_manager *) stationManager;
@end
GGENDDEC

#endif
