/* 	-*-ObjC-*- */
/*
 *  Commandant.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Commandant_h
#define __Commandant_h

#import "GGShip.h"

GGBEGINDEC
typedef enum __modePilotage 
{
    Manuel,
    AutoPilote,
    Hinter
}ModePilotage;

typedef struct __wing
{
    Vect3D pos;
    Vect3D n;
    float alpha, beta;
}Wing;

typedef struct __wheel
{
    Vect3D pos;
}Wheel;

@class Complex_Autopilot, Complex_run_contract;


/*%%%
import GGShip;
%%%*/


@interface Commandant : GGShip
{
    int				numShip;
    ModePilotage			modePilote;
    GGSpaceObject			*destination;  //not retained
    unsigned			hinterFlag;
    GGReal				speedWished;
    double			sleepTimeHinter;
    double			timerAction;
    double			altitude;
    double			hauteurTerrain;
    BOOL				nearPlanete;
    int				wishSystemIndex;
    NSMutableArray		*contracts;
    
#ifdef GGSIM
    //experimental
    //  float 			xMove, yMove, rolMove;
    Wing				wings[7];
    Wheel				wheels[3];
#endif
}

- (GGSpaceObject *) destination;
- (void) setDestination: (GGSpaceObject *) newDest;

- (int) wishSystemIndex;
- (void) setWishSystemIndex: (int) index;

- (void) changeModePilot;

- (void) nextTarget;
- (void) autoPilotTo: (GGSpaceObject *)dest;
- (void) autoPilotToCurrentTarget;

- (void) hyperJump;

- (ModePilotage) modePilote;
- (void) setModePilotage: (ModePilotage) newModePilote;
- (double) speedWished;
- (void) setSpeedWished: (GGReal) val;
- (void) setSpeedWishedWithNumber: (NSNumber*) val;


- (void) setNearPlanete: (BOOL) flag;
- (void) setAltitude: (double) val;
- (void) setHauteurTerrain: (double) val;
- (BOOL) nearPlanete;
- (double) altitude;
- (double) hauteurTerrain;
- (void) addContract: (Complex_run_contract *)c;
- (NSArray *) contracts;
@end

extern Commandant *theCommandant;
GGENDDEC

#endif
