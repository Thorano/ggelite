/* 	-*-ObjC-*- */
/*
 *  GGMobile.m
 *
 *  Copyright (c) 2002 by Frédéric De Jaeger
 *  
 *  Date: September 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
 Various continuation
 
 - (void) updatePosition : called every frame to update graphical states
 - tryGlobalCoordinates \   called sometimes to manage the reference frame
 - tryLocalCoordinates  /   of the ship
 */


#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSCoder.h>


#import "GGLoop.h"
#import "GGMobile.h"
#import "Metaobj.h"
#import "utile.h"
#import "Sched.h"
#import "World.h"
#import "GGRepere.h"
#import "Tableau.h"
#import "GGRepere.h"

NSString *GGNotificationShipBecomeActivated = 
@"GGNotificationShipBecomeActivated";
NSString *GGNotificationShipBecomeInactivated =
@"GGNotificationShipBecomeInactivated";
NSString *GGNotificationSwitchToNonUpdatePosition =
@"GGNotificationSwitchToNonUpdatePosition";


@interface GGMobile (Private)
- (void) scheduleAsLocal;
- (void) scheduleAsGlobal;
- (void) _activate;
@end

static const float distanceLocal2 =  1000*distanceCameraRepere2;
static const float distanceGlobal2 = 4000*distanceCameraRepere2;


@implementation GGMobile
- init
{
    [super init];
    [self setMode: GGInertialRotationalMode | GGInertialMode];
    _flags.graphical = YES;
    return self;
}

- (BOOL) shouldSave
{
    return YES;
}

- (BOOL) saveChannel
{
    return YES;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [super encodeWithCoder: encoder];
    GGEncode(tracked);
}

- (id)initWithCoder:(NSCoder *)decoder
{
    [super initWithCoder: decoder];
    GGDecode(tracked);
    
    return self;
}

- (void) _activate
{
    if( _flags.locality == 2 || tracked )
    {
        [theLoop fastSchedule: self
                 withSelector: @selector(updatePosition)
                          arg: nil];
        if( ! _flags.activated )
        {
            NSDebugMLLog(@"GGShip", @"%@ activated", self);
            _flags.activated = 1;
            [[NSNotificationCenter defaultCenter]
	    postNotificationName: GGNotificationShipBecomeActivated
                      object: self];
        }
    }
    else
    {
        [[theLoop fastScheduler]
	deleteTarget: self
	withSelector: @selector(updatePosition)];
        
        if( _flags.activated )
        {
            NSDebugMLLog(@"GGShip", @"%@ inactivated", self);
            _flags.activated = 0;
            [[NSNotificationCenter defaultCenter]
	    postNotificationName: GGNotificationShipBecomeInactivated
                      object: self];
        }
    }
}


- (void) beginSchedule
{
    [super beginSchedule];
    [self _activate];
    if( _flags.locality == 1 )
        [self scheduleAsGlobal];
    //     [theLoop slowSchedule: self
    // 	     withSelector: @selector(tryLocalCoordinates)
    // 	     arg: nil];
    else if( _flags.locality == 2 )
        [self scheduleAsLocal];
    //     [theLoop slowSchedule: self
    // 	     withSelector: @selector(tryGlobalCoordinates)
    // 	     arg: nil];
    else
        NSAssert(0, @"Should not be here");
}

- (void) endSchedule
{
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationSwitchToNonUpdatePosition
                  object: self];
    [[theLoop slowScheduler]
    deleteTarget: self
    withSelector: @selector(tryLocalCoordinates)];
    [[theLoop slowScheduler]
    deleteTarget: self
    withSelector: @selector(tryGlobalCoordinates)];
    [super endSchedule];
}


- (void) scheduleAsLocal
{
    NSParameterAssert(_flags.locality == 2);
    [self setVisible: YES];
    [[theLoop slowScheduler] deleteTarget: self
                             withSelector: @selector(tryLocalCoordinates)];
    [theLoop slowSchedule: self
             withSelector: @selector(tryGlobalCoordinates)
                      arg: nil];
}

- (void) scheduleAsGlobal
{
    NSParameterAssert(_flags.locality == 1);
    [self setVisible: NO];
    [[theLoop slowScheduler] deleteTarget: self
                             withSelector: @selector(tryGlobalCoordinates)];
    [theLoop slowSchedule: self
             withSelector: @selector(tryLocalCoordinates)
                      arg: nil];
}




- (void) switchToLocal:(GGRepere *)_repere
{
    NSDebugMLLog(@"SpaceObj", @"switch to local");
    
    NSAssert((GGNode *)_repere != [self nodePere], @"already in local...");
    [_repere reparent: self];
    [self scheduleAsLocal];
}

- (void) adjustGlobalCoordinates
{
    GGSpaceObject *node = [manager repere];
    node = node->nodePere;
    while(node != nil && ! [node isInNeighboorhood: self]) node = node->nodePere;
    if( node != nil ) //so self isInNeighboorhood of node
    {
        if( node != nodePere )
        {
            NSDebugMLLog(@"SpaceObj", @"change father orbiteur from %@ to %@",
                         nodePere, node);
            [node reparent: self];
        }
    }
    NSDebugMLLogWindow(@"SpaceObj", @"node = %@ nodePere = %@", node, nodePere);
}

- (void) switchToGlobal: (GGRepere *) _repere
{
    NSDebugMLLog(@"SpaceObj", @"switch to global");
    
    NSAssert((GGNode *)_repere == [self nodePere], @"Object is not local");
    [self adjustGlobalCoordinates];
    //  [[_repere ancestor] reparent: self];
    [self scheduleAsGlobal];
}

- (BOOL) tryLocalCoordinatesIfPossible
{
    if( _flags.locality == 1 )
    {
        [self tryLocalCoordinates];
        if( _flags.locality == 2 )
            return YES;
    }
    return NO;
}


- (void) tryLocalCoordinates
{
    GGReal val;
    GGRepere *_repere = [manager repere];
    val = distanceAbsolu2(self, _repere);
    if( val < distanceLocal2 || val < prodScal(Point(self), Point(self)) * 1e-4 )
    {
        [self switchToLocal: _repere];
    }
    else
        [self adjustGlobalCoordinates];
}

- (void) tryGlobalCoordinates
{
    GGRepere * _repere = [manager repere];
    double d;
    NSParameterAssert((GGNode *)_repere == [self nodePere]);
    d = (prodScal(Point(self), Point(self)));
    if( d > distanceGlobal2 && d > prodScal(Point(nodePere), Point(nodePere)) * 1e-3 )
    {
        [self switchToGlobal: _repere];
        //      NSLog(@"%@ go global", self);
    }
}


- (void) setLocality: (unsigned)val
{
    [super setLocality: val];
    [self _activate];
}

- (BOOL) tracked
{
    return tracked;
}


- (void) setTrack: (BOOL) val
{
    tracked = val;
    [self _activate];
}
@end
