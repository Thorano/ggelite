/*
 *  Explosion.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSDebug.h>
#import "GGLoop.h"
#import "Explosion.h"
#import "GGRepere.h"
#import "utile.h"
#import "Sched.h"
#import "Tableau.h"
#import "SoundSDL.h"
#import "GGShip.h"
#import "GGModel.h"
#import "Preference.h"
#import "ModelBlend.h"

#import <math.h>
#import <time.h>

static Vect3D randomVect(float max)
{
    Vect3D res;
    max /= 10;
    res.x = (vrnd()-0.5)*max;
    res.y = (vrnd()-0.5)*max;
    res.z = (vrnd()-0.5)*max;
    return res;
}


@implementation Explosion
- (void) __initTriangles: (double) temp
                   model: (ModelBlender*) model
{
    int i;
    
    //  _flags.graphical = YES;
    
    if( model != nil && [model respondsToSelector: @selector(getTriangles:)] )
    {
        nTriangles = [model getTriangles: &triangles];
        for( i = 0; i < nTriangles; ++ i)
        {
            triangles[i].rotation = randomVect(temp);
            normaliseVect(&triangles[i].rotation);
            //	  triangles[i].vit = randomVect(temp);
            divScalVect(&triangles[i].pos, 1, &triangles[i].vit);
            triangles[i].totalLife = triangles[i].lifeTime = 8*vrnd();
        }
    } 
    
    if( nTriangles == 0 )
    {
        nTriangles = 15;
        triangles = malloc(nTriangles*sizeof(Triangle));
        for( i = 0; i < nTriangles; ++ i)
        {
            Vect3D AB, AC, A, B, C, N, G;
            A = randomVect(temp);
            B = randomVect(temp);
            C = randomVect(temp);
            G = A;
            addVect(&G, &B, &G);
            addVect(&G, &C, &G);
            divScalVect(&G, 3, &G);
            diffVect(&A, &G, &triangles[i].A);
            diffVect(&B, &G, &triangles[i].B);
            diffVect(&C, &G, &triangles[i].C);
            
            mkVectFromPoint(&A, &B, &AB);
            mkVectFromPoint(&A, &B, &AB);
            mkVectFromPoint(&A, &C, &AC);
            prodVect(&AB, &AC, &N);
            if( !normaliseVect(&N) )
                initVect(&N, 0, 0, 1);
            triangles[i].normal = N;
            triangles[i].pos = G;
            
            triangles[i].rotation = randomVect(temp);
            normaliseVect(&triangles[i].rotation);
            triangles[i].vit = randomVect(temp);
            triangles[i].couleur.r = vrnd();
            triangles[i].couleur.g = vrnd();
            triangles[i].couleur.b = vrnd();
            triangles[i].totalLife = triangles[i].lifeTime = 8*vrnd();
        }
    }
    
    collisionCounter = -1;
    masse = 1e6;
}

- 	initAt: (GGSpaceObject *)obj
withEnergy: (float) val
{
    double temp = sqrt(val);
    
    [self initAt: obj];
    if( [obj respondsToSelector: @selector(model)] )
    {
        [self __initTriangles: temp
                        model: [(id) obj model]];
    }
    else
        [self __initTriangles: temp
                        model: nil];
    
    return self;
}

/*The inherited implementation of encodeWithCoder is the good one,
for the moment
*/

- initWithCoder: (NSCoder *) decoder
{
    [super initWithCoder: decoder];
    [self __initTriangles: 1000
                    model: nil ];
    return self;
}

+ explosionAt: (GGSpaceObject *) obj
   withEnergy: (float) val
{
    Explosion *blast;
    NSDebugMLLog(@"Explosion", @"blast = %g", val);
    blast = [self alloc];
    [soundManager playFile: @"Explosion3.wav"];
    return AUTORELEASE([blast initAt: obj
                          withEnergy: val]);
}

- (void) dealloc
{
    NSDebugMLLog(@"Explosion", @"Explosion libere");
    free(triangles);
    [super dealloc];
}

- (void) updatePosition
{
    int i;
    BOOL ok=NO;
    for( i = 0; i < nTriangles; ++i )
    {
        Triangle *T = &triangles[i];
        if( T->lifeTime > 0 )
        {
            ok = YES;
            T->lifeTime -= deltaTime;
            addLambdaVect(&T->pos, deltaTime, &T->vit, &T->pos);
        }
    }
    
    masse /= exp(deltaTime);
    
    if( ok )
        [super updatePosition];
    else
        [self destroy];
    
}

- (BOOL) shouldCache
{
    return NO;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

- (void) rasterise: (Camera *)cam
{
    int i;
    
    NSDebugMLLogWindow(@"Explosion", @"on rasterise");
    glDisable(GL_CULL_FACE);
    if( thePref.GLBlend )
    {
        glEnable(GL_BLEND);
    }
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    for( i = 0; i < nTriangles; ++i )
    {
        Triangle *T = &triangles[i];
        if( T->lifeTime > 0 )
        {
            glPushMatrix();
            glTranslated(T->pos.x, T->pos.y, T->pos.z); 
            glRotatef(100*T->lifeTime, 
                      T->rotation.x, T->rotation.y, T->rotation.z);
            glNormal3v((GGReal *)&T->normal);
            glBegin(GL_TRIANGLES);
            //	  glColor4d(0.4, 0.4, 0.4,   T->lifeTime / T->totalLife);
            glColor4d(T->couleur.r,T->couleur.g,T->couleur.b, 
                      T->lifeTime / T->totalLife);
            glVertex3d(T->A.x, T->A.y, T->A.z);
            glVertex3d(T->B.x, T->B.y, T->B.z);
            glVertex3d(T->C.x, T->C.y, T->C.z);
            glEnd();
            glPopMatrix();
        }
        
    }
    glEnable(GL_CULL_FACE);
    glDisable(GL_BLEND);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
}

@end

