/*
 *  Commandant.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSException.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSArray.h>
#import "Commandant.h"
#import "GGConcreteShip.h"
#import "World.h"
#import "Tableau.h"
#import "utile.h"
#import "GGOrbiteur.h"
#import "Sched.h"
#import "Laser.h"
#import "Console.h"
#import "GGSky.h"
#import "GGRepere.h"
#import "Preference.h"
#import "Station.h"
#import "autopilote.h"
#import "contract.h"


#define VAISSEAU 1
#define ORBITEUR 2

Commandant *theCommandant = nil;

static inline Wing createWings(GGReal a, GGReal b,GGReal c, GGReal x, GGReal y, GGReal z,
                               GGReal alpha, GGReal beta)
{
    Wing w;
    initVect(&w.pos, a, b, c);
    initVect(&w.n, x, y, z);
    w.alpha = -alpha;
    w.beta = -beta;
    return w;
}


@implementation Commandant
- init
{
    [super init];
    _flags.locality = 1;
    numShip = 0;
    destination = nil;
    modePilote = Manuel;
    theCommandant = self;
    carburant = 10000;
    fric = 100000;
    indexSystem = 0;
    speedWished = 0.0;
    contracts = [NSMutableArray new];
    [self _computeInternalData];
    
    tracked = YES;
    
#ifdef GGSIM
    /*grandes ailes*/
    wings[0] = createWings(5, 0, 0,
                           0, 1, 0,
                           2, 5);
    wings[1] = createWings(-5, 0, 0,
                           0, 1, 0,
                           2, 5);
    
    /*ailerons de roulis */
    wings[2] = createWings(5, 0, 0,
                           0, 1, 0,
                           0, 1);
    wings[3] = createWings(-5, 0, 0,
                           0, 1, 0,
                           0, 1);
    
    /*ailerons de profondeur */
    wings[4] = createWings(0, 0, -5,
                           0, 1, 0,
                           0, 1);
    
    /*dérive */
    wings[5] = createWings(0, 0, -5,
                           1, 0, 0,
                           0, 3);
    
    /*gouverne*/
    wings[6] = createWings(0, 1, -5,
                           1, 0, 0,
                           0, 1);
    
    initVect(&inertie, 10, 10, 10);
#endif
    return self;
}

// - (BOOL) shouldBeClipped
// {
//   return NO;
// }


// - (BOOL) shouldCache
// {
//   return NO;
// }


- (void)encodeWithCoder:(NSCoder *)encoder
{    
    [super encodeWithCoder: encoder];	
    GGEncode(numShip);
    GGEncode(modePilote);
    GGEncode(destination);
    GGEncode(hinterFlag);
    GGEncode(speedWished);
    GGEncode(sleepTimeHinter);
    GGEncode(timerAction);
    GGEncode(altitude);
    GGEncode(hauteurTerrain);
    GGEncode(nearPlanete);
    GGEncode(wishSystemIndex);
    GGEncode(contracts);
}

- (id)initWithCoder:(NSCoder *)decoder
{
    [super initWithCoder: decoder];
    GGDecode(numShip);
    GGDecode(modePilote);
    GGDecodeObject(destination);
    GGDecode(hinterFlag);
    GGDecode(speedWished);
    GGDecode(sleepTimeHinter);
    GGDecode(timerAction);
    GGDecode(altitude);
    GGDecode(hauteurTerrain);
    GGDecode(nearPlanete);
    GGDecode(wishSystemIndex);
    GGDecode(contracts);
    
    theCommandant = self;
    
    return self;
}

- (void) invalidate
{
    [contracts makeObjectsPerformSelector: @selector(invalidate)];
    [contracts removeAllObjects];
    [super invalidate];
}

- (void) dealloc
{
    RELEASE(contracts);
    if(theCommandant == self){
        theCommandant = nil;
    }
    [super dealloc];
}

- (void) destroy
{
    GGSpaceObject *obj;
    consoleLog(@"You are a loser");
    obj = [GGMobile spaceObjectAt: self];
    
    
    addLambdaVect(Point(obj), -10* Dimension(self), Direction(obj), Point(obj));
    [[self nodePere] addSubNode: obj];
    
    [theWorld setMainCamera: obj];
    [super destroy];
}

- (BOOL) isCommandant
{      
    return YES;
}

- (ModePilotage) modePilote
{
    return modePilote;
}

- (int) wishSystemIndex
{
    return wishSystemIndex;
}

- (void) setWishSystemIndex: (int) index
{
    wishSystemIndex = index;
}

- (void) addContract: (Complex_run_contract *)c
{
    NSParameterAssert(c);
    if( [contracts indexOfObjectIdenticalTo: c] == NSNotFound )
    {
        NSDebugMLLog(@"Contract", @"adding %@", c);
        [contracts addObject: c];
        [c registerContinuation: self
                             at: @selector(endContract:)];
    }
    else
        consoleLog(@"The contract %@ is already subsribed", c);
}

- (void) endContract: c
{
    NSDebugMLLog(@"Contract", @"fin de contrat %@");
    
    [contracts removeObjectIdenticalTo: c];
    NSDebugMLLog(@"Contract", @"contrats: $@");
}

- (NSArray *) contracts
{
    return contracts;
}

- (void) hasJumpedDistance: (GGReal) dist
{
    int nh;
    
    nh = (int) ceil(dist / [self rangeForHyperJump] * hyperDriveType);
    if( nh > 0 )
    {
        if( [self removeItem: [Item hydrogen]
                       count: nh] != NoError )
            NSWarnMLog(@"An error here, strange");
    }
}

- (void) reallyJump: obj
{
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationShipDoHyperJump
                  object: self];
    //FIXME: add the cloud here
    [theWorld jumpToSystemIndex: wishSystemIndex];
    indexSystem = wishSystemIndex;
    [[theWorld sky] setDestination: -1];
}

- (void) hyperJump
{
    if( indexSystem != wishSystemIndex )
    {
        GGReal dist = 0.0;
        if( wizard || (dist = [[theWorld sky] distanceWith: indexSystem
                                                       and: wishSystemIndex]) < [self rangeForHyperJump] )
        {
            Complex_TurnToPoint *es;
            Vect3D v;
            GGSky *sky;
            Star *s;
            sky = [theWorld sky];
            [sky setDestination: wishSystemIndex];
            s = [sky stars: NULL];
            v = s[wishSystemIndex].pos;
            es = [Complex_TurnToPoint allocInstance];
            [es registerContinuation: self
                                  at: @selector(reallyJump:)];
            [es runWitharg1: self	
                       arg2: v];
            [self stopEngine];
            [self hasJumpedDistance: dist];
            RELEASE(es);
        }
        else
        {
            [self notify: @"%@ is too far away", 
                [theWorld starNameForIndex: wishSystemIndex]];
        }
    }
    else
        [self notify: @"can't jump to the same system"];
}

- (GGSpaceObject *) destination
{
    return destination;
}

- (void) notify: (NSString *) format,...
{
    va_list ap;
    
    va_start (ap, format);
    consoleLogv (format, ap);
    va_end (ap);
    [theLoop setTimeSpeed: 1];
}

- (void) atDestination: obj 
                status: (Status) status
{
    DESTROY(ia); //don't need to invalidate it
    [self setModePilotage: Manuel];
    if ( status == AtDestination)
        [self notify: @"Ship at Destination"];
}


- (void) setDestination: (GGSpaceObject *) newDest
{
    if( newDest == destination ) return;
    destination =  newDest;
    [destination enregistreForRemoving: self
                                  with: @selector(removeDestinationWithNotification:)];
    if( modePilote == AutoPilote )
        [self autoPilotTo: newDest];
}

- (void) removeDestinationWithNotification:  (NSNotification *) aNotification
{
    if( [aNotification object] == destination )
        [self setDestination: nil];
}

- (void) changeModePilot
{
    switch(modePilote)
    {
        case Manuel:
            [self setModePilotage: Hinter];
            break;
        case AutoPilote:
            [self setModePilotage: Manuel];
            break;
        case Hinter:
            [self setModePilotage: AutoPilote];
            break;
        default:
            consoleLog(@"mode pilotage inconnu");
            break;
    }
}

- (void) autoPilotTo: (GGSpaceObject *)dest
{
    [self setDestination: dest];
    [self setModePilotage: Manuel];
    [self setModePilotage: AutoPilote];
}

- (void) autoPilotToCurrentTarget
{
    [self autoPilotTo:[self target]];
}

- (void) setTrack: (BOOL) val
{
}

- (NSArray *) neighBoorhood
{
    NSMutableArray *res = [NSMutableArray array];
    NSArray *objs = [[self nodePere] subNodes];
    NSEnumerator *en = [objs objectEnumerator];
    GGSpaceObject *obj;
    Class ship = [GGMobile class];
    
    while((obj = [en nextObject]) != nil )
    {
        if( obj != self && [obj isKindOfClass: ship] )
            [res addObject: obj];
    }
    return res;
}

- (void) nextTarget
{
    NSArray *ships = [self neighBoorhood];
    unsigned size;
    if( numShip >= (size = [ships count]) )
        numShip = 0;
    if( size == 0 )
    {
        consoleLog(@"No ship to target!");
    }
    else
    {
        [self setTarget: [ships objectAtIndex: numShip]];
        numShip++;
    }
}

- (GGShip *) fireMissile
{
    GGShip *mis = [super fireMissile];
    if( mis != nil )
        [theWorld setAuxiliaryCamera: mis];
    return mis;
}

- (double) speedWished
{
    return speedWished;
}

- (void) startHinterMode
{
    GGOrbiteur *closestOrbiteur = [theTableau closestOrbiteur];
    PetitGalileen pg;
    
    if( closestOrbiteur == nil )
        return;
    
    [closestOrbiteur positionSpeedRelativesTo: self
                                        decal: NULL
                                           in: &pg];
    speedWished = sqrt(prodScal(&pg.speed, &pg.speed));
    
#ifdef DEBUG
    if( thePref.easyPilot )
        speedWished = 0.0;
#endif
    
    [theLoop iaSchedule: self
           withSelector: @selector(hintShip)
                    arg: nil];
    modePilote = Hinter;
    sleepTimeHinter = 0;
    hinterFlag = 0;
}

- (void) stopHinterMode
{
    [self stopPousse];
    [[theLoop iaScheduler] deleteTarget: self
                           withSelector: @selector(hintShip)];
    modePilote = Manuel;
}

- (void) hintShip
{
    GGOrbiteur *closestOrbiteur = [theTableau closestOrbiteur];
    double constanteFacteur = 0;
    double normeDiff;
    PetitGalileen pg;
    Vect3D wish;
    
    if( closestOrbiteur == nil )
    {
        [self stopHinterMode];
        return;
    }
    
    [closestOrbiteur positionSpeedRelativesTo: self
                                        decal: NULL
                                           in: &pg];
    mulScalVect(Direction(self), speedWished, &wish);
    addVect(&wish, &pg.speed, &wish);
    
    normeDiff = prodScal(&wish, &wish);
    if( normeDiff > EPS*EPS )
    {
        normeDiff = sqrt(normeDiff);
        divScalVect(&wish, normeDiff, &wish);
        [self setPousse: &wish];
        [self setPower: normeDiff/(GACC(self)*deltaTime)];
    }
    else
        [self stopPousse];
    
    NSDebugMLLogWindow(@"hinter", @"accelere %d", hinterFlag);
    
    if( hinterFlag & (GGAccelere | GGDecelere) )
    {
        timerAction += realDeltaTime;
        constanteFacteur = 8+2*timerAction;
    }
    
    if( sleepTimeHinter > 0 )
    {
        sleepTimeHinter -= realDeltaTime;
    }
    else if( hinterFlag & GGAccelere )
    {
        if( speedWished < 0 && speedWished > -1 )
        {
            speedWished = 0;
            sleepTimeHinter = 2.0;
        }
        else
        {
            double K = pow(constanteFacteur, realDeltaTime);
            if( speedWished < 0 )
                speedWished /= K;
            else if (speedWished > 0 )
                speedWished *= K;
            else
                speedWished = 1;
        }
    }
    else if( hinterFlag & GGDecelere )
    {
        if( speedWished > 0 && speedWished < 1 )
        {
            speedWished = 0;
            sleepTimeHinter = 2.0;
        }
        else
        {
            double K = pow(constanteFacteur, realDeltaTime);
            if( speedWished < 0 )
                speedWished *= K;
            else if (speedWished > 0 )
                speedWished /= K;
            else
                speedWished = -1;
        }
    }
}

- (void) stopEngine
{
    [self setModePilotage: Manuel];
    [self stop];
}

- (void) finpilote: (Status) blob
{
    NSLog(@"ship arrived status = %d", blob);
    [self stopEngine];
}


- (void) setModePilotage: (ModePilotage) theNewMode
{
    if( modePilote == theNewMode )
        return;
    
    switch(modePilote)
    {
        case AutoPilote:
            [self stopAutoPilot];
            break;
        case Hinter:
            [self stopHinterMode];
            break;
        default:
            break;
    }
    
    NSDebugMLLog(@"hinter", @"newMode = %d", theNewMode);
    
    switch(theNewMode)
    {
        case AutoPilote:
            if( destination != nil )
            {
                ExtendedProcedure *mis;
                //	NSLog(@"cible = %@", cible);
                if( [destination isKindOfClass: [Station class]] )
                    // 	    mis = [FlightToStation destination: (Station *)destination
                    // 				   ship: self];
                {
                    Complex_Flight_to_station *fts;
                    fts = [Complex_Flight_to_station allocInstance];
                    [fts registerContinuation: self
                                           at: @selector(atDestination:status:)];
                    [fts runWitharg1: [destination signature]
                                arg2: self];
                    mis = fts;
                }
                else
                    //	    mis = [AutoPilotCommon autoPilotWithTarget: destination
                    //				   andShip: self];
                {
                    Complex_Autopilot *bleu;
                    bleu = [Complex_Autopilot allocInstance];
                    [bleu registerContinuation: self
                                            at: @selector(atDestination:status:)];
                    [bleu runWitharg1: destination
                                 arg2: self
                                 arg3: GGZeroVect
                                 arg4: -1
                                 arg5: -1
                                 arg6: GGModeVaisseau];
                    mis = bleu;
                }
                
                [self setIA: mis];
                RELEASE(mis);
                modePilote = AutoPilote;
                break;
            }
            //else, we go to next case :
        case Manuel:
            modePilote = Manuel;
            break;
        case Hinter:
            [self startHinterMode];
            break;
        default:
            break;
    }
}

- (void) stopAutoPilot
{
    [ia invalidate];
    AUTORELEASE(ia);
    ia = nil;
    [self stop];
}


- (void) accelere
{
    NSDebugMLLog(@"hinter", @"modePilote %d", modePilote);
    if( modePilote != Hinter)
        [super accelere];
    else
    {
        hinterFlag |= GGAccelere;
        timerAction = 0;
    }
}

- (void) stopAccelere
{
    [super stopAccelere];
    hinterFlag &= ~GGAccelere;
}

- (void) decelere
{
    if( modePilote != Hinter )
        [super decelere];
    else
    {
        hinterFlag |= GGDecelere;
        timerAction = 0;
    }
}

- (void) stopDecelere
{
    [super stopDecelere];
    hinterFlag &= ~GGDecelere;
}

- (void) setSpeedWished: (GGReal) val
{
    speedWished = val;
}

- (void) setSpeedWishedWithNumber: (NSNumber*) val
{
    [self setSpeedWished:[val doubleValue]];
}

- (void) setNearPlanete: (BOOL) flag
{
    nearPlanete = flag;
}

- (void) setAltitude: (double) val
{
    nearPlanete = YES;
    altitude = val;
}

- (void) setHauteurTerrain: (double) val
{
    nearPlanete = YES;
    hauteurTerrain = val;
}

- (BOOL) nearPlanete
{
    return nearPlanete;
}

- (double) altitude
{
    NSAssert(nearPlanete, @"no planete");
    return altitude;
}

- (double) hauteurTerrain
{
    NSAssert(nearPlanete, @"no planete");
    return hauteurTerrain;
}

- (void) tryGlobalCoordinates
{
    GGRepere * localFrame = [manager repere];
    NSAssert(localFrame == [self nodePere], @"wrong assumption about our father");
    [localFrame recenterFrameOnObjectIfNeeded:self];
    
}

- (void) notifyAttackFrom: (GGShip *)ship
{
    [theLoop setTimeSpeed: 1];
    [self notify: @"@rSHIP IS UNDER ATTACK@w"];
    NSDebugMLLog(@"Attack", @"pos = %@, adv = %@\ndist = %g",
                 stringOfVect(Point(self)), stringOfVect(Point(ship)),
                 distanceAbsolu(self, ship));
}


- (void) translate: (PetitGalileen *) ppg
          refering: (GGSpaceObject *) obj
             decal: (Vect3D *)v
{
    PetitGalileen pg = *ppg;
    BOOL cont = YES;
    do
    {
        GGSpaceObject *_closest = [theWorld closestOrbiteur];
        //      NSLog(@"closest = %@", _closest);
        [super translate: &pg
                refering: obj
                   decal: v];
        [self tryGlobalCoordinates];
        [(id)theWorld setClosestOrbiteurRec];
        if( _closest != [theWorld closestOrbiteur] )
        {
            
            [(id)[theWorld closestOrbiteur] tryBeingSuperLocal];
            
            // 	  NSLog(@"repere pos = %@", stringOfVect(Point(nodePere)));
            
            // 	  NSLog(@"v = %@", stringOfVect(v));
            
            [obj positionSpeedRelativesTo: self
                                    decal: v
                                       in: &pg];
        }
        else
            cont = NO;
    }while(cont);
}

#ifdef GGSIM

- (void) setXMouvement: (float) val
{
    GGReal sina = sin(val*M_PI/4);
    GGReal cosa = cos(val*M_PI/4);
    wings[2].n.z = -sina;
    wings[2].n.x = 0;
    wings[2].n.y = cosa;
    wings[3].n.z = sina;
    wings[3].n.x = 0;
    wings[3].n.y = cosa;
}

- (void) setYMouvement: (float) val
{
    GGReal sina = sin(val*M_PI/4);
    GGReal cosa = cos(val*M_PI/4);
    wings[4].n.z = sina;
    wings[4].n.x = 0;
    wings[4].n.y = cosa;
    //  NSLog(@"fuck %@", stringOfVect(&wings[4].n));
}

- (void) setRolMouvement: (float) val
{
    GGReal sina = sin(val*M_PI/4);
    GGReal cosa = cos(val*M_PI/4);
    wings[6].n.z = -sina;
    wings[6].n.x = cosa;
    wings[6].n.y = 0;
}

- (void) updatePosition
{
    int i;
    Vect3D grav = {0, 0, -10};
    Vect3D v;
    
    NSDebugMLLogWindow(@"ailes", @"speed %@", stringOfVect(Speed(self)));
    transposeProduitVect3D(Matrice(self), Speed(self), &v);
    //  [self addResultante: &grav];
    gAcc = 10;
    gFrein = 4;
    
    NSDebugMLLogWindow(@"ailes", @"speed %@", stringOfVect(&v));
    
    for( i = 0; i <= 6; ++i )
    {
        Vect3D temp, moment, MG, result;
        mulScalVect(&v, wings[i].alpha, &temp);
        addLambdaVect(&temp, 
                      (wings[i].beta-wings[i].alpha)*prodScal(&v, &wings[i].n),
                      &wings[i].n, &temp);
        mkVectFromPoint(&wings[i].pos, (Vect3D *)&GGZeroVect, &MG);
        prodVect(&temp, &MG, &moment);
        produitMatriceVecteur3D(Matrice(self), &temp, &result);
        [self addMomentum: &moment
               resultante: &result];
        NSDebugMLLogWindow(@"ailes", @"%@\n%@", stringOfVect(&temp),
                           stringOfVect(&moment));
    }
    [super updatePosition];
}
#else

#ifdef DEBUG
#define coef 1
- (void) updatePosition
{
    NSDebugMLLogWindow(@"Memory", @"\ncount = %d", [self retainCount]);
    if( thePref.easyPilot && wizard )
    {
        if( modeInertie & GGLeft )
            addLambdaVect(Direction(self), realDeltaTime*coef, Gauche(self),
                          Direction(self));
        if( modeInertie & GGRight )
            addLambdaVect(Direction(self), -realDeltaTime*coef, Gauche(self),
                          Direction(self));
        if( modeInertie & GGUp )
            addLambdaVect(Direction(self), -realDeltaTime*coef, Haut(self),
                          Direction(self));
        if( modeInertie & GGDown )
            addLambdaVect(Direction(self), +realDeltaTime*coef, Haut(self),
                          Direction(self));
        if( modeInertie & GGStrafLeft )
            addLambdaVect(Haut(self), +realDeltaTime*coef, Gauche(self),
                          Haut(self));
        if( modeInertie & GGStrafRight )
            addLambdaVect(Haut(self), -realDeltaTime*coef, Gauche(self),
                          Haut(self));
        addLambdaVect(Point(self), deltaTime*speedWished, Direction(self),
                      Point(self));
        
        [self normalise];
        
    }
    else
        [super updatePosition];
}
#endif

#endif


// - retain
// {
//     {
//       printf("retenu %d\n", [self retainCount]);
//     }
//   return   [super retain];

// }

// - (void)release
// {
//     {
//       printf("relache %d\n", [self retainCount]);
//     }
//   [super release];
// }

// - autorelease
// {
//     {
//       printf("autorelache %d\n", [self retainCount]);
//     }
//   return   [super autorelease];

// }

@end
