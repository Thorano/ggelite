/*
 *  Sched.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import "Sched.h"
#import "Metaobj.h"
#import "GGMainLoop.h"
#import "GGLog.h"
#import <Foundation/NSException.h>
#import <Foundation/NSObjCRuntime.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSData.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSNotification.h>
#import <objc/objc.h>
#import "GGArchiverExtension.h"

const double GGDistantFuture = 1e50;


@interface Sched (Private)
- (void)deleteAtIndex:(unsigned) ind;
- (int) indexFor: (NSObject *) obj
    withSelector: (SEL) s;
@end

@interface GGPriorityTimer : ProtoTimer
{
    int         _priority;    
}
- (id) initWithTarget:(NSObject *) _target 
             selector:(SEL)aSel
             priority:(int)priority;

- (int) priority;
@end


@implementation ProtoTimer
- (void) _prototimer_commonInit
{
    f = [target methodForSelector: msg];
    _valid = YES;
    addToCheck(self);
}

- (id) initWithTarget:(NSObject *) _target 
             selector:(SEL)aSel
{
    self = [super init];
    if(self){
        target = _target;
        msg = aSel;
        [self _prototimer_commonInit];
    }
    
    return self;
}

- (void) fire
{
    if( _valid )
        f(target, msg, self);
}

- (BOOL) isValid
{
    return _valid;
}

- (BOOL) shouldSave
{
    if( ! _valid )
        return NO;
    if( [target respondsToSelector: @selector(shouldSave)] )
    {
        return [(id)target shouldSave];
    }
    else if( [target conformsToProtocol: @protocol(NSCoding)] )
        return YES;
    return NO;
}

- (void) encodeWithCoder: (NSCoder *) encoder
{
    GGEncodeObject(target);
    NSString *selectorName = NSStringFromSelector(msg);
    GGEncodeObject(selectorName);
}

- initWithCoder: (NSCoder *)decoder
{
    NSString *selectorName;
    GGDecodeObject(selectorName);
    GGDecodeObject(target);
    msg = NSSelectorFromString(selectorName);
    [self _prototimer_commonInit];
    return self;
}
@end

@implementation GGPriorityTimer
- (id) initWithTarget:(NSObject *) _target 
             selector:(SEL)aSel
             priority:(int)priority
{
    self = [super initWithTarget:_target 
                        selector:aSel];
    if(self){
        _priority = priority;
    }
    
    return self;
}

- (int) priority
{
    return _priority;
}

- (void) encodeWithCoder: (NSCoder *) encoder
{
    [super encodeWithCoder:encoder];
    [encoder encodeInt:_priority forKey:@"priority"];
}

- initWithCoder: (NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    _priority = [decoder decodeIntForKey:@"priority"];
    return self;
}
@end

@implementation Sched
- init
{
    [super init];
    taskArray = [NSMutableArray new];
    maxElem = 0;
    indCourant = 0;
    addToCheck(self);
    return self;
}

- (void) dealloc
{
    RELEASE(taskArray);
    [super dealloc];
}

- (void) addTarget: (NSObject *) obj 
      withSelector: (SEL) s
               arg: (id) arg
          priority: (int) priority
{
    if( [self indexFor: obj
          withSelector: s] >= 0 )
    {
        NSDebugMLLog(@"Sched", @"%@ is added more than once with selector %@", 
                     obj, NSStringFromSelector(s));
        return;
    }
    
    int i  = [taskArray count] - 1;
    
    while(i >= 0){
        GGPriorityTimer* otherTimer = [taskArray objectAtIndex:i];
        if([otherTimer priority] >= priority){
            break;
        }
        i--;
    }
    GGPriorityTimer *timer = [[GGPriorityTimer alloc] initWithTarget:obj selector:s priority:priority];
    [taskArray insertObject:timer atIndex:i+1];
    [timer release];
    
    maxElem++;
}

- (void) addTarget: (NSObject *) obj 
      withSelector: (SEL) s
               arg: (id) arg
{
    [self addTarget:obj withSelector:s arg:arg priority:100];
}

- (void) deleteTarget: (NSObject *)obj
{
    int i;
    for(i = 0; i < maxElem; )
    {
        GGPriorityTimer *timer = [taskArray objectAtIndex: i];
        if( timer->target == obj )
            [self deleteAtIndex: i];
        else
            i++;
    }
}

- (BOOL) isScheduled: (NSObject *)obj
{
    int n;
    n = [taskArray count];
    if ( n > 0 )
    {
        int i;
        GGPriorityTimer *objs[n];
        
        [taskArray getObjects: objs];
        
        for(i = 0; i < n; ++i)
            if( objs[i]->target == obj )
                return YES;
    }
    return NO;
}

- (void) deleteTarget: (NSObject *)obj
         withSelector: (SEL) s
{
    int i;
    
    
    i = 0;
    while( i < maxElem )
    {
        GGPriorityTimer *timer = [taskArray objectAtIndex: i];
        if( timer->target == obj && sel_eq(timer->msg, s) )
        {
            [self deleteAtIndex: i];
        }
        else
            i++;
    }
}

- (void)launch
{
    if( maxElem == 0)
        return;
    if( indCourant >= maxElem )
        indCourant = 0;
    [(GGPriorityTimer *)[taskArray objectAtIndex: indCourant] fire];
    indCourant++;
}

- (void)launchAll
{
    GGPriorityTimer *(*getobj)(id,SEL, int);
    SEL s;
    s = @selector(objectAtIndex:);
    getobj = (GGPriorityTimer *(*)(id, SEL, int))[taskArray methodForSelector: s];
    for( indCourant = 0; indCourant < maxElem; ++indCourant)
        [getobj(taskArray,s,indCourant) fire];
}

- (void) logInWindow
{
    int n = [taskArray count];
    NSAssert(n == maxElem, @"bad");
    if( n > 0 )
    {
        int i;
        GGPriorityTimer *timers[n];
        [taskArray getObjects: timers];
        for( i = 0; i < maxElem; ++i)
            if( timers[i]->_valid )
                NSDebugLLogWindow(@"Sched", @"%@ %@", timers[i]->target,
                                  NSStringFromSelector(timers[i]->msg));
    }
}

- (NSString *) description
{
    GGPriorityTimer *timer;
    NSEnumerator *en = [taskArray reverseObjectEnumerator];
    NSMutableString *res = [NSMutableString stringWithUTF8String: "GGTimer Sched\n"];
    while( (timer = [en nextObject]) != nil)
    {
        if( [timer isValid] )
            [res appendFormat: @"%@ %@\n", timer->target, 
                NSStringFromSelector(timer->msg)];
    }
    return res;
}

static NSArray *archivableObjects(NSArray *taskArray)
{
    int n = [taskArray count];
    NSMutableArray *array = [NSMutableArray array];
    
    if( n > 0)
    {
        GGPriorityTimer *timers[n];
        int i;
        [taskArray getObjects: timers];
        
        for(i = 0; i < n; ++ i)
        {
            if( [timers[i] shouldSave] )
            {
                [array addObject: timers[i]];
                NSDebugLLog(@"Coding", @"saving %@\n%@", timers[i]->target,
                            NSStringFromSelector(timers[i]->msg));
            }
        }
    }
    return array;
}

- (void) encodeWithCoder: (NSCoder *)encoder
{
    NSArray *array;
    NSAssert([taskArray count] == maxElem, @"bad");
    
    array = archivableObjects(taskArray);
    
    [encoder encodeObject: array forKey:@"timers"];
}

- initWithCoder: (NSCoder *) decoder
{
    taskArray = [[decoder decodeObjectForKey:@"timers"] retain];
    maxElem = [taskArray count];
    addToCheck(self);
    return self;
}
@end

@implementation Sched (Private)
- (void)deleteAtIndex:(unsigned) ind
{
    NSAssert(ind < maxElem, @"index greater than maxElem");
    [taskArray removeObjectAtIndex: ind];
    maxElem--;
    if( ind < indCourant )
        indCourant--;
}

- (int) indexFor: (NSObject *) obj
    withSelector: (SEL) s
{
    int n = [taskArray count];
    NSAssert(n == maxElem, @"bad");
    if( n > 0 )
    {
        int i;
        GGPriorityTimer *timers[n];
        [taskArray getObjects: timers];
        for( i = 0; i < maxElem; ++i)
            if( timers[i]->target == obj && sel_eq(timers[i]->msg, s) )
                return i;
    }
    
    return -1;
}

@end


@implementation TaskSched
- init
{
    [super init];
    array = [NSMutableArray new];
    addToCheck(self);
    return self;
}

- (void) dealloc
{
    RELEASE(array);
    [super dealloc];
}

- (void) encodeWithCoder: (NSCoder *)encoder
{
    NSArray *_array;
    
    _array = archivableObjects(array);
    
    [encoder encodeObject: _array forKey:@"timers"];
}

- initWithCoder: (NSCoder *) decoder
{
    array = [[decoder decodeObjectForKey:@"timers"] retain];
    addToCheck(self);
    return self;
}


- (void) schedule: (GGTimer *)timer
          forDate: (double) val
{
    int n = [array count];
    int i = n;
    if( n > 0)
    {
        GGTimer *thetimers[n];
        [array getObjects: thetimers];
        for( i = n ; i > 0 && val > thetimers[i-1]->datef; --i);
    }
    [array insertObject: timer
                atIndex: i];
}

- (void) run
{
    while([array count] > 0)
    {
        GGTimer *timer;
        timer = [array lastObject];
        if( timer->datef <= gameTime )
        {
            RETAIN(timer); //just in case
            [array removeLastObject];
            if( timer->_valid )
            {
                [timer fire]; 
                if( timer->_valid && timer->_repeat )
                {
                    double newDate;
                    newDate = timer->datef + timer->interval;
                    
                    //if the newDate is in the past, the timer will fire
                    // again immediately
                    // Can be bad in very accelerated time speed when
                    // the interval is very small.
                    // So we do it only for strict timer (they aren't by default)
                    if( !timer->_strict && newDate < gameTime )
                        newDate = gameTime;
                    [timer scheduleForDate: newDate];
                }
            }
            RELEASE(timer);
        }
        else
            break;
    }
}

- (void) reschedule: (GGTimer *) timer
            forDate: (double) newtime
{
    // SPEEDUP:
    int index;
    index = [array indexOfObjectIdenticalTo: timer];
    if( index == NSNotFound )
    {
        NSWarnMLog(@"timer %@ is not scheduled anymore, we add it again", 
                   timer);
        [self schedule: timer
               forDate: newtime];
    }
    else
    {
        RETAIN(timer);  //just in case we are the only one to posses it...
        [array removeObjectAtIndex: index];
        [self schedule: timer
               forDate: newtime];
        RELEASE(timer);
    }
}

- (void) unSchedule:(GGTimer *)timer
{
    [array removeObjectIdenticalTo: timer];
}

- (NSString *) description
{
    GGTimer *task;
    NSEnumerator *en = [array reverseObjectEnumerator];
    NSMutableString *res = [NSMutableString stringWithUTF8String: "GGTimer Sched\n"];
    while( (task = [en nextObject]) != nil)
    {
        if( [task isValid] )
            [res appendFormat: @"%@ : %g %@\n", task, task->datef-gameTime, task->target];
    }
    return res;
}
@end

@implementation GGTimer
-  initWithObject: (NSObject *) obj
          message: (SEL) s
          repeats: (BOOL) r
     timeInterval: (double) ti
{
    [super initWithTarget:obj selector:s];
    datef = ti;	
    _valid = YES;
    _repeat =r;
    interval = ti;
    addToCheck(self);
    NSDebugMLLog(@"GGTimer", @"%@ with %@ %@", 
                 self, obj, NSStringFromSelector(s));
    return self;
}

-  initWithObject: (NSObject *) obj
          message: (SEL) s
{
    return [self initWithObject: obj
                        message: s
                        repeats: NO
                   timeInterval: 0.0];
}


+ timerWithObject: (NSObject *) obj
          message: (SEL) s
{
    GGTimer *ggtimer;
    ggtimer = [self alloc];
    [ggtimer initWithObject: obj
                    message: s];
    return AUTORELEASE(ggtimer);
}

+ scheduledTimerWithTimeInterval:(double)seconds 
                          target:(id) obj
                        selector: (SEL) s
                         repeats:(BOOL) r
{
    GGTimer *ggtimer;
    ggtimer = [self alloc];
    [ggtimer initWithObject: obj
                    message: s
                    repeats: r
               timeInterval: seconds];
    [ggtimer scheduleForDate: gameTime + seconds];
    return AUTORELEASE(ggtimer);
}

+ scheduledTimerForDate:(double)seconds 
                 target:(id) obj
               selector: (SEL) s
{
    GGTimer *ggtimer;
    ggtimer = [self alloc];
    [ggtimer initWithObject: obj
                    message: s
                    repeats: NO
               timeInterval: seconds];
    [ggtimer scheduleForDate: seconds];
    return AUTORELEASE(ggtimer);
}


- (void) encodeWithCoder: (NSCoder *) encoder
{
    [super encodeWithCoder: encoder];
    GGEncodeBool(_repeat);
    GGEncodeBool(_strict);
    GGEncodeDouble(datef);
    GGEncodeDouble(interval);
}

- initWithCoder: (NSCoder *)decoder
{
    [super initWithCoder: decoder];
    GGDecodeBool(_repeat);
    GGDecodeBool(_strict);
    GGDecodeDouble(datef);
    GGDecodeDouble(interval);
    return self;
}

- (void) scheduleForDate: (double) val
{
    TaskSched *taskSched;
    datef = val;
    taskSched =  [self shouldSave]  ? [theLoop taskSched] :
        [theLoop taskSchedNoSave];
    [taskSched schedule: self
                forDate: val];
}

- (void) rescheduleForDate: (double) val
{
    TaskSched *taskSched;
    if( val > 0 ) 
    {
        datef = val;
        taskSched =  [self shouldSave]  ? [theLoop taskSched] :
            [theLoop taskSchedNoSave];
        [taskSched   reschedule: self
                        forDate: val];
    }
    else 
        [self invalidate];
}

- (double) date
{
    return datef;
}


- (void) invalidate
{
    _valid = NO;
}



- (void) suspend  //equivalent to rescheduleForDate: \infnty
{
    [self rescheduleForDate: GGDistantFuture];
}

- (void) changeInterval: (double) newval
{
    interval = newval;
}
@end


ListNamed *obsTable;

static void destroyList(ListNamed **lst)
{
    if( *lst != NULL )
    {
        ListNamed *tail;
        [[NSNotificationCenter defaultCenter]
	removeObserver: (*lst)->obs
              name: (*lst)->name
            object: (*lst)->obj];
        tail = (*lst)->next;
        DESTROY((*lst)->name);
        free(*lst);
        *lst = NULL;
        destroyList(&tail);
    }
}

static void addList(ListNamed **lst, id obs, SEL s, NSString *name, id obj)
{
    if( [obs respondsToSelector: @selector(saveChannel)] && 
        [obs saveChannel] )
    {
        ListNamed *ptr;
        ptr = (ListNamed *)malloc(sizeof(ListNamed));
        
        ptr->obs = obs;
        ptr->sel = s;
        ptr->name = RETAIN(name);
        ptr->obj = obj;
        ptr->next = *lst;
        *lst = ptr;
    }
}

static BOOL match1(ListNamed *lst1, ListNamed *lst2)
{
    if( lst2->obs == nil || lst1->obs == lst2->obs )
    {
        if( (lst2->name == nil || [lst2->name isEqualToString: lst1->name]) &&
            (lst2->obj == nil  || lst1->obj == lst2->obj) )
            return YES;
    }
    return NO;
}

static void removeElementsMatching(ListNamed **lst, ListNamed *comp)
{
    NSCAssert(comp->obs || comp->name || comp->obj, 
              @"bad");
    if( *lst != NULL )
    {
        if( match1(*lst, comp) )
        {
            ListNamed *next;
            next = (*lst)->next;
            DESTROY((*lst)->name);
            free(*lst);
            *lst = next;
            removeElementsMatching(lst, comp);
        }
        else
            removeElementsMatching(&(*lst)->next, comp);
        
    }
}

static void encodeList(ListNamed *lst, NSCoder *encoder)
{
    if( lst == NULL )
        [encoder encodeObject: nil];
    else
    {
        [encoder encodeObject: lst->obs];
        [encoder encodeObject: NSStringFromSelector(lst->sel)];
        [encoder encodeObject: lst->name];
        [encoder encodeObject: lst->obj];
        encodeList(lst->next, encoder);
    }
}

static void createWithCoder(ListNamed **lst, NSCoder *decoder)
{
    id val;
    val = [decoder decodeObject];
    if( val != nil )
    {
        id val2, val3, val4;
        SEL s;
        val2 = [decoder decodeObject];
        val3 = [decoder decodeObject];
        val4 = [decoder decodeObject];
        s = NSSelectorFromString(val2);
        NSDebugLLog(@"Message1", @"%@ %@ %@ %@", val, val2,
                    val3, val4);
        addList(lst, val, s,
                val3, val4);
        createWithCoder(lst, decoder);
        [[NSNotificationCenter defaultCenter]
	addObserver: val
       selector: s
           name: val3
         object: val4];
    }
}

static NSString *description(NSMutableString *str, ListNamed *lst)
{
    if( lst )
    {
        [str appendFormat: @"obs: %@  name: %@ obj: %@\n",
            lst->obs, 
            lst->name, lst->obj];
        description(str, lst->next);
    }
    return str;
}

static int depthList(ListNamed *lst, int val)
{
    if( lst )
        return depthList(lst->next, val+1);
    else
        return val;
}

@implementation MessageManager
static MessageManager *template;
+ (void) initialize
{
    if( self == [MessageManager class] )
    {
        template = [super allocWithZone: NSDefaultMallocZone()];
    }
}

+ (id) allocWithZone: (NSZone*)z
{
    return template;
}

+ (void) addObserver: obs
            selector: (SEL) s
                name: (NSString *) name
              object: obj
{
    NSAssert(obs != nil, @"bad");
    addList(&obsTable, obs, s, name, obj);
    [[NSNotificationCenter defaultCenter]
    addObserver: obs
       selector: s
           name: name
         object: obj];
}

+ (void) removeObserver: obs
                   name: (NSString *)s
                 object: obj
{
    ListNamed temp;
    temp.obs = obs;
    temp.name = s;
    temp.obj = obj;
    
    NSDebugMLLog(@"Message", @"%p %p %p %d", obs, s, obj, 
                 depthList(obsTable, 0));
    removeElementsMatching(&obsTable, &temp);
    NSDebugMLLog(@"Message", @"new depth %d",   depthList(obsTable, 0));
    [[NSNotificationCenter defaultCenter]
    removeObserver: obs
              name: s
            object: obj];
}

+ (void) removeObserver: obs
{
    [self removeObserver: obs
                    name: nil
                  object: nil];
}

+ (BOOL) isObserverOrWatched: obj
{
    ListNamed *scan = obsTable;
    
    while(scan)
    {
        if (scan->obs == obj || scan->obj == obj ) return YES;
        scan = scan->next;
    }
    return NO;
}

+ (NSString *) description
{
    NSMutableString *str = [NSMutableString stringWithString: @"Messages\n"];
    return description(str, obsTable);
}

- (void) encodeWithCoder: (NSCoder *)encoder
{
    encodeList(obsTable, encoder);
}

- (void) dealloc
{
    if(0){
        [super dealloc];
    }
}

- initWithCoder: (NSCoder *)decoder
{
    destroyList(&obsTable);
    createWithCoder(&obsTable, decoder);
    return self;
}

- (NSString *) description
{
    return [[self class] description];
}

@end

