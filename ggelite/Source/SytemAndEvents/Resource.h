/* 	-*-ObjC-*- */
/*
 *  Resource.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: April 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Resource_h
#define __Resoure_h

#import <Foundation/NSObject.h>

@class NSMutableDictionary;
@class NSMutableArray;
@class GGFont;
@class NSBundle;

@interface Resource : NSObject
{
  GGFont		*defaultFont;
  NSMutableDictionary 	*resourceDic;
  NSMutableArray	*toClean;
  NSArray		*ships;
  NSBundle		*myBundle;
}
+ initResourceManager;
+ resourceManager;

+ (void) destroyResourceManager;

- (NSArray *) ships;
// - (NSString *)fontPath;
// - (NSString *) texturePath;
// - (NSString *) modelPath;
- (GGFont *) defaultFont;
- (NSString *)bundlePath;
- (NSData *) dataForResource:(NSString*)resource
                 inDirectory:(NSString*)directory;
- (NSString *)pathForSound: (NSString *)str;
- (NSString *)pathForModel: (NSString *)str;
- (NSString *)pathForTexture: (NSString *)str;
- (NSString *)pathForFont: (NSString *)str;
- (void) setDefaultFont: (GGFont *) aFont;
- (id) resourceWithName: (NSString *)name;
- (void) removeResourceForName: (NSString *) name;
- (void) addResource: (id) obj
	    withName: (NSString *)name;
- (void) addForCleaning: obj;
@end


#endif
