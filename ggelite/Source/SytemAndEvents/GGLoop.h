/* 	-*-ObjC-*- */
/*
 *  GGLoop.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGLoop_h
#define __GGLoop_h

#import <Foundation/NSObject.h>
#import "GG3D.h"
#import "GGMainLoop.h"
#import "GGEvent.h"


@protocol InputHandler
- (BOOL) handleEvent: (Event *)event;
@end

@interface GGMainLoop (EventDispatch)

// top entry point to dispatch events through the game.

- (BOOL) dispatchEvent:(Event*)event;

@end

GGBEGINDEC
@class GGRepere;
@class GGTableau;
@class GGSpaceObject;
@class GGSky;
@class GGNSWindow;
@class NSMutableArray;
@class Sched;
@class TaskSched;
@class World;
@class GGInput;
@class SoundManager;
@class GGEventDispatcher;
/*%%%
import GGMainLoop ;
%%%*/

@interface GGMainLoop (GGTrick)
- (void) pushWindow: (GGNSWindow *) aWin;
- (void) popWindow: (GGNSWindow *)aWin;

- (void) setPollMode: (int) theMode;
- (void) displayGL;
- (void) runOnce;

- (GGNSWindow *) windowFirstResponder;

// top entry point to dispatch events through the game.
//
//- (BOOL) dispatchEvent:(Event*)event;
//
@end


@interface GGLoop : GGMainLoop
{
    GGNSWindow		*preference;    
    GGNSWindow		*window;
    SoundManager	*sound;
    short			xMouse, yMouse;
    BOOL			displayMouseCursor;
    int             pollMode;
    BOOL			grab;
    BOOL			enterMode;
    GGEventDispatcher*  _dispatcher;
}

- (void) setMainWindow: (GGNSWindow *)aWin;
- (void) pushWindow: (GGNSWindow *) aWin;
- (void) popWindow: (GGNSWindow *)aWin;
- (void) enableJoystick: (BOOL) val;
- (void) setSoundFlag: (BOOL) val;


// might be subclassed to handle events
- (void) dispatchEventIfNeeded;
- (void) drawMouseCursor;

- (void) runLoop;


- (void) beginGrab;
- (void) endGrab;

@end
GGENDDEC

void gg_resize_screen( unsigned int width, unsigned int height );

extern NSString *GGScreenSizeChange;

extern int  WIDTH;
extern int  HEIGHT;

extern BOOL wizard;
extern struct SDL_Surface *screen;

#define WaitForEvent   			0
#define DontWaitForEvent		1

#endif
