/* 	-*-ObjC-*- */
/*
 *  Resource.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: April 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import "Resource.h"
#import "Metaobj.h"
#import "GGFont.h"
#import "Preference.h"
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#import <Foundation/NSFileManager.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSPathUtilities.h>
#import <Foundation/NSBundle.h>
#import <Foundation/NSException.h>

static Resource *theResourceManager;


@implementation Resource
+ initResourceManager
{
    if( theResourceManager != nil )
        return theResourceManager;
    
    //execute only once :
    [Pref loadOption]; //call initialize
    theResourceManager = [self new];
    [theResourceManager addForCleaning: [Pref class]];
    return theResourceManager;
}

+ resourceManager
{
    return [self initResourceManager];
}

- (void) destroyResourceManager
{
    [toClean makeObjectsPerformSelector: @selector(clean)];
    DESTROY(toClean);
}


+ (void) destroyResourceManager
{
    [theResourceManager destroyResourceManager];
    DESTROY(theResourceManager);
}

// - (NSString *) modelPath
// {
//   return @"data/model";
// }

// - (NSString *) texturePath
// {
//   return @"data/textures";
// }

- (NSString *)bundlePath
{
    return @"./Library/Bundles/";
}

- (NSString *) pathForResource: (NSString *) str
                   inDirectory: (NSString *)dir
{
    if (0)
    {
        return 
        [[@"data" stringByAppendingPathComponent: dir]
          stringByAppendingPathComponent: str];
    }
    else
    {
        NSString *ext = [str pathExtension];
        return [myBundle pathForResource: [str stringByDeletingPathExtension]
                                  ofType: ext
                             inDirectory: dir];
    }
}

- (NSData *) dataForResource:(NSString*)resource
                 inDirectory:(NSString*)directory
{
    NSString* path = [self pathForResource:resource inDirectory:directory];
    NSData* res = nil;
    if(path){
        res = [NSData dataWithContentsOfFile:path];
    }
    
    return res;
}

- (NSArray *) pathsForResourcesOfType: (NSString *) str
                          inDirectory: (NSString *)dir
{
    if (0)
    {
        NSString *p;
        NSFileManager *m;
        NSArray *a;
        
        p = [@"data" stringByAppendingPathComponent: dir];
        m = [NSFileManager defaultManager];
        a = [m directoryContentsAtPath: p];
        a = [a pathsMatchingExtensions: [NSArray arrayWithObject: @"shp"]];
        return a;
    }
    else
        return  [myBundle pathsForResourcesOfType: @"shp" 
                                      inDirectory: @"model"];
}

- (NSString *)pathForSound: (NSString *)str
{
    return [self pathForResource: str
                     inDirectory: @"sound"];
    //  return [@"data/sound" stringByAppendingPathComponent: str];
}

- (NSString *) pathForModel: (NSString *)str
{
    return [self pathForResource: str
                     inDirectory: @"model"];
    //  return [@"data/model" stringByAppendingPathComponent: str];
}

- (NSString *) pathForTexture: (NSString *)str
{
    return [self pathForResource: str 
                     inDirectory: @"textures"];
    //  return [@"data/textures" stringByAppendingPathComponent: str];
}

- (NSString *) pathForFont: (NSString *)str
{
    return [self pathForResource: str
                     inDirectory: @"fonts"];
    
    //  return [fontPath stringByAppendingPathComponent: str];
}

- (void) initShips
{
    NSMutableArray *res;
    NSArray *array;
    NSString *s;
    NSEnumerator *en;
    array = [self pathsForResourcesOfType: @"shp" 
                              inDirectory: @"model"];
    en = [array objectEnumerator];
    res = [NSMutableArray array];
    while((s = [en nextObject]) != nil)
    {
        s = [s lastPathComponent];
        [res addObject: s];
    }
    [res sortUsingSelector: @selector(caseInsensitiveCompare:)];
    
    ASSIGN(ships, res);
}


- (NSArray *) ships
{
    if(!ships)
        [self initShips];
    return ships;
}

- init
{
    [super init];
    
    myBundle = [NSBundle mainBundle];
    
    NSAssert(myBundle != nil, @"Unable to locate resource files.\nAre you sure that ggelite is properly installed?");
    
    RETAIN(myBundle);
    
    //find the resources:
    resourceDic = [NSMutableDictionary new];
    toClean = [NSMutableArray new];
    addToCheck(self);
    
    return self;
}

- (void) addForCleaning: obj
{
    NSDebugMLLog(@"AutoClean", @"adding %@", obj);
    [toClean addObject: obj];
}  

- (void) addResource: (id) obj
            withName: (NSString *)name
{
    [resourceDic setObject: obj
                    forKey: name];
}

- (void) removeResourceForName: (NSString *) name
{
    [resourceDic removeObjectForKey: name];
}

- (id) resourceWithName: (NSString *)name
{
    return [resourceDic objectForKey: name];
}

- (void) dealloc
{	
    RELEASE(toClean);
    RELEASE(defaultFont);
    RELEASE(resourceDic);
    RELEASE(myBundle);
    [super dealloc];
}

- (GGFont *) defaultFont
{
    return defaultFont;
}

- (void) setDefaultFont: (GGFont *) aFont
{
    ASSIGN(defaultFont, aFont);
}
@end
