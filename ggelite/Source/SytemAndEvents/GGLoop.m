/*
 *  GGLoop.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifdef USE_SDL
#import <SDL/SDL.h>
#endif

#import <stdio.h>
#import <stdlib.h>
#import <math.h>
#import <time.h>


#import <Foundation/NSDate.h>
#import <Foundation/NSRunLoop.h>
#import <Foundation/NSException.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSData.h>
#import <Foundation/NSException.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSValue.h>
#import "GGLoop.h"
#import "Console.h"
#import "GGSpaceObject.h"
#import "utile.h"
#import "Tableau.h"
#import "Metaobj.h"
#import "NSWindow.h"
#import "NSView.h"
#import "Sched.h"
#import "Preference.h"
#import "SoundSDL.h"
#import "GGRunTime.h"
#import "GGEventDispatcher.h"
#import "World.h"


int  WIDTH=800;
int  HEIGHT=600;

static float fogcolor[4]={1.0,1.0,1.0,1.0};

NSString *GGScreenSizeChange = @"GGScreenSizeChange";

void gg_resize_screen( unsigned int width, unsigned int height )
{
    NSSize oldSize = NSMakeSize(WIDTH, HEIGHT);
    NSValue *oldData = [NSValue valueWithSize: oldSize];
    NSDebugFLLog(@"Resize", @"old : (%d, %d) new : (%d %d)", WIDTH, HEIGHT,
                 width, height);
    WIDTH = width;
    HEIGHT = height;
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGScreenSizeChange object: oldData];
}


@interface GGLoop (Private)
- (void )initGLContext;
- (void) initJoystickSystem;
@end

#define PlayMode  	0
#define UIMode		1


@implementation GGLoop
- init
{
    [super init];
    
    grab = NO;
    enterMode = 0;
    
    xMouse = 0;
    yMouse = 0;
    displayMouseCursor = YES;
    pollMode = DontWaitForEvent;
    
    
    
    return self;
}


- (void) beforeRunningLoop
{
    [super beforeRunningLoop];
    [self initGLContext];
    preference = RETAIN([UserPreference window]);
    
    if ( thePref.enableJoystick )
        [self initJoystickSystem];
    if( thePref.sound )
        sound = [SoundManager new];
    else
        sound = nil;
    _dispatcher = [[GGEventDispatcher dispatcherWithName:@"ApplicationKeys"] retain];
}

- (void) dealloc
{
    RELEASE(sound);
    RELEASE(window);
    RELEASE(preference);
    RELEASE(_dispatcher);
    
    [super dealloc];
}

#pragma mark -
#pragma mark Joystick management

- (BOOL) hasJoystick
{
    return NO;
}

- (void) initJoystickSystem
{
    
}

- (void) enableJoystick: (BOOL) val
{
    
}

#pragma mark -
- (void )initGLContext
{
    GLfloat mat_specular[] = { 0.2, 0.4, 0.3, 1.0 };
    GLfloat ambient[] = { 0.4, 0.4, 0.4, 1.0};
    GLfloat mat_shininess[] = { 50.0 };
    
    glShadeModel(GL_FLAT);
    
    /*  glEnable(GL_BLEND);*/
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    //   glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_CULL_FACE);
    
    //  glEnable(GL_FOG);
    glFogi(GL_FOG_MODE,GL_EXP);
    glFogfv(GL_FOG_COLOR,fogcolor);
    glFogf(GL_FOG_DENSITY,0.1);
    glHint(GL_FOG_HINT,GL_NICEST);
    
    
    //   glShadeModel (GL_SMOOTH);
    
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
    glLightModeli(GL_LIGHT_MODEL_COLOR_CONTROL, GL_SEPARATE_SPECULAR_COLOR);
    
    glEnable(GL_DEPTH_TEST);
    {
        NSString *glExt;
        NSArray *ext;
        glExt = [[NSString alloc] initWithUTF8String:(const char *)glGetString(GL_EXTENSIONS)];
        ext = [glExt componentsSeparatedByString: @" "];
        [glExt release];
        NSDebugMLLog(@"gl", @"Extension:\n%@", [ext componentsJoinedByString: @"\n"]);
        NSDebugMLLog(@"gl", @"Version : %s", glGetString(GL_VERSION));
    }
    
    [[NSNotificationCenter defaultCenter]
    addObserver: self
       selector: @selector(toggleFullscreen:)
           name: @"ToggleFullScreen"
         object: nil];
}


- (void) toggleFullscreen: (NSNotification *) aNotification
{
}

- (GGNSWindow *) windowFirstResponder
{
    return [window windowOnTop];
}

- (void) setMainWindow: (GGNSWindow *)aWin
{
    ASSIGN(window, aWin);
}

- (void) pushWindow: (GGNSWindow *)aWin
{
    if( window != nil )
    {
        [window looseFocus];
        [window pushOnTheTop: aWin];
    }
    else
    {
        [self setMainWindow: aWin];
    }
    [aWin takeFocus];
}

- (void) popWindow: (GGNSWindow *)aWin
{
    [aWin looseFocus];
    if( aWin == window )
    {
        NSAssert([aWin windowBelow] == nil, @"bad win");
        [self setMainWindow: nil];
    }
    else
    {
        GGNSWindow *below = [aWin windowBelow];
        NSDebugMLLog(@"GGNSWindow", @"pop %@", aWin);
        NSAssert(below != nil, @"bad win");
        [below popWindow];
        [below takeFocus];
    }
}

- (void) setPollMode: (int) theMode
{
    pollMode = theMode;
}

- (void) setSoundFlag: (BOOL) val
{
    if( val && sound == nil )
        sound = [SoundManager new];
    else if(!val && sound != nil )
    {
        //      [sound endSound];
        DESTROY(sound);
    }
}

#pragma mark -
#pragma mark commands

#ifdef DEBUG
- (void) beginCollection
{
    gg_begin_collection();
}

- (void) loggcconsole
{
    loggcconsole();
}

- (void)wizzardQuit{
    if( wizard )
        exit(1);
}
#endif

- (void) quickSave
{
    [theWorld saveGame: @"test.gge"];
}

- (void) quickLoad
{
    [theWorld loadGame: @"test.gge"];
}

- (void) takeAScreenShot
{
    unsigned char *data;
    data = NSZoneMalloc(NSDefaultMallocZone(), WIDTH*HEIGHT*4);
    
    glPixelStorei(GL_PACK_ALIGNMENT,1);
    glReadPixels(0, 0, WIDTH, HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, data);
    writeRGBTofile(WIDTH, HEIGHT, data);
    NSZoneFree(NSDefaultMallocZone(), data);
}

- (void) openPreferences
{
    if( ![preference isOpen] )
    {
        [preference open];
    }
    else{
        [self stopMainLoop];
    }
}

#pragma mark -
#pragma mark event and loop

- (BOOL) dispatchEvent:(Event*)event
{
    if(typeOfEvent(event) == GG_KEYDOWN){
        if([_dispatcher dispatchKeyDownWithEvent:event target:self]){
            return YES;
        }
    }
    
    GGNSWindow *topWin = [self windowFirstResponder];
    return [topWin handleEvent:event];
    
}

- (void) dispatchEventIfNeeded
{
    
}

- (void) drawMouseCursor
{
    if( displayMouseCursor )
    {
        glViewport(0, 0, WIDTH, HEIGHT);
        glDisable(GL_SCISSOR_TEST);
        glMatrixMode(GL_PROJECTION);
        {
            glPushMatrix();
            glLoadIdentity();
            glOrtho(0, WIDTH, 0, HEIGHT, -1, 1);
            glMatrixMode(GL_MODELVIEW);
            {
                glPushMatrix();
                glLoadIdentity();
                glColor3f(0.8, 1.0, 1.0);
                {
                    glBegin(GL_LINES);
                    glVertex2i(xMouse-10, yMouse-10);
                    glVertex2i(xMouse+10, yMouse+10);
                    glVertex2i(xMouse-10, yMouse+10);
                    glVertex2i(xMouse+10, yMouse-10);
                    glEnd();
                }
                glPopMatrix();
            }
            glMatrixMode(GL_PROJECTION);
            glPopMatrix();
        }
        glMatrixMode(GL_MODELVIEW);
    }
}

- (void) runOnce
{
    char buffer[20];

    [self runTimers];
    
    if( enterMode )
    {
        fgets(buffer, 20, stdin);
        lastTime = gettimesec(); //suspend time
    }
    
    
    NSDebugLLogWindow(@"TaskSched", @"%@", state->taskSched);
    NSDebugLLogWindow(@"TaskSched", @"%@", noSaveSched);
    
#ifdef DEBUG
    NSDebugLLogWindow(@"Sched", @"slow : %@",state->slowScheduler);
    NSDebugLLogWindow(@"Sched", @"fast : %@",state->fastScheduler);
    NSDebugLLogWindow(@"Sched", @"ia : %@", state->iaScheduler);
    NSDebugLLogWindow(@"Channel", @"bla \"%@\"\nblou %d", [MessageManager class],
                      obsTable);
#endif
    
}

- (void) displayGL
{
    [window display];
    
    [self drawMouseCursor];
    
    
    GLCHECK;
    
    printGLError(nil);
}

- (void) runLoop
{
    [self beforeRunningLoop];
    // main loop
    
    cont = 1;
    
    while (cont) {
        NSAutoreleasePool *pool = [NSAutoreleasePool new];
        [self runOnce];
        [self displayGL];
#if USE_SDL
        SDL_GL_SwapBuffers();
#endif
        [self dispatchEventIfNeeded];
        RELEASE(pool);
    }
}

#pragma mark -
#pragma mark Mouse various

- (void) beginGrab
{
    NSDebugMLLog(@"grab", @"grab");
    //    XGrabPointer(disp, win, False, ButtonPressMask | PointerMotionMask ,
    //  	       GrabModeAsync, GrabModeAsync, win, None , CurrentTime);
    displayMouseCursor = NO;
    grab = YES;
}
- (void) endGrab
{
    NSDebugMLLog(@"grab", @"ungrab");
    //    XUngrabPointer(disp, CurrentTime);
    grab = NO;
    displayMouseCursor = YES;
}
@end

