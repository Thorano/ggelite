//
//  GGEliteController.m
//  elite
//
//  Created by Frédéric De Jaeger on 23/11/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGEliteController.h"
#import "GGInput.h"
#import "World.h"


@implementation GGEliteController
- (void) dealloc
{
    theGGInput = nil;
    RELEASE(_world);
    RELEASE(_input);

    [super dealloc];
}

- (void) startup
{
    _input = [GGInput new];
    _world = [World new];
    [_input open];
    //  [self addInputHandler: input];
    
    assert(theWorld);
    [theWorld loadBaseSystem];
}

- (GGInput *) input
{
    return _input;
}
@end
