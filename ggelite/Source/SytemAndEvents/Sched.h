/* 	-*-ObjC-*- */
/*
 *  Sched.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Sched_h
#define __Sched_h

#import <Foundation/NSObject.h>
#import "config.h"

/* informal protocol */
@protocol GGSavable
- (BOOL) shouldSave;
- (BOOL) saveChannel;
@end

@interface ProtoTimer : NSObject <NSCoding>
{
    @public
    NSObject	*target;
    SEL		msg;
    IMP		f;
    BOOL			_valid;
}
- (id) initWithTarget:(NSObject *) _target 
             selector:(SEL)aSel;
- (BOOL) isValid;
- (void) fire;
- (BOOL) shouldSave;

@end

@class NSMutableArray;

@interface Sched : NSObject <NSCoding>
{
  NSMutableArray	*taskArray;
  unsigned		maxElem;
  int			indCourant;
}
- (void)launchAll;
- (void)launch;
- (void) deleteTarget: (NSObject *)obj;
- (void) deleteTarget: (NSObject *)obj
	 withSelector: (SEL) s;

- (void) addTarget: (NSObject *) obj 
      withSelector: (SEL) s
               arg: (id) arg
          priority: (int) priority;

- (void) addTarget: (NSObject *) obj 
      withSelector: (SEL) s
	       arg: (id) arg;

- (void) logInWindow;
- (BOOL) isScheduled: (NSObject *)obj;
@end

GGBEGINDEC
/* a GGTimer is exactly like an NSTimer except that it works with gameTime.  
   That the reason why I couldn't use NSTimer.  It's because they work with
   GGReal time.

   When the timer fires, the code it calls should not try to reschedule the
   same timer.
*/
@interface GGTimer : ProtoTimer <NSCoding>
{
@public
  BOOL			_repeat;
  BOOL			_strict;
  double		datef;
  double		interval;
}
-  initWithObject: (NSObject *) obj
	  message: (SEL) s
	  repeats: (BOOL) r
     timeInterval: (double) ti;
+ timerWithObject: (NSObject *) obj
	  message: (SEL) s;
+ scheduledTimerWithTimeInterval:(double)seconds 
			  target:(id) invocation 
			selector: (SEL) s
			 repeats:(BOOL)repeats;
+ scheduledTimerForDate:(double)seconds 
		 target:(id) invocation 
	       selector: (SEL) s;

- (void) scheduleForDate: (double) val;
- (void) rescheduleForDate: (double) val;
- (double) date;
- (void) invalidate;
- (void) suspend;  //equivalent to rescheduleForDate: \infnty
- (void) changeInterval: (double) newval; //only useful for repeated timer

@end

extern const double GGDistantFuture;

/*%%%
extern distant_future : Float = "GGDistantFuture";
%%%*/
GGENDDEC


/*This class schedules timers of type GGTimer.  A GGTimer is like an
NSTimer.  It is an object that will fire an action when some time (measure in
game time occurs).  This object manages them
*/
@interface TaskSched : NSObject <NSCoding>
{
  NSMutableArray  *array;
}
- (void) schedule: (GGTimer *)timer
	  forDate: (double) val;
- (void) reschedule: (GGTimer *) timer
	    forDate: (double) newtime;
- (void) run;
@end

@interface MessageManager : NSObject <NSCoding>
{
}
+ (void) addObserver: obs
	    selector: (SEL) s
		name: (NSString *) name
	      object: obj;
+ (void) removeObserver: obs
		   name: (NSString *)s
		 object: obj;
+ (void) removeObserver: obs;
+ (NSString *) description;
+ (BOOL) isObserverOrWatched: obj;
@end


typedef struct __lstNamed
{
  id			obs;
  NSString 		*name;
  SEL			sel;
  id			obj;
  struct __lstNamed	*next;
}ListNamed;

extern ListNamed *obsTable;

#endif
