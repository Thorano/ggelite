/*
 *  GGInput.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#import <stdio.h>
#import <stdlib.h>

#import <Foundation/NSException.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSDebug.h>
#import "GGInput.h"
#import "GGConcreteShip.h"
#import "GGLoop.h"
#import "GGRepere.h"
#import "World.h"
#import "Tableau.h"
#import "Metaobj.h"
#import "utile.h"
#import "SoundSDL.h"
#import "Commandant.h"
#import "Preference.h"
#import "GGGlyphLayout.h"
#import "GGFont.h"
#import "GGSky.h"
#import "ShipWin.h"
#import "StarView.h"
#import "GalaxyView.h"
#import "TopView.h"
#import "Console.h"
#import "SystemView.h"
#import "GGMenu.h"
#import "GGWidget.h"
#import "Sched.h"
#import "Station.h"
#import "Resource.h"
#import "InfoView.h"
#import "FloatingView.h"
#import "GGShell.h"
#import "GGEventDispatcher.h"
//#import "farouk.h"
#import "GGEliteBridge.h"
#import "Planete.h"
#import "GGLog.h"

#import "GGSmallPlanete.h"


#if USE_SDL
#import "GGLoopSDLExtension.h"
#endif

#import <math.h>
#import <time.h>

GGInput *theGGInput;
@class GGBlock;

#import "GGImage.h"
@interface ImageViewer : FloatingView
{
    GGString*           _name;
    GGImage*            _image;
}
@end

@implementation ImageViewer
- (id) initWithFrame: (NSRect) aRect
{
    self = [super initWithFrame:aRect];
    if(self){
        _name = [[GGString alloc] initWithFrame:NSMakeRect(10,aRect.size.height-30
                                                           ,200,30)];
        [self addSubview:_name];
        
    }
    return self;
}

- (void) setImageName:(NSString*) name
{
    GGImage* newImage = [GGImage imageWithName:name];
    ASSIGN(_image, newImage);
    [_name setString:name];
}

- (void) drawRect: (NSRect) theRect
{
//    NSRect bounds = [self bounds];
    
//    [_image compositeToRect:bounds];
//    [_image drawPixels];
}

- (void) dealloc
{
    RELEASE(_name);
    RELEASE(_image);
    [super dealloc];
}
@end

@interface GGInput (Private)
/** Should be call when all the GUI views have to be closed.  It will call
[popAllView]
*/
- (void) closeAuxiliaryViews;
@end


@interface NextDestView : FloatingView
{
    GGTimer *timer;
}
+ nextDestViewWith: (NSArray *) array;
@end

@implementation NextDestView
static BOOL NextDestViewOpened;

- initWith: (NSArray *) array
{
    int n;
    
    if( !NextDestViewOpened && array != nil && (n = [array count]) > 0 )
    {
        GGSpaceObject *objs[n];
        GGMenu *menu;
        int i;
        
        [self initWithFrame: NSMakeRect(200, 100, 300, 450)];
        [self setTransparency: 0.3];
        [self enableTextureBackground];
        [self setTitle: @"Choose your destination\n't' select the current target"];
        [array getObjects: objs];
        menu = [GGMenu menu];
        [menu setFrameOrigin: NSMakePoint(10, 10)];
        
        for(i = 0; i< n; ++i)
        {
            [menu addItemWithTitle: [objs[i] description]
                            target: self
                            action: @selector(setDestination:)
                               tag: objs[i]];
        }
        [menu endItem];
        [self addSubview: menu];
        
        NextDestViewOpened = YES;
        timer = [GGTimer scheduledTimerWithTimeInterval: 5
                                                 target: self
                                               selector: @selector(closeFloatingView)
                                                repeats: NO];
        [[NSNotificationCenter defaultCenter]
	addObserver: self
       selector: @selector(closeFloatingView)
           name: GGNotificationLeavingSystem
         object: nil];
        RETAIN(timer);
        
        [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"DestinationViewKeys"]];
    }
    else
    {
        NSDebugMLLog(@"NextDestView", @"cannot open destview");
        RELEASE(self);
        return nil;
    }
    
    return self;
}

- (void) closeFloatingView
{
    NextDestViewOpened = NO;
    [super closeFloatingView];
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] 
    removeObserver: self];
    [timer invalidate];
    RELEASE(timer);
    [super dealloc];
}

- (void) setDestination: (GGBouton *)bo
{
    GGSpaceObject *obj = (GGSpaceObject *)[bo tag];
    [[theWorld commandant] autoPilotTo: obj];
    [self closeFloatingView];
}

- (void) setDestToTarget
{
    Commandant *c;
    c = [theWorld commandant];
    [c autoPilotToCurrentTarget];
    NSWarnMLog(@"puppu");
    [self closeFloatingView];
}

+ nextDestViewWith: (NSArray *) array
{
    return AUTORELEASE([[self alloc] initWith: array]);
}
@end


@implementation GGInput
- (void) buildIcons
{
    GGIconAction icons[4] = {
    {self, @selector(command_FN_F1), @"gmap0.jpg"},
    {self, @selector(command_FN_F2), @"ship0.jpg"},
    {self, @selector(command_FN_F3), @"cmdr0.jpg"},
    {self, @selector(command_FN_F4), @"yard0.jpg"}
    };
    
    [view buildIconBar: icons
                    at: 0
                  size: sizeof(icons) / sizeof(GGIconAction)];
    
}

- (ViewTableau *) tableau
{
    return view;
}


- init
{
    //  [super initWithFrame: NSMakeRect(0, 0.1*HEIGHT, WIDTH, HEIGHT*0.9)];
    [super initWithFrame: NSMakeRect(0, 0, WIDTH, HEIGHT)];
    ship = nil;
    trackMode = 0;
    tableauDeBord = [GGTableau new];
    [tableauDeBord setFrameRect: frameWindow];
    
    view = [ViewTableau new];
    [[self contentView] addSubview: view];
    
//    ImageViewer *viewer = [[ImageViewer alloc] initWithFrame:NSMakeRect(50,50,600,400)];
//    [[self contentView] addSubview:viewer];
//    [viewer setImageName:@"earth.jpg"];
////        [viewer setImageName:@"cmdr0.jpg"];
//    [viewer release];

    /*    GGShell* shell = [[GGShell alloc] initWithFrame:NSMakeRect(50,100,600,400)];
    [[self contentView] addSubview:shell];
    [shell release]; */
    
    theConsole = RETAIN([view console]);
    
    RELEASE(view);
    
    //  guiState = GUI_SpaceView;
    
    addToCheck(self);
    
    theGGInput = self;
    
    [[NSNotificationCenter defaultCenter]
    addObserver: self
       selector: @selector(loadingAGame:)
           name: GGNotificationBeforeLoadingGame
         object: nil];
    
    [self setDrawBackground: NO];
    [self buildIcons];
    _logger = [[GGLog alloc] init];
    [GGLog setLogger:_logger];
    
    _shipDispatcher = [[GGEventDispatcher dispatcherWithName:@"NavigationKeys"] retain];
    NSParameterAssert(_shipDispatcher);

    _globalDispatcher = [[GGEventDispatcher dispatcherWithName:@"GlobalKeys"] retain];
    NSParameterAssert(_globalDispatcher);
    
#ifdef DEVEL
    _wizardDispatcher = [[GGEventDispatcher dispatcherWithName:@"WizardKeys"] retain];
    NSParameterAssert(_wizardDispatcher);
#endif
    return self;
}

//  - (void) opening
//  {
//    WinTableau *tab;
//    tab = [WinTableau window];
//    [tab open];
//  }

- (void) dealloc
{
    RELEASE(topviewManager);
    RELEASE(_logger);
    RELEASE(tableauDeBord);
    RELEASE(ship);
    RELEASE(theConsole);
    RELEASE(externCamera);
    
    RELEASE(_shipDispatcher);
    RELEASE(_globalDispatcher);
#ifdef DEVEL
    RELEASE(_wizardDispatcher);
    RELEASE(_bridge);
#endif
    theGGInput = nil;
    [super dealloc];
}

- (void) loadingAGame: obj
{
    station = nil;
    [self closeAuxiliaryViews];
}

- (Console *)console
{
    return theConsole;
}

- (void) setInStation: (Station *) s
{
    station = s;
}

- (Station *) currentStation
{
    return station;
}

- (void) printChose
{
    glViewport(frameWindow.origin.x, frameWindow.origin.y, 
               frameWindow.size.width, frameWindow.size.height);
    glDepthMask(GL_FALSE);
    glDisable(GL_ALPHA_TEST);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_FOG);
    glDisable(GL_LIGHTING);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,frameWindow.size.width,0,frameWindow.size.height,-1.0,1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glPushMatrix();
    glTranslatef(200, 200, 0);
    
    glColor3f(1, 1, 1);
    //  [[GGFont fontWithFile: @"cu12.pcf"] bleuargh];
    //   [[GGFont defaultFont] drawString: @"Salut"
    // 			atOrigin: NSMakePoint(0, 0)];
    glPopMatrix();
    
    [tableauDeBord drawTableau];
}

- (void) setFrameWindow: (NSRect) aRect
{
    NSDebugMLLog(@"Resize", @"new : %g %g", aRect.size.width, 
                 aRect.size.height);
    [super setFrameWindow: aRect];
    [tableauDeBord setFrameRect: aRect];
    [theWorld updateProjection];
}

- (void) manageCameraMovement
{
    if( cameraMove )
    {
        if( cameraMove & GGRight )
            addLambdaVect(Direction(externCamera), 0.8*realDeltaTime, 
                          Gauche(externCamera),
                          Direction(externCamera));
        if( cameraMove & GGLeft )
            addLambdaVect(Direction(externCamera), -0.8*realDeltaTime, 
                          Gauche(externCamera),
                          Direction(externCamera));
        if( cameraMove & GGDown )
            addLambdaVect(Direction(externCamera), 0.8*realDeltaTime, 
                          Haut(externCamera),
                          Direction(externCamera));
        if( cameraMove & GGUp )
            addLambdaVect(Direction(externCamera), -0.8*realDeltaTime, 
                          Haut(externCamera),
                          Direction(externCamera));
        
        if( cameraMove & GGAccelere )
            distanceCamera *= exp(-realDeltaTime);
        if( cameraMove & GGDecelere )
            distanceCamera *= exp(realDeltaTime);
        
        if( distanceCamera < 2*Dimension(ship) )
            distanceCamera = 2*Dimension(ship);
        
        normaliseMatrice(Matrice(externCamera));
        mulScalVect(Direction(externCamera), -distanceCamera, 
                    Point(externCamera));
    }
}

- (void) drawLog
{
    [self initWindowGL];
    [_logger logAndCleanAtPoint:NSMakePoint(250,frameWindow.size.height-20)];
}

- (void) drawWindow
{
    GLCHECK;
    //  NSDebugMLLogWindow(@"GGInput", @"val %d", guiState);
    if( externCamera ){
        [self manageCameraMovement];
        GLCHECK;
    }
    if( (windowAbove != nil && [windowAbove opacity] >= 1)
        || _currentView != nil)
    {
        //        glViewport(0, 0, WIDTH, HEIGHT);
        //        glClearColor(0, 0, 0, 0);
        //        glDisable(GL_SCISSOR_TEST);
        //        glClear(GL_COLOR_BUFFER_BIT);
    }
    else
    {
        if( station )
        {
            //FIXME :
            static NSString *theStr;
            if( theStr == nil )
            {
#define fbeg 0x670
#define fend 0x6b7
                unichar data2 [fend-fbeg];
                int i;
                NSString *str;
                for( i = 0; i < sizeof(data2) / sizeof(unichar); ++i )
                    data2[i] = fbeg+i;
                
                str = [NSString  stringWithCharacters: data2
                                               length: sizeof(data2)/sizeof(unichar)];
                
                str = [@"Hello\n" stringByAppendingString: str];
                RETAIN(str);
                theStr = str;
            }
            GLCHECK;
            
            [self initWindowGL];
            GLCHECK;
            glClearColor(0, 0, 0, 0);
            glClear(GL_COLOR_BUFFER_BIT);
            glPushMatrix();
            glTranslatef(WIDTH/2, HEIGHT/2, 0);
            glRotatef(30, 0, 0, 1);
            glTranslatef(-150, 0, 0);
            glScalef(2, 2, 2);
            glColor3f(1, 0, 0);
            [[GGFont fixedFont]
                //              [[GGFont unicodeFont]
                drawString: theStr
                atOrigin: NSZeroPoint];
	  glPopMatrix();
        GLCHECK;

	}
      else{
          [theWorld drawScene: frameWindow];
          GLCHECK;
      }
      [self printChose];
        GLCHECK;
    }
  [super drawWindow];
    GLCHECK;
}



- (void) display
{
    if( !open )
        return;
    [self drawWindow];
    [windowAbove display];
#ifdef DEBUG
    NSDebugMLLogWindow(@"Fenetres", @"%@\n%@",  topviewManager,
                       _stackView);
    
    [self drawLog];
#endif
}


- (void) setShip: (Commandant *) unShip
{
    ASSIGN(ship, unShip);
    [unShip enregistre: self	
                  with: @selector(removeCommandant:)];
}

- (void) removeCommandant: (NSNotification *) aNotification
{
    id obj = [aNotification object];
    
    if( obj == ship )
    {
        NSWarnMLog(@"Destroying commandant %p", obj);
        DESTROY(ship);
        
        //clean some stuffs:
        trackMode = 0;
        cameraMove = 0;
        DESTROY(externCamera);
    }
}


- (GGShip *) ship
{
    return ship;
}

- (GGShip *) newShipWithFile: (NSString *)fileName
                          at: (Vect3D) pos
                      target: (GGShip *) sh
{
    //   Vect3D dec = { (vrnd()-0.5)*100e3, (vrnd()-0.5)*100e3, (vrnd()-0.5)*100e3};
    Vect3D newPos;
    mulScalVect(Direction(ship),pos.x,&newPos);
    addLambdaVect(&newPos,pos.z,Haut(ship),&newPos);
    addLambdaVect(&newPos,pos.y,Gauche(ship),&newPos);
    addVect(Point(ship),&newPos,&newPos);
    GGShip *unShip = [GGShip shipWithFileName: fileName];
    mulScalVect(Direction(ship),100,Speed(unShip));
    [unShip setDefaultEquipment];
    [unShip setPosition: &newPos];
    [[theWorld repere] addSubNode: unShip];
    [theWorld addShip: unShip];
//    [unShip setDefaultIA: sh];
    return unShip;
}

- (void) newShip
{
    //   int blu = 2;
    //   NSArray *ships = [theResourceManager ships];
    //   NSEnumerator *en;
    //   NSString *name;
    GGShip *sh;
    Vect3D pos = {2000, 0, 0};
    //   en = [ships objectEnumerator];
    
//    sh = [self newShipWithFile: @"asp2.shp"
        sh = [self newShipWithFile: @"vaisseau.shp"
                            at: pos
                        target: nil  ];
    pos.y = 100000;
    //   [self newShipWithFile: @"asp2.shp"
    // 	at: pos
    // 	target: sh];
    
    //   while( (name = [en nextObject]) != nil && blu)
    //     {
    //       [self newShipWithFile: name
    // 	    at: pos];
    //       pos.y += 1000;
    //       blu --;
    //     }
    
}

- (void) changeView
{
    if( externCamera == nil )
    {
        GGShip* targetCamera;
#if DEBUG
        if(wizard && [theCommandant target] != nil){
            targetCamera = [theCommandant target];
        }
        else{
#endif
            targetCamera = ship;
#if DEBUG
        }
#endif
        distanceCamera = 3*Dimension(targetCamera);
        externCamera = [GGSpaceObject new];
        [targetCamera addSubNode: externCamera];
        //      [externCamera setNodePere: (GGNode*)targetCamera];
        Point(externCamera)->z = -distanceCamera;
        [theWorld setMainCamera: externCamera];
    }
    else
    {
        [theWorld setMainCamera: [theWorld commandant]];
        [externCamera destroy];
        DESTROY(externCamera);
    }
}

#pragma mark Camera commands
- (void) goLeft
{
    if(!externCamera ) return;
    
    cameraMove |= GGLeft;
}

- (void) stopGoLeft
{
    if(!externCamera ) return;
    
    cameraMove &= ~GGLeft;
}

- (void) goRight
{
    if(!externCamera ) return;
    
    cameraMove |= GGRight;
}

- (void) stopGoRight
{
    if(!externCamera ) return;
    
    cameraMove &= ~GGRight;
}

- (void) goUp
{
    if(!externCamera ) return;
    
    cameraMove |= GGUp;
}

- (void) stopGoUp
{
    cameraMove &= ~GGUp;
}

- (void) goDown
{
    if(!externCamera ) return;
    
    cameraMove |= GGDown;
}

- (void) stopGoDown
{
    if(!externCamera ) return;
    
    cameraMove &= ~GGDown;
}

- (void) zoomIn
{
    if(!externCamera ) return;
    
    cameraMove |= GGAccelere;
}

- (void) stopZoomIn
{
    if(!externCamera ) return;
    
    cameraMove &= ~GGAccelere;
}

- (void) zoomOut
{
    if(!externCamera ) return;
    
    cameraMove |= GGDecelere;
}

- (void) stopZoomOut
{
    if(!externCamera ) return;
    
    cameraMove &= ~GGDecelere;
}

- (void) command_FN_F1
{
    if( !topviewManager )
        [self changeView];
    else // if( guiState != GUI_StationView )
    {
        [self closeAuxiliaryViews];
    }
}

- (void) closeAuxiliaryViews
{
    [topviewManager invalidate];
    DESTROY(topviewManager);
    [self popAllView];
}

- (void) _setManager: (TopViewManagerCommon *)m
{
    ASSIGN(topviewManager, m);
    [[NSNotificationCenter defaultCenter]
    addObserver: self
       selector: @selector(removeManger:)
           name: @"GGViewManagerInvalidated"
         object: m];
}

- (void) removeManger: (NSNotification *) aNot
{
    id obj = [aNot object];
    
    if ( obj == topviewManager )
    {
        NSDebugMLLog(@"Fenetres", @"Recu not sur %@, je le vire", obj);
        DESTROY(topviewManager);
        [self popAllView];
    }
    else
        NSWarnMLog(@"Strange, receive not from %@", obj);
}

- (void) keyDownIndex:(int) n
{
    NSParameterAssert(1 <= n && n <= 4);
    
    if( topviewManager && [topviewManager tag] == n )
    {
        [topviewManager openNextView];
    }
    else
        switch(n)
        {
            case 1:
            case 4:
                ggabort();
                break;
            case 3:
            {
                TopViewManager *new;
                NSArray *array;
                [self closeAuxiliaryViews];
                array = [NSArray arrayWithObjects:
                    [StarView class],
                    [SystemView class],
                    [GalaxyView class], nil];
                new = [TopViewManager alloc];
                [new initWithTag: 3
                         classes: array
                          window: self];
                [self _setManager: new];
                RELEASE(new);
                
                [topviewManager openFirstView];
                break;
            }
            case 2:
            {
                TopViewManagerCommon *new;
                
                [self closeAuxiliaryViews];
                
                new = [InfoManager new];
                
                [self _setManager: new];
                RELEASE(new);
                
                [topviewManager openFirstView];
                break;
            }
            default:
                ggabort();
                break;
        }
}

- (void) FDownWithNumber: (NSNumber*) arg
{
    [self keyDownIndex:[arg intValue]];
}

- (void) command_FN_F2
{
    [self keyDownIndex: 2];
}

- (void)	command_FN_F3
{
    [self keyDownIndex: 3];
}

- (void) 	command_FN_F4
{
    if( station )
    {
        [self closeAuxiliaryViews];
        [station openView];
    }
}

- (BOOL) acceptShipCommand
{
    return  ship && !topviewManager && !station;
}

#ifdef DEVEL
#pragma mark wizard command
- (void) loadDynamicScneneAtPath:(NSString*)path
{
    if(nil == _bridge){
        NSLog(@"prout");
        _bridge = [GGEliteBridge new];
        Vect3D pos = {
            100, 0, 0,
        };
        [_bridge setPosition:&pos];
        [[theWorld repere] addSubNode:_bridge];
        [theWorld addShip:_bridge];
        //    [bridge enterUniverse];
    }
    
    [_bridge loadFileAtPath:path];
}

- (void) newModel:(id)sender
{
//    [self loadDynamicScneneAtPath:nil];
    GGSmallPlanete* p = [GGSmallPlanete new];
    [[theWorld repere] addSubNode: p];
    [theWorld addShip: p];
    Point(p)->z = -1000;
    RELEASE(p);
}

- (void)refuelShip:(id)sender
{
    [ship setCarburant: 100000.0];
}

- (void)blastTarget:(id)sender
{
    NSLog(@"blasting %@", [ship target]);
    [[ship target] doExplosion];
}

- (void)reloadDestination:(id)sender
{
    GGSpaceObject *o;
    o = [ship destination];
    if (o)
        [o reload];
}
#endif

#pragma mark -

- (void) setTimeSpeedWithNumber:(NSNumber*)aNumber
{
    [theLoop setTimeSpeed:[aNumber floatValue]];
}

- (void) openShipWindow
{
    ShipWindow *sw =  [ShipWindow new];
    [sw openIn: self];
    RELEASE(sw);
}

- (void) startFaroukScenar
{
    //                Complex_principal *farouk = [Complex_principal principal_];
    //                NSLog(@"farouk %@", farouk);
    NSLog(@"hahah");
}

- (void) selectNextDestination
{
    NextDestView *aView = [NextDestView nextDestViewWith: 
        [theWorld allStarPort]];
    if( aView )
    {
        [[self contentView] addSubview: aView];
        [self makeFirstResponder: aView];
    }
}

- (void) keyDown: (Event *)pev
{
    if( !ship )
    {
        [super keyDown: pev];
        return;
    }
    
    if([self acceptShipCommand]){
        id first, second;
        if(externCamera){
            first = self;
            second = ship;
        }
        else{
            first = ship;
            second = self;
        }
        
        if([_shipDispatcher dispatchKeyDownWithEvent:pev target:first] ||
           [_shipDispatcher dispatchKeyDownWithEvent:pev target:second]){
            return;
        }
    }
    
    if([_globalDispatcher dispatchKeyDownWithEvent:pev target:self]){
        return;
    }
    
#ifdef DEVEL
    if( wizard && 
        ([_wizardDispatcher dispatchKeyDownWithEvent:pev target:[ProtoPlanete defaultContextForDebugging]] ||
         [_wizardDispatcher dispatchKeyDownWithEvent:pev target:self])){
        return;
    }
#endif
}

- (void) keyUp: (Event *)pev
{
    if( ! [self acceptShipCommand] )
    {
        [super keyUp: pev];
        return;
    }
    
    id first, second;
    if(externCamera){
        first = self;
        second = ship;
    }
    else{
        first = ship;
        second = self;
    }
    
    if([_shipDispatcher dispatchKeyUpWithEvent:pev target:first] ||
       [_shipDispatcher dispatchKeyUpWithEvent:pev target:second]){
        return;
    }
    
    [super keyUp: pev];
    
}

- (void) mouseDown: (Event *) pev
{
    if( ! [self acceptShipCommand] )
        return;
    if( buttonOfEvent(pev) == GG_BUTTON_RIGHT )
    {
        trackMode = !trackMode;
        if( !trackMode )
        {
            [theTableau setJoyXPos: 0];
            [ship setXMouvement: 0];
            [theTableau setJoyYPos: 0];
            [ship setYMouvement: 0];
        }
    }
    else if (buttonOfEvent(pev) == GG_BUTTON_LEFT)
    {
        /*try to select object in screen*/
        NSPoint 		res 	= mouseLocationOfEvent(pev);
        ProjectionParam	pp;
        
        [theWorld getProjectionParam: &pp];
        
        pp.x = 0;
        pp.y = 0;
        pp.width = frameWindow.size.width;
        pp.height = frameWindow.size.height;
        
        NSDebugMLLog(@"InputSelect", @"mouse %g %g", res.x, res.y);
        if((modifiersOfEvent(pev) & GG_SHIFT_MASK) == 0){
            GGSpaceObject	*closest = nil;
            int              distCliq = 10000;
            GGSpaceObject 	*camera = [theWorld camera];
            GGSpaceObject	*enumerator = [theWorld firstObject];            
            
            while( (enumerator = [enumerator nextObject]) != nil )
            {
                Vect3D screenPos;
                NSDebugMLLog(@"InputSelect", @"obj : %@", enumerator);
                if( [enumerator windowsCoordinatesForCamera: camera
                                                  parameter: &pp
                                                     result: &screenPos] )
                {
                    int temp = fabs(screenPos.x-res.x) + fabs(screenPos.y-res.y);
                    NSDebugMLLog(@"InputSelect", @"pos %@ %g %g", enumerator,
                                 screenPos.x, screenPos.y);
                    if( temp < 20 && temp < distCliq )
                    {
                        distCliq = temp;
                        closest = enumerator;
                    }
                }
            }
            
            [ship setDestination: closest];
            //      NSLog(@"closest = %@", closest);
        }
        else {
            // shift press means we try to get really the entity under the cursor.
            // It can be part of an object.
            // This is to inspect it in the Debug inspector
            NSRect frameToSearch = NSMakeRect(res.x-5, res.y-5, 10, 10);
            id entity = [theWorld selectEntityInFrame:frameToSearch
             projectionParam:&pp camera:[theWorld camera]];
             NSLog(@"entity is %@", entity);
        }

    }
    else
        [super mouseDown: pev];
}



- (void) mouseMoved: (Event *) pev
{
    if( ! [self acceptShipCommand] )
        return;
    NSDebugMLLog(@"Move", @"souris bouge");
    if( trackMode )
    {
        NSSize size = frameWindow.size;
        NSPoint location = mouseLocationOfEvent(pev);
        float x, y;
        x = (float)(location.x) / size.width - 0.5;
        if( fabs(x) <= 0.2 )
            x = 0;
        else if( x >= 0 )
        {
            x = (x-0.2)*3;
        }
        else
            x = (x+0.2)*3;
        x = GGClamp(x);
        y = (float)(location.y) / size.height - 0.5;
        if( fabs(y) <= 0.2 )
            y = 0;
        else if( y >= 0 )
        {
            y = (y-0.2)*3;
        }
        else
            y = (y+0.2)*3;
        y = GGClamp(y);
        
        [theTableau setJoyXPos: x];
        [ship setXMouvement: x];
        [theTableau setJoyYPos: y];
        [ship setYMouvement: y];
        
        //        [ship genMove: pev->motion.xrel
        //  	    and: pev->motion.yrel];
    }
    else
        [super mouseMoved: pev];
}

- (void) clearJoystickState
{
    [ship setXMouvement: 0];
    [ship setYMouvement: 0];
    [ship setRolMouvement: 0];
}

- (void) joystickAxisChange:(Event*)pev
{
    if( ! [self acceptShipCommand] )
        return;
    double value = normalizedValue(pev);
    double equilibreValue = 2.0*value - 1.0;
    NSDebugMLLog(@"Joystick", @"joystickAxisChange %d val %g",
                 axeOfEvent(pev), value);
    
    switch(axeOfEvent(pev)){
        case 0:        
            [theTableau setJoyXPos:equilibreValue];
            [ship setXMouvement:equilibreValue];
            break;
        case 1:  // Y axis
            [theTableau setJoyYPos: equilibreValue];
            [ship setYMouvement: equilibreValue];
            break;
        case 2: // Z axis (rolling)
            [ship setRolMouvement: equilibreValue];
            break;
        case 3: // gaz
            [ship setPower: 1.0-value];
            break;
        default:
            NSDebugMLLog(@"Joystick", @"joystickAxisChange %d val %g",
                         axeOfEvent(pev), value);
            break;
    }
}

- (void) joystickButtonRelease:(Event *)theEvent
{
    if( ! [self acceptShipCommand] )
        return;
    
    if(buttonOfEvent(theEvent) == 0 )
       [ship stopFireLaser];
}

- (void) joystickButtonPress:(Event *)theEvent
{
    if( ! [self acceptShipCommand] )
        return;
    switch(buttonOfEvent(theEvent)){
        case 0:
            [ship startFireLaser];
            break;
        case 1:
            if( [ship lockMissile] )
                [ship fireMissile];
            break;
        default:
            NSDebugMLLog(@"Joystick", @"SDL_JOYBUTTONDOWN %d", 
                         buttonOfEvent(theEvent));
            break;
    }
}

- (BOOL) respondToEvent
{
    return 1;
}

- (void) addTopView: (TopView *) aView
{
    [super addTopView: aView];
    if( _currentView != nil )
        [theWorld setDrawWorld: NO];
    //  [aView open];
}

- (void) aTopViewIsClosing: (TopView *) aView
{
    [super aTopViewIsClosing: aView];
    if ( !_currentView )
        //    guiState = [_currentView state];
    {
        //      guiState = GUI_SpaceView;
        [theWorld setDrawWorld: YES];
    }
    
}	


@end
