/* 	-*-ObjC-*- */
/*
 *  GGInput.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __GGInput_h
#define __GGInput_h

#import "GGLog.h"
#import "GGShip.h"

@class GGOrbiteur;
@class Commandant;
@class GGGlyphLayout;
@class Console;
@class TopView;
@class Station;
@class ViewTableau;
@class StringArray;

#import "NSWindow.h"
#import "TopView.h"

@class GGEventDispatcher, GGEliteBridge;
@class GGLog;

@interface GGInput : GGNSWindow
{
    ViewTableau 			*view;
    Commandant			*ship;
    GGTableau        		*tableauDeBord;
    GGSpaceObject			*externCamera;
    int 				trackMode;
    int 				xold, yold, xcur, ycur;
    Console			*theConsole;
    //  GUIState			guiState;
    int				cameraMove;
    float				distanceCamera;
    Station			*station;
    GGLog			*_logger;
    TopViewManagerCommon		*topviewManager;
    GGEventDispatcher *   _shipDispatcher;
    GGEventDispatcher *   _globalDispatcher;
#ifdef DEVEL
    GGEventDispatcher *   _wizardDispatcher;
    GGEliteBridge*         _bridge;
#endif
}
- (void) clearJoystickState;
- (GGShip *) ship;
- (void) setShip: (Commandant *) unShip;
- (Console *)console;
- (ViewTableau *) tableau;
- (void) setInStation: (Station*) s;
- (Station *) currentStation;

#ifdef DEVEL
- (void) loadDynamicScneneAtPath:(NSString*)path;
#endif
@end

extern GGInput *theGGInput;



#endif
