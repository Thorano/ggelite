//
//  GGEliteController.h
//  elite
//
//  Created by Frédéric De Jaeger on 23/11/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class World, GGInput;
@interface GGEliteController : NSObject 
{
    World			*_world;
    GGInput         *_input;
}
- (void) startup;
@end
