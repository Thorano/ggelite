//
//  GGInteractive3DView.h
//  elite
//
//  Created by Frédéric De Jaeger on 23/11/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <AppKit/AppKit.h>
#import "TopView.h"
#import "GGSpaceObject.h"

@class GGNode;
@interface GGInteractive3DView : GGView <WorldManager>
{
    GLuint                          _depthTexture;
    /* stuff for frame buffer object (FBO)
     */
    GLuint                          _fb;
    GLuint                          _depth_rb;
    
    
    GGSpaceObject*                  _lightObject;
    GGNode*         _root;
    GGSpaceObject*  _camera;
    GGRepere*       _repere;
    
    float           _distance;
    unsigned        _state;
    enum __dragMode{
        DragModeRotate,         // rotate the camera around the lookat point
        DragModeMove,           // move the camera horizontally or vertically
        DragModeZoom,           // go closer/farther
    }_dragType;
    BOOL            _keepSky;
    
    BOOL            _rotateLight;
    BOOL            _firstRun;
}

- (void) centerView;
- (void) updateCameraPosition;

- (GGSpaceObject*) camera;
- (GGSpaceObject*) ligthSource;
- (GGRepere *)repere;

- (GGNode*) root;

- (void) setLookAt:(Vect3D)v;

- (BOOL)rotateLight;
- (void)setRotateLight:(BOOL)value;

@end
