//
//  GGInteractive3DView.m
//  elite
//
//  Created by Frédéric De Jaeger on 23/11/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGInteractive3DView.h"
#import "GGNode.h"
#import "NSWindow.h"
#import "GGEventDispatcher.h"
#import "utile.h"
#import "GGRepere.h"
#import "GGShip.h"
#import "Preference.h"
#import "GGShader.h"

#define DEPTH_MAP_SIZE 512
#define FBO 0

@interface GGRoot : GGNode
@end

@implementation GGInteractive3DView
- (void) _setupFBO
{
    glGenFramebuffersEXT(1, &_fb);
    glGenTextures(1, &_depthTexture);
    glGenRenderbuffersEXT(1, &_depth_rb);
    
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _fb);
    
    // initialize color texture
    glBindTexture(GL_TEXTURE_2D, _depthTexture);
    {
        // z-buffer texture
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
        
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
                        GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
        //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
        glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);
        
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, DEPTH_MAP_SIZE, DEPTH_MAP_SIZE, 0,
                     GL_RGB, GL_INT, NULL);
    }
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT,
                              GL_DEPTH_ATTACHMENT_EXT,
                              GL_TEXTURE_2D, _depthTexture, 0);
    
    // No color buffer to draw to or read from
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

- (void) _setupStandardDepthTexture
{
    glGenTextures(1,&_depthTexture);
    glBindTexture(GL_TEXTURE_2D,_depthTexture);
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    //    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_LUMINANCE);
}

- (id) initWithFrame: (NSRect)frameRect
{
    self = [super initWithFrame:frameRect];
    if(nil == self){
        return nil;
    }
    _firstRun = YES;

    [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"MovementMapKeys"]];
    
    _root = [[GGRoot alloc] init];
    [_root setManager:self];

    _repere = [[GGRepere alloc] init];
    [_root addSubNode:_repere];
    [_repere beginSchedule];

    _camera = [[GGSpaceObject alloc] init];
    [_camera setName:@"Camera"];
    [_repere addSubNode:_camera];
    
    _lightObject = [[GGSpaceObject alloc] init];
    [_lightObject setName:@"Light"];
    [_repere addSubNode:_lightObject];

    
//    [_root addSubNode:_camera];
    _keepSky = YES;
    [self centerView];
    return self;
}

- (void) dealloc
{
    [_root release];
    [_camera release];
    [_repere release];
    [_lightObject release];
    
    [super dealloc];
}

- (void) viewWillMoveToWindow: (GGNSWindow*)newWindow
{
    [super viewWillMoveToWindow:newWindow];
    if(newWindow){
        [theLoop fastSchedule: self
                 withSelector: @selector(updateView)
                          arg: nil];
    }
    else {
        NSLog(@"unscheduling");
        [[theLoop fastScheduler] deleteTarget:self withSelector:@selector(updateView)];
    }
}

- (GGSpaceObject*) ligthSource
{
    return _lightObject;
}

- (GGSpaceObject*) camera
{
    return _camera;
}

- (GGRepere *)repere
{
    NSAssert(_repere, @"not initialized");
    return _repere;
}


- (GGNode*) root
{
    return _root;
}

- (void) setLookAt:(Vect3D)v
{
    _distance = sqrt(distance2Vect(&v, Point(_camera)));
    GGMatrixLookAt(Matrice(_camera), &v);
}

- (Vect3D) lookAt
{
    Vect3D ret;
    addLambdaVect(Point(_camera),_distance,Direction(_camera),&ret);
    return ret;
}

- (BOOL)rotateLight {
    return _rotateLight;
}

- (void)setRotateLight:(BOOL)value {
    if (_rotateLight != value) {
        _rotateLight = value;
    }
}

- (void) myLockFocusInRect:(NSRect) aRect
{
    NSRect wrect;
    
    NSAssert(_window != nil, @"bad");
    
    wrect = [self convertRect: aRect toView: nil];
    [_window setGLFrameInRect: wrect];
}

- (NSRect) setupFrame:(NSSize)size param:(ProjectionParam*)ppRef
{
    NSRect bounds = [self bounds];
    bounds.size = size;
    NSRect wrect = [self convertRect: bounds toView: nil];
    [_window setGLFrameInRect:wrect];
    *ppRef = GGSetUpProjection(bounds,10,1000,Focal);
    NSRect ret = [_window convertWindowRectToFrameBufferRect:wrect];
    return ret;
}


- (void) _drawRect: (NSRect) _theRect
{
    NSRect theRect = [self bounds];
    NSRect workArea = NSMakeRect(5,5,NSWidth(theRect)-10,NSHeight(theRect)-10);
    
	GLCHECK;
    glDepthMask(GL_TRUE);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glColor3f(0.8, 0.5, 0.7);
    
    glBegin(GL_LINE_LOOP);
    glVertex2f(2,2);
    glVertex2f(theRect.size.width-2, 2);
    glVertex2f(theRect.size.width-2,theRect.size.height-2);
    glVertex2f(2,theRect.size.height-2);
    glEnd();
    
    NSDebugMLLogWindow(@"Tree", [_root displayObject: 0]);

    
    
    //glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    //  glEnable(GL_CULL_FACE);
    
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);

    if(_rotateLight){
        Vect3D tmp = mkVect(cos(2*currentTime), sin(0.5*currentTime), 2+sin(currentTime));
        mulScalVect(&tmp,1000,Point(_lightObject));
    }

    Vect3D tmpLookAt = [self lookAt];
    GGMatrixLookAt(Matrice(_lightObject),&tmpLookAt);

    
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    glDepthRange(0, 1);
    glDepthFunc(GL_LEQUAL);
    
    ProjectionParam ppForLight;
    NSRect frameBufferRect = [self setupFrame:NSMakeSize(DEPTH_MAP_SIZE,DEPTH_MAP_SIZE) param:&ppForLight];
    ppForLight.zmin = 150;
    ppForLight.zmax = 400;
    {
        glMatrixMode(GL_PROJECTION);
        GGSetupGLProjectionWithParam(&ppForLight);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    }
    glDisable(GL_LIGHTING);
    [_lightObject drawGraphProjection: &ppForLight];
    glBindTexture(GL_TEXTURE_2D,_depthTexture);
    glCopyTexImage2D(GL_TEXTURE_2D,0, GL_DEPTH_COMPONENT /*GL_RGBA*/ ,frameBufferRect.origin.x,frameBufferRect.origin.y,DEPTH_MAP_SIZE,DEPTH_MAP_SIZE,0);
    
    [self myLockFocusInRect:workArea];
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glEnable(GL_LIGHTING);
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, _depthTexture);
		//		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
	}
    {
		GGGalileen gal;
        GGMatrix4 qid;
		[_camera galileanTransformationTo:_lightObject in:&gal];
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glTranslatef (0.5, 0.5, 0.5);
        glScalef (0.5, 0.5, 0.5);
        GGMultProjectionWithParam(&ppForLight);
        GGMatrixInitModelViewNormalizer(&qid);
        glMultMatrix(&qid);
        glMultMatrix(&gal.matrice);
        glMultMatrix(&qid);   // to cancel the one that will be in the modelview.
    }
       
	ProjectionParam pp = GGSetUpProjection(workArea,10,10000,Focal);
    pp.label = NO;

     {
        glMatrixMode(GL_PROJECTION);
        GGSetupGLProjectionWithParam(&pp);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    }
    
    
	GLCHECK;
        
    GGShader* shadow = [GGShader shadowShader];
    [shadow attach];
    [_camera drawGraphProjection: &pp];   
    [shadow detach];
    {
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
    }

    glDisable(GL_LIGHTING);
    GLCHECK;
    
    [_window initWindowGL];
    [self lockFocus];
#if 1
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _depthTexture);
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    glColor3f(1,1,0);
    glBegin(GL_QUADS);
    {
        glTexCoord2f(0,0);
        glVertex2f(0,0);
        
        glTexCoord2f(1,0);
        glVertex2f(256,0);
        
        glTexCoord2f(1,1);
        glVertex2f(256,256);
        
        glTexCoord2f(0,1);
        glVertex2f(0,256);
    }
    glEnd();
    glDisable(GL_TEXTURE_2D);
#endif
    
    GLCHECK;
}

static void multCamera(GGSpaceObject* camera)
{
    Vect3D posLight = *Point(camera);
    Vect3D dirLight = *Direction(camera);
    Vect3D center;
    addVect(&posLight, &dirLight, &center);
    Vect3D skyLight = *Haut(camera);
    gluLookAt(posLight.x, posLight.y, posLight.z, center.x, center.y, center.z, skyLight.x, skyLight.y, skyLight.z);
}

- (void) _rasterizeScene:(ProjectionParam*)ppp
                fromObject:(GGSpaceObject*)camera
{
    Camera cam;
    bzero(&cam, sizeof(cam));
    cam.param = ppp;
    cam.owner = camera;
    transposeMatrice(Matrice(camera), &cam.modelView);
    
    [_repere drawWithCameraNoTransform:&cam from:nil];
}


- (void) drawRect: (NSRect) _theRect
{
    NSRect theRect = [self bounds];
    NSRect workArea = NSMakeRect(5,5,NSWidth(theRect)-10,NSHeight(theRect)-10);
    
    if(_firstRun){
        _firstRun = NO;
        GLCHECK;
#if FBO
        [self _setupFBO];
#else
        [self _setupStandardDepthTexture];
#endif
        
        GLCHECK;
    }
    
	GLCHECK;
    glDepthMask(GL_TRUE);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glColor3f(0.8, 0.5, 0.7);
    
    glBegin(GL_LINE_LOOP);
    glVertex2f(2,2);
    glVertex2f(theRect.size.width-2, 2);
    glVertex2f(theRect.size.width-2,theRect.size.height-2);
    glVertex2f(2,theRect.size.height-2);
    glEnd();
    
    NSDebugMLLogWindow(@"Tree", [_root displayObject: 0]);

    
    
    //glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);
    //  glEnable(GL_CULL_FACE);
    
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);

    if(_rotateLight){
        Vect3D tmp = mkVect(cos(2*currentTime), sin(0.5*currentTime), 2+sin(currentTime));
        mulScalVect(&tmp,1000,Point(_lightObject));
    }

    Vect3D tmpLookAt = [self lookAt];
    GGMatrixLookAt(Matrice(_lightObject),&tmpLookAt);

    
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    glDepthRange(0, 1);
    glDepthFunc(GL_LEQUAL);
    glPolygonOffset (0, 1000);  
    glEnable(GL_POLYGON_OFFSET_FILL);
    ProjectionParam ppForLight;
#if !FBO
    NSRect frameBufferRect = [self setupFrame:NSMakeSize(DEPTH_MAP_SIZE,DEPTH_MAP_SIZE) param:&ppForLight];
#endif
    ppForLight.zmin = 180;
    ppForLight.zmax = 400;
	ppForLight.shadowRaster = YES;
    ppForLight.displayBackFace = YES;  // in fact, this is not really used.
    GGMatrix4 projectionLightMatrix;
    GGMatrix4 viewLightMatrix;
    GLCHECK;
    
    glDisable(GL_LIGHTING);
    {
        glMatrixMode(GL_PROJECTION);
        GGSetupGLProjectionWithParam(&ppForLight);
        glGetFloatv(GL_PROJECTION_MATRIX, (GGReal*)&projectionLightMatrix);
    }

    GLCHECK;
    
    {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        multCamera(_lightObject);
        glGetFloatv(GL_MODELVIEW_MATRIX, (GGReal*)&viewLightMatrix);
    }

    GLCHECK;
#if FBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, _fb);
#endif
    GLCHECK;
    // we draw back facing face to reduce z fighting
    glCullFace(GL_FRONT);
    GLCHECK;
    [self _rasterizeScene:&ppForLight fromObject:_lightObject];
    glCullFace(GL_BACK);
#if FBO
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
#else
    glBindTexture(GL_TEXTURE_2D,_depthTexture);
    glCopyTexImage2D(GL_TEXTURE_2D,0, GL_DEPTH_COMPONENT /*GL_RGBA*/ ,frameBufferRect.origin.x,frameBufferRect.origin.y,DEPTH_MAP_SIZE,DEPTH_MAP_SIZE,0);
#endif
    glDisable(GL_POLYGON_OFFSET_FILL);
    
    [self myLockFocusInRect:workArea];
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glEnable(GL_LIGHTING);
    {
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, _depthTexture);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
        glActiveTexture(GL_TEXTURE0);
    }
    
       
    ProjectionParam pp = GGSetUpProjection(workArea,10,10000,Focal);
    pp.label = NO;

    {  
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        GGSetupGLProjectionWithParam(&pp);
    }
    
    GGMatrix4 cameraView; 
    {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        multCamera(_camera);
        glGetFloatv(GL_MODELVIEW_MATRIX, (GGReal*)&cameraView);
    }
    
    {
        GGMatrix4 inverseView;
        transposeMatrice(&cameraView, &inverseView);
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glTranslatef (0.5, 0.5, 0.5);
        glScalef (0.5, 0.5, 0.5);
        glMultMatrix(&projectionLightMatrix);
        glMultMatrix(&viewLightMatrix);
        glMultMatrix(&inverseView);
        glMatrixMode(GL_MODELVIEW);
    }
    
    GLCHECK;
        
    GGShadowShader* shadow = [GGShader shadowShader];
    [shadow attach];
    [shadow setEpsilon:1.0/(GLfloat)DEPTH_MAP_SIZE];
    [self _rasterizeScene:&pp fromObject:_camera];
//    [_camera drawGraphProjection: &pp];   

    [shadow detach];
    {
        glMatrixMode(GL_TEXTURE);
        glLoadIdentity();
        glMatrixMode(GL_MODELVIEW);
    }

    glDisable(GL_LIGHTING);
    GLCHECK;
    
    [_window initWindowGL];
    [self lockFocus];
#if 1
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _depthTexture);
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
    glColor3f(1,1,0);
    glBegin(GL_QUADS);
    {
        glTexCoord2f(0,0);
        glVertex2f(0,0);
        
        glTexCoord2f(1,0);
        glVertex2f(256,0);
        
        glTexCoord2f(1,1);
        glVertex2f(256,256);
        
        glTexCoord2f(0,1);
        glVertex2f(0,256);
    }
    glEnd();
    glDisable(GL_TEXTURE_2D);
#endif
    
    GLCHECK;
}
#pragma mark -
#pragma mark movement

- (void) updateCameraPosition
{
    if(_keepSky){
        Vect3D tmp = {0,0,1};
        normaliseVect(Direction(_camera));
        prodVect(&tmp, Direction(_camera), Gauche(_camera));
        normaliseVect(Gauche(_camera));
        prodVect(Direction(_camera), Gauche(_camera), Haut(_camera));
    }
    else {
        normaliseMatrice(Matrice(_camera));
//    addLambdaVect(&_lookAtPoint,-_distance,Direction(_camera),Point(_camera));
    }
//    [_repere recenterFrameOnObjectIfNeeded:_camera];
}

- (void) centerView
{
    initVect(Direction(_camera),1,0,0);
    initVect(Haut(_camera),0,0,1);
    initVect(Gauche(_camera),0,1,0);
//    _lookAtPoint = GGZeroVect;
    _distance = 50;
    initVect(Point(_camera),-50,0,0);
    _state = 0;
    
    [self updateCameraPosition];
}

- (void) goLeft
{
    _state |= GGLeft;
}

- (void) stopGoLeft
{
    _state &= ~GGLeft;
}

- (void) goRight
{
    _state |= GGRight;
}

- (void) stopGoRight
{
    _state &= ~GGRight;
}

- (void) goUp
{
    _state |= GGUp;
}

- (void) stopGoUp
{
    _state &= ~GGUp;
}

- (void) goDown
{
    _state |= GGDown;
}

- (void) stopGoDown
{
    _state &= ~GGDown;
}

- (void) goDeeper
{
    _state |= GGStrafLeft;
}

- (void) stopGoDeeper
{
    _state &= ~GGStrafLeft;
}

- (void) goHigher
{
    _state |= GGStrafRight;
}

- (void) stopGoHigher
{
    _state &= ~GGStrafRight;
}

- (void) zoomIn
{
    _state |= GGAccelere;
}

- (void) stopZoomIn
{
    _state &= ~GGAccelere;
}

- (void) zoomOut
{
    _state |= GGDecelere;
}

- (void) stopZoomOut
{
    _state &= ~GGDecelere;
}

#define SpeedMotion 10

- (Vect3D) _lookAt
{
    Vect3D lookAt;
    addLambdaVect(Point(_camera),_distance,Direction(_camera),&lookAt);
    return lookAt;
}

- (void) _setCameraFromLookAt:(Vect3D)lookAt
{
    addLambdaVect(&lookAt,-_distance,Direction(_camera),Point(_camera));
}

- (void) updateView
{
    Vect3D decal = GGZeroVect;
    if( _state )
    {
        Vect3D lookAt = [self _lookAt];
        float fact = 1.0;

        if( _state & GGLeft )
        {
            decal.x += SpeedMotion*realDeltaTime;
        }
        if( _state & GGRight )
        {
            decal.x -= SpeedMotion*realDeltaTime;
        }
        if( _state & GGDown )
        {
            decal.y -= SpeedMotion*realDeltaTime;
        }
        if( _state & GGUp )
        {
            decal.y += SpeedMotion*realDeltaTime;
        }
        if( _state & GGStrafLeft )
        {
            decal.z -= SpeedMotion*realDeltaTime;
        }
        if (_state & GGStrafRight )
        {
            decal.z += SpeedMotion*realDeltaTime;
        }
        if( _state & GGAccelere && _distance > 50 )
            _distance /= exp(realDeltaTime);
        if( _state & GGDecelere )
            _distance *= exp(realDeltaTime);
        mulScalVect(&decal,fact*_distance*0.1,&decal);
        Vect3D transformDecal;
        produitMatriceVecteur3D(Matrice(_camera),&decal,&transformDecal);
        addVect(&lookAt,&transformDecal,&lookAt);
        [self updateCameraPosition];
        [self _setCameraFromLookAt:lookAt];
    }
}

- (void) mouseDown: (Event *) pev
{
//    NSLog(@"button = %d", buttonOfEvent(pev));
    switch(buttonOfEvent(pev)){
        case 0:
            _dragType = DragModeMove;
            break;
        case 1:
            _dragType = DragModeRotate;
            break;        
        case 2:
            _dragType = DragModeZoom;
            break;
        default:
            _dragType = DragModeZoom;
            NSLog(@"unexpected type: %d", buttonOfEvent(pev));
    }
    if(DragModeMove == _dragType){
        unsigned modifiers = modifiersOfEvent(pev);
        if(modifiers & GG_COMMAND_MASK){
            _dragType = DragModeRotate;
        }
        if(modifiers & GG_ALT_MASK){
            _dragType = DragModeZoom;
        }
    }
}

- (void) mouseUp: (Event *)pev
{
}

- (void) scrollWheel: (Event*)pev
{
    float fact = 1.0;
    if(modifiersOfEvent(pev) & GG_SHIFT_MASK){
        fact = 10.0;
    }
    Vect3D lookAt = [self _lookAt];
    _distance *= exp(-realDeltaTime*fact*deltaYOfEvent(pev));       
    [self _setCameraFromLookAt:lookAt];
}

- (void) mouseMoved: (Event *) pev
{
    float fact = 1.0;
    if(modifiersOfEvent(pev) & GG_SHIFT_MASK){
        fact = 10.0;
    }
    if( isADragMove(pev) )
    {
        NSPoint delta = mouseMotionOfEvent(pev);
        int dx = delta.x;
        int dy = delta.y;
        Vect3D lookAt = [self _lookAt];
        if(DragModeRotate == _dragType){
            addLambdaVect(Direction(_camera), -0.005*dx*fact, Gauche(_camera),
                          Direction(_camera));
            addLambdaVect(Direction(_camera), -0.005*dy*fact, Haut(_camera),
                          Direction(_camera));
        }
        else if (DragModeMove == _dragType) {
            addLambdaVect(&lookAt, 0.01 * _distance*dy * fact,Haut(_camera),&lookAt);
            addLambdaVect(&lookAt,0.01 * _distance*dx * fact,Gauche(_camera),&lookAt);
        }
        else  {
            NSAssert(DragModeZoom == _dragType, @"unexpected drag type");
            _distance *= exp(realDeltaTime*fact*dy);        
        }
        [self updateCameraPosition];
        [self _setCameraFromLookAt:lookAt];
    }
}
@end

@implementation GGRoot
- (id) init
{
    self = [super init];
    if(self){
    }
    
    return self;
}

- (void) willDrawSons: (Camera *)cam
{
    [super willDrawSons:cam];
    
//    glDepthMask(GL_TRUE);
//    glEnable(GL_DEPTH_TEST);
//    glDepthRange(0.95, 0.98);
//    glDepthFunc(GL_LESS);
//    
//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    gluPerspective(cam->param->fovy, cam->param->ratio,1,1e20);
//    glMatrixMode(GL_MODELVIEW);

    {
        Vect4D lightPos;
        lightPos.w = 1.0;
        
        GGInteractive3DView* view = (GGInteractive3DView*)[self manager];
        GGSpaceObject* light = [view ligthSource];
        lightPos.v = *Point(light);
        ggLightPosition(GL_LIGHT0, lightPos);
    }
}
@end
