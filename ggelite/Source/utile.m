/*
 *  utile.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#import <math.h>
#import <sys/time.h>
#import <unistd.h>
#import <stdlib.h>
#import <stdio.h>

#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSString.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSDate.h>
#import <Foundation/NSCalendarDate.h>

#import "config.h"
#import "utile.h"
#import "GGSpaceObject.h"
#import "Preference.h"
#import "GGRepere.h"
#import "Tableau.h"
#import "GGSky.h"
#import "GGGlyphLayout.h"
#import "GGShell.h"



void displayOnGGEliteConsole(NSString* format, ...)
{
    va_list ap;
    
    va_start (ap, format);
    
    NSString*str = [[NSString alloc] initWithFormat: format
                                          arguments: ap];
    
    [[GGShell defaultShell] display:str];
    [str release];
    va_end (ap);
}

GGReal distanceAbsolu2(GGSpaceObject *o1, GGSpaceObject *o2)
{
    Vect3D v;
    GGReal temp;
    [o1 positionInRepere: o2
                      in: &v];
    temp = prodScal(&v, &v);
    
    NSDebugLLogWindow(@"distance", @"%@ %@ %g", o1, o2, temp);
    
    return temp;
}

NSString *stringFromGameDate(double val)
{
    NSDate *theDate;
    NSCalendarDate *cal;
    
    theDate = [NSDate dateWithTimeIntervalSinceReferenceDate: val +
        (365.25*24*3600*1500)];
    cal = [theDate dateWithCalendarFormat: @"%A %B %d %H:%M:%S %Y UT"
                                 timeZone: nil];
    
    return [cal description];
}


void logArray(NSArray *array)
{
    int n = [array count];
    NSString *str[n];
    int i;
    [array getObjects: str];
    
    for(i = 0; i < n; ++i)
        NSLog(@"%@", str[i]);
}

void drawRectBorder(short int x1, short int y1, short int x2, short int y2)
{
    glBegin(GL_LINE_LOOP);
    glVertex2f(x1, y1);
    glVertex2f(x1, y2);
    glVertex2f(x2, y2);
    glVertex2f(x2, y1);
    //  glVertex2f(x1, y1);
    glEnd();
}

void GGMultProjectionWithParam(ProjectionParam *param)
{
    gluPerspective(param->fovy,param->ratio,param->zmin,param->zmax);    
}

void GGSetupGLProjectionWithParam(ProjectionParam *param)
{
    glLoadIdentity();
    GGMultProjectionWithParam(param);
}


double ggmod(double x, double y)
{
    double res = fmod(x, y);
    
    if( res < 0 )
        res += y;
    return res;
}

void pushStandardRep(NSSize size)
{
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,size.width,0,size.height,-1.0,1.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
}

void popStandardRep()
{
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}

static inline int indexForVect(Vect3D *v)
{
    int res = 0;
    if( v->x < 0 )
        res |= 0x4;
    if( v->y < 0 )
        res |= 0x2;
    if( v->z < 0 )
        res |= 0x1;
    return res;
}


void makeFrustum(Frustum *f, Camera *pcam)
{
    GGPlan devant, gauche, droite, haut, bas;
    GGMatrix4 *mat = &pcam->modelView;
    
    
#define Cam (*pcam)
    
    
    /* Compute frustum :
        */
    
    droite = mkPlan(1, 0, Cam.param->xFoc, 0);
    gauche = mkPlan(-1, 0, Cam.param->xFoc, 0);
    haut = mkPlan(0, -1, Cam.param->yFoc, 0);
    bas = mkPlan(0, 1, Cam.param->yFoc, 0);
    devant = mkPlan(0, 0, 1, Cam.param->zmin);
    
    transformePlan(mat, &droite, &f->droite);
    transformePlan(mat, &gauche, &f->gauche);
    transformePlan(mat, &haut, &f->haut);
    transformePlan(mat, &bas, &f->bas);
    transformePlan(mat, &devant, &f->devant);
    
    f->idro = indexForVect(&f->droite.v);
    f->igau = indexForVect(&f->gauche.v);
    f->ihau = indexForVect(&f->haut.v);
    f->ibas = indexForVect(&f->bas.v);
    f->idev = indexForVect(&f->devant.v);
    
}


static int count;

static void outPutPixel(unsigned char r, unsigned char g, 
                        unsigned char b, FILE *f)
{
    fprintf(f, "%d %d %d", (int)r, (int)g, (int)b);
    
    if( ++count > 4 )
    {
        count = 0;
        fprintf(f, "\n");
    }
    else
        fprintf(f, " ");
}


void writeRGBTofile(int width, int height, const unsigned char *data)
{
    int i, j;
    static int indFile = 1;
    NSString *fileName = [NSString stringWithFormat: @"img%d.raw", indFile];
    FILE *outfile;
    
    indFile++;
    count = 0;
    
    if ((outfile = fopen([fileName fileSystemRepresentation], "w")) == NULL) {
        NSLog(@"can't open %@", fileName);
        exit(1);
    }
    
    fprintf(outfile, "P3\n");
    fprintf(outfile, "# CREATOR: XV Version 3.10a  Rev: 12/29/94 (PNG patch 1.2)\n");
    fprintf(outfile, "# CREATOR: XV Version 3.10a  Rev: 12/29/94  Quality = 75, Smoothing = 0\n");
    fprintf(outfile, "%d %d\n255\n", width, height);
    
    
    for( i = 0; i < height; ++i)
        for( j = 0; j < width; ++j)
        {
            int index = 3*(width*(height -1 - i)+j);
            outPutPixel(data[index], data[index+1], data[index+2], outfile);
        }
            
            fclose(outfile);
}




BOOL getWinCoordinates(const Camera *c, const Vect3D *vec, Vect3D *res)
{
    Vect3D pos;
    GGReal	x, y;
    transformePoint4D(&c->modelView, vec, &pos);
    if( pos.z > EPS )
    {
        // swap the x-axis because I presume the orientation is wrong
        x = -pos.x / pos.z / c->param->xFoc;
        y = pos.y / pos.z / c->param->yFoc;
        
        if( x >= -1 && x <= 1 && y >= -1 && y <= 1 )
        {
            res->x = (x+1) * c->param->width/2;
            res->y = (y+1) * c->param->height/2;
            res->z = (pos.z - c->param->zmin) / (c->param->zmax-c->param->zmin);
            return YES;
        }
    }
    return NO;
}

static NSRect boxFrameForDistance(GGBox box, GGReal val, ProjectionParam* param)
{
    float xmin = box.min.x / val / param->xFoc;
    float xmax = box.max.x / val / param->xFoc;
    float ymin = box.min.y / val / param->yFoc;
    float ymax = box.max.y / val / param->yFoc;
    return NSMakeRect(xmin, ymin, xmax-xmin, ymax-ymin);
}

// box is expressed in the camera frame coordinated.
// the method compute a bounding box in the screen coordinated using ProjectionParam
static NSRect frameForBoxWithProjection(ProjectionParam* param, GGBox box)
{
    // swap coordinated on the x axis because I presume the orientation is wrong
    GGReal tmp = box.max.x;
    box.max.x = -box.min.x;
    box.min.x = -tmp;
    NSRect aRect;   // rect in normalized screen coordinates

    if (box.min.z > EPS) {
        // if the minimun z is positive, we keep the closest side of the box and project it
        aRect = boxFrameForDistance(box, box.min.z, param);        
        NSRect r1 = boxFrameForDistance(box, box.max.z, param);
        aRect = NSUnionRect(aRect, r1);
    }
    else if (box.max.z > EPS){
        aRect = boxFrameForDistance(box, box.max.z, param);
        float xmin = NSMinX(aRect);
        float xmax = NSMaxX(aRect);
        float ymin = NSMinY(aRect);
        float ymax = NSMaxY(aRect);
        
    
        // there is one side of the box behind us and one side in front of us
        // the projection is a bit degenerate.  The resulting rect will be partially infinite
        if(xmin < 0){
            // the whole screen on the left is contain in the box.
            xmin = -1.0; //
        }
        if (xmax > 0) {
            // the whole screen on the right in contain in the box
            xmax = 1.0;
        }
        if (ymin < 0) {
            ymin = -1.0;
        }
        if (ymax > 0) {
            ymax = 1.0;
        }
        if (xmin > xmax || ymin > ymax) {
            // can happen now.  Anycase it means the rect is not in the screen
            return NSZeroRect;
        }
        aRect = NSMakeRect(xmin, ymin, xmax-xmin, ymax-ymin);
    }
    else{
        // box fully behind us, not visible at all
        return NSZeroRect;
    }
    
    return NSMakeRect((NSMinX(aRect) + 1)*param->width/2.0, (NSMinY(aRect)+1)*param->height/2.0, 
        NSWidth(aRect)*param->width/2.0, NSHeight(aRect)*param->height/2.0);
}

NSRect frameForBoxInCamera(const Camera *c, GGBox box)
{
    box = transformeBox(&c->modelView, box);
    return frameForBoxWithProjection(c->param, box);
}

NSRect frameForBallInCamera(const Camera* c, Vect3D center, GGReal radius)
{
    Vect3D newCenter;
    transformePoint4D(&c->modelView, &center, &newCenter);
    Vect3D radiusVect = mkVect(radius, radius, radius);
    GGBox box;
    diffVect(&newCenter, &radiusVect, &box.min);
    addVect(&newCenter, &radiusVect, &box.max);
    return frameForBoxWithProjection(c->param, box);
}


float absoluteMagnitude(struct __star *s)
/*distance is in 0.01 * LightYear*/
{
    double dist2 = square(s->xd)+square(s->yd) +  square(s->zd);
    if (dist2 <= 1 )
        return s->mag0;
    else
        return s->mag0+5-2.5*log10(dist2/(326*326));;
}

void draw3DstringFast(Camera *c, GGReal x, GGReal y, GGReal z, GGGlyphLayout *str)
{
    Vect3D pos = {x, y, z};
    Vect3D res;
    
    if( getWinCoordinates(c, &pos, &res) )
    {
        glLoadIdentity();
        glTranslatef(res.x, res.y, 0);
        [str fastDraw];
    }
}

GGReal omegaOrbite(GGReal radius, GGReal orbiteRadius)
{
    return 2e-7*pow((150e9/1e9)*radius/orbiteRadius, 1.5);
}

void printGLError(id str)
{
    GLenum err;
    if( str )
        NSWarnLog(@"id : %@", str);
    if( (err=glGetError() ) != GL_NO_ERROR )
    {
        NSWarnLog(@"gl Error : %s", gluErrorString(err));
        ggabort();
    }
}


#define MAGIC1 0xdeadface
#define MAGIC2 0xfacedead



typedef struct __BlockDebug
{
    int magic1;
    int count;
    int magic2;
    void (*destructor)(void *);
#ifdef DebugMem
    char *s;
#endif
}BlockDebug;

static inline void __checkHeader(BlockDebug *b)
{
    NSCParameterAssert(b->magic1 == MAGIC1 && b->magic2 == MAGIC2);
}

#ifdef DebugMem
#define MaxCache 3000

void setName(void *ptr, char *name)
{
    BlockDebug *b = ((BlockDebug *)ptr) - 1;
    __checkHeader(b);
    b->s = strdup(name);
}

char *stringOfPtr(void *ptr)
{
    BlockDebug *b = ((BlockDebug *)ptr) - 1;
    __checkHeader(b);
    return b->s;
}

static void * checker[MaxCache];
void addPtr(void *ptr)
{
    int i;
    for( i = 0; i < MaxCache && checker[i] != NULL; ++i );
    
    if( i < MaxCache )
        checker[i] = ptr;
    else
        puts("too many blocks allocated");
}

void removePtr(void *ptr)
{
    int i;
    for( i = 0; i < MaxCache && checker[i] != ptr; ++i);
    
    if( i < MaxCache )
        checker[i] = NULL;
    else
        puts("pointer not found");
}

void checkMem(void)
{
    int i;
    puts("début du contrôle mémoire");
    for( i = 0; i < MaxCache; ++i )
        if( checker[i] )
        {
            fprintf(stderr, "object %p not deallocated\nstring value «%s»\n",
                    checker[i], stringOfPtr(checker[i]));
        }
            puts("fin du contrôle mémoire");
}

#undef ggalloc
void *ggalloc(int size);
void *__ggalloc(int size, char *chaine)
{
    void *ptr;
    ptr = ggalloc(size);
    setName(ptr, chaine);
    return ptr;
}

#endif

void __ggabort()
{
    int *x = NULL;
    fprintf(stderr, "crashing ...\n");
    fflush(stderr);
    *x = 42;
}


void *ggalloc(int size)
{
    BlockDebug *b;
    int newSize = size + sizeof(BlockDebug);
    b = (BlockDebug *)malloc(newSize);
    b->magic1 = MAGIC1;
    b->magic2 = MAGIC2;
    b->count = 1;
    b->destructor = NULL;
#ifdef DebugMem
    b->s = NULL;
    addPtr(&b[1]);
#endif
    return &b[1];
}

void ggretain(void *ptr)
{
    BlockDebug *b = ((BlockDebug *)ptr) - 1;
    __checkHeader(b);
    
    (b->count)++;
}

void ggrelease(void *ptr)
{
    BlockDebug *b = ((BlockDebug *)ptr) - 1;
    
    if( ptr == NULL )
        return;
    
    __checkHeader(b);
    
    //  fprintf(stderr, "ggrelease called\n");
    
    if( b->count <= 0 )
    {
        fprintf(stderr, "bock is already dead\n");
        ggabort();
    }
    
    (b->count)--;
    if( b->count == 0)
    {
        if( b->destructor != NULL )
            b->destructor(ptr);
        b->magic1 = 0;
        b->magic2 = 0;
#ifdef DebugMem
        if( b->s != NULL )
            free(b->s);
        removePtr(&b[1]);
#endif
        free(b);
    }
}

void ggSetDestructor(void *ptr, void (*f)(void *))
{
    BlockDebug *b = ((BlockDebug *)ptr) - 1;
    __checkHeader(b);
    b->destructor = f;
}


static inline VectCol moyenne(float val, VectCol c2, VectCol c1)
{
    VectCol ret;
    ret.r = val*c1.r + (1-val)*c2.r;
    ret.g = val*c1.g + (1-val)*c2.g;
    ret.b = val*c1.b + (1-val)*c2.b;
    ret.a = val*c1.a + (1-val)*c2.a;
    return ret;
}

VectCol genColFromMapHeight(const MapCol map[], int size, float param)
{
    int i;
    for( i = 0; i < size; ++i )
    {
        if( param <= map[i].val )
        {
            if( i > 0 )
            {
                float alpha;
                alpha = ((float)param - map[i-1].val) / 
                    ((float)(map[i].val - map[i-1].val));
                return moyenne(alpha, map[i-1].couleur, map[i].couleur);
            }
            else
                return map[0].couleur;
        }
    }
    return map[size-1].couleur;
}

static void plotRawBox(Vect3D pts[8])
{
    glBegin(GL_QUAD_STRIP);
    glNormal3f(-1, 0, 0);
    glVertex3v(&pts[0]);
    glVertex3v(&pts[1]);
    glVertex3v(&pts[2]);
    glVertex3v(&pts[3]);
    
    glNormal3f(0, 1, 0);
    glVertex3v(&pts[6]);
    glVertex3v(&pts[7]);
    
    glNormal3f(1, 0, 0);
    glVertex3v(&pts[4]);
    glVertex3v(&pts[5]);
    
    
    glNormal3f(0, -1, 0);
    glVertex3v(&pts[0]);
    glVertex3v(&pts[1]);
    glEnd();
    
    glBegin(GL_QUADS);
    glNormal3f(0, 0, 1);
    glVertex3v(&pts[3]);
    glVertex3v(&pts[1]);
    glVertex3v(&pts[5]);
    glVertex3v(&pts[7]);
    
    glNormal3f(0, 0, -1);
    glVertex3v(&pts[0]);
    glVertex3v(&pts[2]);
    glVertex3v(&pts[6]);
    glVertex3v(&pts[4]);
    glEnd();
}

void ggPlotBoxCol(Vect3D pts[8], VectCol col)
{
    glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_ENABLE_BIT | GL_LIGHTING_BIT |
                 GL_POLYGON_BIT | GL_CURRENT_BIT);
    glPolygonMode(GL_FRONT, GL_FILL);
    glDepthMask(GL_FALSE);
    glEnable(GL_BLEND);
    glShadeModel(GL_FLAT);
    glColor4f(col.r, col.g, col.b, 0.3);
    glDisable(GL_LIGHTING);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    plotRawBox(pts);
    
    glEnable(GL_LIGHTING);
    glCullFace(GL_BACK);
    glColor4f(col.r, col.g, col.b, 0.5);
    plotRawBox(pts);
    
    glPopAttrib();
}

void ggPlotBox(Vect3D pts[8])
{
    VectCol col = {
        0.7, 0.2, 0.2, 1.0
    };
    ggPlotBoxCol(pts, col);
}

