/* 	-*-ObjC-*- */
/*
 *  System.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: May 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __System_h
#define __System_h

#import <Foundation/NSObject.h>
#import "GGSky.h"
#import "Planete.h"

GGBEGINDEC
typedef enum __systeme_type
{
  Modern = 0,
  Farmer = 1,
  Intermediate = 2
}SystemType;

typedef enum __syteme_speciality
{
  //system type
  Agricole		= 0x1,
  Technique		= 0x2,
  HighTech		= 0x4,
  Populated		= 0x8,
  Integrist 		= 0xa,
  Mafia			= 0x10,


  Luxury 		= 0x20,
  Drugs 		= 0x40,
  Uncommom		= 0x80,
  Rare			= 0xa0,
  Illegal		= 0x100,
  VeryCommon		= 0x200
}SystemSpeciality;

#define SystemAttributeMask   ( Agricole |  Technique |  HighTech |  Populated	|  Integrist  |   Mafia)


typedef enum __force
  {
    Federation,
    Empire,
    Independant,
    None_Force
  }Force;

@class GGOrbiteur;
@class GGTimer;
@class Station;
@class  Complex_station_manager;

@interface SystemArchiver : NSObject <NSCoding>
{
  NSMutableArray	*ships;
  NSMutableDictionary	*dico;
}
+ systemArchiver;
- (NSArray *) ships;
- (void) addShip: (GGSpaceObject *)ship;
- (void) removeShip: (GGSpaceObject *)ship;
- (void) setData: obj
	 station: (Station *)s;
- dataForStation: (Station *)s;
@end

/*%%%
 type Star;
 %%%*/
@interface System : NSObject
{
  NSArray		*goodies, *equipment, *illegal;
  SystemArchiver	*archiver;
  GGOrbiteur		*mainStar;
  SystemType		systemType;
  SystemSpeciality	systemSpeciality;
  double		population; // thousand of people
  System		*theSystem;
  int			index;
  GGTimer		*aTimer;
  double		K0;
  unsigned		piracy; //mean number of pirate that will attack you

  //influence

  float			federation, independant, empire;
  Force			allegeance;
}
+ systemAt: (Star*) s
  archiver: (SystemArchiver *) _archiver;
+ systemForStarIndex: (int) index;
- (void) invalidate;
- (SystemArchiver *) archiver;

//only useful to break some cyclic initialisation problem.  See World.m
- (void) setArchiver: (SystemArchiver *)a;
- (int) index;
- (GGOrbiteur *) mainStar;
- (NSArray *) shipMaterialToSel;
- (NSArray *) goodiesToSel;
- (NSString *) systemType;
- (int) index;
- (Vect3D) randomVect;
- (void) makeCurrent;
- (void) endCurrent;
- (void) saveState;
- (NSArray *) allStations;

- (SystemSpeciality) systemSpeciality;
- (void) setGoodies: (NSArray*) goodies
	  equipment: (NSArray*) equipment
	    illegal: (NSArray*) illegal;

- (NSString *) allegeance;
- (float) federation;
- (float) empire;

// GGReal population (so 1e3 * self->population )
- (double) population;

- (void) postLoadingSystem;
- (void) afterEnteringSystem;
- (id) randomStation;
- (int) randomClosestStarIndex: (int) index;
- (Station *) stationForIndex: (int) val;
+ (NSString *) starNameForIndex: (int) val;
- (GGShip *) createStandardShip;
@end

GGENDDEC

#endif
