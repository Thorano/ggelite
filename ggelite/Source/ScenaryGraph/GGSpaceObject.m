/*
 *  GGSpaceObject.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSCoder.h>
#import "GGLoop.h"
#import "GGSpaceObject.h"
#import "GGInput.h"
#import "GGRepere.h"
#import "Metaobj.h"
#import "utile.h"
#import "Tableau.h"
#import "Preference.h"
#import "Sched.h"
#import "World.h"
#import "GGNode.h"
#import "GGFont.h"
#import "Commandant.h"

#import <math.h>
#import <time.h>
//test

/* Those notifications are posted by GGSpaceObject instances.

*  GGNotificationRemoveFromCurrent is posted when the object is about to be 
remove from the scenary graph.  It is intercepted by GGNodes.
Everyone that does geometric computation with an object should be certain
that the object is still in the scenary graph.  Thus, they should listen for
this notification.

*  GGNotificationDestruction is sent when an object like a ship is destroyed
in the game (I mean semantically).  Thus, every one that has a link to
such this object should clear it upon reception of this notification.

*  GGNotificationVanishing is similar to the preceding.  An object "vanishes" 
when it becomes useless to keep it in the game (for example, a very far away
                                                NPC).  When they disappear, they sent this notification.

The two last notification are quite similar.  Upon destruction, both are 
posted.  Upon vanishing, only the last one is.
You should listen to the last one, unless you have very good reasons.
*/

NSString *GGNotificationRemoveFromCurrent = @"GGNotificationRemoveFromCurrent";
NSString *GGNotificationDestruction = @"GGNotificationDestruction";
NSString *GGNotificationVanishing = @"GGNotificationVanishing";


@interface GGSpaceObject (Private)
- (void) _GGSpaceInvalidate;
@end


@implementation GGSpaceObject
- init
{
    [super init];
    nodePere = nil;
    _flags.isCamera = NO;
    _flags.matrixType = GeneralMatrix;
    _flags.majRepere = NO;
    _flags.visible = 0;
    _flags.graphical = NO;
    _flags.locality = 0;
    [self centerObject];
    dimension = 1;
    _flags.alive = 1;
    masse = 1;
    collisionCounter = 0;
    _flags.shouldCache = [self shouldCache];
    _flags.clipable = [self shouldBeClipped];
	_flags.castShadow = [self castShadow];
    _flags.moreThingsToDraw = [self shouldDrawMoreThings];
    _flags.cacheCorrect = NO;
    
    _flags.isFilsEmpty = YES;
    
    mode = 0;
    addToCheck(self);
    return self;
}

- (void) dealloc
{
    NSDebugMLLog(@"Alloc", @"space object %@ released!!!", self);
    RELEASE(_dynamicalHelper);
    if ( [MessageManager isObserverOrWatched: self] )
    {
        NSWarnMLog(@"some resources are still allocated for %@", self);
    }
    if (  [theLoop isScheduled: self] )
        NSWarnMLog(@"other resources are still allocated for %@", self);
    
    [self _GGSpaceInvalidate];
    
    RELEASE(filsNode);
    
    RELEASE(defaultName);
    RELEASE(cacheNameRep);
    
    [super dealloc];
}



- initAt: (GGSpaceObject *)obj
{
    [self init];
    *Galileen(self) = *Galileen(obj);
    return self;
}

+ spaceObjectAt: (GGSpaceObject *) obj
{
    GGSpaceObject *newObj;
    newObj = [self alloc];
    return AUTORELEASE([newObj initAt: obj]);
}

- (id) copyWithZone: (NSZone *)zone
{
    GGSpaceObject *copy;
    int n;
    copy = (GGSpaceObject *)NSCopyObject(self, 0, zone);
    copy->manager = nil;
    copy->nodePere = nil;
    copy->_flags.cacheCorrect = NO;
    copy->defaultName = [defaultName copy];
    copy->cacheNameRep = nil;
    copy->collisionCounter = 0;
    copy->mode = 0;
    copy->_flags.isCamera = NO;
    copy->_flags.matrixType = GeneralMatrix;
    copy->_flags.visible = 0;
    copy->_flags.alive = 1;
    copy->_flags.shouldCache = [copy shouldCache];
    copy->_flags.clipable = [copy shouldBeClipped];
    copy->filsNode = nil;
    copy->_flags.isFilsEmpty = YES;
    
    n = [filsNode count];
    if( n > 0 )
    {
        GGSpaceObject *objs[n];
        int i;
        
        [filsNode getObjects: objs];
        
        for(i=0; i<n; ++i)
        {
            objs[i] = [objs[i] copyWithZone: zone];
            [copy addSubNode: objs[i]];
            RELEASE(objs[i]);
        }
    }
    
    return copy;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
#define GGEncodeFlag(f) [encoder encodeInt:(unsigned int)(_flags.f) forKey:@#f]
    
    //  [super encodeWithCoder: encoder];
    NSDebugMLLog(@"Coding", @"%@ son of %@", self, nodePere);
    [encoder encodeObject:nodePere forKey:@"fatherNode"];
    GGEncode(manager);
    
    //  [encoder encodeConditionalObject: nodePere];
    //  [encoder encodeConditionalObject: manager];
    GGEncode(repere);
    GGEncode(omega);
    GGEncode(dimension);
    GGEncode(masse);
    GGEncode(collisionCounter);
    GGEncode(mode);
    GGEncode(defaultName);
    GGEncodeFlag(isCamera);
    GGEncodeFlag(matrixType);
    GGEncodeFlag(visible);
    GGEncodeFlag(alive);
    GGEncodeFlag(castShadow);
    GGEncodeFlag(shouldCache);
    GGEncodeFlag(clipable);
    GGEncodeFlag(majRepere);
    GGEncodeFlag(moreThingsToDraw);
    GGEncodeFlag(stateChange);
    GGEncodeFlag(locality);
    GGEncodeFlag(activated);
    GGEncodeFlag(graphical);
}

- (id)initWithCoder:(NSCoder *)decoder
{
    GGNode *pere;
#define GGDecodeFlag(f) _flags.f = [decoder decodeIntForKey:@#f]

    pere = [decoder decodeObjectForKey:@"fatherNode"];
    GGDecodeObject(manager);
    
    filsNode = nil;
    _flags.isFilsEmpty = YES;
    
    GGDecode(repere);
    GGDecode(omega);
    GGDecode(dimension);
    GGDecode(masse);
    GGDecode(collisionCounter);
    GGDecode(mode);
    GGDecode(defaultName);
    GGDecodeFlag(isCamera);
    GGDecodeFlag(matrixType);
    GGDecodeFlag(visible);
    GGDecodeFlag(alive);
    GGDecodeFlag(castShadow);
    GGDecodeFlag(shouldCache);
    GGDecodeFlag(clipable);
    GGDecodeFlag(majRepere);
    GGDecodeFlag(moreThingsToDraw);
    GGDecodeFlag(stateChange);
    GGDecodeFlag(locality);
    GGDecodeFlag(activated);
    GGDecodeFlag(graphical);
    _flags.cacheCorrect = 0;
    if( pere != nil )
    {
        NSDebugMLLog(@"Coding", @"%@ wants %@", self, pere);
        [pere addSubNode: self];
    }
    addToCheck(self);
    return self;
}


- (NSString *) signature
{
    return nil;
}

- objectFromSignature
{
    return self;
}

- (ModelBlender*) model
{
    return nil;
}

- (BOOL) shouldCache
{
    return YES;
}

- (BOOL) shouldBeClipped
{
    return YES;
}

- (BOOL) castShadow
{
	return YES;
}

- (BOOL) shouldDrawMoreThings
{
    return NO;
}

- (BOOL) shouldSave
{
    return YES;
}

- (BOOL) saveChannel
{
    return YES;
}

- (BOOL) isLocal
{
#ifdef DEBUG
    if( _flags.locality == 2 )
        NSAssert(nodePere == [theWorld repere], @"bad");
    else if (_flags.locality == 0)
        NSAssert(nodePere == nil, @"bad");
#endif
    return _flags.locality == 2;
}

- (unsigned) locality
{	
#ifdef DEBUG
    if( _flags.locality == 2 )
        NSAssert(nodePere == [theWorld repere], @"bad");
    else if (_flags.locality == 0)
        NSAssert(nodePere == nil, @"bad");
#endif
    return _flags.locality;
}

- (void) setLocality: (unsigned)val
{
    _flags.locality = val;
}

- (void) invalidCache
{
    if( _flags.cacheCorrect )
    {
        _flags.cacheCorrect = NO;
        glDeleteLists(compileListId, 1);
    }
    _flags.shouldCache = [self shouldCache];
    _flags.clipable = [self shouldBeClipped];
	_flags.castShadow = [self castShadow];
    _flags.moreThingsToDraw = [self shouldDrawMoreThings];
}

- (GGNode *) nodePere
{
    if(nodePere && ![nodePere isKindOfClass:[GGNode class]]){
        NSWarnMLog(@"grrr nodePere is not a GGNode: %@", nodePere);
        ggabort();
    }
    return (GGNode *)nodePere;
}

- (void) setNodePere: (GGSpaceObject *)obj
{
    if( obj != nil )
    {
        NSAssert( nodePere == nil, @"bad");
        NSAssert( _flags.alive, @"bad" );
        //      [self beginSchedule];
        [self setManager: [obj manager]];
    }
    else
    {
        NSAssert( nodePere != nil, @"bad");
        //      [self endSchedule];
        [self setManager: nil];
    }
    nodePere = obj;
    if ( manager )
    {
        if( [manager repere] == (id)nodePere )
            [self setLocality: 2];
        else
            [self setLocality: 1];
    }
    else
        [self setLocality: 0];
}

- (void) setManager: (id<WorldManager>) newManager
{
    manager = newManager;
    [filsNode makeObjectsPerformSelector: @selector(setManager:)
                              withObject: newManager];
}


- (id<WorldManager>) manager
{
    return manager;
}

- (GGSolid *)dynamicalHelper
{
    return _dynamicalHelper;
}

- (void)setDynamicalHelper:(GGSolid *)value
{
    ASSIGN(_dynamicalHelper, value);
}

- (GGSolid*) createDynamicalHelper
{
    return nil;
}

- (void) removeFromSuperNode
{
    //  NSAssert(nodePere != nil, @"bad");
    if( nodePere != nil )
        [nodePere removeSubNode: self];
    else
        NSWarnMLog(@"%@ has no superNode", self);
}

- (void) setDefaultName: (NSString *)aName
{
    ASSIGN(defaultName, aName);
    DESTROY(cacheNameRep);
}

- (void) setName: (NSString *)val
{
    [self setDefaultName: val];
}

- (NSString *)description
{
    NSString *tmp;
    if( defaultName != nil )
    {
#ifdef DEBUG
        if( self->_flags.activated )
            return [NSString stringWithFormat: @"@r%@@w", defaultName];
        else
#endif
            return defaultName;
    }
    else if( (tmp = [self name]) != nil )
        return tmp;
    else
        return [super description];
}

- (void) centerObject
{
    initWithIdentityTransfo(Galileen(self));
    SetZeroVect(omega);
}

- (unsigned char) matrixType
{
    return _flags.matrixType;
}

- (BOOL) isParentOf: (GGSpaceObject *)obj
{
    while(obj != nil)
    {
        if( obj == self )
            return YES;
        obj = obj->nodePere;
    }
    return NO;
}

- (void) setMode: (int) val
{
    int oldmode;
    oldmode = mode;
    mode = val;
    if( nodePere == nil )
        return;
    
    if( !oldmode && mode )
    {
        NSDebugMLLog(@"ModeInertie", @"start sched  for %@",
                     self);
        [theLoop fastSchedule: self
                 withSelector: @selector(updatePosition)
                          arg: nil];
    }
    else if( oldmode && !mode )
    {
        NSDebugMLLog(@"ModeInertie", @"STOP sched for %@",
                     self);
        [[theLoop fastScheduler]   deleteTarget: self
                                   withSelector: @selector(updatePosition)];
    }
}

- (void) setVisible: (BOOL) val
{
    _flags.visible = val;
}

- (void) setCameraFlag: (BOOL) val
{
    _flags.isCamera = val;
}

- (Vect3D *) getDirection
{
    return Direction(self);
}

- (Vect3D *) getHaut
{
    return Haut(self);
}

- (Vect3D *) getSpeed
{
    return Speed(self);
}

- (void) getGalileen:(GGGalileen *)ret
{
    *ret = *Galileen(self);
}

- (GGReal) proximite
{
    return 10*dimension;
}

- (GGSpaceObject *)ancestor
{
    if( nodePere == nil )
        return self;
    else
        return [nodePere ancestor];
}

- (void) translate: (PetitGalileen *) pg
          refering: (GGSpaceObject *) obj
             decal: (Vect3D *)v
{
    addVect(Point(self), &pg->point, Point(self));
    addVect(Speed(self), &pg->speed, Speed(self));
}


- (Vect3D) getPositionAbsolu
{
    //SPEEDUP : too slow
    GGGalileen gl;
    [self galileanTransformationTo: [self ancestor] in: &gl];
    return gl.matrice.position;
}

- (Vect3D) getPositionSpeedAbsolues: (Vect3D *)pspeed
{
    //SPEEDUP : too slow
    GGGalileen gl;
    [self galileanTransformationTo: [self ancestor] in: &gl];
    
    *pspeed = gl.speed;
    return gl.matrice.position;
}

- (void) positionInRepere: (GGSpaceObject *)_repere
                       in: (Vect3D *)pv
{
    //SPEEDUP : speed up this
    GGGalileen gal;
    [self galileanTransformationTo: _repere
                                in: &gal];
    *pv = gal.matrice.position;
}

- (void) positionSpeedRelativesTo: (GGSpaceObject *)obj
                            decal: (const Vect3D *)decal
                               in: (PetitGalileen *)pgl
{
    //SPEEDUP : speed up this
    GGGalileen gal;
    PetitGalileen pg, pgres;
    NSAssert2(nodePere != nil, @"not in the hierarchy %@ - %@", self, obj);
    [nodePere galileanTransformationTo: [obj nodePere]
                                    in: &gal];
    NSDebugMLLogWindow(@"Galileen", @"Matrice\n%@Speed : %@", 
                       stringOfMatrice(&gal.matrice), 
                       stringOfVect(&gal.speed));
    pg.speed = *Speed(self);
    if( decal )
    {
        addVect(decal, Point(self), &pg.point);
    }
    else
        pg.point = *Point(self);
    
    transformeGalileen(&gal, &pg, &pgres);
    
    diffVect(&pgres.point, Point(obj), &pgl->point);
    diffVect(&pgres.speed, Speed(obj), &pgl->speed);
}

- (void) positionSpeedInRepere: (GGSpaceObject *)_repere
                            in: (PetitGalileen *)pgl
{
    GGGalileen gal;
    [self galileanTransformationTo: _repere
                                in: &gal];
    pgl->point = gal.matrice.position;
    pgl->speed = gal.speed;
}


- (void) galileanTransformationTo: (GGSpaceObject *)obj
                               in: (GGGalileen *)pgal
{
    if( [self isParentOf: obj] )
        [obj galileanTransformationFromAncestor: self
                                             in: pgal];
    
    else
    {
        GGGalileen gal;
        NSAssert2(nodePere != nil, @"nodePere == nil, self = %@ obj = %@",
                  self, obj);
        [nodePere galileanTransformationTo: obj
                                        in: &gal];
        produitGalileen(&gal, Galileen(self), pgal);
    }
}

- (void) galileanTransformationFromAncestor: (GGSpaceObject *)ancestor
                                         in: (GGGalileen *)pgal
{
    if( ancestor == self )
        initWithIdentityTransfo(pgal);
    else
    {
        GGGalileen gal;
        NSAssert(nodePere != nil, @"nodePere == nil");
        [nodePere galileanTransformationFromAncestor: ancestor
                                                  in: &gal];
        transposeProduitGalileen(Galileen(self), &gal, pgal);
        NSDebugMLLogWindow(@"Galileen", @"%@ %@", self, nodePere);
        NSDebugMLLogWindow(@"Galileen", @"sub\n%@\nself\n%@\nresult\n%@",
                           stringOfMatrice(&gal.matrice),
                           stringOfMatrice(Matrice(self)),
                           stringOfMatrice(&pgal->matrice));
    }
}


- (void) rotationMatrixFromAncestor: (GGSpaceObject *)ancestor
                                 in: (GGMatrix4 *)res
{
    if( ancestor == self )
        initWithIdentityMatrix(res);
    else
    {
        if( _flags.matrixType == GeneralMatrix )
        {
            GGMatrix4 mat;
            [nodePere rotationMatrixFromAncestor: ancestor
                                              in: &mat];
            transposeProduit3D(Matrice(self), &mat, res);
        }
        else
            [nodePere rotationMatrixFromAncestor: ancestor
                                              in: res];
    }
}

- (void) rotationMatrixTo: (GGSpaceObject *) obj
                       in: (GGMatrix4 *)res
{
    if( [self isParentOf: obj] )
    {
        [obj rotationMatrixFromAncestor: self
                                     in: res];
    }
    else
    {
        if( _flags.matrixType == GeneralMatrix )
        {
            GGMatrix4 mat;
            NSAssert(nodePere != nil, @"nodePere == nil");
            [nodePere rotationMatrixTo: obj
                                    in: &mat];
            produitMatrice3D(&mat, Matrice(self), res);
        }
        else
            [nodePere rotationMatrixTo: obj
                                    in: res];
    }
}

- (Vect3D) rotateDirection: (Vect3D *) v
                 fromFrame: (GGSpaceObject *)o
{
    GGMatrix4 mat;
    Vect3D res;
    NSParameterAssert(o);
    [o rotationMatrixTo: self
                     in: &mat];
    produitMatriceVecteur3D(&mat, v, &res);
    return res;
}

- (GGReal) computeExtensionAlongDirection:(Vect3D)direction
                                   maxRef:(GGReal*) maxRef
{
    int count;
    GGReal min = 0, max = 0;
    if(filsNode && (count = [filsNode count]) > 0){
        GGSpaceObject* sons[count];
        [filsNode getObjects:sons];
        
        int i;
        Vect3D localDirection;
        transposeProduitVect3D(Matrice(self), &direction, &localDirection);
        for(i = 0; i < count; ++i){
            GGReal subMin, subMax;
            // iterate on the sons
            subMin = [sons[i] computeExtensionAlongDirection:localDirection maxRef:&subMax];
            
            // shift to the son's position along the direction.
            GGReal offset = prodScal(Point(sons[i]), &localDirection);
            subMin += offset;
            subMax += offset;
            
            // keep the min and max.
            if(0 == i){
                min = subMin;
                max = subMax;
            }
            else {
                if(subMin < min){
                    min = subMin;
                }
                if(subMax > max){
                    max = subMax;
                }
            }
        }
    }
    
    if(min > -dimension){
        min = -dimension;
    }
    
    if(max < dimension){
        max = dimension;
    }
    
    *maxRef = max;
    return min;
}

- (GGBox) boundingBox
{
    GGBox result = [self localBoudingBox];
    result = transformeBox(Matrice(self), result);
    return result;
}

- (GGBox) localBoudingBox
{
    GGBox result;
    result.min = mkVect(-dimension, -dimension, -dimension);
    result.max = mkVect(dimension, dimension, dimension);
    int count;
    if(filsNode && (count = [filsNode count]) > 0){
        GGSpaceObject* sons[count];
        [filsNode getObjects:sons];
        
        for(int i = 0; i < count; ++i){
            GGBox subbox = [sons[i] boundingBox];
            result = unionBox(&subbox, &result);
        }
    }
    return result;
}

- (GGReal) boudingSphereRadius
{
    GGReal radius = dimension;
    int count = [filsNode count];
    for(int i = 0; i < count; ++i){
        GGSpaceObject* son = [filsNode objectAtIndex:i];
        GGReal sonRadius = [son boudingSphereRadius];
        GGReal tmp = sonRadius + normeVect(Point(son));
        if (tmp > radius) {
            radius = tmp;
        }
    }
    return radius;
}

- (void) setPosition:(Vect3D *) pv
{
    *Point(self) = *pv;
}

- (GGMatrix4 *)getMatrix
{
    return Matrice(self);
}

- (void) _GGSpaceInvalidate
{
    [theLoop unSchedule: self];
    [MessageManager
    removeObserver: nil 
              name: nil 
            object: self];
    [MessageManager removeObserver: self];
    
    if( _flags.cacheCorrect )
    {
        _flags.cacheCorrect = NO;
        glDeleteLists(compileListId, 1);
    }
    
    [self removeAllSons];
}

- (void) invalidate
{
    [filsNode makeObjectsPerformSelector: @selector(invalidate)];
    [self _GGSpaceInvalidate];
}

- (void) reload
{
    [self invalidCache];
}

- (void) enregistre: dest
               with: (SEL) s
{
    [MessageManager addObserver: dest 
                       selector: s 
                           name: GGNotificationVanishing
                         object: self];
    NSDebugMLLog(@"Notification", @"%@ surveille %@ avec %@", dest, self,
                 NSStringFromSelector(s));
}

- (void) enregistreForRemoving: dest
                          with: (SEL) s
{
    [MessageManager addObserver: dest 
                       selector: s 
                           name: GGNotificationRemoveFromCurrent
                         object: self];
    NSDebugMLLog(@"Notification", @"%@ surveille %@ avec %@", dest, self,
                 NSStringFromSelector(s));
}


- (void) desenregistre: dest
{
    //    [[NSNotificationCenter defaultCenter] removeObserver: dest	
    //  					name:@"Destruction"	
    //  					object: self];
}

- (void) looseMainSystem
{
    [self setLocality: 0];
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationRemoveFromCurrent
                  object: self];
    [self endSchedule];
    //  [theLoop unSchedule: self];
}


- (void) reallyDestroy
{
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationRemoveFromCurrent 
                  object: self];
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationVanishing
                  object: self];
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationDestruction
                  object: self];
    NSDebugMLLog(@"Destruction", @"Destruction posted");
    
    [self invalidate];
}

- (void) destroy
{
    _flags.alive = 0;
    [theWorld addToKillPool: self
                   selector: @selector(reallyDestroy)];
    [theLoop unSchedule: self];
    
    if( ! _flags.isFilsEmpty )
    {
        NSParameterAssert(filsNode != nil && [filsNode count] > 0);
        [filsNode makeObjectsPerformSelector: @selector(destroy)];
    }
}

- (void) normalise
{
    normaliseMatrice(Matrice(self));
}

- (void) updatePosition
{
    if(_dynamicalHelper) {
        // if we have an helper, he will compute the dynamic.
        return;
    }
    
    if( mode & GGInertialMode )
    {
        addLambdaVect(Point(self), deltaTime, Speed(self), Point(self));
    }
    if( (mode & GGInertialRotationalMode) /*&& 
        prodScal(Omega(self), Omega(self)) > 1e-6*/ )
    {
        GGReal fact;
        Vect3D temp;
        
        fact = (self == theCommandant ? realDeltaTime : deltaTime);
        _flags.majRepere = YES;
        
        // Direction is moving
        mulScalVect(Gauche(self), Omega(self)->y, &temp);
        addLambdaVect(&temp, -Omega(self)->x, Haut(self), &temp);
        addLambdaVect(Direction(self), fact, &temp, Direction(self));
        
        // Haut
        
        mulScalVect(Gauche(self), -Omega(self)->z, &temp);
        addLambdaVect(&temp, Omega(self)->x, Direction(self), &temp);
        addLambdaVect(Haut(self), fact, &temp, Haut(self));
        
    }
    if( _flags.majRepere )
    {
        [self normalise];
        _flags.majRepere = 0;
    }
    if( collisionCounter > 0 )
    {
        collisionCounter -= deltaTime*1000;;
        if( collisionCounter < 0 )
            collisionCounter = 0;
    }
}

- (NSString *)name
{
    return defaultName;
}

#pragma mark -
#pragma mark drawing stuff

- (void) rasterise
{
}

- (void) rasterise: (Camera *)cam
{
    [self rasterise];
}

- (void) rasteriseMoreThings
{
    [self subclassResponsibility: _cmd];
}

static inline BOOL isClipped(GGReal fov, GGReal x, GGReal z, GGReal dimension)
{
    GGReal om2 = x*x + z*z;
    GGReal v2 = 1+fov*fov;
    GGReal fx = fabs(x);
    return fx >= fov*z && v2*om2 - square(fx*fov+z) >= v2*square(dimension);
}

- (void) drawWithCameraNoTransform: (Camera *)cam
                              from: (GGSpaceObject *)from
{
    Vect3D posInCam;
    if( !_flags.visible ) return;
    
    //   NSDebugMLLogWindow(@"Laser", @"%@ empty %s", self,
    // 		     (_flags.isFilsEmpty ? "YES" : "NO"));
    
    /*test if object is visible*/
    
    if( cam != NULL && cam->param && !cam->param->ignoreClip && _flags.clipable && _flags.isFilsEmpty )
    {
        
        //      transformePoint4D(&cam->modelView, &GGZeroVect, &posInCam);
        posInCam = cam->modelView.position;  
        if( posInCam.z < EPS || 
            isClipped(cam->param->xFoc, posInCam.x, posInCam.z, dimension) ||
            isClipped(cam->param->yFoc, posInCam.y, posInCam.z, dimension) )
        {
            NSDebugMLLogWindow(@"Clip", @"\n%@ is clipped", self);
            return;
        }
        else
            NSDebugMLLogWindow(@"Clip", @"\n%@ is not clipped", self);
    }
    
    if( _flags.graphical && (!cam->param->shadowRaster || _flags.castShadow))
    {
        
        if( _flags.shouldCache )
        {
            if( _flags.cacheCorrect )
                glCallList(compileListId);
            else
            {
                compileListId = glGenLists(1);
                NSDebugMLLog(@"gl", @"idList for %@ = %u", self, compileListId);
                glNewList(compileListId, GL_COMPILE_AND_EXECUTE);
                [self rasterise: cam];
                GLCHECK;
                glEndList();
                GLCHECK;
                
                _flags.cacheCorrect = 1;
            }
        }
        else{
            [self rasterise: cam];
            GLCHECK;
            
        }
        
        if( _flags.moreThingsToDraw ){
            [self rasteriseMoreThings];
            GLCHECK;
            
        }
        
        {
            Vect3D res;
            if( cam != NULL && cam->param->label && defaultName != nil 
                && getWinCoordinates(cam, &GGZeroVect, &res ) )
            {
                if( cacheNameRep == nil )
                {
                    ASSIGN(cacheNameRep, [GGGlyphLayout 
				       glyphLayoutWithString: [self name]
                                    withFont: [GGFont fontWithFile: 
                                        DefaultFontName
                                                            atSize: 14]]);
                }
                glPushAttrib(GL_ENABLE_BIT);
                glDisable(GL_LIGHTING);
                glDisable(GL_DEPTH_TEST);
                pushStandardRep(NSMakeSize(cam->param->width, cam->param->height));
                glColor3f(0.5, 0.9, 0.5);
                glTranslatef(res.x+5, res.y-5, 0);
                [cacheNameRep drawAtPoint: NSZeroPoint];
                popStandardRep();
                glPopAttrib();
                NSDebugMLLogWindow(@"label", @"%@ isdrawing", self);
                GLCHECK;
            }
        }
    }
    GLCHECK;
    
    if( ! _flags.isFilsEmpty )
    {
        int i;
        int n = [filsNode count];
        GGSpaceObject *objs[n];
        NSAssert(filsNode != nil, @"bad");
        
        [self willDrawSons: cam];
        GLCHECK;
        [filsNode getObjects: objs];
        
        for( i = 0; i < n; ++i)
        {
            //optimize a little bit for invisible object.
            if( objs[i] != from && objs[i]->_flags.visible )
                [objs[i] drawWithCamera: cam];
            //	  	  [objs[i] drawWithCamera: NULL];
            GLCHECK;
            
        }
        [self hasDrawnSons: cam];
    }
    GLCHECK;
}

- (void) drawWithCamera: (Camera *)cam
{
    Camera camFils;
    NSDebugMLLogWindow(@"Matrice", @"%@\n%@", self, 
                       stringOfMatrice(Matrice(self)));
    NSDebugMLLogWindow(@"MatrixType", @"%@ %d", self, (int)_flags.matrixType);
    
    if( !_flags.visible || (!_flags.graphical && _flags.isFilsEmpty) ) return;
    
    
    glPushMatrix();
    glMultMatrix(Matrice(self));
    
    produitMatrice(&cam->modelView, Matrice(self), &camFils.modelView);
    camFils.param = cam->param;
    camFils.prev = cam;
#ifdef DEBUG
    camFils.owner = self;
#endif
    
    [self drawWithCameraNoTransform: &camFils
                               from: nil];
    GLCHECK;
    
    
    //  if( _flags.matrixType != IdentityMatrix )
    glPopMatrix();
    GLCHECK;
}

- (void) drawGraphProjection: (ProjectionParam *)ppp
{
    unsigned save;
    Camera cam;
    
    GGMatrix4 qid;
    GGMatrixInitModelViewNormalizer(&qid);
    
    NSDebugMLLogWindow(@"MatriceAncestor", @"%@\n%@", self, 
                       stringOfMatrice(Matrice(self)));
    
    initWithIdentityMatrix(&cam.modelView);
    glLoadMatrix( &qid);
    cam.param = ppp;
#ifdef DEBUG
    cam.owner = self;
#endif
    
    // HACK
    save = _flags.graphical;
    _flags.graphical = NO;
    [self ancestorDraw: &cam
                  from: nil];
    _flags.graphical = save;
    
    glDisable(GL_LIGHTING);
}

- (BOOL) windowsCoordinatesForCamera: (GGSpaceObject *)camera
                           parameter: (ProjectionParam *)param
                              result: (Vect3D *)res
{
    GGGalileen gal;
    Camera p;
    [self galileanTransformationTo: camera
                                in: &gal];
    
    p.param = param;
    p.modelView = gal.matrice;
    
    return getWinCoordinates(&p, &GGZeroVect, res);
}

- (id)  selectEntityInFrame: (NSRect)frameToSearch
            projectionParam: (ProjectionParam *)pp
                    camera: (GGSpaceObject*) camera
{
    NSRect aRect = [self windowRectInCamera:camera projectionParam:pp];
    id result = nil;
    if (NSIntersectsRect(frameToSearch, aRect)) {
        NSLog(@"intersection with %@", self);
        result = self;
        GGSpaceObject* son;
        int count = [filsNode count];
        for(int i = 0; i < count; ++i){
            son = [filsNode objectAtIndex:i];
            id tmp = [son selectEntityInFrame:frameToSearch
             projectionParam:pp
              camera:camera];
            if (tmp) {
                return tmp;
            }
        }
    }
    return result;
}

- (NSRect) windowRectInCamera:(GGSpaceObject*)camera
                projectionParam: (ProjectionParam*) param
{
    GGGalileen gal;
    [self galileanTransformationTo:camera in:&gal];
    Camera c;
    bzero(&c, sizeof(c));
    c.param = param;
    c.modelView = gal.matrice;
    return frameForBallInCamera(&c, GGZeroVect, [self boudingSphereRadius]);
}

#pragma mark -
- (void) changeLocalCoordinates: (GGGalileen *) pgal
    /* obj a bougé dans son repère galiléen et on le recentre.
    * gal code le nouveau repère galiléen dans l'ancien.
    */
{
    GGGalileen res;
    
    produitGalileen(pgal, Galileen(self), &res);
    *Galileen(self) = res;
}

- (NSString *)getStringPosition
{
    return stringOfVect(Point(self));
}

- (void) beginSchedule
{
}

- (void) endSchedule
{
    [[theLoop fastScheduler]   deleteTarget: self
                               withSelector: @selector(updatePosition)];
    
}

- (BOOL) manageCollision
{
    return NO;
}

- (void) chocWithEnergy: (GGReal) val
{
}

- (void) collisionOccursWith: (GGSpaceObject *) other
{
    Vect3D diffPos, diffVit;
    Vect3D uniteX;
    GGReal x, a, c, norme;
    float masseOther = Masse(other);
    
    if( CollisionCounter(self) || CollisionCounter(other) )
        return;
    
    diffVect(Point(self), Point(other), &diffPos);
    diffVect(Speed(self), Speed(other), &diffVit);
    
    uniteX = diffPos;
    norme = prodScal(&uniteX, &uniteX);
    if( norme < EPS )
    {
        Vect3D temp = {1, 0, 0};
        uniteX = temp;
    }
    else
    {
        norme = sqrt(norme);
        divScalVect(&uniteX, norme, &uniteX);
    }
    
    NSDebugMLLog(@"Collision2", @"uniteX : %@", stringOfVect2(uniteX));
    
    x = prodScal(&diffVit, &uniteX);
    
    a = 2*x*masse/(masse + masseOther);
    c = x*(masse-masseOther)/(masseOther+masse);
    
    [self chocWithEnergy: masse*SQ(c-x) * MassePerPowerCollision];
    [other chocWithEnergy: masseOther*SQ(a) * MassePerPowerCollision];
    
    addLambdaVect(Speed(self), (c-x), &uniteX, Speed(self));
    addLambdaVect(Speed(other), a, &uniteX, Speed(other));
    CollisionCounter(self) = 1000;
    CollisionCounter(other) = 1000;
}

- (void) enterUniverse
{
}

- (NSString *) displayObject:(int) ind
{
    NSMutableString *str = [NSMutableString string];
    int i, n;
    
    for( i = 0; i  < ind; ++i )
        [str appendString: @"---"];
    
    [str appendFormat: @"%@ %@ %@\n", [self description], 
        stringOfVect(Point(self)), stringOfVect(Speed(self))];
    
    if( ! _flags.isFilsEmpty )
    {
        NSAssert(filsNode != nil, @"bad");
        n = [filsNode count];
        
        if( n > 0 )
        {
            GGSpaceObject *objs[n];
            [filsNode getObjects: objs];
            
            for( i = 0; i < n; ++i)
                [str appendString: [objs[i] displayObject: ind + 1]];
        }
    }
    
    return str;
}


@end

BOOL isSpaceObject(id obj)
{
    return [(NSObject *)obj isKindOfClass: [GGSpaceObject class]];
}
