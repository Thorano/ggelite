/*
 *  GGScene.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <OpenGL/gl.h>
#import <OpenGL/glx.h>
#import <X11/keysym.h>
#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <math.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSException.h>
#import <Foundation/NSArray.h>
#import "GGInput.h"
#import "GGScene.h"
#import "GGRepere.h"
#import "utile.h"
#import "GG3D.h"
#import "Tableau.h"
#import "Commandant.h"
#import "perlin.h"
#import "GGBlock.h"
#import "Texture.h"
#import "Preference.h"

GGScene *theGGScene;

#if 0
@implementation GGScene

- (void) buildCube
{


  b1 = RETAIN([GGBlock topBlockWithManager: &context
		       bump: [PerlinBumpCube 
			       bumpWithNorm: mkVect(1, 0, 0)
			       dir: mkVect(0, 1, 0)]
		       at: -1
		       and: -1
		       width: 2
		       height: 2]);
  b2 = RETAIN([GGBlock topBlockWithManager: &context
		       bump: [PerlinBumpCube 
			       bumpWithNorm: mkVect(0, 1, 0)
			       dir: mkVect(-1, 0, 0)]
		       at: -1
		       and: -1
		       width: 2
		       height: 2]);
  b3 = RETAIN([GGBlock topBlockWithManager: &context
		       bump: [PerlinBumpCube 
			       bumpWithNorm: mkVect(-1, 0, 0)
			       dir: mkVect(0, -1, 0)]
		       at: -1
		       and: -1
		       width: 2
		       height: 2]);
  b4 = RETAIN([GGBlock topBlockWithManager: &context
		       bump: [PerlinBumpCube 
			       bumpWithNorm: mkVect(0, -1, 0)
			       dir: mkVect(1, 0, 0)]
		       at: -1
		       and: -1
		       width: 2
		       height: 2]);

  b5 = RETAIN([GGBlock topBlockWithManager: &context
		       bump: [PerlinBumpCube 
			       bumpWithNorm: mkVect(0, 0, 1)
			       dir: mkVect(1, 0, 0)]
		       at: -1
		       and: -1
		       width: 2
		       height: 2]);

  b6 = RETAIN([GGBlock topBlockWithManager: &context
		       bump: [PerlinBumpCube 
			       bumpWithNorm: mkVect(0, 0, -1)
			       dir: mkVect(-1, 0, 0)]
		       at: -1
		       and: -1
		       width: 2
		       height: 2]);

  b1->gauche = b4;
  b2->gauche = b1;
  b3->gauche = b2;
  b4->gauche = b3;

  b1->droite = b2;
  b2->droite = b3;
  b3->droite = b4;
  b4->droite = b1;

  b1->haut = b5;
  b2->haut = b5;
  b3->haut = b5;
  b4->haut = b5;

  b1->bas = b6;
  b2->bas = b6;
  b3->bas = b6;
  b4->bas = b6;


  b5->bas = b4;
  b5->droite = b1;
  b5->haut = b2;
  b5->gauche = b3;

  b6->bas = b4;
  b6->droite = b3;
  b6->haut = b2;
  b6->gauche = b1;
}

@end

#endif
