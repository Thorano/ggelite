/* 	-*-ObjC-*- */
/*
 *  GGSpaceObject.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGSpaceObject_h
#define __GGSpaceObject_h

#import "GG3D.h"
#import "GGLoop.h"

enum __SpaceObjectMode {
  GGNormalMode = 0x1,
  GGInertialMode = 0x2,
  GGInertialRotationalMode = 0x4
};


#define Galileen(x)(&((x)->repere))
#define Matrice(x) (&((x)->repere.matrice))
#define Speed(x) (&((x)->repere.speed))
#define Omega(x) (&((x)->omega))
#define Point(x) (&((x)->repere.matrice.position))
#define Haut(x) (&((x)->repere.matrice.haut))
#define Gauche(x) (&((x)->repere.matrice.gauche))
#define Direction(x) (&((x)->repere.matrice.direction))
#define Dimension(x) ((x)->dimension)
#define Masse(x) ((x)->masse)
#define CollisionCounter(x) ((x)->collisionCounter)


#define IdentityMatrix 2
#define TranslationMatrix 1
#define GeneralMatrix 0

@class GGRepere;
@class GGSpaceObject;
@class GGShip;
@class GGNode;
@class GGGlyphLayout;

@protocol IA
- (PetitGalileen *) positionRelativeCible;
- (void) setShip: (GGShip *) aShip;
- (void) setTarget: (GGSpaceObject *)aTarget;
- (double) estimatedDateAtDestination;
@end

@protocol WorldManager
- (GGSpaceObject *)camera;
- (GGRepere *)repere;
@end

@protocol GGNode <NSObject>
- (GGNode *) nodePere;
- (void) setNodePere: (GGSpaceObject *) obj;
- (BOOL) isParentOf: (GGSpaceObject *) obj;
- (void) enregistreForRemoving: dest
			  with: (SEL) s;
- (void) enregistre: obj
	       with: (SEL) s;
- (void) drawWithCamera: (Camera *)cam;
- (void) changeLocalCoordinates: (GGGalileen *)gal;
- (NSString *) displayObject: (int) i;

/*!
    @protocol
    @abstract    compute the bounding box of the node, as measured in the parent frame.
*/
- (GGBox) boundingBox;

/*!
    @protocol
    @abstract    compute the bounding box of the node, as measured in the current frame.
*/
- (GGBox) localBoudingBox;

/*!
    @method     
    @abstract   compute a best radius of a bouding sphere
    @discussion the method visit the children hierarchy recursively.  hence, it is a bit costly.
*/
- (GGReal) boudingSphereRadius;
@end

GGBEGINDEC

/*%%%
import GG3D;
%%%*/


@class GGRepere;
@class GGSpaceObject;
@class GGShip;
@class GGNode;
@class GGGlyphLayout;
@class ModelBlender;
@class GGSolid;

@interface GGSpaceObject : NSObject <GGNode, NSCopying, NSCoding>
{
    GGSolid             *_dynamicalHelper;
    @public
    GGSpaceObject     *nodePere;
    NSMutableArray	*filsNode;
    id<WorldManager>	manager;
    GGGalileen		repere;
    Vect3D		omega;		//rotational speed in local coordinates.
                            //  GGReal			distanceLocal2, distanceGlobal2;
    GGReal			dimension;
    double			masse;
    int			collisionCounter;
    
    int           	mode;
    GLuint		compileListId;
    NSString		*defaultName;
    GGGlyphLayout		*cacheNameRep;
    struct {
        unsigned 			isCamera:1;
        unsigned 			matrixType:2;
        unsigned			visible:1;
        unsigned			alive:1;
		unsigned			castShadow:1;
        unsigned			shouldCache:1;
        unsigned			cacheCorrect:1;
        unsigned			clipable:1;
        unsigned 			majRepere:1;
        unsigned			moreThingsToDraw:1;
        unsigned			stateChange:1;
        unsigned			locality:2;  /*0 outside system
					       1 in system but outside local 
            frame
					       2 in local frame
            */  
        unsigned			activated:1; /* 1 when the object shoould update
            its position frequently
            because being observed*/
        unsigned			isFilsEmpty;
        unsigned			graphical;  /* mean that rasterize need to
					       be called*/
    }_flags;
}
- initAt: (GGSpaceObject *)obj;
- (void) invalidate;  /* This method should be called when an object has become
			 useless (To really free ressources before it is 
			 deallocated
		      */
+ spaceObjectAt: (GGSpaceObject *) obj;
- (GGNode *) nodePere;
- (void) setNodePere: (GGSpaceObject *)obj;
- (void) setManager: (id<WorldManager>) newManager;
- (id<WorldManager>) manager;
- (ModelBlender*) model;

- (GGSolid *)dynamicalHelper;
- (void)setDynamicalHelper:(GGSolid *)value;
- (GGSolid*) createDynamicalHelper;

- (void) removeFromSuperNode;
- (GGSpaceObject *)ancestor;
- (BOOL) isParentOf: (GGSpaceObject *)obj;
- (BOOL) shouldSave;
- (BOOL) saveChannel;

- (BOOL) shouldBeClipped;
- (BOOL) castShadow;
- (BOOL) shouldCache;
- (BOOL) shouldDrawMoreThings;
- (BOOL) isLocal;
- (unsigned) locality;
- (void) setLocality: (unsigned)val;
- (void) invalidCache;
- (void) reload;

- (NSString *) signature;
- objectFromSignature;

- (void) setDefaultName: (NSString *)aName;
- (void) setName: (NSString *)aName;

/* Les méthodes suivantes n'ont pas besoin d'être redéfinie dans des 
 * sous-classes.
 */
- (void) setVisible: (BOOL) val;
- (void) setMode: (int) val;
- (void) setCameraFlag: (BOOL) val;

- (void) enregistre: dest
	       with: (SEL) s;
- (void) enregistreForRemoving: dest
			  with: (SEL) s;
- (void) desenregistre: dest;
- (void) reallyDestroy;
- (void) destroy;

/* call before the object is about to be removed from the current system
   It will post a Notification
*/
- (void) looseMainSystem;


/*méthodes qui font joujou avec la géométrie locale de l'objet
 */
- (GGReal) proximite;
- (Vect3D *) getDirection;
- (Vect3D *) getHaut;
- (Vect3D *) getSpeed;
- (void) getGalileen:(GGGalileen *) ret;
//- (void) setGlobalPosition: (Vect3D *) pv;
- (void) setPosition:(Vect3D *) pv;
- (GGMatrix4 *)getMatrix;
- (void) normalise;
- (void) centerObject;

- (NSString *)getStringPosition;
- (NSString *)name;

- (BOOL) manageCollision;
- (void) chocWithEnergy: (GGReal) val;
- (void) collisionOccursWith: (GGSpaceObject *) other;

- (void) translate: (PetitGalileen *) pg
	  refering: (GGSpaceObject *) obj
	     decal: (Vect3D *)v;

/* Là, on arrive à des méthodes assez foireuses qui gèrent le sytème
 * de repère, de matrice, de changement de vue. 
 */


- (void) rotationMatrixTo: (GGSpaceObject *) obj
		       in: (GGMatrix4 *)res;
- (void) galileanTransformationTo: (GGSpaceObject *)obj
			       in: (GGGalileen *)pgal;
- (void) galileanTransformationFromAncestor: (GGSpaceObject *)ancestor
					 in: (GGGalileen *)pgal;
- (void) rotationMatrixFromAncestor: (GGSpaceObject *)ancestor
				 in: (GGMatrix4 *)res;
- (Vect3D) rotateDirection: (Vect3D *) v
		 fromFrame: (GGSpaceObject *)o;

- (Vect3D) getPositionAbsolu;
- (Vect3D) getPositionSpeedAbsolues: (Vect3D *)pspeed;

/*
 *La sémantique de cette méthode est:
 * On calcule la position et la speed  self relativement à obj.
 * Le résultat est converti dans le système de coordonnée dans lequel obj vie
 * !!!IL NE S'AGIT PAS DES COORDONNEES LOCALES DE OBJ !!!
 * plutôt celle du pere de obj.
 */
- (void) positionSpeedRelativesTo: (GGSpaceObject *)obj
			    decal: (const Vect3D *)v
			       in: (PetitGalileen *)pgl;

/* Cette méthode calcule la position de self dans repere. A ne pas confondre 
 * avec la méthode précédente
 */
- (void) positionInRepere: (GGSpaceObject *)repere
		       in: (Vect3D *)pv;

/*Fais la meme chose que positionSpeedRelativesTo:in: mais le resultat
  est mesuré dans les cooordonnées de repere.  Elle est plus complete que la
  méthode précédente :
*/
- (void) positionSpeedInRepere: (GGSpaceObject *)repere
			    in: (PetitGalileen *)pgl;

/* compute the extension of the node (and its sons) along a direction axis.  It is assumed that direction is unit.  the minimum extension is return.  The max is return by reference in maxRef.  maxRef must point to a valid location.
 */
- (GGReal) computeExtensionAlongDirection:(Vect3D)direction
                                   maxRef:(GGReal*) maxRef;

/* this method is called mostly by nodes to change the coordinates of the 
   object when the frame changes.  If one want to attach an object to the 
   origin of a node (bad idea but it is used in Planete.m), one can override
   this method to do nothing
*/
- (void) changeLocalCoordinates: (GGGalileen *) gal;

/*méthode très utile qui convertit les coordonnées de self en 
  coordonnées fenêtre.  Le type de camera est dans param.
  renvoie YES si on screen, NO sinon
*/
- (BOOL) windowsCoordinatesForCamera: (GGSpaceObject *)camera
			   parameter: (ProjectionParam *)param
			      result: (Vect3D *)res;

/*!
    @method     
    @abstract   select an entity in the scenary graph
    @discussion given a box in the screen and a projection and a camera, find the more relevant guy.
*/
- (id)  selectEntityInFrame: (NSRect)frameToSearch
            projectionParam: (ProjectionParam *)pp
                    camera: (GGSpaceObject*) camera;

- (NSRect) windowRectInCamera:(GGSpaceObject*)camera
                projectionParam: (ProjectionParam*) param;


/* déplace la position courante le long du vecteur speed.
 * La position peut-être locale où globale.
 */
- (void) updatePosition;
- (void) drawWithCameraNoTransform: (Camera *)cam
			      from: (GGSpaceObject *)from;
- (void) drawWithCamera: (Camera *)cam;
- (void) drawGraphProjection: (ProjectionParam *)ppp;
- (void) rasterise;
- (void) rasterise: (Camera *)cam;


/*Fonctions diverses et variées.
 */




/*Should be call after all the initialisation of the objet
 */
- (void) enterUniverse;

/*Should be call after all the initialisation of the objet, in
 *order it can organise it's own scheduling.  
 * For a ship, it when it will be part of the current system.
 */
- (void) beginSchedule;

/* Should be call when an object should be not schedule (because it is
 *  not in the current system any more, ...) all the schedules begun
 *  in [ - beginSchedule] should be ended here 
 */
- (void) endSchedule;



- (NSString *) displayObject:(int) ind;
@end

/*%%%
inline Point(o : GGSpaceObject) : Vect3D { o.repere.matrice.position }
inline Haut(o : GGSpaceObject) : Vect3D { o.repere.matrice.haut }
inline Speed(o : GGSpaceObject) : Vect3D { o.repere.speed }
inline Direction(o : GGSpaceObject) : Vect3D { o.repere.matrice.direction }
inline Dimension(o : GGSpaceObject) : Float { o.dimension }
inline CollisionCounter(o : GGSpaceObject) : Int { o.collisionCounter }
%%%*/


BOOL isSpaceObject(id);

GGENDDEC

#define monAddLambdaVect(a, b, c, d) do {	\
  addLambdaVect((a), (b), (c), (d));		\
  majRepere = 1;				\
}while(0)

#define DOLABEL [self drawLabel]

extern NSString *GGNotificationRemoveFromCurrent;
extern NSString *GGNotificationDisparition;
extern NSString *GGNotificationVanishing;

#endif
