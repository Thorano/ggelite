/*
 *  System.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: May 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*Sequence d'operation
:
J'ai tiré sur le vaisseau.
Puis j'ai envoyé deux missiles. 
Et finalement, je l'ai abattu au laser.  Il y a eu le bug :
2003-04-21 22:42:15.956 elite[17236] GGSpaceObject.m:505  Assertion failed in GGShip(instance), method positionSpeedRelativesTo:decal:in:.  not in the hierarchy QM-7066 H - @rMissile@w
elite: Uncaught exception NSInternalInconsistencyException, reason: GGSpaceObject.m:505  Assertion failed in GGShip(instance), method positionSpeedRelativesTo:decal:in:.  not in the hierarchy QM-7066 H - @rMissile@w
zsh: ggabort (core dumped)  elite

*/

#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSCoder.h>

#import "Metaobj.h"
#import "System.h"
#import "Planete.h"
#import "Station.h"
#import "utile.h"
#import "Item.h"
#import "Sched.h"
#import "Resource.h"
#import "GGConcreteShip.h"
#import "World.h"
#import "Commandant.h"

@interface SystemArchiver (GGPrivate)
- (void) clear;
@end

@interface GGShip (Randomizer)
+ randomPirate: (int) eliteRank;
+ randomPirateForTarget: (GGShip *) target;
@end

@implementation GGShip (Randomizer)
+ randomPirate: (int) eliteRank
{
    GGShip *aShip;
    if( eliteRank < 3 )
    {
        aShip  = [GGShip shipWithFileName: @"eagle1.shp"];
        [aShip addEquipment: Laser_1mw_Pulse]; //Laser Beam 1mw
    }
    else
    {
        aShip  = [GGShip shipWithFileName: @"viper.shp"];
        [aShip addEquipment: Laser_1mw_Beam]; //Laser Beam 1mw
        [aShip addEquipment: Shield_Generator];
    }    
    [aShip setDefaultEquipment];
    [aShip setEliteLevel: eliteRank];
    return aShip;
}

+ randomPirateForTarget: (GGShip *) ship
{
    int n;
    
    /*elite rank of the pirat depends on the capacity of the target*/
    n = log10(ship->capacity)-2;
    return [self randomPirate: 1];
}
@end


@implementation System
static float influenceFromDistance(float val, Vect3D *p)
{
    const double sigma2 = SQ(20*100);
    double fact;
    
    fact = exp(-val/sigma2);
    return fact;
}

static BOOL bordel(float faible, float fort)
{
    if(fort < 0.1) return YES;
    if(faible/fort > 0.8) return YES;
    return NO;
}

- (void) computeRandomData: (Star *)s
{
    static Vect3D posFed = {0,0,0};
    static Vect3D posEmp = {4000,4000,0};
    BOOL bordelo;
    
    double d1, d2;
    Vect3D pos;
    srand(s->indexStar);
    systemType = (SystemType)(irnd(2)+1);
    
    initVect(&pos, s->xd, s->yd, s->zd);
    
    d1 = distance2Vect(&pos, &posFed);
    d2 = distance2Vect(&pos, &posEmp);
    
    federation = influenceFromDistance(d1, &pos);
    empire = influenceFromDistance(d2, &pos);
    
    if(federation > empire)
    {
        if ( bordel(empire,federation) )
        {
            allegeance = None_Force;
            bordelo = YES;
        }
        else
        {
            allegeance =  Federation;
            bordelo = NO;
        }
    }
    else
    {
        if ( bordel(federation, empire) )
        {
            allegeance = None_Force;
            bordelo = YES;
        }
        else
        {
            allegeance =  Empire;
            bordelo = NO;
        }
    }
    
    {
        double val = federation + empire;
        
        if ( coin(5) )
            population = random_var((exp(15*val)-1), 0.2);
        else
            population = random_var(20, 0.4) - 20;
        if (population < 0)
            population = 0;
        
        if( bordelo )
            piracy = log10(population+1)+1;
        else
            piracy = 1;
        
        
    }
}

- initWithStar: (Star *)s
      archiver: (SystemArchiver *) _archiver
{
    float mag;
    [super init];
    mag = absoluteMagnitude(s);
    
    NSDebugMLLog(@"System", @"mag %g", (double) mag);
    theSystem = self;
    index = s->indexStar;
    K0 = -1.0;
    ASSIGN(archiver, _archiver);
    
    if( s->indexStar == 0 )
    {
        federation = 1;
        empire = 0;
        systemType = Modern;
        systemSpeciality = HighTech | Technique | Populated;
        population = 100e6;
    }
    else
    {
        [self computeRandomData: s];
    }
    addToCheck(self);
    return self;
}

/* the method copy will keep the old theSystem, and this is good so I don't
redefine it
*/

- copyWithZone: (NSZone *)zone
{
    NSParameterAssert(0);
    return nil;
}

- (void) invalidate
{
    [mainStar invalidate];
}

- (void) dealloc
{
    [aTimer invalidate];
    RELEASE(goodies);
    RELEASE(equipment);
    RELEASE(illegal);
    RELEASE(archiver);
    RELEASE(aTimer);
    RELEASE(mainStar);
    [super dealloc];
}

+ systemAt: (Star*) s
  archiver: (SystemArchiver *) _archiver
{
    return AUTORELEASE([[self alloc] initWithStar: s
                                         archiver: _archiver]);
}

+ systemForStarIndex: (int) ind
{
    Star *s;
    int size;
    
    s = [[theWorld sky] stars: &size];
    NSParameterAssert(0 <= ind && ind < size);
    
    return [self systemAt: s + ind
                 archiver: nil];
}

- (SystemArchiver *) archiver
{
    NSParameterAssert(archiver);
    return archiver;
}

- (void) setArchiver: (SystemArchiver *)a
{
    NSParameterAssert(archiver == nil);
    ASSIGN(archiver, a);
}

- (GGOrbiteur *) mainStar
{
    if( mainStar == nil )
    {
        if( index == 0 )
        {
            srand(index);
            [ItemStock setSystemGoods: self];
            mainStar = [[Planete alloc] initSystemWithContentOfFile: 
                [[Resource resourceManager] pathForModel: 
                    @"solsys.plist"]];
        }
        else
        {
            srand(index);
            [ItemStock setSystemGoods: self];
            mainStar =  [[Planete alloc]initRandomSystemForStar: self];
        }
        mainStar->surroundingSystem = self;
        [mainStar calculeTailleSysteme];
    }
    return mainStar;
}

- (SystemSpeciality) systemSpeciality
{
    return systemSpeciality;
}

- (float) federation
{
    return federation;
}

- (float) empire
{
    return empire;
}


- (double) population
{
    return population * 1e3;
}

- (NSString *) allegeance
{
    switch(allegeance)
    {
        case     Federation:
            return @"Federation";
        case    Empire:
            return @"Empire";
        case    Independant:
            return @"Independant";
        case	None_Force:
            return @"None";
        default:
            NSAssert(0, @"bad");
            return nil;
    }
}


- (NSArray *) shipMaterialToSel
{
    return AUTORELEASE([equipment copy]);
}

- (NSArray *) goodiesToSel
{
    return AUTORELEASE([goodies copy]);
}


- (NSString *) systemType
{
    switch(systemType)
    {
        case Modern: return @"Modern";
        case Farmer: return @"Farmer";
        case Intermediate: return @"Intermediate";
    }
    return nil;
}

- (int) index
{
    return index;
}

// static inline double rnreal(double val, double scale)
// {
//   if( val < 0.01 )
//     return scale * exp(

static inline double rnreal(void)
{
    //   return atanh(2*vrnd()-1);
    return tan(M_PI*(vrnd()-0.5));
}

- (Vect3D) randomVect
{
    Vect3D res;
    
    if ( K0 < 0 )
    {
        double dim;
        NSAssert(mainStar, @"bad");
        dim = [mainStar tailleSysteme];
        
        K0 = dim / 100;
    }
    
    res.x = K0*rnreal();
    res.y = K0*rnreal();
    res.z = K0*rnreal();
    
    NSDebugMLLog(@"RandomVect", @"tailleSysteme = %g\nK0 = %g\nrandon is %@", 
                 [mainStar tailleSysteme],
                 K0,
                 stringOfVect(&res));
    
    return res;
}

- (void) createAShip: (GGTimer *)ggtimer
{
    NSArray *shipList =   [[Resource resourceManager] ships];
    Vect3D initPos;
    GGShip *aShip = [GGShip shipWithFileName: 
        [shipList objectAtIndex: irnd([shipList count])]];
    initPos = [self randomVect];
    [aShip setPosition: &initPos];
    [theWorld addSubNode: aShip];
    [theWorld addShip: aShip];
    [aShip setDefaultIA: nil];
    [aShip setDefaultEquipment];
    NSDebugMLLog(@"IA", @"blabla createAShip : %@", aShip);
}

- (GGShip *) createStandardShip
{
    GGShip *aShip = [GGShip shipWithFileName: @"boa.shp"];
    Vect3D initPos;
    
    initPos = [self randomVect];
    [aShip setPosition: &initPos];
    [theWorld addSubNode: aShip];
    [theWorld addShip: aShip];
    [aShip setDefaultEquipment];
    NSLog(@"on a mis %@", aShip);
    return aShip;
}

- (void) saveState
{
    [archiver clear];
    [[self allStations] makeObjectsPerformSelector: @selector(saveState:)
                                        withObject: archiver];
}

- (void) postLoadingSystem
{
    NSParameterAssert(archiver != nil);
    [[self allStations] makeObjectsPerformSelector: @selector(restoreState:)
                                        withObject: archiver];
}

- (void) afterEnteringSystem
{
    int i;
    int n;
    
    //   aTimer = RETAIN([GGTimer scheduledTimerWithTimeInterval: 1e5
    // 			   target: self
    // 			   selector: @selector(createAShip:)
    // 			   repeats: YES]);
    
    n = log(population) - 2;
    
    n = n <= 10 ? n: 10;
    n = n >= 0 ? n : 0;
    
    NSLog(@"debut");
    
    for (i = 0; i < piracy ; ++ i)
    {
        Vect3D initPos;
        GGShip *aShip = [GGShip randomPirateForTarget: [theWorld commandant]];
        initPos = [self randomVect];
        [aShip setPosition: &initPos];
        [theWorld addSubNode: aShip];
        [theWorld addShip: aShip];
        //    [aShip beginSchedule];
        
        [aShip setDefaultIA: [theWorld commandant]];
        
        NSDebugMLLog(@"Pirate", @"pirate : %@ at: %g", aShip, gameTime);
    }
    
    NSLog(@"debut2");
    if( n > 0  )
    {
        NSArray *shipList =   [[Resource resourceManager] ships];
        NSLog(@"debut3");
        
        for(i = 0; i < n; ++i)
        {
            Vect3D initPos;
            GGShip *aShip = [GGShip shipWithFileName: 
                [shipList objectAtIndex: 
                    irnd([shipList count])]];
            initPos = [self randomVect];
            [aShip setPosition: &initPos];
            [aShip setDefaultEquipment];
            [theWorld addSubNode: aShip];
            [theWorld addShip: aShip];
            
            [aShip setDefaultIA: nil];
            
            NSDebugMLLog(@"IA", @"blabla createAShip : %@", aShip);
        }
    }
    [self postLoadingSystem];
    
    NSLog(@"fin");
}

- (void) makeCurrent
{
    NSEnumerator *enumerator;
    GGSpaceObject *obj;
    enumerator = [[[self mainStar] getAllOrbiteur] objectEnumerator];
    
    while( (obj = [enumerator nextObject]) != nil )
    {
        [obj setLocality: 1];
    }
}

- (void) endCurrent
{
    NSEnumerator *enumerator;
    GGSpaceObject *obj;
    
    [aTimer invalidate];
    DESTROY(aTimer);
    [self saveState];
    enumerator = [[[self mainStar] getAllOrbiteur] objectEnumerator];
    
    while( (obj = [enumerator nextObject]) != nil )
    {
        [obj setLocality: 0];
    }
}

- (NSArray *) allStations
{
    id obj;
    NSEnumerator *enumerator;
    NSMutableArray *res;
    NSArray *allOrb;
    GGOrbiteur *sol;
    sol = [self mainStar];
    res = [NSMutableArray array];
    
    allOrb = [sol getAllOrbiteur];
    enumerator = [allOrb objectEnumerator];
    
    
    while( (obj = [enumerator nextObject]) != nil )
    {
        if( [obj isKindOfClass: [Station class]] )
            [res addObject: obj];
    }
    
    return res;
}

- (Station *) stationForIndex: (int) val
{
    return [[self allStations] objectAtIndex: val];
}

+ (NSString *) starNameForIndex: (int) val
{
    NSString *s;
    GGSky *sky = [theWorld sky];
    s = [NSString stringWithUTF8String: [sky cNameOf: val]];
    return s;
}

- (id) randomStation
{
    NSArray *stations;
    int size;
    stations = [self allStations];
    size = [stations count];
    if( size > 0 )
    {
        return [[stations objectAtIndex: irnd(size)] signature];
    }
    else
        return nil;
}

- (int) randomClosestStarIndex: (int) _index
{
    if( _index < 0 )
        return [[theWorld sky] randomStarIndexCloseTo: index];
    else
        return [[theWorld sky] randomStarIndexCloseTo: _index];
}

- (void) setGoodies: (NSArray*) _goodies
          equipment: (NSArray*) _equipment
            illegal: (NSArray*) _illegal
{
    ASSIGN(goodies, _goodies);
    ASSIGN(equipment, _equipment);
    ASSIGN(illegal, _illegal);
}

@end

@implementation SystemArchiver
- init
{
    [super init];
    ships = [NSMutableArray new];	
    dico = [NSMutableDictionary new];
    addToCheck(self);
    return self;
}

+ systemArchiver
{
    return AUTORELEASE([self new]);
}


- (NSArray *) ships;
{
    return ships;
}

- (void) addShip: (GGSpaceObject *)ship
{
    [ship enregistre: self
                with: @selector(removeWithNotification:)];
    [ships addObject: ship];
}

- (void) removeWithNotification: (NSNotification *) aNotification
{
    id obj = [aNotification object];
    int index = [ships indexOfObjectIdenticalTo: obj];
    
    if(index != NSNotFound)
    {
        NSDebugMLLog(@"SystemArchiver", @"removing ships %@ with not", obj);
        [ships removeObjectAtIndex: index];
    }
}

- (void) removeShip: (GGSpaceObject *)ship
{	
    int index = [ships indexOfObjectIdenticalTo: ship];
    
    NSParameterAssert(index != NSNotFound);
    NSDebugMLLog(@"SystemArchiver", @"direct remove of %@", ship);
    
    [ships removeObjectAtIndex: index];
}

- (void) setData: obj
         station: (Station *)s
{
    [dico setObject: obj
             forKey: [s signature]];
}

- dataForStation: (Station *)s
{
    return [dico objectForKey: [s signature]];
}

- (void) dealloc
{
    NSDebugMLLog(@"SystemArchiver", @"ships : %@", ships);
    [MessageManager removeObserver: self];
    RELEASE(ships);
    RELEASE(dico);
    [super dealloc];
}

- initWithCoder: (NSCoder *)decoder
{
    GGDecode(ships);
    GGDecode(dico);
    addToCheck(self);
    return self;
}

- (void) encodeWithCoder: (NSCoder *) encoder
{
    GGEncode(ships);
    GGEncode(dico);
}  

- (BOOL) shouldSave
{
    return YES;
}

- (BOOL) saveChannel
{
    return YES;
}

@end


@implementation SystemArchiver (GGPrivate)
- (void) clear
{
    [dico removeAllObjects];
}
@end
