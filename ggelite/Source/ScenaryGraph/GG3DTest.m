//
//  GG3DTest.m
//  elite
//
//  Created by Frédéric De Jaeger on 16/05/10.
//  Copyright 2010 Fotonauts. All rights reserved.
//

#import "GG3DTest.h"
#import "GG3D.h"

@implementation GG3DTest
- (void) testTransposeMatrice4
{
    GGMatrix4 shipPos;
    initWithIdentityMatrix(&shipPos);
    shipPos.direction = mkVect(1, 0, 0);
    shipPos.haut = mkVect(0, 0, 1);
    shipPos.position = mkVect(-150e9+50e6, 0, 0);
    normaliseMatrice(&shipPos);
    
    GGMatrix4 earthPos;
    initWithIdentityMatrix(&earthPos);
    earthPos.position = mkVect(-150e9, 0, 0);
    
    GGMatrix4 localFrame;
    initWithIdentityMatrix(&localFrame);
    localFrame.position = mkVect(50e6, 0, 0);
    
    GGMatrix4 shipInLocalFrame;
    initWithIdentityMatrix(&shipInLocalFrame);
    shipInLocalFrame.direction = mkVect(1, 0, 0);
    shipInLocalFrame.haut = mkVect(0, 0, 1);
    normaliseMatrice(&shipInLocalFrame);
    
    GGMatrix4 identiti;
    initWithIdentityMatrix(&identiti);
    
    GGMatrix4 tmp, tmp2, tmp3;
    transposeProduit(&earthPos, &identiti, &tmp);
    transposeProduit(&localFrame, &tmp, &tmp2);
    transposeProduit(&shipInLocalFrame, &tmp2, &tmp3);
    
    Vect3D resPt;
    transformePoint4D(&tmp3, &shipPos.position, &resPt);
}


@end
