/*
 *  GGSky.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <string.h>
#import <math.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSData.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSValue.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArchiver.h>

#import "Metaobj.h"
#import "GGSky.h"
#import "GGInput.h"
#import "GG3D.h"
#import "GGSpaceObject.h"
#import "utile.h"
#import "Preference.h"
#import "GGRepere.h"
#import "Texture.h"
#import "World.h"
#import "GGGlyphLayout.h"
#import "Tableau.h"
#import "GGFont.h"
#import "Resource.h"

/*
 prematrice :
 | +3.33084e-01 +9.42857e-01 +8.75366e-03 +0.00000e+00 |
 | +5.62825e-01 -2.06261e-01 +8.00428e-01 +0.00000e+00 |
 | +7.56494e-01 -2.61683e-01 -5.99365e-01 +0.00000e+00 |
 | +0.00000e+00 +0.00000e+00 +0.00000e+00 +1.00000e+00 |
 */

static GGSky *theSky;

static double setColor(double mag)
{
    double val;
    if( mag < 0.1 )
        val = 1.1-mag;
    //    val = 1;
    else
    {
        val = (2/mag);
        val = (val < 0 ? 0 : val);
        if( val >= 0.6 )
            val =  sqrt(val);
    }
    //  return val;
    return (val > 1 ? 1 : val);
}


void parseAStar(const char *chaine, Star *pStar, NSMutableData *bigString)
{
    static int line = 0;
    const char *tmp;
    char nom[50];
    int val;
    double x, y, z;
    double mag;
    static float rot[3][3] = {
    {3.33084e-01, 5.62825e-01, 7.56494e-01},
    {+9.42857e-01, -2.06261e-01, -2.61683e-01},
    {+8.75366e-03, +8.00428e-01, -5.99365e-01}};
    chaine = strchr(chaine, ':');
    chaine++;
    tmp = strchr(chaine, ':');
    
    NSCParameterAssert(tmp-chaine < 50);
    
    strncpy(nom, chaine, tmp-chaine); /*FIXME:too bad if tmp-chaine > 50 */
    nom[tmp-chaine] = '\0';
    pStar->index = [bigString length];
    [bigString appendBytes: nom
                    length: tmp-chaine+1];
    pStar->stringLayout = nil;
    chaine = tmp+1;
    if( (val=sscanf(chaine, "%le:%le:%le:%le", &x, &y, &z, &mag)) != 4 )
    {
        NSLog(@"erreur de parsing ligne %d val = %d name =%s", line+1, val, nom);
        abort();
    }
    
    /* let A be the transformation y -> -y;
    In fact, we want to compute  A.M.A
        All this stuffs come from the face that Braben make a mistake with
        his 3d map...
        */
    pStar->xd = rot[0][0]*x - rot[1][0]*y + rot[2][0]*z;
    pStar->yd = -rot[0][1]*x + rot[1][1]*y - rot[2][1]*z;
    pStar->zd = rot[0][2]*x - rot[1][2]*y + rot[2][2]*z;
    pStar->mag0 = mag;
    
    line++;
}



@implementation ZoneKey
- (BOOL) isEqual: (ZoneKey *)k
{
    return x == k->x && y == k->y && z == k->z;
}

- (unsigned) hash
{
    return x+y+z;
}

- (id)copyWithZone:(NSZone *)zone
{
    return NSCopyObject(self, 0, zone);
}

- (id)initWithCoder:(NSCoder *)decoder
{
    GGDecode(x);
    GGDecode(y);
    GGDecode(z);
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    GGEncode(x);
    GGEncode(y);
    GGEncode(z);
}

+ keyWith: (int) _x
      and: (int) _y
      and: (int) _z
{
    ZoneKey *key;
    key = [self alloc];
    key->x = _x;
    key->y = _y;
    key->z = _z;
    addToCheck(key);
    return AUTORELEASE(key);
}

+ keyWith: (int) _x
      and: (int) _y
{
    return [self keyWith: _x
                     and: _y
                     and: 0];
}


- description
{
    if (z)
        return [NSString stringWithFormat: @"(%d, %d, %d)",  x, y, z];
    else
        return [NSString stringWithFormat: @"(%d, %d)",  x, y];
}

ZoneKey *zoneForPos(Vect3D *pos)
{
    int x = pos->x / StarsZoneSize;
    int y = pos->y / StarsZoneSize;
    int z = pos->z / StarsZoneSize;
    
    return [ZoneKey keyWith: x
                        and: y
                        and: z];
}
@end


@implementation GGSky


static NSDictionary *buildCache(Star theStars[SIZEMAX], int n)
{
    NSMutableDictionary *dico;
    int i;
    
    dico = [NSMutableDictionary dictionary];
    
    for(i = 0; i < n; ++i)
    {
        StarsZone *zone;
        Vect3D pos;
        ZoneKey *val;
        initVect(&pos, theStars[i].xd, theStars[i].yd, theStars[i].zd);
        
        if( prodScal(&pos, &pos) < 1e20 )
        {
            val = zoneForPos(&pos);
            
            zone = [dico objectForKey: val];
            
            if( zone == nil )
            {
                zone = [StarsZone starsZone];
                [dico setObject: zone
                         forKey: val];
            }
            
            [zone addIndex: i];
        }
    }
    
    return dico;
}  


- (void) computezones
{
    NSString *str;
    NSData *data;
    
    str = [[Resource resourceManager] pathForModel: @"stars.cache"];
    NSDebugMLLog(@"cache", @"trying cache file %@", str);
    
    if(str == nil || (data=[NSData dataWithContentsOfFile: str])  == nil 
       || [data length] == 0)
    {
        NSDictionary *dico;
        NSWarnMLog(@"unable to load dico, generating a new one");
        dico = buildCache(theStars, nStars);
        
        ASSIGN(starsZones, dico);
        NSDebugMLLog(@"cache", @"il y a %d zones", [starsZones count]);
    }
    else
    {
        starsZones = [NSUnarchiver unarchiveObjectWithData: data];
        NSWarnMLog(@"loading of cache dics successfull %@", str);
        RETAIN(starsZones);
        NSDebugMLLog(@"cache", @"il y a %d zones", [starsZones count]);
    }
}

static int parseStars(Star theStars[SIZEMAX], NSMutableData *bigString)
{
    int i;
    char chaine[200];
    FILE *input;
    input = fopen([[[Resource resourceManager] pathForModel: @"stars.gg"]
        fileSystemRepresentation], "r");
    NSCAssert(input != NULL, @"can't find stars");
    
    for ( i = 0; i < SIZEMAX && !feof(input) ; ++i )
    {
        fgets(chaine, 200, input);
        parseAStar(chaine, &(theStars[i]), bigString);
        theStars[i].indexStar = i;
    }
    NSCAssert1(i < SIZEMAX, @"trop de parsing %d", i);
    return i;
}

- initAtOrigin: (Vect3D) origin
{
    int oldMode;
    oldMode = setMode(0);
    [super init];
    wishIndex = -1;
    isCompile = NO;
    
    fontStar = RETAIN([GGFont fontWithFile: DefaultFontName
                                    atSize: 12]);
    bigString = [NSMutableData new];
    nStars = parseStars(theStars, bigString);
    addToCheck(self);
    [self computezones];
    [self genereSkyAt: origin];
    
    [[NSNotificationCenter defaultCenter]
    addObserver: self
       selector: @selector(smallTextureFlagChanged)
           name: @"SmallTextureFlagChanged"
         object: nil];
    
    //we create a memory leak here because the destruction of this object
    // costs a lot.  And it's only destructed at the end of the process
    // so we don't care
    
    //  RETAIN(self);
    
    setMode(oldMode);
    
    theSky = self;
    
    return self;
}

+ (void) postIntall
{
    BOOL res;
    NSDictionary *dico;
    Star stars[SIZEMAX];
    int ns;
    NSString *tmp;
    
    tmp = [[Resource resourceManager] pathForModel: @"stars.gg"];
    NSAssert(tmp, @"can't find resources");
    tmp = [tmp stringByDeletingLastPathComponent];
    tmp = [tmp stringByAppendingPathComponent: @"stars.cache"];
    
    NSLog(@"generating a dictionary at %@", tmp);
    ns = parseStars(stars, nil);
    dico = buildCache(stars, ns);
    
    NSDebugMLLog(@"cache", @"il y a %d zones", [dico count]);
    res = [[NSArchiver archivedDataWithRootObject: dico]
	  writeToFile: tmp
       atomically: YES];
    if (res)
        NSLog(@"write cache file is successfull");
    else
        NSLog(@"write to cache file fails");
    
}

- (void) setDestination: (int) index
{
    wishIndex = index;
}

- (void) dealloc
{
    int i;
    glDeleteLists(idList, 1);
    [[NSNotificationCenter defaultCenter]
    removeObserver: nil 
              name: nil 
            object: self];
    RELEASE(bigString);
    RELEASE(fontStar);
    RELEASE(dev);
    RELEASE(der);
    RELEASE(ba);
    RELEASE(ha);
    RELEASE(ga);
    RELEASE(dr);
    for( i = 0; i < nStars; ++i)
    {
        RELEASE(theStars[i].stringLayout);
    }
    RELEASE(starsZones);
    [super dealloc];
}

- (void) smallTextureFlagChanged
{
    DESTROY(dev);
    DESTROY(der);
    DESTROY(ba);
    DESTROY(ha);
    DESTROY(ga);
    DESTROY(dr);
    //FIXME
    [self genereSkyAt: GGZeroVect];
}

- (void) genereSkyForIndex: (int) index
{
    Vect3D pos = {theStars[index].xd, theStars[index].yd, theStars[index].zd};
    [self genereSkyAt: pos];
}

typedef unsigned char starFace[SIZETEXTURESKY];

static void setZero(NSZone *zone, starFace **pface)
{
    *pface = NSZoneCalloc(zone, SIZETEXTURESKY, sizeof(starFace));
    NSCParameterAssert(*pface != NULL);
}

- (void) genereSkyAt: (Vect3D) origin
{
    starFace	* devant;
    starFace	*derriere;
    starFace	*droite;
    starFace	*gauche;
    starFace	*haut;
    starFace	*bas;
    int sizeTextureSky;
    int i;
    NSZone	*zone = [self zone];
    
    setZero(zone, &devant);
    setZero(zone, &derriere);
    setZero(zone, &droite);
    setZero(zone, &gauche);
    setZero(zone, &haut);
    setZero(zone, &bas);
    
    if( thePref.SmallTexture )
    {
        sizeTextureSky = 256;
    }
    else
        sizeTextureSky = 512;
    
    for( i = 0; i < nStars; ++i )
    {
        Star 	s 	= theStars[i];
        double 	norm 	= sqrt(square(s.xd-origin.x)+square(s.yd-origin.y) +
                               square(s.zd-origin.z));
        double	distSol = sqrt(square(s.xd)+square(s.yd) + square(s.zd));
        double 	x, y, z;
        int 	xi, yi;
        
        if( norm <= 1 )
        {
            theStars[i].intensity = 0.0;
            DESTROY(theStars[i].stringLayout);
            continue;
        }
        
        x 	= (s.xd-origin.x) / norm;
        y 	= (s.yd-origin.y) / norm;
        z 	= (s.zd-origin.z) / norm;
        
        initVect(&theStars[i].pos, x, y, z);
        theStars[i].intensity = setColor(theStars[i].mag0 + 
                                         5*log10(distSol/norm));
        
        if( theStars[i].intensity > 0.7 )
        {
            ASSIGN(theStars[i].stringLayout,
                   [GGGlyphLayout glyphLayoutWithString:
                       [NSString stringWithUTF8String: [self cNameOf: i]]
                                               withFont: fontStar]);
        }
        else
            DESTROY(theStars[i].stringLayout);
        
        NSParameterAssert(theStars[i].intensity <= 1.0);
        //        {
        //  	unsigned char c = theStars[i].intensity*255;
        //  	NSLog(@"%@ %d %g", theStars[i].name, (int)c, theStars[i].intensity);
        //        }
        
        if( fabs(x) > fabs(y) )
        {
            if( fabs(x) > fabs(z) )
            {
                if( x > 0 )
                {
                    xi = (y / x + 1)/2*(sizeTextureSky-1);
                    yi = (z / x + 1)/2*(sizeTextureSky-1);
                    devant[yi][xi] = theStars[i].intensity*255;
                }
                else
                {
                    xi = (y / x + 1)/2*(sizeTextureSky-1);
                    yi = (z / x + 1)/2*(sizeTextureSky-1);
                    derriere[yi][xi] = theStars[i].intensity*255;
                }
            }
            else
            {
                if( z > 0 )
                {
                    xi = (x / z + 1)/2*(sizeTextureSky-1);
                    yi = (y / z + 1)/2*(sizeTextureSky-1);
                    haut[yi][xi] = theStars[i].intensity*255;
                }
                else
                {
                    xi = (x / z + 1)/2*(sizeTextureSky-1);
                    yi = (y / z + 1)/2*(sizeTextureSky-1);
                    bas[yi][xi] = theStars[i].intensity*255;
                }
                
            }
        }
        else
        {
            if( fabs(y) > fabs(z) )
            {
                if( y > 0 )
                {
                    xi = (x / y + 1)/2*(sizeTextureSky-1);
                    yi = (z / y + 1)/2*(sizeTextureSky-1);
                    gauche[yi][xi] = theStars[i].intensity*255;
                }
                else
                {
                    xi = (x / y + 1)/2*(sizeTextureSky-1);
                    yi = (z / y + 1)/2*(sizeTextureSky-1);
                    droite[yi][xi] = theStars[i].intensity*255;
                }
            }
            else
            {
                if( z > 0 )
                {
                    xi = (x / z + 1)/2*(sizeTextureSky-1);
                    yi = (y / z + 1)/2*(sizeTextureSky-1);
                    haut[yi][xi] = theStars[i].intensity*255;
                }
                else
                {
                    xi = (x / z + 1)/2*(sizeTextureSky-1);
                    yi = (y / z + 1)/2*(sizeTextureSky-1);
                    bas[yi][xi] = theStars[i].intensity*255;
                }
                
            }
            
        }
    }
    
    if( dev != nil )
    {
        [dev changeStarField: &(devant[0][0])
                       width: sizeTextureSky
                      height: sizeTextureSky];
        [der changeStarField: &(derriere[0][0])
                       width: sizeTextureSky
                      height: sizeTextureSky];
        [ha changeStarField: &(haut[0][0])
                      width: sizeTextureSky
                     height: sizeTextureSky];
        [ba changeStarField: &(bas[0][0])
                      width: sizeTextureSky
                     height: sizeTextureSky];
        [dr changeStarField: &(droite[0][0])
                      width: sizeTextureSky
                     height: sizeTextureSky];
        [ga changeStarField: &(gauche[0][0])
                      width: sizeTextureSky
                     height: sizeTextureSky];
    }
    else
    {
        
        dev = [[Texture alloc] initStartField: &(devant[0][0])
                                        width: sizeTextureSky
                                       height: sizeTextureSky];
        der = [[Texture alloc] initStartField: &(derriere[0][0])
                                        width: sizeTextureSky
                                       height: sizeTextureSky];
        ha = [[Texture alloc] initStartField: &(haut[0][0])
                                       width: sizeTextureSky
                                      height: sizeTextureSky];
        ba = [[Texture alloc] initStartField: &(bas[0][0])
                                       width: sizeTextureSky
                                      height: sizeTextureSky];
        dr = [[Texture alloc] initStartField: &(droite[0][0])
                                       width: sizeTextureSky
                                      height: sizeTextureSky];
        ga = [[Texture alloc] initStartField: &(gauche[0][0])
                                       width: sizeTextureSky
                                      height: sizeTextureSky];
    }
    NSZoneFree(zone, devant);
    NSZoneFree(zone, derriere);
    NSZoneFree(zone, haut);
    NSZoneFree(zone, bas);
    NSZoneFree(zone, droite);
    NSZoneFree(zone, gauche);
}

- (void) drawSky: (Camera *) cam
{
    int i;
    if( thePref.stars )
    {
        if( isCompile )
        {
            glCallList(idList);
        }
        else
        {
            idList = glGenLists(1);
            NSDebugMLLog(@"gl", @"idList = %u", idList);
            glNewList(idList, GL_COMPILE_AND_EXECUTE);
            
            glDepthMask(GL_TRUE);
            glDepthRange(1, 1);
            glDepthFunc(GL_ALWAYS);
            glEnable(GL_DEPTH_TEST);
            
            
            glDisable(GL_CULL_FACE);
            glEnable(GL_TEXTURE_2D);
            glBindTexture(GL_TEXTURE_2D, [dev idTexture]);
            glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_REPLACE);
            
            //	  glColor3d(1, 1, 1);
            glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex3f(1, -1, -1);
            glTexCoord2f(1, 0);
            glVertex3f(1, 1, -1);
            glTexCoord2f(1, 1);
            glVertex3f(1, 1, 1);
            glTexCoord2f(0, 1);
            glVertex3f(1, -1, 1);
            glEnd();
            
            glBindTexture(GL_TEXTURE_2D, [der idTexture]);
            glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex3f(-1, 1, 1);
            glTexCoord2f(1, 0);
            glVertex3f(-1, -1, 1);
            glTexCoord2f(1, 1);
            glVertex3f(-1, -1, -1);
            glTexCoord2f(0, 1);
            glVertex3f(-1, 1, -1);
            glEnd();
            
            glBindTexture(GL_TEXTURE_2D, [ha idTexture]);
            glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex3f(-1, -1, 1);
            glTexCoord2f(1, 0);
            glVertex3f(1, -1, 1);
            glTexCoord2f(1, 1);
            glVertex3f(1, 1, 1);
            glTexCoord2f(0, 1);
            glVertex3f(-1, 1, 1);
            glEnd();
            
            glBindTexture(GL_TEXTURE_2D, [ba idTexture]);
            glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex3f(1, 1, -1);
            glTexCoord2f(1, 0);
            glVertex3f(-1, 1, -1);
            glTexCoord2f(1, 1);
            glVertex3f(-1, -1, -1);
            glTexCoord2f(0, 1);
            glVertex3f(1, -1, -1);
            glEnd();
            
            glBindTexture(GL_TEXTURE_2D, [ga idTexture]);
            glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex3f(-1, 1, -1);
            glTexCoord2f(1, 0);
            glVertex3f(1, 1, -1);
            glTexCoord2f(1, 1);
            glVertex3f(1, 1, 1);
            glTexCoord2f(0, 1);
            glVertex3f(-1, 1, 1);
            glEnd();
            
            glBindTexture(GL_TEXTURE_2D, [dr idTexture]);
            glBegin(GL_QUADS);
            glTexCoord2f(0, 0);
            glVertex3f(1, -1, 1);
            glTexCoord2f(1, 0);
            glVertex3f(-1, -1, 1);
            glTexCoord2f(1, 1);
            glVertex3f(-1, -1, -1);
            glTexCoord2f(0, 1);
            glVertex3f(1, -1, -1);
            glEnd();
            
            
            glEnable(GL_CULL_FACE);
            glDisable(GL_TEXTURE_2D);
            
            glDepthRange(0.0, 1.0);
            glDepthFunc(GL_LESS);
            glDisable(GL_DEPTH_TEST);
            glEndList();
            isCompile = YES;
        }
    }
    else
    {
        glDepthMask(GL_TRUE);
        glClearColor(0, 0, 0, 1.0);
        glClearDepth(1.0);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glDepthMask(GL_FALSE);
    }
    if( thePref.nameStar && cam->param->label)
    {
        NSSize size = NSMakeSize(cam->param->width, cam->param->height);
        glColor3f(0.6, 0.6, 0.6);
        
        pushStandardRep(size);
        [fontStar selectFont];
        NSDebugMLLogWindow(@"GGSky", @"mat\n%@", stringOfMatrice(&cam->modelView));
        for( i = 0; i < nStars; ++i)
        {
            if( i == wishIndex ) continue;
            if( theStars[i].stringLayout!= nil )
            {
                Vect3D pos = theStars[i].pos;
                Vect3D res;
                if( getWinCoordinates(cam, &pos, &res) )
                {
                    res.x += 5;
                    res.y -= 5;
                    glLoadIdentity();
                    glTranslatef(res.x, res.y, 0);
                    [theStars[i].stringLayout fastDraw];
                    NSDebugMLLogWindow(@"GGSky", @"%@\n  %@", 
                                       stringOfVect(&pos),
                                       stringOfVect(&res));
                }
            }
        }
        [fontStar endSelectFont];
        if( wishIndex >= 0 )
        {
            Vect3D res;
            if( getWinCoordinates(cam, &theStars[wishIndex].pos, &res) )
            {
                res.x += 5;
                res.y -= 5;
                glLoadIdentity();
                glTranslatef(res.x, res.y, 0);
                glColor3f(1, 0, 0);
                [fontStar drawString: 
                    [NSString stringWithUTF8String: 
                        [self cNameOf: wishIndex]]
                            atOrigin: NSZeroPoint];
            }
        }
        
        popStandardRep();
    }
}

- (Star *) stars: (int*)size
{
    if( size != NULL )
        *size = nStars;
    return theStars;
}

- (char *) names
{
    return (char *)[bigString bytes];
}

- (char *) cNameOf:(int) index
{
    NSParameterAssert(0<= index && index < nStars);
    return (char *)[bigString bytes] + theStars[index].index;
}

- (char *) cNameOfStar: (Star *)s
{
    return [self cNameOf: s->indexStar];
}

- (StarsZone *) starsZoneForVect: (Vect3D *)pos
{
    ZoneKey *val = zoneForPos(pos);
    
    return [starsZones objectForKey: val];
}

- (StarsZone *) starsZoneForStarIndex: (int) index
{
    Vect3D v;
    NSParameterAssert(0 <= index && index < nStars);
    initVect(&v, theStars[index].xd, theStars[index].yd,theStars[index].zd);
    return [self starsZoneForVect: &v];
}

- (int) randomStarIndexCloseTo: (int) index
{
    StarsZone *z;
    z = [self starsZoneForStarIndex: index];
    NSParameterAssert(z != nil);
    return [z randomIndexCloseTo: index];
}

- (Vect3D) starPosForIndex: (int) index
{
    Vect3D v;
    NSParameterAssert(0 <= index && index < nStars);
    initVect(&v, theStars[index].xd, theStars[index].yd,theStars[index].zd);
    return v;
}

- (GGReal) distanceWith: (int) i
                  and: (int) j
{
    Vect3D v,w;
    initVect(&v, theStars[i].xd, theStars[i].yd,theStars[i].zd);
    initVect(&w, theStars[j].xd, theStars[j].yd,theStars[j].zd);
    return sqrt(distance2Vect(&v, &w)) / 100.0;
}


@end

@implementation StarsZone
- (void) addIndex: (int) val
{
    if (indices == NULL)
    {
        indices = NSZoneMalloc([self zone], 2*sizeof(int));
        NSAssert(indices != NULL, @"Not enough memory");
        allocated = 2;
    }
    
    if (size >= allocated)
    {
        allocated *= 2;
        indices = NSZoneRealloc([self zone], indices, allocated*sizeof(int));
        NSAssert(indices != NULL, @"Not enough memory");
    }
    
    NSAssert(size < allocated, @"bad");
    
    indices[size++] = val;
}

- (int) randomIndexCloseTo: (int) index
{
    int  chek; 
    int i, indsel = 0;
    Star *ps, cur;
    Vect3D poscur;
    int indclose[size];
    
    ps = [theSky stars: &chek];
    NSParameterAssert(index >= 0 && index < chek);
    
    cur = ps[index];
    initVect(&poscur, cur.xd, cur.yd, cur.zd);
    
    for (i = 0; i < size; ++i)
    {
        Vect3D rem;
        Star dest;
        if(indices[i] == index)
            continue;
        
        dest = ps[indices[i]];
        initVect(&rem, dest.xd, dest.yd, dest.zd);
        
        if( distance2Vect(&poscur, &rem) < SQ(1300) )
        {
            indclose[indsel++] = indices[i];
        }
    }
    
    NSParameterAssert(indsel > 0);
    
    NSDebugMLLog(@"GGSky", @"indsel = %d", indsel);
    
    return indclose[irnd(indsel)];
}


- (id)initWithCoder:(NSCoder *)decoder
{
    void *p;
    unsigned osize;
    p = [decoder decodeBytesWithReturnedLength: &osize];
    if(osize > 0)
    {
        indices = NSZoneMalloc([self zone], osize);
        memcpy(indices, p, osize);
        size = allocated = osize / sizeof(int);
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeBytes: indices
                  length: size*sizeof(int)];
}

- (void) dealloc
{
    if(indices)
        NSZoneFree([self zone], indices);
    [super dealloc];
}

+ starsZone
{
    id obj;
    obj = [self new];
    addToCheck(obj);
    return AUTORELEASE(obj);
}

- description
{
    if( size > 4 )
        return [NSString stringWithFormat: @"(%d, %d, %d, %d, ...)",
            indices[0], indices[1],
            indices[2], indices[3]];
    else
    {
        int i;
        NSMutableString *string;
        
        string = [NSMutableString stringWithString: @"("];
        for(i = 0; i < size-1; ++i)
            [string appendFormat: @"%d, ", indices[i]];
        [string appendFormat: @"%d)", indices[size-1]];
        return string;
    }
}
@end
