//
//  GG3D.m
//  elite
//
//  Created by Frederic De Jaeger on 27/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GG3D.h"

const GGReal EPS=0.00000001;
const Vect3D GGZeroVect = {0.0, 0.0, 0.0};

NSString *stringOfVect(Vect3D *v)
{
    return [NSString stringWithFormat: @"(%g, %g, %g)", 
        v->x, v->y, v->z];
}

NSString *stringOfVect2(Vect3D v)
{
    return stringOfVect(&v);
}

NSString *stringOfMatrice(GGMatrix4 *aMat)
{
    NSMutableString *res = [NSMutableString string];
    pmat mat = (pmat)aMat;
    int i;
    
    for( i = 0; i < 4; ++i)
    {
        [res appendFormat: @"| %+5.5e %+5.5e %+5.5e %+5.5e |\n",
            mat[0][i], mat[1][i], mat[2][i], mat[3][i]];
    }
    return res;
}

NSString* stringOfBox(GGBox box)
{
    return [NSString stringWithFormat:@"[%@, %@]", stringOfVect(&box.min), stringOfVect(&box.max)];
}

void normaliseMatrice(GGMatrix4 *mat)
{
    GGReal tmp;
    normaliseVect(&mat->direction);
    tmp = prodScal(&mat->direction, &mat->haut);
    addLambdaVect(&mat->haut, -tmp, &mat->direction, &mat->haut);
    normaliseVect(&mat->haut);
    
    prodVect(&mat->haut, &mat->direction, &mat->gauche);
}

void GGMatrixLookAt(GGMatrix4 *mat, const Vect3D *lookAtRef){
    diffVect(lookAtRef, &mat->position, &mat->direction);
    normaliseMatrice(mat);
}

void rotateFrameWithLocalAngularSpeed(GGMatrix4 *mat, Vect3D* angularSpeed, GGReal delta)
{
    Vect3D temp;
    
    
    // Direction is moving
    mulScalVect(&mat->gauche, angularSpeed->y, &temp);
    addLambdaVect(&temp, -angularSpeed->x, &mat->haut, &temp);
    addLambdaVect(&mat->direction, delta, &temp, &mat->direction);
    
    // Haut
    
    mulScalVect(&mat->gauche, -angularSpeed->z, &temp);
    addLambdaVect(&temp, angularSpeed->x, &mat->direction, &temp);
    addLambdaVect(&mat->haut, delta, &temp, &mat->haut);
    
    normaliseMatrice(mat);
}

/*   for a matrix M
    compute (v_{min})^i = \Sum_{j, m_{i,j} <= 0} m_{i,j}
    and similarly v_max \in \R^3
    
*/
static void getMinMaxMatrix(const GGMatrix4* matrix, Vect3D* minOut, Vect3D *maxOut)
{
    const pmat mat = (pmat)matrix;
    pvect pmin = (pvect)minOut;
    pvect pmax = (pvect)maxOut;
    for (int i = 0; i < 3; ++i) {
        pmin[i] = pmax[i] = 0.0;
        for (int j = 0; j < 3; ++j) {
            if (mat[j][i] > 0) {
                pmax[i] += mat[j][i];
            }
            else {
                pmin[i] += mat[j][i];
            }
        }
    }
}

GGBox transformeBox(const GGMatrix4* mat, GGBox inbox)
{
    Vect3D tmp;
    transformePoint4D(mat, &inbox.min, &tmp);
    Vect3D diff;
    diffVect(&inbox.max, &inbox.min, &diff);
    Vect3D min, max;
    getMinMaxMatrix(mat, &min, &max);
    GGBox result;
    dualProduct(&min, &diff, &min);
    dualProduct(&max, &diff, &max);
    addVect(&tmp, &min, &result.min);
    addVect(&tmp, &max, &result.max);
    return result;
}