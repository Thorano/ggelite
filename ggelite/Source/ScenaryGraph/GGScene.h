/* 	-*-ObjC-*- */
/*
 *  GGScene.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGScene_h
#define __GGScene_h

#import "GGOrbiteur.h"

@class GGBlock;

@interface GGScene : GGSpaceObject
{
  GGBlock		*b1, *b2, *b3, *b4, *b5, *b6;
  BlockContext		context;
}
- freeze;
@end

@class LocalPlanetBlock;

@interface Planete2 : GGOrbiteur
{
  GGBlock		*b1, *b2, *b3, *b4, *b5, *b6;
  BlockContext		context;
  GGReal          	radius;
  LocalPlanetBlock	*lb;  //not retained
  NSString		*textureName;
}
@end


extern GGScene *theGGScene;

#endif
