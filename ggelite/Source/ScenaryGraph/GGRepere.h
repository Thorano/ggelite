/* 	-*-ObjC-*- */
/*
 *  GGRepere.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGRepere_h
#define __GGRepere_h

#import "GGSpaceObject.h"

@class GGSky;
@class GGOrbiteur;
@class Sched;
@class PlaneteManager;
@class GGDynamicSimulator;

typedef enum __mode_repere
{
  RepereNormal = 0,
  ReperePlanete = 1
}Mode_Repere;

#import "GGNode.h"

@interface GGRepere : GGNode
{
    GGDynamicSimulator*         _dynSimulator;
    double                      _currentCameraLimit;
    Mode_Repere                 modeRepere;
	double						_zmin, _zmax;
@public
        //  FIXME: horrible hack, should be private
    BOOL                        shouldMove;
}
- (void) setZMin:(double)zmin zMax:(double)zMax;
- (Mode_Repere) modeRepere;
- (void) setModeRepere:(Mode_Repere)repere;
- (void) updateOrigine: (GGSpaceObject *) obj;

- (void) recenterFrameOnObjectIfNeeded:(GGSpaceObject*) obj;
@end

#define FacteurEchelle  1.0

#define PosPlaneteOrigine(x) (&((x)->posPlaneteOrigine))
#define VitPlaneteOrigine(x) (&((x)->vitPlaneteOrigine))


#endif
