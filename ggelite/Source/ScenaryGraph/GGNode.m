/*
 *  GGNode.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSArray.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSNotification.h>
#import "GGSpaceObject.h"
#import "GGNode.h"
#import "Tableau.h"
#import "utile.h"
#import "GGOrbiteur.h"
#import "World.h"

@implementation GGSpaceObject (GGNode)
- (void) _destroySons
{
    if( !_flags.isFilsEmpty )
    {
        
        int count = [filsNode count];
        NSAssert(filsNode != nil, @"bad");
        if( count > 0 )
        {
            GGSpaceObject *objs[count];
            int i;
            
            [filsNode getObjects: objs];
            for( i = 0; i < count; ++i){
                [self willRemoveSubNode:objs[i]];
                [objs[i] setNodePere: nil];
            }
            [filsNode removeAllObjects];
            _flags.isFilsEmpty = YES;
        }
    }
}

- (void) removeAllSons
{
    [self _destroySons];
}



- (BOOL) isFilsEmpty
{
    return _flags.isFilsEmpty;
}

- (NSArray *) subNodes
{
    if( filsNode != nil )
        return filsNode;
    else
    {
        NSWarnMLog(@"should not be here : %@", self);
        return [NSArray array];  //we return an empty array
    }
}

- (void) willDrawSons: (Camera *)cam
{
}

- (void) hasDrawnSons: (Camera *)cam
{
}


- (void) ancestorDraw: (Camera *) cam
                 from: (GGSpaceObject *)sons
{
    // update cam to keep track of the transformation matrix
    // This could be avoided by using glGetfv(GL_MODELVIEW) but with
    // harware accelerated devices that do TL, it forces a roundtrip
    // so we do it ourselves
    
    NSDebugMLLogWindow(@"DrawArbre", @"%@ nodePere %@", self, nodePere);
    NSDebugMLLogWindow(@"MatriceAncestor", @"%@\n%@", self, 
                       stringOfMatrice(Matrice(self)));
    
    if( nodePere != nil )
    {
        GGMatrix4 mat;
        Camera camPere;
        glPushMatrix();
        
        // multby the inverse of the matrix of self
        
#ifdef DEBUG
        NSAssert(determinant(Matrice(self)) > 0, 
                 @"bad");
#endif
        
        transposeMatrice(Matrice(self), &mat);
        glMultMatrix(&mat);
        
        produitMatrice(&cam->modelView, &mat, &camPere.modelView);
        camPere.param = cam->param;
        camPere.prev = cam;
#ifdef DEBUG
        camPere.owner = self;
#endif
        
        [nodePere ancestorDraw: &camPere
                          from: self];
        
        glPopMatrix();
    }
    
    [self drawWithCameraNoTransform: cam
                               from: sons];
}

- (void) reparent: (GGSpaceObject *)obj
{
    GGSpaceObject *cousin = [obj nodePere];
    
    
    if( cousin == self )
        return;
    NSDebugMLLog(@"GGNode", @"%@ become father of %@", self, obj);
    NSParameterAssert(cousin != nil);
    {
        GGGalileen gal;
        [cousin galileanTransformationTo: self
                                      in: &gal];
        
        [obj changeLocalCoordinates: &gal];
    }
    [self addSubNode: obj];
}

- (void) reparentFromArray: (NSArray *)array
{
    int n = [array count];
    if( n > 0 )
    {
        GGSpaceObject *elems[n];
        int i;
        [array getObjects: elems];
        
        for( i = 0; i < n; ++i)
            [self reparent: elems[i]];
    }
}

- (void) removeFromSuperNodeAndReparentSons
{
    NSArray *temp;
    // save from reentrance problem
    //FIXME: we can optimize this
    if( !_flags.isFilsEmpty )
    {
        NSAssert(filsNode != nil, @"bad");
        temp = [NSArray arrayWithArray: filsNode];
        [[self nodePere]
	reparentFromArray: temp];
    }
    [self removeFromSuperNode];
}

- (void) removeSubNode: (NSObject<GGNode> *)obj
{
    unsigned index = [filsNode indexOfObjectIdenticalTo: obj];
    NSAssert1(index != NSNotFound, @"obj %@ is not managed by this node", obj);
    NSAssert(filsNode != nil, @"bad");
    
    [self willRemoveSubNode:(GGSpaceObject*)obj];
    NSDebugMLLog(@"GGNode", @"%@ is removed from %@", obj, self);
    [obj setNodePere: nil];
    [filsNode removeObjectAtIndex: index];
    _flags.isFilsEmpty = ([filsNode count] == 0);
}

- (void) addSubNode: (GGSpaceObject*)obj
            atIndex: (unsigned) index
{
    GGSpaceObject *oldFather = [obj nodePere];
    if( filsNode == nil )
        filsNode = [[NSMutableArray allocWithZone: [self zone]] init];
    
    NSAssert2(oldFather != self, @"oldFather == self for %@ and self=%@ !", obj,
              self);
    [filsNode insertObject: obj
                   atIndex: index ];
    NSDebugMLLog(@"GGNode", @"%@ is added to %@", obj, self);
    if( oldFather != nil )
        [oldFather removeSubNode: obj];
    [obj setNodePere: self];
    [[NSNotificationCenter defaultCenter]
    addObserver: self
       selector: @selector(removeSubNodeWithNotification:)
           name: GGNotificationRemoveFromCurrent
         object: obj];
    //    [obj enregistreForRemoving: self 
    //         with:@selector(removeSubNodeWithNotification:)];
    
    _flags.isFilsEmpty = NO;
    [self didAddSubNode:(GGSpaceObject*)obj];    
}

- (void) addSubNode: (GGSpaceObject*)obj;
{
    if( _flags.isFilsEmpty )
        [self addSubNode: obj 
                 atIndex: 0];
    else
        [self addSubNode: obj 
                 atIndex: [filsNode count]];
}



- (void) addSubNodeFromArray: (NSArray *)array
{
    int n = [array count];
    if(n > 0)
    {
        GGSpaceObject *elems[n];
        int i;
        [array getObjects: elems];
        for(i = 0; i < n; ++i)
            [self addSubNode: elems[i]];
    }
}

- (void) removeSubNodeWithNotification: (NSNotification *)aNot
{
    GGSpaceObject *obj;
    if( !_flags.isFilsEmpty )
    {
        obj = (GGSpaceObject *)[aNot object];
        if( [filsNode indexOfObjectIdenticalTo: obj] != NSNotFound )
            [self removeSubNode: obj];
    }
}

- (void)willRemoveSubNode:(GGSpaceObject*)aNode
{
    
}

- (void)didAddSubNode:(GGSpaceObject*)aNode
{
    
}

- (void) galileanTransformationNodeWith: (GGGalileen *)pgal
{
    GGGalileen tmp;
    if( !_flags.isFilsEmpty )
    {
        GGGalileen gal;
        transposeGalileen(pgal, &gal);
        
        [filsNode makeObjectsPerformSelector: @selector(changeLocalCoordinates:)
                                  withObject: (id)&gal];
    }
    produitGalileen(Galileen(self), pgal, &tmp);
    
    *Galileen(self) = tmp;
}

- (void) translateNodeTo: (GGSpaceObject *) obj
{
    Vect3D i = {1, 0, 0};
    Vect3D j = {0, 1, 0};
    Vect3D k = {0, 0, 1};
    GGGalileen gal0, tmp;
    
    //  SPEEDUP:
    
    [obj galileanTransformationTo: self
                               in: &gal0];
    
    gal0.matrice.gauche = i;
    gal0.matrice.haut = j;
    gal0.matrice.direction = k;
    
    
    if( !_flags.isFilsEmpty )
    {
        GGGalileen gal;
        transposeGalileen(&gal0, &gal);
        
        [filsNode makeObjectsPerformSelector: @selector(changeLocalCoordinates:)
                                  withObject: (id)&gal];
    }
    produitGalileen(Galileen(self), &gal0, &tmp);
    
    *Galileen(self) = tmp;
}  

- (void) decaleNodeTo: (GGSpaceObject *)obj
    /*without moving the children globally...
    */
{
    GGGalileen gal0, tmp;
    
    [obj galileanTransformationTo: self
                               in: &gal0];
    
    if( !_flags.isFilsEmpty )
    {
        GGGalileen gal;
        transposeGalileen(&gal0, &gal);
        //        [self galileanTransformationTo: obj
        //  	    in: &gal];
        [filsNode makeObjectsPerformSelector: @selector(changeLocalCoordinates:)
                                  withObject: (id)&gal];
    }
    produitGalileen(Galileen(self), &gal0, &tmp);
    
    *Galileen(self) = tmp;
}

- (void) rectifyAxes
    /*change the frame in order to have a zero rotation component.  So that, we
    are a pure translation frame.
    If we have any sons, we correct them because we don't want that the sons
    move globally.
    */
{
    Vect3D i = {1, 0, 0};
    Vect3D j = {0, 1, 0};
    Vect3D k = {0, 0, 1};
    if( !_flags.isFilsEmpty )
    {
        GGGalileen gal = *Galileen(self);
        SetZeroVect(gal.matrice.position);
        SetZeroVect(gal.speed);
        [filsNode makeObjectsPerformSelector: @selector(changeLocalCoordinates:)
                                  withObject: (id)&gal];
        
    }
    Matrice(self)->gauche = i;
    Matrice(self)->haut = j;
    Matrice(self)->direction = k;
    
}

- (GGSpaceObject *) nextObject
{
    if( nodePere )
        return [nodePere nextObjectFrom: self];
    else
        return nil;
}

- (GGSpaceObject *)selfObject
{
    return self;
}

- (GGSpaceObject *) firstObject
{
    if( ! _flags.isFilsEmpty )
        return [(GGSpaceObject *)[filsNode objectAtIndex: 0] firstObject];
    else
        return [self selfObject];
}

- (GGSpaceObject *) nextObjectFrom: (GGSpaceObject *)obj
{
    if( !_flags.isFilsEmpty )
    {
        int count = [filsNode count];
        unsigned index = [filsNode indexOfObjectIdenticalTo: obj];
        
        NSAssert(obj != nil, @"bad");
        
        if( index == NSNotFound )
        {
            NSAssert(obj == nodePere, @"bad");
            if( count > 0 )
                return [(GGSpaceObject *)[filsNode objectAtIndex: 0]
				     nextObjectFrom: self];
            else
                return [self selfObject];
        }
        
        if( index < count - 1 )
            return [(GGSpaceObject *)[filsNode objectAtIndex: index+1]
				 nextObjectFrom: self];
        else
            return [self selfObject];
    }
    else
        return [self selfObject];
}

- (BOOL) isInNeighboorhood: (GGSpaceObject *) obj
{
    return NO;
}


@end

@implementation GGNode
- init
{
    [super init];
    [self setVisible: YES];
    return self;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

- (id)replacementObjectForArchiver:(NSArchiver *)anArchiver
{
    NSAssert1(_originOrbiter != nil, @"_originOrbiter of %@ is nil", self);
    
    return [GGArchiver ggArchiver: [self signature]  ];
}

- (BOOL) saveChannel
{
    return NO;
}

- (BOOL) shouldSave
{
    return NO;
}

- (void) encodeWithCoder: (NSCoder *) encoder
{
}

- (void) rasterise: (Camera *)cam
{
    NSAssert(0, @"bad");
}

- (void) setOriginOrbiteur: (GGOrbiteur *)orb
{
    _originOrbiter = orb;
}

- (GGOrbiteur*) originalOrbiteur
{
    return _originOrbiter;
}

- (BOOL) isInNeighboorhood: (GGSpaceObject *) obj
{
    if( _originOrbiter )
    {
        GGReal ts = [_originOrbiter tailleSysteme];
        if( distanceAbsolu2(self, obj) < ts*ts )
            return YES;
    }
    return NO;
}

- (NSString*) signature
{
    assert(_originOrbiter != nil);
    NSAssert(_originOrbiter != nil, @"bad");
    return [NSString  stringWithFormat: @"rep:%@", [_originOrbiter name]];
}

- (GGSpaceObject *)nextObject
{
    NSAssert(0, @"bad");
    return nil;
}

- (GGSpaceObject *)selfObject
{
    return [super nextObject];
}

- (GGSpaceObject *) firstObject
{
    GGSpaceObject *res;
    res = [super firstObject];
    if( res == self)
        return [super nextObject];
    else
        return res;
}

@end
