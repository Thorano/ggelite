/* 	-*-ObjC-*- */
/*
 *  Clouds.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: August 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Clouds_h
#define __Clouds_h

#import "GGShip.h"

GGBEGINDEC
@class Cloud;

@interface Tunnel : NSObject <NSCoding>
{
  Tunnel	*next;	
  Vect3D 	posd;
  Cloud		*c_source, *c_dest;
@public
  int 		source, destination;
  float		timeLength;
  double	startDate;
  Vect3D	poss;
@private
  BOOL		init;
}
- (Vect3D) destinationPointInSystem: (System *)s;
- (void) setNextTunnel: (Tunnel *)t;
- (void) setStartCloud: (Cloud *)c;
- (void) setDestCloud: (Cloud *)c;
- (double) departureDate;
- (double) arrivalDate;
- (Cloud *) departureCloud;
- (Cloud *) arrivalCloud;

+ tunnelWithSource: (int) s
       destination: (int) d
	timeLength: (float) val
	     start: (double) st;
@end
GGENDDEC

@interface Cloud : GGMobile
{
  Tunnel *tunnel;
  GGReal	dim;
}
+ cloudFrom: (GGShip *) ship
     tunnel: (Tunnel *) t;
- (Tunnel *) tunnel;
@end

#endif
