/* 	-*-ObjC-*- */
/*
 *  GGNode.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGNode_h
#define __GGNode_h


#import "GGSpaceObject.h"

@protocol GGSpaceObjectDelegate
- (void) willDrawSonsOfNode:(GGSpaceObject*) node
				 withCamera:(Camera*)camera;
- (void) didDrawSonsOfNode:(GGSpaceObject*) node
				withCamera:(Camera*)camera;
@end


@interface GGSpaceObject (GGNode)
- (NSArray *) subNodes;
- (void) addSubNode: (GGSpaceObject*)obj
            atIndex: (unsigned) index;
- (void)willRemoveSubNode:(GGSpaceObject*)aNode;
- (void)didAddSubNode:(GGSpaceObject*)aNode;
- (void) addSubNode: (GGSpaceObject*)obj;
- (void) addSubNodeFromArray: (NSArray *)array;
- (void) removeSubNode: (NSObject<GGNode> *)obj;
- (void) removeAllSons;
- (BOOL) isFilsEmpty;

//move the origin of the node by pgal without moving the children globaly
- (void) galileanTransformationNodeWith: (GGGalileen *)pgal;

// same as galileanTransformationNodeWith but compute pgal to bring the node
// to the current position of obj.  That is, after this call, obj will be
// located exaclty at the origin of the node with the same axis.
- (void) decaleNodeTo: (GGSpaceObject *)obj;

// same as decaleNodeTo but only translate the node.  don't touch the axes.
- (void) translateNodeTo: (GGSpaceObject *) obj;


  /*change the frame in order to have a zero rotation component.  So that, we
    are a pure translation frame.
    If we have any sons, we correct them because we don't want that the sons
    move globally.
  */
- (void) rectifyAxes;


- (void) reparent: (GGSpaceObject *)obj;
- (void) reparentFromArray: (NSArray *)obj;
- (void) removeFromSuperNodeAndReparentSons;

- (void) ancestorDraw: (Camera *) cam
		 from: (GGSpaceObject *)sons;
- (void) willDrawSons: (Camera *)cam;
- (void) hasDrawnSons: (Camera *)cam;

- (GGSpaceObject *) nextObjectFrom: (GGSpaceObject *)obj;
- (GGSpaceObject *) nextObject;
- (GGSpaceObject *) firstObject;

- (BOOL) isInNeighboorhood: (GGSpaceObject *)sons;

@end

GGBEGINDEC

/*%%%
import GGSpaceObject;
%%%*/

@class GGOrbiteur;
@interface GGNode : GGSpaceObject
{
  GGOrbiteur			*_originOrbiter; //not retained
}
- (void) setOriginOrbiteur: (GGOrbiteur *)orb;
- (GGOrbiteur*) originalOrbiteur;

@end

GGENDDEC

#endif
