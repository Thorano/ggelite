/* 	-*-ObjC-*- */
/*
 *  Clouds.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: August 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSCoder.h>
#import <Foundation/NSException.h>
#import "World.h"
#import "Sched.h"
#import "GGSpaceObject.h"
#import "Clouds.h"
#import "Metaobj.h"
#import "System.h"
#import "Tableau.h"
#import "Preference.h"
#import "Resource.h"
#import "GGArchiverExtension.h"



@implementation Tunnel
- initWithSource: (int) s
     destination: (int) d
      timeLength: (float) val
	   start: (double) t
{
  [super init];
  source = s;
  destination = d;
  init = NO;
  timeLength = val;
  startDate = t;

  //I hope I won't be deallocated before the timer fires.  Normally it could
  //not happen.  Because, only the firing of the timer could destroy me...
  [GGTimer scheduledTimerWithTimeInterval: timeLength*1.1
	   target: self
	   selector: @selector(invalidate:)
	   repeats: NO];
  
  addToCheck(self);
  return self;
}

- (void) dealloc
{
  RELEASE(next);
  RELEASE(c_source);
  RELEASE(c_dest);
  [super dealloc];
}

- (void) invalidate: (GGTimer *)t
{
  [c_source destroy];
  [c_dest destroy];
  NSDebugMLLog(@"Tunnel", @"Timer expired!");
  [theWorld destroyTunnel: self];
}

+ tunnelWithSource: (int) s
       destination: (int) d
	timeLength: (float) val
	     start: (double) ti

{
  Tunnel *t = AUTORELEASE([self alloc]);
  return     [t initWithSource: s
		destination: d	
		timeLength: val
		start: ti];
}

- (double) departureDate
{
  return startDate;
}

- (double) arrivalDate
{
  return startDate + timeLength;
}

- (Cloud *) departureCloud
{
  return c_source;
}

- (Cloud *) arrivalCloud
{
  return c_dest;
}

- (void) setNextTunnel: (Tunnel *)t
{
  ASSIGN(next,t);
}

- (void) setStartCloud: (Cloud *)c
{
  ASSIGN(c_source, c);
}

- (void) setDestCloud: (Cloud *)c
{
  ASSIGN(c_dest, c);
}

- (Vect3D) destinationPointInSystem: (System *)s
{
  if(!init)
    {
      if (next)
	{
	  posd = [next destinationPointInSystem: s];
	  posd.x += 1e5;
	}
      else
	{
	  posd = [s randomVect];
	}
      NSParameterAssert(s == [theWorld currentSystem]);
      if( c_dest != nil )
	{
	  *Point(c_dest) = posd;
	  /*we don't add the ship to the graph because it will be done
	    when the archiver of the currentsystem will finalize
	  */
	  [[s  archiver] addShip: c_dest];
	}
      init = YES;
    }
  return posd;
}

- initWithCoder: (NSCoder *) decoder
{
  GGDecode(source);
  GGDecode(destination);
  GGDecode(poss);
  GGDecode(posd);
  GGDecode(c_source);
  GGDecode(c_dest);
  GGDecode(init);
  GGDecode(next);
  GGDecode(timeLength);
  GGDecode(startDate);
  
  addToCheck(self);
  return self;
}

- (void) encodeWithCoder: (NSCoder *)encoder
{
  GGEncode(source);
  GGEncode(destination);
  GGEncode(poss);
  GGEncode(posd);
  GGEncode(c_source);
  GGEncode(c_dest);
  GGEncode(init);
  GGEncode(next);
  GGEncode(timeLength);
  GGEncode(startDate);
}


- (BOOL) shouldSave
{
  return YES;
}

- (BOOL) saveChannel
{
  return YES;
}

- (NSString*) description
{
  return [NSString stringWithFormat: @"tunnel(%@ -> %@)",
		   [theWorld starNameForIndex: source],
		   [theWorld starNameForIndex: destination]];
}
@end

@implementation Cloud
static GLint idList;

+ (void) initialize
{
  float data [7][15] = {
    { -0.514562, -1.058253,  0,
      -0.368931, -1.058253,  0.242718,
      0.031416, -1.058253,  0.208538,
      0.466020, -1.058253,  0.271845,
      0.621360, -1.058253,  0},

    { -0.613834,  0.000000,  0,
      -0.417274,  0.000000,  0.251426,
      0.001080,  0.000000,  0.230549,
      0.485236,  0.000000,  0.284181,
      0.663587,  0.000000,  0},

    { -0.433567,  0.495145,  0,
      -0.298888,  0.495145,  0.189008,
      0.002066,  0.495145,  0.273078,
      0.318306,  0.495145,  0.211936,
      0.460628,  0.495145,  0},

    { -0.499656,  0.834952,  0,
      -0.388349 , 0.834952,  0.242718,
      -0.009709,  0.836939,  0.331630,
      0.398058,  0.834952,  0.271845,
      0.527494,  0.834952,  0},

    { -0.563106,  1.140699,  0.166905,
      -0.427184 , 1.135922,  0.398058,
      -0.009708,  1.135922,  0.349515,
      0.407767,  1.135922,  0.427185,
      0.572816,  1.141057,  0.171276},

    { -0.526243,  1.407767,  0,
      -0.322436,  1.374663,  0.236938,
      -0.015606,  1.392720,  0.244965,
      0.313696,  1.383692,  0.263655,
      0.515733,  1.407767,  0},

    { -0.218060,  1.572816,  0,
      -0.213906,  1.539712,  0.113569,
      -0.022843,  1.506608,  0.217449,
      0.172062,  1.539712,  0.123843,
      0.182650,  1.572816,  0}};

  if(self == [Cloud class] )
    {
      float bla[] =
	{0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1};
	//	{0, 0.1, 0.2, 0.3, 0.4, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	// 	{0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1};
      float blu [] = 
	//{0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1};
	{0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
      //      GLfloat knots[8] = {0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0};

      GLUnurbsObj *nurbs;
      nurbs = gluNewNurbsRenderer();
      
      gluNurbsProperty(nurbs, GLU_SAMPLING_METHOD, GLU_DOMAIN_DISTANCE);
      gluNurbsProperty(nurbs, GLU_DISPLAY_MODE, GLU_OUTLINE_POLYGON);
      idList = glGenLists(1);
      glNewList(idList, GL_COMPILE);      
      glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

      glEnable(GL_AUTO_NORMAL);
      //      glDisable(GL_CULL_FACE);
      glEnable(GL_RESCALE_NORMAL);
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);


      glScalef(100, 100, 100);
      glColor3f(0.7,0.7,1);

      gluBeginSurface(nurbs);
      gluNurbsSurface(nurbs, 
		      8, //nknots in s
		      bla,
		      10,
		      blu,
		      3,
		      15,
		      &data[0][0],
		      4,4, GL_MAP2_VERTEX_3);
//       gluNurbsSurface(nurbs, 
// 		      8, //nknots in s
// 		      knots,
// 		      8,
// 		      knots,
// 		      3,
// 		      15,
// 		      &data[0][0],
// 		      4,4, GL_MAP2_VERTEX_3);
		      
      gluEndSurface(nurbs);
      glPopAttrib();

      glEndList();
      gluDeleteNurbsRenderer(nurbs);
      [[Resource resourceManager] addForCleaning: self];
    }
}

+ (void) clean
{
  glDeleteLists(idList, 1);
}


- initWithShip: (GGShip *)ship
	tunnel: (Tunnel *) t;
{
  [super init];
  tunnel = t;
  dim = Dimension(ship);
  _flags.graphical = YES;
  collisionCounter = -1;
  Masse(self) = 101;
  Dimension(self) = Dimension(ship);

  return self;
}

- (void) encodeWithCoder: (NSCoder *)encoder
{
  [super encodeWithCoder: encoder];
  GGEncode(dim);
  GGEncode(tunnel);
}

-  initWithCoder: (NSCoder *) decoder
{
  [super initWithCoder:  decoder];
  GGDecode(dim);
  GGDecodeObject(tunnel);
  
  return self;
}


+ cloudFrom: (GGShip *) ship
     tunnel: (Tunnel *) t
{
  Cloud *cloud;
  cloud = [self alloc];
  [cloud initWithShip: ship
	 tunnel: t];
  [cloud setName: @"Hyperspatial Cloud"];
  [cloud setVisible: YES];
  return AUTORELEASE(cloud);
}

- (Tunnel *) tunnel
{
  return tunnel;
}

- (void) dealloc
{
  NSDebugMLLog(@"Cloud", @"Destruction");
  [super dealloc];
}

- (BOOL) shouldCache
{
  return YES;
  //  return NO;
}

- (BOOL) shouldBeClipped
{
  return NO;
}

- (BOOL) shouldDrawMoreThings
{
  return NO;
}

- (void) rasterise
{
  double mytime;
  mytime = gameTime - 1000*floor(gameTime/1000.0);
  NSDebugMLLogWindow(@"Cloud", @"on dessine gametime %g", gameTime);
  glColor3f(0.4,0.8,0.7);
  glPushMatrix();
//   glRotated(50*mytime, sin(mytime), sin(1.3*mytime),
// 	    sin(1.5*mytime));
  glCallList(idList);
  glPopMatrix();

}


// - (void) rasterise
// {
//   double mytime;
//   mytime = gameTime - 1000*floor(gameTime/1000.0);
//   NSDebugMLLogWindow(@"Cloud", @"on dessine gametime %g", gameTime);
//   glColor3f(0.4,0.8,0.7);

//   glPushAttrib(GL_ENABLE_BIT);
//   glDisable(GL_LIGHTING);
//   glDisable(GL_DEPTH_TEST);
//   //  glDisable(GL_CULL_FACE);
//   if( thePref.GLBlend )
//     glEnable(GL_BLEND);
//   {
//     int i;
// #define SIZE 1000    
//     struct { GLubyte r,g,b,a; GLfloat x,y,z;} data[SIZE];
//     for( i = 0; i < SIZE; ++i)
//       {
// 	double t;
// 	data[i].r = 200;
// 	data[i].g = 200;
// 	data[i].b = 90;
// 	data[i].a = 	  255*(float)i/(float)1000;
	

// 	t =  (double)i / 1000.0 + mytime;
// 	t = 10*t;

// 	data[i].x = 3*dim*sin(t);
// 	data[i].y = 3*dim*sin(2.145914*t);
// 	data[i].z = 10*dim*sin(t*10.0);
//       }

//     glPushMatrix();
//     glRotated(500*mytime, sin(mytime), sin(1.3*mytime),
// 	      sin(1.5*mytime));
//     glInterleavedArrays(GL_C4UB_V3F, 0, data);
//     glDrawArrays(GL_LINE_STRIP, 0, SIZE);
//     glPopMatrix();
//   }
  
//   glPopAttrib();
// }
@end
