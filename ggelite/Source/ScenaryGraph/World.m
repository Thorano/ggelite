/*
 *  World.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <math.h>
#import <time.h>


#import <Foundation/NSObject.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSValue.h>
#import <Foundation/NSScanner.h>
#import <Foundation/NSData.h>
#import <Foundation/NSInvocation.h>

#import "Metaobj.h"
#import "GGLoop.h"
#import "GGSpaceObject.h"
#import "GGInput.h"
#import "GGSky.h"
#import "GGRepere.h"
#import "World.h"
#import "utile.h"
#import "Tableau.h"
#import "GGOrbiteur.h"
#import "Preference.h"
#import "Sched.h"
#import "Explosion.h"
#import "Commandant.h"
#import "Laser.h"
#import "Flare.h"
#import "GGGlyphLayout.h"
#import "Console.h"
#import "Item.h"
#import "System.h"
#import "Resource.h"
#import "GGConcreteShip.h"
#import "GGRunTime.h"
#import "Clouds.h"
#import "Station.h"
#import "stationadmin.h"



World *theWorld=nil;
NSString *GGNotificationLeavingSystem = @"GGNotificationLeavingSystem";
NSString *GGNotificationEnteringSystem = @"GGNotificationEnteringSystem";
NSString *GGNotificationBeforeLoadingGame = @"GGNotificationBeforeLoadingGame";
NSString *GGNotificationAfterLoadingGame = @"GGNotificationAfterLoadingGame";

@interface WorldState : NSObject <NSCoding> {
    @public
    Commandant                *commandant;
    
    //where ship on others system goes :
    NSMutableDictionary		*otherSystemState;  
    NSMutableDictionary		*tunnels;
    NSMutableDictionary           *stationManagerDico;
    
    NSMutableArray		*hyperShips;
    NSMutableArray		*lasers;
    Vect3D			galacticPosition;
    BOOL				managingLaser;
}
@end

@implementation WorldState
- (id) init
{
    self = [super init];
    if(self){
        lasers = [NSMutableArray new];
        otherSystemState = [NSMutableDictionary new];
        hyperShips = [NSMutableArray new];
        tunnels = [NSMutableDictionary new];
        stationManagerDico = [NSMutableDictionary new];
        
        commandant = nil;
        galacticPosition = GGZeroVect;
    }
    return self;
}

- (void) dealloc {
    RELEASE(commandant);
    RELEASE(otherSystemState);
    RELEASE(hyperShips);
    RELEASE(lasers);
    RELEASE(tunnels);
    RELEASE(stationManagerDico);

    
    [super dealloc];
}

- (void) encodeWithCoder: (NSCoder *) encoder
{
    GGEncode(commandant);
    GGEncode(otherSystemState);
    GGEncode(tunnels);
    GGEncode(stationManagerDico);
    GGEncode(hyperShips);
    GGEncode(lasers);
    GGEncode(galacticPosition);
    GGEncode(managingLaser);
}

-  initWithCoder: (NSCoder *) decoder
{
    GGDecode(commandant);
    GGDecode(otherSystemState);
    GGDecode(tunnels);
    GGDecode(stationManagerDico);
    GGDecode(hyperShips);
    GGDecode(lasers);
    GGDecode(galacticPosition);
    GGDecode(managingLaser);
    
    return self;
}
@end

@interface WorldArchiver : NSObject <NSCoding>
{
    @public
    int             indexSystem;
    WorldState      *state;
    GGLoopState     *loopstate;
    NSString 		*nameLoc;
    GGGalileen		galLoc;
    BOOL			superLocal;
    SystemArchiver	*currentArchiver;
}
+ worldArchiverWithWorld:(World*)world;
- (WorldState *) state;
@end

@interface World (PrivateMethod)
- (BOOL) makeFatherCurrentOrbiteur;
- (void) checkClosestOrbiteur;
- (void) unloadSystem;
- (void) _setCurrentSystemArchiver: (SystemArchiver *)sa;
- getObjectAtSystemIndex: (int) index
                    name: (NSString *)name;
- (GGNode *) nodeForOrbiteur: (GGOrbiteur *)orb;
- objectForName: (NSString *) str;
- (void) setWorldArchiver: (WorldArchiver *)  _wa;
- (void) loadSystemAtIndex: (int) index
                  archiver: (SystemArchiver *)archiver;
- (void) _anOrbiteurBeingOriginOfLocalOldClosest: obj;

- (void) makeSonCurrentOrbiteur: (GGOrbiteur *)son;
- (void) initCommandant;
- (WorldState *) state;
- (SystemArchiver *) systemArchiverForIndex: (int) index;
- (SystemArchiver *) buildSystemArchiverForIndex: (int) index;
@end


@implementation WorldArchiver
- (id) initWithWorld:(World*)world
{
    self = [super init];
    if(self){
        state = RETAIN([world state]);
        indexSystem = [world currentSystemIndex];
        loopstate = RETAIN([theLoop state]);
        nameLoc = RETAIN([[[world closestOrbiteur] subSystemFrame] signature]);
        galLoc = *Galileen([world repere]);
        superLocal = ([[world repere] modeRepere] == ReperePlanete);
        currentArchiver = RETAIN([[world currentSystem] archiver]);
    }
    
    return self;
}
+ worldArchiverWithWorld:(World*)world
{
    WorldArchiver *wa;
    
    wa = [[self alloc] initWithWorld:world];;
    return AUTORELEASE(wa);
}

- (void) dealloc
{
    RELEASE(state);
    RELEASE(nameLoc);
    RELEASE(loopstate);
    RELEASE(currentArchiver);
    [super dealloc];
}

- (void) encodeWithCoder: (NSCoder *) encoder
{
    GGEncode(indexSystem);
    GGEncode(gameTime);
    GGEncode(superLocal);
    GGEncode(nameLoc);
    GGEncode(galLoc);
    GGEncode(currentArchiver);
    GGEncode(state);
    GGEncode(loopstate);
    [encoder encodeObject:[MessageManager new] forKey:@"MessageManager"];
    [encoder encodeObject:[ProcessManager processManager] forKey:@"ProcessManager"];
}

/*Here we have a big problem.  Because, we need to decode the archiver for the
current system.  Doing so, it may be possible that we need to build the 
current system itself. (for instance, if the archiver contains ship, when 
                        those want to be decoded, they will need an ambient system. ) but then, 
how can we build the current system if we haven't build its archiver yet ?
The solution, we provide nil for the archiver when we build the current
system, and later, we set its archiver when it has been build
*/

-  initWithCoder: (NSCoder *) decoder
{
    [theWorld setWorldArchiver: self];
    GGDecode(indexSystem);
    GGDecode(gameTime);
    GGDecode(superLocal);
    GGDecode(nameLoc);
    GGDecode(galLoc);
    GGDecode(currentArchiver);
    [theWorld _setCurrentSystemArchiver: currentArchiver];
    GGDecode(state);
    GGDecode(loopstate);
    [decoder decodeObjectForKey:@"MessageManager"];
    [decoder decodeObjectForKey:@"ProcessManager"];
    
    //  NSLog(@"Message restoring : %@", [MessageManager class]);
    
    return self;
}

- (WorldState*) state
{
    return state;
}
@end


@implementation World
- init
{
    if (theWorld)
    {
        NSWarnMLog(@"world allocated more than once...");
        [super dealloc];
        return RETAIN( theWorld );
    }
    [super init];
    state = [WorldState new];
    [self setManager: self];
    
    needToDie = [NSMutableArray new];
    //  ships = [NSMutableArray new];
    flares = [Flare new];
    planeteArray = nil;
    isInit = 1;
    isPaused = NO;
    lightPosition.w = 0;
    checkCollision = YES;
    drawWorld = YES;
    
    theWorld = self;
    
    addToCheck(self);
    
    leRepereLocal = [GGRepere new];
    [leRepereLocal setDefaultName: @"repere local"];
    [self addSubNode: leRepereLocal];
    [leRepereLocal beginSchedule];
    
    [theLoop slowSchedule: self
             withSelector: @selector(checkClosestOrbiteur)
                      arg: nil];
    
    theSky = [[GGSky alloc] initAtOrigin: GGZeroVect];
    [self updateProjection];
    
    [[NSNotificationCenter defaultCenter]
    addObserver: self
       selector: @selector(loadingAGame:)
           name: GGNotificationBeforeLoadingGame
         object: nil];
    
    return self;
}

- (void) encodeWithCoder: (NSCoder *) encoder
{
}

- (id)replacementObjectForArchiver:(NSArchiver *)anArchiver
{
    NSAssert(_originOrbiter != nil, @"bad");
    
    return [GGArchiver ggArchiver: @"theWorld" ];
}

- (BOOL) shouldSave
{
    return YES;
}

- (BOOL) saveChannel
{
    return YES;
}

- (void) loadingAGame: obj
{
    checkCollision = YES;
}

- (id)archiver:(NSKeyedArchiver *)archiver willEncodeObject:(id)object
{
    NSLog(@"will encoding: %@ (%@)", object, [object class]);
    return object;
}


- (void)archiver:(NSKeyedArchiver *)archiver didEncodeObject:(id)object
{
    NSLog(@"did encoding: %@ (%@)", object, [object class]);
}




- (NSData *) saveState
{
    WorldArchiver *wa2;
    [starSytem saveState];
    wa2 = [WorldArchiver worldArchiverWithWorld:self];
    NSMutableData* data = [NSMutableData data];
    NSKeyedArchiver* archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
//    [archiver setDelegate:self];
    [archiver encodeObject:wa2 forKey:@"WorldState"];
    [archiver finishEncoding];
    [archiver release];
    return data;
}

- (void) unloadState
{
    id bla = state->commandant;
    RETAIN(bla);
    [state->commandant reallyDestroy];
    RELEASE(bla);
    
    [self unloadSystem];
    DESTROY(state);
    
    //  blabladestroy;
    
    DESTROY(mainCamera);
    DESTROY(auxiliaryCamera);
    
    theClosest = nil;
    DESTROY(theLightSource);
    DESTROY(starSytem);
}

- (void) loadState: (NSData *) data
{
    WorldArchiver *wa2;
    [self unloadState];
    wa = nil;
    indexSystem = -2;
    NSKeyedUnarchiver* unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    wa2 = [unarchiver decodeObjectForKey:@"WorldState"];
    [unarchiver release];
    NSAssert(wa2 != nil, @"Unable to restore saved states");
    NSAssert(wa2 == wa, @"bad");
    
    
    state = [[wa state] retain];
    NSAssert(state->commandant, @"should have load the commandant");
    
    [self initCommandant];
    
    //  NSLog(@"%@", wa->loopstate.slowScheduler);
    //  NSLog(@"%@", wa->loopstate.taskSched);
    [theLoop setState: wa->loopstate];
    [theLoop setTimeSpeed: 10];
    [theLoop setTimeSpeed: 1];
    [starSytem postLoadingSystem];
}

- (void) saveGame: (NSString *) fileName
{
    NSData *data;
    NSParameterAssert(fileName);
    if( !state->commandant )
    {
        consoleLog(@"You are dead, there is nothing to save");
        return;
    }
    data = [self saveState];
    
    [data writeToFile: fileName
           atomically: NO];
    NSLog(@"saved, size = %d", [data length]);
}

- (void) loadGame: (NSString *) fileName
{
    NSData *data;
    data = [NSData dataWithContentsOfFile: fileName];
    if( data == nil )
    {
        consoleLog(@"cannot open saved game %@", fileName);
        return;
    }
    
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationBeforeLoadingGame
                  object: nil];
    [self loadState: data];
    
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationAfterLoadingGame
                  object: nil];
    
    
    //  NSLog(@"tree:\n%@", [self displayObject: 0]);
}


- (void) updateProjection
{
    NSRect bounds = [[theGGInput contentView] bounds];
    projectionParam = GGSetUpProjection(bounds, 0.1, 10000, Focal);
}

- (GGSpaceObject *)ancestor
{
    return self;
}

- (void) drawSky: (Camera *)cam
{
    GGMatrix4 fuck;
    Camera skyCam;
    
    initWithIdentityMatrix(&fuck);
    fuck.gauche.x = -1;
    fuck.direction.z = -1;
    skyCam.modelView = cam->modelView;
    SetZeroVect(skyCam.modelView.position);
    glPushMatrix();
    glLoadMatrix(&fuck);
    glMultMatrix(&skyCam.modelView);
    
    skyCam.param = cam->param;
    
    [theSky drawSky: &skyCam];
    glPopMatrix();
    
    //  debugOrbiteurWindow(@"Terre");
}

- (void) willDrawSons: (Camera *)cam
{
    glDisable(GL_LIGHTING);
    [self drawSky: cam];
    
    glDepthMask(GL_TRUE);
    glEnable(GL_DEPTH_TEST);
    glDepthRange(0.95, 0.95);
    glDepthFunc(GL_ALWAYS);
    
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, (GGReal*)&lightPosition);
    
    /*nécessaire pour avoir une ilumination correcte des objets proches.
        */
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,   (int) thePref.LightLocal);
    
    
    if( thePref.GLSmooth )
        glShadeModel(GL_SMOOTH);
    else
        glShadeModel(GL_FLAT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(cam->param->fovy,cam->param->ratio,1,1e20);
    glMatrixMode(GL_MODELVIEW);
}

- (GGRepere *)repere
{
    return leRepereLocal;
}

- (GGOrbiteur *) closestOrbiteur
{
    return theClosest;
}

- (void) dealloc
{
    theWorld = nil;
    [state->commandant invalidate];
    [self unloadSystem];
    [theLoop unSchedule: self];
    RELEASE(state);
    RELEASE(flares);
    RELEASE(theLightSource);
    RELEASE(needToDie);
    RELEASE(planeteArray);
    RELEASE(starSytem);
    RELEASE(mainCamera);
    RELEASE(auxiliaryCamera);
    RELEASE(leRepereLocal);
    
    RELEASE(theSky);
    
    [super dealloc];
}

#pragma mark -
- (void) loadBaseSystem
{
    NSAutoreleasePool *pool;    
    pool = [NSAutoreleasePool new];
    {
        //      Vect3D posObs = { -150e9+50e6, 0, -1e11};
        Vect3D posObs = { -150e9+50e6, 0, 0};  // close to earth
//        Vect3D posObs = { 50e9, -100e9, -100e9};  // suitable for a nice light
        Commandant *_commandant;
        
        _commandant = [Commandant shipWithFileName: @"courier.shp"];
//        _commandant = [Commandant shipWithFileName: @"asp2.shp"];
        //    _commandant = [Commandant shipWithFileName: @"gg.shp"];
//        _commandant = [Commandant shipWithFileName: @"trader.shp"];
        //_commandant = [Commandant shipWithFileName: @"eagle1.shp"];
        [_commandant setName: @"GG-007"];
        [_commandant setPosition: &posObs];
        [_commandant setIndexSystem: 0]; //we start in Sol
        initVect(Direction(_commandant), 1, 0, 0);
        initVect(Haut(_commandant), 0, 0, 1);
        SetZeroVect(*Omega(_commandant));
        [_commandant normalise];
        [_commandant addEquipment: MV2_Assault_Missile];
        [_commandant addEquipment: MV2_Assault_Missile];
        [_commandant addEquipment: MV2_Assault_Missile];
        [_commandant addEquipment: MV2_Assault_Missile];
        
        [_commandant addEquipment: Laser_4mw_Beam];
        
        [_commandant addItem: [Item equipmentWithArg: Shield_Generator]
                       count: 3 ];
        
        [_commandant addEquipment: Hyper_Cloud_Analizer];
        [_commandant addEquipment: Radar_Mapper];
        
        [_commandant addEquipment: Auto_Refueler];
        Fuel(_commandant) = 1000.0;
        
        [self setCommandant: _commandant];
        
        {
            Item *i;
            i = [Item hydrogen];
            [_commandant addItem: i
                           count: 50];
        }
        
        //     i = [Item hyperDriveCivil: 3];
        //     [_commandant addItem: i];
        
        
#ifdef GGSIM
        {
            GGScene *scene;
            scene = [GGScene new];
            [leRepereLocal addSubNode: scene];
            RELEASE(scene);
        }
#endif
        
#if 0
        {
            GGScene *scene;
            scene = [GGScene new];
            initVect(Point(scene), 0, 0, -10000);
            //      initVect(Point(scene), 0, 0, 20000);
            [leRepereLocal addSubNode: scene];
            RELEASE(scene);
        }
#endif
        
        [self loadSystemAtIndex:  [_commandant indexSystem]
                       archiver: [self buildSystemArchiverForIndex: 
                           [_commandant indexSystem]]];
        Point(_commandant)->z = 10;
    }
    RELEASE(pool);
}

- (void) setPaused: (BOOL) flag
{
    isPaused = flag;
    if( isPaused )
    {
        [theLoop setPollMode: WaitForEvent];
        [theLoop setTimeSpeed: 0];
    }
    else
    {
        [theLoop setPollMode: DontWaitForEvent];
        [theLoop setTimeSpeed: 1];
    }
}

- (Complex_station_manager *) stationManagerFor: (Station *) station
{
    Complex_station_manager *ret;
    id sig;
    NSParameterAssert(station);
    
    sig = [station signature];
    
    ret = [state->stationManagerDico objectForKey: sig];
    
    NSDebugLLog(@"StationM", @"searching %@", sig);
    
    if( ret == nil )
    {
        NSDebugLLog(@"StationM", @"not fould regenerationg %@", sig);
        ret = [Complex_station_manager station_manager_station: station];
        [state->stationManagerDico setObject: ret
                                     forKey: sig];
        [ret registerContinuation: self
                               at: @selector(endSessionManager:sig:)];
    }
    else
        NSDebugLLog(@"StationM", @"found  %@", sig);
    return ret;
}

- (void) endSessionManager: obj
                       sig: signature
{
    NSDebugLLog(@"StationM", @"removing %@", signature);
    [state->stationManagerDico removeObjectForKey: signature];
}

- (void) jumpToSystemIndex: (int) index
{
    Cloud *cloud;
    NSDictionary *aDic;
    Tunnel *t;
    [theSky genereSkyForIndex: index];
    [self reparent: leRepereLocal];
    
    aDic = [NSDictionary dictionaryWithObject: [NSNumber numberWithInt: 
        [starSytem index]]
                                       forKey: @"Name"];
    
    
    t = [self tunnelForHyperJumpFrom: indexSystem
                         destination: index
                                ship: state->commandant];
    
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationLeavingSystem
                  object: self
                userInfo: aDic];
    
    [self unloadSystem];  //this remove the local frame
    
    //recenter the local frame around the camera.
    
    [theLoop incrementGameTime: t->timeLength];
    [theLoop setTimeSpeed: 1.0];
    
    [self loadSystemAtIndex: index
                   archiver: [self buildSystemArchiverForIndex: index]];
    
    *Point(leRepereLocal) = GGZeroVect;
    *Point(state->commandant) = [t destinationPointInSystem: starSytem];
    [state->commandant tryGlobalCoordinates];
    
    *Speed(leRepereLocal) = GGZeroVect;
    *Speed(state->commandant) = GGZeroVect;
    
    NSDebugLLog(@"Tunnel", @"arrive is %@", 
                stringOfVect2([state->commandant getPositionAbsolu]));
    
    
    [state->commandant setIndexSystem: [starSytem index]];
    consoleLog(@"jump to system %s", [theSky cNameOf: index]);
    
    aDic = [NSDictionary dictionaryWithObject: [NSNumber numberWithInt: 
        [starSytem index]]
                                       forKey: @"Name"];
    
    [self restoreShips: [[starSytem archiver] ships]];
    
    [[NSNotificationCenter defaultCenter]
    postNotificationName: GGNotificationEnteringSystem
                  object: self
                userInfo: aDic];
    [starSytem afterEnteringSystem];
    cloud = [Cloud cloudFrom: state->commandant
                      tunnel: t];
    [t setDestCloud: cloud];
    [leRepereLocal addSubNode: cloud];
    *Point(cloud) = *Point(state->commandant);
    [theWorld addShip: cloud];
}

- (NSMutableArray *) getOrCreateTunnelArrayForZone: (ZoneKey *) key
{
    NSMutableArray *a;
    a = [state->tunnels objectForKey: key];
    if( a == nil )
    {
        a = [NSMutableArray new];
        [state->tunnels setObject: a
                          forKey: key];
        RELEASE(a);
    }
    return a;
}

- (void) destroyTunnel: (Tunnel *) t
{
    NSMutableArray *tuns;
    ZoneKey *key;
    int index;
    
    key = [ZoneKey keyWith: t->source
                       and: t->destination];
    
    tuns = [self getOrCreateTunnelArrayForZone: key];
    index = [tuns indexOfObjectIdenticalTo: t];
    NSDebugMLLog(@"Tunnel", @"destroying %@", t);
    NSParameterAssert(index != NSNotFound);
    
    [tuns removeObjectAtIndex: index];
    
    if( [tuns count] == 0 )
    {
        [state->tunnels removeObjectForKey: key];
    }
}

- (Tunnel *) tunnelForHyperJumpFrom: (int) source
                        destination: (int) dest
                               ship: (GGShip *)ship
{
    NSMutableArray *tuns;
    Tunnel *t;
    ZoneKey *key;
    
    GGReal dist;
    dist = [theSky distanceWith: source
                            and: dest];
    
    key = [ZoneKey keyWith: source
                       and: dest];
    
    tuns = [self getOrCreateTunnelArrayForZone: key];
    t = [Tunnel tunnelWithSource: source	
                     destination: dest
                      timeLength: timeForJump(dist, ship)
                           start: gameTime ];
    
    t->poss = [ship getPositionAbsolu];
    if( [tuns count] > 0 )
    {
        Tunnel *t2 = [tuns lastObject];
        NSDebugMLLog(@"Tunnel", @"find another one : %@", t2);
        
        [t setNextTunnel: t2];
    }
    [tuns addObject: t];
    return t;
}

- (Tunnel *) pnjEnterHyperSpace: (GGShip *)ship
                      forSystem: (int) dest
{
    Cloud *cloud;
    Tunnel *t;
    SystemArchiver *sa;
    int curindex;
    
    curindex = [ship indexSystem];
    t = [self tunnelForHyperJumpFrom: curindex
                         destination: dest
                                ship: ship];
    
    [state->hyperShips addObject: ship];
    sa = [self systemArchiverForIndex: curindex];
    NSParameterAssert(sa);
    
    if( curindex == indexSystem )
    {
        Cloud *cloud = [Cloud cloudFrom: ship
                                 tunnel: t];
        NSParameterAssert([ship nodePere]);
        [[ship nodePere] addSubNode: cloud];
        *Point(cloud) = *Point(ship);
        [self addShip: cloud];
        [self reparent: ship];
        [t setStartCloud: cloud];
    }
    else
        NSWarnMLog(@"Strange");
    
    cloud = [Cloud cloudFrom: ship
                      tunnel:t];
    [t setDestCloud: cloud];
    if ( dest == indexSystem )
    {
        *Point(cloud) = [t destinationPointInSystem: starSytem];
    }
    
    [sa removeShip: ship];
    
    if( curindex == [starSytem index] )
    {
        NSDebugMLLog(@"World", @"%@ leaves current system", ship);
        [ship looseMainSystem];
    }
    [ship setIndexSystem: -1];
    return t;
}

- (void) pnjLeavingHyperSpace: (GGShip *)ship
                    forSystem: (int) index
{
    int indship;
    SystemArchiver *sa;
    
    indship = [state->hyperShips indexOfObjectIdenticalTo: ship];
    NSParameterAssert(indship != NSNotFound);
    
    NSParameterAssert(index >= 0);
    sa = [self buildSystemArchiverForIndex: index];
    [sa addShip: ship];
    
    [state->hyperShips removeObjectAtIndex: indship];
    if ( index == [starSytem index] )
    {
        NSDebugMLLog(@"World", @"%@ arriving in current system", ship);
        [self addSubNode: ship];
        [self makeCurrent: ship];
    }
    [ship setIndexSystem: index];
}



- (void) setCheckCollision: (BOOL) f
{
    checkCollision = f;
}

- (BOOL) isPaused
{
    return isPaused;
}

- (NSArray *) currentShips
{
    GGShip *aShip;
    NSArray * array =  [[starSytem archiver] ships];
    NSMutableArray *new = [NSMutableArray array];
    NSEnumerator *en;
    
    en = [array objectEnumerator];
    
    while((aShip = [en nextObject]) != nil)
    {
        if( [aShip nodePere] != nil )
            [new addObject: aShip];
    }
    return new;
}

- (Commandant *) commandant
{
    return state->commandant;
}

- (void) setCommandant: (Commandant *) obj
{
    ASSIGN(state->commandant, obj);
    //  [obj setInvincibility: YES];
    [self addSubNode: obj];
    
    *Point(leRepereLocal) = *Point(obj);
    [obj enterUniverse];
    [obj beginSchedule];
    [self initCommandant];
    [obj tryLocalCoordinates];
    [obj enregistre: self
               with: @selector(removeCommandant:)];
}

- (void) removeCommandant: (NSNotification *) aNotification
{
    id obj = [aNotification object];
    
    if( obj == state->commandant )
    {
        NSWarnMLog(@"Destroying commandant");
        DESTROY(state->commandant);
        [[theLoop  slowScheduler]
	deleteTarget: self
	withSelector: @selector(removeCommandant:)];
    }
}


- (void) makeCurrent: (GGSpaceObject *) ship
{
    NSParameterAssert([ship nodePere] != nil);
    //    GGNode *node = [ship nodePere];
    //   if( node == leRepereLocal )
    //     {
    //       NSDebugMLLog(@"World", @"%@ is local", ship);
    //       [ship scheduleAsLocal];
    //       //      [ship setLocality: 2];
    //     }
    //   else
    //     {
    //       NSDebugMLLog(@"World", @"%@ is global", ship);
    //       [ship scheduleAsGlobal];
    //       //      [ship setLocality: 1];
    //     }
    [ship beginSchedule];
}

- (void) addShip: (GGSpaceObject *) aSpaceObject
{
    int index = [starSytem index];
    NSParameterAssert(aSpaceObject);
    [[starSytem archiver] addShip: aSpaceObject];
    if( [aSpaceObject isKindOfClass: [GGShip class]] )
    {
        GGShip *ship = (GGShip *)aSpaceObject;
        
        [ship setIndexSystem: index];
    }
    [self makeCurrent: aSpaceObject];
    [aSpaceObject enregistre: self
                        with: @selector(removeWithNotification:)];
    [aSpaceObject enterUniverse];
}


- (void) restoreShips: (NSArray *) array
{
    int n = [array count];
    if( n > 0 )
    {
        GGSpaceObject *lst[n];
        int i;
        [array getObjects: lst];
        
        for(i = 0; i < n; ++i)
        {
            [self addSubNode: lst[i]];
            [self makeCurrent: lst[i]];
        }
    }
}

- (void) removeWithNotification: (NSNotification *) aNotification
{
    GGShip *obj= [aNotification object];
    if( [ obj isKindOfClass: [GGShip class]] )
    {
        int index;
        
        index = [obj indexSystem];
        if( index == -1) //hyperspace
        {
            NSMutableArray *array;
            array = state->hyperShips;
            if( [array indexOfObjectIdenticalTo: obj] == NSNotFound )
                NSWarnMLog(@"object %@ not Found", obj);
            else
                [array removeObjectIdenticalTo: obj];
        }
        else
        {
            SystemArchiver *sa;
            sa = [self systemArchiverForIndex: index];
            if( sa == nil || 
                [[sa ships] indexOfObjectIdenticalTo: obj] == NSNotFound )
            {
                NSWarnMLog(@"object %@ not Found", obj);
            }
            else
            {
                NSDebugMLLog(@"World", @"ship %@ dying", obj);
                if(index != indexSystem && [[sa ships] count] == 0 )
                {
                    NSDebugMLLog(@"World", @"no more ships in the system : %d (%@)",
                                 index, [self starNameForIndex: index]);
                    
                    [state->otherSystemState removeObjectForKey: 
                        [NSNumber numberWithInt: index]];
                }
                
            }
        }
    }
}

- (GGOrbiteur *) orbiteurByName: (NSString *)name
{
    int n = [planeteArray count];
    GGOrbiteur *orbs[n];
    int i;
    
    [planeteArray getObjects: orbs];
    
    for( i = 0; i < n; ++i )
    {
        if( [[orbs[i] name] isEqualToString: name] )
            return orbs[i];
    }
    return nil;
}

- (NSArray *) allOrbiteur
{
    return planeteArray;
}

- (NSArray *) allStarPort
{
    return [starSytem allStations];
}

- (void) setMainCamera:(GGSpaceObject *) obj
{
    if( mainCamera == obj )
        return;
    [mainCamera desenregistre: self]; //Puique qu'on peut envoyer des messages à nil
    [mainCamera setCameraFlag: 0];
    ASSIGN(mainCamera, obj);
    [mainCamera setCameraFlag: 1];
    [obj enregistre: self
               with:@selector(removeCameraWithNotification:)];
}

- (void) setAuxiliaryCamera:(GGSpaceObject *)obj
    /*on ne regle pas le camera flag de l'objet car il ne concerne que la 
    *camera principale
    */
{
    NSDebugMLLog(@"auxcamera", @"on perd le missile");
    if( auxiliaryCamera == obj || !thePref.HighQuality )
        return;
    [auxiliaryCamera desenregistre: self]; //Puique qu'on peut envoyer des messages à nil
    ASSIGN(auxiliaryCamera, obj);
    [obj enregistreForRemoving: self
                          with:@selector(removeCameraWithNotification:)];
}

- (GGSpaceObject *) camera
{
    return mainCamera;
}

- (void) removeCameraWithNotification: (NSNotification *)aNotification
{
    /* Si l'objet qui est la caméra se fait buter, le vaisseau commandant
    devient la nouvelle caméra
    */
    id obj = [aNotification object];
    if( obj == mainCamera )
    {
        NSWarnMLog(@"la camera principale meure!!!");
        //      [self setMainCamera: state->commandant];
        [self setMainCamera: nil];
    }
    if( obj == auxiliaryCamera )
        [self setAuxiliaryCamera: nil];
}

- (void) setSky: (GGSky *) s
{
    ASSIGN(theSky, s);
}

- (GGSky *) sky
{
    return theSky;
}

- (void) setDrawWorld: (BOOL) flag
{
    drawWorld = flag;
}

#pragma mark drawing code

- (void) drawSceneWithCamera: (GGSpaceObject *)aCamera
                        left: (int) x
                       lower: (int) y
                       width: (int) w
                      height: (int) h
                       label: (BOOL) label
{
    //  printf("%d %d %d %d\n", x, y, w, h);
    ProjectionParam p = projectionParam;
    p.x = x;
    p.y = y;
    p.width = w;
    p.height = h;
    p.ratio = (float)w/(float)h;
    p.label = label;
    glViewport(x, y, w, h);
    glEnable(GL_SCISSOR_TEST);
    glScissor(x, y, w, h);
    //  clearScene(0);
    
    glMatrixMode(GL_PROJECTION);
    GGSetupGLProjectionWithParam(&p);
    glMatrixMode(GL_MODELVIEW);
    
    
    if(label){
        [theTableau doIndicator: &p	
                         camera: aCamera];
    }
    
    if(0 && theLightSource){
        /* this code attemps de draw from the light source to compute the shadow.  We need to generate an accurate frustum */
        // first path from the light source.
        Vect3D posInLocalFrame;
        [theLightSource positionInRepere:leRepereLocal
                                      in:&posInLocalFrame];
        normaliseVect(&posInLocalFrame);
        mulScalVect(&posInLocalFrame, -1.0, &posInLocalFrame);
        GGReal min, max;
        min = [leRepereLocal computeExtensionAlongDirection:posInLocalFrame
                                                     maxRef:&max];
        
        Vect3D posCamera;
        [aCamera positionInRepere:leRepereLocal in:&posCamera];
        
    }
    [aCamera drawGraphProjection: &p];
    
    if( theLightSource != nil )
    {
        Vect3D pos;
        [theLightSource positionInRepere: aCamera
                                      in: &pos];
        
        if( pos.z < 0.1 )
        {
            NSDebugMLLogWindow(@"Flare", @"sun is behind...");
            
        }
        else
        {
            GGReal x, y;
            x = -pos.x/pos.z/projectionParam.xFoc;
            y = pos.y/pos.z/projectionParam.yFoc;
            
            if( x >= -1 && x <= 1 && y >= -1 && y <= 1 )
                [flares drawFlaresAt: x
                                 and: y];
        }
    }
    glDisable(GL_SCISSOR_TEST);
}

- (void) getProjectionParam: (ProjectionParam *)ppp
{
    *ppp = projectionParam;
}


#if 0 // && DEBUG
static int sortAux(id o1, id o2, void *bla)
{
    int diff = GSDebugAllocationCount(o2)-GSDebugAllocationCount(o1);
    
    if(diff > 0 )
        return NSOrderedDescending;
    if(diff < 0 )
        return NSOrderedAscending;
    else
        return NSOrderedSame;
}
#endif

- (void) drawScene: (NSRect) aFrame
{
    if( !isPaused )
    {
        if( checkCollision ){
//            [self checkForCollision];
        }
        
        /* killing an object may add other object to the pool...*/
        while( [needToDie count] > 0 )
        {
            NSInvocation *obj = [needToDie lastObject];
            NSDebugMLLog(@"Kill", @"Killing %@", obj);
            RETAIN(obj);
            [needToDie removeLastObject];
            [obj invoke];
            RELEASE(obj);
        }
    }
    
    if( !drawWorld )
        return;
    
    [self drawSceneWithCamera: mainCamera
                         left: aFrame.origin.x
                        lower: aFrame.origin.y
                        width: aFrame.size.width 
                       height: aFrame.size.height
                        label: YES];
    
    
    
    //  [mainCamera setVisible: YES];
    NSDebugMLLogWindow(@"Tree", [self displayObject: 0]);
    NSDebugMLLogWindow(@"Speed", stringOfVect(Speed(leRepereLocal)));
#if 0 // && DEBUG
    {
        if( thePref.DebugMemoire )
        {
            NSEnumerator *enumerator;
            Class *lst, *temp;
            Class aClass;
            int count;
            NSMutableArray *array;
            
            lst = GSDebugAllocationClassList();
            for(count = 0, temp=lst; *temp != nil; count++, temp++);
            
            array = [NSMutableArray arrayWithObjects: lst
                                               count: count];
            [array sortUsingFunction: &sortAux context: NULL];
            enumerator = [array objectEnumerator];
            while((aClass = [enumerator nextObject]) != nil)
            {
                [theGGInput addLogString: @"Class %@ count %d total %d peak %d", 
                    aClass,
                    GSDebugAllocationCount(aClass),
                    GSDebugAllocationTotal(aClass),
                    GSDebugAllocationPeak(aClass)];
            }
        }
    }
#endif
    
    
    if( auxiliaryCamera != nil )
    {
        [self drawSceneWithCamera: auxiliaryCamera
                             left: WIDTH-250
                            lower: HEIGHT-250
                            width: 250
                           height: 250
                            label: NO];
        
        //draw  the border :
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0.,1.01,0,1.01,-1.0,1.0);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glColor3f(0.8, 0.8, 1);
        glPolygonMode(GL_FRONT, GL_LINE);
        glRecti(0, 0, 1, 1);
        glPolygonMode(GL_FRONT, GL_FILL);
    }
    
}

#pragma mark various stuff to sort
- (void) checkForCollision
{
    NSArray *locals = [leRepereLocal subNodes];
    int n = [locals count];
    
    NSDebugMLLogWindow(@"Collision", @"ici %@", locals);
    
    if( n > 0 )
    {
        GGSpaceObject *objs[n+1];
        int i, j;
        
        [locals getObjects: objs];
        for( i = 0; i < n; ++i )
        {
            if( CollisionCounter(objs[i]) )
                continue;
            for( j = i+1; j < n; ++j )
            {
                Vect3D *v1 = Point(objs[i]);
                Vect3D *v2 = Point(objs[j]);
                
                GGReal d2 = distance2Vect(v1, v2);
                GGReal dcol = Dimension(objs[i]) + Dimension(objs[j]);
                
                if( CollisionCounter(objs[j]) )
                    continue;
                
                NSDebugMLLogWindow(@"Collision", @"d(%@, %@) = %g",
                                   objs[i], objs[j], sqrt(d2));
                
                
                if( d2 < SQ(dcol) )
                {
                    NSDebugMLLog(@"Collision", 
                                 @"Collision detected between %@ %@",
                                 objs[i], objs[j]);
                    if( [objs[j] manageCollision] )
                        [objs[j] collisionOccursWith: objs[i]];
                    else
                        [objs[i] collisionOccursWith: objs[j]];
                    
                }
            }
        }
    }
}

- (void) manageLaser: (Laser *) l
{
    [l enregistre: self
             with: @selector(removeLaserWithNotification:)];
    [state->lasers addObject: l];
    if( !state->managingLaser )
    {
        state->managingLaser = YES;
        [theLoop fastSchedule: self
                 withSelector: @selector(checkForLaser)
                          arg: nil];
    }
    NSDebugMLLog(@"Laser", @"start managing laser");
}

- (void) removeLaserWithNotification: (NSNotification *)aNotification
{
    id obj = [aNotification object];
    NSDebugMLLog(@"Laser", @"stop managing laser");
    [state->lasers removeObjectIdenticalTo: obj];
    if( state->managingLaser && [state->lasers count] == 0 )
    {
        state->managingLaser = NO;
        [[theLoop fastScheduler] deleteTarget: self
                                 withSelector: @selector(checkForLaser)];
        NSDebugMLLog(@"Laser", @"no more laser");
    }
}

- (void) checkForLaser
{
    NSArray *locals = [leRepereLocal subNodes];
    int n, nl, i , j;
    
    nl = [state->lasers count];
    NSAssert(nl > 0, @"bad");
    n = [locals count];
    if( n > 0 )
    {
        GGSpaceObject *objs[n];
        [locals getObjects: objs];
        for( i = 0; i < nl; ++i)
        {
            Laser *l = [state->lasers objectAtIndex: i];
            Vect3D pos, dir;
            float power = [l power] * MassePerPower;
            //FIXME : hack
            pos = *Point(l->nodePere);
            dir = *Direction(l->nodePere);
            NSDebugMLLogWindow(@"Laser", @"pos %@, dir %@", 
                               stringOfVect(&pos),
                               stringOfVect(&dir));
            for(j = 0; j < n; ++ j)
            {
                GGReal d, xpos;
                GGSpaceObject *obj = objs[j];
                
                /*a laser can't touch its sender*/
                if( CollisionCounter(obj) < 0 || obj == l->nodePere )
                    continue;
                
                d = distanceDroitePoint(&pos, &dir, Point(obj),
                                        &xpos);
                NSDebugMLLogWindow(@"Laser", @"dist with : %@ = %g",
                                   objs[i], d);
                if( xpos <= 10)
                    continue;
                
                if( d < Dimension(obj) )
                {
                    [obj chocWithEnergy: deltaTime*power];
                }
            }
        }
    }
}

- (void) genereExplosionFrom: (GGSpaceObject *) obj
                  withEnergy: (float) val
{
    Explosion *e;
    NSArray *locals = [leRepereLocal subNodes];
    int n = [locals count];
    
    if( [obj locality] != 2 )
    {
        NSWarnMLog(@"%@ is not local", obj);
        return;
    }
    
    
    if( n > 0 )
    {
        GGSpaceObject *objs[n];
        int i;
        Vect3D *v1 = Point(obj);
        
        [locals getObjects: objs];
        
        for( i = 0; i < n; ++i )
        {
            if( objs[i] != obj )
            {
                float att;
                Vect3D *v2 = Point(objs[i]);
                GGReal d = sqrt(distance2Vect(v1, v2));
                d -= Dimension(objs[i]);
                d = d < 1 ? 1 : d;
                att = val/(d*d);
                
                NSDebugMLLog(@"Explosion", @"distance with %@=^2 : %g\natt = %g", 
                             objs[i], d, att);
                
                [objs[i] chocWithEnergy: att];
            }
        }
    }
    e = [Explosion explosionAt: obj
                    withEnergy: val];
    [e setVisible: YES];
    [e enterUniverse];
    [leRepereLocal addSubNode: e];
    [e beginSchedule];
}

- (void) addToKillPool: obj
              selector: (SEL) s
{
    NSInvocation *inv;
    inv = [NSInvocation invocationWithMethodSignature:
        [obj methodSignatureForSelector: s]];
    
    [inv setTarget: obj];
    [inv setSelector: s];
    [inv retainArguments];
    [needToDie addObject: inv];
    NSDebugMLLog(@"Kill", @"adding %@, needToDie = %@", obj, needToDie);
    
}	

- (void) postLoadingSystem
{
    NSArray	*sons;
    GGOrbiteur *soleil = [starSytem mainStar];
    theClosest = soleil;
    [self addSubNode: soleil];
    [theClosest startBeingOrigineOfLocale: self];
    [self _anOrbiteurBeingOriginOfLocalOldClosest: nil];
    sons = [soleil sons];
    [self addSubNodeFromArray: sons];
    [soleil beginSchedule];
    [sons makeObjectsPerformSelector: @selector(beginSchedule)];
    [theTableau setClosestOrbiteur: theClosest];
}

- (void) setSystem: (System *)theSystem
{
    GGOrbiteur *unSoleil = [theSystem mainStar];
    NSArray 	 *allPlanetes = [unSoleil getAllOrbiteur];
    int n = [allPlanetes count];
    int i;
    ASSIGN(starSytem, theSystem);
    [self setOriginOrbiteur: unSoleil];
    for( i = 0; i < n; ++i )
    {
        GGOrbiteur *o = (GGOrbiteur *)[allPlanetes objectAtIndex: i];
        [o enterUniverse];
        if( [o lightSource] )
            ASSIGN(theLightSource, o);
    }
    [unSoleil miseAJourSysteme];
    ASSIGN(planeteArray, allPlanetes);
    [self postLoadingSystem];
}

- (System *)currentSystem
{
    return starSytem;
}

- (int) currentSystemIndex
{
    return indexSystem;
}

- (NSString *) starNameForIndex: (int) index
{
    return [NSString stringWithUTF8String: [theSky cNameOf: index]];
}

- (void) setClosestOrbiteurRec
{
    GGOrbiteur	*oldClosest = theClosest;
    
    Vect3D 		pos;
    GGOrbiteur	*son;
    [state->commandant positionInRepere: [theClosest subSystemFrame]
                                    in: &pos];
    NSDebugMLLog(@"WorldHard", @"position orb %@", stringOfVect(&pos));
    //   if ( theClosest )
    //     {
    //       NSDebugMLLogWindow(@"Prout", @"\n%@ tailleSystem = %g km", theClosest, 
    // 			 [theClosest tailleSysteme]/1000.0);
    //       NSDebugMLLogWindow(@"Prout", @"\n dist = %g km",  
    // 			 distanceAbsolu(state->commandant, theClosest)/1000.0);
    
    //     }
    if( prodScal(&pos, &pos) > square([theClosest tailleSysteme]) )
        [self makeFatherCurrentOrbiteur];
    else if ((son = [theClosest closestFromRelative: &pos] ) != theClosest )
        [self makeSonCurrentOrbiteur: son];
    /* else do nothing */
    
    if( oldClosest != theClosest )
    {
        [self setClosestOrbiteurRec];
    }
}



@end

@implementation World (PrivateMethod)

- (void) calcLightSource
{
    Vect3D com, sol;
    if( theLightSource == nil ){
        return;
    }
    [theLightSource positionInRepere: self
                                  in: &sol];
    [state->commandant positionInRepere: self
                                    in: &com];
    mkVectFromPoint(&com, &sol, &lightPosition.v);
    normaliseVect(&lightPosition.v);
}

- (void) unloadSystem
{
    NSArray *curShips;
    int n;
    
    [state->commandant  stopEngine];
    NSParameterAssert(starSytem != nil);
    // Now the problem of the ships
    curShips = [self currentShips];
    n = [curShips count];
    if( n > 0 ){
        GGSpaceObject *shipsTab[n];
        int i;
        [curShips getObjects: shipsTab];
        
        for( i = 0; i < n ; ++i){
            if (shipsTab[i] != state->commandant ){
                [self reparent: shipsTab[i]];
                [shipsTab[i] removeFromSuperNode];
                [shipsTab[i] looseMainSystem];
            }
        }
    }
    else{
        NSDebugMLLog(@"World", @"no ships, removing archiver");
        
        [state->otherSystemState removeObjectForKey: 
            [NSNumber numberWithInt:   [starSytem index]]];
    }
    
    //Now the problem of the orbiteur.
    while( [self makeFatherCurrentOrbiteur] );
    
    id oldclosest = theClosest;
    //we destroy the sun
    [theClosest stopBeingOrigineOfLocale];
    NSParameterAssert([theClosest nodePere] == self);
    [[theClosest sons] makeObjectsPerformSelector: @selector(endSchedule)];
    [[theClosest sons] makeObjectsPerformSelector: 
        @selector(removeFromSuperNode)];
    [theClosest removeFromSuperNode];
    
    
    [[starSytem mainStar] looseMainSystem];
    [starSytem endCurrent];
    [starSytem invalidate];
    DESTROY(starSytem);
    DESTROY(theLightSource);
    theClosest = nil;
    [self _anOrbiteurBeingOriginOfLocalOldClosest: oldclosest];
    
    [self removeAllSons];
    [self addSubNode: leRepereLocal]; //we put it back
    [leRepereLocal removeAllSons];
    if( state->commandant )
        [leRepereLocal addSubNode: state->commandant];
}

- (void) _anOrbiteurBeingOriginOfLocalOldClosest: obj
{
    if (obj )
        [MessageManager
      removeObserver: self
                name: nil
              object: obj];
    if ( theClosest )
        [MessageManager
      addObserver: self
         selector: @selector(anOrbiteurBeingSuperLocal:)
             name: nil
           object: theClosest];
}

- (void) anOrbiteurBeingSuperLocal: (NSNotification *) aNotification
{
    // HACK:
    if( [[aNotification name] isEqualToString: GGOrbiteurBeingSuperLocal] )
    {
        GGOrbiteur *theOrb = [aNotification object];
        NSAssert(theOrb == theClosest,
                 @"bad");
        [[theOrb nodePere] reparent: leRepereLocal];
        [leRepereLocal setModeRepere:ReperePlanete];
    }
    else if([[aNotification name] 
	    isEqualToString: GGOrbiteurEndBeingSuperLocal])
    {
        GGOrbiteur *theOrb = [aNotification object];
        NSAssert(theOrb == theClosest,
                 @"bad");
        
        [[theOrb subSystemFrame] reparent: leRepereLocal];
        [leRepereLocal setModeRepere:RepereNormal];
    }
}

- (BOOL) makeFatherCurrentOrbiteur
{
    GGNode 	*newNode;
    GGNode 	*oldNode;
    GGOrbiteur	*orbiteurPere = [theClosest orbiteurPere];
    id 		oldclosest = theClosest;
    
    if( orbiteurPere == nil )
        return NO;
    
    [theClosest stopBeingOrigineOfLocale];
    oldNode = [theClosest nodePere];
    NSParameterAssert(oldNode != nil);
    newNode = [oldNode nodePere];
    NSParameterAssert(newNode != nil);
    
    [newNode reparent: leRepereLocal];
    [newNode reparent: theClosest];
    
    //we remove all the suborbiteur from the current universe :
    [[theClosest sons] makeObjectsPerformSelector: @selector(endSchedule)];
    [[theClosest sons] makeObjectsPerformSelector: 
        @selector(removeFromSuperNode)];
    
    //but don't remove other object (like ships)
    //transfer them to the father node.
    
    [oldNode removeFromSuperNodeAndReparentSons];
    
    theClosest =  orbiteurPere;
    NSParameterAssert(theClosest != nil);
    
    [theClosest startBeingMainLocale];
    [self _anOrbiteurBeingOriginOfLocalOldClosest: oldclosest];
    [theTableau setClosestOrbiteur: theClosest];
    return YES;
}

- (void) makeSonCurrentOrbiteur: (GGOrbiteur *)son
{
    GGNode 	*newNode;
    GGNode 	*currentNode;
    NSArray 	*sons;
    id		oldclosest = theClosest;
    
    [theClosest stopBeingMainLocale];
    currentNode = [theClosest nodePere];
    
    newNode = [GGNode new];
    [newNode setOriginOrbiteur: son];
    [newNode setName: [NSString stringWithFormat: 
        @"repere translation from %@",
        son]];
    
    *Point(newNode) = *Point(son);
    *Speed(newNode) = *Speed(son);
    //  *Galileen(newNode) = *Galileen(son);
    
    
    [currentNode addSubNode: newNode];
    [newNode reparent: son];
    [newNode reparent: leRepereLocal];
    theClosest = son;
    
    [theClosest startBeingOrigineOfLocale: newNode];
    [self _anOrbiteurBeingOriginOfLocalOldClosest: oldclosest];
    sons = [son sons];
    [newNode addSubNodeFromArray: sons];
    [son beginSchedule];
    [sons makeObjectsPerformSelector: @selector(beginSchedule)];
    [theTableau setClosestOrbiteur: theClosest];
    
    
    RELEASE(newNode);
}

- (void) checkClosestOrbiteur
{
    NSAssert(theClosest != nil, @"bad");
    
    [self setClosestOrbiteurRec];
    [self calcLightSource];
}

- getObjectAtSystemIndex: (int) index
                    name: (NSString *)name
{
    NSAssert([starSytem index] == indexSystem,
             @"bad");
    if( index == [starSytem index] )
        return [self orbiteurByName: name];
    else
    {
        NSWarnMLog(@"index = %d current = %d", index,
                   [starSytem index]);
        return nil;
    }
}

- (void) loadSystemAtIndex: (int) index
                  archiver: (SystemArchiver *) archiver
{
    indexSystem = index;
    //  NSParameterAssert(archiver != nil);
    
    NSParameterAssert(starSytem == nil);
    [starSytem endCurrent];
    [self setSystem: [System systemAt: [theSky stars: NULL]+index
                             archiver: archiver]];
    [starSytem makeCurrent];
}

- (void) setWorldArchiver: (WorldArchiver *) _wa
{
    wa = _wa;
}

- (GGNode *) nodeForOrbiteur: (GGOrbiteur *)orb
{
    if( [orb isOriginOfNode] )
        return [orb nodePere];
    else
    {
        GGNode *node = nil;
        GGOrbiteur *orbPere;
        GGNode *pereNode;
        orbPere = [orb orbiteurPere];
        NSAssert(orbPere != nil, @"bad");
        if( orbPere != nil )
        {
            pereNode = [self nodeForOrbiteur: orbPere];
            NSAssert(pereNode != nil, @"bad");
            [self makeSonCurrentOrbiteur: orb];
            node = [orb nodePere];
            NSAssert(node != nil, @"bad");
        }
        return node;
    }
}

- objectForName: (NSString *) str
{
    NSCharacterSet *semicolonSet;
    NSScanner *theScanner;
    NSString 	*name;
    
    //  NSLog(@"%@ is requested %@", self, str);
    
    if( indexSystem == -2 )
    {
        GGNode *aNode;
        NSAssert(wa != nil, @"bad");
        indexSystem = wa->indexSystem;
        
        NSAssert(indexSystem >= -1, @"bad");
        [self loadSystemAtIndex: indexSystem
                       archiver: wa->currentArchiver];
        
        NSLog(@"leRepereLocal pere: %@", [leRepereLocal nodePere]);
        aNode = [self objectForName: wa->nameLoc];
        NSLog(@"leRepereLocal pere: %@", [leRepereLocal nodePere]);
        //after this call, normally, the localFrame has been moved to its save 
        //position, we check it now
        
        NSAssert(aNode == [leRepereLocal nodePere], 
                 @"bad");
        
        if( wa->superLocal )
        {
            [theClosest beginBeingSuperLocal];
        }
        
        *Galileen(leRepereLocal) = wa->galLoc;
    }
    
    semicolonSet = [NSCharacterSet
		   characterSetWithCharactersInString:@";"];
    theScanner = [NSScanner scannerWithString: str];
    
    
    if( [str isEqualToString: @"theWorld"] )
        return self;
    else if( [str isEqualToString: @"Tableau"] )
    {
        return theTableau;
    }
    else if ([theScanner scanString: @"rep:" intoString:NULL] )
    {
        if ( [theScanner scanUpToCharactersFromSet: semicolonSet 
                                        intoString: &name])
        {
            GGOrbiteur *orb;
            orb = [self orbiteurByName: name];
            NSAssert1(orb != nil, @"can't recover orbiteur from name \"%@\"",
                      name);
            return [self nodeForOrbiteur: orb];
        }
        NSAssert(0, @"can't parse node name");
    }
    else if( [theScanner scanString: @"orb:" intoString:NULL] )
    {
        if ( [theScanner scanUpToCharactersFromSet: semicolonSet 
                                        intoString: &name])
        {
            GGOrbiteur *orb;
            orb = [self orbiteurByName: name];
            if (orb == nil) 
            {
                NSLog(@"merde : %@", name);
                ggabort();
            }
            NSAssert2(orb != nil, @"can't recover orbiteur from name \"%@\", orbs are %@",
                      name, [self allOrbiteur]);
            return orb;
        }
        NSAssert(0, @"can't parse node name");
    }
    else if( [theScanner scanString: @"reploc:" intoString:NULL] )
    {
        return leRepereLocal;
    }
    
    NSLog(@"%@", str);
    NSAssert(0, @"bad");
    
    return nil;
}

- (WorldState*) state
{
    return state;
}


- (void) initCommandant
{
    Commandant *obj = state->commandant;
    
    [theGGInput setShip: obj];
    if( mainCamera == nil )
        [self setMainCamera: state->commandant];
    [theTableau setObserveur: mainCamera];
}

- (SystemArchiver *) systemArchiverForIndex: (int) index
{
    NSValue *val = [NSNumber numberWithInt: index];
    
    return [state->otherSystemState objectForKey: val];
}

- (SystemArchiver *) buildSystemArchiverForIndex: (int) index
{
    NSValue *val = [NSNumber numberWithInt: index];
    SystemArchiver *sa;
    sa = [state->otherSystemState objectForKey: val];
    if( sa == nil)
    {
        sa = [SystemArchiver systemArchiver];
        [state->otherSystemState setObject: sa
                                   forKey: val];
    }
    return sa;
}

- (void) _setCurrentSystemArchiver: (SystemArchiver *)sa
{
    //if starSytem is nil, it works again
    [starSytem setArchiver: sa];
}
@end

@implementation NSString (GGMethod)
- objectFromSignature
{
    NSCharacterSet *semicolonSet;
    NSScanner *theScanner;
    int 		index;
    NSString 	*name;
    
    semicolonSet = [NSCharacterSet
		   characterSetWithCharactersInString:@";"];
    theScanner = [NSScanner scannerWithString: self];
    
    if ([theScanner scanString: @"orb:" intoString:NULL] &&
        [theScanner scanInt: &index] &&
        [theScanner scanString:@":" intoString:NULL] &&
        [theScanner scanUpToCharactersFromSet: semicolonSet
                                   intoString: &name])
    {
        return [theWorld getObjectAtSystemIndex: index
                                           name: name];
    }
    else
        NSWarnMLog(@"could not identify %@", self);
    
    return self;
}

@end

@implementation GGArchiver
+ ggArchiver: (NSString *)str
{
    GGArchiver *arch;
    arch = [[self alloc] init];
    ASSIGN(arch->name, str);
    return AUTORELEASE(arch);
}

- (void) dealloc
{
    RELEASE(name);
    [super dealloc];
}

- (void) encodeWithCoder: (NSCoder *)aCoder
{
    [aCoder encodeObject:name forKey:@"name"];
}

- initWithCoder: (NSCoder *)decoder
{
    NSString *str = [decoder decodeObjectForKey:@"name"];
    RELEASE(self);
    return RETAIN([theWorld objectForName: str]);
}

@end

id lookupObjet(id obj)
{
    if ( obj != nil &&  [obj isKindOfClass: [NSString class]] )
        return [obj objectFromSignature];
    else
        return obj;
}

