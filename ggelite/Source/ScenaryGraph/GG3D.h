/*
 *  GG3D.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GG3D_H
#define  __GG3D_H 1
#import "types.h"

#import <math.h>

extern const Vect3D GGZeroVect;
typedef GGReal (*pmat)[4];
typedef GGReal (*pvect);

#define SetZeroVect(a) (initVect(&(a), 0, 0, 0)) 
#define SQ(x) ((x)*(x))

GGBEGINDEC

/*%%%
import types;
%%%*/

extern const GGReal EPS;
/*-=
Bool normaliseVect(Vect3D *u);
void SetZeroVect(Vect3D u);
float normeVect(Vect3D *u);
=-*/

/*%%%
extern zero : Vect3D = "GGZeroVect";
%%%*/

GGENDDEC



static inline void initVect(Vect3D *u, GGReal a, GGReal b, GGReal c)
{
    u->x = a;
    u->y = b;
    u->z = c;
}

static inline Vect3D mkVect(GGReal a, GGReal b, GGReal c)
{
    Vect3D res;
    res.x = a;
    res.y = b;
    res.z = c;
    return res;
}

static inline void initRGBA(VectCol *u, GLfloat a, GLfloat b, 
                            GLfloat c, GLfloat d)
{
    u->r = a;
    u->g = b;
    u->b = c;
    u->a = d;
}

static inline GGPlan mkPlan(float a, float b, float c, float d)
{
    GGPlan p;
    initVect(&p.v, a, b, c);
    p.w = d;
    return p;
}

static inline Vect3D mkPoint(float a, float b, float c)
{
    Vect3D p;
    p.x = a;
    p.y = b;
    p.z = c;
    return p;
}

static inline void initWithIdentityMatrix(GGMatrix4 *res)
{
    GGMatrix4 mat = {{1, 0, 0}, 0,
    {0, 1, 0}, 0,
    {0, 0, 1}, 0,
    {0, 0, 0}, 1};
    *res = mat;
}


/*!
    @function   GGMatrixInitModelViewNormalizer
    @abstract   construct a quasi identity matrix
    @discussion this matrix swap the orientation on some axes.  The goal is too bridge the 
    convention we use in GGElite with the convention used by openGL for the modelview matrix.  It is only used when an object is to be set as the camera.  In this case, do the following:
      GGMatrix4 mat;
      GGMatrixInitModelViewNormalizer(&mat);
      glLoadMatrix(&mat);
      transposeMatrice(Matrice(obj), &mat);
      glMultMatrix(&mat);
      
    @param      res  Matrix to be initialized.
*/
static inline void GGMatrixInitModelViewNormalizer(GGMatrix4 *res)
{
    pmat p = (pmat)res;
    
    initWithIdentityMatrix(res);
    
    p[0][0] = -1;
    p[2][2] = -1;
}

static inline void initWithIdentityTransfo(GGGalileen *res)
{
    initWithIdentityMatrix(&(res->matrice));
    SetZeroVect(res->speed);
}

static inline void mkVectFromPoint(const Vect3D *A, const Vect3D *B, Vect3D *res)
{
    res->x = (B->x-A->x);
    res->y = (B->y-A->y);
    res->z = (B->z-A->z);
}

static inline VectCol GGMakeCol(GGReal r, GGReal g, GGReal b, GGReal a)
{
    VectCol ret;
    ret.r = r;
    ret.g = g;
    ret.b = b;
    ret.a = a;
    return ret;
}

static inline VectCol colOfVect(Vect3D v)
{
    return GGMakeCol(v.x, v.y, v.z, 1.0);
}

static inline Vect3D vectOfCol(VectCol col)
{
    return mkPoint(col.r, col.g, col.b);
}

#pragma mark vector operations
static inline void diffVect(const Vect3D *u, const Vect3D *v, Vect3D *res)
{
    res->x = (u->x-v->x);
    res->y = (u->y-v->y);
    res->z = (u->z-v->z);
}

static inline void addVect(const Vect3D *u, const Vect3D *v, Vect3D *res)
{
    res->x = (u->x+v->x);
    res->y = (u->y+v->y);
    res->z = (u->z+v->z);
}


static inline void prodVect(Vect3D *u, Vect3D *v, Vect3D *res)
{
    res->x = (u->y*v->z) - (u->z*v->y);
    res->y = (u->z*v->x) - (u->x*v->z);
    res->z = (u->x*v->y) - (u->y*v->x);
}

static inline void dualProduct(const Vect3D *u, const Vect3D *v, Vect3D *res)
{
    res->x = u->x*v->x;
    res->y = u->y*v->y;
    res->z = u->z*v->z;
}

static inline void mulScalVect(Vect3D *u, GGReal lambda, Vect3D *res)
{
    res->x = u->x * lambda;
    res->y = u->y * lambda;
    res->z = u->z * lambda;
}

static inline void divScalVect(Vect3D *u, GGReal lambda, Vect3D *res)
{
    res->x = u->x / lambda;
    res->y = u->y / lambda;
    res->z = u->z / lambda;
}


static inline void addLambdaVect(const Vect3D *u, GGReal lambda, const Vect3D *v,
                                 Vect3D *res)
{
    res->x = u->x + (lambda * v->x);
    res->y = u->y + (lambda * v->y);
    res->z = u->z + (lambda * v->z);
}

static inline BOOL normaliseVectAndGetNorm(Vect3D *u, GGReal *normPtr)
{
    GGReal norm;
    norm = ((u->x*u->x) + (u->y*u->y) + (u->z*u->z));
    if( norm < EPS*EPS )
        return NO;
    norm = sqrt(norm);
    u->x /= norm;
    u->y /= norm;
    u->z /= norm;
    *normPtr = norm;
    return YES;
}

static inline BOOL normaliseVect(Vect3D *u)
{
    GGReal norm;
    return normaliseVectAndGetNorm(u, &norm);
}

static inline GGReal prodScal(const Vect3D *u, const Vect3D *v)
{
    return (u->x*v->x) + (u->y*v->y) + (u->z*v->z);
}

static inline GGReal distance2Vect(const Vect3D *u, const Vect3D *v)
{
    Vect3D tmp;
    mkVectFromPoint(u, v, &tmp);
    return prodScal(&tmp, &tmp);
}

static inline GGPlan mkPlanFromPointAndNormal(Vect3D point, Vect3D normal)
{
    GGPlan p;
    p.v = normal;
    p.w = -prodScal(&normal,&point);
    
    return p;
}

static inline GGPlan mkPlanFromTriangles(Vect3D A, Vect3D B, Vect3D C)
{
    GGPlan ret;
    Vect3D tmp1, tmp2;
    mkVectFromPoint(&A,&B,&tmp1);
    mkVectFromPoint(&A,&C,&tmp2);
    prodVect(&tmp1,&tmp2,&ret.v);
    ret.w = -prodScal(&A,&ret.v);
    
    return ret;
}

static inline GGReal altituteFromPlanOfPoint(GGPlan p, Vect3D pos){
    return prodScal(&p.v,&pos) + p.w;
}

static inline GGReal distanceManhattan(const Vect3D *u, const Vect3D *v)
{
    Vect3D tmp;
    mkVectFromPoint(u, v, &tmp);
    return fabs(tmp.x)+fabs(tmp.y)+fabs(tmp.z);
}

static inline GGReal normeVect(Vect3D *u)
{
    return sqrt(prodScal(u, u));
}

static inline GGReal normeSup(Vect3D *u)
{
    GGReal ret = fabs(u->x);
    GGReal tmp = fabs(u->y);
    if(tmp > ret){
        ret = tmp;
    }
    tmp = fabs(u->z);
    if(tmp > ret){
        ret = tmp;
    }
    
    return ret;
}

#pragma mark -
#pragma mark matrice like operation 

static inline pvect ggVect3DToArray(Vect3D* pv){
    return (pvect)pv;
}

static inline Vect3D* ggArrayToVect3d(const GGReal pv[3]){
    return (Vect3D*)pv;
}

static inline void initDiagonalMatrice(GGMatrix4 *matrice, GGReal a, GGReal b, GGReal c)
{
    GGMatrix4 mat = {{a, 0, 0}, 0,
    {0, b, 0}, 0,
    {0, 0, c}, 0,
    {0, 0, 0}, 1};
    *matrice = mat;
}

static inline void transposeMatrice(const GGMatrix4 *matrice, GGMatrix4 *retValue)
{
    int i, j;
    const pmat mat = (pmat)matrice;
    pmat ret = (pmat)retValue;
    
    for(i=0; i < 3; ++i)
        for(j=0; j<3; ++j)
            ret[i][j] = mat[j][i];
    
    retValue->position.x = -prodScal((Vect3D*)mat[0], (Vect3D*)mat[3]);
    retValue->position.y = -prodScal((Vect3D*)mat[1], (Vect3D*)mat[3]);
    retValue->position.z = -prodScal((Vect3D*)mat[2], (Vect3D*)mat[3]);
    retValue->nul1 = retValue->nul2 = retValue->nul3 = 0.0;
    retValue->unite = 1.0;
}

static inline void transposeMatriceStrange(GGMatrix4 *matrice, 
                                           GGMatrix4 *retValue)
{
    int i, j;
    pmat mat = (pmat)matrice;
    pmat ret = (pmat)retValue;
    
    for(i=0; i < 3; ++i)
        for(j=0; j<3; ++j)
            ret[i][j] = mat[j][i];
    
    ret[0][0] = -mat[0][0];
    ret[1][0] = -mat[0][1];
    ret[2][0] = -mat[0][2];
    ret[0][1] = mat[1][0];
    ret[1][1] = mat[1][1];
    ret[2][1] = mat[1][2];
    ret[0][2] = -mat[2][0];
    ret[1][2] = -mat[2][1];
    ret[2][2] = -mat[2][2];
    
    retValue->position.x = 0;
    retValue->position.y = 0;
    retValue->position.z = 0;
    retValue->nul1 = retValue->nul2 = retValue->nul3 = 0.0;
    retValue->unite = 1.0;
}


static inline void transposeProduitVect3D(const GGMatrix4 *matrice, 
                                          const Vect3D *v, 
                                          Vect3D *ret)
{
    int i;
    pmat mat1 = (pmat)matrice;
    pvect res = (pvect)(ret);
    
    for( i  = 0; i < 3; ++i )
        res[i] = prodScal((Vect3D*)mat1[i], v);
}

static inline void transposeProduit(const GGMatrix4 *matrice1, const GGMatrix4 *matrice2, 
                                    GGMatrix4 *retValue)
{
    int i;
    pmat mat2 = (pmat)matrice2;
    pmat ret = (pmat)retValue;
    Vect3D v;
    
    for( i  = 0; i < 3; ++i )
        transposeProduitVect3D(matrice1, (Vect3D *)mat2[i], (Vect3D *)ret[i]);
    
    diffVect(&(matrice2->position), &(matrice1->position), &v);
    transposeProduitVect3D(matrice1, &v, &(retValue->position));
    retValue->nul1 = retValue->nul2 = retValue->nul3 = 0.0;
    retValue->unite = 1.0;
}


static inline void transposeProduit3D(const GGMatrix4 *matrice1, 
                                      const GGMatrix4 *matrice2, 
                                      GGMatrix4 *retValue)
{
    int i;
    pmat mat2 = (pmat)matrice2;
    pmat ret = (pmat)retValue;
    
    for( i  = 0; i < 3; ++i )
        transposeProduitVect3D(matrice1, (Vect3D *)mat2[i], (Vect3D *)ret[i]);
    
    retValue->nul1 = retValue->nul2 = retValue->nul3 = 0.0;
    retValue->unite = 1.0;
    SetZeroVect(retValue->position);
}


static inline void produitMatriceVecteur3D(const GGMatrix4 *matrice, 
                                           const Vect3D *vecteur, 
                                           Vect3D *retValue)
{
    int i, k;
    GGReal sum;
    pmat mat1 = (pmat)matrice;
    pvect res = (pvect)(retValue);
    pvect arg = (pvect)vecteur;
    
    for(i = 0; i < 3; ++i)
    {
        sum = 0.0;
        for(k = 0; k < 3; ++k)
            sum += mat1[k][i]*arg[k];
        res[i] = sum;
    }
}

static inline void produitMatrice3D(const GGMatrix4 *matrice1, const GGMatrix4 *matrice2, 
                                    GGMatrix4 *retValue)
{
    int i;
    pmat mat2 = (pmat)matrice2;
    pmat ret = (pmat)retValue;
    
    for( i  = 0; i < 3; ++i )
        produitMatriceVecteur3D(matrice1, (Vect3D *)mat2[i], (Vect3D *)ret[i]);
    retValue->nul1 = retValue->nul2 = retValue->nul3 = 0.0;
    retValue->unite = 1.0;
    SetZeroVect(retValue->position);
}

static inline void produitMatrice(const GGMatrix4 *matrice1, const GGMatrix4 *matrice2, 
                                  GGMatrix4 *retValue)
{
    int i;
    pmat mat2 = (pmat)matrice2;
    pmat ret = (pmat)retValue;
    
    for( i  = 0; i < 4; ++i )
        produitMatriceVecteur3D(matrice1, (Vect3D *)mat2[i], (Vect3D *)ret[i]);
    addVect(&(matrice1->position), &(retValue->position), &(retValue->position));
    
    retValue->nul1 = retValue->nul2 = retValue->nul3 = 0.0;
    retValue->unite = 1.0;
}

static inline void transformePoint4D(const GGMatrix4 *matrice, 
                                     const Vect3D *vect, 
                                     Vect3D *result)
{
    pmat mat = (pmat)matrice;
    produitMatriceVecteur3D(matrice, vect, result);
    addVect(result, (Vect3D*)mat[3], result);
}

static inline void transposeTransformePoint4D(const GGMatrix4 *matrice, 
                                              const Vect3D *vect, 
                                              Vect3D *result)
{
    Vect3D temp;
    diffVect(vect, &matrice->position, &temp);
    transposeProduitVect3D(matrice, &temp, result);
}

static inline void transformeToLocalGalileen(GGGalileen    *gal,
                                             PetitGalileen *obj,
                                             PetitGalileen *res)
{
    Vect3D tmp;
    diffVect(&(obj->point), &(gal->matrice.position), &tmp);
    transposeProduitVect3D(&(gal->matrice), &tmp, &(res->point));
    diffVect(&(obj->speed), &(gal->speed), &tmp);
    transposeProduitVect3D(&(gal->matrice), &tmp, &(res->speed));
}

static inline void transformeGalileen(GGGalileen    *gal,
                                      PetitGalileen *obj,
                                      PetitGalileen *res)
{
    transformePoint4D(&gal->matrice, &obj->point, &res->point);
    produitMatriceVecteur3D(&gal->matrice, &obj->speed, &res->speed);
    addVect(&res->speed, &gal->speed, &res->speed);
}


static inline void produitGalileen(const GGGalileen *gal1, const GGGalileen *gal2,
                                   GGGalileen *res)
{
    produitMatrice(&gal1->matrice, &gal2->matrice, &res->matrice);
    produitMatriceVecteur3D(&gal1->matrice, &gal2->speed, &res->speed);
    addVect(&gal1->speed, &res->speed, &res->speed);
}

static inline void transposeProduitGalileen(const GGGalileen *gal1, const GGGalileen *gal2,
                                            GGGalileen *res)
{
    Vect3D tmp;
    transposeProduit(&gal1->matrice, &gal2->matrice, &res->matrice);
    diffVect(&gal2->speed, &gal1->speed, &tmp);
    transposeProduitVect3D(&gal1->matrice, &tmp, &res->speed);
}

static inline void transposeGalileen(GGGalileen *gal, GGGalileen *res)
{
    Vect3D tmp;
    transposeMatrice(&gal->matrice, &res->matrice);
    mulScalVect(&gal->speed, -1, &tmp);
    transposeProduitVect3D(&gal->matrice, &tmp, &res->speed);
}

static inline void transformePlan(GGMatrix4 *mat, GGPlan *p, GGPlan *res)
{
    transposeProduitVect3D(mat, &p->v, &res->v);
    res->w = prodScal(&mat->position, &p->v) + p->w;
}

GGBox transformeBox(const GGMatrix4* mat, GGBox inbox);

static inline  GGBox unionBox(GGBox *box1, GGBox *box2)
{
    GGBox result;
    // x
    result.min.x = MIN(box1->min.x, box2->min.x);
    result.max.x = MAX(box1->max.x, box2->max.x);
    result.min.y = MIN(box1->min.y, box2->min.y);
    result.max.y = MAX(box1->max.y, box2->max.y);
    result.min.z = MIN(box1->min.z, box2->min.z);
    result.max.z = MAX(box1->max.z, box2->max.z);
    return result;
}

static inline BOOL pointInPlane(GGPlan *p, Vect3D *res)
{
    return prodScal(&p->v, res) + p->w >= 0;
}

static inline GGReal determinant(GGMatrix4 *mat)
{
    Vect3D u = mat->gauche;
    Vect3D v = mat->haut;
    Vect3D w = mat->direction;
    return u.x * (v.y * w.z - v.z*w.y) -u.y * (v.x*w.z - v.z*w.x) + 
        u.z * (v.x*w.y - v.y*w.x);
}

static inline GGReal distanceDroitePoint(Vect3D *orig, Vect3D *dir, 
                                         Vect3D *point, GGReal *xpos)
/*assume dir is of norme 1*/
{
    GGReal tmp;
    Vect3D ab;
    mkVectFromPoint(orig, point, &ab);
    *xpos = tmp = prodScal(&ab, dir);
    return sqrt(prodScal(&ab, &ab)-(tmp)*(tmp));
}

#pragma mark -
#pragma mark advanced functions

void normaliseMatrice(GGMatrix4 *mat);
void GGMatrixLookAt(GGMatrix4 *mat, const Vect3D *lookAtRef);
void rotateFrameWithLocalAngularSpeed(GGMatrix4 *mat, Vect3D* angularSpeed, GGReal delta);

#pragma mark -
#pragma mark logging stuff
NSString *stringOfVect2(Vect3D v);
NSString *stringOfVect(Vect3D *v);
NSString *stringOfMatrice(GGMatrix4 *aMat);
NSString* stringOfBox(GGBox box);

#endif