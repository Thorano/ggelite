/* 	-*-ObjC-*- */
/*
 *  World.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __World_h
#define __World_h

#import "GGNode.h"
#import <Foundation/NSGeometry.h>
#import "config.h"

#import "GGNode.h"


GGBEGINDEC

@class GGSky;
@class GGOrbiteur;
@class Sched;
@class PlaneteManager;
@class GGInput;
@class GGShip;
@class Commandant;
@class Laser;
@class Flare;
@class System;
@class NSMutableDictionary;
@class WorldArchiver;
@class Tunnel;
@class Complex_station_manager;
@class Station;



/*%%%
import System;
%%%*/

@class WorldState;

@interface World : GGNode <WorldManager>
{
    int         indexSystem;
    WorldState	*state;
    GGRepere		*leRepereLocal;
    NSArray		*planeteArray;
    GGSpaceObject		*mainCamera, *auxiliaryCamera;
    GGOrbiteur		*theClosest, *theLightSource;
    
    System		*starSytem;
    Vect4D		lightPosition;
    GGSky			*theSky;
    Flare			*flares;
    
    
    
    NSMutableArray	*needToDie;
    int               	isInit;
    ProjectionParam	projectionParam;
    WorldArchiver		*wa;
    BOOL			isPaused;
    BOOL			checkCollision;
    BOOL			drawWorld;
}

- (void) setCheckCollision: (BOOL) f;
- (void) setDrawWorld: (BOOL) flag;
- (void) setPaused: (BOOL) flag;
- (GGRepere *)repere;

- (void) updateProjection;

- (NSString *) starNameForIndex: (int) index;

- (void) setSystem: (System *)theSystem;
- (System *)currentSystem;
- (int) currentSystemIndex;
- (void) jumpToSystemIndex: (int) index;

- (Tunnel *) tunnelForHyperJumpFrom: (int) source
			destination: (int) dest
			       ship: (GGShip *)ship;
- (void) destroyTunnel: (Tunnel *) t;
- (Tunnel *) pnjEnterHyperSpace: (GGShip *)ship
		      forSystem: (int) dest;
- (void) pnjLeavingHyperSpace: (GGShip *)ship
		    forSystem: (int) index;


- (void) loadBaseSystem;
- (NSArray *) allOrbiteur;
- (NSArray *) allStarPort;
- (GGOrbiteur *) orbiteurByName: (NSString *)name;
- (Commandant *) commandant;
- (void) setCommandant: (Commandant *) obj;
- (void) setMainCamera:(GGSpaceObject *) obj;
- (void) setAuxiliaryCamera:(GGSpaceObject *)obj;
- (GGSpaceObject *) camera;
- (void) getProjectionParam: (ProjectionParam *)ppp;
//- (PlaneteManager *) systeme;
- (void) setSky: (GGSky *) s;
- (GGSky *) sky;

- (Complex_station_manager *) stationManagerFor: (Station *) station;
- (NSArray *) currentShips;

/* this method add the ship to current system.  It assumes that the ship 
   has already been declared to the World.  Use it, for instance, when a
   ship enter the current system, via an hyperjump.  Or, when a ship 
   leaves an orbital station.  It enters the scenary graph again.

   Actually, it simply calls [ship beginSchedule].
*/
- (void) makeCurrent: (GGSpaceObject *) ship;

/* This method is called when a new ship (in fact, any mobile object)
   enters the game.  It assumes that the ship enters in the current system
*/
- (void) addShip: (GGSpaceObject *) aSpaceObject;

- (void) restoreShips: (NSArray *) array;
- (void) setSky: (GGSky *) s;

- (void) drawScene: (NSRect) aFrame;
- (void) checkForCollision;

- (void) manageLaser: (Laser *) l;
- (void) genereExplosionFrom: (GGSpaceObject *) obj
		  withEnergy: (float) val;
- (void) addToKillPool: obj
	      selector: (SEL) s;

- (void) saveGame: (NSString *) fileName;
- (void) loadGame: (NSString *) fileName;
- (GGOrbiteur *) closestOrbiteur;
- (void) setClosestOrbiteurRec;
@end

extern World *theWorld;
extern NSString *GGNotificationLeavingSystem;
extern NSString *GGNotificationEnteringSystem;
extern NSString *GGNotificationBeforeLoadingGame;
extern NSString *GGNotificationAfterLoadingGame;

id lookupObjet(id obj);

GGENDDEC

@interface GGArchiver : NSObject <NSCoding>
{
  NSString *name;
}
+ ggArchiver: (NSString *)str;
@end

@interface NSString (GGMethod)
- objectFromSignature;
@end

#endif
