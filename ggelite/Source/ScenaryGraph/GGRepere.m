/*
 *  GGRepere.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <math.h>
#import <time.h>

#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import "Metaobj.h"
#import "GGLoop.h"
#import "GGSpaceObject.h"
#import "GGShip.h"
#import "GGRepere.h"
#import "utile.h"
#import "Tableau.h"
#import "GGOrbiteur.h"
#import "Preference.h"
#import "World.h"
#import "utile.h"
#import "Commandant.h"

// dynamical stuff
#import "GGDynamicSimulator.h"
#import "GGSolid.h"



@implementation GGRepere
- init
{
	[super init];
	
	modeRepere = RepereNormal;
	shouldMove = YES;
	_dynSimulator = [[GGDynamicSimulator alloc] init];
	_currentCameraLimit = distanceCameraRepere2;
	_zmin = 20;
	_zmax = 3000;
	
#if defined(DEBUG) && defined(HAVE_COCOA)
	[[GGDynamicDebugController controller] setSimulator:_dynSimulator];
	//  _flags.graphical = YES;
#endif    
	
	return self;
}

- (void) dealloc
{
    RELEASE(_dynSimulator);
    
    [super dealloc];
}

- (BOOL) shouldSave
{
  return YES;
}

- (id)replacementObjectForArchiver:(NSArchiver *)anArchiver
{
  return [GGArchiver  ggArchiver:@"reploc:"];
}

- (void) setZMin:(double)zmin zMax:(double)zMax
{
	_zmin = zmin;
	_zmax = zMax;
}


- (Mode_Repere) modeRepere
{
    return modeRepere;
}

- (void) setModeRepere:(Mode_Repere)val
{
    modeRepere = val;
}

- (void) updateOrigine: (GGSpaceObject *) obj
  /*
   *modifie l'origine du repère en supposant que obj vit dans les coordonnées
   *locales dudit repère. Cela donne des subtilités dans la formule de 
   *changement de repère.
   * Si le repère est centré sur une planète, il faut repasser en repère
   * flottant. De tout façon, obj est nécessairement la caméra, donc
   * ce ne peut jamais être une planète.
   */
{
#ifdef DEBUG
  {
    Vect3D pos, vit;
    pos = [obj getPositionSpeedAbsolues: &vit];
    NSDebugMLLog(@"updaterep", @"pos : %@ speed : %@", 
		 stringOfVect2(pos), stringOfVect2(vit));
  }
#endif

  if( !shouldMove )
    return;

  NSAssert1([self isParentOf: obj], @"%@ is not a son of self", obj);

  if( modeRepere == ReperePlanete )
    {
      Vect3D i = {1, 0, 0};
      Vect3D j = {0, 1, 0};
      Vect3D k = {0, 0, 1};
      GGGalileen gal0, gal1;
//        Vect3D posObj;
  
      //  SPEEDUP:
  
      [obj galileanTransformationTo: self
	   in: &gal0];
      transposeGalileen(Galileen(self), &gal1);
      //      transformePoint4D(&gal1.matrice, &gal0.matrice.position, &posObj);
      gal1.matrice.position = gal0.matrice.position;
//        transposeProduitVect3D(Matrice(self), Speed(self), &vcom);
//        mulScalVect(&vcom, -1, &vcom);

//        gal0.matrice.gauche = i;
//        gal0.matrice.haut = j;
//        gal0.matrice.direction = k;
//        gal0.speed = vcom;

      [self galileanTransformationNodeWith: &gal1];
      Matrice(self)->gauche = i;
      Matrice(self)->haut = j;
      Matrice(self)->direction = k;
      
      
      NSDebugMLLog(@"updaterep", @"speed should be nul %@", 
		   stringOfVect(Speed(self)));
      NSDebugMLLog(@"updaterep", @"position should be nul %@", 
		   stringOfVect(Point(self)));
    }
  else
    {
      [self translateNodeTo: obj];
      [self rectifyAxes];
      [self setMode: GGInertialMode];
    }

#ifdef DEBUG
  {
    Vect3D pos, vit;
    pos = [obj getPositionSpeedAbsolues: &vit];
    NSDebugMLLog(@"updaterep", @"pos : %@ speed : %@", 
		 stringOfVect(Point(self)), stringOfVect(Speed(self)));
  }
#endif
  //  NSDebugMLLog(@"updaterep", @"position : %@", [self getStringPosition]);
}

- (void) recenterFrameOnObjectIfNeeded:(GGSpaceObject*) obj
{
    double d;
    d = (prodScal(Point(obj), Point(obj)));
    if( d > _currentCameraLimit )
    {
        NSDebugMLLog(@"updaterep", @"obj : %@ rep : %@", 
                     [obj getStringPosition],
                     [self getStringPosition]);
        [self updateOrigine: obj];
    }    
}

- (void) willDrawSons: (Camera *)cam
{
  //      glShadeModel(GL_FLAT);

	if(_zmin >= 0){
        GGBox box = [self localBoudingBox];
        GGReal diameter = distanceManhattan(&box.min, &box.max);

        NSDebugMLLogWindow(@"updaterep", @"box = %@\ndiameter = %g", stringOfBox(box), diameter);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
		glDepthRange(0, 0.94);
		glDepthFunc(GL_LESS);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(cam->param->fovy, cam->param->ratio,_zmin,diameter);
		glMatrixMode(GL_MODELVIEW);
	}		
}

- (void) hasDrawnSons: (Camera *)cam
{
#ifdef DEBUG
    [_dynSimulator rasterise:cam];
#endif
}

- (void) beginSchedule
  /*I don't wan't to call super because here it has no meaning (according
   *to the job that does super in the current implementation
   */
{
    [super beginSchedule];
    [self setMode: GGInertialMode];
    [[theLoop fastScheduler] addTarget:self withSelector:@selector(updateLocalDynamic:) arg:nil priority:80];
}

- (void) endSchedule
{
    [[theLoop fastScheduler] deleteTarget:self
                             withSelector:@selector(updateLocalDynamic:)];
    [super endSchedule];
}

- (void) changeLocalCoordinates: (GGGalileen *) pgal
{
  [super changeLocalCoordinates: pgal];
  [self rectifyAxes];
}

#pragma mark -

- (GGSolid*)_createDynamicalHelper:(GGSpaceObject*)aNode
{
    GGSolid* helper = [aNode createDynamicalHelper];
    NSDebugMLLog(@"updaterep", @"got %@ with helper %@", aNode, helper);
    if(helper){
        [aNode setDynamicalHelper:helper];
        [_dynSimulator addBody:helper];
        if(CollisionCounter(aNode) >= 0){
            [_dynSimulator addForCollision:helper];
        }
    }
    return helper;
}

- (void) updateLocalDynamic:(id)obj
{
    GGSpaceObject* aSon;
    NSEnumerator* sonsEnum = [[self subNodes] objectEnumerator];
    while((aSon = [sonsEnum nextObject])!=nil){
        GGSolid* solid = [aSon dynamicalHelper];
        if(nil == solid){
            /* try to create it just in case.  This is an absolutely ugly workaround.  When we reload a game, the helper in not properly created because the code path through initWithCoder involves adding an object to the scenary graph that does not have a model.*/
            solid = [self _createDynamicalHelper:aSon];            
        }
        if(solid){
            solid->_position = *Matrice(aSon);
            solid->_speed = *Speed(aSon);
            solid->_angularSpeed = *Omega(aSon);
//            [solid setMasse:Masse(aSon)];
        }
    }
    
    // here, this is very specific.  That should probably be fixed
    GGSpaceObject* closest = [self nodePere];
    Vect3D gravity = GGZeroVect;
    if(closest){
        GGOrbiteur* planete = nil;
        if([closest isKindOfClass:[GGOrbiteur class]]){
            planete = (GGOrbiteur*)closest;
        }
        while(nil == planete && closest != nil){
            if([closest isKindOfClass:[GGNode class]]){
                planete = [(GGNode*)closest originalOrbiteur];
            }
            closest = [closest nodePere];
        }
        
        
        if(planete){
            Vect3D tmp;
            mulScalVect(&repere.matrice.position,-1.0,&tmp);
            transposeProduitVect3D(&repere.matrice,&tmp,&gravity);
            GGReal norm;

            if(normaliseVectAndGetNorm(&gravity, &norm)){
                mulScalVect(&gravity,
                            6.67e-11 * planete->masse / (norm*norm),
                            &gravity);
                _currentCameraLimit = norm / 100;
                NSDebugMLLogWindow(@"Gravity", @"grav = %g %@\ncamLim = %g km", 
                                   normeVect(&gravity), stringOfVect2(gravity), _currentCameraLimit / 1000.0);                
            }
        }
    }
    NSDebugMLLogWindow(@"Gravity", @"_currentCameraLimit = %g km", (double)_currentCameraLimit / 1000.0);
    [_dynSimulator setGravity:gravity];
    [_dynSimulator advanceTimeByDelta:deltaTime];
    sonsEnum = [[self subNodes] objectEnumerator];
    while((aSon = [sonsEnum nextObject])!=nil){
        GGSolid* solid = [aSon dynamicalHelper];
        if(solid){
//            NSDebugMLLog(@"updaterep", @"\"%@\" : %g\n  speed=%@\nang=%@\nmat=\n%@",
//                aSon,
//                solid->_masse,
//                stringOfVect2(solid->_speed),
//                stringOfVect2(solid->_angularSpeed),
//                stringOfMatrice(&solid->_position));
            *Speed(aSon) = solid->_speed;
            *Omega(aSon) = solid->_angularSpeed;
            *Matrice(aSon) = solid->_position;
        }
    }
}

- (void)willRemoveSubNode:(GGSpaceObject*)aNode
{
    GGSolid* solid = [aNode dynamicalHelper];
    NSDebugMLLog(@"updaterep", @"will remove %@ with helper %@", aNode, solid);
    if(solid){
        [_dynSimulator removeSolidFromSimulation:solid];
        [aNode setDynamicalHelper:nil];
    }
}

- (void)didAddSubNode:(GGSpaceObject*)aNode
{
    NSAssert([aNode dynamicalHelper] == nil, @"what, got an helper!");
    [self _createDynamicalHelper:aNode];
}

@end
