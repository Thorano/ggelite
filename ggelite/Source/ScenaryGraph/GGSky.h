/* 	-*-ObjC-*- */
/*
 *  GGSky.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGSky_h
#define __GGSky_h

#define SIZEMAX 10000

#import "GGSpaceObject.h"
@class Texture;
@class GGGlyphLayout;
@class GGFont;
@class NSMutableData;

typedef struct __star
{
  Vect3D	pos;
  //  float		x, y, z;
  float		mag0, intensity;
  int		index, indexStar;
  GGGlyphLayout	*stringLayout;
  double		xd, yd, zd;
}Star;

@interface StarsZone : NSObject <NSCoding>
{
@public
  int 		size;
  int		allocated;
  int		*indices;
}
+ starsZone;
- (void) addIndex: (int) val;
- (int) randomIndexCloseTo: (int) index;
@end

@interface GGSky : NSObject
{
@public
  Star		theStars[SIZEMAX];
  int		nStars;
  BOOL		isCompile;
  GLint		idList;
  Texture	*dev, *der, *ha, *ba, *dr, *ga;
  GGFont	*fontStar;
  NSMutableData	*bigString;
  NSDictionary	*starsZones;
  int		wishIndex;
}
+ (void) postIntall;
- initAtOrigin: (Vect3D) origin;
- (void) drawSky: (Camera *) cam;
- (void) genereSkyForIndex: (int) index;
- (void) genereSkyAt: (Vect3D) origin;
- (Star *) stars: (int*)size;
- (char *) names;
- (char *) cNameOf:(int) index;
- (char *) cNameOfStar: (Star *)s;
- (StarsZone *) starsZoneForVect: (Vect3D *)pos;
- (int) randomStarIndexCloseTo: (int) index;
- (Vect3D) starPosForIndex: (int) index;
- (GGReal) distanceWith: (int) i
		  and: (int) j;
- (void) setDestination: (int) index;
@end


@interface ZoneKey : NSObject <NSCoding, NSCopying>
{
@public
  int x, y, z;
}
+ keyWith: (int) _x
      and: (int) _y;
+ keyWith: (int) _x
      and: (int) _y
      and: (int) _z;
@end


extern BOOL sky_dump;
#endif
