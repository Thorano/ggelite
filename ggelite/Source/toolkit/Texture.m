/*
 *  Texture.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdlib.h>
#import <stdio.h>

#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSDebug.h>
#import "Metaobj.h"
#import "Texture.h"
#import "Preference.h"
#import "Resource.h"
#import "utile.h"
#import "GGImage.h"

static NSMutableDictionary *texturesDic;

@interface GGImage (GenCube) <TexInitializer>
- (int) nCol;
- (void) colorBoxForPoint: (double [3]) pts
                       in: (unsigned char *) dest;

@end

@implementation GGImage (GenCube)
- (int) nCol
{
    switch([self ncomp]){
        case 3:
        case 4:
            return 3;
        case 1:
            return 1;
        default:ggabort();
    }
    return -1;
}

- (void) colorBoxForPoint: (double [3]) val
                       in: (unsigned char *) dest
{
    double n, r, s, t;
    double theta, phi;
    double a, b;
    int i, j, index;
    r = val[0];
    s = val[1];
    t = val[2];
    n = sqrt(r*r+s*s+t*t);
    r /= n;
    s /= n;
    t /= n;
    theta = asin(t);
    phi = atan2(s,r);
    
    a = (phi)/(2*M_PI);
    b = (theta + (M_PI/2))/M_PI;
    
    if( a < 0 )
        a += 1;
    
    i = floor(a*_width);
    j = floor(b*_height);
    NSAssert(i>=0 && j>=0, @"bad");
    
    if( i >= _width ) i = _width-1;
    if( j >= _height ) j = _height-1;
    
    
//    index = _ncomp*(j*_width+i);
    index = _bytesPerRow*j + _ncomp*i;
    int nCol = [self nCol];
    const char *dataChar = [self rawData];
    
    for(i = 0; i < nCol; ++i, dest++, index++)
        *dest = dataChar[index];
}
@end

@implementation Texture

+ (void)initialize
{
    if( self == [Texture class])
    {
        texturesDic = [NSMutableDictionary new];
    }
}

+ (void) addToDic: (Texture *)obj 
           forKey: (NSString *)str
{
    //  [texturesDic setObject: obj forKey: str];
}

+ boxTexture: (id <TexInitializer>) obj
{
    Texture *t;
    t = [[self alloc] initBoxTexture: obj
                                size: 256
                              mipmap: thePref.GLMipmap];
    return AUTORELEASE(t);
}


+ textureWithFileName: (NSString *)fileName
               mipmap: (BOOL) mipmap
{
    if( fileName )
    {
        fileName = [[Resource resourceManager] pathForTexture:   fileName];
        if(fileName){
            return AUTORELEASE([[self alloc] initWithTextureFile: fileName
                                                      mipmap: mipmap]);
        }
        else{
            return nil;
        }
    }
    else
        return nil;
}

+ textureWithFileName: (NSString *)fileName
{
    return [self textureWithFileName: fileName
                              mipmap: YES];
}


+ stupidTexture
{
    return AUTORELEASE([self new]);
}

+ cubeMapTextureWithFileName: (NSString *)fileName
{
    return [self boxTexture:[GGImage imageWithName:fileName]];
}

- (void) dealloc
{
    glDeleteTextures(1, &idTexture);
    NSDebugMLLog(@"Texture", @"destroying texture %@", self);
    [super dealloc];
}

- (int) idTexture
{
    return idTexture;
}

- (void) __commonInit
{
    [super init];
    glGenTextures(1,&idTexture);
    addToCheck(self);
}

- (void) _setTextureParamsMipmap: (BOOL) mipmap
                            dest: (int) dest
{
    glTexParameteri(dest,GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameteri(dest,GL_TEXTURE_WRAP_T,GL_REPEAT);
    
    if( thePref.GLMipmap && mipmap)
    {
        glTexParameteri(dest,GL_TEXTURE_MIN_FILTER,
                        GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(dest,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    }
    else
    {
        glTexParameteri(dest,GL_TEXTURE_MIN_FILTER,
                        GL_NEAREST);
        glTexParameteri(dest,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    }
    
}

- (void) _setTextureParamsMipmap: (BOOL) mipmap
{
    [self _setTextureParamsMipmap: mipmap
                             dest: GL_TEXTURE_2D];
}

#define LoadTexture(Unit)  if( mipmap )\
gluBuild2DMipmaps(Unit, interalFormat, size, \
                  size, format, GL_UNSIGNED_BYTE, \
                  (GLvoid *)data);\
else \
glTexImage2D(Unit, 0, interalFormat, size, \
             size, 0, format, GL_UNSIGNED_BYTE, \
             (GLvoid *)data)
- initBoxTexture: (id <TexInitializer>) obj
            size: (int) size
          mipmap: (BOOL) mipmap
{
    int ncol;
    void (*fn)(id bla, SEL s, double val[3], unsigned char *);
    SEL  sel;
    unsigned char *data;
    double tex[3];
    int i, j;
    double size2;
    GLint		format, interalFormat;
    
    
    
    [self __commonInit];
    ncol = [obj nCol];
    
    if( ncol == 3 ) /* RGB image */
    {
        interalFormat = GL_RGB;
        format = GL_RGB;
    }
    else
    {
        NSParameterAssert(1 == ncol);
        interalFormat = GL_LUMINANCE;
        format = GL_LUMINANCE;
    }
    sel = @selector(colorBoxForPoint:in:);
    fn = (void (*)(id, SEL, double[3], unsigned char *))[(NSObject*)obj methodForSelector: sel];
    
    glBindTexture(GL_TEXTURE_CUBE_MAP, idTexture);
    
    data = malloc(ncol*size*size);
    NSAssert(data != NULL, @"Not Enough Memory");
    size2 = size>>1;
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    
    // X-MAJOR-POSITIVE
    tex[0] = 1.0;
    for(i = 0; i < size; ++i)
    {
        tex[2] = -((double)i-size2)/size2;
        for(j = 0; j < size; ++j)
        {
            int index;
            index = ncol*(i+j*size);
            tex[1] = -((double)j-size2)/size2;
            fn(obj, sel, tex, data+index);
        }
    }
    LoadTexture(GL_TEXTURE_CUBE_MAP_POSITIVE_X);
    //   glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, interalFormat, size, 
    // 	       size, 0, format, GL_UNSIGNED_BYTE, 
    // 	       (GLvoid *)data);
    
    // X-MAJOR-NEGATIVE
    tex[0] = -1.0;
    for(i = 0; i < size; ++i)
    {
        tex[2] = ((double)i-size2)/size2;
        for(j = 0; j < size; ++j)
        {
            int index;
            index = ncol*(i+j*size);
            tex[1] = -((double)j-size2)/size2;
            fn(obj, sel, tex, data+index);
        }
    }
    LoadTexture(GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
    //   glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, interalFormat, size, 
    // 	       size, 0, format, GL_UNSIGNED_BYTE, 
    // 	       (GLvoid *)data);
    
    
    //Y-MAJOR-POSITIVE
    tex[1] = 1.0;
    for(i = 0; i < size; ++i)
    {
        tex[0] = ((double)i-size2)/size2;
        for(j = 0; j < size; ++j)
        {
            int index;
            index = ncol*(i+j*size);
            tex[2] = ((double)j-size2)/size2;
            fn(obj, sel, tex, data+index);
        }
    }
    LoadTexture(GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
    //   glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, interalFormat, size, 
    // 	       size, 0, format, GL_UNSIGNED_BYTE, 
    // 	       (GLvoid *)data);
    
    
    //Y-MAJOR-NEGATIVE
    tex[1] = -1.0;
    for(i = 0; i < size; ++i)
    {
        tex[0] = ((double)i-size2)/size2;
        for(j = 0; j < size; ++j)
        {
            int index;
            index = ncol*(i+j*size);
            tex[2] = -((double)j-size2)/size2;
            fn(obj, sel, tex, data+index);
        }
    }
    LoadTexture(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
    //   glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, interalFormat, size, 
    // 	       size, 0, format, GL_UNSIGNED_BYTE, 
    // 	       (GLvoid *)data);
    
    //Z-MAJOR-POSITIVE
    tex[2] = 1.0;
    for(i = 0; i < size; ++i)
    {
        tex[0] = ((double)i-size2)/size2;
        for(j = 0; j < size; ++j)
        {
            int index;
            index = ncol*(i+j*size);
            tex[1] = -((double)j-size2)/size2;
            fn(obj, sel, tex, data+index);
        }
    }
    LoadTexture(GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
    //   glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, interalFormat, size, 
    // 	       size, 0, format, GL_UNSIGNED_BYTE, 
    // 	       (GLvoid *)data);
    
    
    //Z-MAJOR-NEGATIVE
    tex[2] = -1.0;
    for(i = 0; i < size; ++i)
    {
        tex[0] = -((double)i-size2)/size2;
        for(j = 0; j < size; ++j)
        {
            int index;
            index = ncol*(i+j*size);
            tex[1] = -((double)j-size2)/size2;
            fn(obj, sel, tex, data+index);
        }
    }
    LoadTexture(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);
    //   glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, interalFormat, size, 
    // 	       size, 0, format, GL_UNSIGNED_BYTE, 
    // 	       (GLvoid *)data);
    
    [self _setTextureParamsMipmap: mipmap
                             dest: GL_TEXTURE_CUBE_MAP];
    free(data);
    _width = size;
    _height = size;
    _kind = GGTextureCubeMap;
    
    return self;
}

- buildWithJPGFileName: (NSString *)fileName
                mipmap: (BOOL) mipmap
{
    GLenum gluerr;
    GLint		format, interalFormat;
    
    NSDebugMLLog(@"Texture", @"Loading Texture %@", fileName);
    
    
    GGImage* image = [GGImage imageFromPath:fileName];
    
    if( image == nil )
    {
        NSWarnMLog(@"could not load texture %@", fileName);
        RELEASE(self);
        return nil;
    }
    
    glBindTexture(GL_TEXTURE_2D,idTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glPixelStorei(GL_UNPACK_ROW_LENGTH,[image scanLine]);
    
    if( [image ncomp] == 3 ) /* RGB image */
    {
        interalFormat = GL_RGB;
        format = GL_RGB;
    }
    else if ([image ncomp] == 4)
    {
        interalFormat = GL_RGB;
        format = GL_RGBA;
    }
    else if ([image ncomp] == 1)
    {
        interalFormat = GL_LUMINANCE;
        format = GL_LUMINANCE;
    }
    else
    {
        NSLog(@"Cannot init a texture with %d as depth", [image ncomp]);
        RELEASE(self);
        return nil;
    }
    if( mipmap )
    {
        if((gluerr=gluBuild2DMipmaps(GL_TEXTURE_2D, interalFormat, [image width], 
                                     [image height], format, GL_UNSIGNED_BYTE, 
                                     (GLvoid *)[image rawData]))) 
        {
            NSWarnMLog(@"GLULib%s\n",gluErrorString(gluerr));
            exit(-1);
        }
    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, interalFormat, [image width], 
                     [image height], 0, format, GL_UNSIGNED_BYTE, 
                     (GLvoid *)[image rawData]);
    }
    
    [self _setTextureParamsMipmap: mipmap];
    glPixelStorei(GL_UNPACK_ROW_LENGTH,0);
    _width =  [image width];
    _height = [image height];
    _kind = GGTextureRegular;
    
    addToCheck(self);
    return self;
}


- initWithTextureFile:(NSString *)fileName
               mipmap:(BOOL) mipmap
{
    Texture *t = [texturesDic objectForKey: fileName];
    [self __commonInit];
    if( t != nil )
    {
        RELEASE(self);
        return RETAIN(t);
    }
    else
    {
        self =  [self buildWithJPGFileName: fileName
                                    mipmap: mipmap];
        if( self != nil )
        {
            [Texture addToDic: self forKey: fileName];
            return self;
        }
        else
            return nil;
    }
}

- initWithData: (void *)dataChar
         width: (int) width
        height: (int) height
      channels: (int) ncomp
        mipmap: (BOOL) mipmap
{
    GLint		format, interalFormat;
    GLenum 	gluerr;
    [self __commonInit];
    
    glBindTexture(GL_TEXTURE_2D,idTexture);
    
    if( ncomp == 3 ) /* RGB image */
    {
        interalFormat = GL_RGB;
        format = GL_RGB;
    }
    else if (ncomp == 4)
    {
        interalFormat = GL_RGBA;
        format = GL_RGBA;
    }
    else
    {
        interalFormat = GL_LUMINANCE;
        format = GL_LUMINANCE;
    }
    
    if( mipmap )
    {
        if((gluerr=gluBuild2DMipmaps(GL_TEXTURE_2D, interalFormat, width, 
                                     height, format, GL_UNSIGNED_BYTE, 
                                     (GLvoid *)dataChar))) 
        {
            NSWarnMLog(@"GLULib%s\n",gluErrorString(gluerr));
            exit(-1);
        }
    }
    else
    {
        glTexImage2D(GL_TEXTURE_2D, 0, interalFormat, width, 
                     height, 0, format, GL_UNSIGNED_BYTE, 
                     (GLvoid *)dataChar);
    }
    
    [self _setTextureParamsMipmap: mipmap];
    _width =  width;
    _height = height;
    _kind = GGTextureRegular;
    
    return self;
}

- (void) setSubImage: (GGImage *)subImage
              format: (GLint) format
             xoffset: (int) xoffset
             yoffset: (int) yoffset
{
    glBindTexture(GL_TEXTURE_2D, idTexture);
    glPixelStorei(GL_UNPACK_ROW_LENGTH,[subImage scanLine]);
    glPixelStorei(GL_UNPACK_ALIGNMENT,4);

    glTexSubImage2D(GL_TEXTURE_2D, 0, xoffset, yoffset, [subImage width], [subImage height],
                    format, GL_UNSIGNED_BYTE, [subImage rawData]);
    glPixelStorei(GL_UNPACK_ROW_LENGTH,0);
}

- initStartField: (void *)data 
           width: (int) w 
          height: (int) h
{
    [self __commonInit];
    
    glBindTexture(GL_TEXTURE_2D,idTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, SIZETEXTURESKY);
    glTexImage2D(GL_TEXTURE_2D, 		// target
                 0,			// mipmap level
                 GL_INTENSITY4,		// Format interne (je panne rien !!)
                 w, 			// largeur
                 h, 			// hauteur
                 0,			// bord ?
                 GL_LUMINANCE,		// Format de texture (type)
                 GL_UNSIGNED_BYTE, 	// encoding
                 data			// data
                 );
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST);
    
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    
    
    if( thePref.GLMipmap )
    {
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    }
    else
    {
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    }
    
    return self;
}


- (void) changeStarField: (void *)data 
                   width: (int) w 
                  height: (int) h
{
    glBindTexture(GL_TEXTURE_2D,idTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glTexSubImage2D(GL_TEXTURE_2D, 		// target
                    0,			// mipmap level
                    0, 0, 		// coordonnée de la sous image
                    w, 			// largeur
                    h, 			// hauteur
                    GL_LUMINANCE,		// Format de texture (type)
                    GL_UNSIGNED_BYTE, 	// encoding
                    data			// data
                    );
}


- initForFont: (void *) data
        width: (int) w
       height: (int) h
{
    [self __commonInit];
    
    glBindTexture(GL_TEXTURE_2D,idTexture);
    
    glPixelStorei(GL_UNPACK_ALIGNMENT,1);
    glTexImage2D(GL_TEXTURE_2D, 		// target
                 0,			// mipmap level
                 GL_ALPHA4,		// Format interne (je panne rien !!)
                 w, 			// largeur
                 h, 			// hauteur
                 0,			// bord ?
                 GL_ALPHA,		// Format de texture (type)
                 GL_UNSIGNED_BYTE, 	// encoding
                 data			// data
                 );
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST);
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    _width = w;
    _height = h;
    _kind = GGTextureRegular;
    
    return self;
}

- init
    /*make a stupid bitmap dummy texture 8x10.
    *used by GGFont for testing
    */
{
    GLenum err;
    unsigned char data[] = {
        0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
        0, 0, 0xff, 0, 0xff, 0, 0, 0,
        0, 0, 0xff, 0, 0xff, 0, 0, 0,
        0, 0xff, 0, 0, 0, 0xff, 0, 0,
        0, 0xff, 0, 0, 0, 0xff, 0, 0,
        0, 0xff, 0, 0, 0, 0xff, 0, 0,
        0, 0, 0xff, 0, 0xff, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
        0, 0, 0, 0xff, 0, 0, 0, 0,
    };
    if( (err=glGetError() ) != GL_NO_ERROR )
        NSWarnMLog(@"gl Error : %s", gluErrorString(err));
    
    [self __commonInit];
    glBindTexture(GL_TEXTURE_2D, idTexture);
    
    glTexImage2D(GL_TEXTURE_2D, 		// target
                 0,			// mipmap level
                 GL_INTENSITY8,		// Format interne (je panne rien !!)
                 8, 			// largeur
                 16, 			// hauteur
                 0,			// bord ?
                 GL_LUMINANCE,		// Format de texture (type)
                 GL_UNSIGNED_BYTE,	// encoding
                 data			// data
                 );
    
    if( (err=glGetError() ) != GL_NO_ERROR )
        NSWarnMLog(@"gl Error : %s", gluErrorString(err));
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP);
    
    if( (err=glGetError() ) != GL_NO_ERROR )
        NSWarnMLog(@"gl Error : %s", gluErrorString(err));
    
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST);
    
    
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
    
    if( (err=glGetError() ) != GL_NO_ERROR )
        NSWarnMLog(@"gl Error : %s", gluErrorString(err));
    
    return self;
}

@end
