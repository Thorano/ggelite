/*
 *  FloatingView.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: September 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#import <Foundation/NSException.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSDebug.h>
#import "FloatingView.h"
#import "Preference.h"
#import "Texture.h"
#import "GGFont.h"
#import "utile.h"
#import "GGGlyphLayout.h"

@implementation FloatingView
- (id) initWithFrame: (NSRect)frameRect
{
  [super initWithFrame: frameRect];
  
  alpha = 0.0; //opaque by default

  [self setAutoresizingMask: GGNSViewWidthSizable | GGNSViewHeightSizable];

  return self;
}

- (BOOL) isOpaque
{
  return alpha == 0.0;
}

- (void) setTexture: (Texture *)_t
{
  ASSIGN(t, _t);
}

- (void) enableTextureBackground
{
  if( thePref.HighQuality)
    [self setTexture: [Texture textureWithFileName: @"marble.jpg"
			       mipmap: NO]];
}

- (void) drawRect: (NSRect) _theRect
{
  NSRect theRect = [self frame];
  NSRect titleRect = [title frameRect];
  if( alpha < 1 )
    {
      if( t )
	{
	  glPushAttrib(GL_ENABLE_BIT);
	  glEnable(GL_TEXTURE_2D);
	  glBindTexture(GL_TEXTURE_2D, [t idTexture]);
	  glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	  glColor4f(0.4, 0.4, 0.4, alpha);
	  glBegin(GL_QUADS);
	  glTexCoord2f(0, 0);
	  glVertex2f(0, 0);

	  glTexCoord2f(1, 0);
	  glVertex2f(NSWidth(theRect), 0);

	  glTexCoord2f(1, 1);
	  glVertex2f(NSWidth(theRect), NSHeight(theRect));

	  glTexCoord2f(0, 1);
	  glVertex2f(0, NSHeight(theRect));
	  glEnd();

	  glPopAttrib();
	}
      else
	{
	  glColor4f(0,0,0,alpha);
	  glRecti(0,0,NSWidth(theRect), NSHeight(theRect));
	}
    }
  glColor3f(1, 1, 0);
  drawRectBorder(1,1,NSWidth(theRect)-1, NSHeight(theRect)-1);
  if( title )
    {
      glColor3f(1, 0.9, 0.9);
      [title drawAtPoint: 
	       NSMakePoint(NSWidth(theRect)/2-NSWidth(titleRect)/2, 
			   NSHeight(theRect)-NSHeight(titleRect)-5)];
    }
  GLCHECK;
}

- (void) setTitle: (NSString *)str
{
  if( title == nil )
    {
      title = [GGGlyphLayout glyphLayoutWithString: str
			     withFont: [GGFont defaultTitleFont]];
      RETAIN(title);
    }
  else
    [title updateString: str];
}

- (void) setTransparency: (float) val
{
  val = val >= 0 ? val : 0;
  val = val <= 1 ? val : 1;
  alpha = val;
}

- (void) dealloc
{
  RELEASE(t);
  RELEASE(title);
  [super dealloc];
}

- (void) sizeToFit
{
  NSEnumerator *en = [[self subviews] objectEnumerator];
  GGView *v;
  NSRect rect = NSZeroRect;
  //  FIXME: if there is a title ?

  while( (v = [en nextObject]) != nil)
    {
      if( [v respondsToSelector: @selector(sizeToFit)] )
	[(id)v sizeToFit];
      rect = NSUnionRect(rect, [v frame]);
    }

  [self setFrameSize: NSMakeSize(NSMaxX(rect) + 5, NSMaxY(rect) + 5)];
}

- (void) closeFloatingView
{
  [self removeFromSuperview];
}
@end
