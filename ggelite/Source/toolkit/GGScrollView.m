/*
 *  GGScrollView.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: August 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import "GGScrollView.h"
#import "GGLoop.h"
#import "GGWidget.h"
#import "GGInput.h"
#import "GGEventDispatcher.h"

@implementation GGScrollView
- initWithFrame: (NSRect) aRect
{
    [super initWithFrame: aRect];
    posDocView = 0;
    
    [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"MovementMapKeys"]];
    return self;
}

- (void) dealloc
{
    [theLoop unSchedule: self];
    RELEASE(documentView);
    [super dealloc];
}

- (void) setDocumentView: (GGView *)aView
{
    NSRect frameView;
    NSRect frame;
    
    if( aView == documentView )
        return;
    
    if( documentView != nil )
        [self removeSubview: documentView];
    ASSIGN(documentView, aView);
    
    if( aView == nil )
    {
        scroll = NO;
        return;
    }
    
    //  [documentView setFrameOrigin: NSMakePoint(0, 0)];
    
    [self addSubview: documentView];
    
    frameView = [documentView frame];
    frame = [self frame];
    
    NSDebugMLLog(@"Scroll", @"scroll = %d", (int) scroll);
    //   [documentView setFrameOrigin: NSMakePoint(0, frame.size.height - 
    // 					    frameView.size.height)];
    if( frameView.size.height > frame.size.height )
    {
        scroll = YES;
    }
    else
    {
        scroll = NO;
    }
    heightDocument = frameView.size.height;
    [self scrollToBeginningOfDocument];
}

- (void) _setPosDocView: (float) val
{
    NSRect frame;
    //  NSParameterAssert(scroll);
    frame = [self frame];
    
    if( val + frame.size.height > heightDocument )
        val = heightDocument - frame.size.height;
    if( val < 0 )
        val = 0;
    
    posDocView = val;
    [documentView setFrameOrigin: 
        NSMakePoint(0, 
                    frame.size.height - heightDocument + posDocView)];
}

- (void) _moveDown
{
    [self _setPosDocView: posDocView+100*realDeltaTime];
}

- (void) _moveUp
{
    [self _setPosDocView: posDocView-100*realDeltaTime];
}

- (void) scrollToBeginningOfDocument
{
    [self _setPosDocView: 0];
}

- (void) scrollDown: (BOOL) val
{
    NSDebugMLLog(@"Scroll", @"scroll = %d", scroll);
    if( scroll && documentView != nil )
    {
        NSRect frame = [self frame];
        if( heightDocument - posDocView > frame.size.height )
        {
            if( val )
            {
                [theLoop fastSchedule: self
                         withSelector: @selector(_moveDown)
                                  arg: nil];
            }
            else
            {
                [self _setPosDocView: posDocView+100];
            }
        }
    }
}


- (void) scrollUp: (BOOL) val
{
    if( scroll && documentView != nil )
    {
        if( posDocView >= 20 )
        {
            if( val )
            {
                [theLoop fastSchedule: self
                         withSelector: @selector(_moveUp)
                                  arg: nil];
            }
            else
            {
                [self _setPosDocView: posDocView-100];
            }
        }
    }
}

- (void)goUp
{
    [self scrollUp: YES];
}

- (void) stopGoUp
{
    [theLoop unSchedule: self];
}


- (void)goDown
{
    [self scrollDown: YES];
}

- (void) stopGoDown
{
    [theLoop unSchedule: self];
}

- (void)goHigher
{
    [self scrollUp: NO];
}

- (void)goDeeper
{
    [self scrollDown: NO];
}

- (void) drawRect: (NSRect) _theRect
{
    NSRect theRect = [self frame];
    NSDebugMLLogWindow(@"GGScrollView", @"frame \n%@", NSStringFromRect(theRect));
    glColor3fv(GGBorderColor);
    glBegin(GL_LINE_LOOP);
    glVertex2f(0,0);
    glVertex2f(theRect.size.width-1, 0);
    glVertex2f(theRect.size.width-1,theRect.size.height-1);
    glVertex2f(0, theRect.size.height-1);
    glEnd();
    [super drawRect: theRect];
}
@end
