/* 
NSResponder.m
 
 Abstract class which is basis of command and event processing
 
 Copyright (C) 1996,1999 Free Software Foundation, Inc.
 
 Author:  Scott Christley <scottc@net-community.com>
 Date: 1996
 
 This file is part of the GNUstep GUI Library.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 
 You should have received a copy of the GNU Library General Public
 License along with this library; see the file COPYING.LIB.
 If not, write to the Free Software Foundation,
 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */ 

#import <Foundation/NSCoder.h>
#import "config.h"
#import "NSResponder.h"
#import <Foundation/NSDebug.h>
#import "GGEventDispatcher.h"

@implementation GGResponder

/*
 * Class methods
 */
+ (void) initialize
{
    if (self == [GGResponder class])
    {
        NSDebugLog(@"Initialize GGResponder class\n");
        
        [self setVersion: 1];
    }
}

- (void) dealloc {
    [_dispatcher release];
    [super dealloc];
}


/*
 * Instance methods
 */
/*
 * Managing the next responder
 */
- (GGResponder*) nextResponder
{
    return _next_responder;
}

- (void) setNextResponder: (GGResponder*)aResponder
{
    _next_responder = aResponder;
}

- (GGEventDispatcher *)dispatcher {
    return _dispatcher;
}

- (void)setDispatcher:(GGEventDispatcher *)value {
    ASSIGN(_dispatcher, value);
}



/*
 * Determining the first responder
 */
- (BOOL) acceptsFirstResponder
{
    return NO;
}

- (BOOL) becomeFirstResponder
{
    return YES;
}

- (BOOL) resignFirstResponder
{
    return YES;
}

/*
 * Aid event processing
 */
- (BOOL) performKeyEquivalent: (Event*)theEvent
{
    return NO;
}

- (BOOL) tryToPerform: (SEL)anAction with: (id)anObject
{
    /* Can we perform the action -then do it */
    if ([self respondsToSelector: anAction])
    {
        [self performSelector: anAction withObject: anObject];
        return YES;
    }
    else
    {
        /* If we cannot perform then try the next responder */
        if (!_next_responder)
            return NO;
        else
            return [_next_responder tryToPerform: anAction with: anObject];
    }
}

- (BOOL) performMnemonic: (NSString*)aString
{
    return NO;
}

#if 0
- (void) interpretKeyEvents:(NSArray*)eventArray
{
    // FIXME: As NSInputManger is still missing this method is hard coded
    Event *theEvent;
    NSEnumerator *eventEnum = [eventArray objectEnumerator];
    
    while((theEvent = [eventEnum nextObject]) != nil)
    {
        NSString *characters = [theEvent characters];
        unichar character = 0;
        
        if ([characters length] > 0)
        {
            character = [characters characterAtIndex: 0];
        }
        
        switch (character)
        {
            case NSUpArrowFunctionKey:
                [self doCommandBySelector: @selector(moveUp:)];
                break;
            case NSDownArrowFunctionKey:
                [self doCommandBySelector: @selector(moveDown:)];
                break;
            case NSLeftArrowFunctionKey:
                [self doCommandBySelector: @selector(moveLeft:)];
                break;
            case NSRightArrowFunctionKey:
                [self doCommandBySelector: @selector(moveRight:)];
                break;
            case NSDeleteFunctionKey:
                [self doCommandBySelector: @selector(deleteForward:)];
                break;
            case NSHomeFunctionKey:
                [self doCommandBySelector: @selector(moveToBeginningOfDocument:)];
                break;
            case NSBeginFunctionKey:
                [self doCommandBySelector: @selector(moveToBeginningOfLine:)];
                break;
            case NSEndFunctionKey:
                [self doCommandBySelector: @selector(moveToEndOfLine:)];
                break;
            case NSPageUpFunctionKey:
                [self doCommandBySelector: @selector(pageUp:)];
                break;
            case NSPageDownFunctionKey:
                [self doCommandBySelector: @selector(pageDown:)];
                break;
            case NSBackspaceCharacter:
                [self doCommandBySelector: @selector(deleteBackward:)];
                break;
            case NSTabCharacter:
                if ([theEvent modifierFlags] & NSShiftKeyMask)
                    [self doCommandBySelector: @selector(insertBacktab:)];
                else
                    [self doCommandBySelector: @selector(insertTab:)];
                break;
            case NSEnterCharacter:
            case NSFormFeedCharacter:
            case NSCarriageReturnCharacter:
                [self doCommandBySelector: @selector(insertNewline:)];
                break;
            case 0:
                /* Character to implement ?? */
                break;
            default:
                // If the character(s) was not a special one, simply insert it.
                [self insertText: characters];
        }  
    }
}
#endif

- (void) flushBufferedKeyEvents
{
}

- (void) doCommandBySelector:(SEL)aSelector
{
    [self tryToPerform: aSelector with: nil];
}

/*
 * Forwarding event messages
 */
- (void) flagsChanged: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder flagsChanged: theEvent];
    else
        return [self noResponderFor: @selector(flagsChanged:)];
}

- (void) keyDown: (Event*)theEvent
{
    NSDebugMLLog(@"KeyMapping", @"try dispatch keydown to %@", self);

    if(_dispatcher && [_dispatcher dispatchKeyDownWithEvent:theEvent target:self]){
        NSDebugMLLog(@"KeyMapping", @"successfull dispatch on %@", self);
        return;
    }
    if (_next_responder)
        return [_next_responder keyDown: theEvent];
    else
        return [self noResponderFor: @selector(keyDown:)];
}

- (void) keyUp: (Event*)theEvent
{
    if(_dispatcher && [_dispatcher dispatchKeyUpWithEvent:theEvent target:self])
        return;
    if (_next_responder)
        return [_next_responder keyUp: theEvent];
    else
        return [self noResponderFor: @selector(keyUp:)];
}

- (void) middleMouseDown: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder middleMouseDown: theEvent];
    else
        return [self noResponderFor: @selector(middleMouseDown:)];
}

- (void) middleMouseDragged: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder middleMouseDragged: theEvent];
    else
        return [self noResponderFor: @selector(middleMouseDragged:)];
}

- (void) middleMouseUp: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder middleMouseUp: theEvent];
    else
        return [self noResponderFor: @selector(middleMouseUp:)];
}

- (void) mouseDown: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder mouseDown: theEvent];
    else
        return [self noResponderFor: @selector(mouseDown:)];
}

- (void) mouseDragged: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder mouseDragged: theEvent];
    else
        return [self noResponderFor: @selector(mouseDragged:)];
}

- (void) mouseEntered: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder mouseEntered: theEvent];
    else
        return [self noResponderFor: @selector(mouseEntered:)];
}

- (void) mouseExited: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder mouseExited: theEvent];
    else
        return [self noResponderFor: @selector(mouseExited:)];
}

- (void) mouseMoved: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder mouseMoved: theEvent];
    else
        return [self noResponderFor: @selector(mouseMoved:)];
}

- (void) mouseUp: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder mouseUp: theEvent];
    else
        return [self noResponderFor: @selector(mouseUp:)];
}

- (void) scrollWheel: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder scrollWheel: theEvent];
    else
        return [self noResponderFor: @selector(scrollWheel:)];
}

- (void) noResponderFor: (SEL)eventSelector
{
}

- (void) rightMouseDown: (Event*)theEvent
{
    if (_next_responder != nil)
    {
        return [_next_responder rightMouseDown: theEvent];
    }
    else
    {
        return [self noResponderFor: @selector(rightMouseDown:)];
    }
}

- (void) rightMouseDragged: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder rightMouseDragged: theEvent];
    else
        return [self noResponderFor: @selector(rightMouseDragged:)];
}

- (void) rightMouseUp: (Event*)theEvent
{
    if (_next_responder)
        return [_next_responder rightMouseUp: theEvent];
    else
        return [self noResponderFor: @selector(rightMouseUp:)];
}


- (void) joystickAxisChange: (Event *)theEvent
{
    if (_next_responder)
        return [_next_responder joystickAxisChange: theEvent];
    else
        return [self noResponderFor: @selector(joystickAxisChange:)];
}

- (void) joystickButtonPress:(Event *)theEvent
{
    if (_next_responder)
        return [_next_responder joystickButtonPress: theEvent];
    else
        return [self noResponderFor: @selector(joystickButtonPress:)];
}

- (void) joystickButtonRelease:(Event *)theEvent
{
    if (_next_responder)
        return [_next_responder joystickButtonRelease: theEvent];
    else
        return [self noResponderFor: @selector(joystickButtonRelease:)];
}

- (void) joystickHat:(Event *)theEvent
{
    if (_next_responder)
        return [_next_responder joystickHat: theEvent];
    else
        return [self noResponderFor: @selector(joystickHat:)];
}


/*
 * Services menu support
 */
- (id) validRequestorForSendType: (NSString*)typeSent
                      returnType: (NSString*)typeReturned
{
    if (_next_responder)
        return [_next_responder validRequestorForSendType: typeSent
                                               returnType: typeReturned];
    else
        return nil;
}

/*
 * NSCoding protocol
 * NB. Don't encode responder chain - it's transient information that should
 * be reconstructed from else where in the encoded archive.
 */
- (void) encodeWithCoder: (NSCoder*)aCoder
{
}

- (id) initWithCoder: (NSCoder*)aDecoder
{
    
    return self;
}


- (NSUndoManager*) undoManager
{
    return nil;
}

- (BOOL) shouldSave
{
    return NO;
}

@end
