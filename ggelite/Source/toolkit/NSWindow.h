/* 	-*-ObjC-*- */

#ifndef __NSWindow_h
#define __NSWindow_h


#import "NSResponder.h"
#import "GGLoop.h"

@class NSString;
@class NSArray;
@class NSMutableArray;
@class NSData;

@class GGNSWindow;
@class GGView;
@class NSClipView;
@class NSImage;
@class NSCursor;
@class TopView;


@interface GGNSWindow : GGResponder <InputHandler>
{
  GGView 		*contentView, *metaView;;
  GGView 		*firstResponder;
  GGNSWindow		*windowOnTop, *windowBelow, *windowAbove;
  GGNSWindow		*firstOpaque;
  GGView		*grabeur, *viewUnderMouse;
  NSRect		frameWindow;
  int 			open;
  VectCol		couleur;
  BOOL			doBackGround;

  TopView			*_currentView;
  NSMutableArray		*_stackView;

}
+ window;

- (void) setFrameWindow: (NSRect) aRect;
- initWithFrame: (NSRect) aRect;
- (void) initWindowGL;
- (void) setDrawBackground:(BOOL) f;

- (GGView *)firstResponder;
- (BOOL) makeFirstResponder: (GGView *)aView;

- (void) setOpacity:(float) val;
- (GGReal) opacity;
- (void) setBackgroundColor: (VectCol) val;

- (void) drawWindow;

- (void) setContentView: (GGView *) v;
- (GGView *)contentView;
- (NSRect) convertWindowRectToFrameBufferRect:(NSRect)windowRect;
- (void) setGLFrameInRect: (NSRect) _aRect;
- (void) setFrameInRect: (NSRect) aRect
	       atOrigin: (NSPoint) aPoint;
- (void) display;
- (void) flushWindow;
- (void) setViewsNeedDisplay: (BOOL) flag;
- (BOOL) isOpen;
- (void) open;
- (void) close;
- (void) pushOnTheTop: (GGNSWindow *)aWin;
- (void) pushWindow: (GGNSWindow *) aWin;
- (GGNSWindow *) popWindow;
- (BOOL) isOpaque;
- (void) grabEvent: (GGView *)theGrab;
- (void) unGrabEvent;
- (void) setWindowBelow: (GGNSWindow *)win;
- (GGNSWindow *)windowBelow;
- (GGNSWindow *) windowOnTop;

- (void) takeFocus;
- (void) looseFocus;

- (void) addCloseButton;
- (void) addTopView: (TopView *) aView;
- (void) aTopViewIsClosing: (TopView *) aView;
- (void) popAllView;

@end

//  #define VirtualWidth 640
//  #define VirtualHeight 480
#define HeightTableau 50
#define VirtualWidth WIDTH
#define VirtualHeight (HEIGHT - HeightTableau)


#endif // _GNUstep_H_NSView

