/* 	-*-ObjC-*- */
/* 
   NSResponder.h

   Abstract class which is basis of command and event processing

   Copyright (C) 1996,1999 Free Software Foundation, Inc.

   Author:  Scott Christley <scottc@net-community.com>
   Date: 1996
   
   This file is part of the GNUstep GUI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
   
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; see the file COPYING.LIB.
   If not, write to the Free Software Foundation,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/ 

#ifndef _GNUstep_H_NSResponder
#define _GNUstep_H_NSResponder

#import <Foundation/NSCoder.h>

@class NSString;
@class NSMenu;
@class NSUndoManager;

#import "config.h"
#import "GGEvent.h"

//avoid name clashing

@class GGEventDispatcher;

@interface GGResponder : NSObject <NSCoding>
{
    GGEventDispatcher*      _dispatcher;
  GGResponder		*_next_responder;

  /*
   * Flags for internal use by GGResponder and it's subclasses.
   */
@public
  struct _rFlagsType {
    /*
     * 'flipped_view' is set in GGNSViews designated initialiser (and other
     * methods that create views) to be the value returned by [-isFlipped]
     * This caching assumes that the value returned by [-isFlipped] will
     * not change during the views lifetime - if it does, the view must
     * be sure to change the flag accordingly.
     */
    unsigned	flipped_view:1;
    unsigned	has_subviews:1;		/* The view has subviews.	*/
    unsigned	has_draginfo:1;		/* View/window has drag types.	*/
    unsigned	opaque_view:1;		/* For views whose opacity may	*/
					/* change to keep track of it.	*/
    unsigned	valid_rects:1;		/* Some cursor rects may be ok.	*/
    unsigned	needs_display:1;	/* Window/view needs display.	*/
  } _rFlags;
}

/*
 * Instance methods
 */

/*
 * Managing the next responder
 */
- (GGResponder*) nextResponder;
- (void) setNextResponder: (GGResponder*)aResponder;

- (GGEventDispatcher *)dispatcher;
- (void)setDispatcher:(GGEventDispatcher *)value;


/*
 * Determining the first responder
 */
- (BOOL) acceptsFirstResponder;
- (BOOL) becomeFirstResponder;
- (BOOL) resignFirstResponder;

/*
 * Aid event processing
 */
- (BOOL) performKeyEquivalent: (Event*)theEvent;
- (BOOL) tryToPerform: (SEL)anAction with: (id)anObject;

/*
 * Forwarding event messages
 */
- (void) flagsChanged: (Event*)theEvent;
- (void) keyDown: (Event*)theEvent;
- (void) keyUp: (Event*)theEvent;
- (void) mouseDown: (Event*)theEvent;
- (void) mouseDragged: (Event*)theEvent;
- (void) mouseEntered: (Event*)theEvent;
- (void) mouseExited: (Event*)theEvent;
- (void) mouseMoved: (Event*)theEvent;
- (void) mouseUp: (Event*)theEvent;
- (void) scrollWheel: (Event*)theEvent;
- (void) noResponderFor: (SEL)eventSelector;
#ifndef	NO_GNUSTEP
- (void) middleMouseDown: (Event*)theEvent;
- (void) middleMouseDragged: (Event*)theEvent;
- (void) middleMouseUp: (Event*)theEvent;
#endif
- (void) rightMouseDown: (Event*)theEvent;
- (void) rightMouseDragged: (Event*)theEvent;
- (void) rightMouseUp: (Event*)theEvent;

/* GGExt */
- (void) joystickAxisChange: (Event *)theEvent;
- (void) joystickButtonPress:(Event *)theEvent;
- (void) joystickButtonRelease:(Event *)theEvent;
- (void) joystickHat:(Event *)theEvent;

/*
 * Services menu support
 */
- (id) validRequestorForSendType: (NSString*)typeSent
		      returnType: (NSString*)typeReturned;

/*
 * NSCoding protocol
 */
- (void) encodeWithCoder: (NSCoder*)aCoder;
- (id) initWithCoder: (NSCoder*)aDecoder;

@end

#endif /* _GNUstep_H_NSResponder */
