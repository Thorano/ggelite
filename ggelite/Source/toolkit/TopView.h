/* 	-*-ObjC-*- */
/*
 *  TopView.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: May 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __TopView_h
#define __TopView_h

#import "NSView.h"
#import "GGSky.h"

@class GGFont;
@class GGString;
@class Texture;
@class GGEventDispatcher;

@protocol IconsInfo
+ (NSString *) iconName;
@end

@interface TopViewManagerCommon : NSObject
{
  GGNSWindow		*window;  //NOT RETAINED
  int			category;
  int			currentC;
  int			max;
  GGEventDispatcher*    _dispatcher;
}
- (BOOL) acceptKeyDown: (Event *)pev;
- (void) invalidate;
- (int) tag;
- initWithTag: (int) val
       window: (GGNSWindow *) win;
- (void) openNextView;
- (void) openFirstView;
- (void) switchTo: (int) val;
- (NSString *) iconForIndex: (int) val;
@end


@interface TopViewManager : TopViewManagerCommon
{
  NSArray 		*classes;
  NSMutableDictionary	*dico;
}
- initWithTag: (int) val
      classes: (NSArray *) a
       window: (GGNSWindow *) win;
@end


@interface TopView : GGView
{
  GGString 		*title;
  Texture 		*t;
  //  GUIState		_state;
  TopViewManagerCommon	*manager; //NOT RETAINED
}

- (void) setManager: (TopViewManagerCommon *) obj;
- (void) openIn: (GGNSWindow *)w;
- (void) closing;
- (void) close;
+ topview;
- (void) setTexture: (Texture *)_t;
- (void) enableTextureBackground;
- (GGString *) titleView;
/*
- (GUIState) state;
- (void) setState: (GUIState) newState;
*/
@end

extern NSString *GGTopViewClosing;

#endif
