/* 	-*-ObjC-*- */
/*
 *  FloatingView.h
 *
 *  Copyright (c) 2002 by Frédéric De Jaeger
 *  
 *  Date: September 2002
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __FloatingView_h
#define __FloatingView_h

#import "NSView.h"

@class GGFont;
@class GGString;
@class Texture;
@class GGGlyphLayout;

@interface FloatingView : GGView
{
  GGGlyphLayout *title;
  Texture 	*t;
  float		alpha;
}
- (void) setTransparency: (float) val;
- (void) setTexture: (Texture *)_t;
- (void) enableTextureBackground;
- (void) setTitle: (NSString *) string;
- (void) sizeToFit;
- (void) closeFloatingView;
@end

#endif
