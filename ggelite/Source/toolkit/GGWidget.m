/*
 *  GGWidget.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSArray.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>

#import "GGWidget.h"
#import "Metaobj.h"
#import "NSWindow.h"
#import "utile.h"
#import "Texture.h"
#import "GGGlyphLayout.h"
#import "GGFont.h"
#import "GGIcon.h"
#import "GGEventDispatcher.h"

@protocol TextDelegate
- (void) textHasBeenValidated: (GGString *) str;
@end

float GGBorderColor[3] = BorderColor;


typedef enum __buttonSate
{
  Pushed = 1,
  Released = 0
}ButtonState;

@implementation GGBouton
- init
{
  [super init];
  target = nil;
  action = NULL;
  state = Released;
  str = nil;
  tag = -1;
  type = GGMomentaryPushButton;
  border = YES;
  enable = YES;
  return self;
}

+ button
{
  return AUTORELEASE([self new]);
}

- (void) setButtonType: (GGButtonType)aType
{
  type = aType;
}


- (void) setEnable: (BOOL) f
{
  enable = f;
}

- (BOOL) isEnable
{
  return enable;
}

- (void) setType: (GGButtonType) theType
{
  type = theType;
}

- (void) _computeFrame
{
  BOOL ok = NO;
  NSSize aSize;
  NSRect temp = NSZeroRect;
  NSRect tempStr = NSZeroRect;
  float ystart;

  if( border )
    {
      aSize = NSMakeSize(20, 20);
      ystart = 10;
    }
  else
    {
      aSize = NSZeroSize;
      ystart = 0;
    }

  if( strLayout != nil )
    {
      tempStr = [strLayout frameRect];
      aSize.width += tempStr.size.width;
      aSize.height += tempStr.size.height;
      ok = YES;
    }

  if( icon != nil )
    {
      float val;
      temp = [icon frameRect];
      val = temp.size.width + (border ? 10:0);
      if( val > aSize.width )
	aSize.width = val;
      aSize.height += temp.size.height+(strLayout?2:0);
      ok = YES;
    }

  if( strLayout != nil )
    {
      textOrigin = NSMakePoint(aSize.width/2.0 - tempStr.size.width/2.0,   
			       ystart);
      ystart += tempStr.size.height + 2;
    }
  if( icon != nil )
    iconOrigin = NSMakePoint(aSize.width/2.0 - temp.size.width/2.0, 
			     ystart);
      
  if( ok )
    {
      NSDebugMLLog(@"GGBouton", NSStringFromSize(aSize));
      [self setFrameSize: aSize];
    }
}

- (void) setBorder: (BOOL) val
{
  border = val;
  [self _computeFrame];
}

- (void) setIcon: (GGIcon *) _icon
{
  ASSIGN(icon, _icon);
  [self _computeFrame];
}

- (void) setTitle: (NSString *) theStr
	 withFont: (GGFont *) f
{
  GGGlyphLayout *temp;
  ASSIGN(str, theStr);
  temp = [GGGlyphLayout glyphLayoutWithString: str
			withFont: f];
  [temp setAlignement: BottomAlignement];
  ASSIGN(strLayout, temp);
  [self _computeFrame];
}

- (void) setTitle: (NSString *) theStr
{
  [self setTitle: theStr
	withFont: [GGFont defaultButtonFont]];
}

- (void) dealloc
{
  RELEASE(icon);
  RELEASE(str);
  RELEASE(strLayout);
  [super dealloc];
}

- (BOOL) isOpaque
{
  return YES;
}

- (void) drawRect: (NSRect) theRect
{
  NSRect aRect = [self frame];
  if( border || state != Released || highlighted || !enable)
    {
      glPushAttrib(GL_POLYGON_BIT);
      if ( !enable) 
	{
	  glColor4f(0, 0, 1, 0.4);
	  glPolygonMode(GL_FRONT, GL_FILL);
	}
      else
	{      
	  if( highlighted && state == Released )
	    glColor3f(0.7, 0.3, 0.3);
	  else
	    glColor3f(0.8, 0.5, 0.4);
	  glPolygonMode(GL_FRONT, state == Pushed || highlighted 
			? GL_FILL : GL_LINE);
	}

      glRecti(1, 1, aRect.size.width-1, aRect.size.height-1);
      glPopAttrib();
    }

  if( icon )
    {
      glColor4f(1, 1, 1, 0.4);
      [icon drawAtPoint: iconOrigin];
    }

  if( strLayout != nil )
    {
      glColor3f(1, 1, 1);
      [strLayout drawAtPoint: textOrigin];
    }

  [super drawRect: theRect];
  GLCHECK;

}

- (void) invertState
{
  state = (state == Pushed ? Released : Pushed);
}

- (void) mouseDown: (Event *) pev
{
  NSDebugMLLog(@"GGBouton", @"mouse down in GGBouton %@!!", self);
  if( !enable )
    return;
  switch(type)
    {
    case GGMomentaryPushButton:
      state = Pushed;
      break;
    case GGToggleButton:
      [self invertState];
      [target performSelector: action withObject: self];
      break;
    }
}

- (void) mouseUp: (Event *) pev
{
  NSDebugMLLog(@"GGBouton", @"mouse up in GGBouton %@!!", self);
  if ( !enable )
    return;
  switch(type)
    {
    case GGMomentaryPushButton:
      state = Released;
      [target performSelector: action withObject: self];
      break;
    case GGToggleButton:
      break;
    }

}

- (void) setTarget: obj
{
  target = obj;
}

- (void) setAction: (SEL) aSel
{
  action = aSel;
}

- (void) setState: (int) theState
{
  state = theState;
}

- (int) state
{
  return state;
}

- (void) viewWillMoveToWindow: (GGNSWindow *)win
{
  [super viewWillMoveToWindow: win];

  if( _window == nil )
    highlighted = NO;
}

- (void)  wasViewUnderMouse
{
  highlighted = NO;
}

- (void) isViewUnderMouse
{
  highlighted = YES;
}
  

@end

@implementation GGString
- initWithFrame: (NSRect) aFrame
{
  [super initWithFrame: aFrame];
    str = [NSMutableString new];
  [self setString: @"aString"];
  [self setDispatcher:[GGEventDispatcher dispatcherWithName:@"MovementKeys"]];
  return self;
}

- (BOOL) isOpaque
{
  return NO;
}

- (void) dealloc
{
  RELEASE(str);
  RELEASE(strLayout);
  [super dealloc];
}

- (void)setString: (NSString *)aStr
{
  [self setString: aStr
	withFont: [GGFont defaultFont]];
}

- (void) _updateLayout
{
  NSRect aRect;
  [strLayout updateString: str];
  aRect = [strLayout frameRect];
  [self setFrameSize: aRect.size];
}

- (void) updateString: (NSString *)Astr
{
  [str setString: Astr];
  [self _updateLayout];
}

- (void) setString: (NSString *) aStr
	  withFont: (GGFont *) f
{
  NSRect aRect;
  GGGlyphLayout *temp;
  [str setString: aStr];
  temp = [GGGlyphLayout glyphLayoutWithString: str
			withFont: f];
  aRect = [temp frameRect];
  ASSIGN(strLayout, temp);
  [self setFrameSize: aRect.size];
}

- (void) drawRect: (NSRect) theRect
{
  if( strLayout != nil )
    {
      NSDebugMLLog(@"GGString", @"rect %@ %@", NSStringFromRect(theRect),
		   str);
      glColor3f(1, 1, 1);
      [strLayout drawAtPoint: NSZeroPoint];
    }
  [super drawRect: theRect];
    GLCHECK;

}

- (NSString *)description
{
  if( str == nil )
    return [super description];
  else
    return str;
}

- (void) setEditable: (BOOL) val
{
  isEditable = val;
}

- (void) setDelegate: (id) _delegate
{
  delegate = _delegate;
}

- (void) validate:(id)sender
{
    if( isEditable && delegate)
        [(id<TextDelegate>) delegate textHasBeenValidated: self];
}

- (void) insertText:(NSString*) text
{
    if(isEditable){
        [str appendString:text];
        [self _updateLayout];
    }
}

- (NSString *) value
{
  return [str copy];
}
@end


@implementation GGOption
- init
{
  [super init];
  bo = AUTORELEASE([GGBouton new]);
  [bo setFrame: NSMakeRect(0, 2, 15, 15)];
  [bo setType: GGToggleButton];
  [self addSubview: bo];


  [self setFrameSize: NSMakeSize(200, 35)];
  
  return self;
}

- (void) dealloc
{
  RELEASE(strLayout);
  [super dealloc];
}

- (void) setType: (GGButtonType) aType
{
  [bo setType: aType];
}

- (void) _commonUpdate
{
  NSRect rect;
  NSSize size;
  rect = [strLayout frameRect];
  size.width = rect.size.width+30;
  size.height = rect.size.height+2;
  [self setFrameSize: size];
}

- (void) setTitle: (NSString *)theStr
{
  [strLayout updateString: theStr];
  [self _commonUpdate];
}


- (void) setTitle: (NSString *)theStr
	    width: (int) w
	     font: (GGFont *)font
{
  GGGlyphLayout *l;

  l = [GGGlyphLayout glyphLayoutWithString: @""
		     withFont: font];
  ASSIGN(strLayout, l);
  [strLayout setMaxWidth: w];
  [strLayout appendString: theStr];
  [self _commonUpdate];
}

+ optionWithStr: (NSString *) theStr
	  width: (int) w
{
  return [self optionWithStr: theStr
	       width: w 
	       font: [GGFont defaultOptionFont]];
}


+ optionWithStr: (NSString *) theStr
{
  return [self optionWithStr: theStr
	       width: -1];
}

+ optionWithFormat: (NSString *)format,...
{
  GGOption *opt;
  va_list ap;
  NSString *string;

  va_start (ap, format);

  string = [[NSString alloc] initWithFormat: format
                                  arguments: ap];
  opt = [self optionWithStr: string
	      width: -1];
  RELEASE(string);
  va_end (ap);
  return opt;
}

+ optionWithStr: (NSString *) theStr
	  width: (int) w
	   font: (GGFont *)font
{
  GGOption *opt;
  opt = AUTORELEASE([self new]);
  [opt setTitle: theStr
       width: w
       font: font];
  return opt;
}


+ optionWithStr: (NSString *) theStr
	   font: (GGFont *)font
{
  return [self optionWithStr: theStr
	       width: -1
	       font: font];
}

- (void) setTarget: obj
{
  [bo setTarget: obj];
}

- (void) setAction: (SEL) aSel
{
  [bo setAction: aSel];
}

- (void) setState: (int) state
{
  [bo setState: state];
}

- (void) setButtonType: (GGButtonType)aType
{
  [bo setButtonType: aType];
}

- (void) mouseDown: (Event *)pev
{
  NSPoint point = mouseLocationOfEvent(pev);
  NSPoint local;
  local = [self convertPoint: point fromView: nil];
  NSDebugMLLog(@"MousePress", @"%@ global %@ local %@", strLayout,
	       NSStringFromPoint(point),
	       NSStringFromPoint(local));
  [super mouseDown: pev];
}

- (void) setTag: (int) _tag
{
  [super setTag: _tag];
  [bo setTag: _tag];
}

- (void) drawRect: (NSRect) aRect
{
  if( strLayout != nil )
    {
      glColor3f(1, 1, 1);
      [strLayout drawAtPoint: NSMakePoint(30,0)];
    }
  [super drawRect: aRect];
    GLCHECK;
}
@end


@implementation GGControl 
- (void) setTag: (int) _tag
{
  tag = _tag;
}

- (int) tag
{
  return tag;
}
@end
