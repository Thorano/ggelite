//
//  GGShell.h
//  elite
//
//  Created by Frédéric De Jaeger on 11/29/04.
//  Copyright 2004 __MyCompanyName__. All rights reserved.
//

#import "FloatingView.h"

@class GGString, Console;
@interface GGShell : FloatingView {
    GGString*           _inputView;
    Console*            _console;
}
+ (GGShell*) defaultShell;
- (id) initWithFrame: (NSRect) aRect;
- (void) display: (NSString*) string;
@end
