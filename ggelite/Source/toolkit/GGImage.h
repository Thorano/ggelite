//
//  GGImage.h
//  elite
//
//  Created by Frederic De Jaeger on 10/09/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GGImage : NSObject {
    int             _width;
    int             _height;
    int             _scanLine;
    int             _alignement;
    int             _bytesPerRow;
    int             _ncomp;
}
+ (GGImage*)imageFromPath:(NSString*)path;
+ (GGImage*)imageWithName:(NSString*)name;

- (int) width;
- (int) height;
- (int) scanLine;
- (int) bytesPerRow;
- (int) alignement;
- (int) ncomp;

- (void*) rawData;

- (void) compositeToRect:(NSRect)destRect;
- (void) drawPixels;

@end
