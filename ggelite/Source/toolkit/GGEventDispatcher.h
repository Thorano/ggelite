//
//  GGEventDispatcher.h
//  elite
//
//  Created by Frederic De Jaeger on 12/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GGLoop.h"
#import "GGEvent.h"

struct dispatchMappingDescription
{
    NSString*           selName;
    NSString*           keystroke;
    id                  extraArgument;
    NSString*           localizedDescription;
};

#define kGGCommandName @"GGCommandName"
#define kGGKeyStroke @"GGKeyStroke"
#define kGGExtraArgument @"GGExtraArgument"
#define kGGDescription @"GGDescription"
#define kGGDispatcherName @"GGDispatcherName"
#define kGGDispatcherAncestor @"GGDispatcherAncestor"
#define kGGBindings @"GGBindings"

// key combos are described a la emacs.
@interface GGEventDispatcher : NSObject {
    NSMutableDictionary*    _mapping;
    GGEventDispatcher*      _ancestorDispatcher;
    NSString*               _tableName;
}
+ dispatcherWithName:(NSString*)name;
- initWithDictionary:(NSDictionary*)mapping
        inHeritsFrom:(GGEventDispatcher*)ancestorDispatcher;

- (BOOL) dispatchKeyDownWithEvent:(Event*) event
                           target:(id)target;

- (BOOL) dispatchKeyUpWithEvent:(Event*) event
                         target:(id)target;
@end
