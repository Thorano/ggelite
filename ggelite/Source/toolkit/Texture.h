/* 	-*-ObjC-*- */
/*
 *  Texture.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Texture_h
#define __Texture_h

#import <Foundation/NSObject.h>

#import "types.h"

@protocol TexInitializer <NSObject>
- (int) nCol;
- (void) colorBoxForPoint: (double [3]) pts
		       in: (unsigned char *) dest;
@end

@class GGImage;
@interface Texture : NSObject
{
    GLuint		idTexture;
    int         _width;
    int         _height;
    enum __kindTexture{
        GGTextureRegular,
        GGTextureCubeMap,
    }           _kind;
}
+ stupidTexture;
+ textureWithFileName: (NSString *)fileName
	       mipmap: (BOOL) mipmap;
+ textureWithFileName: (NSString *)fileName;
- initWithTextureFile:(NSString *)fileName
	       mipmap: (BOOL) mipmap;
- (int) idTexture;
- initStartField: (void *)data 
	   width: (int) w 
	  height: (int) h;
- initForFont: (void *) data
	width: (int) w
       height: (int) h;
- initWithData: (void *)dataChar
	 width: (int) width
	height: (int) height
      channels: (int) ncomp
	mipmap: (BOOL) mipmap;
- (void) setSubImage: (GGImage *)subImage
              format: (GLint) format
             xoffset: (int) xoffset
             yoffset: (int) yoffset;

- (void) changeStarField: (void *)data 
		   width: (int) w 
		  height: (int) h;

- initBoxTexture: (id <TexInitializer>) obj
            size: (int) size
          mipmap: (BOOL) mipmap;
+ boxTexture: (id <TexInitializer>) obj;
+ cubeMapTextureWithFileName: (NSString *)fileName;

@end


#endif
