/*
 *  TopView.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: May 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSNotification.h>

#import "GGRepere.h"
#import "GGLoop.h"
#import "Metaobj.h"
#import "Tableau.h"
#import "Sched.h"
#import "World.h"
#import "GGWidget.h"
#import "utile.h"
#import "GGInput.h"
#import "GGFont.h"
#import "TopView.h"
#import "Texture.h"
#import "Preference.h"
#import "GGIcon.h"
#import "GGEventDispatcher.h"

NSString *GGTopViewClosing = @"GGTopViewClosing";

@implementation TopViewManagerCommon
- initWithTag: (int) val
       window: (GGNSWindow *) win
{
    [super init];
    window = win;
    category = val;
    
    _dispatcher = [[GGEventDispatcher dispatcherWithName:@"TopViewKeys"] retain];
    NSParameterAssert(_dispatcher);
    addToCheck(self);
    return self;
}

- (void) dealloc
{
    [_dispatcher release];
    [super dealloc];
}

- (int) tag
{
    return category;
}

- (void) invalidate
{
    GGIconAction icons[4] = {
    {nil, NULL, nil},
    {nil, NULL, nil},
    {nil, NULL, nil},
    {nil, NULL, nil},
    };
    
    [[theGGInput tableau] buildIconBar: icons
                                    at: 4
                                  size: sizeof(icons) / sizeof(GGIconAction)];
    
    [window popAllView];
    [[NSNotificationCenter defaultCenter]
    postNotificationName: @"GGViewManagerInvalidated"
                  object: self];
}

- (void) switchTo: (int) val
{
    [self subclassResponsibility: _cmd];
}

- (void) switchToNumber: (NSNumber*) number
{
    [self switchTo:[number intValue]];
}

- (void) openFirstView
{
    int i;
    if (max > 0)
    {
        GGIconAction tab[max];
        
        for( i = 0; i < max; ++i)
        {
            tab[i].obj = self;
            tab[i].action = @selector(openCallback:);
            tab[i].icon = [self iconForIndex: i];
        }
        
        [[theGGInput tableau] buildIconBar: tab
                                        at: 4
                                      size: max];
    }
    
    [self switchTo: 0];
}


-  (void) openNextView
{
    if ( max == 0 ) return;
    
    if (currentC < max-1)
        currentC++;
    else
        currentC = 0;
    [self switchTo: currentC];
}

- (BOOL) acceptKeyDown: (Event *)pev
{
    return [_dispatcher dispatchKeyDownWithEvent:pev target:self];
}

- (NSString *) iconForIndex: (int)val
{
    return nil;
}

- (void) openCallback: (GGBouton *)button
{
    int tag = [button tag];
    [self switchTo: tag - 5];
}
@end

@implementation TopViewManager
- initWithTag: (int) val
      classes: (NSArray *) a
       window: (GGNSWindow *) win
{
    [super initWithTag: val
                window: win];
    currentC = -1;
    classes = [a copy];
    dico = [NSMutableDictionary new];
    max = [classes count];
    
    return self;
}

- (TopView *) topviewForIndex: (int) val
{
    id c;
    TopView *t;
    c = [classes objectAtIndex: currentC];
    t = [dico objectForKey: c];
    
    if(!t)
    {
        t = [c topview];
        [t setManager: self];
        [dico setObject: t
                 forKey: c];
    }
    return t;
}

- (void) switchTo: (int) val
{
    TopView *t;
    if( val < 0 || val >= [classes count] )
        return;
    
    currentC = val;
    
    t = [self topviewForIndex: val];
    
    [window popAllView];
    [t openIn: window];
}

- (NSString *) iconForIndex: (int)val
{
    id c;
    NSParameterAssert(val >= 0 && val < [classes count]);
    
    c = [classes objectAtIndex: val];
    
    if( [c conformsToProtocol: @protocol(IconsInfo)] )
        return [c iconName];
    else
        return nil;
}

- (void) dealloc
{
    RELEASE(classes);
    RELEASE(dico);
    [super dealloc];
}

@end

@implementation TopView
+ topview
{
    return AUTORELEASE([[self alloc] init]);
}

- init
{
    NSRect aRect = NSMakeRect(0, HauteurTableauBord, WIDTH, 
                              HEIGHT-HauteurTableauBord);
    [self initWithFrame: aRect];
    return self;
}

- (id) initWithFrame: (NSRect)frameRect
{
    [super initWithFrame: frameRect];
    
    {
        GGBouton *bo;
        
        bo = AUTORELEASE([GGBouton new]);
        [bo setFrame: NSMakeRect(frameRect.size.width-100, 10, 100, 35)];
        [bo setTitle: @"close"];
        [bo setTarget: self];
        [bo setAction: @selector(closeButton)];
        [bo setAutoresizingMask: GGNSViewMinXMargin | GGNSViewMaxYMargin];
        
        [self addSubview: bo];
        
        title = [[GGString alloc] initWithFrame: 
            NSMakeRect(300, frameRect.size.height-30,
					   150, 20)];
        [title setString: @"TopView"
                withFont: [GGFont defaultTitleFont]];
        [self addSubview: title];
        RELEASE(title);
    }
    [self setAutoresizingMask: GGNSViewWidthSizable | GGNSViewHeightSizable];
    //  [self setState: GUI_OtherView];
    
    return self;
}

- (void) setTexture: (Texture *)_t
{
    ASSIGN(t, _t);
}

- (void) enableTextureBackground
{
    if( thePref.HighQuality)
        [self setTexture: [Texture textureWithFileName: @"marble.jpg"
                                                mipmap: NO]];
}

- (void) drawRect: (NSRect) _theRect
{
    NSRect theRect = [self frame];
    if( t)
    {
        glPushAttrib(GL_ENABLE_BIT);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, [t idTexture]);
        glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
        glColor3f(0.4, 0.4, 0.4);
        glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2f(0, 0);
        
        glTexCoord2f(1, 0);
        glVertex2f(NSWidth(theRect), 0);
        
        glTexCoord2f(1, 1);
        glVertex2f(NSWidth(theRect), NSHeight(theRect));
        
        glTexCoord2f(0, 1);
        glVertex2f(0, NSHeight(theRect));
        glEnd();
        
        glPopAttrib();
    }
    else
    {
        //        glClearColor(0, 0, 0, 0);
        //        glClear(GL_COLOR_BUFFER_BIT);
        glColor3f(0,0,0);
        glRecti(0,0,NSWidth(theRect), NSHeight(theRect));
    }
    GLCHECK;
}

- (GGString *) titleView
{
    return title;
}

- (void) dealloc
{
    [[NSNotificationCenter defaultCenter]
    removeObserver: nil 
              name: nil 
            object: self];
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    RELEASE(t);
    [super dealloc];
}

- (void) openIn: (GGNSWindow *)win
{
    [win addTopView: self];
    //  [self setNextResponder: nil];
}

- (void) closing
{
}

- (void) viewWillMoveToWindow: (GGNSWindow *)win
{
    if (_window && win == nil)
        [self closing];
    [super viewWillMoveToWindow: win];
}

- (void) closeButton
{
    if ( manager )
        [manager invalidate];
    else
        [self close];
}


- (void) close
{
    RETAIN(self);
    NSAssert(_window != nil, @"bad");
    
    [_window aTopViewIsClosing: self];
    [self removeFromSuperview];
    AUTORELEASE(self);
}

// - (GUIState) state
// {
//   return _state;
// }

// - (void) setState: (GUIState) newState
// {
//   _state = newState;
// }

- (void) setManager: (TopViewManagerCommon *) obj
{
    manager = obj;
}

- (void) keyDown: (Event *)pev
{
    if (manager && [manager acceptKeyDown: pev])
        return;
    else
        [super keyDown: pev];
}
@end
