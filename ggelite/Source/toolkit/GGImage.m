//
//  GGImage.m
//  elite
//
//  Created by Frederic De Jaeger on 10/09/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "utile.h"
#import "Metaobj.h"
#import "Resource.h"
#import "GGImage.h"



#ifdef HAVE_COCOA
#import <Cocoa/Cocoa.h> 
@interface GGNSBitmapImageRep : GGImage
{
    NSBitmapImageRep*   _imageRep;
}
- initWithPath:(NSString*)path;
@end

@implementation GGNSBitmapImageRep
- initWithPath:(NSString*)fileName
{
    _imageRep = [NSBitmapImageRep imageRepWithContentsOfFile:fileName];
    if(_imageRep){
        [_imageRep retain];
        //        NSSize size = [_imageRep size];
        _width = [_imageRep pixelsWide];
        _height = [_imageRep pixelsHigh];
        //        *pwidth = [_imageRep bytesPerRow]/[_imageRep samplesPerPixel];
        _ncomp = [_imageRep samplesPerPixel];
        _scanLine = [_imageRep bytesPerRow] / _ncomp;
        _bytesPerRow = [_imageRep bytesPerRow];
/*        if(_ncomp >= 3){
            _alignement = 4;
        }*/
        /*        if(3 == *pncomp){
            *pncomp = 4;
        }*/
        
//        NSLog(@"%@", _imageRep);
//
//        NSLog(@"%@: width=%d height=%d ncomp=%d", [fileName lastPathComponent], _width, _height, _ncomp);
//        NSLog(@"wx=%d hx=%d sample = %d, bits = %d bitmapFormat=%d bitsPerPixel=%d bytesPerPlane=%d bytesPerRow=%d numberOfPlanes=%d", [_imageRep pixelsWide], [_imageRep pixelsHigh], [_imageRep samplesPerPixel], [_imageRep bitsPerSample], [_imageRep bitmapFormat], [_imageRep bitsPerPixel], [_imageRep bytesPerPlane], [_imageRep bytesPerRow], [_imageRep numberOfPlanes]);
        addToCheck(self);
        return self;
    }
    else{
        [self release];
        return nil;
    }
}

- (void) dealloc
{
    [_imageRep release];
    [super dealloc];
}

- (void*) rawData
{
    return [_imageRep bitmapData];
}

@end
#endif

#ifdef HAVE_JPEG
#import <jpeglib.h>

static void writeRGBTofileJPG(int width, int height, const unsigned char *data)
{
    static int ind = 1;
    NSString *fileName;
    int quality = 90;
    
    
    /* This struct contains the JPEG compression parameters and pointers to
        * working space (which is allocated as needed by the JPEG library).
        * It is possible to have several such structures, representing multiple
        * compression/decompression processes, in existence at once.  We refer
        * to any one struct (and its associated working data) as a "JPEG object".
        */
    struct jpeg_compress_struct cinfo;
    /* This struct represents a JPEG error handler.  It is declared separately
        * because applications often want to supply a specialized error handler
        * (see the second half of this file for an example).  But here we just
        * take the easy way out and use the standard error handler, which will
        * print a message on stderr and call exit() if compression fails.
        * Note that this struct must live as long as the main JPEG parameter
        * struct, to avoid dangling-pointer problems.
        */
    struct jpeg_error_mgr jerr;
    /* More stuff */
    FILE * outfile;               /* target file */
    JSAMPROW row_pointer[1];      /* pointer to JSAMPLE row[s] */
    int row_stride;               /* physical row width in image buffer */
    
    /* Step 1: allocate and initialize JPEG compression object */
    
    /* We have to set up the error handler first, in case the initialization
        * step fails.  (Unlikely, but it could happen if you are out of memory.)
        * This routine fills in the contents of struct jerr, and returns jerr's
        * address which we place into the link field in cinfo.
        */
    
    fileName = [NSString stringWithFormat: @"pic%d.jpg", ind];
    ind ++;
    cinfo.err = jpeg_std_error(&jerr);
    /* Now we can initialize the JPEG compression object. */
    jpeg_create_compress(&cinfo);
    
    /* Step 2: specify data destination (eg, a file) */
    /* Note: steps 2 and 3 can be done in either order. */
    
    /* Here we use the library-supplied code to send compressed data to a
        * stdio stream.  You can also write your own code to do something else.
        * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
        * requires it in order to write binary files.
        */
    if ((outfile = fopen([fileName cString], "wb")) == NULL) {
        NSLog(@"can't open %@\n", fileName);
        exit(1);
    }
    jpeg_stdio_dest(&cinfo, outfile);
    
    /* Step 3: set parameters for compression */
    
    /* First we supply a description of the input image.
        * Four fields of the cinfo struct must be filled in:
        */
    cinfo.image_width = width;      /* image width and height, in pixels */
    cinfo.image_height = height;
    cinfo.input_components = 3;           /* # of color components per pixel */
    cinfo.in_color_space = JCS_RGB;       /* colorspace of input image */
    /* Now use the library's routine to set default compression parameters.
        * (You must set at least cinfo.in_color_space before calling this,
           * since the defaults depend on the source color space.)
        */
    jpeg_set_defaults(&cinfo);
    /* Now you can set any non-default parameters you wish to.
        * Here we just illustrate the use of quality (quantization table) scaling:
        */
    jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);
    
    /* Step 4: Start compressor */
    
    /* TRUE ensures that we will write a complete interchange-JPEG file.
        * Pass TRUE unless you are very sure of what you're doing.
        */
    jpeg_start_compress(&cinfo, TRUE);
    
    /* Step 5: while (scan lines remain to be written) */
    /*           jpeg_write_scanlines(...); */
    
    /* Here we use the library's state variable cinfo.next_scanline as the
        * loop counter, so that we don't have to keep track ourselves.
        * To keep things simple, we pass one scanline per call; you can pass
        * more if you wish, though.
        */
    row_stride = width * 3; /* JSAMPLEs per row in image_buffer */
    
    while (cinfo.next_scanline < cinfo.image_height) {
        /* jpeg_write_scanlines expects an array of pointers to scanlines.
        * Here the array is only one element long, but you could pass
        * more than one scanline at a time if that's more convenient.
        */
        row_pointer[0] = (unsigned char *)& data[(height - 1 - cinfo.next_scanline )* row_stride];
        (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
    }
    
    /* Step 6: Finish compression */
    
    jpeg_finish_compress(&cinfo);
    /* After finish_compress, we can close the output file. */
    fclose(outfile);
    
    /* Step 7: release JPEG compression object */
    
    /* This is an important step since it will release a good deal of memory. */
    jpeg_destroy_compress(&cinfo);
    
    /* And we're done! */
}

static void *rawImageFromJPGFileMalloc(const char *fileName, int *pwidth,
                                       int *pheight, int *pncomp)
{
    struct jpeg_decompress_struct cinfo;
    struct jpeg_error_mgr jerr;
    int width, height;
    int	row_stride;
    FILE * infile;		/* source file */
    JSAMPROW row_pointer[1];
    unsigned char *dataChar;
    
    
    if ((infile = fopen(fileName, "rb")) == NULL) {
        NSWarnLog(@"can't open %@", fileName);
        return NULL;
    }
    
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_decompress(&cinfo);
    
    jpeg_stdio_src(&cinfo, infile);
    
    jpeg_read_header(&cinfo, TRUE);
    
    jpeg_start_decompress(&cinfo);
    
    *pwidth = width = cinfo.output_width;
    *pheight = height = cinfo.output_height;
    *pncomp = cinfo.output_components;
    row_stride = cinfo.output_components*cinfo.output_width;
    
    dataChar = malloc(width*height*cinfo.output_components);
    while (cinfo.output_scanline < cinfo.output_height) 
    {
        row_pointer[0] = dataChar+(cinfo.output_height - 1 - 
                                   cinfo.output_scanline)*row_stride;
        jpeg_read_scanlines(&cinfo, row_pointer, 1);
    }
    (void) jpeg_finish_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    fclose(infile);
    
    return dataChar;
}
#endif

#if defined(HAVE_SDLIMAGE) 
#import <SDL/SDL.h>
#import <SDL_image/SDL_image.h>
static void *rawImageFromSDLFileMalloc(const char *fileName, int *pwidth,
                                       int *pheight, int *pncomp)
{
    char *data = NULL;
    SDL_PixelFormat *format;
    SDL_Surface *surface;
    
    surface = IMG_Load(fileName);
    if(surface == NULL)
    {
        NSLog(@"cannot open %s", fileName);
        return NULL;
    }
    
    format = surface->format;
    if( format->palette )
        NSLog(@"surface has a palette");
    
    
    if( format->BytesPerPixel == 3 )
    {
        int i, j;
        
        data = malloc(3 * surface->w * surface->h);
        NSCParameterAssert(data);
        *pwidth = surface->w;
        *pheight = surface->h;
        *pncomp = 3;
        for(i = 0; i < surface->h; ++i)
        {
            Uint8 *linedest = (Uint8 *)data + i*3*surface->w;
            Uint8 *linesrc = (Uint8 *)surface->pixels + i * surface->pitch;
            
            for(j = 0; j < surface->w; ++j)
            {
                Uint8 *dest = linedest + 3*j;
                Uint32 val;
                /*SDL is a piece of shit
                    */
#if (SDL_BYTEORDER==SDL_LIL_ENDIAN)
                val = (linesrc[3*j]) | ((linesrc[3*j+1]) << 8) | 
                    ((linesrc[3*j+2]) << 16);
#else
                val = (linesrc[3*j+2]) | (linesrc[3*j+1] << 8) | 
                    (linesrc[3*j] << 16);
#endif
                
                SDL_GetRGB(val, format, dest, dest+1, dest+2);
            }
        }
    }
    else if( format->BytesPerPixel == 4 )
    {
        int i, j;
        
        data = malloc(4 * surface->w * surface->h);
        NSCParameterAssert(data);
        *pwidth = surface->w;
        *pheight = surface->h;
        *pncomp = 4;
        for(i = 0; i < surface->h; ++i)
        {
            Uint8 *linedest = (Uint8 *)data + i*4*surface->w;
            Uint32 *linesrc = (Uint32 *)(surface->pixels + (surface->h -1 - i) * surface->pitch);
            
            for(j = 0; j < surface->w; ++j)
            {
                Uint8 *dest = linedest + 4*j;
                Uint32 val = linesrc[j];
                
                SDL_GetRGBA(val, format, dest, dest+1, dest+2, dest+3);
            }
        }
    }
    else
    {
        NSLog(@"don't know how to handle this type of SDL_surface for %s", fileName);
    }
    SDL_FreeSurface(surface);
    
    return data;
}
#endif

@interface GGImageLegacy : GGImage
{
    void *          _data;
}
@end

@implementation GGImageLegacy
- (id) initWithPath:(NSString*)path
{
#ifdef    HAVE_JPEG  
    if( [extension isEqualToString: @"jpg"] )
        _data = rawImageFromJPGFileMalloc([path cString],
                                         &_width, &_height, &_ncomp);
    else
#endif
#if defined(HAVE_SDLIMAGE)
        _data = rawImageFromSDLFileMalloc([path cString],
                                         &_width, &_height, &_ncomp);
#endif
    if(NULL == _data){
        [self release];
        return nil;
    }
    else{
        NSLog(@"call with %@", path);
        _bytesPerRow = _width*_ncomp;
        _scanLine = 0;
    }
    
    return self;
}

- (void) dealloc
{
    if(_data){
        free(_data);
    }
    [super dealloc];
}

- (void*) rawData
{
    return _data;
}

@end


@implementation GGImage
- (int) width
{
    return _width;
}

- (int) height
{
    return _height;
}

- (int) scanLine
{
    return _scanLine;
}

- (int) alignement
{
    return _alignement;
}

- (int) ncomp
{
    return _ncomp;
}

- (int) bytesPerRow
{
    return _bytesPerRow;
}

- (void*) rawData
{
    [self subclassResponsibility:_cmd];
    return NULL;
}

- (void) compositeToRect:(NSRect)destRect
{
    int maxx = destRect.size.width;
    int maxy  = destRect.size.height;
    
    int i, j;
    const GLubyte *baseData = [self rawData];
    glDisable(GL_CULL_FACE);
    glBegin(GL_QUADS);
    for(j = 0; j < maxy; ++j){
        int y = j * _height / maxy;
        const GLubyte *line = baseData + y*_bytesPerRow;
        if( y >= _height ){
            y = _height-1;
        }
        for(i = 0; i < maxx; ++i){
            int x = i * _width / maxx;
            if(x >= _width){
                x = _width-1;
            }
            const GLubyte *ptr = line + x * _ncomp;
            GLubyte red, green, blue;
            switch(_ncomp){
                case 3:
                case 4:
                    red = ptr[0];
                    green = ptr[1];
                    blue = ptr[2];
                    break;
                case 1:
                    red = blue = green = ptr[0];
                    break;
                default:
                    red = blue = green = 0.0;
                    ggabort();
            }
            glColor3ub(red, green, blue);
            glVertex2i(i,j);
            glVertex2i(i,j+1);
            glVertex2i(i+1,j+1);
            glVertex2i(i+1,j);
        }
    }
    glEnd();
}

- (void) drawPixels
{
    GLCHECK;
    glPixelStorei(GL_UNPACK_ROW_LENGTH,_scanLine);
    glPixelStorei(GL_UNPACK_ALIGNMENT,4);
    GLenum format;
    switch(_ncomp){
        case 3:
            format = GL_RGB;
            break;
        default:
            format = 0;
            ggabort();
            break;
    }
    glRasterPos2i(0,0);
    glDrawPixels(_width,_height,format,GL_UNSIGNED_BYTE,[self rawData]);
    GLCHECK;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"[w=%d h=%d bytesperrow=%d ncomp=%d align=%d]",
        _width, _height, _bytesPerRow, _ncomp, _alignement];
}

+ (GGImage*)imageFromPath:(NSString*)fileName
{
    NSString *extension;
    
    if (fileName == nil)
    {
        NSWarnLog(@"fileName = nil!!");
        return nil;
    }
    
    extension = [fileName pathExtension];
#if defined(HAVE_COCOA)
    GGImage* ret =  [[[GGNSBitmapImageRep alloc] initWithPath:fileName] autorelease];
#else
    GGImage* ret =  [[[GGImageLegacy alloc] initWithPath:fileName] autorelease];
#endif
    
    return ret;
}

+ (GGImage*)imageWithName:(NSString*)name
{
    NSString* path = [[Resource resourceManager] pathForTexture:name];
    return [self imageFromPath:path];
}

@end
