//
//  GGEventDispatcher.m
//  elite
//
//  Created by Frederic De Jaeger on 12/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGEventDispatcher.h"
#import "GGEvent.h"
#import "utile.h"
#import "NSResponder.h"

@interface GGKeyMapping : NSObject
{
    NSString*           _commandName;
    NSString*           _keystroke;
    id                  _extraArgument;
    NSString*           _localizedDescription;
}
- (id) initWithCommandName:(NSString*)commandName
                 keyStroke:(NSString*)keyStroke
             extraArgument:(id)extraArgument
      localizedDescription:(NSString*)localizedDescription;
- (BOOL) dispatchKeyDownOnTarget:(id)target;
- (BOOL) dispatchKeyUpOnTarget:(id)target;
@end

@interface NSString (GGUtilities)
- (NSString*) gg_capitalizeFirstLetter;
@end

@interface GGEventDispatcher (Private)
- (void) setAncestor:(GGEventDispatcher*)ancestor;
- (GGKeyMapping*) _commandForKey:(NSString*)key;
@end

static BOOL tryDispatchSelector(id target, SEL selector, id argument) 
{
    NSCParameterAssert(selector);
    
    if([target isKindOfClass:[GGResponder class]]){
        return [(GGResponder*)target tryToPerform:selector 
                                             with:argument];
    }
    else{
        if(![target respondsToSelector:selector]){
            NSDebugFLLog(@"KeyMapping", @"%@ does not respond to %@", target, NSStringFromSelector(selector));
            
            return NO;
        }
        else{
            NSDebugFLLog(@"KeyMapping", @"%@ responds to %@", target, NSStringFromSelector(selector));
            [target performSelector:selector 
                         withObject:argument];
            
            return YES;
        }
    }
}

@implementation GGKeyMapping
- (id) initWithCommandName:(NSString*)commandName
                 keyStroke:(NSString*)keyStroke
             extraArgument:(id)extraArgument
      localizedDescription:(NSString*)localizedDescription
{
    self = [super init];
    if(nil != self){
        _commandName = [commandName copy];
        _keystroke = [keyStroke copy];
        _extraArgument = [extraArgument copy];
        _localizedDescription = [localizedDescription copy];
    }
    
    return self;
}

- (id) initFromDictionary:(NSDictionary*)dictionary
{
    return [self initWithCommandName:[dictionary objectForKey:kGGCommandName]
                           keyStroke:[dictionary objectForKey:kGGKeyStroke]
                       extraArgument:[dictionary objectForKey:kGGExtraArgument]
                localizedDescription:[dictionary objectForKey:kGGDescription]];
}

- (void) dealloc
{
    [_commandName release];
    [_keystroke release];
    [_extraArgument release];
    [_localizedDescription release];
    
    [super dealloc];
}

- (NSString*)keystroke
{
    return _keystroke;
}

- (BOOL) _tryDispatchSelectorName:(NSString*)selectorName
                           target:(id)target
{
    SEL selector = NSSelectorFromString(selectorName);
    NSParameterAssert(selector);
    
    return tryDispatchSelector(target, selector, _extraArgument);
}


- (BOOL) dispatchKeyDownOnTarget:(id)target
{
    NSString* selectorName = _commandName;
    if(nil==selectorName || [selectorName length] == 0){
        return NO;
    }
    
    BOOL ret = [self _tryDispatchSelectorName:selectorName target:target]
        || [self _tryDispatchSelectorName:[@"start" stringByAppendingString:[selectorName gg_capitalizeFirstLetter]]
                                   target:target];
    
    return ret;
}

- (BOOL) dispatchKeyUpOnTarget:(id)target
{
    NSString* selectorName = _commandName;
    if(nil==selectorName || [selectorName length] == 0){
        return NO;
    }
    
    return [self _tryDispatchSelectorName:[@"stop" stringByAppendingString:[selectorName gg_capitalizeFirstLetter]]
                                   target:target];
}
@end

@implementation NSString (GGUtilities)
- (NSString*) gg_capitalizeFirstLetter
{
    int length = [self length];
    if(0 == length){
        return self;
    }
    
    if(1 == length){
        return [self capitalizedString];
    }
    
    
    NSString* firstLetter = [self substringToIndex:1];
    return [[firstLetter capitalizedString] stringByAppendingString:[self substringFromIndex:1]];
}
@end

@implementation GGEventDispatcher
static NSMutableDictionary* sDispatcherByName;

- initWithDictionary:(NSDictionary*)mapping
        inHeritsFrom:(GGEventDispatcher*)ancestorDispatcher
{
    self = [super init];
    
    if(self){
        _mapping = [[NSMutableDictionary alloc] initWithCapacity:[mapping count]];
        NSString* keyCombo;
        
        NSEnumerator* keyEnum = [mapping keyEnumerator];
        
        while((keyCombo = [keyEnum nextObject])!=nil){
            NSString* command = [mapping objectForKey:keyCombo];
            NSParameterAssert(command);
            GGKeyMapping* aMapping = [[GGKeyMapping alloc] initWithCommandName:command
                                                                     keyStroke:keyCombo
                                                                 extraArgument:nil
                                                          localizedDescription:nil];
            [_mapping setObject:aMapping forKey:keyCombo];
            [aMapping release];
        }
        _ancestorDispatcher = [ancestorDispatcher retain];
    }
    return self;
}

- (id) _initWithProperty:(NSDictionary*) property
                    name:(NSString*)name
{
    self = [super init];
    if(self){
        NSArray* bindings = [property objectForKey:kGGBindings];
        _mapping = [[NSMutableDictionary alloc] initWithCapacity:[bindings count]];
        _tableName = [name copy];
        NSDictionary* aBinding;
        NSEnumerator* enumerator = [bindings objectEnumerator];
        
        while((aBinding = [enumerator nextObject]) != nil){
            GGKeyMapping* aMapping = [[GGKeyMapping alloc] initFromDictionary:aBinding];
            NSString* keystroke = [aMapping keystroke];
            if(keystroke){
                [_mapping setObject:aMapping forKey:keystroke];
            }
            else{
                NSWarnMLog(@"strange, no keystroke defined");
                ggabort();
            }
            [aMapping release];
        }
    }
    
    return self;
}

+ dispatcherWithName:(NSString*)name
{
    if(nil == sDispatcherByName){
        NSString* path = [[NSBundle mainBundle] pathForResource:@"keymapping"
                                                         ofType:@"plist"];
        NSAssert(path, @"cannot find keymapping.plist");
        NSDictionary* plist = [NSDictionary dictionaryWithContentsOfFile:path];
        NSAssert(plist, @"cannot load keymapping.plist");
        sDispatcherByName = [[NSMutableDictionary alloc] initWithCapacity:[plist count]];
        
        NSEnumerator* keyEnum = [plist keyEnumerator];
        NSString* key;
        while((key = [keyEnum nextObject])!=nil){
            NSDictionary* dico = [plist objectForKey:key];
            GGEventDispatcher* aDispatcher = [[GGEventDispatcher alloc] _initWithProperty:dico
                                                                                     name:key];
            [sDispatcherByName setObject:aDispatcher forKey:key];
            [aDispatcher release];
        }
        
        keyEnum = [plist keyEnumerator];
        while((key = [keyEnum nextObject])!=nil){
            NSDictionary* dico = [plist objectForKey:key];
            NSString* ancestorName = [dico objectForKey:kGGDispatcherAncestor];
            if(ancestorName){
                GGEventDispatcher* aDispatcher = [sDispatcherByName objectForKey:key];
                GGEventDispatcher* ancestor = [sDispatcherByName objectForKey:ancestorName];
                if(nil == ancestor){
                    NSWarnMLog(@"Could not find ancestor with name %@ for key bindings %@", ancestorName, key);
                }
                else{
                    [aDispatcher setAncestor:ancestor];
                }
            }
        }
        
    }
    
    return [sDispatcherByName objectForKey:name];
}

- (void) dealloc
{
    [_mapping release];
    [_ancestorDispatcher release];
    [_tableName release];
    
    [super dealloc];
}

- (void) setAncestor:(GGEventDispatcher*)ancestor
{
    ASSIGN(_ancestorDispatcher, ancestor);
}

- (GGKeyMapping*) _commandForKey:(NSString*)key
{
    GGKeyMapping* map = [_mapping objectForKey:key];
    if(nil == map && nil != _ancestorDispatcher){
        NSDebugMLLog(@"KeyMapping", @"%@:Lookup ancestor for key %@", _tableName, key);
        map = [_ancestorDispatcher _commandForKey:key];
    }
    
    return map;
}

- (BOOL) dispatchKeyDownWithEvent:(Event*) event
                           target:(id)target
{
    if(nil == target){
        return NO;
    }
    
    BOOL basicText;
    NSString* key = stringKeyForEvent(event, &basicText);
    if(nil == key){
        return NO;
    }
    
    GGKeyMapping* map = [self _commandForKey:key];
    if(map && [map dispatchKeyDownOnTarget:target]){
        return YES;
    }

    if(basicText){
        // try to insert text.
        return tryDispatchSelector(target, @selector(insertText:), key);
    }
    else{
        return NO;
    }
}

- (BOOL) dispatchKeyUpWithEvent:(Event*) event
                         target:(id)target
{
    if(nil == target){
        return NO;
    }
    NSString* key = stringKeyForEvent(event, NULL);
    if(nil == key){
        return NO;
    }
    
    GGKeyMapping* map = [self _commandForKey:key];
    
    if(nil==map){
        return NO;
    }
    
    return [map dispatchKeyUpOnTarget:target];
}

@end
