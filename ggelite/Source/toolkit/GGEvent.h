//
//  GGEvent.h
//  elite
//
//  Created by Frederic De Jaeger on 17/07/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#ifndef __GGEvent_h
#define __GGEvent_h 1
extern const int GG_BUTTON_LEFT;
extern const int GG_BUTTON_MIDDLE;
extern const int GG_BUTTON_RIGHT ;
extern const int GG_BUTTON_WHEELUP;
extern const int GG_BUTTON_WHEELDOWN;

extern const int GG_SHIFT_MASK;
extern const int GG_CONTROL_MASK;
extern const int GG_COMMAND_MASK;
extern const int GG_ALT_MASK;
//event type

typedef enum {
    GG_KEYDOWN,			/* Keys pressed */
    GG_KEYUP,			/* Keys released */
    GG_MOUSEMOTION,			/* Mouse moved */
    GG_MOUSEBUTTONDOWN,		/* Mouse button pressed */
    GG_MOUSEBUTTONUP,		/* Mouse button released */
    GG_SCROLLWHEEL,
    GG_JOYSTICKAXIS,
    GG_JOYSTICKBUTTONUP,
    GG_JOYSTICKBUTTONDOWN,
    GG_JOYSTICKHAT,
    GG_OTHER_EVENT,
}GGEventType;


// anonymous struct 

typedef struct __Event Event;

int buttonOfEvent(Event *pev);  // works for mouse and joystick events
NSPoint mouseLocationOfEvent(Event *pev);
NSPoint mouseMotionOfEvent(Event *pev);

float deltaXOfEvent(Event *_pev);
float deltaYOfEvent(Event *_pev);
float deltaZOfEvent(Event *_pev);


NSString* stringKeyForEvent(Event *_pev, BOOL* basicText);
GGEventType typeOfEvent(Event *pev);
unsigned modifiersOfEvent(Event *pev);

BOOL isADragMove(Event *pev);

// joystick only
int axeOfEvent(Event* pev);
double  normalizedValue(Event* pev);

#endif