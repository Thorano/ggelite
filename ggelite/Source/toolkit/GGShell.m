//
//  GGShell.m
//  elite
//
//  Created by Frédéric De Jaeger on 11/29/04.
//  Copyright 2004 __MyCompanyName__. All rights reserved.
//

#import "GGShell.h"
#import "GGWidget.h"
#import "Console.h"
#import "Metaobj.h"

@implementation GGShell
static GGShell* sDefaultShell;
+ (GGShell*) defaultShell
{
    NSParameterAssert(sDefaultShell);
    return sDefaultShell;
}

- (id) initWithFrame: (NSRect) aRect
{
    self = [super initWithFrame:aRect];
    _inputView = [[GGString alloc] initWithFrame: NSMakeRect(0,0,aRect.size.width,20)];
    [_inputView setEditable:YES];
    [self addSubview:_inputView];
    
    _console = [[Console alloc] initWithFrame:NSMakeRect(0,21,aRect.size.width,aRect.size.height-50)];
    [self addSubview:_console];
    [_console logString: @"Hello guys"];

    [self setTransparency: 0.2];
    [self enableTextureBackground];
    [self setTitle: @"Shell"];
    
    
    if ( sDefaultShell ){
        NSWarnMLog(@"have already a default shell");
        [sDefaultShell release];
    }
    sDefaultShell = [self retain];
    addToCheck(self);
    
    return self;
}

- (void) dealloc
{
    RELEASE(_inputView);
    RELEASE(_console);
    
    [super dealloc];
}

- (void) display: (NSString*) string
{
    [_console logString: string];
}

@end
