#import <Foundation/NSArray.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSData.h>
#import <Foundation/NSValue.h>

#import "GGLoop.h"
#import "Metaobj.h"
#import "NSView.h"
#import "NSWindow.h"
#import "Preference.h"
#import "GGWidget.h"
#import "TopView.h"
#import "utile.h"

@interface GGView (GGExtension)
- (void) wasViewUnderMouse;
- (void) isViewUnderMouse;
@end

@implementation GGView (GGExtension)
- (void) wasViewUnderMouse{}
- (void) isViewUnderMouse{}
@end

@interface GGContentView : GGView
{}
@end

@implementation GGContentView
- (BOOL) isOpaque
{
    return YES;
}

- initWithFrame: (NSRect) aRect
{
    [super initWithFrame: aRect];
    //  [self setAutoresizesSubviews: NO];
    return self;
}
@end

@interface GGNSWindow (Private)
- (void) setFirstOpaque: (GGNSWindow *) aWin;
@end


@implementation GGNSWindow
+ window
{
    return AUTORELEASE([self new]);
}

- (GGView *)firstResponder
{
    return firstResponder;
}

- (BOOL) makeFirstResponder: (GGView *)aView
{
    //   if( (id) aView == self )
    //     aView = nil;
    firstResponder = aView;
    NSDebugMLLog(@"Event", @"view = %@", aView);
    return YES;
}


- (void) flushWindow
{
}

- (void) setViewsNeedDisplay: (BOOL) flag
{
}

- initWithFrame: (NSRect) aRect
{    
    GGView *aView;
    [super init];
    frameWindow = aRect;
    firstResponder = nil;
    contentView = nil;
    open = 0;
    windowOnTop = self;
    windowAbove = nil;
    windowBelow = nil;
    firstOpaque = nil;
    doBackGround = YES;
    couleur = GGMakeCol(0, 0, 0, 0);
    grabeur = nil;
    metaView = [GGContentView new];
    [metaView setFrame: NSMakeRect(0, 0, frameWindow.size.width, 
                                   frameWindow.size.height)];
    [metaView viewWillMoveToWindow: self];
    [self setContentView: AUTORELEASE([[GGView alloc] initWithFrame: aRect])];
    aView = [self contentView];
    [aView setAutoresizingMask: GGNSViewWidthSizable | GGNSViewHeightSizable];
    
    [self setOpacity: 0.5];
    addToCheck(self);
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(screenSizeChanged:)
                                                 name: GGScreenSizeChange
                                               object: nil];
    
    _stackView = [NSMutableArray new];
    return self;
}

//  - init
//  {
//    NSRect aRect = NSMakeRect(0, 0, WIDTH, HEIGHT);
//    [self initWithFrame: aRect];
//    return self;
//  }

- init
{
    NSRect aRect = NSMakeRect(0, HauteurTableauBord, WIDTH, 
                              HEIGHT-HauteurTableauBord);
    [self initWithFrame: aRect];
    return self;
}



- (void) addCloseButton
{
    GGBouton *bo;
    
    bo = AUTORELEASE([GGBouton new]);
    [bo setFrame: NSMakeRect(frameWindow.size.width-100, 10, 100, 35)];
    [bo setTitle: @"close"];
    [bo setTarget: self];
    [bo setAction: @selector(close)];
    [bo setAutoresizingMask: GGNSViewMinXMargin | GGNSViewMaxYMargin];
    
    [[self contentView] addSubview: bo];
    
}

- (void) setFrameWindow: (NSRect) newFrame
{
    frameWindow = newFrame;
    [metaView setFrameSize: newFrame.size];
    //  [contentView setFrameSize: frameWindow.size];
}

- (void) screenSizeChanged: (NSNotification *)obj
{
    NSRect newFrame = frameWindow;
    float cx, cy;
    NSValue * old = [obj object];
    NSSize oldSize = [old sizeValue];
    NSDebugMLLog(@"Resize", @"old : %g %g new : %d %d",
                 oldSize.width, oldSize.height, WIDTH, HEIGHT);
    newFrame.size.width *= cx = WIDTH/oldSize.width;
    newFrame.size.height *= cy = HEIGHT/oldSize.height;
    newFrame.origin.x *= cx;
    newFrame.origin.y *= cy;
    NSDebugMLLog(@"Resize", @"new : %g %g", newFrame.size.width, 
                 newFrame.size.height);
    [self setFrameWindow: newFrame];
    
    
    NSDebugMLLog(@"Resize", @"new : %g %g new : %d %d",
                 frameWindow.size.width, frameWindow.size.height, WIDTH, HEIGHT);
}

- (void) setOpacity:(float) val
{
    GGReal opacity;
    NSAssert(!open, @"try to set opacity when already open");
    opacity = val;
    opacity = (opacity < 0 ? 0 : opacity);
    opacity = (opacity > 1 ? 1 : opacity);
    couleur.a = opacity;
    NSDebugMLLog(@"Opacity", @"opacity %g arg %g", opacity, val);
    if (opacity == 1 && firstOpaque == nil )
    {
        [self setFirstOpaque: self];
    }
}

- (GGReal) opacity
{
    if( thePref.GLBlend )
        return couleur.a;
    else
        return 1.0;
}

- (void) setBackgroundColor: (VectCol) val
{
    couleur.r = val.r;
    couleur.g = val.g;
    couleur.b = val.b;
    [self setOpacity: val.a];
}

- (BOOL) isOpen
{
    return open;
}

- (void) opening
{
}

- (void) open
{
    if( !open )
    {
        open = YES;
        [theLoop pushWindow: self];
        [self opening];
    }
}

- (void) closing
{
}

- (void) close
{
    if( open )
    {
        open = NO;
        RETAIN(self);
        [theLoop popWindow: self];
        [self closing];
        RELEASE(self);
    }
}

- (void)dealloc 
{
    /*Inutile, ce cause des segfault à la fin du programme
    */
    //  if( open )
    //    [self close];
    // that's not robust to do stuff like that in a dealloc.
    [metaView removeSubview:contentView];
    RELEASE(_currentView);
    RELEASE(_stackView);
    
    RELEASE(viewUnderMouse);
    RELEASE(grabeur);
    RELEASE(windowAbove);
    RELEASE(metaView);
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [super dealloc];
}

- (void) setContentView: (GGView*)aView
{
    if (aView == nil)
    {
        aView = AUTORELEASE([GGView new]);
    }
    if (contentView != nil)
    {
        [contentView removeFromSuperview];
    }
    contentView = aView;
    [contentView setAutoresizingMask: (GGNSViewWidthSizable 
                                       | GGNSViewHeightSizable)];
    [metaView addSubview: contentView];
    [contentView resizeWithOldSuperviewSize: [contentView frame].size]; 
    [contentView setFrameOrigin: [metaView bounds].origin];
    
    NSAssert1 ([[metaView subviews] count] == 1,
               @"window's view has %d	 subviews!", [[metaView subviews] count]);
    
    [contentView setNextResponder: self];
}

- (GGView *)contentView
{
    return contentView;
}

- (void) setFrameInRect: (NSRect) aRect
               atOrigin: (NSPoint) aPoint;
{
    glLoadIdentity();
    glTranslatef(aPoint.x, aPoint.y, 0);
    glScissor((GLint)(frameWindow.origin.x+NSMinX(aRect)), 
              (GLint)(frameWindow.origin.y+NSMinY(aRect)),
              (GLint)(NSWidth(aRect)), (GLint)(NSHeight(aRect)));
}

- (NSRect) convertWindowRectToFrameBufferRect:(NSRect)windowRect
{
    float fx, fy;
    NSRect aRect;
    //  NSDebugMLLog(@"GGNSWindow", @"rect = %@", NSStringFromRect(_aRect));
    fx = 1;
    //(float)WIDTH/VirtualWidth;
    fy = 1;
    //(float)HEIGHT/VirtualHeight;
    aRect.origin = NSMakePoint(fx*NSMinX(windowRect) + frameWindow.origin.x, fy*NSMinY(windowRect) + frameWindow.origin.y);
    aRect.size = NSMakeSize(fx*NSWidth(windowRect), fy*NSHeight(windowRect));
    return aRect;
}

- (void) setGLFrameInRect: (NSRect) _aRect
{
    NSRect aRect = [self convertWindowRectToFrameBufferRect:_aRect];
    glScissor((GLint)(NSMinX(aRect)), 
              (GLint)(NSMinY(aRect)), 
              (GLint)(NSWidth(aRect)), (GLint)(NSHeight(aRect)));
    glViewport((GLint)(NSMinX(aRect)), 
               (GLint)(NSMinY(aRect)), 
               (GLint)(NSWidth(aRect)), (GLint)(NSHeight(aRect)));
    /*
     [theTableau addLogString: NSStringFromRect(_aRect)];
     [theTableau addLogString: NSStringFromRect(aRect)];
     */
}

- (void) initWindowGL
{
    glViewport(frameWindow.origin.x, 
               frameWindow.origin.y, 
               frameWindow.size.width,
               frameWindow.size.height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.,frameWindow.size.width,0,frameWindow.size.height,-1.0,1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glDisable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);
}

- (void) setDrawBackground:(BOOL) f
{
    doBackGround = f;
}


- (void) drawWindow
{
//    NSDebugMLLog(@"display", @"debut display");
    [self initWindowGL];
    glEnable(GL_SCISSOR_TEST);
    if( thePref.GLBlend && couleur.a < 1 )
    {
        glEnable(GL_BLEND);
    }
    
    if( doBackGround )
    {
        [self setFrameInRect: NSMakeRect(0, 0, frameWindow.size.width, 
                                         frameWindow.size.height)
                    atOrigin: NSZeroPoint];
        
        glColor4fv((GGReal *)&couleur);
        glRecti(0,0,frameWindow.size.width,frameWindow.size.height);
    }
    GLCHECK;
    [contentView display];
    GLCHECK;
    glDisable(GL_SCISSOR_TEST);
    glDisable(GL_BLEND);
}

- (void) display
{
    if( !open )
        return;
    if( thePref.GLBlend && firstOpaque != nil && firstOpaque != self )
    {
        [firstOpaque display];
    }
    else if( !thePref.GLBlend && windowOnTop != self )
        [windowOnTop display];
    else
    {
        [self drawWindow];
        [windowAbove display];
    }
}

- (BOOL) handleEvent: (Event *)event
{
    NSPoint 		thePoint;
    GGView		*hitView;
    NSDebugMLLog(@"GGNSWindow", 
                 @"self : %@ grabeur : %@ type : %d\nfirst responder %@", 
                 self,  grabeur, typeOfEvent(event), firstResponder);
    if( !open )
        return 0;
    //    if( windowOnTop != self )
    //      return [windowOnTop handleEvent: event];
    
    thePoint = mouseLocationOfEvent(event);
    NSDebugMLLog(@"GGNSWindow", @"point is %@", NSStringFromPoint(thePoint));

    if( grabeur && [grabeur window]  == nil )
        DESTROY(grabeur);
    
    if( viewUnderMouse && [viewUnderMouse window] == nil )
        DESTROY(viewUnderMouse);
    
    if (grabeur != nil )
    {
        // in case we miss a mouse up
        if(typeOfEvent(event) == GG_MOUSEBUTTONDOWN)
            DESTROY(grabeur);
    }
    
    if( grabeur != nil )
        hitView = grabeur;
    else
    {
        hitView = [contentView hitTest: thePoint];
        if( hitView != viewUnderMouse )
        {
            if( viewUnderMouse )
                [viewUnderMouse wasViewUnderMouse];
            ASSIGN(viewUnderMouse, hitView);
            if( viewUnderMouse )
                [viewUnderMouse isViewUnderMouse];
        }
    }	  
    
    if( hitView == nil )
    {
        if( windowBelow != nil )
            return [windowBelow handleEvent: event];
        else
            return NO;
    }
    
    NSDebugMLLog(@"GGNSWindow", @"hitView %@", hitView);
    switch(typeOfEvent(event))
    {
        case GG_KEYDOWN:
            NSDebugMLLog(@"Event", 
                         @"self : %@ grabeur : %@ type : %d\nfirst responder %@", 
                         self,  grabeur, typeOfEvent(event), firstResponder);
            NSDebugMLLog(@"Event", @"hitview : %@", hitView);
            if( firstResponder != nil )
            {
                
                NSDebugMLLog(@"Event", @"firstResponder->next = %@", 
                             [firstResponder nextResponder]);
                
                
                [firstResponder keyDown: event];
            }
                else
                    [hitView keyDown: event];
            break;
        case GG_KEYUP:
            if( firstResponder != nil )
                [firstResponder keyUp: event];
            else
                [hitView keyUp: event];
            break;
        case GG_MOUSEBUTTONDOWN:
            NSDebugMLLog(@"MousePress", @"%@ point %@, grabeur = %@", hitView,
                         NSStringFromPoint(thePoint), grabeur);
            [hitView mouseDown: event];
            ASSIGN(grabeur, hitView);
            break;
        case GG_MOUSEBUTTONUP:
            [hitView mouseUp: event];
            DESTROY(grabeur);
            break;
        case GG_MOUSEMOTION:
            [hitView mouseMoved: event];
            break;
        case GG_JOYSTICKAXIS:
            [hitView joystickAxisChange: event];
            break;
        case GG_JOYSTICKBUTTONUP:
            [hitView joystickButtonRelease: event];
            break;
        case GG_JOYSTICKBUTTONDOWN:
            [hitView joystickButtonPress: event];
            break;
        case GG_JOYSTICKHAT:
            [hitView joystickHat: event];
            break;
        case GG_SCROLLWHEEL:
            [hitView scrollWheel:event];
            break;
        default:
            return NO;
    }
    return YES;
    
}

- (void) setFirstOpaque: (GGNSWindow *) aWin
{
    if( aWin == nil && [self isOpaque] )
        aWin = self;
    firstOpaque = aWin;
    [windowBelow setFirstOpaque: aWin];
}

- (void) setWinOnTheTop: (GGNSWindow *) aWin
{
    windowOnTop = aWin;
    [windowBelow setWinOnTheTop: aWin];
}

- (GGNSWindow *) windowOnTop
{
    return windowOnTop;
}

- (BOOL) isOpaque
{
    return couleur.a >= 1;
}

- (GGNSWindow *) getFirstOpaque
{
    if( firstOpaque )
        return firstOpaque;
    else if( [self isOpaque] )
        return self;
    else
        return nil;
}

- (void) pushOnTheTop: (GGNSWindow *)aWin
{
    [windowOnTop pushWindow: aWin];
}

- (void) pushWindow: (GGNSWindow *) aWin
{
    if( aWin == windowAbove )
        return;
    if( windowAbove != nil )
    {
        [self setFirstOpaque: nil];
        [self setWinOnTheTop: self];
    }
    
    ASSIGN(windowAbove, aWin);
    [aWin setWindowBelow: self];
    
    if( aWin == nil )
        return;
    
    [self setFirstOpaque: [aWin getFirstOpaque]];
    [self setWinOnTheTop: [aWin windowOnTop]];
}

- (GGNSWindow *) popWindow
{
    GGNSWindow *retVal = nil;
    ASSIGN(retVal, windowAbove);
    DESTROY(windowAbove);
    
    [self setFirstOpaque: nil];
    [self setWinOnTheTop: self];
    
    [retVal setWindowBelow: nil];
    NSDebugMLLog(@"poping", @"poping from %@ value %@", self, retVal);
    
    return AUTORELEASE(retVal);
}

- (void) grabEvent: (GGView *) aView
{
    //    [self makeFirstResponder: aView];
    //    grabeur = aView;
}

- (void) unGrabEvent
{
    //    grabeur = nil;
}

- (void) setWindowBelow: (GGNSWindow *)win
{
    windowBelow = win;
}

- (GGNSWindow *) windowBelow
{
    return windowBelow;
}

- (void) takeFocus
{
}

- (void) looseFocus
{
}

- (void) addTopView: (TopView *) aView
{
    if( _currentView != nil )
        [[self contentView]
      removeSubview: _currentView];
    else
    {
        [self looseFocus];
    }
    [_stackView addObject: aView];
    ASSIGN(_currentView, aView);
    [[self contentView]
    addSubview: aView];
}

- (void) aTopViewIsClosing: (TopView *) aView
{
    if( aView == _currentView )
    {
        [_stackView removeLastObject];
        if( [_stackView count] >= 1 )
        {
            TopView *temp;
            temp = [_stackView lastObject];
            ASSIGN(_currentView, temp);
            [[self contentView]
	    addSubview: temp];
            NSDebugMLLog(@"GGInput", @"someone in the stack");
        }
        else
        {
            DESTROY(_currentView);
            [self takeFocus];
            NSDebugMLLog(@"GGInput", @"noone in the stack");
        }
    }
    else
        NSWarnMLog(@"bizarre");
}

- (void) popAllView
{
    while( _currentView != nil )
        [_currentView close];
}


@end
