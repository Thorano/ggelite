/*
 *  Console.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: April 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <math.h>
#import <Foundation/NSObjCRuntime.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSException.h>
#import <Foundation/NSData.h>
#import <Foundation/NSDebug.h>
#import "config.h"
#import "Metaobj.h"
#import "Console.h"
#import "GGLoop.h"
#import "World.h"
#import "GGRepere.h"
#import "GGFont.h"
#import "GGInput.h"
#import "GGGlyphLayout.h"
#import "Tableau.h"
#import "utile.h"


//static Console *theConsole;
/*
NSLog_printf_handler *oldHandler;

void myLog(NSString *str)
{
  Console *theConsole = [theGGInput console];
  (*oldHandler)(str);
  [theConsole logString: str];
}
*/

void consoleLogv (NSString* format, va_list args)
{
  Console *theConsole = [theGGInput console];
  [theConsole logString: 
		[NSString stringWithFormat: format arguments: args]];
  if( wizard )
    NSLogv(format, args);
}

void 
consoleLog (NSString* format, ...)
{
  va_list ap;

  va_start (ap, format);
  consoleLogv (format, ap);
  va_end (ap);
}

void systemLog(NSString* format, ...)
{
  va_list ap;

  va_start (ap, format);
  consoleLogv (format, ap);
  va_end (ap);
}


@implementation Console
+ (void) initialize
{
  if( self == [Console class] )
    {
//        oldHandler = _NSLog_printf_handler;

//        _NSLog_printf_handler = myLog;
    }
}

#define fbeg 0x670
#define fend 0x6b7
- (id) initWithFrame: (NSRect)frameRect
{
  unichar data2 [fend-fbeg];
  int i;
  NSMutableString *str;
  [super initWithFrame: frameRect];

  for( i = 0; i < sizeof(data2) / sizeof(unichar); ++i )
    data2[i] = fbeg+i;

  str = [NSMutableString  stringWithCharacters: data2
			  length: sizeof(data2)/sizeof(unichar)];

  layout = RETAIN([GGGlyphLayout glyphLayoutWithString: @""
                                              withFont: [GGFont fontWithFile:Fixed7x14]

//				 withFont: [GGFont fontWithFile:@"cu12.pcf"]
		   ]);
  [layout setMaxWidth: 0.9 * frameRect.size.width];
  [layout setAlignement: TopAlignement];
  return self;
}

- (void) setFont: (GGFont *)font
{
  GGGlyphLayout *l;
  l = [GGGlyphLayout glyphLayoutWithString: @""
		     withFont: font];
  ASSIGN(layout,l);
}

- (void) dealloc
{
  RELEASE(layout);
  //  NSWarnMLog(@"fuck!!!");
  [super dealloc];
}

- (void) logString: (NSString *)str
{
  [layout appendString: str];
}


- (void) log: (NSString *)format, ...
{
  va_list ap;
  NSString *str;

  va_start (ap, format);
  str = [[NSString alloc] initWithFormat: format
		  arguments: ap];
  [self logString: str];
  [str release];
  va_end (ap);
}

- (void) clear
{
  [layout clear];
  [layout setAlignement: TopAlignement];
}

- (void) drawRect: (NSRect) theRect
{
  NSRect aRect = [self bounds];

  glColor3f(1, 1, 1);
  if( NSHeight([layout frameRect]) >  NSHeight(aRect) - 5 )
    {
      [layout setAlignement: BottomAlignement];
      [layout drawAtPoint: NSMakePoint(NSMinX(aRect)+2, 0)];
    }
  else
    {
      NSPoint point = NSMakePoint(NSMinX(aRect)+2, NSMaxY(aRect)-5);
      [layout drawAtPoint: point];
      NSDebugMLLogWindow(@"Console", @"pt = %@", NSStringFromPoint(point));
    }
  GLCHECK;

}

- (void) sizeToFit
{
  NSRect theRect;
  if( layout )
    {
      theRect = [layout frameRect];
      [self setFrameSize: NSMakeSize(NSWidth(theRect) + 10,
				     NSHeight(theRect) + 10)];
    }
}
@end
