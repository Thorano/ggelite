/* 	-*-ObjC-*- */
/*
   GGNSAffineTransform.h

   Copyright (C) 1996 Free Software Foundation, Inc.

   Author: Ovidiu Predescu <ovidiu@net-community.com>
   Date: August 1997
   Rewrite for macOS-X compatibility: Richard Frith-Macdonald, 1999
   
   This file is part of the GNUstep GUI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.
   
   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/

#ifndef _GNUstep_H_NSAffineTransform
#define _GNUstep_H_NSAffineTransform

#import <Foundation/NSObject.h>
#import <Foundation/NSGeometry.h>

#import "config.h"

@class NSBezierPath;

typedef	struct {
  float	m11;
  float	m12;
  float	m21;
  float	m22;
  float	tx;
  float	ty;
} GGAffineTransformStruct;


@interface GGNSAffineTransform : NSObject <NSCopying>
{
@public
  GGAffineTransformStruct	matrix;
  float rotationAngle;
}

+ (GGNSAffineTransform*) transform;
- (void) appendTransform: (GGNSAffineTransform*)aTransform;

- (id) initWithTransform: (GGNSAffineTransform*)aTransform;
- (void) invert;
- (void) prependTransform: (GGNSAffineTransform*)aTransform;
- (void) rotateByDegrees: (float)angle;
- (void) rotateByRadians: (float)angle;
- (void) scaleBy: (float)scale;
- (void) scaleXBy: (float)scaleX yBy: (float)scaleY;

- (void) setTransformStruct: (GGAffineTransformStruct)val;

- (NSPoint) transformPoint: (NSPoint)aPoint;
- (NSSize) transformSize: (NSSize)aSize;
- (GGAffineTransformStruct) transformStruct;
- (void) translateXBy: (float)tranX yBy: (float)tranY;

#ifndef	NO_GNUSTEP
+ (GGNSAffineTransform*) matrixFrom: (const float[6])matrix;
- (void) translateToPoint: (NSPoint)point;
- (void) rotateByAngle: (float)angle;
- (void) scaleBy: (float)sx : (float)sy;
- (void) scaleTo: (float)sx : (float)sy;
- (void) makeIdentityMatrix;
- (float) rotationAngle;
- (void) setFrameOrigin: (NSPoint)point;
- (void) setFrameRotation: (float)angle;
- (void) inverse;

- (BOOL) isRotated;

- (void) boundingRectFor: (NSRect)rect result: (NSRect*)result;

/* Returns anotherMatrix * self */
- (void) concatenateWith: (GGNSAffineTransform*)anotherMatrix;

- (NSPoint) pointInMatrixSpace: (NSPoint)point;
- (NSSize) sizeInMatrixSpace: (NSSize)size;
- (NSRect) rectInMatrixSpace: (NSRect)rect;

- (void) setMatrix: (const float[6])replace;
- (void) getMatrix: (float[6])replace;

- (void) takeMatrixFromTransform: (GGNSAffineTransform *)other;

@end
#endif

#endif /* _GNUstep_H_NSAffineTransform */
