/* 	-*-ObjC-*- */
/*
   GGNSView.h

   <abstract>Encapsulates all drawing functionality</abstract>

   Copyright <copy>(C) 1996 Free Software Foundation, Inc.</copy>

   Author:  Scott Christley <scottc@net-community.com>
   Date: 1996
   Heavily changed and extended by Ovidiu Predescu <ovidiu@net-community.com>.
   Date: 1997
   Author:  Felipe A. Rodriguez <far@ix.netcom.com>
   Date: August 1998

   This file is part of the GNUstep GUI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; see the file COPYING.LIB.
   If not, write to the Free Software Foundation,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef _GNUstep_H_NSView
#define _GNUstep_H_NSView


#import "NSResponder.h"
#import "GGLoop.h"

@class NSString;
@class NSArray;
@class NSMutableArray;
@class NSData;

@class GGNSWindow;
@class NSPasteboard;
@class GGView;
@class NSClipView;
@class NSImage;
@class NSCursor;

typedef enum _GGNSWindowOrderingMode
{
  GGNSWindowAbove,
  GGNSWindowBelow,
  GGNSWindowOut

} GGNSWindowOrderingMode;


typedef int NSTrackingRectTag;
typedef int NSToolTipTag;

/*
 * constants representing the four types of borders that
 * can appear around an GGView
 */

/*
typedef enum _GGNSBorderType {
  NSNoBorder,
  NSLineBorder,
  NSBezelBorder,
  NSGrooveBorder
} GGNSBorderType;
*/

/*
 * autoresize constants which GGView uses in
 * determining the parts of a view which are
 * resized when the view's superview is resized
 */
enum {
  GGNSViewNotSizable	= 0,	// view does not resize with its superview
  GGNSViewMinXMargin	= 1,	// left margin between views can stretch
  GGNSViewWidthSizable	= 2,	// view's width can stretch
  GGNSViewMaxXMargin	= 4,	// right margin between views can stretch
  GGNSViewMinYMargin	= 8,	// bottom margin between views can stretch
  GGNSViewHeightSizable	= 16,	// view's height can stretch
  GGNSViewMaxYMargin	= 32 	// top margin between views can stretch
};


@interface GGView : GGResponder
{
  NSRect _frame;
  NSRect _bounds;
  id _frameMatrix;
  id _boundsMatrix;
  id _matrixToWindow;
  id _matrixFromWindow;

  GGView* _super_view;
  NSMutableArray *_sub_views;
  id _window;
  NSMutableArray *_tracking_rects;
  NSMutableArray *_cursor_rects;
  NSRect _invalidRect;
  NSRect _visibleRect;
  unsigned int _autoresizingMask;


  BOOL _is_rotated_from_base;
  BOOL _is_rotated_or_scaled_from_base;
  BOOL _post_frame_changes;
  BOOL _post_bounds_changes;
  BOOL _autoresizes_subviews;
  BOOL _coordinates_valid;

  GGView *_nextKeyView;
  GGView *_previousKeyView;
}

/*
 * Initializing GGView Objects
 */
- (id) initWithFrame: (NSRect)frameRect;

/*
 * Managing the GGView Hierarchy
 */
- (void) addSubview: (GGView*)aView;
- (void) addSubview: (GGView*)aView
	 positioned: (GGNSWindowOrderingMode)place
	 relativeTo: (GGView*)otherView;
- (GGView*) ancestorSharedWithView: (GGView*)aView;
- (BOOL) isDescendantOf: (GGView*)aView;
- (GGView*) opaqueAncestor;
- (void) removeFromSuperviewWithoutNeedingDisplay;
- (void) removeFromSuperview;
#ifndef	NO_GNUSTEP
- (void) removeSubview: (GGView*)aView;
#endif
- (void) replaceSubview: (GGView*)oldView
		   with: (GGView*)newView;
- (void) sortSubviewsUsingFunction: (int (*)(id ,id ,void*))compare
			   context: (void*)context;
- (NSArray*) subviews;
- (GGView*) superview;
- (GGNSWindow*) window;
- (void) viewWillMoveToSuperview: (GGView*)newSuper;
- (void) viewWillMoveToWindow: (GGNSWindow*)newWindow;
#ifndef STRICT_OPENSTEP
- (void) didAddSubview: (GGView *)subview;
- (void) viewDidMoveToSuperview;
- (void) viewDidMoveToWindow;
- (void) willRemoveSubview: (GGView *)subview;
#endif

/*
 * Assigning a Tag
 */
- (int) tag;
- (id) viewWithTag: (int)aTag;

/*
 * Modifying the Frame Rectangle
 */
- (float) frameRotation;
- (NSRect) frame;
- (void) setFrame: (NSRect)frameRect;
- (void) setFrameOrigin: (NSPoint)newOrigin;
- (void) setFrameRotation: (float)angle;
- (void) setFrameSize: (NSSize)newSize;

/*
 * Modifying the Coordinate System
 */
- (float) boundsRotation;
- (NSRect) bounds;
- (void) setBounds: (NSRect)aRect;
- (void) setBoundsOrigin: (NSPoint)newOrigin;
- (void) setBoundsRotation: (float)angle;
- (void) setBoundsSize: (NSSize)newSize;

- (void) translateOriginToPoint: (NSPoint)point;
- (void) scaleUnitSquareToSize: (NSSize)newSize;
- (void) rotateByAngle: (float)angle;

- (BOOL) isFlipped;
- (BOOL) isRotatedFromBase;
- (BOOL) isRotatedOrScaledFromBase;

/*
 * Converting Coordinates
 */
- (NSRect) centerScanRect: (NSRect)aRect;
- (NSPoint) convertPoint: (NSPoint)aPoint
		fromView: (GGView*)aView;
- (NSPoint) convertPoint: (NSPoint)aPoint
		  toView: (GGView*)aView;
- (NSRect) convertRect: (NSRect)aRect
	      fromView: (GGView*)aView;
- (NSRect) convertRect: (NSRect)aRect
		toView: (GGView*)aView;
- (NSSize) convertSize: (NSSize)aSize
	      fromView: (GGView*)aView;
- (NSSize) convertSize: (NSSize)aSize
		toView: (GGView*)aView;

/*
 * Notifying Ancestor Views
 */
- (void) setPostsFrameChangedNotifications: (BOOL)flag;
- (BOOL) postsFrameChangedNotifications;
- (void) setPostsBoundsChangedNotifications: (BOOL)flag;
- (BOOL) postsBoundsChangedNotifications;

/*
 * Resizing Subviews
 */
- (void) resizeSubviewsWithOldSize: (NSSize)oldSize;
- (void) setAutoresizesSubviews: (BOOL)flag;
- (BOOL) autoresizesSubviews;
- (void) setAutoresizingMask: (unsigned int)mask;
- (unsigned int) autoresizingMask;
- (void) resizeWithOldSuperviewSize: (NSSize)oldSize;

/*
 * Focusing
 */
- (void) lockFocus;
- (void) unlockFocus;
#ifndef STRICT_OPENSTEP
- (BOOL) lockFocusIfCanDraw;
#endif

/*
 * Displaying
 */
- (void) display;
- (void) displayIfNeeded;
- (void) displayIfNeededIgnoringOpacity;
- (void) displayIfNeededInRect: (NSRect)aRect;
- (void) displayIfNeededInRectIgnoringOpacity: (NSRect)aRect;
- (void) displayRect: (NSRect)aRect;
- (void) displayRectIgnoringOpacity: (NSRect)aRect;
- (BOOL) needsDisplay;
- (void) setNeedsDisplay: (BOOL)flag;
- (void) setNeedsDisplayInRect: (NSRect)invalidRect;
- (BOOL) isOpaque;

- (void) drawRect: (NSRect)rect;
- (NSRect) visibleRect;
- (BOOL) canDraw;
- (BOOL) shouldDrawColor;

/*
 * Assigning a Tag
 */
- (int) tag;
- (id) viewWithTag: (int)aTag;

/*
 * Aiding Event Handling
 */
- (BOOL) acceptsFirstMouse: (Event*)theEvent;
- (GGView*) hitTest: (NSPoint)aPoint;
- (BOOL) mouse: (NSPoint)aPoint
	inRect: (NSRect)aRect;
- (BOOL) performKeyEquivalent: (Event*)theEvent;
- (BOOL) shouldDelayWindowOrderingForEvent: (Event*)anEvent;
- (void) setNextKeyView: (GGView*)aView;
- (GGView*) nextKeyView;
- (GGView*) nextValidKeyView;
- (void) setPreviousKeyView: (GGView*)aView;
- (GGView*) previousKeyView;
- (GGView*) previousValidKeyView;

/*
 * Printing
 */
- (NSData*) dataWithEPSInsideRect: (NSRect)aRect;
- (void) fax: (id)sender;
- (void) print: (id)sender;
- (void) writeEPSInsideRect: (NSRect)rect
	       toPasteboard: (NSPasteboard*)pasteboard;

/*
 * Pagination
 */
- (void) adjustPageHeightNew: (float*)newBottom
			 top: (float)oldTop
		      bottom: (float)oldBottom
		       limit: (float)bottomLimit;
- (void) adjustPageWidthNew: (float*)newRight
		       left: (float)oldLeft
		      right: (float)oldRight
		      limit: (float)rightLimit;
- (float) heightAdjustLimit;
- (BOOL) knowsPagesFirst: (int*)firstPageNum
		    last: (int*)lastPageNum;
- (NSPoint) locationOfPrintRect: (NSRect)aRect;
- (NSRect) rectForPage: (int)page;
- (float) widthAdjustLimit;

/*
 * Writing Conforming PostScript
 */
- (void) addToPageSetup;
- (void) beginPage: (int)ordinalNum
	     label: (NSString*)aString
	      bBox: (NSRect)pageRect
	     fonts: (NSString*)fontNames;
- (void) beginPageSetupRect: (NSRect)aRect
		  placement: (NSPoint)location;
- (void) beginPrologueBBox: (NSRect)boundingBox
	      creationDate: (NSString*)dateCreated
		 createdBy: (NSString*)anApplication
		     fonts: (NSString*)fontNames
		   forWhom: (NSString*)user
		     pages: (int)numPages
		     title: (NSString*)aTitle;
- (void) beginSetup;
- (void) beginTrailer;
- (void) drawPageBorderWithSize: (NSSize)borderSize;
- (void) drawSheetBorderWithSize: (NSSize)borderSize;
- (void) endHeaderComments;
- (void) endPrologue;
- (void) endSetup;
- (void) endPageSetup;
- (void) endPage;
- (void) endTrailer;

/*
 * NSCoding protocol
 */
- (void) encodeWithCoder: (NSCoder*)aCoder;
- (id) initWithCoder: (NSCoder*)aDecoder;

@end


@class GGNSAffineTransform;

/*
 * GNUstep extensions
 * Methods whose names begin with an underscore must NOT be overridden.
 */
#ifndef	NO_GNUSTEP
@interface GGView (PrivateMethods)

/*
 * The [-_invalidateCoordinates] method marks the cached visible rectangles
 * of the view and it's subview as being invalid.  GGNSViews methods call this
 * whenever the coordinate system of the view is changed in any way - thus
 * forcing recalculation of cached values next time they are needed.
 */
- (void) _invalidateCoordinates;
- (void) _rebuildCoordinates;

- (GGNSAffineTransform*) _matrixToWindow;
- (GGNSAffineTransform*) _matrixFromWindow;
@end
#endif

/*
 * GNUstep specific function to determine the drag types registered for a view.
 */
extern NSArray *GSGetDragTypes(GGView* aView);

/* Notifications */
extern NSString *GGNSViewFrameDidChangeNotification;
extern NSString *GGNSViewBoundsDidChangeNotification;
extern NSString *GGNSViewFocusDidChangeNotification;

#endif // _GNUstep_H_NSView

