/* 	-*-ObjC-*- */
/*
 *  GGWidget.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGWidget_h
#define __GGWidget_h

#import <Foundation/NSGeometry.h>
#import "NSView.h"

@class Texture;
@class GGGlyphLayout;
@class GGFont;
@class GGIcon;

typedef enum _GGButtonType {
  GGMomentaryPushButton,
  //  NSPushOnPushOffButton,
  GGToggleButton,
  /*  NSSwitchButton,
  NSRadioButton,
  NSMomentaryChangeButton,
  NSOnOffButton,
  NSMomentaryLight*/
} GGButtonType;

@interface GGControl : GGView
{
  int tag;
}
- (void) setTag: (int) _tag;
@end


@interface GGBouton : GGControl
{
  id			target;
  SEL			action;
  GGButtonType		type;
  int			state;
  NSString		*str;
  GGGlyphLayout		*strLayout;
  GGIcon		*icon;
  NSPoint		textOrigin;
  NSPoint		iconOrigin;
  BOOL			border;
  BOOL			highlighted;
  BOOL			enable;
}
+ button;
- (void) setTitle: (NSString *) theStr;
- (void) setTitle: (NSString *) theStr
	 withFont: (GGFont *) f;
- (void) setTarget: obj;
- (void) setBorder: (BOOL) val;
- (void) setAction: (SEL) aSel;
- (void) setType: (GGButtonType) theType;
- (void) setState: (int) state;
- (int) state;
- (void) setButtonType: (GGButtonType)aType;
- (void) setIcon: (GGIcon *) _icon;
- (void) setEnable: (BOOL) f;
- (BOOL) isEnable;
@end

@interface GGString : GGView
{
  NSMutableString	*str;
  GGGlyphLayout		*strLayout;
  BOOL			isEditable;
  id			delegate;
}
- (void) updateString: (NSString *)Astr;
- (void) setString: (NSString *)aStr;
- (void) setString: (NSString *) aStr
	  withFont: (GGFont *) f;
- (void) setEditable: (BOOL) val;
- (void) setDelegate: (id) _delegate;
- (NSString *) value;
@end

@interface GGOption : GGControl
{
  GGBouton 		*bo;
  //  GGString		*str;
  GGGlyphLayout		*strLayout;
}
+ optionWithStr: (NSString *) theStr
	  width: (int) w
	   font: (GGFont *)font;
+ optionWithStr: (NSString *) theStr
	   font: (GGFont *)font;
+ optionWithStr: (NSString *) str;
+ optionWithStr: (NSString *) str
	  width: (int) w;
+ optionWithFormat: (NSString *)str,...;
- (void) setType: (GGButtonType) aType;
- (void) setTarget: obj;
- (void) setAction: (SEL) aSel;
- (void) setState: (int) state;
- (void) setButtonType: (GGButtonType)aType;
@end

extern float GGBorderColor[3];
#endif
