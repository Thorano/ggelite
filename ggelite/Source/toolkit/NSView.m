/** <title>GGView</title>

   <abstract>The view class which encapsulates all drawing functionality</abstract>

   Copyright (C) 1996 Free Software Foundation, Inc.

   Author: Scott Christley <scottc@net-community.com>
   Date: 1996
   Author: Ovidiu Predescu <ovidiu@net-community.com>
   Date: 1997
   Author: Felipe A. Rodriguez <far@ix.netcom.com>
   Date: August 1998
   Author: Richard Frith-Macdonald <richard@brainstorm.co.uk>
   Date: January 1999

   This file is part of the GNUstep GUI Library.

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; see the file COPYING.LIB.
   If not, write to the Free Software Foundation,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#import <math.h>
#import <float.h>

#import <Foundation/NSString.h>
#import <Foundation/NSCalendarDate.h>
#import <Foundation/NSCoder.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSThread.h>
#import <Foundation/NSLock.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSValue.h>
#import <Foundation/NSData.h>
#import <Foundation/NSDebug.h>

#import "NSView.h"
#import "NSWindow.h"
#import "NSAffineTransform.h"
#import "Metaobj.h"
#import "utile.h"

@implementation GGView

/*
 *  Class variables
 */
static Class	viewClass;

static GGNSAffineTransform	*flip = nil;

static NSNotificationCenter *nc = nil;

static SEL	appSel;
static SEL	invalidateSel;

static void	(*appImp)(GGNSAffineTransform*, SEL, GGNSAffineTransform*);
static void	(*invalidateImp)(GGView*, SEL);

/*
 *	Stuff to maintain a map table so we know what views are
 *	registered for drag and drop - we don't store the info in
 *	the view directly 'cot it would take up a pointer in each
 *	view and the vast majority of views wouldn't use it.
 *	Types are not registered/unregistered often enough for the
 *	performance of this mechanism to be an issue.
 */
static NSMapTable	*typesMap = 0;
static NSLock		*typesLock = nil;

/*
 * This is the only external interface to the drag types info.
 
NSArray*
GSGetDragTypes(GGView *obj)
{
  NSArray	*t;

  [typesLock lock];
  t = (NSArray*)NSMapGet(typesMap, (void*)(gsaddr)obj);
  [typesLock unlock];
  return t;
}
*/


/*
 * Class methods
 */
+ (void) initialize
{
  if (self == [GGView class])
    {
      Class	matrixClass = [GGNSAffineTransform class];
      GGAffineTransformStruct	ats = { 1, 0, 0, -1, 0, 1 };

      typesMap = NSCreateMapTable(NSNonOwnedPointerMapKeyCallBacks,
                NSObjectMapValueCallBacks, 0);
      typesLock = [NSLock new];

      appSel = @selector(appendTransform:);
      invalidateSel = @selector(_invalidateCoordinates);

      appImp = (void (*)(GGNSAffineTransform*, SEL, GGNSAffineTransform*))
		[matrixClass instanceMethodForSelector: appSel];

      invalidateImp = (void (*)(GGView*, SEL))
		[self instanceMethodForSelector: invalidateSel];

      flip = [matrixClass new];
      [flip setTransformStruct: ats];

      nc = [NSNotificationCenter defaultCenter];

      viewClass = [GGView class];
      NSDebugLLog(@"GGView", @"Initialize GGView class\n");
      [self setVersion: 1];
    }
}


/*
 * Instance methods
 */
- (id) init
{
  return [self initWithFrame: NSZeroRect];
}

- (id) initWithFrame: (NSRect)frameRect
{
  [super init];

  if (frameRect.size.width < 0)
    {
      NSWarnMLog(@"given negative width", 0);
      frameRect.size.width = 0;
    }
  if (frameRect.size.height < 0)
    {
      NSWarnMLog(@"given negative height", 0);
      frameRect.size.height = 0;
    }
  _frame = frameRect;			// Set frame rectangle
  _bounds.origin = NSZeroPoint;		// Set bounds rectangle
  _bounds.size = _frame.size;

  _frameMatrix = [GGNSAffineTransform new];	// Map fromsuperview to frame
  _boundsMatrix = [GGNSAffineTransform new];	// Map fromsuperview to bounds
  _matrixToWindow = [GGNSAffineTransform new];	// Map to window coordinates
  _matrixFromWindow = [GGNSAffineTransform new];	// Map from window coordinates
  [_frameMatrix setFrameOrigin: _frame.origin];

  _sub_views = [NSMutableArray new];
  _tracking_rects = [NSMutableArray new];
  _cursor_rects = [NSMutableArray new];

  _super_view = nil;
  _window = nil;
  _is_rotated_from_base = NO;
  _is_rotated_or_scaled_from_base = NO;
  _rFlags.needs_display = YES;
  _post_frame_changes = NO;
  _autoresizes_subviews = YES;
  _autoresizingMask = GGNSViewNotSizable;
  _coordinates_valid = NO;
  _nextKeyView = nil;
  _previousKeyView = nil;

  _rFlags.flipped_view = [self isFlipped];

  addToCheck(self);

  return self;
}

- (void) dealloc
{
  if (_nextKeyView)
    [_nextKeyView setPreviousKeyView: nil];

  if (_previousKeyView)
     [_previousKeyView setNextKeyView: nil];

  RELEASE(_matrixToWindow);
  RELEASE(_matrixFromWindow);
  RELEASE(_frameMatrix);
  RELEASE(_boundsMatrix);
  TEST_RELEASE(_sub_views);
  TEST_RELEASE(_tracking_rects);
  TEST_RELEASE(_cursor_rects);

  [super dealloc];
}

/** Adds <var>aView</var> as a subview of the receiver. */
- (void) addSubview: (GGView*)aView
{
  if (aView == nil)
    {
      [NSException raise: NSInvalidArgumentException
		  format: @"Adding a nil subview"];
    }
  if ([self isDescendantOf: aView])
    {
      [NSException raise: NSInvalidArgumentException
		format: @"addSubview: creates a loop in the views tree!\n"];
    }

  RETAIN(aView);
  [aView removeFromSuperview];
  if (aView->_coordinates_valid)
    {
      (*invalidateImp)(aView, invalidateSel);
    }
  [aView viewWillMoveToWindow: _window];
  [aView viewWillMoveToSuperview: self];
  [aView setNextResponder: self];
  [_sub_views addObject: aView];
  _rFlags.has_subviews = 1;
  [aView setNeedsDisplay: YES];
  [aView viewDidMoveToWindow];
  [aView viewDidMoveToSuperview];
  [self didAddSubview: aView];
  RELEASE(aView);
}

- (void) addSubview: (GGView*)aView
	 positioned: (GGNSWindowOrderingMode)place
	 relativeTo: (GGView*)otherView
{
  unsigned	index;

  if (aView == nil)
    {
      [NSException raise: NSInvalidArgumentException
		  format: @"Adding a nil subview"];
    }
  if ([self isDescendantOf: aView])
    {
      [NSException raise: NSInvalidArgumentException
		  format: @"addSubview: positioned: relativeTo: creates a loop in the views tree!\n"];
    }

  if (aView == otherView)
    return;

  index = [_sub_views indexOfObjectIdenticalTo: otherView];
  if (index == NSNotFound)
    {
      if (place == GGNSWindowBelow)
	index = 0;
      else
	index = [_sub_views count];
    }
  RETAIN(aView);
  [aView removeFromSuperview];
  if (aView->_coordinates_valid)
    {
      (*invalidateImp)(aView, invalidateSel);
    }
  [aView viewWillMoveToWindow: _window];
  [aView viewWillMoveToSuperview: self];
  [aView setNextResponder: self];
  if (place == GGNSWindowBelow)
    [_sub_views insertObject: aView atIndex: index];
  else
    [_sub_views insertObject: aView atIndex: index+1];
  _rFlags.has_subviews = 1;
  [aView setNeedsDisplay: YES];
  [aView viewDidMoveToWindow];
  [aView viewDidMoveToSuperview];
  [self didAddSubview: aView];
  RELEASE(aView);
}

/**
  Returns <code>self</code> if <var>aView</var> is the receiver or
  <var>aView</var> is a subview of the receiver, the ancestor view
  shared by <var>aView</var> and the receiver, if any,
  <var>aView</var> if it is an ancestor of the receiver, otherwise
  returns <code>nil</code>.  */
- (GGView*) ancestorSharedWithView: (GGView*)aView
{
  if (self == aView)
    return self;

  if ([self isDescendantOf: aView])
    return aView;

  if ([aView isDescendantOf: self])
    return self;

  /*
   * If neither are descendants of each other and either does not have a
   * superview then they cannot have a common ancestor
   */
  if (!_super_view)
    return nil;

  if (![aView superview])
    return nil;

  /* Find the common ancestor of superviews */
  return [_super_view ancestorSharedWithView: [aView superview]];
}

/**
  Returns <code>YES</code> if <var>aView</var> is an ancestor of the receiver.
*/
- (BOOL) isDescendantOf: (GGView*)aView
{
  if (aView == self)
    return YES;

  if (!_super_view)
    return NO;

  if (_super_view == aView)
    return YES;

  return [_super_view isDescendantOf: aView];
}

- (GGView*) opaqueAncestor
{
  GGView	*next = _super_view;
  GGView	*current = self;

  while (next != nil)
    {
      if ([current isOpaque] == YES)
	{
	  break;
	}
      current = next;
      next = current->_super_view;
    }
  return current;
}

/**
  Removes the receiver from its superviews list of subviews, by
  invoking the superviews [-removeSubview:] method.  */
- (void) removeFromSuperviewWithoutNeedingDisplay
{
  if (_super_view != nil)
    {
      [_super_view removeSubview: self];
    }
}

/**
  <p> Removes the receiver from its superviews list of subviews, by
  invoking the superviews [-removeSubview:] method, and marks the
  rectangle that the reciever occupied in the superview as needing
  redisplay.  </p>

  <p> This is dangerous to use during display, since it alters the
  rectangles needing display.  </p> */
- (void) removeFromSuperview
{
  if (_super_view != nil)
    {
      [_super_view setNeedsDisplayInRect: _frame];
      [_super_view removeSubview: self];
    }
}

/**
  <p> Removes the view from the receivers list of subviews and from
  the responder chain.  </p>

  <p> Also invokes [aView -viewWillMoveToWindow: nil] to handle
  removal of aView (and recursively, its children) from its window -
  performing tidyup by invalidating cursor rects etc.  </p> 
  <standards><NotMacOS-X/><NotOpenStep/></standards>
*/
- (void) removeSubview: (GGView*)aSubview
{
  id view;
  /*
   * This must be first because it invokes -resignFirstResponder:, 
   * which assumes the view is still in the view hierarchy
   */
  for (view = [_window firstResponder];
       view != nil && [view respondsToSelector:@selector(superview)];
       view = [view superview] )
    {
      if (view == aSubview)
	{      
	  [_window makeFirstResponder: _window];
	  break;
	}
    }
  [self willRemoveSubview: aSubview];
  aSubview->_super_view = nil;
  [aSubview viewWillMoveToWindow: nil];
  [aSubview setNextResponder: nil];
  RETAIN(aSubview);
  [_sub_views removeObjectIdenticalTo: aSubview];
  [aSubview viewDidMoveToWindow];
  [aSubview viewDidMoveToSuperview];
  RELEASE(aSubview);
  if ([_sub_views count] == 0)
    {
      _rFlags.has_subviews = 0;
    }
}

/**
  Removes <var>oldView</var> from the receiver and places
  <var>newView</var> in its place.  */
- (void) replaceSubview: (GGView*)oldView with: (GGView*)newView
{
  if (newView == oldView)
    {
      return;
    }
  /*
   * NB. we implement the replacement in full rather than calling addSubview:
   * since classes like NSBox override these methods but expect to be able to
   * call [super replaceSubview:with:] safely.
   */
  if (oldView == nil)
    {
      /*
       * Strictly speaking, the docs say that if 'oldView' is not a subview
       * of the receiver then we do nothing - but here we add newView anyway.
       * So a replacement with no oldView is an addition.
       */
      RETAIN(newView);
      [newView removeFromSuperview];
      if (newView->_coordinates_valid)
	{
	  (*invalidateImp)(newView, invalidateSel);
	}
      [newView viewWillMoveToWindow: _window];
      [newView viewWillMoveToSuperview: self];
      [newView setNextResponder: self];
      [_sub_views addObject: newView];
      _rFlags.has_subviews = 1;
      [newView setNeedsDisplay: YES];
      [newView viewDidMoveToWindow];
      [newView viewDidMoveToSuperview];
      [self didAddSubview: newView];
      RELEASE(newView);
    }
  else if ([_sub_views indexOfObjectIdenticalTo: oldView] != NSNotFound)
    {
      if (newView == nil)
	{
	  /*
	   * If there is no new view to add - we just remove the old one.
	   * So a replacement with no newView is a removal.
	   */
	  [oldView removeFromSuperview];
	}
      else
	{
	  unsigned index;

	  /*
	   * Ok - the standard case - we remove the newView from wherever it
	   * was (which may have been in this view), locate the position of
	   * the oldView (which may have changed due to the removal of the
	   * newView), remove the oldView, and insert the newView in it's
	   * place.
	   */
	  RETAIN(newView);
	  [newView removeFromSuperview];
	  if (newView->_coordinates_valid)
	    {
	      (*invalidateImp)(newView, invalidateSel);
	    }
	  index = [_sub_views indexOfObjectIdenticalTo: oldView];
	  [oldView removeFromSuperview];
	  [newView viewWillMoveToWindow: _window];
	  [newView viewWillMoveToSuperview: self];
	  [newView setNextResponder: self];
	  [_sub_views addObject: newView];
	  _rFlags.has_subviews = 1;
	  [newView setNeedsDisplay: YES];
	  [newView viewDidMoveToWindow];
	  [newView viewDidMoveToSuperview];
	  [self didAddSubview: newView];
	  RELEASE(newView);
	}
    }
}

- (void) sortSubviewsUsingFunction: (int (*)(id ,id ,void*))compare
			   context: (void*)context
{
  [_sub_views sortUsingFunction: compare context: context];
}

/**
  Notifies the receiver that its superview is being changed to
  <var>newSuperview</var>.  */
- (void) viewWillMoveToSuperview: (GGView*)newSuper
{
  _super_view = newSuper;
}

/**
  Notifies the receiver that it will now be a view of <var>newWindow</var>.
  Note, this method is also used when removing a view from a window
  (in which case, <var>newWindow</var> is nil) to let all the subviews know
  that they have also been removed from the window.
 */
- (void) viewWillMoveToWindow: (GGNSWindow*)newWindow
{
  if (newWindow == _window)
    {
      return;
    }
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }

  _window = newWindow;

  if (_rFlags.has_subviews)
    {
      unsigned	count = [_sub_views count];

      if (count > 0)
	{
	  unsigned	i;
	  GGView	*array[count];

	  [_sub_views getObjects: array];
	  for (i = 0; i < count; ++i)
	    {
	      [array[i] viewWillMoveToWindow: newWindow];
	    }
	}
    }
}

- (void) didAddSubview: (GGView *)subview
{}

- (void) viewDidMoveToSuperview
{}

- (void) viewDidMoveToWindow
{}

- (void) willRemoveSubview: (GGView *)subview
{}

- (void) rotateByAngle: (float)angle
{
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }
  [_boundsMatrix rotateByAngle: angle];
  _is_rotated_from_base = _is_rotated_or_scaled_from_base = YES;
}

- (void) _updateBoundsMatrix
{
  float sx;
  float sy;
  
  if (_bounds.size.width == 0)
    {
      if (_frame.size.width == 0)
	sx = 1;
      else
	sx = FLT_MAX;
    }
  else
    {
      sx = _frame.size.width / _bounds.size.width;
    }
  
  if (_bounds.size.height == 0)
    {
      if (_frame.size.height == 0)
	sy = 1;
      else
	sy = FLT_MAX;
    }
  else
    {
      sy = _frame.size.height / _bounds.size.height;
    }
  
  [_boundsMatrix scaleTo: sx : sy];
  if (sx != 1 || sy != 1)
    {
      _is_rotated_or_scaled_from_base = YES;
    }
}

- (void) setFrame: (NSRect)frameRect
{
  BOOL	changedOrigin = NO;
  BOOL	changedSize = NO;
  NSSize old_size = _frame.size;

  if (frameRect.size.width < 0)
    {
      NSWarnMLog(@"given negative width", 0);
      frameRect.size.width = 0;
    }
  if (frameRect.size.height < 0)
    {
      NSWarnMLog(@"given negative height", 0);
      frameRect.size.height = 0;
    }

  if (NSMinX(_frame) != NSMinX(frameRect) 
      || NSMinY(_frame) != NSMinY(frameRect))
    changedOrigin = YES;
  if (NSWidth(_frame) != NSWidth(frameRect) 
      || NSHeight(_frame) != NSHeight(frameRect))
    changedSize = YES;
  
  _frame = frameRect;
  /* FIXME: Touch bounds only if we are not scaled or rotated */
  _bounds.size = frameRect.size;
  

  if (changedOrigin)
    {
      [_frameMatrix setFrameOrigin: _frame.origin];
    }

  if (changedSize && _is_rotated_or_scaled_from_base)
    {
      [self _updateBoundsMatrix];
    }

  if (changedSize || changedOrigin)
    {
      if (_coordinates_valid)
	{
	  (*invalidateImp)(self, invalidateSel);
	}
      [self resizeSubviewsWithOldSize: old_size];
    }
}

- (void) setFrameOrigin: (NSPoint)newOrigin
{
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }
  _frame.origin = newOrigin;
  [_frameMatrix setFrameOrigin: _frame.origin];

}

- (void) setFrameSize: (NSSize)newSize
{
  NSSize old_size = _frame.size;

  if (newSize.width < 0)
    {
      NSWarnMLog(@"given negative width", 0);
      ggabort();
      newSize.width = 0;
    }
  if (newSize.height < 0)
    {
      NSWarnMLog(@"given negative height", 0);
      newSize.height = 0;
    }
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }

  if (_is_rotated_or_scaled_from_base)
    {
      float sx = _bounds.size.width  / _frame.size.width;
      float sy = _bounds.size.height / _frame.size.height;
      
      _frame.size = newSize;
      _bounds.size.width  = _frame.size.width  * sx;
      _bounds.size.height = _frame.size.height * sy;
    }
  else
    {
      _frame.size = _bounds.size = newSize;
    }

  [self resizeSubviewsWithOldSize: old_size];
}

- (void) setFrameRotation: (float)angle
{
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }
  [_frameMatrix setFrameRotation: angle];
  _is_rotated_from_base = _is_rotated_or_scaled_from_base = YES;

}

- (BOOL) isRotatedFromBase
{
  if (_is_rotated_from_base)
    {
      return YES;
    }
  else if (_super_view)
    {
      return [_super_view isRotatedFromBase];
    }
  else
    {
      return NO;
    }
}

- (BOOL) isRotatedOrScaledFromBase
{
  if (_is_rotated_or_scaled_from_base)
    {
      return YES;
    }
  else if (_super_view)
    {
      return [_super_view isRotatedOrScaledFromBase];
    }
  else
    {
      return NO;
    }
}

- (void) scaleUnitSquareToSize: (NSSize)newSize
{
  if (newSize.width < 0)
    {
      NSWarnMLog(@"given negative width", 0);
      newSize.width = 0;
    }
  if (newSize.height < 0)
    {
      NSWarnMLog(@"given negative height", 0);
      newSize.height = 0;
    }
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }
  _bounds.size.width  = _bounds.size.width  / newSize.width;
  _bounds.size.height = _bounds.size.height / newSize.height;

  _is_rotated_or_scaled_from_base = YES;
  
  [self _updateBoundsMatrix];
}

- (void) setBounds: (NSRect)aRect
{
  if (aRect.size.width < 0)
    {
      NSWarnMLog(@"given negative width", 0);
      aRect.size.width = 0;
    }
  if (aRect.size.height < 0)
    {
      NSWarnMLog(@"given negative height", 0);
      aRect.size.height = 0;
    }
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }
  _bounds = aRect;
  [_boundsMatrix
    setFrameOrigin: NSMakePoint(-_bounds.origin.x, -_bounds.origin.y)];
  [self _updateBoundsMatrix];

}

- (void) setBoundsOrigin: (NSPoint)newOrigin
{
  _bounds.origin = newOrigin;

  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }
  [_boundsMatrix setFrameOrigin: NSMakePoint(-newOrigin.x, -newOrigin.y)];

}

- (void) setBoundsSize: (NSSize)newSize
{
  if (newSize.width < 0)
    {
      NSWarnMLog(@"given negative width", 0);
      newSize.width = 0;
    }
  if (newSize.height < 0)
    {
      NSWarnMLog(@"given negative height", 0);
      newSize.height = 0;
    }
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }

  _bounds.size = newSize;
  [self _updateBoundsMatrix];

}

- (void) setBoundsRotation: (float)angle
{
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }
  [_boundsMatrix setFrameRotation: angle];
  _is_rotated_from_base = _is_rotated_or_scaled_from_base = YES;

}

- (void) translateOriginToPoint: (NSPoint)point
{
  if (_coordinates_valid)
    {
      (*invalidateImp)(self, invalidateSel);
    }
  [_boundsMatrix translateToPoint: point];

}

- (NSRect) centerScanRect: (NSRect)aRect
{
  GGNSAffineTransform	*matrix;

  /*
   *	Hmm - we assume that the windows coordinate system is centered on the
   *	pixels of the screen - this may not be correct of course.
   *	Plus - this is all pretty meaningless is we are not in a window!
   */
  matrix = [self _matrixToWindow];
  aRect.origin = [matrix pointInMatrixSpace: aRect.origin];
  aRect.size = [matrix sizeInMatrixSpace: aRect.size];

  aRect.origin.x = floor(aRect.origin.x);
  aRect.origin.y = floor(aRect.origin.y);
  aRect.size.width = floor(aRect.size.width);
  aRect.size.height = floor(aRect.size.height);

  matrix = [self _matrixFromWindow];
  aRect.origin = [matrix pointInMatrixSpace: aRect.origin];
  aRect.size = [matrix sizeInMatrixSpace: aRect.size];

  return aRect;
}

- (NSPoint) convertPoint: (NSPoint)aPoint fromView: (GGView*)aView
{
  NSPoint	new;
  GGNSAffineTransform	*matrix;

  if (!aView)
    aView = [[_window contentView] superview];
  if (aView == self || aView == nil)
    return aPoint;
  NSAssert(_window == [aView window], @"bad");

  matrix = [aView _matrixToWindow];
  new = [matrix pointInMatrixSpace: aPoint];

  if (_coordinates_valid)
    {
      matrix = _matrixFromWindow;
    }
  else
    {
      matrix = [self _matrixFromWindow];
    }
  new = [matrix pointInMatrixSpace: new];

  return new;
}

- (NSPoint) convertPoint: (NSPoint)aPoint toView: (GGView*)aView
{
  NSPoint	new;
  GGNSAffineTransform	*matrix;

  if (aView == nil)
    {
      aView = [[_window contentView] superview];
    }
  if (aView == self || aView == nil)
    {
      return aPoint;
    }
  NSAssert(_window == [aView window], @"bad");

  if (_coordinates_valid)
    {
      matrix = _matrixToWindow;
    }
  else
    {
      matrix = [self _matrixToWindow];
    }
  new = [matrix pointInMatrixSpace: aPoint];  
  matrix = [aView _matrixFromWindow];
  new = [matrix pointInMatrixSpace: new];

  return new;
}

- (NSRect) convertRect: (NSRect)aRect fromView: (GGView*)aView
{
  GGNSAffineTransform	*matrix;
  NSRect	r;

  if (aView == nil)
    {
      aView = [[_window contentView] superview];
    }
  if (aView == self || aView == nil)
    {
      return aRect;
    }
  NSAssert(_window == [aView window], @"bad");

  matrix = [aView _matrixToWindow];
  r.origin = [matrix pointInMatrixSpace: aRect.origin];
  r.size = [matrix sizeInMatrixSpace: aRect.size];

  if (_coordinates_valid)
    {
      matrix = _matrixFromWindow;
    }
  else
    {
      matrix = [self _matrixFromWindow];
    }
  r.origin = [matrix pointInMatrixSpace: r.origin];
  r.size = [matrix sizeInMatrixSpace: r.size];

  if (aView->_rFlags.flipped_view  != _rFlags.flipped_view)
    {
      r.origin.y -= r.size.height;
    }
  return r;
}

- (NSRect) convertRect: (NSRect)aRect toView: (GGView*)aView
{
  GGNSAffineTransform	*matrix;
  NSRect	r;

  if (aView == nil)
    {
      aView = [[_window contentView] superview];
    }
  if (aView == self || aView == nil)
    {
      return aRect;
    }
  NSAssert(_window == [aView window], @"bad");

  if (_coordinates_valid)
    {
      matrix = _matrixToWindow;
    }
  else
    {
      matrix = [self _matrixToWindow];
    }
  r.origin = [matrix pointInMatrixSpace: aRect.origin];
  r.size = [matrix sizeInMatrixSpace: aRect.size];

  matrix = [aView _matrixFromWindow];
  r.origin = [matrix pointInMatrixSpace: r.origin];
  r.size = [matrix sizeInMatrixSpace: r.size];

  if (aView->_rFlags.flipped_view  != _rFlags.flipped_view)
    {
      r.origin.y -= r.size.height;
    }
  return r;
}

- (NSSize) convertSize: (NSSize)aSize fromView: (GGView*)aView
{
  NSSize		new;
  GGNSAffineTransform	*matrix;

  if (aView == nil)
    {
      aView = [[_window contentView] superview];
    }
  if (aView == self || aView == nil)
    {
      return aSize;
    }
  NSAssert(_window == [aView window], @"bad");
  matrix = [aView _matrixToWindow];
  new = [matrix sizeInMatrixSpace: aSize];

  if (_coordinates_valid)
    {
      matrix = _matrixFromWindow;
    }
  else
    {
      matrix = [self _matrixFromWindow];
    }
  new = [matrix sizeInMatrixSpace: new];

  return new;
}

- (NSSize) convertSize: (NSSize)aSize toView: (GGView*)aView
{
  NSSize		new;
  GGNSAffineTransform	*matrix;

  if (aView == nil)
    {
      aView = [[_window contentView] superview];
    }
  if (aView == self || aView == nil)
    {
      return aSize;
    }
  NSAssert(_window == [aView window], @"bad");
  if (_coordinates_valid)
    {
      matrix = _matrixToWindow;
    }
  else
    {
      matrix = [self _matrixToWindow];
    }
  new = [matrix sizeInMatrixSpace: aSize];

  matrix = [aView _matrixFromWindow];
  new = [matrix sizeInMatrixSpace: new];

  return new;
}

- (void) setPostsFrameChangedNotifications: (BOOL)flag
{
  _post_frame_changes = flag;
}

- (void) setPostsBoundsChangedNotifications: (BOOL)flag
{
  _post_bounds_changes = flag;
}

/*
 * resize subviews only if we are supposed to and we have never been rotated
 */
- (void) resizeSubviewsWithOldSize: (NSSize)oldSize
{
  if (_rFlags.has_subviews)
    {
      id e, o;

      if (_autoresizes_subviews == NO || _is_rotated_from_base == YES)
	return;

      e = [_sub_views objectEnumerator];
      o = [e nextObject];
      while (o)
	{
	  [o resizeWithOldSuperviewSize: oldSize];
	  o = [e nextObject];
	}
    }
}

- (void) resizeWithOldSuperviewSize: (NSSize)oldSize
{
  int		options = 0;
  NSSize	superViewFrameSize = [_super_view frame].size;
  NSRect        newFrame = _frame;
  BOOL		changedOrigin = NO;
  BOOL		changedSize = NO;

  if (_autoresizingMask == GGNSViewNotSizable)
    return;

  /*
   * determine if and how the X axis can be resized
   */
  if (_autoresizingMask & GGNSViewWidthSizable)
    options++;

  if (_autoresizingMask & GGNSViewMinXMargin)
    options++;

  if (_autoresizingMask & GGNSViewMaxXMargin)
    options++;

  /*
   * adjust the X axis if any X options are set in the mask
   */
  if (options > 0)
    {
      float change = superViewFrameSize.width - oldSize.width;
      float changePerOption = change / options;

      if (_autoresizingMask & GGNSViewWidthSizable)
	{ 
          newFrame.size.width += changePerOption;
          changedSize = YES;
	}
      if (_autoresizingMask & GGNSViewMinXMargin)
	{
	  newFrame.origin.x += changePerOption;
	  changedOrigin = YES;
	}
    }

  /*
   * determine if and how the Y axis can be resized
   */
  options = 0;
  if (_autoresizingMask & GGNSViewHeightSizable)
    options++;

  if (_autoresizingMask & GGNSViewMinYMargin)
    options++;

  if (_autoresizingMask & GGNSViewMaxYMargin)
    options++;

  /*
   * adjust the Y axis if any Y options are set in the mask
   */
  if (options > 0)
    {
      float change = superViewFrameSize.height - oldSize.height;
      float changePerOption = change / options;
      
      if (_autoresizingMask & GGNSViewHeightSizable)
	{
          newFrame.size.height += changePerOption;
	  changedSize = YES;
	}
      if (_autoresizingMask & (GGNSViewMaxYMargin | GGNSViewMinYMargin))
	{
	  if (_super_view && _super_view->_rFlags.flipped_view == YES)
	    {
	      if (_autoresizingMask & GGNSViewMaxYMargin)
		{
		  newFrame.origin.y += changePerOption;
		  changedOrigin = YES;
		}
	    }
	  else
	    {
	      if (_autoresizingMask & GGNSViewMinYMargin)
		{
		  newFrame.origin.y += changePerOption;
		  changedOrigin = YES;
		}
	    }
	}
    }
  [self setFrame: newFrame];
}

- (void) lockFocusInRect: (NSRect)rect
{
  NSRect wrect;
  NSPoint origin;

  NSAssert(_window != nil, @"bad");

  wrect = [self convertRect: rect toView: nil];
  origin = [self convertPoint: NSZeroPoint
		 toView: nil];
//  NSDebugLLog(@"GGView", @"Displaying rect \n\t%@\n\t window %p", 
//	      NSStringFromRect(wrect), _window);
  [_window setFrameInRect: wrect
	   atOrigin: origin];
}

- (void) unlockFocusNeedsFlush: (BOOL)flush
{
}

- (void) lockFocus
{
  [self lockFocusInRect: [self visibleRect]];
}

- (void) unlockFocus
{
  [self unlockFocusNeedsFlush: YES ];
}

- (BOOL) lockFocusIfCanDraw
{
  if ([self canDraw])
    {
      [self lockFocus];
      return YES;
    }
  else
    {
      return NO;
    }
}

- (BOOL) canDraw
{			// not implemented per OS spec FIX ME
  if (_window != nil)
    {
      return YES;
    }
  else
    {
      return NO;
    }
}

- (void) display
{
  if (_window != nil)
    {
      if (_coordinates_valid == NO)
	{
	  [self _rebuildCoordinates];
	}      
      [self displayRect: _visibleRect];
    }
}

- (void) displayIfNeeded
{
  if (_rFlags.needs_display == YES)
    {
      if ([self isOpaque] == YES)
	{
	  [self displayIfNeededIgnoringOpacity];
	}
      else
	{
	  GGView	*firstOpaque = [self opaqueAncestor];
	  NSRect	rect;

	  if (_coordinates_valid == NO)
	    {
	      [self _rebuildCoordinates];
	    }
	  rect = NSIntersectionRect(_invalidRect, _visibleRect);
	  rect = [firstOpaque convertRect: rect  fromView: self];
	  if (NSIsEmptyRect(rect) == NO)
	    {
	      [firstOpaque displayIfNeededInRectIgnoringOpacity: rect];
	    }
	  /*
	   * If we still need display after displaying the invalid rectangle,
	   * display any subviews that need display.
	   */ 
	  if (_rFlags.needs_display == YES)
	    {
	      NSEnumerator	*enumerator = [_sub_views objectEnumerator];
	      GGView		*sub;

	      while ((sub = [enumerator nextObject]) != nil)
		{
		  if (sub->_rFlags.needs_display)
		    {
		      [sub displayIfNeededIgnoringOpacity];
		    }
		}
	      _rFlags.needs_display = NO;
	    }
	}
    }
}

- (void) displayIfNeededIgnoringOpacity
{
  if (_rFlags.needs_display == YES)
    {
      NSRect	rect;

      if (_coordinates_valid == NO)
	{
	  [self _rebuildCoordinates];
	}
      rect = NSIntersectionRect(_invalidRect, _visibleRect);
      if (NSIsEmptyRect(rect) == NO)
	{
	  [self displayIfNeededInRectIgnoringOpacity: rect];
	}
      /*
       * If we still need display after displaying the invalid rectangle,
       * display any subviews that need display.
       */ 
      if (_rFlags.needs_display == YES)
	{
	  NSEnumerator	*enumerator = [_sub_views objectEnumerator];
	  GGView	*sub;

	  while ((sub = [enumerator nextObject]) != nil)
	    {
	      if (sub->_rFlags.needs_display)
		{
		  [sub displayIfNeededIgnoringOpacity];
		}
	    }
	  _rFlags.needs_display = NO;
	}
    }
}

- (void) displayIfNeededInRect: (NSRect)aRect
{
  if (_rFlags.needs_display == NO)
    {
      if ([self isOpaque] == YES)
	{
	  [self displayIfNeededInRectIgnoringOpacity: aRect];
	}
      else
	{
	  GGView	*firstOpaque = [self opaqueAncestor];
	  NSRect	rect;

	  rect = [firstOpaque convertRect: aRect fromView: self];
	  [firstOpaque displayIfNeededInRectIgnoringOpacity: rect];
	}
    }
}

- (void) displayIfNeededInRectIgnoringOpacity: (NSRect)aRect
{
  if (_window == nil)
    {
      return;
    }
  if (_rFlags.needs_display == YES)
    {
      BOOL	subviewNeedsDisplay = NO;
      NSRect	neededRect;
      NSRect	redrawRect;

      if (_coordinates_valid == NO)
	{
	  [self _rebuildCoordinates];
	}
      aRect = NSIntersectionRect(aRect, _visibleRect);
      redrawRect = NSIntersectionRect(aRect, _invalidRect);
      neededRect = NSIntersectionRect(_visibleRect, _invalidRect);

      if (NSIsEmptyRect(redrawRect) == NO)
	{
	  [self lockFocusInRect: redrawRect];
	  [self drawRect: redrawRect];
	  [self unlockFocusNeedsFlush: YES];
	}
      if (_rFlags.has_subviews == YES)
	{
	  unsigned	count = [_sub_views count];

	  if (count > 0)
	    {
	      GGView	*array[count];
	      unsigned	i;

	      [_sub_views getObjects: array];

	      for (i = 0; i < count; i++)
		{
		  NSRect	isect;
		  GGView	*subview = array[i];
		  NSRect	subviewFrame = subview->_frame;
		  BOOL		intersectCalculated = NO;

		  if ([subview->_frameMatrix isRotated])
		    {
		      [subview->_frameMatrix boundingRectFor: subviewFrame
						     result: &subviewFrame];
		    }

		  /*
		   * Having drawn ourself into the rect, we must make sure that
		   * subviews overlapping the area are redrawn.
		   */
		  isect = NSIntersectionRect(redrawRect, subviewFrame);
		  if (NSIsEmptyRect(isect) == NO)
		    {
		      isect = [subview convertRect: isect
					  fromView: self];
		      intersectCalculated = YES;
		      /*
		       * hack the ivars of the subview directly for speed.
		       */
		      subview->_rFlags.needs_display = YES;
		      subview->_invalidRect = NSUnionRect(subview->_invalidRect,
			    isect);
		    }

		  if (subview->_rFlags.needs_display == YES)
		    {
		      if (intersectCalculated == NO
			|| NSEqualRects(aRect, redrawRect) == NO)
			{
			  isect = NSIntersectionRect(aRect, subviewFrame);
			  isect = [subview convertRect: isect
					      fromView: self];
			}
		      [subview displayIfNeededInRectIgnoringOpacity: isect];
		      if (subview->_rFlags.needs_display == YES)
			{
			  subviewNeedsDisplay = YES;
			}
		    }
		}
	    }
	}

      /*
       * If the rect we displayed contains the _invalidRect or _visibleRect
       * then we can empty _invalidRect.
       * If all subviews have been fully displayed, we can also turn off the
       * 'needs_display' flag.
       */
      if (NSEqualRects(aRect, NSUnionRect(neededRect, aRect)) == YES)
	{
	  _invalidRect = NSZeroRect;
	  _rFlags.needs_display = subviewNeedsDisplay;
	}
      if (_rFlags.needs_display == YES
	&& NSEqualRects(aRect, NSUnionRect(_visibleRect, aRect)) == YES)
	{
	  _rFlags.needs_display = NO;
	}
      [_window flushWindow];
    }
}

- (void) displayRect: (NSRect)rect
{
  if ([self isOpaque] == YES)
    {
      [self displayRectIgnoringOpacity: rect];
    }
  else
    {
      GGView *firstOpaque = [self opaqueAncestor];

      rect = [firstOpaque convertRect: rect fromView: self];
      [firstOpaque displayRectIgnoringOpacity: rect];
    }
}

- (void) displayRectIgnoringOpacity: (NSRect)aRect
{
  BOOL		subviewNeedsDisplay = NO;
  NSRect	neededRect;

  if (_window == nil)
    {
      return;
    }
  if (_coordinates_valid == NO)
    {
      [self _rebuildCoordinates];
    }
  aRect = NSIntersectionRect(aRect, _visibleRect);
  neededRect = NSIntersectionRect(_invalidRect, _visibleRect);

  if (NSIsEmptyRect(aRect) == NO)
    {
      /*
       * Now we draw this view.
       */
      [self lockFocusInRect: aRect];
      [self drawRect: aRect];
      [self unlockFocusNeedsFlush: YES];
    }

  if (_rFlags.has_subviews == YES)
    {
      unsigned		count = [_sub_views count];

      if (count > 0)
	{
	  GGView	*array[count];
	  unsigned	i;

	  [_sub_views getObjects: array];

	  for (i = 0; i < count; ++i)
	    {
	      GGView	*subview = array[i];
	      NSRect	subviewFrame = subview->_frame;
	      NSRect	isect;
	      BOOL	intersectCalculated = NO;

	      if ([subview->_frameMatrix isRotated] == YES)
		[subview->_frameMatrix boundingRectFor: subviewFrame
					       result: &subviewFrame];

	      /*
	       * Having drawn ourself into the rect, we must make sure that
	       * subviews overlapping the area are redrawn.
	       */
	      isect = NSIntersectionRect(aRect, subviewFrame);
	      if (NSIsEmptyRect(isect) == NO)
		{
		  isect = [subview convertRect: isect
				      fromView: self];
		  intersectCalculated = YES;
		  /*
		   * hack the ivars of the subview directly for speed.
		   */
		  subview->_rFlags.needs_display = YES;
		  subview->_invalidRect = NSUnionRect(subview->_invalidRect,
			isect);
		}

	      if (subview->_rFlags.needs_display == YES)
		{
		  if (intersectCalculated == NO)
		    {
		      isect = [subview convertRect: isect
					  fromView: self];
		    }
		  [subview displayIfNeededInRectIgnoringOpacity: isect];
		  if (subview->_rFlags.needs_display == YES)
		    {
		      subviewNeedsDisplay = YES;
		    }
		}
	    }
	}
    }

  /*
   * If the rect we displayed contains the _invalidRect or _visibleRect
   * then we can empty _invalidRect.  If all subviews have been
   * fully displayed, we can also turn off the 'needs_display' flag.
   */
  if (NSEqualRects(aRect, NSUnionRect(neededRect, aRect)) == YES)
    {
      _invalidRect = NSZeroRect;
      _rFlags.needs_display = subviewNeedsDisplay;
    }
  if (_rFlags.needs_display == YES
    && NSEqualRects(aRect, NSUnionRect(_visibleRect, aRect)) == YES)
    {
      _rFlags.needs_display = NO;
    }
  [_window flushWindow];
}

/**
  This method is invoked to handle drawing inside the view.  The
  default GGView's implementation does nothing; subclasses might
  override it to draw something inside the view.  Since GGView's
  implementation is guaranteed to be empty, you should not call
  super's implementation when you override it in subclasses.
  drawRect: is invoked when the focus has already been locked on the
  view; you can use arbitrary postscript functions in drawRect: to
  draw inside your view; the coordinate system in which you draw is
  the view's own coordinate system (this means for example that you
  should refer to the rectangle covered by the view using its bounds,
  and not its frame).  The argument of drawRect: is the rectangle
  which needs to be redrawn.  In a lossy implementation, you can
  ignore the argument and redraw the whole view; if you are aiming at
  performance, you may want to redraw only what is inside the
  rectangle which needs to be redrawn; this usually improves drawing
  performance considerably.  */
- (void) drawRect: (NSRect)rect
{
#ifdef DEBUG
//   NSRect theRect = [self frame];
//   glColor3f(0,0,1);
//   glBegin(GL_LINE_LOOP);
//   glVertex2f(20,0);
//   glVertex2f(theRect.size.width-1, 20);
//   glVertex2f(theRect.size.width - 20,theRect.size.height-1);
//   glVertex2f(0, theRect.size.height-20);
//   glEnd();

#endif
//  NSDebugMLLog(@"GGView", @"%@", self);
}

- (NSRect) visibleRect
{
  if (_coordinates_valid == NO)
    {
      [self _rebuildCoordinates];
    }
  return _visibleRect;
}

- (void) setNeedsDisplay: (BOOL)flag
{
  if (flag)
    {
      [self setNeedsDisplayInRect: _bounds];
    }
  else
    {
      _rFlags.needs_display = NO;
      _invalidRect = NSZeroRect;
    }
}

- (void) setNeedsDisplayInRect: (NSRect)rect
{
  GGView	*currentView = _super_view;

  /*
   *	Limit to bounds, combine with old _invalidRect, and then check to see
   *	if the result is the same as the old _invalidRect - if it isn't then
   *	set the new _invalidRect.
   */
  rect = NSIntersectionRect(rect, _bounds);
  rect = NSUnionRect(_invalidRect, rect);
  if (NSEqualRects(rect, _invalidRect) == NO)
    {
      GGView	*firstOpaque = [self opaqueAncestor];

      _rFlags.needs_display = YES;
      _invalidRect = rect;
      if (firstOpaque == self)
	{
	  [_window setViewsNeedDisplay: YES];
	}
      else
	{
	  rect = [firstOpaque convertRect: _invalidRect fromView: self];
	  [firstOpaque setNeedsDisplayInRect: rect];
	}
    }
  /*
   * Must make sure that superviews know that we need display.
   * NB. we may have been marked as needing display and then moved to another
   * parent, so we can't assume that our parent is marked simply because we are.
   */
  while (currentView)
    {
      currentView->_rFlags.needs_display = YES;
      currentView = currentView->_super_view;
    }
}

static GGView* findByTag(GGView *view, int aTag, unsigned *level)
{
  unsigned	i, count;
  NSArray	*sub = [view subviews];

  count = [sub count];
  if (count > 0)
    {
      GGView	*array[count];

      [sub getObjects: array];

      for (i = 0; i < count; i++)
	{
	  if ([array[i] tag] == aTag)
	    return array[i];
	}
      *level += 1;
      for (i = 0; i < count; i++)
	{
	  GGView	*v;

	  v = findByTag(array[i], aTag, level);
	  if (v != nil)
	    return v;
	}
      *level -= 1;
    }
  return nil;
}

- (id) viewWithTag: (int)aTag
{
  GGView	*view = nil;

  /*
   * If we have the specified tag - return self.
   */
  if ([self tag] == aTag)
    {
      view = self;
    }
  else if (_rFlags.has_subviews)
    {
      unsigned	count = [_sub_views count];

      if (count > 0)
	{
	  GGView	*array[count];
	  unsigned	i;

	  [_sub_views getObjects: array];

	  /*
	   * Quick check to see if any of our direct descendents has the tag.
	   */
	  for (i = 0; i < count; i++)
	    {
	      GGView *subView = array[i];

	      if ([subView tag] == aTag)
	        {
		  view = subView;
		  break;
		}
	    }

	  if (view == nil)
	    {
	      unsigned	level = 0xffffffff;

	      /*
	       * Ok - do it the long way - search the while tree for each of
	       * our descendents and see which has the closest view matching
	       * the tag.
	       */
	      for (i = 0; i < count; i++)
		{
		  unsigned	l = 0;
		  GGView	*v;

		  v = findByTag(array[i], aTag, &l);

		  if (v != nil && l < level)
		    {
		      view = v;
		      level = l;
		    }
		}
	    }
	}
    }
  return view;
}

/*
 * Aiding Event Handling
 */
/**
  Returns <code>YES</code> if the view object will accept the first
  click received when in an inactive window, and <code>NO</code>
  otherwise.  */
- (BOOL) acceptsFirstMouse: (Event*)theEvent
{
  return NO;
}

/**
  Returns the subview, lowest in the receiver's hierarchy, which
  contains <var>aPoint</var> */
- (GGView*) hitTest: (NSPoint)aPoint
{
  NSPoint p;
  unsigned count;
  GGView *v = nil, *w;

  /* If not within our frame then it can't be a hit */
  if (![_super_view mouse: aPoint inRect: _frame])
    return nil;

  p = [self convertPoint: aPoint fromView: _super_view];

  if (_rFlags.has_subviews)
    {
      count = [_sub_views count];
      if (count > 0)
	{
	  GGView*	array[count];

	  [_sub_views getObjects: array];

	  while (count > 0)
	    {
	      w = array[--count];
	      v = [w hitTest: p];
	      if (v)
		break;
	    }
	}
    }
  /*
   * mouse is either in the subview or within self
   */
  if (v)
    return v;
  else
    return self;
}

/**
  Returns whether or not <var>aPoint</var> lies within <var>aRect</var>
*/
- (BOOL) mouse: (NSPoint)aPoint  inRect: (NSRect)aRect
{
  if (aPoint.x < aRect.origin.x)
    return NO;
  if (aPoint.y < aRect.origin.y)
    return NO;
  if (aPoint.x > (aRect.origin.x + aRect.size.width))
    return NO;
  if (aPoint.y > (aRect.origin.y + aRect.size.height))
    return NO;

  return YES;
}

- (BOOL) performKeyEquivalent: (Event*)theEvent
{
  unsigned i;

  for (i = 0; i < [_sub_views count]; i++)
    if ([[_sub_views objectAtIndex: i] performKeyEquivalent: theEvent] == YES)
      return YES;
  return NO;
}

- (BOOL) performMnemonic: (NSString *)aString
{
  unsigned i;

  for (i = 0; i < [_sub_views count]; i++)
    if ([[_sub_views objectAtIndex: i] performMnemonic: aString] == YES)
      return YES;
  return NO;
}

- (BOOL) shouldDelayWindowOrderingForEvent: (Event*)anEvent
{
  return NO;
}


- (void) setNextKeyView: (GGView *)aView
{
  if (!aView)
    {
      _nextKeyView = nil;
      return;
    }

  if ([aView isKindOfClass: viewClass])
    {
      // As an exception, we do not retain aView, to avoid retain loops
      // (the simplest being a view retaining and being retained
      // by another view), which prevents objects from being ever
      // deallocated.  To understand how we manage without retaining
      // _nextKeyView, see [GGView -dealloc].
      _nextKeyView = aView;
      if ([aView previousKeyView] != self)
	[aView setPreviousKeyView: self];
    }
}
- (GGView *) nextKeyView
{
  return _nextKeyView;
}
- (GGView *) nextValidKeyView
{
  GGView *theView;

  theView = _nextKeyView;
  while (1)
    {
      if ([theView acceptsFirstResponder] || (theView == nil)
	  || (theView == self))
	return theView;

      theView = [theView nextKeyView];
    }
}
- (void) setPreviousKeyView: (GGView *)aView
{
  if (!aView)
    {
      _previousKeyView = nil;
      return;
    }

  if ([aView isKindOfClass: viewClass])
    {
      _previousKeyView = aView;
      if ([aView nextKeyView] != self)
	[aView setNextKeyView: self];
    }
}
- (GGView *) previousKeyView
{
  return _previousKeyView;
}
- (GGView *) previousValidKeyView
{
  GGView *theView;

  theView = _previousKeyView;
  while (1)
    {
      if ([theView acceptsFirstResponder] || (theView == nil)
	  || (theView == self))
	return theView;

      theView = [theView previousKeyView];
    }
}


/*
 * Printing
 */
- (NSData*) dataWithEPSInsideRect: (NSRect)aRect
{
  return nil;
}

- (void) fax: (id)sender
{
}

- (void) print: (id)sender
{
}

- (void) writeEPSInsideRect: (NSRect)rect
	       toPasteboard: (NSPasteboard*)pasteboard
{
}

/*
 * Pagination
 */
- (void) adjustPageHeightNew: (float*)newBottom
			 top: (float)oldTop
		      bottom: (float)oldBottom
		       limit: (float)bottomLimit
{
}

- (void) adjustPageWidthNew: (float*)newRight
		       left: (float)oldLeft
		      right: (float)oldRight
		      limit: (float)rightLimit
{
}

- (float) heightAdjustLimit
{
  return 0;
}

- (BOOL) knowsPagesFirst: (int*)firstPageNum last: (int*)lastPageNum
{
  return NO;
}

- (NSPoint) locationOfPrintRect: (NSRect)aRect
{
  return NSZeroPoint;
}

- (NSRect) rectForPage: (int)page
{
  return NSZeroRect;
}

- (float) widthAdjustLimit
{
  return 0;
}

/*
 * Writing Conforming PostScript
 */
- (void) beginPage: (int)ordinalNum
	     label: (NSString*)aString
	      bBox: (NSRect)pageRect
	     fonts: (NSString*)fontNames
{
}

- (void) beginPageSetupRect: (NSRect)aRect placement: (NSPoint)location
{
}

- (void) beginPrologueBBox: (NSRect)boundingBox
	      creationDate: (NSString*)dateCreated
		 createdBy: (NSString*)anApplication
		     fonts: (NSString*)fontNames
		   forWhom: (NSString*)user
		     pages: (int)numPages
		     title: (NSString*)aTitle
{
}

- (void) addToPageSetup
{
}

- (void) beginSetup
{
}

- (void) beginTrailer
{
}

- (void) drawPageBorderWithSize: (NSSize)borderSize
{
}

- (void) drawSheetBorderWithSize: (NSSize)borderSize
{
}

- (void) endHeaderComments
{
}

- (void) endPrologue
{
}

- (void) endSetup
{
}

- (void) endPageSetup
{
}

- (void) endPage
{
}

- (void) endTrailer
{
}

/*
 * NSCoding protocol
 */
- (void) encodeWithCoder: (NSCoder*)aCoder
{
  [super encodeWithCoder: aCoder];

  NSDebugLLog(@"GGView", @"GGView: start encoding\n");
  [aCoder encodeRect: _frame];
  [aCoder encodeRect: _bounds];
  [aCoder encodeValueOfObjCType: @encode(BOOL) at: &_is_rotated_from_base];
  [aCoder encodeValueOfObjCType: @encode(BOOL)
			     at: &_is_rotated_or_scaled_from_base];
  [aCoder encodeValueOfObjCType: @encode(BOOL) at: &_post_frame_changes];
  [aCoder encodeValueOfObjCType: @encode(BOOL) at: &_autoresizes_subviews];
  [aCoder encodeValueOfObjCType: @encode(unsigned int) at: &_autoresizingMask];
  [aCoder encodeConditionalObject: _nextKeyView];
  [aCoder encodeConditionalObject: _previousKeyView];
  [aCoder encodeObject: _sub_views];
  NSDebugLLog(@"GGView", @"GGView: finish encoding\n");
}

- (id) initWithCoder: (NSCoder*)aDecoder
{
  NSRect	rect;
  NSEnumerator	*e;
  GGView	*sub;
  NSArray	*subs;

  self = [super initWithCoder: aDecoder];

  NSDebugLLog(@"GGView", @"GGView: start decoding\n");

  _frame = [aDecoder decodeRect];
  _bounds.origin = NSZeroPoint;
  _bounds.size = _frame.size;

  _frameMatrix = [GGNSAffineTransform new];	// Map fromsuperview to frame
  _boundsMatrix = [GGNSAffineTransform new];	// Map fromsuperview to bounds
  _matrixToWindow = [GGNSAffineTransform new];	// Map to window coordinates
  _matrixFromWindow = [GGNSAffineTransform new];	// Map from window coordinates
  [_frameMatrix setFrameOrigin: _frame.origin];

  rect = [aDecoder decodeRect];
  [self setBounds: rect];

  _sub_views = [NSMutableArray new];
  _tracking_rects = [NSMutableArray new];
  _cursor_rects = [NSMutableArray new];

  _super_view = nil;
  _window = nil;
  _rFlags.needs_display = YES;
  _coordinates_valid = NO;

  _rFlags.flipped_view = [self isFlipped];

  [aDecoder decodeValueOfObjCType: @encode(BOOL) at: &_is_rotated_from_base];
  [aDecoder decodeValueOfObjCType: @encode(BOOL)
			       at: &_is_rotated_or_scaled_from_base];
  [aDecoder decodeValueOfObjCType: @encode(BOOL) at: &_post_frame_changes];
  [aDecoder decodeValueOfObjCType: @encode(BOOL) at: &_autoresizes_subviews];
  [aDecoder decodeValueOfObjCType: @encode(unsigned int) at: &_autoresizingMask];
  _nextKeyView = [aDecoder decodeObject];
  _previousKeyView = [aDecoder decodeObject];

  [aDecoder decodeValueOfObjCType: @encode(id) at: &subs];
  e = [subs objectEnumerator];
  while ((sub = [e nextObject]) != nil)
    {
      NSAssert(sub->_window == nil, @"bad");
      NSAssert(sub->_super_view == nil, @"bad");
      [sub viewWillMoveToWindow: _window];
      [sub viewWillMoveToSuperview: self];
      [sub setNextResponder: self];
      [_sub_views addObject: sub];
      _rFlags.has_subviews = 1;
      [sub setNeedsDisplay: YES];
      [sub viewDidMoveToWindow];
      [sub viewDidMoveToSuperview];
      [self didAddSubview: sub];
    }
  RELEASE(subs);

  NSDebugLLog(@"GGView", @"GGView: finish decoding\n");

  return self;
}

/*
 * Accessor methods
 */
- (void) setAutoresizesSubviews: (BOOL)flag
{
  _autoresizes_subviews = flag;
}

- (void) setAutoresizingMask: (unsigned int)mask
{
  _autoresizingMask = mask;
}

/** Returns the window in which the receiver resides. */
- (GGNSWindow*) window
{
  return _window;
}

- (BOOL) autoresizesSubviews
{
  return _autoresizes_subviews;
}

- (unsigned int) autoresizingMask
{
  return _autoresizingMask;
}

- (NSArray*) subviews
{
  /*
   * Return a mutable copy 'cos we know that a mutable copy of an array or
   * a mutable array does a shallow copy - which is what we want to give
   * away - we don't want people to mess with our actual subviews array.
   */
  return AUTORELEASE([_sub_views mutableCopyWithZone: NSDefaultMallocZone()]);
}

- (GGView*) superview
{
  return _super_view;
}

- (BOOL) shouldDrawColor
{
  return YES;
}

- (BOOL) isOpaque
{
  return NO;
}

- (BOOL) needsDisplay
{
  return _rFlags.needs_display;
}

- (int) tag
{
  return -1;
}

- (BOOL) isFlipped
{
  return NO;
}

- (NSRect) bounds
{
  return _bounds;
}

- (NSRect) frame
{
  return _frame;
}

- (float) boundsRotation
{
  return [_boundsMatrix rotationAngle];
}

- (float) frameRotation
{
  return [_frameMatrix rotationAngle];
}

- (BOOL) postsFrameChangedNotifications
{
  return _post_frame_changes;
}

- (BOOL) postsBoundsChangedNotifications
{
  return _post_bounds_changes;
}


/*
 *	Private methods.
 */


/*
 *	The [-_invalidateCoordinates] method marks the coordinate mapping
 *	matrices (matrixFromWindow and _matrixToWindow) and the cached visible
 *	rectangle as invalid.  It recursively invalidates the coordinates for
 *	all subviews as well.
 *	This method must be called whenever the size, shape or position of
 *	the view is changed in any way.
 */
- (void) _invalidateCoordinates
{
  if (_coordinates_valid == YES)
    {
      unsigned	count;

      _coordinates_valid = NO;
      if (_rFlags.has_subviews)
	{
	  count = [_sub_views count];
	  if (count > 0)
	    {
	      GGView*	array[count];
	      unsigned	i;

	      [_sub_views getObjects: array];
	      for (i = 0; i < count; i++)
		{
		  GGView	*sub = array[i];

		  if (sub->_coordinates_valid == YES)
		    {
		      (*invalidateImp)(sub, invalidateSel);
		    }
		}
	    }
	}
    }
}

/*
 *	The [-_matrixFromWindow] method returns a matrix that can be used to
 *	map coordinates in the windows coordinate system to coordinates in the
 *	views coordinate system.  It rebuilds the mapping matrices and
 *	visible rectangle cache if necessary.
 *	All coordinate transformations use this matrix.
 */
- (GGNSAffineTransform*) _matrixFromWindow
{
  if (_coordinates_valid == NO)
    {
      [self _rebuildCoordinates];
    }
  return _matrixFromWindow;
}

/*
 *	The [-_matrixToWindow] method returns a matrix that can be used to
 *	map coordinates in the views coordinate system to coordinates in the
 *	windows coordinate system.  It rebuilds the mapping matrices and
 *	visible rectangle cache if necessary.
 *	All coordinate transformations use this matrix.
 */
- (GGNSAffineTransform*) _matrixToWindow
{
  if (_coordinates_valid == NO)
    {
      [self _rebuildCoordinates];
    }
  return _matrixToWindow;
}

/*
 *	The [-_rebuildCoordinates] method rebuilds the coordinate mapping
 *	matrices (matrixFromWindow and _matrixToWindow) and the cached visible
 *	rectangle if they have been invalidated.
 */
- (void) _rebuildCoordinates
{
  if (_coordinates_valid == NO)
    {
      _coordinates_valid = YES;
      if (!_window)
	{
	  _visibleRect = NSZeroRect;
	  [_matrixToWindow makeIdentityMatrix];
	  [_matrixFromWindow makeIdentityMatrix];
	}
      if (!_super_view)
	{
	  _visibleRect = _bounds;
	  [_matrixToWindow makeIdentityMatrix];
	  [_matrixFromWindow makeIdentityMatrix];
	}
      else
	{
	  NSRect	superviewsVisibleRect;
	  BOOL		wasFlipped = _super_view->_rFlags.flipped_view;
	  GGNSAffineTransform	*pMatrix = [_super_view _matrixToWindow];

	  [_matrixToWindow takeMatrixFromTransform: pMatrix];
	  (*appImp)(_matrixToWindow, appSel, _frameMatrix);
	  if (_rFlags.flipped_view != wasFlipped)
	    {
	      /*
	       * The flipping process must result in a coordinate system that
	       * exactly overlays the original.	 To do that, we must translate
	       * the origin by the height of the view.
	       */
	      flip->matrix.ty = _frame.size.height;
	      (*appImp)(_matrixToWindow, appSel, flip);
	    }
	  (*appImp)(_matrixToWindow, appSel, _boundsMatrix);
	  [_matrixFromWindow takeMatrixFromTransform: _matrixToWindow];
	  [_matrixFromWindow inverse];

	  superviewsVisibleRect = [self convertRect: [_super_view visibleRect]
					   fromView: _super_view];

	  _visibleRect = NSIntersectionRect(superviewsVisibleRect, _bounds);

	}
    }
}

@end

