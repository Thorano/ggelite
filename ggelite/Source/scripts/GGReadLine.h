//
//  GGReadLine.h
//  elite
//
//  Created by Fr�d�ric De Jaeger on 26/10/04.
//  Copyright 2004 __MyCompanyName__. All rights reserved.
//

#ifndef __GGReadLine_h
#define __GGReadLine_h
#import "GGRunTime.h"

GGBEGINDEC
/*%%%
extern complex readline():NSString;
%%%*/
GGENDDEC

@interface Complex_readline : ExtendedProcedure {

}
- (void) runWith;
+ readline_;
@end

#endif