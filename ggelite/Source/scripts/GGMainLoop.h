/* 	-*-ObjC-*- */
/*
 *  GGMainLoop.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __GGMainLoop_h
#define __GGMainLoop_h

#import <Foundation/Foundation.h>
#import "config.h"


GGBEGINDEC
@class Sched;
@class TaskSched;

@interface GGLoopState : NSObject <NSCoding>
{
    @public
    Sched			*fastScheduler, *slowScheduler, *iaScheduler;
    TaskSched		*taskSched;
}
@end



@interface GGMainLoop : NSObject 
{
    GGLoopState             *state;
    TaskSched               *noSaveSched;
    int                     cont;
    BOOL                    timeSpeedChange;
    double                  lastTime;
    double                  moyDeltaTime;
    double                  incrementGame;
}
- initWithArgc: (int) argc
          argv: (char *[]) argv;

+ new: (int) argc arg: (char *[]) argv;
- (float) timeSpeed;
- setTimeSpeed: (float) val;
- (void) incrementGameTime: (double)val;
- (double) fps;
- (GGLoopState *)state;
- (void) setState: (GGLoopState *)state;



- (void) stopMainLoop;
- (void) beforeRunningLoop;
//- (void) runLoop;
- (BOOL) isStop;
- (void) runTimers;


- (Sched *) slowScheduler;
- (Sched *) fastScheduler;
- (Sched *) iaScheduler;
- (TaskSched *) taskSched;
- (TaskSched *) taskSchedNoSave;

- (void) slowSchedule: (NSObject *)obj
         withSelector: (SEL) s
                  arg: arg;
- (void) fastSchedule: (NSObject *)obj
         withSelector: (SEL) s
                  arg: arg;
- (void) iaSchedule: (NSObject *)obj
       withSelector: (SEL) s
                arg: arg;
- (void) unSchedule: (NSObject *) obj;
- (BOOL) isScheduled: (NSObject *) obj;


@end

extern double lastTimeSpeedChange;
extern double timeSpeed;
extern double deltaTime;	//time between two frame measure in game time
extern double gameTime;		// time in the game
extern double realDeltaTime; 	// GGReal time between two frames
extern double currentTime;	// GGReal system current time.

extern GGMainLoop *theLoop;

GGENDDEC



#endif
