/* 	-*-ObjC-*- */
/*
 *  GGRunTime.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef __GGRunTime_h
#define __GGRunTime_h

#import <Foundation/NSObject.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSException.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSValue.h>
#import <Foundation/NSInvocation.h>
#import "GGLog.h"
#import "GG3D.h"
#import "Sched.h"
#import "GGArchiverExtension.h"
//#import "Tableau.h"

GGBEGINDEC
/*%%%
 import types;
 %%%*/

@class ExtendedProcedure;
@class GGTimer;
@class GGProcessScheduler;

@interface ProcessManager : NSObject <NSCoding>
{
    NSMutableSet *set;
}
+ processManager;
+ (void) clean;

- (void) add: (ExtendedProcedure *) p;
- (void) remove: (ExtendedProcedure *) p;
- (NSSet *) processes;
@end



@interface ListeningBlock : NSObject <NSCoding>
{
  ExtendedProcedure 	*callback;
  NSMutableArray	*timers;
}
+ newWithCallback: (ExtendedProcedure *) callback;
- (void) forwardInvocation: (NSInvocation *) invocation;
- (void) addTimer: (GGTimer *)aTimer;
- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (void) invalidate;
@end

@interface ExtendedProcedure : NSObject <NSCoding>
{
    GGProcessScheduler              *_currentScheduler;   // not retained.
  id                    _process;
  id 		        handle;			/*callee */
  //  NSInvocation          *thecallback;
  NSMutableArray        *targets;
@public
  NSMutableArray	*stack;
}
+ allocInstance;
- (void) registerContinuation: obj
			   at: (SEL) fn;
- (void) removeListeningBlock: (ListeningBlock *) block;
- (void) popListeningBlock;
- (void) addListeningBlock: (ListeningBlock *) block;
- (ListeningBlock *) currentListeningBlock;
- (void) killAllBlock;
- (void) invalidate;
- (GGTimer *)scheduledTimerForDate: (double)val
			  selector: (SEL) sel;
- (void) sleepDuring: (double) val
	    wakeUpAt: (SEL) s;
- (void) markForGC;

- (id) initWithCoder:(NSCoder *)decoder;
- (void) encodeWithCoder:(NSCoder *)encoder;
- (BOOL) isAlive;
- (void) returnToContinuation;
- (void) returnToContinuationWithArg: (void *) ptr;


- (void) _setProcess: p;

#pragma mark -
#pragma mark convenienc method
- (void) postNotificationWithName: (NSString*) name;

@end
GGENDDEC

#define GGHandle (self->handle)

typedef enum
{
  NEXT_FRAME,
  SOME_TIME,
  FOREVER,
  NOP
}time_cond;

NSString * ggbuild_format(NSString *format, ...);
void gg_run_once(void);
void gg_register_continuation(id obj, SEL cont, time_cond val);
void gg_unregister_continuation(id obj);
void gg_init_runtime(void);
void gg_clean(void);
void gg_add_module(id m);


void gg_add_gc(id obj, BOOL release);
void gg_mark(id obj);
void gg_begin_collection(void);

#ifdef DEBUG
void loggc(void);
void loggcconsole(void);
#endif




#ifndef GGBEGINDEC
#define GGBEGINDEC
#endif
#ifndef GGENDDEC
#define GGENDDEC
#endif


#endif
