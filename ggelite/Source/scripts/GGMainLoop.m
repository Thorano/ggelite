/*
 *  GGMainLoop.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: September 2004
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#import <stdio.h>
#import <stdlib.h>
#import <math.h>
#import <time.h>


#import <Foundation/Foundation.h>
#import <Foundation/NSDebug.h>
#import "GGMainLoop.h"
#import "Metaobj.h"
#import "Sched.h"
#import "GGRunTime.h"
#import "utile.h"

GGMainLoop *theLoop = nil;


double deltaTime, realDeltaTime, gameTime, currentTime;
double timeSpeed;
double lastTimeSpeedChange;
BOOL wizard;

@implementation GGLoopState
- (id) init
{
    self = [super init];
    fastScheduler = [Sched new];
    slowScheduler = [Sched new];
    iaScheduler = [Sched new];
    taskSched = [TaskSched new];
    
    return self;
}

- (void) dealloc {
    RELEASE(fastScheduler);
    RELEASE(slowScheduler);
    RELEASE(iaScheduler);
    RELEASE(taskSched);
    
    [super dealloc];
}

-  initWithCoder: (NSCoder *) decoder
{
    self = [super init];
    
    GGDecode(fastScheduler);
    GGDecode(slowScheduler);
    GGDecode(iaScheduler);
    GGDecode(taskSched);
    return self;
}

- (void) encodeWithCoder: (NSCoder *) encoder
{
    GGEncode(fastScheduler);
    GGEncode(slowScheduler);
    GGEncode(iaScheduler);
    GGEncode(taskSched);
}
@end

@implementation GGMainLoop
- init
{
    [super init];
    if( theLoop != nil ){
        NSWarnLog(@"GGLoop should be instanciated only once");
        exit(1);
    }
    theLoop = self;
    
    addToCheck(self);
    state = [GGLoopState new];
    noSaveSched = [TaskSched new];
    timeSpeed = 1.0;
    timeSpeedChange = 0;
    
    gg_init_runtime();
    cont = 1;
    
    return self;
}

// obsolete code.
- initWithArgc: (int) argc
          argv: (char *[]) argv
{
    return [self init];
}

+ new: (int) argc 
  arg: (char *[]) argv
{
    return [[self alloc] initWithArgc: argc
                                 argv: argv];
}

- (void) dealloc
{
    theLoop = nil;
    gg_clean();

    RELEASE(state);
    RELEASE(noSaveSched);
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [super dealloc];
}


- setTimeSpeed: (float) val
{
    if( val != timeSpeed )
    {
        timeSpeedChange = 1;
        timeSpeed = val;
        lastTimeSpeedChange = currentTime;
    }
    return self;
}

- (GGLoopState *)state
{
    return state;
}

- (void) setState: (GGLoopState *)pstate
{
    ASSIGN(state, pstate);
}

- (float) timeSpeed
{
    return timeSpeed;
}

- (double) fps
{
    static double base;
    static double fps = 0;
    static int count = 0;
    
    if( count == 0 )
    {
        base = currentTime;
    }
    if( currentTime - base >= 1.0 )
    {
        fps = (double) count/(currentTime-base);
        count = 0;
        base = currentTime;
    }
    count ++;
    return fps;
}

- (void) stopMainLoop
{
    cont = 0;
}

- (BOOL) isStop
{
    return cont == 0;
}

- (void) incrementGameTime: (double)val
{
    incrementGame = val;
}

- (void) beforeRunningLoop
{
    lastTimeSpeedChange = lastTime = gettimesec();
    gameTime = 0.0;
    moyDeltaTime = 0.05;
    incrementGame = -1;
}

- (void) runTimers
{
    double tp;
    
#ifdef DEBUG
    NSRunLoop *runloop;
    runloop = [NSRunLoop currentRunLoop];
    
    
    if( wizard )
        [runloop runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow: 0.0001]];
#endif
    currentTime = tp = gettimesec();    
    
    //     if( pollMode == WaitForEvent )
    //       {
    // 	lastTime = tp;
    //       }
    
    realDeltaTime = tp - lastTime;
    moyDeltaTime = (moyDeltaTime + 0.05*realDeltaTime) / 1.05;
    if ( realDeltaTime > 0.001 )
    {
        double quot;
        quot = moyDeltaTime / realDeltaTime;
        if( quot > 8 || quot < 0.125 )
        {
            NSDebugMLLog(@"DeltaTime", @"delta adjustment, coef = %f", quot);
            realDeltaTime = moyDeltaTime;
        }
    }
    if(incrementGame > 0)
    {
        deltaTime = incrementGame;
        incrementGame = -1;
    }
    else
        deltaTime = timeSpeed*realDeltaTime;
    gameTime += deltaTime;
    lastTime = tp;
    
    
    [state->fastScheduler launchAll];
    
    if( timeSpeedChange )
    {
        timeSpeedChange = 0;
        [state->slowScheduler launchAll];
    }
    
    //[state->slowScheduler launchAll];
    [state->slowScheduler launch];
    [state->iaScheduler launchAll];
    [state->taskSched run];
    [noSaveSched run];
    gg_run_once();
}


- (Sched *) slowScheduler
{
    return state->slowScheduler;
}

- (Sched *) fastScheduler
{
    return state->fastScheduler;
}

- (Sched *) iaScheduler
{
    return state->iaScheduler;
}

- (TaskSched *) taskSched
{
    return state->taskSched;
}

- (TaskSched *) taskSchedNoSave
{
    return noSaveSched;
}

- (void) slowSchedule: (NSObject *)obj
         withSelector: (SEL) s
                  arg: arg
{
    [state->slowScheduler addTarget: obj
                      withSelector: s
                               arg: arg];
}

- (void) fastSchedule: (NSObject *)obj
         withSelector: (SEL) s
                  arg: arg
{
    [state->fastScheduler addTarget: obj
                      withSelector: s
                               arg: arg];
}

- (void) iaSchedule: (NSObject *)obj
       withSelector: (SEL) s
                arg: arg
{
    [state->iaScheduler addTarget: obj
                    withSelector: s
                             arg: arg];
}

- (void) unSchedule: (NSObject *) obj
{
    [state->fastScheduler deleteTarget: obj];
    [state->slowScheduler deleteTarget: obj];
    [state->iaScheduler deleteTarget: obj];
}

- (BOOL) isScheduled: (NSObject *) obj
{
    return [state->fastScheduler isScheduled: obj] ||
    [state->slowScheduler isScheduled: obj] ||
    [state->iaScheduler isScheduled: obj];
}
@end
