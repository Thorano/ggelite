/*
 *  stdutile.c
 *  elite
 *
 *  Created by Frederic De Jaeger on 9/26/04.
 *  Copyright 2004 __MyCompanyName__. All rights reserved.
 *
 */

#import <math.h>
#import <sys/time.h>
#import <unistd.h>
#import <stdlib.h>
#import <stdio.h>

#include "stdutile.h"

double gettimesec(void)
{
    static double begin;
    static BOOL first = YES;
    double res;
    struct timeval ret;
    if( gettimeofday(&ret, NULL) == 0 )
    {
        res = ret.tv_sec;
        res += (double)(ret.tv_usec)/1e6;
        
        if( first )
        {
            begin = res;
            first = NO;
            return 0.0;
        }
        else
        {
            return res - begin;
        }
    }
    else
    {	
        perror("bug!!");
        return 0.0;
    }
}

/* old version
double vrnd(void)
{
    return(((double)rand())/RAND_MAX);
}
*/

static double maxrandom;
static BOOL second;

double vrnd(void) 
{
    if ( ! second ){
        maxrandom = pow(2.0, 31) - 1;
        second = YES;
    }
    return(((double)random())/maxrandom);
}

void randomize(void)
{
    srandomdev();
}

unsigned irnd(unsigned max)
{
    unsigned res = (unsigned)floor(vrnd()*(max));
    if( res >= max )
        res = max-1;
    return res;
}

BOOL coin(int val)
{
  return irnd(val) != 0;
}



double percent(double x, double y)
{
    double res;
    NSCAssert(y > 0 && x >= 0, @"bad");
    res = 1000.0*x/y;
    res = floor(res);
    return res/10;
}


