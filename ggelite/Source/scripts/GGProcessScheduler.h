//
//  GGProcessScheduler.h
//  elite
//
//  Created by Frédéric on 17/02/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


/*!
    @class
    @abstract    a scheduler of processes.
    @discussion  there is one scheduler per working thread.
*/
@interface GGProcessScheduler : NSObject {
    /* represents the current processes attached to the current thread */
    NSMutableSet*                   _processes;
}

@end
