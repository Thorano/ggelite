//
//  GGReadLine.m
//  elite
//
//  Created by Fr�d�ric De Jaeger on 26/10/04.
//  Copyright 2004 __MyCompanyName__. All rights reserved.
//

#import "GGReadLine.h"


@implementation Complex_readline
- (void) runWith
{
    char buffer[500];
    *buffer = '\0';
    fgets(buffer, 499, stdin);
    NSString* str = [NSString stringWithUTF8String:buffer];
    [self returnToContinuationWithArg:&str];
}

+ readline_
{
    Complex_readline* proc = [self allocInstance];
    [proc runWith];
    return AUTORELEASE(proc);
}
@end
