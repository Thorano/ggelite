/*
 *  stdutile.h
 *  elite
 *
 *  Created by Frederic De Jaeger on 9/26/04.
 *  Copyright 2004 __MyCompanyName__. All rights reserved.
 *
 */

#import "config.h"
#import "GGReadLine.h"

GGBEGINDEC
/*%%%
import GGReadLine ;
%%%*/

double gettimesec(void);    /* donne le temps absolu indépendemment du programme */
double vrnd(void);          /* donne un nombre au hasard dans [0,1]*/
void randomize(void);   /* initialize les *rnd */
unsigned irnd(unsigned max);  /* get an integer a such that 0 <= a <= max-1*/
BOOL coin(int val);  /*  */
double percent(double x, double y);

GGENDDEC
