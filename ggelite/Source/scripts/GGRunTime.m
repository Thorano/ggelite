/* 	-*-ObjC-*- */
/*
 *  GGRunTime.m
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import "GGRunTime.h"
#import <Foundation/Foundation.h>
#import <Foundation/NSObjCRuntime.h>
#import <Foundation/NSException.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSInvocation.h>
#import <Foundation/NSSet.h>
#import "Metaobj.h"
#import "Resource.h"
#import "GGLog.h"

#import "GGSpaceObject.h"
#import "utile.h"

static void gg_save(NSCoder *encoder);
static void gg_restore(NSCoder *decoder);


//#import "ia.h"

/* FIXME:
Here, I put a call to [invalidate] inside a dealloc.
It will work because nobody redefine invalidate in subclasses.
This is a VERY BAD design, because a redefinition of invalidate in
a subclass may refer objects that have already been deallocated.
*/

@implementation ProcessManager
static ProcessManager *pm;
/*
 + (void) initialize
 {
     if ( self == [ProcessManager class] )
     {
         [theResourceManager addForCleaning: self];
     }
 }
 */

+ processManager
{
    if ( pm ) return pm;
    
    pm = [[self alloc] init];
    return pm;
}


+ (void) clean
{
    RELEASE(pm);
    pm = nil;
}

- init
{
    [super init];
    set = [NSMutableSet new];
    return self;
}

- (void) _clearProcesses
{
    id obj;
    NSParameterAssert(set);
    
    while ((obj = [set anyObject]) != nil )
    {
        [obj invalidate];
        //When an object is invalidated, it should remove itself 
        // from this set...
    }
    DESTROY(set);
}

-  initWithCoder: (NSCoder *) decoder
{
    [super dealloc];
    self = [[ProcessManager processManager] retain];
    
    gg_restore(decoder);
    [self _clearProcesses];
    GGDecode(set);
    return self;
}


- (void) encodeWithCoder: (NSCoder *) encoder
{
    gg_save(encoder);
    GGEncode(set);
}

- (void) dealloc
{
    [self _clearProcesses];
    [super dealloc];
}

- (void) add: (ExtendedProcedure *) p
{
    NSParameterAssert(set);
    NSDebugMLLog(@"ProcessManager", @"adding %@", p);
    [set addObject: p];
}

- (void) remove: (ExtendedProcedure *) p
{
    NSDebugMLLog(@"ProcessManager", @"removing %@", p);
    //  NSAssert2([set containsObject: p], @"%@ does not contain %@", self, p);
    [set removeObject: p];
}


- (NSSet *) processes
{
    return set;
}

@end

@implementation ListeningBlock
- initWithCallback: (ExtendedProcedure *) p
{
    [super init];
    timers = [NSMutableArray new];
    callback = p;
    
    addToCheck(self);
    
    return self;
}

- (void) invalidate
{
    callback = nil;
    [MessageManager removeObserver: self];
    [timers makeObjectsPerformSelector: @selector(invalidate)];
    [timers removeAllObjects];
}

- (void) dealloc
{
    //  NSDebugMLLog(@"GGRunTime", @"release %p", self);
    [self invalidate];
    RELEASE(timers);
    [super dealloc];
}

- (id)initWithCoder:(NSCoder *)decoder
{
    GGDecodeObject(callback);
    GGDecode(timers);
    addToCheck(self);
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    GGEncode(callback);
    GGEncode(timers);
}


+ newWithCallback: (ExtendedProcedure *) c
{
    return [[self alloc] initWithCallback: c];
}

- (BOOL) respondsToSelector: (SEL) s
{
    return  [callback respondsToSelector: s];
}

- (NSString *) description
{
    NSString *old = [super description];
    
    return [NSString stringWithFormat: @"%@ : array: %@",
        old, timers];
}

- (NSMethodSignature *) methodSignatureForSelector:(SEL)aSelector
{
    NSMethodSignature* sig = [super methodSignatureForSelector: aSelector];
    
    if ( nil == sig ){
        sig = [callback methodSignatureForSelector: aSelector];
    }
    
    return sig;
}

- (void) forwardInvocation: (NSInvocation *) invocation
{
    ExtendedProcedure *cb = callback;
    NSNotification *not;
    SEL s;
    if( [[invocation methodSignature] numberOfArguments] >= 3 )
    {
        [invocation getArgument: &not atIndex: 2];
        
        NSDebugMLLog(@"GGRunTime", @"self : %@, msg : %@", self, [not name]);
        [invocation getArgument: &s atIndex: 1];
        NSDebugMLLog(@"GGRunTime", @"SEL : %@ for : %@", 
                     NSStringFromSelector(s),
                     cb);
    }
    
    
    /* we need to be alive after the callback destroy us
        so we put ourself on the autorelease pool.
        
        garbage collectors are a nice invention...
        */
    
    RETAIN(self);
    AUTORELEASE(self);
    //  [callback removeListeningBlock: self];
    if ( cb != nil )
    {
        [invocation invokeWithTarget: cb];
        /*prevent recursive invocation, just in case. because I don't know the
        exact semantic of NSNotificationCenter when notifications posts
        notifications*/
    }
}

- (void) addTimer: (GGTimer *)aTimer
{
    NSAssert([timers indexOfObjectIdenticalTo: aTimer] == NSNotFound,
             @"bad");
    [timers addObject: aTimer];
}
@end

@implementation ExtendedProcedure
- init
{
    [super init];
    _process = self;
    stack = [NSMutableArray new];
    addToCheck(self);
    NSParameterAssert(pm);
    [pm add: self];
    return self;
}

+ allocInstance
{
    return [self new];
}

+ (void) initialize
{
    if(self == [ExtendedProcedure class])
    {
        //      [theResourceManager addForCleaning: self];
        [ProcessManager processManager];
    }
}

+ (void) clean
{
    gg_clean();
}

- (void) _setProcess: p
{
    _process = p;
}

- (BOOL) isAlive
{
    return stack != nil;
}

- (BOOL) respondsToSelector: (SEL) s
{
    return  [super respondsToSelector: s] || [handle respondsToSelector: s];
}

- (NSMethodSignature *) methodSignatureForSelector:(SEL)aSelector
{
    NSMethodSignature* sig = [super methodSignatureForSelector: aSelector];
    
    if ( nil == sig ){
        sig = [handle methodSignatureForSelector: aSelector];
    }
    
    return sig;
}


- (void) forwardInvocation: (NSInvocation *) invocation
{
    NSParameterAssert(stack);
    
    if ( handle )
    {
        [invocation invokeWithTarget: handle];
    }
    else
        [NSException raise: NSInvalidArgumentException
                    format: @"ExtendedProcedure %@ does not respond to message %@",
            self, NSStringFromSelector([invocation selector])];
}

- (BOOL) saveChannel
{
    return YES;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    GGDecode(handle);
    GGDecode(stack);
    NSArray* decodedInvocations = [decoder decodeObjectForKey:@"continuations"];
    int i, n = [decodedInvocations count];
    NSAssert(n%2 == 0, @"should have even number of objs in list");
    for(i = 0; i < n/2; ++i){
        id obj = [decodedInvocations objectAtIndex:2*i];
        NSString* selName = [decodedInvocations objectAtIndex:2*i+1];
        NSAssert([selName isKindOfClass:[NSString class]], @"unexpected object type in selector name");
        SEL s = NSSelectorFromString(selName);
        [self registerContinuation:obj at:s];
    }
    addToCheck(self);
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    int i;
    if( stack == nil ) ggabort();
    NSAssert(stack != nil, @"bad");
    GGEncode(handle);
    GGEncode(stack);
    
    
    /* before encoding targets, remove dead one */
    NSMutableArray* arrayToEncode = [[NSMutableArray alloc] initWithCapacity:2*[targets count]];
    if ( targets )
    {
        i=0;
        while(i < [targets count])
        {
            NSInvocation*      p;
            id      o;
            p = [targets objectAtIndex: i];
            NSAssert([p isKindOfClass:[NSInvocation class]], @"p is not an invocation");
            o = [p target];
            NSParameterAssert(o);
            
            if ( [o isKindOfClass: [ExtendedProcedure class]] && ![o isAlive] )
            {
                NSDebugMLLog(@"ExtendedProcedure",
                             @"removing dead %@ before encoding", p);
                [targets removeObjectAtIndex: i];
            }
            else{
                [arrayToEncode addObject:o];
                [arrayToEncode addObject:NSStringFromSelector([p selector])];
                i++;                
            }
        }	
        if([targets count] == 0){
            DESTROY(targets);
        }
    }
    
    [encoder encodeObject:arrayToEncode forKey:@"continuations"];
    [arrayToEncode release];
}

- (void) registerContinuation: obj
                           at: (SEL) fn
{
    if ( stack != nil )
    {
        NSInvocation *inv;
        NSMethodSignature *sig;
        NSParameterAssert(obj != nil);
        if ( !targets )
            targets = [NSMutableArray new];
        
        NSParameterAssert(targets);
        sig = [obj methodSignatureForSelector: fn];
        NSParameterAssert(sig != nil);
        inv = [NSInvocation invocationWithMethodSignature: sig];
        [inv setSelector: fn];
        [inv setArgument: &self
                 atIndex: 2];
        [inv setTarget: obj];
        [inv retainArguments];
        [targets addObject: inv];
    }
}

- (void) returnToContinuation
{
    if ( targets )
    {
        int n;
        NSArray *tab = targets;
        RETAIN(self);
        RETAIN(tab);
        [self killAllBlock];
        if(tab && (n = [tab count]) > 0)
        {
            NSInvocation *objs[n];
            int i;
            if (n > 1)
                NSWarnMLog(@"Multiple dispatch !!");
            
            [tab getObjects: objs];
            for(i = 0; i < n; ++i)
            {
                [objs[i] invoke];
            }
        }
        else
        {
            NSLog(@"impossible");
            ggabort();
        }
        if ( targets == nil )
        {
            NSLog(@"Someone invalidates us, find this asshole!!");
            ggabort();
        }
        [self invalidate];
        RELEASE(tab);
        RELEASE(self);
    }
    
}

- (void) returnToContinuationWithArg: (void *) ptr
{
    if (targets)
    {
        NSEnumerator *en;
        NSInvocation *inv;
        en = [targets objectEnumerator];
        
        while ( (inv = [en nextObject]) != nil )
        {
            [inv setArgument: ptr
                     atIndex: 3];
        }
        [self returnToContinuation];
    }
}

- (void) invalidate
    /*here, we retain before calling pm because, if pm is the only guy to 
    possess us, we might be destroyed after the first of this procedure which is
    bad for the rest
    */
{
    NSDebugMLLog(@"GGRunTimeH", @"invalidated val = %@ handle = %@", self, 
                 handle);
    RETAIN(self);  //just in case...
    [pm remove: self];
    [handle invalidate];
    [MessageManager removeObserver: self];
    gg_unregister_continuation(self);
    [self killAllBlock];
    DESTROY(stack);
    DESTROY(targets);
    RELEASE(self);
}

- (void) dealloc
{
    NSDebugMLLog(@"GGRunTime", @"desalloue %p", self);
    if ( stack )
    {
        NSWarnMLog(@"deallocation of someone not invalidated: %@", self);
        [self invalidate];
    }
    RELEASE(handle);
    RELEASE(stack);
    RELEASE(targets);
    [super dealloc];
}

- (void) markForGC
{}

- (GGTimer *)scheduledTimerForDate: (double)val
                          selector: (SEL) sel
{
    GGTimer *ret;
    if(stack == nil) return nil;
    
    ret = [GGTimer scheduledTimerForDate: val
                                  target: self
                                selector: sel];
    
    [[self currentListeningBlock] addTimer: ret];
    return ret;
}

- (void) sleepDuring: (double) val
            wakeUpAt: (SEL) s
{
    ListeningBlock *bl;
    GGTimer *t;
    
    if(stack == nil) return; 
    bl = [self currentListeningBlock];
    if( bl == nil )
    {
        ListeningBlock *bli;
        bli = [ListeningBlock newWithCallback: self]; 
        [self addListeningBlock: bli];
        bl = bli;
        RELEASE(bli);
    }
    t = [GGTimer scheduledTimerWithTimeInterval: val
                                         target: self
                                       selector: s
                                        repeats: NO];
    [bl addTimer: t];
}

- (void) addListeningBlock: (ListeningBlock *) block
{
    NSDebugMLLog(@"GGRunTime", @"block is %@", block);
    if( stack == nil ) return;
    [stack addObject: block];
}

- (void) popListeningBlock
{
    ListeningBlock *bl;
    if( stack == nil ) return;
    bl = [stack lastObject];
    NSParameterAssert(bl != nil);
    NSDebugMLLog(@"GGRunTime", @"last is %@", bl);
    [bl invalidate];
    [stack removeLastObject];
}

- (void) killAllBlock
{
    if( stack == nil ) return;
    [stack makeObjectsPerformSelector: @selector(invalidate)];
    [stack removeAllObjects];
}

- (void) removeListeningBlock: (ListeningBlock *) block
{
    NSRange range;
    NSArray *tmp;
    int index;
    if( stack == nil ) return;
    
    index = [stack indexOfObjectIdenticalTo: block];
    
    NSDebugMLLog(@"GGRunTime", @"stack = %@\nblock = %@\nindex = %d", 
                 stack, block, index);
    
    NSAssert(index != NSNotFound, @"bad");
    
    range = NSMakeRange(index, [stack count] - index);
    tmp = [stack subarrayWithRange: range];
    [tmp makeObjectsPerformSelector: @selector(invalidate)];
    
    [stack removeObjectsInRange: range];
    
    NSDebugMLLog(@"GGRunTime", @"stack = %@", stack);
}

- (ListeningBlock *) currentListeningBlock
{
    if( stack == nil ) return nil;
    
    return (ListeningBlock *)[stack lastObject];
}

- (void) postNotificationWithName: (NSString*) name
{
    [[NSNotificationCenter defaultCenter] postNotification:
        [NSNotification notificationWithName:name object:_process]];
}

// - retain
// {
//   if ( [self  isKindOfClass: [Complex_Stupid_IA class]] )
//     {
//       puts("retenu");
//     }
//   return   [super retain];

// }

// - (void)release
// {
//   if ( [self  isKindOfClass: [Complex_Stupid_IA class]] )
//     {
//       puts("relache");
//     }
//   [super release];
// }

// - autorelease
// {
//   if ( [self  isKindOfClass: [Complex_Stupid_IA class]] )
//     {
//       puts("autorelache");
//     }
//   return   [super autorelease];

// }

@end

#pragma mark -
#pragma mark utilities

static void myretain(NSHashTable *t, const void * obj)
{ 
    id b = (id)obj;
    RETAIN(b);
}

static void myrelease(NSHashTable *t, void *obj)
{ 
    id b = (id)obj;
    RELEASE(b);
}

static NSString *ggdescribe(NSHashTable *t, const void * obj)
{ 
    id b = (id)obj;
    return [b description];
}

typedef struct __cont_data
{
    id 			obj;
    SEL			cb;
}cont_data;

@protocol Bidon
- (void) initModule;
@end


static NSMutableArray *cont_list_fast;
static NSMutableArray *cont_list_slow;
static NSMutableArray *while_dispaching;
static NSMutableArray *instant_dispaching;
static BOOL dispaching = NO;
static BOOL metadispaching = NO;
static NSHashTable *gc_objects;
static NSHashTable *gc_marked;

static BOOL init_done = NO;

static id *modules;
static int size_m;
static int n_module;

static NSHashTableCallBacks hashcb = 
{ NULL, NULL, myretain, myrelease, ggdescribe };
/*
 static NSHashTableCallBacks hashnoretain = 
 { 
     NULL, 
     NULL, 
     NULL, 
     NULL, 
     ggdescribe };
 */
static NSValue *valueFromCont(id obj, SEL cb)
{
    cont_data data;
    
    data.obj = obj;
    data.cb = cb;
    
    return [NSValue value: &data
             withObjCType:	@encode(cont_data)];
}

NSString* ggbuild_format(NSString *format, ...)
{
    NSString *str;
    va_list ap;
    
    va_start (ap, format);
    
    str = [[NSString alloc] initWithFormat: format
                                 arguments: ap];
    AUTORELEASE(str);
    va_end (ap);
    
    return str;
}

void initModule(NSString *moduleName)
{
    id mod = NSClassFromString(moduleName);
    NSCAssert1(mod, @"cannot find module %@", moduleName);
    [mod initModule];
}

void gg_init_runtime(void)
{
    cont_list_slow = [NSMutableArray new];
    cont_list_fast = [NSMutableArray new];
    while_dispaching  = [NSMutableArray new];
    instant_dispaching = [NSMutableArray new];
    gc_objects = NSCreateHashTable(hashcb, 100);
    gc_marked = NSCreateHashTable(hashcb, 100);
    
    if (modules && n_module > 0)
    {
        int i;
        NSDebugLLog(@"GGRuntime", @"Some modules require initialisation");
        //It doesn't work ?
        //      [modules makeObjectsPerformSelector: @selector(initModule)];
        
        for(i = 0; i < n_module; ++i)
        {
            NSDebugLLog(@"GGRuntime", @"Initialization of %@", modules[i]);
            initModule(modules[i]);
            //	  [modules[i] initModule];
        }
        free(modules);
        modules = NULL;
    }
    else
    {
        NSLog(@"No modules? %@", modules);
    }
    init_done = YES;
}

void gg_add_module(id o)
{
    if ( init_done )
    {
        initModule(o);
        NSLog(@"Adding %@", o);
    }
    
    else
    {
        if ( modules == NULL )
        {
            size_m = 10;
            modules = malloc(size_m * sizeof(id));
            n_module = 0;
        }
        
        else  if (n_module == size_m )
        {
            size_m *= 2;
            modules = realloc(modules, size_m * sizeof(id));
        }
        
        modules[n_module++] = o;
        //      NSLog(@"adding module %@", o);
    }
}

void gg_clean(void)
{
    DESTROY(cont_list_fast);
    DESTROY(cont_list_slow);
    DESTROY(while_dispaching);
    DESTROY(instant_dispaching);
    
    NSFreeHashTable(gc_objects);
    gc_objects = NULL;
    NSFreeHashTable(gc_marked);
    gc_marked = NULL;
    [ProcessManager clean];
}

void gg_register_continuation(id obj, SEL cont, time_cond val)
{
    if ( [obj isKindOfClass: [ExtendedProcedure class]] )
    {
        ExtendedProcedure *o = (ExtendedProcedure *)obj;
        
        if( o->stack == nil )
        {
            NSWarnLog(@"%@ has been inactivated", obj);
            return;
        }
    }
    
    switch(val)
    {
        case NEXT_FRAME:
            if (dispaching)
            {
                //	  NSDebugLLog(@"GGRunTime", @"adding %@ while dispaching", obj);
                [while_dispaching addObject: valueFromCont(obj, cont)];
            }
            else
            {
                [cont_list_fast addObject: valueFromCont(obj, cont)];
            }
            break;
        case SOME_TIME:
            [cont_list_slow addObject: valueFromCont(obj, cont)];
            break;
        case FOREVER:
            break;
        case NOP:
            [instant_dispaching addObject: valueFromCont(obj, cont)];
            break;
        default:
            NSCAssert(0, @"bad");
            ggabort();
    }
}

static void my_remove(NSMutableArray *array, id obj)
{
    int i;
    
    i=0;
    while(i < [array count])
    {
        NSValue *val;
        cont_data data;
        val = [array objectAtIndex: i];
        
        [val getValue: &data];
        
        if ( data.obj == obj )
            [array removeObjectAtIndex: i];
        else
            i++;
    }
}



void gg_unregister_continuation(id obj)
{
    my_remove(cont_list_slow, obj);
    my_remove(cont_list_fast, obj);
    my_remove(instant_dispaching, obj);
    my_remove(while_dispaching, obj);
}

void gg_run_once(void)
{
    /* the code should be reentrant
    someone could add a continuation when we execute another one
    so we have to take care of everything
    */
    
    int n;
    
    dispaching = YES;
    while ( (n = [cont_list_fast count]) > 0)
    {
        NSValue *val;
        cont_data data;
        
        /*order is very important below because of reentrancy on
            the array cont_list_fast.
            
            We take objects from the last, otherwise it's quadratic !!!
            */
        val = [cont_list_fast lastObject];
        [val getValue: &data];
        [cont_list_fast removeLastObject];
        
        [(NSObject *) data.obj performSelector: data.cb];
    }
    
    metadispaching = YES;
    while( [instant_dispaching count] > 0)
    {
        NSValue *val;
        cont_data data;
        
        /*order is very important below because of reentrancy on
            the array cont_list_fast.
            
            We take object from the last, otherwise it's quadratic !!!
            we read val before we get it's value.  it looks safe.
            
            */
        val = [instant_dispaching lastObject];
        [val getValue: &data];
        [instant_dispaching removeLastObject];
        [(NSObject *) data.obj performSelector: data.cb];
    }
    metadispaching = NO;
    
    [cont_list_fast setArray: while_dispaching];
    [while_dispaching removeAllObjects];
    dispaching = NO;
    
    if ( [cont_list_slow count] > 0 )
    {
        NSValue *val;
        cont_data data;
        
        /*order is very important below because of reentrancy on
            the array cont_list_slow
            */
        val = [cont_list_slow objectAtIndex: 0];
        [val getValue: &data];
        [cont_list_slow removeObjectAtIndex: 0];
        [(NSObject *) data.obj performSelector: data.cb];
    }
    
    NSDebugLLogWindow(@"GGRunTime", @"%@\n%@", cont_list_fast, cont_list_slow);
    
#ifdef DEBUG
    loggc();
#endif
}

static void save(NSArray *array, NSCoder *encoder, NSString* baseName)
{
    int n = [array count];
    [encoder encodeInt:n forKey:[NSString stringWithFormat:@"%@.n", baseName]];
    if(n > 0)
    {
        NSMutableArray *storage = [NSMutableArray new];
        NSValue *tabs[n];
        int i;
        [array getObjects: tabs];
        
        for(i = 0; i < n; ++ i)
        {
            cont_data data;
            
            [tabs[i] getValue: &data];
            
            
            
            NSCParameterAssert(data.obj != nil && data.cb != NULL);
            
            [storage addObject:data.obj];
            [storage addObject:NSStringFromSelector(data.cb)];
        }	
        [encoder encodeObject:storage forKey:[NSString stringWithFormat:@"%@.store", baseName]];
        [storage release];
    }
}

static void decode(NSMutableArray *array, NSCoder *decoder, NSString* baseName)
{
    int n;
    n = [decoder decodeIntForKey:[NSString stringWithFormat:@"%@.n", baseName]];
    NSArray* storage = [decoder decodeObjectForKey:[NSString stringWithFormat:@"%@.store", baseName]];
    
    NSCAssert(2*n == [storage count], @"bad");
    [array removeAllObjects];
    
    if( n > 0 )
    {
        int i;
        for(i = 0; i < n; ++i)
        {
            cont_data data;
            NSValue *val;
            
            data.obj = [storage objectAtIndex:2*i];
            data.cb = NSSelectorFromString([storage objectAtIndex:2*i+1]);
            
            val = [NSValue value: &data
                    withObjCType:	@encode(cont_data)];
            [array addObject: val];
        }
    }
}


static void gg_save(NSCoder *encoder)
{
    NSCParameterAssert(dispaching == NO);
    NSCParameterAssert(metadispaching == NO);
    save(cont_list_slow, encoder, @"slow");
    save(cont_list_fast, encoder, @"fast");
    save(instant_dispaching, encoder, @"instant");
}

static void gg_restore(NSCoder *decoder)
{
    NSCParameterAssert(dispaching == NO);
    NSCParameterAssert(metadispaching == NO);
    decode(cont_list_slow, decoder, @"slow");
    decode(cont_list_fast, decoder, @"fast");
    decode(instant_dispaching, decoder, @"instant");
}  


/*Garbage collector stuffs
*/

/*This is a very primitive GC.  It's slow and consumes a lot of resource.  
Hope it will be upgrade soon.
*/

void gg_add_gc(id obj, BOOL release)
{
    if( obj == nil ) return;
    NSCParameterAssert(gc_objects);
    
    NSDebugLLog(@"GC", @"we add %p \"%@\":%@, already there? %d, release = %d", obj, obj,
                [obj class],   NSHashGet(gc_objects, obj) != NULL, release);
    
    NSHashInsertIfAbsent(gc_objects, obj);
    if (release)
        RELEASE(obj);
}

void gg_begin_collection(void)
{
    NSHashTable *tmp;
    ExtendedProcedure *p;
    NSSet *processes;
    NSEnumerator *en;
    NSCParameterAssert(gc_marked && NSCountHashTable(gc_marked) == 0);
    
    processes = [[ProcessManager processManager] processes];
    
    NSCParameterAssert(processes);
    
    en = [processes objectEnumerator];
    
    /* first, we mark  */
    while ((p = [en nextObject]) != nil)
    {
        //      if( NSHashGet(gc_marked, p) == NULL)
        [p markForGC];
    }
    
    /* now, sweep */
    NSResetHashTable(gc_objects);
    tmp = gc_objects;
    gc_objects = gc_marked;
    gc_marked = tmp;
}

void gg_mark(id obj)
{
    if(obj == nil) return;
    
    NSHashInsertIfAbsent(gc_marked, obj);
}


#ifdef DEBUG
void loggc(void)
{
    NSHashEnumerator en;
    NSEnumerator *e;
    id obj;
    NSSet *set;
    
    en = NSEnumerateHashTable(gc_objects);
    
    while ((obj = NSNextHashEnumeratorItem(&en)))
    {
        NSDebugLLogWindow(@"GC", @"%p: %@ retainedCount = %d", obj, [obj class],
                          [obj retainCount]);
    }
    NSEndHashTableEnumeration(&en);
    
    
    if ( GSDebugSet(@"GCH") )
    {
        set = [[ProcessManager processManager] processes];
        e = [set objectEnumerator];
        
        while ((obj = [e nextObject]) != nil )
        {
            NSDebugLLogWindow(@"GCH", @"process : %@ state %s", 
                              obj, ([obj isAlive] ? "alive" : "dead"));
        }
    }
}

void loggcconsole(void)
{
    NSHashEnumerator en;
    id obj;
    
    en = NSEnumerateHashTable(gc_objects);
    
    while ((obj = NSNextHashEnumeratorItem(&en)))
    {
        NSDebugLLog(@"GC", @"%p: %@ retainedCount = %d", obj, [obj class],
                    [obj retainCount]);
    }
    NSEndHashTableEnumeration(&en);
}
#endif
