/* DO NOT EDIT !!!
 * This file is automatically generated from the file 
ia.gg
 * modify this file instead
 */


#ifndef __ia__h
#define __ia__h
#include "GGRunTime.h"


#include "autopilote.h"

GGBEGINDEC
/*%%%
import autopilote ;
%%%*/

GGENDDEC

#include "World.h"

GGBEGINDEC
/*%%%
import World ;
%%%*/

GGENDDEC

#include "utile.h"

GGBEGINDEC
/*%%%
import utile ;
%%%*/

GGENDDEC

#include "Station.h"

GGBEGINDEC
/*%%%
import Station ;
%%%*/

GGENDDEC

#include "Clouds.h"

GGBEGINDEC
/*%%%
import Clouds ;
%%%*/

GGENDDEC

#include "System.h"

GGBEGINDEC
/*%%%
import System ;
%%%*/

GGENDDEC

GGBEGINDEC
/*%%%
extern complex go_to(ship : GGShip, station : Station, arrival_date : Float):Unit;
%%%*/
GGENDDEC

/*Complex function from go_to  compiled as a class
 */
@interface Complex_go_to : ExtendedProcedure
{  


}
- (void) runWitharg1: (GGShip *) ship
                   arg2: (Station *) station
                   arg3: (double ) arrival_date;
+ go_to_ship: (GGShip *) ship
                                                   station: (Station *) station
                                                   arrival_date: (double ) arrival_date;
@end


GGBEGINDEC
/*%%%
extern complex hyperJump(ship : GGShip, target : Int):Float;
%%%*/
GGENDDEC

/*Complex function from hyperJump  compiled as a class
 */
@interface Complex_hyperJump : ExtendedProcedure
{  

     Tunnel *(__tunnel_190);
     double (__save_time_192);
     ListeningBlock *(__block_195);
     int (__target_185);
     GGShip *(__ship_186);
     double (__return_val_188);

}
- (void) runWitharg1: (GGShip *) ship
                   arg2: (int ) target;
+ hyperJump_ship: (GGShip *) ship
                                          target: (int ) target;
@end


GGBEGINDEC
/*%%%
extern complex Continue_IA(ship : GGShip, arrival_date : Float, jump : Bool):Unit;
%%%*/
GGENDDEC

/*Complex function from Continue_IA  compiled as a class
 */
@interface Complex_Continue_IA : ExtendedProcedure
{  

     id (__s_name_214);
     BOOL (__jump_204);
     double (__arrival_date_205);
     BOOL (__flag_216);
     GGShip *(__ship_206);
     ListeningBlock *(__block_217);

}
- (void) runWitharg1: (GGShip *) ship
                   arg2: (double ) arrival_date
                   arg3: (BOOL ) jump;
+ Continue_IA_ship: (GGShip *) ship
                                         arrival_date: (double ) arrival_date
                                         jump: (BOOL ) jump;
@end


GGBEGINDEC
/*%%%
extern complex Stupid_IA(ship : GGShip):Unit;
%%%*/
GGENDDEC

/*Complex function from Stupid_IA  compiled as a class
 */
@interface Complex_Stupid_IA : ExtendedProcedure
{  


}
- (void) runWitharg1: (GGShip *) ship;
+ Stupid_IA_ship: (GGShip *) ship;
@end

#endif
