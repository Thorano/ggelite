#ifndef __ggmath_h
#define __ggmath_h

/*we don't want any c-prog imports those silly decs*/

#import "GG3D.h"

#if 0

GGBEGINDEC

double sqrt(double);
double sin(double);
double cos(double);
double fabs(double);

GGENDDEC
#endif

static inline void my_mulScalVect(GGReal x, Vect3D *v, Vect3D *res)
{
  mulScalVect(v,x,res);
}

#endif
