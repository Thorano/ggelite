extern now : Float = "gameTime";

import GGLoop;
import types;
import GG3D;
import GGShip;
import Sched;
import utile;
import GGNode;

import ggmath;
import World;


type ModePilotageAuto = 
enum {
  NothingYet,
  Accelerating,
  Freining,
  OutSideSystem
}

type GGTypeAutoPilote =   
enum 
  {
    GGModeMissile,
    GGModeVaisseau
}	

type Status =
enum
  {
    Success,
    TargetLost,
    AtDestination,
    Failure_div,
    No_target
  }

type Pilote_State =
enum
  {
    LocalState,
    NonLocalState
  }


const Konst : Float = 1.0;
const DistCont : Float = 20.0;
const MaxTangente : Float = 3.0;
const CoefSecu : Float =1.0;
const PowerOptimale : Float = 0.9;
const CorrectionPower : Float = 1.5;

function  intervalForDestination(target : GGSpaceObject,
				     ship : GGShip) : Float

{
  if( ship == nil || target == nil )
    return -1.0;
  else
    {
      let gal : PetitGalileen;
      let d : Float;
      let delta : Float;
      [target positionSpeedRelativesTo: ship
	      decal: NULL
	      in: gal];
      d := normeVect(gal.point);
      
      delta := sqrt(2.0*d/GACC(ship));
      return delta;
    }
}


function Autopilot(_target : Id,
		   ship : GGShip,
		   _decal : Vect3D,
		   speed : Float,
		   distanceTarget : Float,
		   typ : GGTypeAutoPilote) : Status
{
  if( _target == nil ) return No_target;

  let internal_state : Pilote_State = NonLocalState;
  assert(_target <> nil && ship <> nil);

  let target : GGSpaceObject = lookupObjet(_target);

  let modeFlight : ModePilotageAuto = NothingYet;
  let admissibleSpeed : Float;

  let decal : Vect3D = zero;

  if( speed >= 0.0)
    admissibleSpeed := speed;
  else    
    admissibleSpeed := 0.;


  debug("typ = ", typ, " for ", ship, " to ", _target);

  if (typ == GGModeMissile )
    {
      if ( target <> nil && distanceTarget < 0.0 )
	distanceTarget := Dimension(target);
      CollisionCounter(ship) := 1000;
    }
  else
    {
      if (target <> nil && distanceTarget < 0.0)
	distanceTarget := [target proximite];
      decal := _decal;
    }

  debug("init, decal = ", decal);

  if (distanceTarget < 0.0)
    distanceTarget := 0.0;

  let signature <owner> : Id = nil;
  let timeOfLeaving : Float = 0.0;

  wait next frame;

  loop <at_target_point; my_timer>
    {
      loop {
	internal_state := NonLocalState;
	debug("locality : ", [ship locality], 
	      " tracked : ", ship.tracked,
	      " activated : ", ship._flags.activated   );
	if ( [ship locality] <> 0 )	
	  {
	    if (target == nil )
	      sleep;
	    internal_state := LocalState;
	  
	    if ( ship._flags.activated <> 0 )
		while(true)
		{
		  wait next frame;
		  assert(target <> nil);
		  let gal : PetitGalileen;
		  [target positionSpeedRelativesTo: ship
			  decal: decal
			  in: gal];
		  gal.speed := (-1.0) * gal.speed;
		  let norm : Float = sqrt(gal.point * gal.point);
		  let distCible : Float = norm;

		  if ( distCible < distanceTarget )
		    {
		      [ship stop];
		      return AtDestination;
		    } 
		  (*Unit vector*)
		  let i : Vect3D = gal.point / norm ; 
		  let vScalI : Float = i * gal.speed;
		  let vit : Float = sqrt(gal.speed * gal.speed);
		  let veff : Float;
		
		  if( vit > EPS ) (*je panne rien à ce code...*)
		    {
		      if( vScalI > 0.0 ) (*je vais plus ou moins vers ma cible *)
			{
			  let cosa : Float
			    and sina : Float	(*angle between speed and direction*)
			    and  HM: Float
			    and OH: Float;

			  reschedule_timer_for_date ( now + distCible / 
						      (2.0*vScalI) + 1.0; my_timer);

			  cosa := vScalI / vit ;
			  if( cosa <= 1. )
			    sina := sqrt(1.-cosa*cosa);
			  else
			    {
			      cosa := 1.;
			      sina := 0.;
			    }
			  HM := sina*distCible;
			  let tol : Float;
			  if( timeSpeed >= 1000.0 )
			    tol := 10.0;
			  else	
			    tol := 1.0;
			  if( HM < distanceTarget * tol )
			    (* the trajectory will encounter the target sometime in the future*)
			    {
			      let delta:Float = sqrt(distanceTarget*distanceTarget - HM*HM);
			      let OC : Float;
			      OH := cosa*distCible;
			      OC := OH - delta;
			      if( OC  < deltaTime * vit )
				{
				  (* we will touch the target before next frame *)
				  let timeEff:Float = OC/vit;
				  debug("risque de collision ", HM, " ", distanceTarget);
				  debug("OH = ", OH, "\nOC = ", OC, " delta = ", delta);
				  debug("target hit");
				  Point(ship) := Point(ship) + (OC/vit) * gal.speed;
				  if( typ == GGModeMissile )
				    {
				      let dif : Vect3D = Point(target) - Point(ship);
				      debug("distance ", sqrt(dif * dif));
				    }
				  else
				    {
				      if( vit < (GFREIN(ship)) * timeEff * tol)
					{
					  debug("make corrections");
					  Speed(ship) := Speed(ship) - gal.speed;
					}
				      else	
					Speed(ship) := Speed(ship) - 
					  (timeEff*GFREIN(ship)/vit) * gal.speed;
				    }	
				  (*		      [self notifyArrival]; *)
				  [ship stop];
				  return AtDestination;
				}
			    }
			}
		      else
			reschedule_timer_for_date ( distant_future ; my_timer);
		    
		      veff := vScalI;
		    }
		  else 
		    {
		      vScalI := 0.;
		      vit := 0.;
		      veff := 0.;
		    }

		  let sens : Float;

		  match typ with
		    {	
		      GGModeMissile ->
			{	
			  debugw("Mode Missile");
			  if( modeFlight <> Accelerating )
			    {	
			      debug("ModeMissile -> Acceleration");
			      [ship stop];
			      [ship accelere];
			      modeFlight := Accelerating;
			      [ship setPower: 1.0];
			      debug("fuel : ", ship.carburant);
			    }
			  sens := 1.0;	
			}
	
		      | GGModeVaisseau ->
			{	
			  let gAccObj  : Float = GACC(ship);
			  let gFreinObj : Float = GFREIN(ship);
			  distCible := distCible - distanceTarget;
      
			  debugw("obj proximite = ", [target proximite], ", ",
				 distanceTarget);
			  debugw("Mode Vaisseau");
			  debugw("veff= ", veff, " distCible= ", distCible,
				 "\n admissibleSpeed=", admissibleSpeed);
			  let dmax : Float = (veff*veff - admissibleSpeed*admissibleSpeed )/
			    (2.0*CoefSecu*gFreinObj);
                          (* add some histeresis. *)
                          let cond : Bool = 
                            modeFlight == Accelerating && dmax < 0.8*distCible||
                            modeFlight <> Accelerating && dmax < 0.7*distCible;
			  if ( veff < admissibleSpeed || cond )
			    {	
			      debugw("Acceleration dmax = ", dmax);
			      if( modeFlight <> Accelerating )
				{
				  debug("ModeVaisseau -> Acceleration");
				  modeFlight := Accelerating;
				  [ship stop];
				  [ship accelere];
				  [ship setPower: 1.0];
				  debug("ship carburant :", ship.carburant);
				}
			      sens := 1.0;
			    }
			  else (* dmax has just been calculated!!*)
			    {
			      (* should be greater than 0.7*CoefSecu *)
			      let power : Float = CoefSecu* dmax/distCible;

			      debugw("Freinage power : ", power);


			      power := (power - PowerOptimale) * 
				CorrectionPower+PowerOptimale;

			      debugw("real power : ", power);
			      debugw("dmax = ", dmax);
			      if( modeFlight <> Freining )
				{
				  debug("-> Freinage");
				  [ship stop];
				  [ship decelere];
				  modeFlight := Freining;
				}
			      [ship setPower: power];
			      sens := -1.0;
			    }
			}
		      + -> 
			{	
			  sens := 1.0;
			  log("tres bizarre :", typ);
			}
		    }

		  let ok : Bool = true;
		  let tmp : Vect3D;
		  let j : Vect3D = zero;
		  let theTant : Float;

		  if( fabs(vScalI) < EPS * vit )
		    (* $ <V, i> est presque droit
		     *)
		    {
		      debugw("V.i \\simeq 0");
		      theTant := -sens*MaxTangente;
		    }
		  else
		    {
		      if( vScalI <= 0.0 )
			{
			  debugw("missile part dans le mauvais sens");
			  (*      [obj setWishDirection: &i]; *)
			  ok := true;
			}
		      tmp := i ^ gal.speed;
		      debugw("tmp = ", tmp);
		      if( tmp * tmp <= vit*EPS )
			{
			  debugw("direction quasi optimale");
			  [ship setWishDirection: i
				speedManche:0.01];
			  continue;
			}
		      j := tmp ^ i;
		      normaliseVect(j);
		      let tant : Float = (j * gal.speed)/(i * gal.speed);
		      tant := fabs(tant);
		      (*        tant = -Konst*sqrt(tant); *)
		      let tmpd : Float = sqrt(tant);
		      tant := -Konst*tmpd*sqrt(tmpd);
		      if( tant < -MaxTangente )
			tant := -MaxTangente;
		      theTant := sens*tant;
		    }

		  debugw("tant = ", theTant);
		  let res : Vect3D = i + theTant * j;
		  if( ok )
		    [ship setWishDirection: res
			  speedManche: fabs(theTant)];
		  else
		    [ship setWishDirection: res
			  speedManche: 1.0];
		  debugw("res =", res);
		  debugw(" power : ", ship.power, " flag : ", ship.modeInertie);
		  debugw("fuel : ", ship.carburant);
		}
	    else
	      {
		let lastTime : Float = -1.0;
		let dateAtDest : Float = -1.0;
		while(true)
		  {
		    wait some time;	
		  
		    if( lastTime > 0.0 )
		      {	
			let pg : PetitGalileen;	
			assert(dateAtDest > 0.0);
			assert(lastTime < dateAtDest);
			[target  positionSpeedRelativesTo: ship
				 decal: NULL
				 in: pg];
			Point(ship) := Point(ship) +
			  ((now - lastTime)/
			   ([my_timer date]-lastTime)) * pg.point;
		      }
		    else
		      {
			dateAtDest := now + 
			  intervalForDestination(target, ship);
			assert(dateAtDest > 0.0);
			reschedule_timer_for_date(dateAtDest ; my_timer);
			debug("we init the timer: ", dateAtDest,
			      "\nnow its ", now);

		      }

		    lastTime := now;
		  }
	      }
	  }
	else
	  {
	    internal_state := NonLocalState;
	    reschedule_timer_for_date(now + 1e4; my_timer);
	    sleep;
	  }
      }
      while listening {
	+ GGNotificationShipBecomeActivated from ship -> 
	  {
	    debug(ship, " becomes activated");
	    break;
	  }
	+ GGNotificationShipBecomeInactivated from ship -> 
	  {
	    debug(ship, " becomes inactivated");
	    break;
	  }
	+ GGNotificationLeavingSystem -> 
	  {
	    debug("leaving system");
	    if (modeFlight <> OutSideSystem)
	      {
		modeFlight := OutSideSystem;
		timeOfLeaving := now;
		signature := [target signature];
		if (signature <> nil)
		  {
		    target := nil;
		  }
		if( [my_timer date] == distant_future )
		  reschedule_timer_for_date (now + 1e5; my_timer);
	      }
	    debug("modeFlight = ", modeFlight);
	    break;
	  }
	+ GGNotificationEnteringSystem(Name:Int) -> 
	  {
	    assert(modeFlight == OutSideSystem);
	    debug(ship, " is here, Dude!");
	    if( [ship indexSystem] == Name )
	      (* So we are back in our system
	       *)
	      {
		(* we recover the target*)
		modeFlight := NothingYet;
		debug("target = ", target);
		if( target == nil && signature <> nil )
		  {
		    target := [signature objectFromSignature];
		    if( target == nil )
		      {	
			debug("target is now nil!!, sig was ", signature);
			return Failure_div;
		      }
		  }

		assert(target <> nil);
		(* we position the ship *)

		let pg : PetitGalileen;
		if( [my_timer date] > timeOfLeaving + EPS)
		  {
		    [target positionSpeedRelativesTo: ship
			    decal: NULL
			    in: pg];
		    Point(ship) := Point(ship) + 
		      ((now - timeOfLeaving)/
		       ([my_timer date]-timeOfLeaving)) * pg.point;
		  }
	      }
	    else
	      debug(ship, " is not in current system:", [ship locality],
		    " modeFlight = ", modeFlight);
	   break;
	  }
      }
    }
  while listening
    {
      + GGNotificationShipDoHyperJump from target  -> 
	{
	  log("target left system :", target);
	  [ship stop];
	  return TargetLost;
	}
      + GGNotificationShipDoHyperJump from ship -> return TargetLost;
      + GGNotificationVanishing from target -> 
	{
	  debug(target, " is destroyed");
	  [ship stop];
	  return TargetLost;
	}
      + GGNotificationVanishing from ship -> 
	{
	  debug(ship, " is destroyed here...");
	  return Failure_div;
	}
      + GGNotificationShipEnteredStation from target  ->
	{
	  debug(ship, " lost target because of entering station, I presume");
	  [ship stop];
	  return TargetLost;
	}
      * at_target_point -> 
	{
	  debug("timer expired for ", ship, " to ", _target, "\ninternal_state ",
		internal_state);
	  [ship stop];
	  if( internal_state == LocalState && 
	      typ == GGModeVaisseau (*&&
				      [ship locality] == 2*))
	    {
	      assert(target <> nil);
	      let pg : PetitGalileen;
	      [target positionSpeedRelativesTo: ship
		      decal: decal
		      in: pg];
	      let unit : Vect3D = pg.point;
	      normaliseVect(unit);
	      let newdecal : Vect3D = decal - distanceTarget * unit; 
 	      debug("distanceTarget = ", distanceTarget,
 		    "\ndecal = ", newdecal, "\nolddecal = ", decal);
	      pg.point := pg.point + newdecal;
	      debug("abs_pos = ", [ship getPositionAbsolu],
		    "\ntarget_pos = ", [target getPositionAbsolu]);
	      [ship translate: pg
		    refering: target
		    decal: newdecal];
	      debug("target_pos = ", [target getPositionAbsolu]);
	      debug("distance = ", distanceAbsolu(target, ship));
	      if( [ship tryLocalCoordinatesIfPossible] )
		{
		  debug("on le replace!!");
		  Point(ship) := Point(target) - newdecal;
		}
	      else
		{
		  debug("locality = ", [ship locality]);
		  debug("Pos local target = ", Point(target));
		  (*		  debug("dist orig = ", distanceAbsolu(ship, [target nodePere])); *)
		}
	    }
	  break;
	}
    }	
  [ship tryLocalCoordinatesIfPossible];
  return AtDestination;
}


(*Move the ship until it's orientation is dir.  dir is expressed in the global
  frame system.  It assumes that the ship will remain local during all the 
  movement.  Otherwise, it exits cleanly.
*)
function TurnToPoint(ship : GGShip, dir : Vect3D) : Unit
{
  loop
    {
      wait next frame;
      let newdir : Vect3D = [[ship nodePere] rotateDirection: dir
					     fromFrame: theWorld];

      normaliseVect(newdir);
      let prod : Vect3D = newdir ^ Direction(ship);
      let val : Float = prod * prod;
      debugw("val = ",val);
      if( val < 0.001 )
	return;

      [ship setWishDirection: newdir
	    speedManche: sqrt(val)];
    }
  while listening
    {
      + GGNotificationVanishing from ship -> return;
      + GGNotificationWishedDirection from ship -> 
           {
	     log("success!");
	     break;
	   }
      + GGNotificationShipBecomeInactivated from ship -> 
	{
	  log("ou lala");
	  break;
	}
      + GGNotificationLeavingSystem -> 
	{
	  log("blurbs");
	  break;
	}
    }
  (* we wait a little bit because we may be in a half state (between an
     hyper jump)*)
  wait next frame;
}

(*we say that this always succeed.  So we return Unit. *)

function TurnToDest(ship : GGShip, _station <owner> : NSString) : Unit
{
  let station : GGSpaceObject = lookupObjet(_station);
  assert(station <> nil);

  loop
    {
      wait next frame;
      let pg : PetitGalileen;
      [station positionSpeedRelativesTo: ship
	       decal: zero
	       in: pg];

      normaliseVect(pg.point);
      let prod : Vect3D = pg.point ^ Direction(ship);
      let val : Float = prod * prod;
      debugw("val = ",val);
      if( val < 0.001 )
	return;

      [ship setWishDirection: pg.point
	    speedManche: sqrt(val)];
    }
  while listening
    {
      + GGNotificationVanishing from ship -> return;
      + GGNotificationWishedDirection from ship -> {log("success!");break;}
      + GGNotificationShipBecomeInactivated from ship -> {log("ou lala");break;}
      + GGNotificationLeavingSystem -> {log("blurbs");break;}
    }
  (* we wait a little bit because we may be in a half state (between an
     hyper jump)*)
  wait next frame;
}

function Flight_to_station(_station <owner> : NSString, ship : GGShip) : Status

{
  let station : GGSpaceObject = lookupObjet(_station);
  [ship stop];
  do
    {
      if( station <> nil )
	{
	  let mystatus : Status = Autopilot(_station, 
					  ship, 
					  2e4 * Direction(station), 
					  -1.0, 100.0, GGModeVaisseau);
	  if ( mystatus == AtDestination )
	    {
	      if ( ship._flags.locality  == 2)
		{
		  if( [ship isCommandant] )
		    [theLoop setTimeSpeed: 1.0];

		  TurnToDest(ship, _station);
	  
		  return Autopilot(_station, 
				   ship, 
				   zero, -1.0, 100.0, GGModeVaisseau);
		}
	    }
	  else
	    {
	      log("probleme");
	      return Failure_div;
	    }
	}
      else
	{
	  log(_station, " is not local");
	  return Autopilot(_station, 
			   ship, 
			   zero, -1.0, 100.0, GGModeVaisseau);
	}
    }
  while listening
    {
      + GGNotificationVanishing from ship -> return Failure_div;
    }
}

