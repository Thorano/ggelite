//
//  GGLog.m
//  elite
//
//  Created by Frédéric De Jaeger on 29/12/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGLog.h"
#import "Tableau.h"
#import "GGFont.h"
#import "config.h"
#if DEBUG_COCOA
#import "GGDebugController.h"
#endif

@implementation GGLog
static GGLog* sLogger;
+ (void)logFunction:(const char *)func 
     fileName:(const char *)fileName 
     lineNo:(unsigned)lineNo
     format:(NSString*)format, ...
{
    if(sLogger){
        va_list list;
        va_start (list, format);

        [sLogger logFunction:func fileName:fileName lineNo:lineNo format:format arguments:list];
        va_end (list);
    }
}

+ (void) setLogger:(GGLog*)logger
{
    ASSIGN(sLogger, logger);
}

- (id)init {
    self = [super init];
    if (self) {
        _logArray = [StringArray new];
        [_logArray setFont:[GGFont fontWithFile:DefaultFontName atSize:12]];
    }
    return self;
}

- (void)logFunction:(const char *)func 
     fileName:(const char *)file 
     lineNo:(unsigned)lineNo
     format:(NSString*)format
     arguments:(va_list)list
{
    NSString *fileName = [NSString stringWithUTF8String: file];
    fileName = [fileName lastPathComponent];
    NSString* tmp = [[NSString alloc] initWithFormat:format arguments:list];
    NSString *message = [[NSString alloc] initWithFormat:@"File %@: %d. In %s:\n%@",
        fileName, lineNo, func, tmp];
    [tmp release];
    [_logArray addString: message];
    [message release];
}

- (void) logAndCleanAtPoint:(NSPoint)point;
{
#if DEBUG_COCOA
    [[GGDebugController controller] setLogContent:[_logArray content]];
    [_logArray clear];
#else
    if( ! [_logArray isEmpty] )
    {
        glPushAttrib(GL_ENABLE_BIT);
        glDisable(GL_ALPHA_TEST);
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_FOG);
        glDisable(GL_LIGHTING);
        [_logArray displayAt: point.x and: point.y];
        [_logArray clear];
        glPopAttrib();
    }
#endif
}

- (void) dealloc {
    [_logArray release];
    
    [super dealloc];
}
@end
