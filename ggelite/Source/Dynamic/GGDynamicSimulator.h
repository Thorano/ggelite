//
//  GGDynamicSimulator.h
//  elite
//
//  Created by Frédéric De Jaeger on 12/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GGDynamicProtocol.h"

@class GGSolid;
@class GGInteraction;
@class GGCollisionManager;

@interface GGDynamicSimulator : NSObject {
    GGCollisionManager*                                _collisionManager;
    NSMutableArray*                             _solids;
    /* liste of solid managed by the simulator */
    
    GGLinearSystem*                             _linearSystem;
    
    NSMutableArray*                             _interactions;
    /* all the interactions between all the bodies
        */
    
    NSMutableArray*                             _formalInteractions;
    NSMutableArray*                             _contactsInteractions;
    Vect3D                                      _gravity;
    
    int                                         _currentFrame;
    /* counter incremented after each frame */
    
    double                                      _simulationTime;
    double                                      _currentTime;
    BOOL                                        _firstRunInFrame;
    BOOL                                        _useGameTime;
    BOOL                                        _isPaused;
    
#ifdef DEBUG
    NSMutableArray*                              _effectsToObserve;
    NSArray*                                    observable;
    BOOL                                        _shouldUpdate;
    BOOL                        _showSolids;
    BOOL                        _pauseOnCollisions;
    BOOL                        _showCollisions;
    BOOL                        _showCollisionsBoxes;
#endif
}
- (void) advanceTimeByDelta:(double)delta;

- (void) setGravity:(Vect3D) g;
- (void) addBody:(GGSolid*)solid;
- (void) addInteraction:(id<GGInteractionProtocol>)interaction;
- (void) addFormalInteraction:(id<GGFormalInteractionProtocol>)formalInteraction;
- (void) addForCollision:(GGSolid*)solid;
- (void) removeSolidFromSimulation:(GGSolid*)solid;

- (GGCollisionManager*) collisionManager;
- (GGLinearSystem*) system;

- (BOOL)isPaused;
- (void)setIsPaused:(BOOL)value;

#ifdef DEBUG
- (void) addForce:(Vect3D)force
         location:(Vect3D)pos
             name:(NSString*)forceName
      interaction:(id<GGInteractionProtocol>)interaction;

- (void) addFormalForce:(GGFormalVector*)formalForce
               location:(Vect3D)pos
                   name:(NSString*)forceName
            interaction:(id<GGInteractionProtocol>)interaction;
- (void) invalidateEffects;

- (NSArray *)observable;
- (void)setObservable:(NSArray *)value;

- (BOOL)showSolids;
- (void)setShowSolids:(BOOL)value;

- (void) rasterise: (Camera *)cam;

- (BOOL)showCollisions;
- (void)setShowCollisions:(BOOL)value;

- (BOOL)showCollisionsBoxes;
- (void)setShowCollisionsBoxes:(BOOL)value;

- (BOOL)pauseOnCollisions;
- (void)setPauseOnCollisions:(BOOL)value;

- (BOOL)useGameTime;
- (void)setUseGameTime:(BOOL)value;

#endif
@end

#ifdef DEBUG
@class NSArrayController, NSTableView;
#ifndef IBOutlet
#define IBOutlet
#endif

#ifndef IBAction
#define IBAction void
#endif

@class NSTreeController;
@interface GGDynamicDebugController : NSObject
{
    GGDynamicSimulator*         _simulator;

    float                       _energy;
    float                       _potentialEnergy;
    float                       _inertialEnergy;
    float                       _rotationalEnergy;
    BOOL                        _ignoreFormalInteractions;
    
    IBOutlet NSTreeController*           collisionNodes;
    IBOutlet NSTreeController*           sorters;
    IBOutlet NSArrayController*          formalInteractions;
    IBOutlet NSTableView*                tableView;
}

+ (GGDynamicDebugController*) controller;

- (NSIndexSet *)selectedRowIndexes;
- (NSArray*) selectionIndexPaths;
- (NSArray*) selectedNodes;

- (IBAction) reloadCollisionItem:(id)sender;


- (BOOL) isInteractionSelected:(id<GGInteractionProtocol>)interaction;

#pragma mark -

- (BOOL)ignoreFormalInteractions;
- (void)setIgnoreFormalInteractions:(BOOL)value;

- (float)energy;
- (void)setEnergy:(float)value;

- (float)potentialEnergy;
- (void)setPotentialEnergy:(float)value;

- (float) inertialEnergy;
- (void)setInertialEnergy:(float)value;

- (float)rotationalEnergy;
- (void)setRotationalEnergy:(float)value;

- (GGDynamicSimulator *)simulator;
- (void)setSimulator:(GGDynamicSimulator *)value;

@end
#endif