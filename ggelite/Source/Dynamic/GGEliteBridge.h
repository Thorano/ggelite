//
//  GGEliteBridge.h
//  elite
//
//  Created by Frederic De Jaeger on 29/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGMobile.h"

@class GGDynamicSimulator;

@interface GGEliteBridge : GGMobile
{
    GGDynamicSimulator*             _simulator;
}

- (void)loadFileAtPath:(NSString*)path;
@end

