//
//  GGGravitation.h
//  elite
//
//  Created by Frederic De Jaeger on 29/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GGDynamicProtocol.h"
#import "GGSolid.h"

extern float gravitationIntensity;

@interface GGGravitation : NSObject <GGInteractionProtocol>{
    GGSolid*            _body;
    NSString*           _name;
}
- (id) initWithBody:(GGSolid*)firstBody;
- (void)setName:(NSString *)value;
@end

@interface GGSolid (GravitaionExtension)
- (void) addGravity;
@end