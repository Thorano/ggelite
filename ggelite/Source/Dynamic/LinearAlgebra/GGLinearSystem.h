//
//  GGLinearSystem.h
//  elite
//
//  Created by Frédéric De Jaeger on 12/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class GGLinearPoly, GGFormalVector;
@interface GGLinearSystem : NSObject {
    NSMutableArray*         _equations;
    unsigned                _nextUnknown;
    double*                 _solutions;
    unsigned                _size;
}
- (void) addScalarCondition:(GGLinearPoly*)condition;
- (void) addVectorialCondition:(GGFormalVector*)condition;
- (BOOL) solveSystem;
- (unsigned) nextFreeIndex;
- (unsigned) numberOfUnknown;

- (double*) solutions;

- (void) resetSystem;
@end

BOOL gauss(int dim, double coef[dim][dim+1], double soluce[dim]);
void gradientSolve(int nrow, int ncol, double coef[nrow][ncol+1], double soluce[ncol]);
BOOL solveLinearWithConjugateGradient(int nrow, int ncol, double coef[nrow][ncol+1], double soluce[ncol]);
