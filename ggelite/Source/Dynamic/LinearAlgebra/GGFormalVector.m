//
//  GGFormalVector.m
//  elite
//
//  Created by Frédéric De Jaeger on 12/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGFormalVector.h"

#import "GGLinearSystem.h"
#import "utile.h"

@implementation GGLinearPoly
- (id) initWithConstant:(double)val
{
    _size = 1;
    _terms = AllocType(_size, double);
    _terms[0] = val;
    
    return self;
}

- (id) initWithCoefsNoCopy:(double*)coefs
                      size:(unsigned)size
{
    _size = size;
    _terms = coefs;
    
    return self;
}

- (id) initWithUknownInLinearSystem:(GGLinearSystem*)system
{
    _size = [system nextFreeIndex]+1;
    _terms = AllocType(_size, double);
    int i;
    for(i = 0; i < _size-1; ++i){
        _terms[i] = 0.0;
    }
    _terms[_size-1] = 1.0;

    return self;
}

- (void) dealloc
{
    if(_terms){
        free(_terms);
    }
    
    [super dealloc];
}

- (void) _commonCopyToObj:(GGLinearPoly*) ret
{
    ret->_size = _size;
    ret->_terms = AllocType(_size, double);
    memcpy(ret->_terms,_terms,_size*sizeof(double));
}

- (id)copyWithZone:(NSZone *)zone
{
    GGLinearPoly* ret = (id)NSAllocateObject([GGLinearPoly class],0,zone);
    
    [self _commonCopyToObj:ret];
    return ret;
}

- (id)mutableCopyWithZone:(NSZone *)zone
{
    GGLinearPoly* ret = (id)NSAllocateObject([GGMutableLinearPoly class],0,zone);
    [self _commonCopyToObj:ret];

    return ret;
}

- (id) opposite
{
    double *newCoefs = AllocType(_size, double);
    int i;
    for(i = 0; i < _size; ++i){
        newCoefs[i] = -_terms[i];
    }
    
    id ret = [[[[self class] alloc] initWithCoefsNoCopy:newCoefs
                                          size:_size] autorelease];

    return ret;
}

- (double) evaluateInSolutions:(GGLinearSystem*)system
{
    double ret = 0.0;
    int i;
    double* solutions = [system solutions];
    // assume that solutions[0] = 1.0
    for(i = 0; i < _size; ++i){
        ret += _terms[i]*solutions[i];
    }
    return ret;
}

- (double) constant
{
    return _terms[0];
}

- (void)getCoefficients:(double*)result
              fromIndex:(unsigned)index
{
    int i, j;
    for(i = index, j = 0; i < _size; ++i, j++){
        result[j] = _terms[i];
    }
}

- (NSString*) description
{
    NSMutableString* result = [NSMutableString string];
    int i;
    
    [result setString:@"[ "];
    for(i = 0; i < _size; ++i){
        [result appendFormat:@"%9.5g, ", _terms[i]];
    }
    
    [result appendString:@"]"];
    
    return result;
}
@end

@implementation GGMutableLinearPoly
- (void) _extendToSizeIfNeeded:(unsigned)newSize
{
    NSAssert(_size>0, @"we should already have some vectors");
    if(newSize > _size){
        int oldSize = _size;
        _size = newSize;
        _terms = ReallocType(_terms, _size, double);
        int i;
        for(i = oldSize; i < _size; ++i){
            _terms[i] = 0.0;
        }
    }
}

- (void) add:(GGLinearPoly*)v
      lambda:(double)lambda
{
    [self _extendToSizeIfNeeded:v->_size];
    int i;
    // at this point, we know that _size >= v->size
    int limit = v->_size < _size ? v->_size : _size;
    for(i = 0; i < limit; ++i){
        _terms[i] += lambda*v->_terms[i];
    }
}

- (void) add:(GGLinearPoly*)v
{
    [self add:v lambda:1.0];
}

- (void) addConstant:(double)constant
{
    _terms[0] += constant;
}

- (void) substract:(GGLinearPoly*)v
{
    [self add:v lambda:-1.0];
}

- (void) multiply:(double)factor
{
    int i;
    for(i = 0; i < _size; ++i){
        _terms[i] *= factor;
    }
}

- (void) divide:(double)val
{
    [self multiply:1.0/val];
}
@end

@implementation GGFormalVector
+ (GGFormalVector*) vectorWithVect:(Vect3D*)vect
{
    return [[[self alloc] initWithVect:vect] autorelease];
}

- (id) initWithCoef:(id[3])coef
{
    _formalCoef[0] = [coef[0] retain];
    _formalCoef[1] = [coef[1] retain];
    _formalCoef[2] = [coef[2] retain];
    
    return self;
}

- (id) initWithUknownInLinearSystem:(GGLinearSystem*)system;
{
    _formalCoef[0] = [[GGMutableLinearPoly alloc] initWithUknownInLinearSystem:system];
    _formalCoef[1] = [[GGMutableLinearPoly alloc] initWithUknownInLinearSystem:system];
    _formalCoef[2] = [[GGMutableLinearPoly alloc] initWithUknownInLinearSystem:system];
    
    return self;
}


- (id) _initWithCoefNoRetain:(id[3])coef
{
    _formalCoef[0] = coef[0];
    _formalCoef[1] = coef[1];
    _formalCoef[2] = coef[2];
    
    return self;
}

- (id) initWithVect:(Vect3D*)vect
{
    _formalCoef[0] = [[GGMutableLinearPoly alloc] initWithConstant:vect->x];
    _formalCoef[1] = [[GGMutableLinearPoly alloc] initWithConstant:vect->y];
    _formalCoef[2] = [[GGMutableLinearPoly alloc] initWithConstant:vect->z];

    return self;
}

- (void) dealloc
{
    int i;
    for(i = 0; i < 3; ++i){
        [_formalCoef[i] release];
    }
    [super dealloc];
}

- (NSString*) description
{
    NSMutableString* result = [NSMutableString string];
    [result setString:@"[\n"];
    [result appendFormat:@"%@\n", _formalCoef[0]];
    [result appendFormat:@"%@\n", _formalCoef[1]];
    [result appendFormat:@"%@\n]", _formalCoef[2]];
    return result;
}

- (void) _commonCopyInCoefs:(GGMutableLinearPoly*[3])coef
                   withZone:(NSZone*)zone
{
    coef[0] = [_formalCoef[0] mutableCopyWithZone:zone];
    coef[1] = [_formalCoef[1] mutableCopyWithZone:zone];
    coef[2] = [_formalCoef[2] mutableCopyWithZone:zone];
}

- (id)copyWithZone:(NSZone *)zone
{
    GGMutableLinearPoly* coef[3];
    [self _commonCopyInCoefs:coef withZone:zone];
    
    GGFormalVector* copy = [[GGFormalVector allocWithZone:zone] _initWithCoefNoRetain:coef];

    return copy;
}

- (id)mutableCopyWithZone:(NSZone *)zone
{
    GGMutableLinearPoly* coef[3];
    [self _commonCopyInCoefs:coef withZone:zone];
    
    GGMutableFormalVector* copy = [[GGMutableFormalVector allocWithZone:zone] _initWithCoefNoRetain:coef];

    return copy;
}

- (id) transformWithMatrix3D:(GGMatrix4*)matrice
{
    GGMutableLinearPoly* coef[3];
    int i;
    pmat mat1 = (pmat)matrice;
    for(i = 0; i < 3; ++i){
        coef[i] = [_formalCoef[0] mutableCopy];
        [coef[i] multiply:mat1[0][i]];
        [coef[i] add:_formalCoef[1] 
              lambda:mat1[1][i]];
        [coef[i] add:_formalCoef[2]
              lambda:mat1[2][i]];
    }
    
    return [[[[self class] alloc] _initWithCoefNoRetain:coef] autorelease];
}

- (id) transformWithTransposeMatrix3D:(GGMatrix4*)matrice
{
    GGMutableLinearPoly* coef[3];
    int i;
    pmat mat1 = (pmat)matrice;
    for(i = 0; i < 3; ++i){
        coef[i] = [_formalCoef[0] mutableCopy];
        [coef[i] multiply:mat1[i][0]];
        [coef[i] add:_formalCoef[1] 
              lambda:mat1[i][1]];
        [coef[i] add:_formalCoef[2]
              lambda:mat1[i][2]];
    }
    
    return [[[[self class] alloc] _initWithCoefNoRetain:coef] autorelease];
}

- (id) oppositeVect
{
    GGMutableLinearPoly* newCoef[3];
    newCoef[0] = [_formalCoef[0] opposite];
    newCoef[1] = [_formalCoef[1] opposite];
    newCoef[2] = [_formalCoef[2] opposite];
    
    return [[[[self class] alloc] initWithCoef:newCoef] autorelease];
}

static GGMutableLinearPoly* newDet2x2(GGLinearPoly* a, GGLinearPoly* b, double x, double y)
{
    NSCParameterAssert(a!=nil);
    NSCParameterAssert(b!=nil);
    
    GGMutableLinearPoly* det = [a mutableCopy];
    [det multiply:y];
    [det add:b
      lambda:-x];
    
    return det;
}

- (id) produitVectoriel:(Vect3D*)vect
{
    GGMutableLinearPoly* newCoef[3];
    
    newCoef[0] = newDet2x2(_formalCoef[1], _formalCoef[2], vect->y, vect->z);
    newCoef[1] = newDet2x2(_formalCoef[2], _formalCoef[0], vect->z, vect->x);
    newCoef[2] = newDet2x2(_formalCoef[0], _formalCoef[1], vect->x, vect->y);
    
    return [[[[self class] alloc] _initWithCoefNoRetain:newCoef] autorelease];
}

- (GGMutableLinearPoly*) produitScalaire:(Vect3D*)vect
{
    GGMutableLinearPoly* res = [[_formalCoef[0] mutableCopy] autorelease];
    [res multiply:vect->x];
    [res add:_formalCoef[1] 
      lambda:vect->y];
    [res add:_formalCoef[2]
      lambda:vect->z];
        
    return res;
}


- (Vect3D) constantVect
{
    Vect3D v;
    v.x = [_formalCoef[0] constant];
    v.y = [_formalCoef[1] constant];
    v.z = [_formalCoef[2] constant];
    
    return v;
}

- (void) getAllCoef:(GGLinearPoly*[3])coef
{
    coef[0] = _formalCoef[0];
    coef[1] = _formalCoef[1];
    coef[2] = _formalCoef[2];
}

- (void) evaluateInSolutions:(GGLinearSystem*)solutions
                vectorResult:(Vect3D*)vectPtr
{
    initVect(vectPtr,
             [_formalCoef[0] evaluateInSolutions:solutions],
             [_formalCoef[1] evaluateInSolutions:solutions],
             [_formalCoef[2] evaluateInSolutions:solutions]);
}
@end

@implementation GGMutableFormalVector
- (void) addVect:(GGFormalVector*)vector
{
    [_formalCoef[0] add:vector->_formalCoef[0]];
    [_formalCoef[1] add:vector->_formalCoef[1]];
    [_formalCoef[2] add:vector->_formalCoef[2]];
}

- (void) addConstantVect:(Vect3D*)vect
{
    [_formalCoef[0] addConstant:vect->x];
    [_formalCoef[1] addConstant:vect->y];
    [_formalCoef[2] addConstant:vect->z];
}

- (void) substractVect:(GGFormalVector*)vector
{
    [_formalCoef[0] substract:vector->_formalCoef[0]];
    [_formalCoef[1] substract:vector->_formalCoef[1]];
    [_formalCoef[2] substract:vector->_formalCoef[2]];
}

- (void) multiply:(double)val
{
    [_formalCoef[0] multiply:val];
    [_formalCoef[1] multiply:val];
    [_formalCoef[2] multiply:val];
}

- (void) divide:(double)val
{
    [_formalCoef[0] divide:val];
    [_formalCoef[1] divide:val];
    [_formalCoef[2] divide:val];
}

@end