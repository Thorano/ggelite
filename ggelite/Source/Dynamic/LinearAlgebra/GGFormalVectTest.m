//
//  GGFormalVectTest.m
//  elite
//
//  Created by Frederic De Jaeger on 27/12/05.
//  Copyright (c) 2005 __MyCompanyName__. All rights reserved.
//

#import "GGFormalVectTest.h"

#import "GGFormalVector.h"
#import "GGLinearSystem.h"


@implementation GGFormalVectTest
- (void) testFormalVect
{
    Vect3D v = {
        1, 2, 3
    };
    GGFormalVector* vFormal = [[GGFormalVector alloc] initWithVect:&v];
    Vect3D w = [vFormal constantVect];
    [vFormal release];
    STAssertTrue(distanceManhattan(&v,&w) == 0.0, @"test a failure");
}

- (void) produitVectWithArg1:(Vect3D)u
                        arg2:(Vect3D)v
{
    GGFormalVector* formalU = [[GGFormalVector alloc] initWithVect:&u];
    GGFormalVector* res = [formalU produitVectoriel:&v];
    Vect3D w;
    prodVect(&u,&v,&w);
    [formalU release];
    Vect3D res2 = [res constantVect];
    STAssertTrue(distanceManhattan(&w,&res2) == 0.0, @"bad screw product");
}

- (void) testProduitVect
{
    Vect3D u = {
        1, -4, 7
    };
    Vect3D v = {
        2, 9, -3.1
    };
    
    [self produitVectWithArg1:u
                         arg2:v];
    [self produitVectWithArg1:mkVect(1,-4,7)
                         arg2:mkVect(2,8,-1)];
    
    [self produitVectWithArg1:mkVect(-0.1,0,0.001)
                         arg2:mkVect(3.5,5.0,-1.9)];
}

static BOOL isEqualVect(Vect3D a, Vect3D b){
    double max = normeVect(&a);
    double tmp = normeVect(&b);
    if(tmp > max){
        max = tmp;
    }
    return distanceManhattan(&a, &b) <= 1e-6 * max;
}

static inline  BOOL isDoubleNear(double a, double b)
{
    return fabs(a-b) < 1e-9;
}

- (void) produitMatrice:(GGMatrix4*)matrice
                 vector:(Vect3D)u
{
    normaliseMatrice(matrice);
    GGFormalVector* formalU = [[GGFormalVector alloc] initWithVect:&u];
    GGFormalVector* formalV = [formalU transformWithMatrix3D:matrice];
    GGFormalVector* formalW = [formalU transformWithTransposeMatrix3D:matrice];
    GGFormalVector* formalUU = [formalV transformWithTransposeMatrix3D:matrice];
    
    Vect3D v, w;
    produitMatriceVecteur3D(matrice,&u,&v);
    transposeProduitVect3D(matrice,&u,&w);
    
    STAssertTrue(isEqualVect([formalV constantVect], v), @"bad direct product: %@\n%@", stringOfVect2([formalV constantVect]), stringOfVect2(v));
    STAssertTrue(isEqualVect([formalW constantVect], w), @"bad transpose product");
    STAssertTrue(isEqualVect(u,[formalUU constantVect]), @"bad inversion property");
    
    [formalU release];
}

- (void) testFormalMatrix
{
    GGMatrix4 mat = {
    {
        1, 2, 3
    },0,
    {
        -1, 2, 3
    },0,
    {
        -1, 2, -3
    },0,
    {
        0,0,0
    }, 1
    };
    
    [self produitMatrice:&mat
                  vector:mkVect(1.2,0.9,-1.4)];
    initWithIdentityMatrix(&mat);
    [self produitMatrice:&mat
                  vector:mkVect(1.2,-0.9,-1.4)];
}
@end

@interface LinearSystemTest : SenTestCase {
    
}

@end

#define eprintf(format, args...) fprintf(stderr, format , ## args)

static void drawHLine(int dim){
    int i;
    for(i = 0;  i < dim+1; ++i){
        eprintf("-----");
    }
    eprintf("\n");
}

static void printSystem(int dim, double coef[dim][dim+1])
{
    drawHLine(dim);
    int i;
    for(i = 0; i < dim; ++i){
        eprintf("|");
        int k;
        for(k = 0; k < dim+1; ++k){
            eprintf("%g  |", coef[i][k]);
        }
        eprintf("\n");
    }
    drawHLine(dim);
}


@implementation LinearSystemTest
static void systemFrom(GGMatrix4* matrice, Vect3D v, double coef[3][4])
{
    pmat mat1 = (pmat)matrice;
    pvect res = (pvect)&v;
    
    int i;
    for(i=0; i < 3; ++i){
        int j;
        for(j = 0; j < 3; ++j){
            coef[i][j] = mat1[j][i];
        }
        coef[i][3] = res[i];
    }
}

- (void) testGauss
{
    double coef[3][4] = {
    {
        1, 2, 3, 4,
    },
    {
        2, 3, 4, 1,
    },
    {
        3, 4, 1, 2,
    },
    };
    
    double soluce[3];

    gauss(3, coef, soluce);
    STAssertTrue(isDoubleNear(soluce[0],-11), @"bad soluce: %g", soluce[0]);
    STAssertTrue(isDoubleNear(soluce[1], 9), @"bad soluce: %g", soluce[1]);
    STAssertTrue(isDoubleNear(soluce[2], -1), @"bad soluce: %g", soluce[2]);
}

- (void) testGradient
{
//    double coef[3][4] = {
//    {
//        1, 0, 0, 4,
//    },
//    {
//        0, 3, 0, 1,
//    },
//    {
//        0, 0, 1, 2,
//    },
//    };

    double coef[3][4] = {
    {
        1, 2, 3, 4,
    },
    {
        2, 3, 4, 1,
    },
    {
        3, 4, 1, 2,
    },
    };
    
    
    double soluce[3];
//    gradientSolve(3,3,coef,soluce);
    solveLinearWithConjugateGradient(3, 3,coef,soluce);
    NSLog(@"soluce = (%g, %g, %g", soluce[0], soluce[1], soluce[2]);

    STAssertTrue(fabs(soluce[0] + 11) < 1e-6, @"bad coef");
    STAssertTrue(fabs(soluce[1] + -9) < 1e-6, @"bad coef");
    STAssertTrue(fabs(soluce[2] + 1) < 1e-6, @"bad coef");
}

- (void) testGradienBig
{
    double coef[6][7] = {
        {    6.7111,         0,         0,         0,         0,  -0.81309, 0}, 
        {         0,       0.1,         0,         0,         0,         0, 0}, 
        {         0,         0,    6.7111,   0.81309,         0,         0, -5.1786}, 
        {         0,        -0,        -0,        -0,      -0.1,        -0, 0}, 
        {         0,         0,   0.81309,       0.1,         0,         0, 0}, 
        {         0,         0,         0,         0,         0,        -1, 0}
    };
        
    double coefCopy[6][7] = {
    {    6.7111,         0,         0,         0,         0,  -0.81309, 0}, 
    {         0,       0.1,         0,         0,         0,         0, 0}, 
    {         0,         0,    6.7111,   0.81309,         0,         0, -5.1786}, 
    {         0,        -0,        -0,        -0,      -0.1,        -0, 0}, 
    {         0,         0,   0.81309,       0.1,         0,         0, 0}, 
    {         0,         0,         0,         0,         0,        -1, 0}
    };
    
    double soluce[6];
    solveLinearWithConjugateGradient(6, 6,coef,soluce);
    NSLog(@"solucebig = (%g, %g, %g, %g, %g, %g)", soluce[0], soluce[1], soluce[2],
          soluce[3], soluce[4], soluce[5]);
    double soluceGauss[6];

    gauss(6, coef, soluceGauss);
    NSLog(@"soluceGauss = (%g, %g, %g, %g, %g, %g)", soluceGauss[0], soluceGauss[1], soluceGauss[2],
          soluceGauss[3], soluceGauss[4], soluceGauss[5]);
    
    int i;
    for(i = 0; i < 6; ++i){
        double val1 = 0;
        double val2 = 0;
        int j;
        
        for(j = 0; j < 6; ++j){
            val1 += coefCopy[i][j]*soluce[j];
            val2 += coefCopy[i][j]*soluceGauss[j];
        }
        NSLog(@"%g = %g(conju) = %g(gauss)", coefCopy[i][6], val1, val2);
            
    }
}

- (void) testOrtho
{
    GGMatrix4 mat = 
    {
    {
        1, 0, 0
    },0,
    {
        -1, 1, 0
    },0,
    {
        3, 4, 5
    },0,
    {
        0, 0, 0
    },1.0,
    };
    
    normaliseMatrice(&mat);
    Vect3D v = {
        1,2,3,
    };
    
    double coef[3][4];
    
    systemFrom(&mat, v, coef);
    
    printSystem(3, coef);
    double soluce[3];
    gauss(3, coef, soluce);
    Vect3D w2;
    initVect(&w2,soluce[0],soluce[1],soluce[2]);
    
    Vect3D w;
    transposeProduitVect3D(&mat,&v,&w);
    
    STAssertTrue(isEqualVect(w, w2), @"incorrect gauss resolution");
    printSystem(3, coef);
//    NSLog(@"w = %@", stringOfVect(&w));
}
@end
