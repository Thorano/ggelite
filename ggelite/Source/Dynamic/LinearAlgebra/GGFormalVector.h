//
//  GGFormalVector.h
//  elite
//
//  Created by Frédéric De Jaeger on 12/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "GG3D.h"


@class GGLinearSystem;
@interface GGLinearPoly : NSObject <NSCopying, NSMutableCopying>
{
    double*                     _terms;
    unsigned                    _size;
}
- (id) initWithConstant:(double)val;
- (id) initWithUknownInLinearSystem:(GGLinearSystem*)system;

- (id) opposite;

- (double) evaluateInSolutions:(GGLinearSystem*)solutions;
- (double) constant;
- (void)getCoefficients:(double*)result
              fromIndex:(unsigned)index;
@end

@interface GGMutableLinearPoly : GGLinearPoly
- (void) add:(GGLinearPoly*)v
      lambda:(double)lambda;
- (void) add:(GGLinearPoly*)v;
- (void) addConstant:(double)constant;
- (void) substract:(GGLinearPoly*)v;
- (void) multiply:(double)val;
- (void) divide:(double)val;
@end

@interface GGFormalVector : NSObject <NSCopying, NSMutableCopying> 
{
    GGMutableLinearPoly*        _formalCoef[3];
}
+ (GGFormalVector*) vectorWithVect:(Vect3D*)vect;
- (id) initWithVect:(Vect3D*)vect;
- (id) initWithUknownInLinearSystem:(GGLinearSystem*)system;

//- (id) initWithDirection:(Vect3D*)direction;

// the following returns something mutable if the object is.
- (id) transformWithMatrix3D:(GGMatrix4*)matrice;
- (id) transformWithTransposeMatrix3D:(GGMatrix4*)matrice;

- (id) oppositeVect;
- (id) produitVectoriel:(Vect3D*)vect;
- (GGMutableLinearPoly*) produitScalaire:(Vect3D*)vect;

- (Vect3D) constantVect;

- (void) getAllCoef:(GGLinearPoly*[3])coef;

- (void) evaluateInSolutions:(GGLinearSystem*)solutions
                vectorResult:(Vect3D*)vectPtr;
@end

@interface GGMutableFormalVector : GGFormalVector
- (void) addVect:(GGFormalVector*)vector;
- (void) addConstantVect:(Vect3D*)vect;
- (void) substractVect:(GGFormalVector*)vector;
- (void) divide:(double)val;
- (void) multiply:(double)val;
@end