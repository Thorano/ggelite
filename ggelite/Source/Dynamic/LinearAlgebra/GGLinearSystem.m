//
//  GGLinearSystem.m
//  elite
//
//  Created by Frédéric De Jaeger on 12/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGLinearSystem.h"

#import "GGFormalVector.h"
#import "utile.h"


@implementation GGLinearSystem
- init
{
    _equations = [NSMutableArray new];
    _nextUnknown = 0;
    
    return self;
}

- (void) dealloc
{
    [_equations release];
    if(_solutions){
        free(_solutions);
    }
    
    [super dealloc];
}

- (NSString*) description
{
    NSMutableString* str = [NSMutableString stringWithFormat:@"[System:\n%@",
        _equations];
    if(_solutions){
        [str appendString:@"soluce\n[ "];
        int i;
        for(i = 0; i <= _nextUnknown; ++i){
            [str appendFormat:@"%9.5g, ", _solutions[i]];
        }
        [str appendString:@"]"];
    }
    [str appendString:@"]"];
    
    return str;
}

- (unsigned) nextFreeIndex
{
    // 1 is the first index because 0 is reserved for the constant term.
    _nextUnknown++;
    return _nextUnknown;
}

- (void) addScalarCondition:(GGLinearPoly*)condition
{
    [_equations addObject:condition];
}

- (void) addVectorialCondition:(GGFormalVector*)condition
{
    GGLinearPoly* coef[3];
    [condition getAllCoef:coef];

    [_equations addObject:coef[0]];
    [_equations addObject:coef[1]];
    [_equations addObject:coef[2]];
}

#define ZEROPIVOT 1e-5
#define Coef(i,j) coef[_indices[i]][j]
BOOL multiGauss(int dim, int extraDim, double coef[dim][dim+extraDim], double (*results)[dim])
{
    int fullRow = dim + extraDim;
    int i;
    int _indices[dim];
    
    for(i=0; i<dim; ++i){
        _indices[i] = i;
    }
    
//    printSystem(dim, coef);
    for(i = 0; i < dim; ++i){
        // look for a pivot
        int j;
        int bestPivot = -1;
        double normeBestPivot = 0.0;
        for(j = i; j < dim; ++j){
            double coefNorm = fabs(Coef(j,i));
            if(coefNorm >= ZEROPIVOT){
                if(bestPivot == -1 || coefNorm > normeBestPivot){
                    bestPivot = j;
                    normeBestPivot = coefNorm;
                }
            }
        }
        
        if(bestPivot < 0){
            NSLog(@"singular system, could not find a pivot"); 
            return NO;
        }
        
//        swapLine(dim, coef, i, j);
        if(i != bestPivot)
        {
            // we have to swap the lines.
            int tmp = _indices[i];
            _indices[i] = _indices[bestPivot];
            _indices[bestPivot] = tmp;
        }
        
        double pivotCoef = Coef(i, i);
        NSCParameterAssert(fabs(pivotCoef) >= ZEROPIVOT);
        
        // substract all the pivot
        
        for(j = i+1; j < dim; ++j){
            double leadCoef = Coef(j,i);
            if(leadCoef != 0.0){  // not bad if approximately true
                leadCoef /= pivotCoef;
                // substract the pivot
                int k;
                for(k = i; k < fullRow; ++k){
                    Coef(j, k) -= leadCoef*Coef(i,k);
                }
            }
        }
//        printSystem(dim, coef);        
    }


    // now, we have a triangular system.  solve it.
    
    for(i = dim-1; i >= 0; --i){
        int j;
        for(j = i+1; j < dim; ++j){
            NSCParameterAssert(Coef(j, j) == 1.0);
            int k;
            for(k = 0; k < extraDim; ++k){
                Coef(i, dim+k) -= Coef(i,j) * Coef(j, dim+k);
            }
        }
        
        int k;
        for(k = 0; k < extraDim; ++k){
            Coef(i, dim+k) /= Coef(i, i);
        }
            
        Coef(i, i) = 1.0;
    }
    
    // now the system is diagonal.
//    printSystem(dim, coef);

    // reorder stuff
    for(i = 0; i < extraDim; ++i){
        int j;
        
        for(j = 0; j < dim; ++j){
            results[i][j] = Coef(j, dim+i);
        }
    }
    
    return YES;
}

BOOL gauss(int dim, double coef[dim][dim+1], double soluce[dim])
{
    double (*tmp)[dim] = (double (*)[dim])soluce;
    return multiGauss(dim, 1, coef, tmp);
}

static void evalMatrix(int nrow, int ncol, double coef[nrow][ncol+1], double input[ncol], double res[nrow])
{
    int i;
    for(i = 0; i < nrow;  ++i){
        double sum = 0.0;
        int j;
        for(j = 0; j < ncol; ++j){
            sum += coef[i][j]*input[j];
        }
        res[i] = sum;
    }
}

static void evalTranspose(int nrow, int ncol, double coef[nrow][ncol+1], double input[nrow], double res[ncol])
{
    int i;
    for(i = 0; i < ncol; ++i){
        double sum = 0.0;
        int j;
        for(j = 0;  j < nrow; ++j){
            sum += coef[j][i]*input[j];
        }
        res[i] = sum;
    }
}

static double _prodScal(int size, double coef[size], double coef2[size])
{
    double ret = 0.0;
    
    int i;
    for(i = 0; i < size; ++i){
        ret += coef[i]*coef2[i];
    }
    
    return ret;
}

static double norme2(int size, double coef[size])
{
    return _prodScal(size, coef, coef);
}

void gradientSolve(int nrow, int ncol, double coef[nrow][ncol+1], double soluce[ncol])
{
    double grad[ncol];
    double tmpImage[nrow];
    double grad2[ncol];
    
    int i;
    for(i = 0;  i < ncol;  ++i){
        soluce[i] = 0.;
    }
    
    BOOL cont = YES;
    int nIter = 0;
    
    do{
        nIter++;
        evalMatrix(nrow, ncol, coef, soluce, tmpImage);
        {
            int j;
            for(j = 0; j < nrow; ++j){
                tmpImage[j] -= coef[j][ncol];
            }
        }
        
        double error = norme2(nrow, tmpImage);
        if(error <= 1e-6){
            cont = NO;
        }
        else{
            NSLog(@"error = %g", error);
            evalTranspose(nrow, ncol, coef, tmpImage, grad);
            double norme2Grad = norme2(ncol, grad);
            evalMatrix(nrow, ncol, coef, grad, tmpImage);
            double norme2AGrad = norme2(nrow, tmpImage);
            if(norme2AGrad <= 1e-6){
                // co grad too small.
                cont = NO;
            }
            else{
                double fact = -(norme2Grad/norme2AGrad);
                NSLog(@"coef = %g", fact);
                for(i = 0; i < ncol; ++i){
                    soluce[i] += fact * grad[i];
                }
                {
                    // debug stuff.
                
                    evalMatrix(nrow, ncol, coef, soluce, tmpImage);
                    int j;
                    for(j = 0; j < nrow; ++j){
                        tmpImage[j] -= coef[j][ncol];
                    }
                    evalTranspose(nrow, ncol, coef, tmpImage, grad2);
                    
                    NSLog(@"scal = %g", _prodScal(ncol,grad,grad2));
                }    
            }
        }
    }while(cont);
    
    NSLog(@"nIter = %d", nIter);
}

static void computeResidual(int dim, double coef[dim][dim+1], double guess[dim], double result[dim])
{
    evalMatrix(dim,dim,coef,guess, result);
    int j;
    for(j = 0; j < dim; ++j){
        result[j] -= coef[j][dim];
    }
    
}

static double conjugateProduct(int dim, double coef[dim][dim+1], double a[dim], double b[dim])
{
    int i, j;
    
    double ret = 0.0;
    for(i = 0; i < dim; ++i){
        for(j = 0; j < dim; ++j){
            ret += a[i]*b[j]*coef[i][j];
        }
    }
    
    return ret;
}

#define LIMIT 10000

BOOL conjugateGradientSolve(int dim, double coef[dim][dim+1], double x[dim])
{
    double g[dim]; // gradient
    double h[dim]; // conjugate gradient.
    int i;
    for(i = 0; i < dim; ++i){
        x[i] = 0.0;
    }
    computeResidual(dim, coef, x, g);
    double normeGradient = _prodScal(dim,g,g);

    memcpy(h,g,sizeof(g));
    
    BOOL cont = YES;
    int attempt = 0;
    while(cont && normeGradient > 1e-12 && attempt < LIMIT){
//        NSLog(@"normeGradient = %g", normeGradient);
        double denom = conjugateProduct(dim, coef, h, h);
        if(denom <= 1e-12 /*EPS*/){
            NSLog(@"degenerated conjugate stuff: %g", denom);
            cont = NO;
        }
        else{
            double lambda = _prodScal(dim,h,g) / denom;
            for(i = 0; i < dim; ++i){
                x[i] -= lambda*h[i];
            }
            
            computeResidual(dim, coef, x, g);  // new gradient at new x.
            double newNormeGradient = _prodScal(dim,g,g);
            double gamma = newNormeGradient/normeGradient;
            
            for(i = 0; i < dim; ++i){
                h[i] = g[i] + gamma*h[i];
            }
            normeGradient = newNormeGradient;
        }
        attempt++;
    }
    if(attempt >= LIMIT){
        NSLog(@"wow more than %d iteration, stoping", LIMIT);
        return NO;
    }
    else{
        return YES;
    }
}

BOOL solveLinearWithConjugateGradient(int nrow, int ncol, double coef[nrow][ncol+1], double soluce[ncol])
{
    double newCoef[ncol][ncol+1];
    
    // first step:
    // compute tA.A (including the last column)
    int i, j;
    for(i = 0; i < ncol; ++i){
        for(j = 0; j < ncol+1; ++j){
            newCoef[i][j] = 0.0;
            int k;
            for(k = 0; k < nrow; ++k){
                newCoef[i][j] += coef[k][i]*coef[k][j];
            }
        }
    }
    
    return conjugateGradientSolve(ncol,newCoef,soluce);
}

- (unsigned) numberOfUnknown
{
    return _nextUnknown+1;
}

- (BOOL) solveSystem
{
    GGLinearPoly* equation;

    //    NSLog(@"%d equations %d variables", [_equations count], [coefToIndex count]);
    
    int nrow = [_equations count];
    int ncol = _nextUnknown;
    
    if(NULL == _solutions){
        _size = [self numberOfUnknown];
        _solutions = AllocType(_size, double);
    }
    else if(_size < [self numberOfUnknown]){
        _size = [self numberOfUnknown];
        _solutions = ReallocType(_solutions, _size, double);
    }
        
    if(ncol > nrow || ncol > 50){
        NSLog(@"can't solve that system yet: (ncol = %d, nrow = %d)", ncol, nrow);
    }
    else{
        double coef[nrow][ncol+1];
        memset(coef,0,sizeof(coef));
        
        int i;
        for(i = 0; i < nrow; ++i){
            equation = [_equations objectAtIndex:i];
            [equation getCoefficients:coef[i] fromIndex:1];
            coef[i][ncol] = -[equation constant];
        }
        
        // keep the first element to put the coef for the constant
        _solutions[0] = 1.0;
        BOOL ret;
#if 0
        ret = solveLinearWithConjugateGradient(nrow, ncol, coef, _solutions+1);
#else
        NSAssert(nrow == ncol, @"system must be square for Gauss");
        ret = gauss(nrow,coef,_solutions+1);;
#endif
        BOOL bad = NO;
        double error[nrow];
        if(ret){
            int i;
            for(i = 0; i < nrow; ++i){
                equation = [_equations objectAtIndex:i];
                double val = [equation evaluateInSolutions:self];
                error[i] = val;
                if(fabs(val) > 1e-3){
                    bad = YES;
                }
            }
        }
        if(bad){
            NSLog(@"bogus system:\n%@", self);
            NSMutableString* str = [NSMutableString stringWithCapacity:80];
            [str setString:@"[ "];
            for(i = 0; i < nrow; ++i){
                [str appendFormat:@"%9.5g, ", error[i]];
            }
            NSLog(@"error:\n%@", str);
//            ggabort();
            //                    [NSException raise:@"BAA" format:@"can't solve equation"];
        }
        return ret;
    }
    
    return NO;
}

- (double*) solutions
{
    if(NULL == _solutions){
        _solutions = AllocType(1, double);
        _solutions[0] = 1.0;
    }
    return _solutions;
}

- (void) resetSystem
{
    [_equations removeAllObjects];
}
@end

