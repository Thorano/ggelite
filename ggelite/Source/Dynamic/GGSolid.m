//
//  GGSolid.m
//  elite
//
//  Created by Frederic De Jaeger on 09/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGSolidPrivate.h"

#import "GGModel.h"
#import "GGDynamicProtocol.h"
#import "GGDynamicSimulator.h"

#import "GGFormalVector.h"
#import "GGLog.h"
#import "utile.h"
#import "GGIdGenerator.h"
#import "GGMMesh.h"

// collision stuff
#import "GGCNode.h"
#import "GGCNodeBuilder.h"

@interface GGMNode (CollisionAddition)
- (GGCNodeCommon*) collisionNodeWithSolid:(GGSolid*)solid
                                inManager:(GGCollisionManager*)manager;
@end

@implementation GGSolid
#pragma mark init and such
- (void)_setupDefaultPosition
{
    initWithIdentityMatrix(&_position);
    SetZeroVect(_speed);
    SetZeroVect(_angularSpeed);
    _masse = 1;
    _masseInverse = 1.0;
    initWithIdentityMatrix(&_inertial);
    initWithIdentityMatrix(&_inertialInverse);
}

static NSMutableIndexSet* sIdGenenerator;
+ (void) initialize
{
    if(self == [GGSolid class]){
        sIdGenenerator = [NSMutableIndexSet new];
    }
}

+ (unsigned) maxIdentifier
{
    return [sIdGenenerator lastIndex];
}

- (id) init
{
    NSAssert(sIdGenenerator, @"badly init");
    self = [super init];
    if(self){
        [self _setupDefaultPosition];
        NSLog(@"model = %@", _model);
        _key = [sIdGenenerator nextAvailableIdentifier];
    }
    
    return self;
}

- (void) dealloc
{
    [sIdGenenerator releaseIdentifier:_key];
    [_formalForce release];
    [_formalMomentum release];
    [_name release];
    [_model release];
    
    [super dealloc];
}

#pragma mark public API

- (unsigned)identiifer
{
    return _key;
}

- (BOOL) hasGravity
{
    return _masse != 0.0;
}

- (NSString *)name {
    return [[_name retain] autorelease];
}

- (void)setName:(NSString *)value {
    if (_name != value) {
        [_name release];
        _name = [value copy];
    }
}

- (NSString*) description
{
    NSString* name = [self name];
    if(name){
        return name;
    }
    else{
        return [super description];
    }
}

- (void) setPosition:(GGMatrix4*)pMat
{
    _position = *pMat;
}

- (ModelBlender*)model {
    return _model;
}

- (void)setModel:(ModelBlender*)value {
    ASSIGN(_model, value);
}

- (int)indexForCollision {
    return _indexForCollision;
}

- (void)setIndexForCollision:(int)value {
        _indexForCollision = value;
}

- (GGDynamicSimulator*) simulator
{
    return _simulator;
}

- (void) setSimulator:(GGDynamicSimulator*)simulator
{
    _simulator = simulator;
}

- (void) setHomogeneousInertia:(double)val
{
    pmat mat = (pmat)&_inertialInverse;
    mat[0][0] = mat[1][1] = mat[2][2] = 1.0/val;
    
    mat = (pmat)&_inertial;
    mat[0][0] = mat[1][1] = mat[2][2] = val;
}

- (void) setInertialVector:(Vect3D)v
{
    initWithIdentityMatrix(&_inertialInverse);
    pmat mat = (pmat)&_inertialInverse;
    mat[0][0] = 1.0/v.x;
    mat[1][1] = 1.0/v.y;
    mat[2][2] = 1.0/v.z;
    
    initWithIdentityMatrix(&_inertial);
    mat = (pmat)&_inertial;
    mat[0][0] = v.x;
    mat[1][1] = v.y;
    mat[2][2] = v.z;
}

- (void) setMasse:(GGReal)masse
{
    _masse = masse;
    _masseInverse = 1/masse;
}

- (void) setInfiniteMass
{
    // set masse and inertial to zero because they should not be
    // in energy computation.
    // (the only place where that is used).
    _masse = 0.0;
    _masseInverse = 0.0;
    initDiagonalMatrice(&_inertialInverse, 0, 0, 0);
    initDiagonalMatrice(&_inertial,0,0,0);
    pmat mat = (pmat)&_inertialInverse;
    mat[0][0] = mat[1][1] = mat[2][2] = 0.0;
}

#pragma mark -
- (void) resetActions
{
    DESTROY(_formalAcceleration);
    DESTROY(_formalAngularAcceleration);
    DESTROY(_formalForce);
    DESTROY(_formalMomentum);
    SetZeroVect(_force);
    SetZeroVect(_momentum);
    SetZeroVect(_correctionFormalForce);
    SetZeroVect(_correctionFormalMomentum);
}

- (void) addForce:(Vect3D) force
         momentum:(Vect3D) momentum
{
    addVect(&_force, &force, &_force);
    addVect(&_momentum, &momentum, &_momentum);
}

- (void) addForce:(Vect3D) force
          atPoint:(Vect3D) point
{
    Vect3D momemtum;
    Vect3D GM;
    mkVectFromPoint(&_position.position, &point,&GM);
    prodVect(&GM,&force,&momemtum);
    [self addForce:force momentum:momemtum];
}

- (void) addFormalForce:(GGFormalVector*)formalForce
         formalMomentum:(GGFormalVector*)formalMomentum
  correctionFormalForce:(Vect3D)correctionFormalForce
correctionFormalMomentum:(Vect3D)correctionFormalMomentum
{
#if DEBUG_COCOA
    if(![[GGDynamicDebugController controller] ignoreFormalInteractions])
#endif
    {
        if(formalForce){
            if(nil == _formalForce){
                _formalForce = [formalForce mutableCopy];
            }
            else{
                [_formalForce addVect:formalForce];
            }
        }
        
        if(formalMomentum){
            if(nil == _formalMomentum){
                _formalMomentum = [formalMomentum mutableCopy];
            }
            else{
                [_formalMomentum addVect:formalMomentum];
            }
        }        
    }
    
    NSDebugMLLogWindow(@"Force", @"_correctionFormalForce = \n%@\ncorrectionFormalMomentum=%@", stringOfVect(&_correctionFormalForce),
                       stringOfVect(&correctionFormalMomentum));
    
    addVect(&_correctionFormalForce, &correctionFormalForce,
            &_correctionFormalForce);
    addVect(&_correctionFormalMomentum, &correctionFormalMomentum,
            &_correctionFormalMomentum);
}

- (void) advanceTimeByDelta:(double)delta
{
    Vect3D acceleration;

    // XXX remove that
#if 1
    addVect(&_force,&_correctionFormalForce,&_force);
    addVect(&_momentum,&_correctionFormalMomentum,&_momentum);
#endif
    // compute inertial and angular acceleration.
    mulScalVect(&_force, _masseInverse,&acceleration);
    Vect3D localMomentum;
    transposeProduitVect3D(&_position, &_momentum, &localMomentum);
    Vect3D angularAcceleration;
    produitMatriceVecteur3D(&_inertialInverse, &localMomentum, &angularAcceleration);

//    NSLogWindow(@"force = %@\nangularAcceleration = %@", 
//                stringOfVect(&_force),
//                stringOfVect(&angularAcceleration));

    // step 2: integrate actions
    [self advanceTimeByDelta:delta
                acceleration:acceleration
        angularAcceleration:angularAcceleration];
}    

- (void) rasterise: (Camera *)cam
{
    glPushMatrix();
    {
        // just a cube
        glMultMatrix(&_position);
        if(_model){
            [_model rasterise:cam];
        }
        else{
            //        glRotatef(90,0,1,0);
            glScalef(10,10,10);
            glColor3f(0.7,0.9,0.9);
            glBegin(GL_QUAD_STRIP);
            {
                // face 1
                glNormal3f(0,-1,0);
                glVertex3f(-1,-1,-1);
                glVertex3f(1,-1,-1);
                glVertex3f(-1,-1,1);
                glVertex3f(1,-1,1);
                
                //face 2
                glNormal3f(0,0,1);
                glVertex3f(-1,1,1);
                glVertex3f(1,1,1);
                
                //face 3
                glNormal3f(0,1,0);
                glVertex3f(-1,1,-1);
                glVertex3f(1,1,-1);
                
                //face 4
                glNormal3f(0,0,-1);
                glVertex3f(-1,-1,-1);
                glVertex3f(1,-1,-1);
            }
            glEnd();
            
            glBegin(GL_QUADS);
            {
                // face 5
                glNormal3f(-1,0,0);
                glVertex3f(-1,1,-1);
                glVertex3f(-1,-1,-1);            
                glVertex3f(-1,-1,1);
                glVertex3f(-1,1,1);
                
                // face 6
                glNormal3f(1,0,0);
                glVertex3f(1,-1,-1);
                glVertex3f(1,1,-1);            
                glVertex3f(1,1,1);
                glVertex3f(1,-1,1);
            }
            glEnd();
        }
    }
    glPopMatrix();
}

- (double) inertialEnergy:(double*)inertialRotationPtr
{
    double energy = 0.5*_masse*prodScal(&_speed, &_speed);
    
    Vect3D tmp;
    produitMatriceVecteur3D(&_inertial,&_angularSpeed,&tmp);
    
    if(inertialRotationPtr){
        *inertialRotationPtr = 0.5*prodScal(&_angularSpeed,&tmp);
    }
    
    return energy;
}

#pragma mark -
#pragma mark about formal stuff
- (GGFormalVector *)formalAcceleration {
    NSAssert(_formalAcceleration, @"we should have computed it");

    return [[_formalAcceleration retain] autorelease];
}

- (GGFormalVector *)formalAngularAcceleration {
    NSAssert(_formalAngularAcceleration, @"we should have computed it");

    return [[_formalAngularAcceleration retain] autorelease];
}

- (BOOL) hasFormalInteractions
{
    return _formalForce != nil || _formalMomentum != nil;
}

- (void) computeFormalAccelerations
{
    NSAssert(_formalForce != nil || _formalMomentum != nil, 
             @"Should not use formal path when no formal interaction");
    
    if(nil == _formalForce){
        _formalForce = [[GGMutableFormalVector alloc] initWithVect:&_force];
    }
    else{
        [_formalForce addConstantVect:&_force];
    }
    
    if(nil == _formalMomentum){
        _formalMomentum = [[GGMutableFormalVector alloc] initWithVect:&_momentum];
    }
    else{
        [_formalMomentum addConstantVect:&_momentum];
    }

    [_formalForce multiply:_masseInverse];
    NSParameterAssert(nil == _formalAcceleration);
    
    // that's a little hackish.  I want to avoid any bad reference to _formalForce.
    _formalAcceleration = _formalForce;
    _formalForce = nil;
    
    GGFormalVector* localMomentum = [_formalMomentum transformWithTransposeMatrix3D:&_position];
    GGFormalVector* localAngularAcceleration = [localMomentum transformWithMatrix3D:&_inertialInverse];
    
    NSParameterAssert(nil == _formalAngularAcceleration);
    _formalAngularAcceleration = [localAngularAcceleration retain];
}

- (void) speedAtSolidPoint:(Vect3D*)pointInSpace
               speedResult:(Vect3D*)speedPtr
{
    Vect3D localSpeed;
    prodVect(&_angularSpeed, pointInSpace, &localSpeed);
    Vect3D tmp;
    produitMatriceVecteur3D(&_position,&localSpeed,&tmp);
    addVect(&tmp,&_speed,speedPtr);
}

- (GGFormalVector*) formalAccelerationAtSolidPoint:(Vect3D*)pointInSpace
{
    /* a = a_G + \Omega ^ GM + \omega ^ (\omega ^ GM) 
    */
    
    NSParameterAssert(_formalAcceleration!=nil);
    NSParameterAssert(_formalAngularAcceleration!=nil);
    Vect3D tmp;
    prodVect(&_angularSpeed,pointInSpace,&tmp);
    Vect3D tmp2;
    prodVect(&_angularSpeed,&tmp,&tmp2);
    produitMatriceVecteur3D(&_position,&tmp2,&tmp);
    
    GGMutableFormalVector* formalAccel = [[GGMutableFormalVector alloc] initWithVect:&tmp];
    [formalAccel addVect:[[_formalAngularAcceleration produitVectoriel:pointInSpace] transformWithMatrix3D:&_position]];
    [formalAccel addVect:_formalAcceleration];
    
    
    return [formalAccel autorelease];
}

- (void) advanceTimeByDelta:(double)delta
            formalSolutions:(GGLinearSystem*)solutions
{
    NSParameterAssert(_formalAcceleration!=nil);
    NSParameterAssert(_formalAngularAcceleration!=nil);

    Vect3D acceleration, angularAcceleration;
    [_formalAcceleration evaluateInSolutions:solutions
                                vectorResult:&acceleration];
    [_formalAngularAcceleration evaluateInSolutions:solutions
                                       vectorResult:&angularAcceleration];
    
    {
        Vect3D extraAccel, extraAngularAccel;
        // compute inertial and angular acceleration.
        mulScalVect(&_correctionFormalForce, _masseInverse,
                    &extraAccel);
        Vect3D localMomentum;
        transposeProduitVect3D(&_position, &_correctionFormalMomentum,
                               &localMomentum);
        produitMatriceVecteur3D(&_inertialInverse, &localMomentum, &extraAngularAccel);
        addVect(&acceleration,&extraAccel,&acceleration);
        addVect(&angularAcceleration,&extraAngularAccel,&angularAcceleration);
    }
    
    [self advanceTimeByDelta:delta
                acceleration:acceleration
         angularAcceleration:angularAcceleration];
}

- (NSArray*) collisionNodesInManager:(GGCollisionManager*)manager
{
    ModelBlender* model = [self model];
    NSArray* nodes = [model nodes];
    NSMutableArray* collisionStuff = [NSMutableArray arrayWithCapacity:[nodes count]];
    
    NSEnumerator* nodesEnum = [nodes objectEnumerator];
    GGMNode* aNode;
    while((aNode = [nodesEnum nextObject])!=nil){
        GGCNodeCommon* collisionNode = [aNode collisionNodeWithSolid:self
                                                           inManager:manager];
        [collisionStuff addObject:collisionNode];
    }
    
    return collisionStuff;
}

- (id<GGCollisionNode>) topLevelCollisionNodeInManager:(GGCollisionManager*)manager
{
    NSArray* nodes = [self collisionNodesInManager:manager];
    NSParameterAssert([nodes count] > 0);
    if([nodes count] == 1){
        return [nodes objectAtIndex:0];
    }
    else{
        return [[[GGCNode alloc] initWithSubNodes:nodes
                                          inSolid:self
                                        inManager:manager] autorelease];
    }
}
@end

@implementation GGMNode (CollisionAddition)
- (GGCNodeCommon*) populateNodeBuilder:(GGCNodeBuilder*)collisionNode
                             inManager:(GGCollisionManager*)manager
{
    int i;
    int nVertices = [[self mesh] numberOfVertices];
    GGCNodeCommon* nodeCollision = nil;
    if(nVertices){
        CacheVertex* vertices = AllocType(nVertices, CacheVertex);
        NSIndexSet* indexSet = [[NSIndexSet alloc] initWithIndexesInRange:NSMakeRange(0,nVertices)];
        [self computeVerticesAtIndices:indexSet
                     forTransformation:NULL
                             inResults:vertices];
        [indexSet release];
        
        FaceBlend* faces = [[self mesh] faces];
        int nface = [[self mesh] numberOfFaces];
        Vect3D verticesTriangle[3];
        for(i = 0; i < nface; ++i){
            verticesTriangle[0] = vertices[faces[i].A].pos;
            verticesTriangle[1] = vertices[faces[i].B].pos;
            verticesTriangle[2] = vertices[faces[i].C].pos;
            
            [collisionNode addTriangleWithIndex:i
                                       vertices:verticesTriangle
                                    nbrVertices:3];
        }
        nodeCollision = [collisionNode consolidateTreeInTransformedVertices:vertices
                                                                  inManager:manager];
        free(vertices);
    }
    
    return nodeCollision;
}

- (GGCNodeCommon*) collisionNodeWithSolid:(GGSolid*)solid
                                inManager:(GGCollisionManager*)manager
{
    BoundingBox box = {
    {
        1e20, 1e20, 1e20
    },
    {
        -1e20, -1e20, -1e20
    }
    };
    [self  improveBoundingBox:&box];
    
    AABB_enlarge(&box, 0.05);
    GGCNodeBuilder* collisionNodeBuilder = [[GGCNodeBuilder alloc] initWithMeshNode:self
                                                                            inSolid:solid
                                                                          forDomain:box];
    GGCNodeCommon* collisionNode = [self populateNodeBuilder:collisionNodeBuilder
                                                   inManager:manager];
    [collisionNodeBuilder release];
//    NSLog(@"collisionStuff =\n%@",  collisionNode);
    
    return collisionNode;
}
@end


@implementation GGSolid (Private)
- (void) advanceTimeByDelta:(double)delta
               acceleration:(Vect3D) acceleration
        angularAcceleration:(Vect3D) angularAcceleration
{
    Vect3D newSpeed;
    addLambdaVect(&_speed,delta/2,&acceleration,&newSpeed);
    addLambdaVect(&_position.position,delta,&newSpeed,&_position.position);
    addLambdaVect(&_speed,delta,&acceleration,&_speed);
    
    // step 3:integrate momentum
    // convert momentum to local frame.
    Vect3D newAngularSpeed;
    addLambdaVect(&_angularSpeed, delta/2, &angularAcceleration, &newAngularSpeed);
    rotateFrameWithLocalAngularSpeed(&_position, &newAngularSpeed, delta);
    addLambdaVect(&_angularSpeed, delta, &angularAcceleration, &_angularSpeed);
}

//- (void)setFormalAcceleration:(GGFormalVector *)value {
//    if (_formalAcceleration != value) {
//        [_formalAcceleration release];
//        _formalAcceleration = [value retain];
//    }
//}
//
//- (void)setFormalAngularAcceleration:(GGFormalVector *)value {
//    if (_formalAngularAcceleration != value) {
//        [_formalAngularAcceleration release];
//        _formalAngularAcceleration = [value retain];
//    }
//}
@end
