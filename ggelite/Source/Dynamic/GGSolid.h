//
//  GGSolid.h
//  elite
//
//  Created by Frederic De Jaeger on 09/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GG3D.h"
#import "GGDynamicProtocol.h"

@class GGDynamicSimulator, GGFormalVector, GGLinearSystem;
@class GGSolid, GGMutableFormalVector;
@class GGCNode, GGCollisionManager;
@protocol GGCollisionNode;
#import "GGModel.h"

@class ModelBlender;
@interface GGSolid : NSObject {
    NSString*               _name;
    ModelBlender*           _model;
    unsigned                      _key;

    @public
    GGMatrix4               _position;
    Vect3D                  _speed;
    Vect3D                  _angularSpeed;  // local frame
    double                  _masse;
    double                  _masseInverse;
    GGMatrix4               _inertial;      // local frame
    GGMatrix4               _inertialInverse;
    
    @protected
    GGDynamicSimulator*     _simulator;   // NOT RETAINED
    
    
    Vect3D                  _force;
    Vect3D                  _momentum;
    GGMutableFormalVector*  _formalForce;
    GGMutableFormalVector*  _formalMomentum;  // local frame
    GGFormalVector*         _formalAcceleration;
    GGFormalVector*         _formalAngularAcceleration;  // local frame
    Vect3D                  _correctionFormalForce;
    Vect3D                  _correctionFormalMomentum;

    // stuff for collision detection
    int                     _indexForCollision;
}
+ (unsigned) maxIdentifier;

- (id) init;
// designated initializer

- (unsigned)identiifer;

- (BOOL) hasGravity;

- (void) setPosition:(GGMatrix4*)pMat;

- (NSString *)name;
- (void)setName:(NSString *)value;

- (ModelBlender*)model;
- (void)setModel:(ModelBlender*)value;

- (int)indexForCollision;
- (void)setIndexForCollision:(int)value;

- (GGDynamicSimulator*) simulator;
- (void) setSimulator:(GGDynamicSimulator*)simulator;


- (void) advanceTimeByDelta:(double)delta;
- (void) rasterise: (Camera *)cam;

- (double) inertialEnergy:(double*)inertialRotationPtr;

- (void) resetActions;
- (void) addForce:(Vect3D) force
         momentum:(Vect3D) momentum;

    // point is considered in the global frame, of course.
- (void) addForce:(Vect3D) force
          atPoint:(Vect3D) point;

- (void) addFormalForce:(GGFormalVector*)formalForce
         formalMomentum:(GGFormalVector*)formalMomentum
  correctionFormalForce:(Vect3D)correctionFormalForce
correctionFormalMomentum:(Vect3D)correctionFormalMomentum;

- (void) setHomogeneousInertia:(double)val;
- (void) setInertialVector:(Vect3D)v;
- (void) setMasse:(GGReal)masse;
- (void) setInfiniteMass;

- (BOOL) hasFormalInteractions;
- (void) computeFormalAccelerations;
//- (void) computeFormalInteractions;

- (void) advanceTimeByDelta:(double)delta
            formalSolutions:(GGLinearSystem*)solutions;

- (GGFormalVector*) formalAcceleration;
- (GGFormalVector*) formalAngularAcceleration;

// pointInSpace is expressed in local coordinates.
// the result is expressed in global coordinates.
- (void) speedAtSolidPoint:(Vect3D*)pointInSpace
               speedResult:(Vect3D*)speedPtr;

- (GGFormalVector*) formalAccelerationAtSolidPoint:(Vect3D*)pointInSpace;

#pragma mark -
- (id<GGCollisionNode>) topLevelCollisionNodeInManager:(GGCollisionManager*)manager;

@end
