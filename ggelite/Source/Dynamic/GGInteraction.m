//
//  GGInteraction.m
//  elite
//
//  Created by Frederic De Jaeger on 09/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGInteraction.h"

#import "GGDynamicSimulator.h"
#import "GGFormalVector.h"
#import "GGLinearSystem.h"
#import "GGSolid.h"
#import "utile.h"

float FRICTION_PER_SPEED_FOR_CORRECTION = 10;
float SPRING_FOR_CORRECTION = 10;

@implementation GGInteraction
- initWithFirstBody:(GGSolid*)firstBody
         secondBody:(GGSolid*)secondBody
{
    self = [super init];
    if(self){
        _first = firstBody;
        _second = secondBody;
    }
    
    return self;
}

- (void) dealloc
{
    [_name release];
    [super dealloc];
}

- (NSString *)name {
    return [[_name retain] autorelease];
}

- (void)setName:(NSString *)value {
    if (_name != value) {
        [_name release];
        _name = [value copy];
    }
}

- (void) addInSimulator:(GGDynamicSimulator*)simulator
{
//    [_first addInteraction:self];
//    [_second addInteraction:self];
    _simulator = simulator;
}

- (void) rasterise: (Camera *)cam
{
    
}


- (void) computeInteractionInSimulator:(GGDynamicSimulator*)simulator
{
    initVect(&_cacheForce,0,0,0);
}

- (double) potentialEnergy
{
    return 0.0;
}

- (GGSolid*) firstObject
{
    return _first;
}

- (GGSolid*) secondObject
{
    return _second;
}
@end

@implementation GGRessort
- initWithFirstBody:(GGSolid*)firstBody
         secondBody:(GGSolid*)secondBody
{
    self = [super initWithFirstBody:firstBody
                         secondBody:secondBody];
    if(self){
        SetZeroVect(_firstAnchor);
        SetZeroVect(_secondAnchor);
        SetZeroVect(_spaceAnchor1);
        SetZeroVect(_spaceAnchor2);
        _naturalLength = 20;
        _power = 1;
        _modulus = 0;
    }
    
    return self;
}

- (void) setFirstAnchor:(Vect3D)vectPtr
{
    _firstAnchor = vectPtr;
}

- (void) setSecondAnchor:(Vect3D)vectPtr
{
    _secondAnchor = vectPtr;
}

- (double)naturalLength {
    return _naturalLength;
}

- (void)setNaturalLength:(double)value {
    _naturalLength = value;
}

- (double)power {
    return _power;
}

- (void)setPower:(double)value {
    _power = value;
}

static MapCol sRessortMap[] = 
{
    { -0.2, {  0.1, 0.1, 0.8, 1}},
    { 0, {  0.9, 0.9, 0.9, 1}},
    { 0.2, {  1, 0.1, 0.1, 1}},
    };

- (void) rasterise: (Camera *)cam
{
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    VectCol col = genColFromMapHeight(sRessortMap, sizeof(sRessortMap)/sizeof(MapCol), _modulus);
    glColor4v(&col);
    glBegin(GL_LINES);
    {
        glVertex3v(&_spaceAnchor1);
        glVertex3v(&_spaceAnchor2);
    }
    glEnd();
    glPopAttrib();
}

- (void) computeInteractionInSimulator:(GGDynamicSimulator*)simulator
{
    transformePoint4D(&_first->_position, &_firstAnchor, &_spaceAnchor1);
    transformePoint4D(&_second->_position, &_secondAnchor, &_spaceAnchor2);
    
    Vect3D AB;
    mkVectFromPoint(&_spaceAnchor1,&_spaceAnchor2,&AB);
    GGReal distance = normeVect(&AB);
    _modulus = (distance-_naturalLength)/_naturalLength;
    if(distance > EPS){
        GGReal fact = _power*(distance-_naturalLength)/distance;
        {
            Vect3D speed1, speed2, diffSpeed;
            [_first speedAtSolidPoint:&_firstAnchor speedResult:&speed1];
            [_second speedAtSolidPoint:&_secondAnchor speedResult:&speed2];
            diffVect(&speed2,&speed1,&diffSpeed);
            GGReal relativeSpeed = prodScal(&diffSpeed,&AB) / (distance*distance);
//            NSLog(@"relativeSpeed = %g", relativeSpeed);
            fact += FRICTION_PER_SPEED_FOR_CORRECTION*relativeSpeed;
        }
        mulScalVect(&AB,fact,&_cacheForce);
    }
    else{
        SetZeroVect(_cacheForce);
    }
    Vect3D GM, momemtum;
    mkVectFromPoint(&_first->_position.position,&_spaceAnchor1,&GM);
    prodVect(&GM,&_cacheForce,&momemtum);
    [_first addForce:_cacheForce
            momentum:momemtum];
    
    Vect3D oppositeForce;
    mulScalVect(&_cacheForce,-1.0,&oppositeForce);
    mkVectFromPoint(&_second->_position.position,&_spaceAnchor2,&GM);
    prodVect(&GM, &oppositeForce, &momemtum);
    [_second addForce:oppositeForce
            momentum:momemtum];
    
}

- (double) potentialEnergy
{
    return 0.5 * prodScal(&_cacheForce,&_cacheForce) / _power;
}
@end

@implementation GGStandardContact
- (id) initWithFirstBody:(GGSolid*)firstBody
            contactPoint:(Vect3D) pVect1
              secondBody:(GGSolid*)secondBody
            contactPoint:(Vect3D) pVect2
                inSystem:(GGLinearSystem*)system
{
    self = [super init];
    if(self){
        _first = firstBody;
        _second = secondBody;
        _contact1 = pVect1;
        _contact2 = pVect2;
        
        _formalAction = [[GGFormalVector alloc] initWithUknownInLinearSystem:system];
        _oppositeFormalAction = [[_formalAction oppositeVect] retain];
    }
    return self;
}

- (void) dealloc
{
    [_formalAction release];
    [_oppositeFormalAction release];
    [_name release];
    
    [super dealloc];
}

- (NSString *)name {
    return [[_name retain] autorelease];
}

- (void)setName:(NSString *)value {
    if (_name != value) {
        [_name release];
        _name = [value copy];
    }
}


- (GGSolid*) firstObject
{
    return _first;
}

- (GGSolid*) secondObject
{
    return _second;
}

- (void) computeInteractionInSimulator:(GGDynamicSimulator*)simulator
{
    NSAssert(_first && _second, @"should have 2 valid abjects there");

    Vect3D correctionForce;
    Vect3D correctionMomentum;
    Vect3D diffSpeed;
    {
        Vect3D drift;
        transformePoint4D(&_first->_position, &_contact1, &_articulationPosition);
        transformePoint4D(&_second->_position, &_contact2, &_articulationPosition2);
        mkVectFromPoint(&_articulationPosition,&_articulationPosition2,&drift);
        
        GGReal distance = 0.0;
        if(normaliseVectAndGetNorm(&drift, &distance)){
            GGReal coef = SPRING_FOR_CORRECTION*distance;
            Vect3D speed1, speed2;
            [_first speedAtSolidPoint:&_contact1 speedResult:&speed1];
            [_second speedAtSolidPoint:&_contact2 speedResult:&speed2];
            diffVect(&speed2,&speed1,&diffSpeed);
            GGReal frictionCoef = prodScal(&drift,&diffSpeed)*FRICTION_PER_SPEED_FOR_CORRECTION;
            
            if(frictionCoef < -coef){
                frictionCoef = -coef;
            }
            else if(frictionCoef > coef){
                frictionCoef = coef;
            }

//            if(fabs(frictionCoef) < coef){
////                coef += frictionCoef;
//            }
            coef += frictionCoef;

            mulScalVect(&drift,coef,&correctionForce);
        }
        else{
            correctionForce = GGZeroVect;
        }

#ifdef DEBUG
        [simulator addForce:correctionForce 
                   location:_articulationPosition
                       name:@"CorrectionPosition"
                interaction:self];
        [simulator addFormalForce:_formalAction
                         location:_articulationPosition
                             name:@"Reaction"
                      interaction:self];
#endif
    }
    
    Vect3D MG;
    mkVectFromPoint(&_articulationPosition,&_first->_position.position,&MG);
    prodVect(&correctionForce,&MG,&correctionMomentum);
    GGFormalVector* momentum = [_formalAction produitVectoriel:&MG];
    [_first addFormalForce:_formalAction
            formalMomentum:momentum
     correctionFormalForce:correctionForce
  correctionFormalMomentum:correctionMomentum];
    
    mkVectFromPoint(&_articulationPosition2,&_second->_position.position,&MG);
    momentum = [_oppositeFormalAction produitVectoriel:&MG];
    mulScalVect(&correctionForce,-1.0,&correctionForce);
    prodVect(&correctionForce,&MG,&correctionMomentum);
    [_second addFormalForce:_oppositeFormalAction
            formalMomentum:momentum
     correctionFormalForce:correctionForce
  correctionFormalMomentum:correctionMomentum];
}

- (void) feedSystem:(GGLinearSystem*) problem
{
    GGMutableFormalVector* system = [[_first formalAccelerationAtSolidPoint:&_contact1] mutableCopy];
    GGFormalVector* accel2 = [_second formalAccelerationAtSolidPoint:&_contact2];
    [system substractVect:accel2];
    [problem addVectorialCondition:system];
    [system release];
}

- (void) addInSimulator:(GGDynamicSimulator*)simulator
{
}

- (void) rasterise: (Camera *)cam
{
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glColor3f(0,1,1);
    glBegin(GL_POINTS);
    {
        glVertex3v(&_articulationPosition);
    }
    glEnd();
    glPopAttrib();
}

- (double) potentialEnergy
{
    return 0.0;
}

@end