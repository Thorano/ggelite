#import "GGSolid.h"

@interface GGSolid (Private)
- (void) advanceTimeByDelta:(double)delta
               acceleration:(Vect3D) acceleration
        angularAcceleration:(Vect3D) angularAcceleration;
@end