//
//  GGDynamicSimulator.m
//  elite
//
//  Created by Frédéric De Jaeger on 12/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGDynamicSimulator.h"

#import <AppKit/AppKit.h>
#import "GGLinearSystem.h"
#import "GGSolid.h"
#import "GGCollision.h"
#import "GGGravitation.h"
#import "GGLog.h"
#import "GGMainLoop.h"
#import "GGInteraction.h"
#import "GGCharniere.h"
#import "GGFormalVector.h"
#import "utile.h"

@implementation GGDynamicSimulator
- (id) init
{
    self = [super init];
    if(self){
        _solids = [[NSMutableArray alloc] initWithCapacity:101]; 
        _interactions = [[NSMutableArray alloc] initWithCapacity:203];
        _formalInteractions = [[NSMutableArray alloc] initWithCapacity:57];
        _linearSystem = [GGLinearSystem new];
        _simulationTime = 0.0;
        _currentTime = 0.0;
        _useGameTime = YES;
        _gravity = GGZeroVect;
    }
    
    return self;
}

- (void) dealloc
{
    [_linearSystem release];
    [_collisionManager release];
    [_solids release];
    [_interactions release];
    [_formalInteractions release];
    [_contactsInteractions release];

#ifdef DEBUG
    [_effectsToObserve release];
    [observable release];
#endif
    
    [super dealloc];
}

- (void) _computeEnergy
{
#if DEBUG_COCOA
    double inertialEnergy = 0.0;
    double rotationEnergy = 0.0;
    int i, count = [_solids count];
    for(i = 0; i < count; ++i){
        GGSolid* solid = [_solids objectAtIndex:i];
        double tmpRot = 0.0;
        inertialEnergy += [solid inertialEnergy:&tmpRot];
        rotationEnergy += tmpRot;
    }
    
    double potentialEnergy = 0.0;
    count = [_interactions count];
    for(i = 0; i < count; ++i){
        id<GGInteractionProtocol> interaction = [_interactions objectAtIndex:i];
        potentialEnergy += [interaction potentialEnergy];
    }

    count = [_formalInteractions count];
    for(i = 0; i < count; ++i){
        id<GGFormalInteractionProtocol> interaction = [_formalInteractions objectAtIndex:i];
        potentialEnergy += [interaction potentialEnergy];
    }
    
    GGDynamicDebugController* controller = [GGDynamicDebugController controller];
    [controller setPotentialEnergy:potentialEnergy];
    [controller setInertialEnergy:inertialEnergy];
    [controller setRotationalEnergy:rotationEnergy];
    [controller setEnergy:potentialEnergy+inertialEnergy+rotationEnergy];
#endif
}

//static inline Vect3D 

static  GGReal partDenom(GGSolid* obj, Vect3D *GM, Vect3D *normal)
{
    Vect3D tmp, tmp2;
    prodVect(GM, normal,&tmp);
    // we have to convert to local coordinates
    transposeProduitVect3D(&obj->_position, &tmp, &tmp2);
    produitMatriceVecteur3D(&obj->_inertialInverse,&tmp2,&tmp);
    
    // we transforme back to global coordinates
    produitMatriceVecteur3D(&obj->_position,&tmp,&tmp2);
    prodVect(&tmp2,GM,&tmp);
    
    return prodScal(&tmp,normal);    
}

static  void applyImpulsion(GGSolid* obj, Vect3D *impulsion, Vect3D* GM, GGReal coef)
{
    addLambdaVect(&obj->_speed,coef*(obj->_masseInverse),
                  impulsion,&obj->_speed);
    
    Vect3D tmp, tmp2;
    prodVect(GM,impulsion,&tmp);
    
    // in local coordinates
    transposeProduitVect3D(&obj->_position, &tmp, &tmp2);
    produitMatriceVecteur3D(&obj->_inertialInverse,&tmp2,&tmp);
    addLambdaVect(&obj->_angularSpeed, coef, &tmp,
                  &obj->_angularSpeed);
}

static inline void computeSpeedFromDeltaG(GGSolid* obj, Vect3D* GM, Vect3D* res)
{
    Vect3D  tmp, tmp2;
    produitMatriceVecteur3D(&obj->_position,&obj->_angularSpeed,&tmp);
    prodVect(&tmp,GM,&tmp2);
    addVect(&obj->_speed,&tmp2,res);
}

//#define REPOS 1

#if !REPOS
static double  REACTION_PER_DEPTH = 100.0;
static double  DYNAMIC_FRICTION = 1.0;
#endif
static double  THRESOLD = 0.6517857313156128;
static double  EPSRESPONSE = 0.1428571492433548;
static double  MuSlide = 0.0892857164144516;

- (void) addTemporaryContact:(CollisionData*)cData
{
    if(nil == _contactsInteractions){
        _contactsInteractions = [NSMutableArray new];
    }
    
    
}

- (void) manageCollisions
{
    [_contactsInteractions removeAllObjects];
    [_collisionManager findCollisionsFaster];
//    [_collisionManager findCollision];
    int size;
    CollisionData* cData = [_collisionManager collectedCollisions:&size];
    
    int i;
#if REPOS
    unsigned maxIdentifer = [_collisionManager numberOfObjects];
#endif
    
#ifdef DEBUG
    if(size > 0){
        NSDebugILLog(@"Collision", @"found %d collision", size);
        if(_pauseOnCollisions){
            [self setIsPaused:YES];
        }
    }
#endif
    
    if([self isPaused]){
        return;
    }

    NSMutableIndexSet* handleReposition = [[NSMutableIndexSet alloc] init];
    for(i = 0; i < size;  ++i){
        GGSolid* obj1 = [_collisionManager objectInCollidableAtIndex:cData[i].index1];
        GGSolid* obj2 = [_collisionManager objectInCollidableAtIndex:cData[i].index2];
        
        NSDebugMLLog(@"Collision", @"(%d)collision %d, %d:\nloc:%@\nnormal:%@\ndepth:%g", i,
                     cData[i].index1, cData[i].index2,
                     stringOfVect(&cData[i].location), 
                     stringOfVect(&cData[i].normal), cData[i].depth);
        // obj2 est la masse (aka le collisione)
        
//        NSDebugMLLog(@"Collision", @"obj1 = %d\nobj2 = %d", [obj1 indexForCollision], [obj2 indexForCollision]);

        
        Vect3D* normalPtr = &(cData[i].normal);  // normal  of obj2
        Vect3D v1, v2;
        Vect3D G1M, G2M;
        mkVectFromPoint(&obj1->_position.position, &(cData[i].location), &G1M);
        mkVectFromPoint(&obj2->_position.position, &(cData[i].location), &G2M);
        

        computeSpeedFromDeltaG(obj1, &G1M, &v1);
        computeSpeedFromDeltaG(obj2, &G2M, &v2);
     
        Vect3D vRelVect;
        diffVect(&v1,&v2,&vRelVect);
        
        GGReal vRel;
        vRel = prodScal(&vRelVect, normalPtr);
//        NSDebugMLLog(@"Collision", @"relative speed = %@\nnorm = %g",
//                           stringOfVect(&vRelVect), vRel);
        Vect3D vTangential;
        addLambdaVect(&vRelVect,-vRel,normalPtr,&vTangential);
        BOOL applyImpulse = NO;
        GGReal ImpulseFraction = 0.0;
        
        if(vRel >= THRESOLD){
            NSDebugMLLog(@"Collision", @"separating");
        }
        else if(vRel >= -THRESOLD){
            
#if REPOS
            unsigned ind1, ind2;
            ind1 = cData[i].index1;
            ind2 = cData[i].index2;
            NSAssert(ind1!=ind2, @"an object cannot collide itself");
            unsigned sig;
            if(ind1 < ind2){
                sig = maxIdentifer*ind1+ind2;
            }
            else{
                sig = maxIdentifer*ind2+ind1;
            }
            if([handleReposition containsIndex:sig]) continue;
            
            [handleReposition addIndex:sig];
            
            
            GGReal factMasse = 1.0/(obj1->_masseInverse + obj2->_masseInverse);
            GGReal alphaShift = cData[i].depth*factMasse;
            
            addLambdaVect(&obj1->_position.position,alphaShift*obj1->_masseInverse,normalPtr,
                          &obj1->_position.position);
            addLambdaVect(&obj2->_position.position,-alphaShift*obj2->_masseInverse,normalPtr,
                          &obj2->_position.position);
            
            
            if(vRel <= 0){
                applyImpulse = YES;
                ImpulseFraction = 0.0;
//                alphaShift = -vRel*factMasse;
//                addLambdaVect(&obj1->_speed,alphaShift*obj1->_masseInverse,normalPtr,
//                              &obj1->_speed);
//                addLambdaVect(&obj2->_speed,alphaShift*obj2->_masseInverse,normalPtr,
//                              &obj2->_speed);
            }
            NSDebugMLLog(@"Collision", @"Repos %@ (%d):\ndepth: %g, amort = %s", obj1,
                         i, cData[i].depth, applyImpulse ? "YES" : "NO");
#else            
            Vect3D reaction = *normalPtr;
            Vect3D slideDir = vTangential;
            if(normaliseVect(&slideDir)){
                addLambdaVect(&reaction,-MuSlide,&slideDir,
                              &reaction);
            }

            mulScalVect(&reaction,REACTION_PER_DEPTH*cData[i].depth-vRel*DYNAMIC_FRICTION,&reaction);
            [obj1 addForce:reaction atPoint:cData[i].location];
            mulScalVect(&reaction, -1, &reaction);
            [obj2 addForce:reaction atPoint:cData[i].location];
            NSDebugMLLog(@"Collision", @"Repos %@ (%d):\ndepth: %g, %g", obj1,
                         i, cData[i].depth,
                         normeVect(&reaction));
            if(_firstRunInFrame){
                NSDebugMLLogWindow(@"Collision", @"%@ (%d):\ndepth: %g, %g", obj1,
                                   i, cData[i].depth,
                                   normeVect(&reaction));
            }
#endif
        }
        else{
            applyImpulse = YES;
            ImpulseFraction = EPSRESPONSE;
        }
        if(applyImpulse) {
            
            // collision
            NSDebugMLLog(@"Collision", @"resolving %d, %d:\nloc:%@\nnormal:%@\ndepth:%g", 
                         cData[i].index1, cData[i].index2,
                         stringOfVect(&cData[i].location), 
                         stringOfVect(&cData[i].normal), cData[i].depth);
            
            GGReal denom = obj1->_masseInverse + obj2->_masseInverse;
            
            denom += partDenom(obj1, &G1M, normalPtr);
            denom += partDenom(obj2, &G2M, normalPtr);
            
            GGReal impulsion = - (1 + EPSRESPONSE) * vRel / denom;
            NSDebugMLLog(@"Collision", @"impulstion = %g", impulsion);
            
            Vect3D impulsionVect;
            mulScalVect(normalPtr,impulsion,&impulsionVect);
            Vect3D slideDir = vTangential;
            if(normaliseVect(&slideDir)){
                addLambdaVect(&impulsionVect,-impulsion*MuSlide,&slideDir,
                              &impulsionVect);
            }
            
            applyImpulsion(obj1, &impulsionVect, &G1M, 1.0);
            applyImpulsion(obj2, &impulsionVect, &G2M, -1.0);
        }            
    }
    [handleReposition release];
}

- (void) _advanceTimeByDelta:(double)delta
{
    [_solids makeObjectsPerformSelector:@selector(resetActions)];

    if(_collisionManager){
        [self manageCollisions];
    }
    
#ifdef DEBUG
    [_effectsToObserve removeAllObjects];
#endif
    [_interactions makeObjectsPerformSelector:@selector(computeInteractionInSimulator:)
                                   withObject:self];
    [_formalInteractions makeObjectsPerformSelector:@selector(computeInteractionInSimulator:)
                                         withObject:self];
#ifdef DEBUG
    [self setObservable:_effectsToObserve];
    if(_shouldUpdate){
        _shouldUpdate = NO;
    }
#endif
    int i, n = [_solids count];
    
    for(i = 0; i < n; ++i){
        GGSolid* aSolid = [_solids objectAtIndex:i];
        if([aSolid hasGravity]){
            Vect3D grav;
            mulScalVect(&_gravity,aSolid->_masse,&grav);
            [aSolid addForce:grav momentum:GGZeroVect];
        }
    }
    _currentFrame++;
    
    NSMutableArray* badGuys; // objects with a formal interaction.
    badGuys = [[NSMutableArray alloc] initWithCapacity:[_solids count]];
    for(i = 0; i < n; ++i){
        GGSolid* aSolid = [_solids objectAtIndex:i];
//        NSDebugMLLog(@"HOHO", @"%@:mat:\n%@", [aSolid model], stringOfMatrice(&aSolid->_position));
        if([aSolid hasFormalInteractions]){
            [aSolid computeFormalAccelerations];
            [badGuys addObject:aSolid];
        }
        else{
            [aSolid advanceTimeByDelta:delta];
        }
    }
    
    // now, on bad guys.
    n = [badGuys count];
    if(n > 0){
        // there are formal objects.
        [_formalInteractions makeObjectsPerformSelector:@selector(feedSystem:)
                                             withObject:_linearSystem];
        BOOL foundSolution = [_linearSystem solveSystem];
        [_linearSystem resetSystem];
        if(foundSolution){
            for(i = 0; i < n; ++i){
                GGSolid* aSolid = [badGuys objectAtIndex:i];
                [aSolid advanceTimeByDelta:delta
                           formalSolutions:_linearSystem];
            }
        }
    }
    [badGuys release];
    
    _firstRunInFrame = NO;
}

static float SLICE = 0.01;
- (void) advanceTimeByDelta:(double)delta
{
    _currentTime += delta;
    _firstRunInFrame = YES;
    if(SLICE == 0.0 || _isPaused || delta == 0.0){
//        [_interactions makeObjectsPerformSelector:@selector(computeInteractionInSimulator:)
//                                       withObject:self];
        _simulationTime = _currentTime;
        [self _advanceTimeByDelta:0];   // to have collision computed while paused.
    }
    else{
        double incr = _useGameTime ? deltaTime : SLICE;
        while(_simulationTime < _currentTime){
            [self _advanceTimeByDelta:incr];
            _simulationTime += incr;
        }
    }

    [self _computeEnergy];
}

- (void) setGravity:(Vect3D) g
{
    _gravity = g;
}

- (void) addBody:(GGSolid*)solid
{
    NSParameterAssert(_solids != nil);
    [solid setSimulator:self];
    [_solids addObject:solid];
}

- (void) addInteraction:(id<GGInteractionProtocol>)interaction
{
    NSParameterAssert(_interactions != nil);
    [interaction addInSimulator:self];
    [_interactions addObject:interaction];
}

- (void) addFormalInteraction:(id<GGFormalInteractionProtocol>)formalInteraction
{
//    NSLog(@"adding %@", formalInteraction);
    NSParameterAssert(_formalInteractions!=nil);
    [formalInteraction addInSimulator:self];
    [_formalInteractions addObject:formalInteraction];
}

- (void) addForCollision:(GGSolid*)solid
{
    [[self collisionManager] addObject:solid];
}

- (void) removeSolidFromSimulation:(GGSolid*)solid
{
    int index = [_solids indexOfObjectIdenticalTo:solid];
    NSAssert(index != NSNotFound, @"removing a solid we don't know");
    if(index != NSNotFound){
        [solid setSimulator:nil];
        [_solids removeObjectAtIndex:index];
        [[self collisionManager] removeObject:solid];
    }
}

- (GGCollisionManager*) collisionManager
{
    if(nil == _collisionManager){
        _collisionManager = [GGCollisionManager new];
    }
    
    return _collisionManager;
}

- (GGLinearSystem*) system
{
    return _linearSystem;
}

#pragma mark debug stuff

#if DEBUG_COCOA
- (void) _addFormalForce:(GGFormalVector*)formalForce
               location:(Vect3D)pos
                   name:(NSString*)forceName
            interaction:(id<GGInteractionProtocol>)interaction
{
    if(nil == _effectsToObserve){
        _effectsToObserve = [[NSMutableArray alloc] init];
    }
    GGFormalVector *formalVector = [[GGFormalVector alloc] initWithVect:&pos];
    NSDictionary* dico = [[NSDictionary alloc] initWithObjectsAndKeys:
        formalForce, @"FormalForce",
        formalVector, @"FormalPos",
        forceName, @"Name",
        nil];
    [formalVector release];
    
    if(_shouldUpdate) [self willChangeValueForKey:@"_effectsToObserve"];
    [_effectsToObserve addObject:dico];
    if(_shouldUpdate){
        [self didChangeValueForKey:@"_effectsToObserve"];
        NSLog(@"bla %@", dico);
    } 
    [dico release];
}
#endif

- (void) addForce:(Vect3D)force
         location:(Vect3D)pos
             name:(NSString*)forceName
      interaction:(id<GGInteractionProtocol>)interaction
{
#if DEBUG_COCOA
    if([[GGDynamicDebugController controller] isInteractionSelected:interaction]){
        GGFormalVector *formalVector = [[GGFormalVector alloc] initWithVect:&force];
        [self _addFormalForce:formalVector
                    location:pos
                        name:forceName
                 interaction:interaction];
        [formalVector release];
    }
#endif
}

- (void) addFormalForce:(GGFormalVector*)formalForce
               location:(Vect3D)pos
                   name:(NSString*)forceName
            interaction:(id<GGInteractionProtocol>)interaction
{
#if DEBUG_COCOA
    if([[GGDynamicDebugController controller] isInteractionSelected:interaction]){
        [self _addFormalForce:formalForce
                     location:pos
                         name:forceName
                  interaction:interaction];
    }
#endif
}

#ifdef DEBUG
- (void) invalidateEffects
{
    _shouldUpdate = YES;
}

- (NSArray *)observable {
    return [[observable retain] autorelease];
}

- (void)setObservable:(NSArray *)value {
    if (observable != value) {
        [observable release];
        observable = [value copy];
    }
}

- (BOOL)showSolids {
    return _showSolids;
}

- (void)setShowSolids:(BOOL)value {
    if (_showSolids != value) {
        _showSolids = value;
    }
}

- (BOOL)showCollisions {
    return _showCollisions;
}

- (void)setShowCollisions:(BOOL)value {
    if (_showCollisions != value) {
        _showCollisions = value;
    }
}

- (BOOL)pauseOnCollisions {
    return _pauseOnCollisions;
}

- (BOOL)showCollisionsBoxes {
    return _showCollisionsBoxes;
}

- (void)setShowCollisionsBoxes:(BOOL)value {
    if (_showCollisionsBoxes != value) {
        _showCollisionsBoxes = value;
    }
}

- (void)setPauseOnCollisions:(BOOL)value {
    if (_pauseOnCollisions != value) {
        _pauseOnCollisions = value;
    }
}

- (void) rasterise: (Camera *)cam
{    
    if(_showSolids){
        [_solids makeObjectsPerformSelector:@selector(rasterise:) withObject:(id)cam];        
    }
    [_interactions makeObjectsPerformSelector:@selector(rasterise:) withObject:(id)cam];
    //    [_formalInteractions makeObjectsPerformSelector:@selector(rasterize)];
    
    if(_showCollisions){
        int nCollision;
        CollisionData* colData = [_collisionManager collectedCollisions:&nCollision];
        if(nCollision > 0){
            int i;
            glPushAttrib(GL_ENABLE_BIT);
            glDisable(GL_LIGHTING);
            glDisable(GL_DEPTH_TEST);
            pushStandardRep(NSMakeSize(cam->param->width, cam->param->height));
            glColor3f(0.5, 0.9, 0.5);
            glBegin(GL_LINES);
            for(i = 0; i < nCollision; ++i){
                Vect3D screenPos;
                if(getWinCoordinates(cam, &colData[i].location, &screenPos )){
                    glVertex2f(screenPos.x-5,screenPos.y);
                    glVertex2f(screenPos.x+5,screenPos.y);
                    glVertex2f(screenPos.x,screenPos.y-5);
                    glVertex2f(screenPos.x,screenPos.y+5);
                }
            }            
            glEnd();
            popStandardRep();
            glPopAttrib();
        }
    }
    
#if DEBUG_COCOA
    {
        unsigned i, count = [[self observable] count];
        if(count > 0){
            NSIndexSet* index = [[GGDynamicDebugController controller] selectedRowIndexes];
            glPushAttrib(GL_ENABLE_BIT);
            glDisable(GL_LIGHTING);
            glDisable(GL_DEPTH_TEST);
            
            for(i = 0; i < count; ++i){
                NSDictionary* info = [[self observable] objectAtIndex:i];
                GGFormalVector* fforce = [info objectForKey:@"FormalForce"];
                GGFormalVector* fpos = [info objectForKey:@"FormalPos"];
                Vect3D force, pos;
                [fforce evaluateInSolutions:_linearSystem
                               vectorResult:&force];
                [fpos evaluateInSolutions:_linearSystem
                             vectorResult:&pos];
                
                glPushMatrix();
                {
                    glTranslatef(pos.x,pos.y,pos.z);
                    glScalef(1,1,1);
                    glBegin(GL_LINES);
                    {
                        if(index && [index containsIndex:i]){
                            glColor3f(1,0,0);
                        }
                        else{
                            glColor3f(0,1,0);
                        }
                        glVertex3f(0,0,0);
                        glVertex3v(&force);
                    }
                    glEnd();
                }
                glPopMatrix();        
            }        
            glPopAttrib();
        }
    }
#endif    
    
    if([self showCollisionsBoxes]){
        [_collisionManager rasterise:cam];        
    }
}
#endif


- (BOOL)isPaused {
    return _isPaused;
}

- (void)setIsPaused:(BOOL)value {
    if (_isPaused != value) {
        _isPaused = value;
    }
}

- (BOOL)useGameTime {
    return _useGameTime;
}

- (void)setUseGameTime:(BOOL)value {
    _useGameTime = value;
}
@end

#pragma mark -

#if DEBUG_COCOA
#import "GGDebugController.h"

@implementation GGDynamicDebugController
+ (GGDynamicDebugController*) controller
{
    return [[GGDebugController controller] dynamicController];
}

- (void) dealloc
{
    [_simulator release];
    
    [super dealloc];
}

- (BOOL) isInteractionSelected:(id<GGInteractionProtocol>)interaction
{
    if(interaction && [[formalInteractions selectedObjects] indexOfObjectIdenticalTo:interaction] != NSNotFound){
        return YES;
    }
    else{
        return NO;
    }
}

#pragma mark actions
- (IBAction) reloadCollisionItem:(id)sender
{
    GGDynamicSimulator* simulator = [[self simulator] retain];
    [self setSimulator:nil];
    [self setSimulator:simulator];
    [simulator release];
    NSLog(@"value = %@", [[[self simulator] collisionManager] collisionNodes]);
}

#pragma mark table view delegate
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
    [[self simulator] invalidateEffects];
    NSLog(@"reload");
    [tableView reloadData];
}

- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
    int ret = [[[self simulator] observable] count];
//    NSLog(@"numberOfRows = %d", ret);
    return ret;
}

- (id)tableView:(NSTableView *)aTableView objectValueForTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
    NSArray* observable = [[self simulator] observable];
    if(observable == nil || [observable count] <= rowIndex){
        NSLog(@"ask for %d, size if %d", rowIndex, [observable count]);
        return nil;
    }
    else{
        return [[observable objectAtIndex:rowIndex] objectForKey:@"Name"];
    }
}

- (NSIndexSet *)selectedRowIndexes
{
    return [tableView selectedRowIndexes];
}

- (NSArray*) selectionIndexPaths
{
    return [collisionNodes selectionIndexPaths];
}

- (NSArray*) selectedNodes
{
    return [collisionNodes selectedObjects];
}


#pragma mark -
#pragma mark KVC stuff
- (BOOL)ignoreFormalInteractions {
    return _ignoreFormalInteractions;
}

- (void)setIgnoreFormalInteractions:(BOOL)value {
    if (_ignoreFormalInteractions != value) {
        _ignoreFormalInteractions = value;
    }
}

- (float)energy {
    return _energy;
}

- (void)setEnergy:(float)value {
        _energy = value;
}

- (float)potentialEnergy {
    return _potentialEnergy;
}

- (void)setPotentialEnergy:(float)value {
    if (_potentialEnergy != value) {
        _potentialEnergy = value;
    }
}

- (float)inertialEnergy {
    return _inertialEnergy;
}

- (void)setInertialEnergy:(float)value {
    if (_inertialEnergy != value) {
        _inertialEnergy = value;
    }
}

- (float)rotationalEnergy {
    return _rotationalEnergy;
}

- (void)setRotationalEnergy:(float)value {
    if (_rotationalEnergy != value) {
        _rotationalEnergy = value;
    }
}

- (float) gravite
{
    return gravitationIntensity;
}

- (void) setGravite:(float)val
{
    gravitationIntensity = val;
}

#pragma mark parameters for the correction
- (double) frictionForCorrection
{
    return FRICTION_PER_SPEED_FOR_CORRECTION;
}

- (void) setFrictionForCorrection:(double)val
{
    FRICTION_PER_SPEED_FOR_CORRECTION = val;
}

- (double) springForCorrection
{
    return SPRING_FOR_CORRECTION;
}

- (void) setSpringForCorrection:(double)val
{
    SPRING_FOR_CORRECTION = val;
}

- (double) correctionTorque
{
    return CORRECTION_TORQUE;
}

- (void) setCorrectionTorque:(double)val
{
    CORRECTION_TORQUE = val;
}

- (double) correctionAngFriction
{
    return CORRECTION_ANG_FRICTION;
}

- (void) setCorrectionAngFriction:(double)val
{
    CORRECTION_ANG_FRICTION = val;
}

#pragma mark parameters for the collision

- (double) dynamicFriction
{
#if !REPOS
    return DYNAMIC_FRICTION;
#else
    return 0.0;
#endif    
}

- (void)  setDynamicFriction:(double)val
{
#if !REPOS
    DYNAMIC_FRICTION = val;
#endif
}

- (double) reactionPerDepth
{
#if !REPOS
    return REACTION_PER_DEPTH;
#else
    return 0.0;
#endif    
}

- (void) setReactionPerDepth:(double)val
{
#if !REPOS
    REACTION_PER_DEPTH = val;
#endif
}

- (double) thresold
{
    return THRESOLD;
}

- (void) setThresold:(double)val
{
    THRESOLD = val;
}

- (double) epsilonReponse
{
    return EPSRESPONSE;
}

- (void) setEpsilonReponse:(double)val
{
    EPSRESPONSE = val;
}

- (double) muSlide
{
    return MuSlide;
}

- (void) setMuSlide:(double)val
{
    MuSlide = val;
}

- (void) setTimeSlice:(double)val
{
    if(val <= -4.0){
        SLICE = 0.0;
    }
    else{
        SLICE = pow(10, val);
    }
    NSLog(@"SLICE = %g", SLICE);
}

- (double) timeSlice
{
    if(SLICE == 0.0){
        return -4.0;
    }
    else{
        return log10(SLICE);
    }
}

- (GGDynamicSimulator *)simulator {
    return [[_simulator retain] autorelease];
}

- (void)setSimulator:(GGDynamicSimulator *)value {
    if (_simulator != value) {
        [_simulator release];
        _simulator = [value retain];
    }
}

@end
#endif