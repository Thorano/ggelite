//
//  GGPseudoFormalInteraction.h
//  elite
//
//  Created by Frederic De Jaeger on 29/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GGDynamicProtocol.h"

@class GGFormalVector;

@interface GGPseudoFormalInteraction : NSObject <GGFormalInteractionProtocol>
{
    id<GGInteractionProtocol>           _originalInteraction;
    Vect3D                              _cacheAction;
    Vect3D                              _cacheMomentum;
    
    GGFormalVector*                     _formalAction;
    GGFormalVector*                     _formalMomentum;
}
- initWithInteraction:(id<GGInteractionProtocol>)interaction;
@end
