//
//  GGConstrainedSolid.h
//  elite
//
//  Created by Frederic De Jaeger on 26/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGSolid.h"


@interface GGConstrainedSolid : GGSolid {
    Vect3D              _currentAcceleration;
    Vect3D              _currentAngularAcceleration;
}

@end
