//
//  GGGravitation.m
//  elite
//
//  Created by Frederic De Jaeger on 29/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGGravitation.h"
#import "GGSolid.h"
#import "GGDynamicSimulator.h"
#import "utile.h"

float gravitationIntensity = 5.178571;

@implementation GGGravitation
- (id) initWithBody:(GGSolid*)firstBody
{
    _body = firstBody;
    
    return self;
}

- (void) dealloc
{
    [_name release];
    
    [super dealloc];
}

- (NSString *)name {
    return [[_name retain] autorelease];
}

- (void)setName:(NSString *)value {
    if (_name != value) {
        [_name release];
        _name = [value copy];
    }
}



- (GGSolid*) firstObject
{
    return _body;
}

- (GGSolid*) secondObject
{
    ggabort();
    return nil;
}

- (void) addInSimulator:(GGDynamicSimulator*)simulator
{
//    [_body addInteraction:self];
}

- (void) computeInteractionInSimulator:(GGDynamicSimulator*)simulator
{
    Vect3D force = mkVect(0,0,-_body->_masse*gravitationIntensity);
    [_body addForce:force
           momentum:GGZeroVect];
}

- (void) rasterise: (Camera *)cam
{
    
}

- (double) potentialEnergy
{
    return _body->_masse*_body->_position.position.z * gravitationIntensity;
}

@end

@implementation GGSolid (GravitaionExtension)
- (void) addGravity
{
    GGGravitation* grav = [[GGGravitation alloc] initWithBody:self];
    [grav setName:@"Gravitation"];
    [[self simulator] addInteraction:grav];
    [grav release];
}
@end