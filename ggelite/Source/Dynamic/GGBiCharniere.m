//
//  GGBiCharniere.m
//  elite
//
//  Created by Frédéric De Jaeger on 5/20/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGBiCharniere.h"

#import "GGFormalVector.h"
#import "GGSolid.h"
#import "GGLinearSystem.h"
#import "utile.h"

@implementation GGBiCharniere
- (void) computeInteractionInSimulator:(GGDynamicSimulator*)simulator
{
    [super computeInteractionInSimulator:simulator];

    GGMatrix4 secondToFirst;
    transposeProduit(&_first->_position,&_second->_position,&secondToFirst);

    Vect3D dir2in1;
    Vect3D angularSpeed2in1;

    produitMatriceVecteur3D(&secondToFirst,&_direction2,&dir2in1);
    produitMatriceVecteur3D(&secondToFirst,&_second->_angularSpeed,&angularSpeed2in1);
    
    Vect3D dir1VDir2;
    prodVect(&_direction1,&dir2in1,&dir1VDir2);
    GGReal dir1SDir2 = prodScal(&_direction1,&dir2in1);
    Vect3D correction;
    mulScalVect(&dir1VDir2,-dir1SDir2*CORRECTION_TORQUE,&correction);
    
    Vect3D diffAngularSpeed;
    diffVect(&_first->_angularSpeed,&angularSpeed2in1,&diffAngularSpeed);
    addLambdaVect(&correction,-prodScal(&dir1VDir2,&diffAngularSpeed)*CORRECTION_ANG_FRICTION,
                  &dir1VDir2, &correction);
    
    [_first addFormalForce:nil
            formalMomentum:_formalMomentum
     correctionFormalForce:GGZeroVect 
  correctionFormalMomentum:correction];
    
    mulScalVect(&correction,-1,&correction);

    [_second addFormalForce:nil
             formalMomentum:_oppositeFormalMomentum
      correctionFormalForce:GGZeroVect
   correctionFormalMomentum:correction];
}

- (void) feedSystem:(GGLinearSystem*) problem
{
    [super feedSystem:problem];
    
    // compute equation on torque.
    GGMatrix4 secondToFirst;
    transposeProduit(&_first->_position,&_second->_position,&secondToFirst);
    
    GGReal term1;
    Vect3D dir2in1;
    Vect3D angularSpeed2in1;
    Vect3D tmp;
    {
        // (\omega_1 - \omega_2).((\omega_1 ^ d_1)^d_2 + d_1 ^ (\omega_2 ^ d_2))
        produitMatriceVecteur3D(&secondToFirst,&_direction2,&dir2in1);
        produitMatriceVecteur3D(&secondToFirst,&_second->_angularSpeed,&angularSpeed2in1);
        
        Vect3D diffAngularSpeed;
        diffVect(&_first->_angularSpeed,&angularSpeed2in1,&diffAngularSpeed);
        Vect3D angularMomentumIn1;
        Vect3D angularMomentumIn2, tmp2;
        prodVect(&_first->_angularSpeed,&_direction1,&angularMomentumIn1);
        prodVect(&angularMomentumIn1,&dir2in1,&tmp);

        prodVect(&angularSpeed2in1,&dir2in1,&angularMomentumIn2);
        prodVect(&_direction1,&angularMomentumIn2,&tmp2);
        addVect(&tmp,&tmp2,&tmp);
        
        term1 = prodScal(&tmp,&diffAngularSpeed);
    }
    
    GGMutableLinearPoly* torqueEquation;
    {
        prodVect(&_direction1,&dir2in1,&tmp);
        GGMutableFormalVector* diffAngAccel;
        {
            GGFormalVector* angAccel2InFirst;
            {
                GGFormalVector* angAccel2 = [_second formalAngularAcceleration];
                angAccel2InFirst = [angAccel2 transformWithMatrix3D:&secondToFirst];
            }
            diffAngAccel = [[_first formalAngularAcceleration] mutableCopy];
            [diffAngAccel substractVect:angAccel2InFirst];
        }
        
        
        torqueEquation = [diffAngAccel produitScalaire:&tmp];
        [diffAngAccel release];
        [torqueEquation addConstant:-term1];
    }
    
    [problem addScalarCondition:torqueEquation];
    produitMatriceVecteur3D(&_first->_position,&_direction1,&tmp);
    GGLinearPoly* scal = [_formalMomentum produitScalaire:&tmp];
    [problem addScalarCondition:scal];

    produitMatriceVecteur3D(&_second->_position,&_direction2,&tmp);
    scal = [_formalMomentum produitScalaire:&tmp];
    [problem addScalarCondition:scal];    
}
@end
