//
//  GGCharniere.m
//  elite
//
//  Created by Frederic De Jaeger on 15/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGCharniere.h"

#import "GGDynamicSimulator.h"
#import "GGFormalVector.h"
#import "GGSolid.h"
#import "GGLinearSystem.h"
#import "utile.h"

float CORRECTION_TORQUE = 10.0;
float CORRECTION_ANG_FRICTION = 10.0;

@implementation GGCommonCharniere
- (id) initWithFirstBody:(GGSolid*)firstBody
            contactPoint:(Vect3D) pVect1
               direction:(Vect3D) pDir1
              secondBody:(GGSolid*)secondBody
            contactPoint:(Vect3D) pVect2
               direction:(Vect3D) pDir2
                inSystem:(GGLinearSystem*)system
{
    self = [super initWithFirstBody:firstBody
                       contactPoint:pVect1
                         secondBody:secondBody
                       contactPoint:pVect2
                           inSystem:system];
    if(self){
        _direction1 = pDir1;
        _direction2 = pDir2;
        
        _formalMomentum = [[GGFormalVector alloc] initWithUknownInLinearSystem:system];
        _oppositeFormalMomentum = [[_formalMomentum oppositeVect] retain];        
    }
    
    return self;
}

- (void) dealloc
{
    [_formalMomentum release];
    [_oppositeFormalMomentum release];
    
    [super dealloc];
}

- (void) rasterise: (Camera *)cam
{
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glPushMatrix();
    {
        glMultMatrix(&_first->_position);
        glTranslatef(_contact1.x,_contact1.y,_contact1.z);
        glScalef(5,5,5);
        glBegin(GL_LINES);
        {
            glColor3f(0,1,1);
            glVertex3f(0,0,0);
            glVertex3v(&_direction1);
        }
        glEnd();
    }
    glPopMatrix();        
    glPushMatrix();
    {
        glMultMatrix(&_second->_position);
        glTranslatef(_contact2.x,_contact2.y,_contact2.z);
        glScalef(5,5,5);
        glBegin(GL_LINES);
        {
            glColor3f(1,0,1);
            glVertex3f(0,0,0);
            glVertex3v(&_direction2);
        }
        glEnd();
    }
    glPopMatrix();        
    glPopAttrib();
}
@end

@implementation GGCharniere
- (void) computeInteractionInSimulator:(GGDynamicSimulator*)simulator
{
    [super computeInteractionInSimulator:simulator];
    
    produitMatriceVecteur3D(&_first->_position, &_direction1, 
                            &_articulationDirection1);
    
    Vect3D artDir2;
    Vect3D correctionTorque;
    produitMatriceVecteur3D(&_second->_position,&_direction2,&artDir2);
    NSDebugMLLogWindow(@"charniere", @"primDir1 = \n%@\nprimDir2=%@", stringOfVect(&_direction1),
                       stringOfVect(&_direction2));
    NSDebugMLLogWindow(@"charniere", @"dir1 = \n%@\ndir2=%@", stringOfVect(&_articulationDirection1),
                       stringOfVect(&artDir2));
    prodVect(&_articulationDirection1, &artDir2, &correctionTorque);
    mulScalVect(&correctionTorque, CORRECTION_TORQUE, &correctionTorque);
    
    {
        Vect3D angSpeed1, angSpeed2;
        Vect3D deltaSpeed;
        Vect3D tmp;
        produitMatriceVecteur3D(&_first->_position,&_first->_angularSpeed,&tmp);
        prodVect(&tmp,&_articulationDirection1,&angSpeed1);
        produitMatriceVecteur3D(&_second->_position,&_second->_angularSpeed,&tmp);
        prodVect(&tmp,&artDir2,&angSpeed2);
        diffVect(&angSpeed1,&angSpeed2,&deltaSpeed);
        
        Vect3D speedRelArtDir;
        prodVect(&deltaSpeed,&_articulationDirection1,&speedRelArtDir);
        addLambdaVect(&correctionTorque,CORRECTION_ANG_FRICTION,&speedRelArtDir,
                      &correctionTorque);
    }

#ifdef DEBUG
    [simulator addForce:correctionTorque 
               location:_articulationPosition
                   name:@"CorrectionTorque"
            interaction:self];
    [simulator addFormalForce:_formalMomentum
                     location:_articulationPosition
                         name:@"Torque"
                  interaction:self];
#endif
    
    [_first addFormalForce:nil
            formalMomentum:_formalMomentum
     correctionFormalForce:GGZeroVect 
  correctionFormalMomentum:correctionTorque];

    Vect3D oppCorrectionTorque;
    mulScalVect(&correctionTorque,-1.0,&oppCorrectionTorque);

    [_second addFormalForce:nil
             formalMomentum:_oppositeFormalMomentum
      correctionFormalForce:GGZeroVect
   correctionFormalMomentum:oppCorrectionTorque];
}

- (void) feedSystem:(GGLinearSystem*) problem
{
    [super feedSystem:problem];
    
    // compute equation on torque.
    GGMatrix4 secondToFirst;
    transposeProduit(&_first->_position,&_second->_position,&secondToFirst);
    
    Vect3D term1;
    {
        Vect3D diffAngularSpeedInFirst;
        {
            Vect3D angularSpeed2InFirst;
            
            produitMatriceVecteur3D(&secondToFirst,&_second->_angularSpeed,&angularSpeed2InFirst);
            diffVect(&_first->_angularSpeed,&angularSpeed2InFirst,&diffAngularSpeedInFirst);
        }
        
        Vect3D tmp;
        prodVect(&_first->_angularSpeed,&_direction1,&tmp);
        prodVect(&diffAngularSpeedInFirst,&tmp,&term1);
    }
    
    
    GGMutableFormalVector* torqueEquation;
    {
        GGMutableFormalVector* diffAngAccel;
        {
            GGFormalVector* angAccel2InFirst;
            {
                GGFormalVector* angAccel2 = [_second formalAngularAcceleration];
                angAccel2InFirst = [angAccel2 transformWithMatrix3D:&secondToFirst];
            }
            diffAngAccel = [[_first formalAngularAcceleration] mutableCopy];
            [diffAngAccel substractVect:angAccel2InFirst];
        }
        
        
        torqueEquation = [diffAngAccel produitVectoriel:&_direction1];
        [diffAngAccel release];
        [torqueEquation addConstantVect:&term1];
    }
    
    // vectorial product is in fact a 2-condition.
    // remove carefully the extra equation to keep a square inversible matrix.
    int greaterIndex = 0;
    GGReal maxCoef = fabs(_direction1.x);
    if(fabs(_direction1.y) > maxCoef){
        greaterIndex = 1;
        maxCoef = fabs(_direction1.y);
    }
    if(fabs(_direction1.z) > maxCoef){
        greaterIndex = 2;
        maxCoef = fabs(_direction1.z);
    }
    
    // now, keep all the equations but the one corresponding to greaterIndex
    {   
        GGLinearPoly* coefs[3];
        [torqueEquation getAllCoef:coefs];
        int i;
        for(i = 0; i < 3; ++i){
            if(i == greaterIndex) continue;
            [problem addScalarCondition:coefs[i]];
        }
    }
//    [problem addVectorialCondition:torqueEquation];

    NSAssert(_formalMomentum, @"we should have set it before");
    GGLinearPoly* scal = [_formalMomentum produitScalaire:&_articulationDirection1];
    [problem addScalarCondition:scal];
}
@end
