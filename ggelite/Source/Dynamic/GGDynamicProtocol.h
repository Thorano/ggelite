//
//  GGDynamicProtocol.h
//  elite
//
//  Created by Frederic De Jaeger on 28/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GG3D.h"

@class GGSolid, GGFormalVector, GGLinearSystem, GGDynamicSimulator;

@protocol GGInteractionProtocol <NSObject>
- (NSString *)name;

// hook called when the interaction is added to the simulator.  Mostly useless.
- (void) addInSimulator:(GGDynamicSimulator*)simulator;

// invoke before each frame.  hook useful to precalculate things.
- (void) computeInteractionInSimulator:(GGDynamicSimulator*)simulator;

// debugging stuff
- (void) rasterise: (Camera *)cam;
- (double) potentialEnergy;
@end


@protocol GGFormalInteractionProtocol <GGInteractionProtocol>
// add constrained to the solver.
- (void) feedSystem:(GGLinearSystem*) problem;
@end
