//
//  GGCharniere.h
//  elite
//
//  Created by Frederic De Jaeger on 15/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GGDynamicProtocol.h"
#import "GGInteraction.h"


@interface GGCommonCharniere : GGStandardContact {
    // expressed in local coordinates of (1) and (2) respectively.
    Vect3D                      _direction1;
    Vect3D                      _direction2;
    
    
    GGFormalVector*             _formalMomentum;
    GGFormalVector*             _oppositeFormalMomentum;

}
- (id) initWithFirstBody:(GGSolid*)firstBody
            contactPoint:(Vect3D) pVect1
               direction:(Vect3D) pDir1
              secondBody:(GGSolid*)secondBody
            contactPoint:(Vect3D) pVect2
               direction:(Vect3D) pDir2
                inSystem:(GGLinearSystem*)system;

@end

extern float CORRECTION_TORQUE;
extern float CORRECTION_ANG_FRICTION;

@interface GGCharniere : GGCommonCharniere {
    Vect3D                      _articulationDirection1;
}
@end