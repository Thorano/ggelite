//
//  GGPseudoFormalInteraction.m
//  elite
//
//  Created by Frederic De Jaeger on 29/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGPseudoFormalInteraction.h"

#import "GGSolid.h"
#import "GGFormalVector.h"
#import "GGLinearSystem.h"
#import "utile.h"
#import "GGLog.h"


@implementation GGPseudoFormalInteraction
- initWithInteraction:(id<GGInteractionProtocol>)interaction
{
    _originalInteraction = [interaction retain];
    _formalAction = [[GGFormalVector alloc] initWithUknown];
    _formalMomentum = [[GGFormalVector alloc] initWithUknown];
    
    return self;
}

- (void) dealloc
{
    [_originalInteraction release];
    [_formalAction release];
    [_formalMomentum release];
    
    [super dealloc];
}

- (NSString*) name
{
    return @"not-defined";
}

- (void) addInSimulator:(GGDynamicSimulator*)simulator
{
//    [[_originalInteraction firstObject] addFormalInteraction:self];
//    [[_originalInteraction secondObject] addFormalInteraction:self];
}

- (GGSolid*) firstObject
{
    return [_originalInteraction firstObject];
}

- (GGSolid*) secondObject
{
    return [_originalInteraction secondObject];
}


- (void) computeInteractionInSimulator:(GGDynamicSimulator*)simulator
{
    NSAssert(_originalInteraction != nil, @"there must an original interaction");
    [_originalInteraction computeInteractionInSimulator:simulator];
    [_originalInteraction computeOnBody:[_originalInteraction firstObject]
                                forceIn:&_cacheAction
                               momentum:&_cacheMomentum];
    NSDebugMLLogWindow(@"SIM", @"action\n = %@", stringOfVect(&_cacheAction));    
}

- (GGFormalVector*) computeFormalForceOnBody:(GGSolid*)body
                              formalMomentum:(GGFormalVector**) formatMomentumPtr
{
    if([_originalInteraction firstObject] == body){
        *formatMomentumPtr = _formalMomentum;
        return _formalAction;
    }
    else if([_originalInteraction secondObject] == body){
        *formatMomentumPtr = [_formalMomentum oppositeVect];
        return [_formalAction oppositeVect];
    }
    else{
        ggabort();
        return nil;
    }
}

- (void) correctionOnBody:(GGSolid*)body
              forceResult:(Vect3D*)vectorForcePtr
           momentumResult:(Vect3D*)momentumPtr
{
    SetZeroVect(*vectorForcePtr);
    SetZeroVect(*momentumPtr);
}

- (void) feedSystem:(GGLinearSystem*) problem
{
    GGMutableFormalVector* equationOnAction = [_formalAction mutableCopy];
    [equationOnAction substractVect:[GGFormalVector vectorWithVect:&_cacheAction]];
    [problem addVectorialCondition:equationOnAction];
    [equationOnAction release];
    
    GGMutableFormalVector* equtionOnMomentum = [_formalMomentum mutableCopy];
    [equtionOnMomentum substractVect:[GGFormalVector vectorWithVect:&_cacheMomentum]];
    [problem addVectorialCondition:equtionOnMomentum];
    [equtionOnMomentum release];
}

- (void) rasterise: (Camera *)cam
{
    [_originalInteraction rasterise:cam];
}

- (double) potentialEnergy
{
    return [_originalInteraction potentialEnergy];
}
@end
