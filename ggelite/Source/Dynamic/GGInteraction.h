//
//  GGInteraction.h
//  elite
//
//  Created by Frederic De Jaeger on 09/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GG3D.h"

@class GGDynamicSimulator, GGSolid;

#import "GGDynamicProtocol.h"

@interface GGInteraction : NSObject <GGInteractionProtocol> {
    GGSolid*                _first;     // NOT RETAINED
    GGSolid*                _second;    // NOT RETAINED

    GGDynamicSimulator*     _simulator; // NOT RETAINED
    
    Vect3D                  _cacheForce;// force exerced by _second on _first;
        NSString*               _name;
}
- initWithFirstBody:(GGSolid*)firstBody
         secondBody:(GGSolid*)secondBody;
- (void) addInSimulator:(GGDynamicSimulator*)simulator;

- (void) rasterise: (Camera *)cam;

- (NSString *)name;
- (void)setName:(NSString *)value;


@end

@interface GGRessort : GGInteraction
{
    Vect3D                  _firstAnchor;
    Vect3D                  _secondAnchor;
    
    double                  _naturalLength;
    double                  _power;
    
    // used as a cache
    Vect3D                  _spaceAnchor1;
    Vect3D                  _spaceAnchor2;
    float                   _modulus;
}

- (void) setFirstAnchor:(Vect3D)vectPtr;
- (void) setSecondAnchor:(Vect3D)vectPtr;

- (double)naturalLength;
- (void)setNaturalLength:(double)value;

- (double)power;
- (void)setPower:(double)value;

@end

@class GGFormalVector;

// liaison rotule.
@interface GGStandardContact : NSObject <GGFormalInteractionProtocol>
{
    GGSolid*                _first;     // NOT RETAINED
    GGSolid*                _second;    // NOT RETAINED
    
    Vect3D                  _contact1;   // express in the frame of _first.
    Vect3D                  _contact2;   // express in the frame of _second.
//    Vect3D                  _normal;    // express in the frame of _second.
    
    GGFormalVector*         _formalAction;
    GGFormalVector*         _oppositeFormalAction;
    
// cache
    // this is _contact1 in global coordinates.  Assumed to be the same
    // as _contact2 in global coordinates.
    Vect3D                  _articulationPosition;
    Vect3D                  _articulationPosition2;  // should be the same of the previous but there is the drift.
    
    NSString*               _name;
}
- (id) initWithFirstBody:(GGSolid*)firstBody
            contactPoint:(Vect3D) pVect1
              secondBody:(GGSolid*)secondBody
            contactPoint:(Vect3D) pVect2
                inSystem:(GGLinearSystem*)system;

- (void)setName:(NSString *)value;
@end

extern float FRICTION_PER_SPEED_FOR_CORRECTION;
extern float SPRING_FOR_CORRECTION;
