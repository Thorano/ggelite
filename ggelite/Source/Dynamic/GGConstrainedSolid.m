//
//  GGConstrainedSolid.m
//  elite
//
//  Created by Frederic De Jaeger on 26/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGConstrainedSolid.h"

#import "GGSolidPrivate.h"
#import "GGFormalVector.h"

// XXX remove this dependencies at some point.
#import "GGMainLoop.h"

@implementation GGConstrainedSolid
//- (BOOL) hasFormalInteractions
//{
//    return YES;
//}
//

- (id) init
{
    self = [super init];
    if(self){
        initVect(&_speed,0,10,0);
    }
    
    return self;
}

- (void) computeAccelerations
{
    SetZeroVect(_currentAngularAcceleration);
    // XXX remove this reference to the global state.  I should use a context instead.
//    initVect(&_currentAcceleration,0, 30*cos(gameTime),0);
    mulScalVect(&_position.position,-0.2,&_currentAcceleration);
}

- (void) advanceTimeByDelta:(double)delta
{
    [self computeAccelerations];
    [self advanceTimeByDelta:delta
                acceleration:_currentAcceleration
         angularAcceleration:_currentAngularAcceleration];
}

- (void) advanceTimeByDelta:(double)delta
            formalSolutions:(GGLinearSystem*)solutions
{
    [self advanceTimeByDelta:delta
                acceleration:_currentAcceleration
         angularAcceleration:_currentAngularAcceleration];
}

- (void) computeFormalAccelerations
{
    [self computeAccelerations];
}

- (double) inertialEnergy:(double*)inertialRotationPtr
{
    return 0;
}
@end
