//
//  GGEliteBridge.m
//  elite
//
//  Created by Frederic De Jaeger on 29/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//

#import "GGEliteBridge.h"

#import "GGPseudoFormalInteraction.h"
#import "GGSolid.h"
#import "GGConstrainedSolid.h"
#import "GGInteraction.h"
#import "GGGravitation.h"
#import "GGCharniere.h"
#import "GGBiCharniere.h"
#import "GGDynamicSimulator.h"
#import "GGLog.h"
#import "utile.h"

#import "GGModel.h"
#import "ModelBlend.h"
#import "GGMMesh.h"

@interface GGEliteBridge (Private)
- (void) _populateFromNode:(GGMNode*) node
                   inSolid:(GGSolid*) currentSolid
               translateBy:(Vect3D)translation;
- (GGSolid*) _solidForTopLevelNode:(GGMNode*)node;
@end

@implementation GGEliteBridge
- init
{
    [super init];
    [self setMode:0];
    [self setVisible: YES];
    collisionCounter = -1;
    Masse(self) = 101;
    _flags.graphical = YES;
    [self setName:@"Physical model"];
    
    
    return self;
}

- (void) dealloc
{
    [_simulator release];
    
    [super dealloc];
}

- (GGSolid*) _addCubeAtPos:(Vect3D)v
{
    static int count;
    GGSolid* solid = [GGSolid new];
    [solid setName:[NSString stringWithFormat:@"Cube-%d", ++count]];
    solid->_position.position = v;
    
    [_simulator addBody:solid];
    return [solid autorelease];
}

- (void) _feedSim
{
    GGSolid* cube1 = [self _addCubeAtPos:mkVect(0,0,0)];
    [cube1 setHomogeneousInertia:100];
    [cube1 setModel:(id)[GGModel modelWithName:@"Duplo.bld"
                                         scale:10]];
    [cube1 addGravity];
    
    GGSolid* cube2 = [self _addCubeAtPos:mkVect(0,100,0)];
    [cube2 setInfiniteMass];
    [cube2 setModel:(id)[GGModel modelWithName:@"Duplo.bld" scale:10]];
    
    
    
    {
        GGRessort* ressort = [[GGRessort alloc] initWithFirstBody:cube1
                                                       secondBody:cube2];
        [ressort setName:@"Ressort"];
        [ressort setNaturalLength:20];
        [ressort setPower:0.1];
        [ressort setFirstAnchor:mkVect(-10,10,0)];
        [ressort setSecondAnchor:mkVect(0,0,0)];
        //        [ressort setFirstAnchor:mkVect(10,10,10)];
        //        [ressort setSecondAnchor:mkVect(0,-10,0)];
        
        [_simulator addInteraction:ressort];
        
        
        [ressort release];
    }
    
    GGSolid* cube3 = [self _addCubeAtPos:mkVect(0,0,19)];
    [cube3 setModel:(id)[GGModel modelWithName:@"Duplo.bld" scale:9]];
    [cube3 setMasse: 0.1];
    [cube3 setHomogeneousInertia:10];
    
    //    {
    //        GGStandardContact* contact = [[GGStandardContact alloc] initWithFirstBody:cube1
    //                                                                     contactPoint:mkVect(10,-10,10) 
    //                                                                       secondBody:cube3
    //                                                                     contactPoint:mkVect(10,-10,-10)];
    //        [_simulator addFormalInteraction:contact];
    //        [contact release];
    //    }
    
    {
        GGCharniere* charniere = [[GGCharniere alloc] initWithFirstBody:cube1
                                                           contactPoint:mkVect(10,-10,10)
                                                              direction:mkVect(1,0,0)
                                                             secondBody:cube3
                                                           contactPoint:mkVect(9,-9,-9)
                                                              direction:mkVect(1,0,0)
                                                               inSystem:[_simulator system]];
        [_simulator addFormalInteraction:charniere];
        [charniere release];
    }
    [_simulator addForCollision:cube1];
    [_simulator addForCollision:cube2];
    [_simulator addForCollision:cube3];
}

- (void) _biCharniere
{
    GGSolid* cube1 = [self _addCubeAtPos:mkVect(0,0,0)];
    [cube1 setHomogeneousInertia:100];
    [cube1 setModel:(id)[GGModel modelWithName:@"Duplo.bld"
                                         scale:10]];
//    [cube1 addGravity];
    
    GGSolid* cube2 = [self _addCubeAtPos:mkVect(0,100,0)];
    [cube2 setInfiniteMass];
    [cube2 setModel:(id)[GGModel modelWithName:@"Duplo.bld" scale:10]];
    
    
    
    {
        GGRessort* ressort = [[GGRessort alloc] initWithFirstBody:cube1
                                                       secondBody:cube2];
        [ressort setName:@"Ressort"];
        [ressort setNaturalLength:20];
        [ressort setPower:0.1];
        [ressort setFirstAnchor:mkVect(-10,10,0)];
        [ressort setSecondAnchor:mkVect(0,0,0)];
        //        [ressort setFirstAnchor:mkVect(10,10,10)];
        //        [ressort setSecondAnchor:mkVect(0,-10,0)];
        
        [_simulator addInteraction:ressort];
        
        
        [ressort release];
    }
    
    GGSolid* cube3 = [self _addCubeAtPos:mkVect(0,0,21)];
    [cube3 setModel:(id)[GGModel modelWithName:@"Duplo.bld" scale:9]];
    [cube3 setMasse: 0.1];
    [cube3 setHomogeneousInertia:10];
    
    
    {
        GGBiCharniere* charniere = [[GGBiCharniere alloc] initWithFirstBody:cube1
                                                           contactPoint:mkVect(0,0,11)
                                                              direction:mkVect(1,0,0)
                                                             secondBody:cube3
                                                           contactPoint:mkVect(0,0,-10)
                                                              direction:mkVect(0,1,0)
                                                               inSystem:[_simulator system]];
        [_simulator addFormalInteraction:charniere];
        [charniere release];
    }
//    [_simulator addForCollision:cube1];
//    [_simulator addForCollision:cube2];
//    [_simulator addForCollision:cube3];
}

- (void) simpleRessort
{
    GGSolid* cube1 = [self _addCubeAtPos:mkVect(0,0,0)];
    [cube1 setHomogeneousInertia:100];
    [cube1 setModel:(id)[GGModel modelWithName:@"Duplo.bld"
                                         scale:10]];
    GGSolid* cube2 = [self _addCubeAtPos:mkVect(0,100,0)];
    [cube2 setHomogeneousInertia:10000];
    [cube2 setMasse: 1000000];
    [cube2 setModel:(id)[GGModel modelWithName:@"Duplo.bld" scale:10]];
    
    
    {
        GGRessort* ressort = [[GGRessort alloc] initWithFirstBody:cube1
                                                       secondBody:cube2];
        [ressort setName:@"Ressort"];
        [ressort setNaturalLength:20];
        [ressort setPower:0.1];
        [ressort setFirstAnchor:mkVect(-10,10,0)];
        [ressort setSecondAnchor:mkVect(0,0,0)];
        //        [ressort setFirstAnchor:mkVect(10,10,10)];
        //        [ressort setSecondAnchor:mkVect(0,-10,0)];
        
        [_simulator addInteraction:ressort];
        
        [ressort release];
    }
}

- (void) _feedSim2
{
    GGSolid* cube2 = [self _addCubeAtPos:mkVect(0,0,0)];
    [cube2 setHomogeneousInertia:10000];
    [cube2 setMasse: 1000000];
    [cube2 setModel:(id)[GGModel modelWithName:@"Duplo.bld"
                                         scale:10]];
    
    GGConstrainedSolid* truc = [[GGConstrainedSolid alloc] init];
    [_simulator addBody:truc];
    [truc setModel:(id)[GGModel modelWithName:@"pyramide.bld"
                                        scale:10]];
    [truc release];
    
    
    
    [_simulator addForCollision:cube2];
    [_simulator addForCollision:truc];
}

- (void) _ground
{
    GGSolid* sol = [self _addCubeAtPos:mkVect(0,0,-120)];
    [sol setInfiniteMass];
    [sol setModel:(id)[GGModel modelWithName:@"Duplo.bld"
                                       scale:100]];
    
    
    [_simulator addForCollision:sol];
}

- (void) _torque
{
    GGSolid* cube1 = [self _addCubeAtPos:mkVect(0,0,0)];
    [cube1 setInfiniteMass];
    [cube1 setModel:(id)[GGModel modelWithName:@"Duplo.bld"
                                         scale:10]];
    [cube1 addGravity];
    
    
    GGSolid* cube3 = [self _addCubeAtPos:mkVect(0,0,20)];
    [cube3 setModel:(id)[GGModel modelWithName:@"Duplo.bld" scale:10]];
    [cube3 setMasse: 0.1];
    [cube3 setHomogeneousInertia:10];
    [cube3 addGravity];
    cube3->_position.haut = mkVect(0.707,0.707,0);
    normaliseMatrice(&cube3->_position);

#if 1
    {
        GGCharniere* charniere = [[GGCharniere alloc] initWithFirstBody:cube1
                                                           contactPoint:mkVect(10,-10,10)
                                                              direction:mkVect(1,0,0)
                                                             secondBody:cube3
                                                           contactPoint:mkVect(10,-10,-10)
                                                              direction:mkVect(1,0,0)
                                                               inSystem:[_simulator system]];
        [_simulator addFormalInteraction:charniere];
        [charniere release];
    }
#else
    {
        GGStandardContact* contact = [[GGStandardContact alloc] initWithFirstBody:cube1
                                                                     contactPoint:mkVect(10,-10,10) 
                                                                       secondBody:cube3
                                                                     contactPoint:mkVect(10,-10,-10)
                                                                         inSystem:[_simulator system]];
        [_simulator addFormalInteraction:contact];
        [contact release];
        
        
    }
#endif
}

- (void) _feedSim3
{
    //    GGSolid* cube2 = [self _addCubeAtPos:mkVect(0,0,0)];
    //    [cube2 setHomogeneousInertia:10];
    //    [cube2 setMasse: 10];
    //    [cube2 setModel:(id)[GGModel modelWithName:@"Duplo.bld"
    //                                         scale:10]];
    //    [cube2 addGravity];
    //    [_simulator addForCollision:cube2];
    
    GGSolid* truc = [self _addCubeAtPos:mkVect(0,-30,0)];
    initVect(&truc->_speed,0,3,0);
    //    initVect(&truc->_angularSpeed,0,0,0.4);
    [truc setModel:(id)[GGModel modelWithName:@"pyramide.bld"
                                        scale:10]];
    truc->_position.direction = mkVect(0,1,0);
    truc->_position.haut = mkVect(0,0,-1);
    normaliseMatrice(&truc->_position);
    [truc setName:@"Pyramide"];
    [truc addGravity];
    
    [self _ground];
    [_simulator addForCollision:truc];
}

- (GGSolid*) _addBriqueAtPos:(Vect3D)vect
{
    GGSolid* brique = [self _addCubeAtPos:vect];
    [brique setHomogeneousInertia:10];
    [brique setMasse:10];
    [brique setModel:(id)[GGModel modelWithName:@"Duplo.bld"
                                          scale:10]];
    [brique addGravity];
    [_simulator addForCollision:brique];
    
    return brique;
}

- (void) _wall
{
    [self _addBriqueAtPos:mkVect(0,0,-5)];
    [self _addBriqueAtPos:mkVect(25,0,-5)];
    [self _addBriqueAtPos:mkVect(25,25,-5)];
    [self _addBriqueAtPos:mkVect(0,25,-5)];
    
    [self _addBriqueAtPos:mkVect(10,15,20)];
    [self _addBriqueAtPos:mkVect(80, 20, 60)];
    
    GGSolid* truc = [self _addCubeAtPos:mkVect(0,-30,0)];
    //    initVect(&truc->_speed,0,3,0);
    //    initVect(&truc->_angularSpeed,0,0,0.4);
    [truc setModel:(id)[GGModel modelWithName:@"pyramide.gg3d"
                                        scale:10]];
    truc->_position.direction = mkVect(0,1,0);
    truc->_position.haut = mkVect(0,0,-1);
    normaliseMatrice(&truc->_position);
    [truc setName:@"Pyramide"];
    [truc addGravity];
    
    [_simulator addForCollision:truc];
    
    [self _ground];
}


- (GGSolid*) _cylindre
{
    GGSolid* cylindre = [self _addCubeAtPos:mkVect(80, -30, 0)];
    [cylindre setHomogeneousInertia:100];
    [cylindre setMasse:100];
//    [cylindre setModel:[GGModel modelWithName:@"Blob.gg3d" ]];
    [cylindre setModel:[GGModel modelWithName:@"Cylindre.bld"]];
    [cylindre setName:@"Cylindre"];
    
    [cylindre addGravity];
    //    [cylindre setInfiniteMass];
    
    cylindre->_position.direction = mkVect(0,0,1);
    cylindre->_position.haut = mkVect(-1,0,0);
    normaliseMatrice(&cylindre->_position);
    [_simulator addForCollision:cylindre];
    
    return cylindre;
}

- (void) _courier
{
    GGSolid* brique = [self _addCubeAtPos:mkVect(80, 30, 0)];
//    GGSolid* brique = [self _addCubeAtPos:mkVect(80, 100, 0)];
    [brique setHomogeneousInertia:10];
    [brique setMasse:10];
    [brique setModel:(id)[GGModel modelWithName:@"Courier.gg3d"]];
//    [brique setModel:(id)[GGModel modelWithName:@"Blob.gg3d" ]];
    [brique setName:@"Courier"];
    brique->_position.direction = mkVect(0,0,1);
    brique->_position.haut = mkVect(0,1,0);
    normaliseMatrice(&brique->_position);
//    initVect(&brique->_speed,0,-2,0);
    [brique addGravity];
    [_simulator addForCollision:brique];
        
}

- (void) _chose
{
    GGSolid* brique = [self _addCubeAtPos:mkVect(63, -20, 60)];
    [brique setHomogeneousInertia:10];
    [brique setMasse:10];
    [brique setModel:(id)[GGModel modelWithName:@"Chose.gg3d"]];
    [brique setName:@"Chose"];
    brique->_position.direction = mkVect(0,-1,0);
    brique->_position.haut = mkVect(0.1,0,1);
    normaliseMatrice(&brique->_position);
    //    initVect(&brique->_speed,0,-2,0);
    [brique addGravity];
    [_simulator addForCollision:brique];
    
}

- (void) _simpleChoc
{
    GGSolid* cube2 = [self _addCubeAtPos:mkVect(0,0,0)];
    [cube2 setHomogeneousInertia:10];
    [cube2 setMasse: 10];
    [cube2 setModel:(id)[GGModel modelWithName:@"Duplo.bld"
                                         scale:10]];
    
    GGSolid* truc = [self _addCubeAtPos:mkVect(0,-30,0)];
    truc->_position.direction = mkVect(0,0,1);
    truc->_position.haut = mkVect(0,1,0);
    normaliseMatrice(&truc->_position);
    
    initVect(&truc->_speed,0,3,0);
    //    initVect(&truc->_angularSpeed,0,0,0.4);
    [truc setModel:(id)[GGModel modelWithName:@"pyramide.bld"
                                        scale:10]];
    [truc setName:@"Pyramide"];
    
    [_simulator addForCollision:cube2];
    [_simulator addForCollision:truc];
}

- (void)loadFileAtPath:(NSString*)path
{
    RELEASE(_simulator);
    _simulator = [[GGDynamicSimulator alloc] init];
#ifdef DEBUG
    [_simulator setShowSolids:YES];
    [_simulator setShowCollisions:YES];
#endif
    if(path){
        NSDictionary* properties = [[NSDictionary alloc] initWithObjectsAndKeys:
            [NSNumber numberWithFloat:4.0], @"Scale",
            nil];
        
        ModelBlender* model = [GGModel modelWithPath:path properties:properties];
        [properties release];
        if(model){
            GGSolid* cube2 = [self _addCubeAtPos:mkVect(0,50,0)];
            [cube2 setInfiniteMass];
            [cube2 setModel:(id)[GGModel modelWithName:@"Duplo.bld" scale:10]];

            NSArray* nodes = [model topLevelObjects];
            GGMNode* node;
            NSEnumerator* meshenum = [nodes objectEnumerator];
            while((node = [meshenum nextObject])!=nil){
                /*GGSolid* solid = */[self _solidForTopLevelNode:node];
                // attach top level solid

#if 0
                GGRessort* ressort = [[GGRessort alloc] initWithFirstBody:solid
                                                               secondBody:cube2];
                [ressort setName:@"Ressort"];
                [ressort setNaturalLength:20];
                [ressort setPower:1];
                [ressort setFirstAnchor:mkVect(-10,10,0)];
                [ressort setSecondAnchor:mkVect(0,0,0)];
                [_simulator addInteraction:ressort];
                [ressort release];
#endif
            }
        }
//        [self _ground];
    }
    else{
//        [self _torque];
//        [self _feedSim];
//        [self _biCharniere];
        //            [self _feedSim2];
//        [self _feedSim3];
        //    [self simpleRessort];
//        [self _simpleChoc];
//                [self _wall];   
        //        [self _duploPyramideCollide];
        [self _ground];
//        [self _cylindre];
//        [self _chose];
        [self _courier];
    }
    
#if DEBUG_COCOA
    [[GGDynamicDebugController controller] setSimulator:_simulator];
#endif    
}

#pragma mark administrative stuff

- (BOOL) shouldSave
{
    return NO;
}

- (BOOL) saveChannel
{
    return NO;
}

- (BOOL) shouldCache
{
    return NO;
}

- (BOOL) shouldBeClipped
{
    return NO;
}

- (BOOL) shouldDrawMoreThings
{
    return NO;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    ggabort();
}

- (id)initWithCoder:(NSCoder *)decoder
{
    ggabort();
    [self release];
    
    return nil;
}

- (void) rasterise: (Camera *)cam
{
    NSDebugMLLogWindow(@"SIM", @"drawing");
#ifdef DEBUG
    [_simulator rasterise:cam];
#endif
}

- (void) setNodePere:(GGSpaceObject*)guy
{
    [super setNodePere:guy];
    if(guy){
        [theLoop fastSchedule: self
                 withSelector: @selector(runSimulatorForFrame)
                          arg: nil];        
    }
    else {
        [[theLoop fastScheduler]   deleteTarget: self
                                   withSelector: @selector(runSimulatorForFrame)];

    }

}

- (void) runSimulatorForFrame
{
    [_simulator advanceTimeByDelta:deltaTime];
}

@end

@implementation GGEliteBridge (Private)
- (GGSolid*) _solidForTopLevelNode:(GGMNode*)node
                       toptopLevel:(BOOL)toptopLevel
{
    NSParameterAssert(node!=nil);
    GGSolid* aSolid = [[GGSolid alloc] init];
    
    Vect3D pos = [node position];
    aSolid->_position.position = pos;
    mulScalVect(&pos,-1.0,&pos);
    [node translateByVect:&pos];
    ModelBlender* subStuff = [[ModelBlender alloc] initWithNode:node];
    [aSolid setModel:subStuff];
    [subStuff release];
    
    
    [_simulator addBody:aSolid];
//    [_simulator addForCollision:aSolid];

    if(toptopLevel){
        [aSolid setInfiniteMass];
    }
    else{
        [aSolid setHomogeneousInertia:10];
        [aSolid setMasse: 10];
        [aSolid addGravity];
    }

    [aSolid setName:[node objectName]];
    [self _populateFromNode:node
                    inSolid:aSolid
                translateBy:pos];
    [[aSolid model] finalizeAfterLoading];
    [aSolid release];
    
    return aSolid;
}

- (GGSolid*) _solidForTopLevelNode:(GGMNode*)node
{
    return [self _solidForTopLevelNode:node
                           toptopLevel:YES];
}

- (Vect3D) pointFromAxe:(GGMNode*)axeNode
           directionPtr:(Vect3D*)direction
{
    // need to apply the matrix here.  Hence, we should work with the parent node.
    CacheVertex vertices[2];
    NSIndexSet* two = [[NSIndexSet alloc] initWithIndexesInRange:NSMakeRange(0,2)];
    [axeNode computeVerticesAtIndices:two
                    forTransformation:NULL
                            inResults:vertices];
    [two release];
    Vect3D articulPosition = vertices[0].pos;
    *direction = vertices[1].pos;
    addVect(&articulPosition,direction,&articulPosition);
    mulScalVect(&articulPosition,0.5,&articulPosition);
    diffVect(direction,&articulPosition,direction);
    normaliseVect(direction);
    
    return articulPosition;
}

static int indexForName;

- (void) _populateFromNode:(GGMNode*) node
                   inSolid:(GGSolid*) currentSolid
               translateBy:(Vect3D)translation
{
    NSArray* subMeshNodes = [node subMeshNodes];
    GGMNode* subNode;
    NSEnumerator* subNodesEnum = [subMeshNodes objectEnumerator];
    while((subNode = [subNodesEnum nextObject])!=nil){
        GGMMesh* subMesh = [subNode mesh];
        if([[subMesh name] hasPrefix:@"Axe"]){
            NSAssert([[subNode subMeshNodes] count] <= 1, @"an Axe can't  have more than  one sone");
            if([[subNode subMeshNodes] count] == 0){
                // ignore this dangling node.
                continue;
            }
            
            NSAssert([subMesh numberOfVertices] == 2, @"Axe without 2 vertices");
            Vect3D articulPosition, direction;
            articulPosition = [self pointFromAxe:subNode
                                    directionPtr:&direction];
            Vect3D localPos1;
            Vect3D localPos2;
            Vect3D localDir1;
            Vect3D localDir2;
            transposeTransformePoint4D(&currentSolid->_position,&articulPosition,&localPos1);
            transposeProduitVect3D(&currentSolid->_position,&direction,&localDir1);
            GGMNode* effectiveSubNode = [subNode firstSon];
            Class c = [GGCharniere class];
            if([[[effectiveSubNode mesh] name] hasPrefix:@"Axe"])
            {
                NSLog(@"bi charniere %@", stringOfVect(&articulPosition));
                articulPosition = [self pointFromAxe:effectiveSubNode
                                        directionPtr:&direction];
                NSLog(@"bi charniere %@", stringOfVect(&articulPosition));
                NSAssert([[effectiveSubNode subMeshNodes] count] == 1, @"axe");
                effectiveSubNode = [effectiveSubNode firstSon];
                c = [GGBiCharniere class];
            }
            if(effectiveSubNode){
                GGSolid* solid = [self _solidForTopLevelNode:effectiveSubNode
                                                 toptopLevel:NO];
                transposeTransformePoint4D(&solid->_position,&articulPosition,&localPos2);
                transposeProduitVect3D(&solid->_position,&direction,&localDir2);
                NSAssert(solid, @"could not build stuff");
                GGCharniere* axe = [[c alloc] initWithFirstBody:currentSolid
                                                   contactPoint:localPos1
                                                      direction:localDir1
                                                     secondBody:solid
                                                   contactPoint:localPos2
                                                      direction:localDir2
                                                       inSystem:[_simulator system]];
                [axe setName:[NSString stringWithFormat:@"%@-%d", c, ++indexForName]];
                [_simulator addFormalInteraction:axe];
                [axe release];
            }
            else{
                NSLog(@"dangling axe, let's forget about it");
            }
        }
        else{
            // this is a regular submesh
//            [subMesh translateMeshByVect:&translation];
            [subNode translateByVect:&translation];
            [[currentSolid model] addMeshNode:subNode withName:nil];
        }
    }
}
@end
