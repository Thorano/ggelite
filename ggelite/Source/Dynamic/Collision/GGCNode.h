//
//  GGCNode.h
//  elite
//
//  Created by Frederic De Jaeger on 26/03/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GGCollision.h"
#import "GGAABB.h"

@class GGMNode;

@class GGCollisionManager, GGSolid;

@interface GGCNodeCommon : NSObject <GGCollisionNode>
{
    GGSolid*                _mainSolid;  // weak pointer

    BoundingBox             _domain;
    GGReal                  _diameter;
    
    BoundingBox             _transformedDomain;
    GGCollisionManager*     _manager;  //weak pointer
    
    
    // this one has to contain the cache version of the 
    // transformed coordinates.
}
- (id) initWithSolid:(GGSolid*)solid
              domain:(BoundingBox)domain
           inManager:(GGCollisionManager*)manager;

// implementation returns nil
- (NSArray*)subNodes;
- (BoundingBox) domain;

- (GGSolid*)solid;
- (GGReal) diameter;
- (void) computeCacheTransformed;
- (const BoundingBox) tranformedBoundingBoxCache;

@end

@interface GGCNode : GGCNodeCommon {
    NSArray*                _subNodesAsArray;
}
- (id) initWithSubNodes:(NSArray*)nodes
                inSolid:(GGSolid*)solid
              inManager:(GGCollisionManager*)manager;


- (NSArray*)subNodes;

@end

@interface GGCLeaf : GGCNodeCommon
{
    // immutable stff
    unsigned                _identifier;
    GGMNode*                _meshNode;
    NSIndexSet*             _faceIndices;
    NSIndexSet*             _vertexIndices;
    int                     _nVertex;
    
    
    // cache and other mutable stuff.
    struct __cacheVertex*   _verticesCache;
    unsigned                _verticesCacheGeneration;
    
    GGPlan*                 _facesCache;
    unsigned                _facesCacheGeneration;
    FaceIndices*             _faces;
}
- (id) initWithMeshNode:(GGMNode*)node
                inSolid:(GGSolid*)solid
                 domain:(BoundingBox)domain
            faceIndices:(NSIndexSet*)faceIndices
          vertexIndices:(NSIndexSet*)vertexIndices
              inManager:(GGCollisionManager*)manager;

@end

