//
//  GGCLinkedList.m
//  elite
//
//  Created by Frederic De Jaeger on 02/06/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGCLinkedList.h"

#import "utile.h"

void FIS_Init(struct ggFastIndexSet *set){
    set->size = 0;
    set->bitFlags = NULL;
    set->count = 0;
}

static void FIS_destroy(struct ggFastIndexSet *set)
{
    if(set->bitFlags) free(set->bitFlags);
    FIS_Init(set);
}

void FIS_removeAllIndices(struct ggFastIndexSet *set)
{
    int i;
    for(i = 0; i < set->size; ++i){
        set->bitFlags[i] = 0;
    }
    set->count = 0;
}

void FIS_Add(struct ggFastIndexSet *set, unsigned index)
{
    div_t result = div(index,  NBRBIT_PER_BUCKET);
    if( result.quot >= set->size){
        unsigned oldSize = set->size;
        set->size = 2*(result.quot+1);
        if(NULL == set->bitFlags){
            set->bitFlags = AllocType(set->size, uint32_t);
        }
        else{
            set->bitFlags = ReallocType(set->bitFlags, set->size, uint32_t);            
        }
        int i;
        for(i = oldSize; i < set->size; ++i){
            set->bitFlags[i] = 0;
        }
    }
    
    NSCAssert(!FIS_member(set,index), @"index should not be there");
    set->bitFlags[result.quot] |= (1 << result.rem);
    NSCAssert(FIS_member(set, index), @"adding a new member that is not there");
    set->count += 1;
}

NSArray* FIS_allElementsInSet(struct ggFastIndexSet *set, NSArray* allObjects)
{
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:[allObjects count]];
    int i;
    int base;
    for(i = 0, base = 0; i < set->size; ++i, base += NBRBIT_PER_BUCKET){
        if(set->bitFlags[i] != 0){
            int j;
            uint32_t mask;
            for(j = 0, mask = 1; j < NBRBIT_PER_BUCKET; ++j, mask <<= 1){
                if(set->bitFlags[i] & mask){
                    [array addObject:[allObjects objectAtIndex:base+j]];
                }
            }
        }
    }
    
    return array;
}

static void FIS_initEnum(struct FIS_enumerator *iterator, struct ggFastIndexSet *set)
{
    iterator->i = 0;
    iterator->base = 0;
    iterator->j = 0;
    iterator->mask = 1;
    iterator->set = set;
};

unsigned FIS_nextInteger(struct FIS_enumerator *iterator)
{
    unsigned ret = NSNotFound;
    BOOL found = NO;
    while(iterator->i < iterator->set->size && !found){
        BOOL bigIncrement = NO;
        if(0 == iterator->j && 0 == iterator->set->bitFlags[iterator->i]){
            bigIncrement = YES;
        }
        else{
            if(iterator->set->bitFlags[iterator->i] & iterator->mask){
                ret = iterator->base + iterator->j;
                found = YES;
            }
            // incrementation
            iterator->j += 1;
            iterator->mask <<= 1;            
        }
        
        if(bigIncrement || iterator->j >= NBRBIT_PER_BUCKET){
            iterator->j = 0;
            iterator->mask = 1;
            iterator->i += 1;
            iterator->base += NBRBIT_PER_BUCKET;
        }
    }
    
    return ret;
}

static unsigned FIS_count(struct ggFastIndexSet *set)
{
    struct FIS_enumerator iterator;
    FIS_initEnum(&iterator,set);
    unsigned count;
    for(count = 0; FIS_nextInteger(&iterator) != NSNotFound; ++count);
    NSCAssert(set->count == count, @"inconsistency in count");
    return count;
}

@implementation GGCLinkedListItem
- (id) initWithObject:(id)object
{
    self = [self init];
    [self setObject:object];
    
    return self;
}

- (id) init
{
    FIS_Init(&_guys);
    FIS_Init(&_guysPreviousRun);
    
    return self;
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"LL(%@)", _value];
}

- (void) dealloc
{
    FIS_destroy(&_guys);
    FIS_destroy(&_guysPreviousRun);
    [_value release];
    [super dealloc];
}

- (id) object
{
    return _value;
}

- (void) setObject:(id)object
{
    ASSIGN(_value, object);
}

- (BOOL) addIndexNodeForCollision:(unsigned)otherNode
                        firstLoop:(BOOL)firstLoop
{
    //    NSAssert(_guys == nil || ![_guys containsObject:otherNode], @"guys already in friends");
    BOOL shouldAdd = NO;
    if(!firstLoop){
        if(FIS_member(&_guysPreviousRun,otherNode)){
            shouldAdd = YES;
        }
    }
    else{
        shouldAdd = YES;
    }
    
    if(shouldAdd){
        FIS_Add(&_guys,otherNode);
        return YES;
    }
    else{
        return NO;
    }
}

- (void) endOfLoop
{
    struct ggFastIndexSet tmp;
    FIS_removeAllIndices(&_guysPreviousRun);
    tmp = _guysPreviousRun;
    _guysPreviousRun = _guys;
    _guys = tmp;
    _previous = nil;
    _next = nil;
}

- (void) clearAllCollisionStates
{
    FIS_removeAllIndices(&_guysPreviousRun);
    FIS_removeAllIndices(&_guys);
    _previous = nil;
    _next = nil;
}

- (BOOL) inList
{
    return _previous != nil || _next != nil;
}

- (unsigned) countOfFriends
{
    return FIS_count(&_guysPreviousRun);
}

- (void) initIteratorOnFriends:(struct FIS_enumerator*)iteratorPtr
{
    FIS_initEnum(iteratorPtr,&_guysPreviousRun);
}


void GGCNLL_append(struct GCCNode_LinkedList *llist, GGCLinkedListItem *guy)
{
    NSCAssert(guy->_previous == nil, @"guy already has a previous");
    NSCAssert(guy->_next == nil, @"guy already has a next");
    if(llist->first == NULL){
        NSCAssert(llist->last == NULL, @"last not nil but first is");
        llist->first = guy;
        llist->last = guy;
    }
    else{
        NSCAssert(llist->last!=nil, @"has a first but no last");
        guy->_previous = llist->last;
        llist->last->_next = guy;
        llist->last = guy;
    }
}

void GGCNLL_remove(struct GCCNode_LinkedList *llist, GGCLinkedListItem *guy)
{
    if(guy == llist->first){
        llist->first = guy->_next;
        NSCAssert(guy->_previous == nil, @"cause the first");
    }
    if(guy == llist->last){
        llist->last = guy->_previous;
        NSCAssert(guy->_next == nil, @"should not have a next if last");
    }
    if(guy->_next){
        NSCAssert(guy->_next->_previous == guy, @"inconsistency");
        guy->_next->_previous = guy->_previous;
    }
    if(guy->_previous){
        NSCAssert(guy->_previous->_next == guy, @"inconsistency");
        guy->_previous->_next = guy->_next;
    }
    guy->_next = nil;
    guy->_previous = nil;
}

GGCLinkedListItem* GGCNLL_next(GGCLinkedListItem* node)
{
    return node->_next;
}

BOOL GGCNLL_contains(struct GCCNode_LinkedList *llist, GGCLinkedListItem *guy)
{
    GGCLinkedListItem* node = llist->first;
    for(node = llist->first; node != nil; node = node->_next){
        if(node == guy){
            return YES;
        }
    }
    return NO;
}

static GGCLinkedListItem* collectFriends(GGCLinkedListItem* last, GGCLinkedListItem* node, NSArray* nodes)
{
    struct FIS_enumerator fis_enum;
    FIS_initEnum(&fis_enum, &node->_guysPreviousRun);
    
    unsigned indice;
    
    while((indice = FIS_nextInteger(&fis_enum))!= NSNotFound){
        GGCLinkedListItem* otherNode = [nodes objectAtIndex:indice];
        if([otherNode inList]){
            continue;
        }
        
        otherNode->_previous = last;
        last->_next = otherNode;
        last = otherNode;
        
        last = collectFriends(last, otherNode, nodes);
    }
    
    return last;
}

- (NSArray*) collectFriendsWithNodes:(NSArray*)nodes
{
    GGCLinkedListItem* last = self;
    collectFriends(last, self, nodes);
    
    NSMutableArray* friends = nil;
    if(_next){
        friends = [NSMutableArray array];
        GGCLinkedListItem* currentNode;
        for(currentNode = self;  currentNode != nil; currentNode = currentNode->_next){
            [friends addObject:currentNode];
        }
    }
    
    return friends;
}

- (void) setCurrentIndex:(unsigned)index
{
    _currentIndex = index;
}

- (unsigned) currentIndex
{
    return _currentIndex;
}

#ifdef DEBUG
- (void) verifyNeutralState
{
    NSAssert(FIS_count(&_guys) == 0, @"guys set!");
    NSAssert(FIS_count(&_guysPreviousRun) == 0, @"_guysPreviousRun set!");
    NSAssert(nil == _previous, @"hoh");
    NSAssert(nil == _next, @"hoh");
    
}
#endif
@end



