//
//  GGCNodeBuilder.h
//  elite
//
//  Created by Frederic De Jaeger on 12/05/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGCNode.h"

#define NBR_SUB_NODE_PER_DIM    5
#define NBR_SUB_NODE (NBR_SUB_NODE_PER_DIM*NBR_SUB_NODE_PER_DIM*NBR_SUB_NODE_PER_DIM)

#define NODE_CROSSING           0.5
#define RELATIVE_SIZE_SUB_NODE  ((1.0 / (GGReal)NBR_SUB_NODE_PER_DIM)*(1.0 + 2.0 * NODE_CROSSING))


@interface GGCNodeBuilder : NSObject {
    GGSolid*                _mainSolid;  // weak pointer
    GGMNode*                _meshNode;
    NSMutableIndexSet*      _faceIndices;
//    NSMutableIndexSet*      _vertexIndices;

    BoundingBox             _domain;
    GGReal                  _incrementation[SPACE_DIM];
    GGCNodeBuilder*         _subNodes[NBR_SUB_NODE];  // only usefull when building the tree.
    int                     _nbrSubNode;
    
}
- (id) initWithMeshNode:(GGMNode*)node
                inSolid:(GGSolid*)solid
              forDomain:(BoundingBox)box;

- (void) addTriangleWithIndex:(unsigned)index
                     vertices:(Vect3D*)vertices
                  nbrVertices:(unsigned)nbrVertices;

- (GGCNodeCommon*) consolidateTreeInTransformedVertices:(struct __cacheVertex*) vertices
                                              inManager:(GGCollisionManager*)manager;
@end
