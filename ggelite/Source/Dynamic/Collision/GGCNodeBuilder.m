//
//  GGCNodeBuilder.m
//  elite
//
//  Created by Frederic De Jaeger on 12/05/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGCNodeBuilder.h"
#import "GGMMesh.h"
#import "GGSolid.h"
#import "utile.h"


@implementation GGCNodeBuilder
- (id) initWithMeshNode:(GGMNode*)node
                inSolid:(GGSolid*)solid
              forDomain:(BoundingBox)box
{
    ASSIGN(_meshNode, node);
    _domain = box;
    GGReal max = -1;
    _mainSolid = solid;
    
    int i;
    for(i = 0; i < SPACE_DIM; ++i){
        GGReal myDiff = _domain.max[i]-_domain.min[i];
        if(myDiff > max){
            max = myDiff;
        }
    }
    
    for(i = 0; i < SPACE_DIM; ++i){
        _domain.max[i] = _domain.min[i] + max;
        _incrementation[i] = (_domain.max[i]-_domain.min[i])/NBR_SUB_NODE_PER_DIM;
    }
    
    return self;
}

- (void) _destroySubNodes
{
    int i;
    for(i=0; i<NBR_SUB_NODE; ++i)
        DESTROY(_subNodes[i]);
}

- (void) dealloc
{
    [self _destroySubNodes];
    RELEASE(_faceIndices);
    RELEASE(_meshNode);

    [super dealloc];
}

#pragma mark -
#pragma mark initializations

- (NSMutableIndexSet*) newAcceptedNodeIndicesForPoint:(Vect3D*)_point
{
    int i;
    GGReal *point = ggVect3DToArray(_point);
#ifdef DEBUG
    for(i = 0; i < SPACE_DIM; ++i){
        NSParameterAssert(point[i] >= _domain.min[i] &&
                          point[i] <= _domain.max[i]);
    }
#endif
    
    NSMutableIndexSet* acceptableSet = nil;
    int fact = NBR_SUB_NODE_PER_DIM;
    for(i = 0; i < SPACE_DIM; ++i){
        NSRange range;
        {
            int indice = floor((point[i] - _domain.min[i])/(_domain.max[i] - _domain.min[i]) * NBR_SUB_NODE_PER_DIM);
            NSParameterAssert(indice>=0 && indice <= NBR_SUB_NODE_PER_DIM);
            indice = (indice >= NBR_SUB_NODE_PER_DIM ? NBR_SUB_NODE_PER_DIM-1 : indice);
            if(indice > 0 && (point[i] - _domain.min[i] -indice*_incrementation[i]) < NODE_CROSSING*_incrementation[i]){
                range = NSMakeRange(indice-1,2);
            }
            else if (indice < NBR_SUB_NODE_PER_DIM-1 && ((indice+1)*_incrementation[i]-(point[i]-_domain.min[i])) < NODE_CROSSING*_incrementation[i]){
                range = NSMakeRange(indice,2);
            }
            else{
                range = NSMakeRange(indice,1);
            }
        }
        
        if(nil == acceptableSet){
            acceptableSet = [[NSMutableIndexSet alloc] initWithIndexesInRange:range];
        }
        else{
            unsigned indices[TWO_POWER_SPACE_DIM];
            NSParameterAssert([acceptableSet count] < TWO_POWER_SPACE_DIM);
            unsigned numberOfExistingIndices = [acceptableSet getIndexes:indices
                                                                maxCount:TWO_POWER_SPACE_DIM
                                                            inIndexRange:NULL];
            NSParameterAssert(numberOfExistingIndices > 0);
            NSMutableIndexSet* newAcceptableSet = [[NSMutableIndexSet alloc] init];
            
            int indice;
            for(indice = range.location; indice < range.location+range.length; ++indice){
                int k;
                for(k = 0; k < numberOfExistingIndices; ++k){
                    unsigned newIndice = indice*fact + indices[k];
                    [newAcceptableSet addIndex:newIndice];
                }
            }
            
            RELEASE(acceptableSet);
            acceptableSet = newAcceptableSet;
            fact *= NBR_SUB_NODE_PER_DIM;
        }
    }
    
    //    NSLog(@"acceptableSet = %@", acceptableSet);
    return acceptableSet;
}

- (void) addTriangleWithIndex:(unsigned)index
                     vertices:(Vect3D*)vertices
                  nbrVertices:(unsigned)nbrVertices
{
    NSMutableIndexSet* acceptableIndices = [self newAcceptedNodeIndicesForPoint:&vertices[0]];
    BOOL forMe = NO;
    int i;
    for(i = 1; i < nbrVertices && !forMe; ++i){
        NSIndexSet* otherIndices = [self newAcceptedNodeIndicesForPoint:&vertices[i]];
        NSMutableIndexSet* complement = [[NSMutableIndexSet alloc] initWithIndexesInRange:NSMakeRange(0,NBR_SUB_NODE)];
        [complement removeIndexes:otherIndices];
        [otherIndices release];
        
        [acceptableIndices removeIndexes:complement];
        [complement release];
        
        if([acceptableIndices count] == 0){
            forMe = YES;
        }
    }
    
    if(forMe){
        if(nil == _faceIndices){
            _faceIndices = [[NSMutableIndexSet alloc] initWithIndex:index];
        }
        else{
            [_faceIndices addIndex:index];
        }
    }
    else{
        unsigned subIndex = [acceptableIndices firstIndex];
        NSParameterAssert(subIndex != NSNotFound);
        NSParameterAssert(subIndex < NBR_SUB_NODE);
        if(nil == _subNodes[subIndex]){
            BoundingBox subDomain;
            unsigned coordGlob = subIndex;
            for(i = 0; i < SPACE_DIM; ++i){
                div_t res = div(coordGlob, NBR_SUB_NODE_PER_DIM);
                coordGlob = res.quot;
                subDomain.min[i] = ((GGReal)(res.rem) - NODE_CROSSING)*_incrementation[i] + _domain.min[i];
                subDomain.max[i] = subDomain.min[i]+_incrementation[i] * (1.0 + 2.0*NODE_CROSSING);
            }
            _subNodes[subIndex] = [[GGCNodeBuilder alloc] initWithMeshNode:_meshNode
                                                            inSolid:_mainSolid
                                                          forDomain:subDomain];
            _nbrSubNode++;
        }
        //        NSLog(@"inspecting subnode");
        [_subNodes[subIndex] addTriangleWithIndex:index
                                         vertices:vertices
                                      nbrVertices:nbrVertices];
    }
    //    NSLog(@"found %@", acceptableIndices);
    [acceptableIndices release];
}

- (void) _populateVertexIndices:(NSMutableIndexSet*)vertexIndices
                    faceIndices:(NSMutableIndexSet*)faceIndices
            transformedVertices:(CacheVertex*) vertices
                         domain:(BoundingBox*)domainPtr
{
    [faceIndices addIndexes:_faceIndices];

    FaceBlend* faces = [[_meshNode mesh] faces];
    
    if(_faceIndices){
        unsigned int index = [_faceIndices firstIndex];
        while(index != NSNotFound){
            [vertexIndices addIndex:faces[index].A];
            [vertexIndices addIndex:faces[index].B];
            [vertexIndices addIndex:faces[index].C];
            extendDomainByPoint(domainPtr, vertices[faces[index].A].pos);
            extendDomainByPoint(domainPtr, vertices[faces[index].B].pos);
            extendDomainByPoint(domainPtr, vertices[faces[index].C].pos);
            index = [_faceIndices indexGreaterThanIndex:index];
        }
    }
    
    int i;
    for(i = 0;  i < NBR_SUB_NODE; ++i){
        if(_subNodes[i]){
            [_subNodes[i] _populateVertexIndices:vertexIndices
                                     faceIndices:faceIndices
                             transformedVertices:vertices
                                          domain:domainPtr];
        }
    }
}

- (GGCNodeCommon*) consolidateTreeInTransformedVertices:(CacheVertex*) vertices
                                              inManager:(GGCollisionManager*)manager
{
    GGCLeaf* leafNode = nil;
    NSMutableArray* subNodes = nil;
    BoundingBox newDomain = {
    {1e20, 1e20,1e20},
    {-1e20, -1e20,-1e20},
    };
    BOOL shouldCollapse = NO;
    if([_faceIndices count] > 0){
        shouldCollapse = YES;
        NSMutableIndexSet* vertexIndices = [NSMutableIndexSet new];
        NSMutableIndexSet* faceIndices = [NSMutableIndexSet new];
        
        [self _populateVertexIndices:vertexIndices
                         faceIndices:faceIndices
                 transformedVertices:vertices
                              domain:&newDomain];
        leafNode = [[[GGCLeaf alloc] initWithMeshNode:_meshNode
                                                  inSolid:_mainSolid
                                                   domain:newDomain
                                              faceIndices:faceIndices
                                             vertexIndices:vertexIndices
                                                 inManager:manager] autorelease];
        [vertexIndices release];
        [faceIndices release];
    }
    else{
        int i, current;
        subNodes = [NSMutableArray array];
        for(i = 0, current = 0; i < NBR_SUB_NODE; ++i){
            if(_subNodes[i]){
                GGCNodeCommon* aSubNode = [_subNodes[i] consolidateTreeInTransformedVertices:vertices
                                                                                   inManager:manager];
                NSAssert(aSubNode, @"we should have a subnode there");
                [subNodes addObject:aSubNode];
                if(0 == current){
                    newDomain = [aSubNode domain];
                }
                else{
                    BoundingBox otherDomain = [aSubNode domain];
                    extendDomainByArray(&newDomain, otherDomain.min);
                    extendDomainByArray(&newDomain, otherDomain.max);
                }
                current++;
            }
        }    
        NSAssert(current>0, @"there must be a subnode");
    }

    GGCNodeCommon *retNode = nil;
    if(subNodes == nil){
        NSAssert(leafNode != nil, @"must have a leaf node");
        retNode = leafNode;
    }
    else{
        if([subNodes count] == 1 && leafNode == nil){
            retNode = [subNodes objectAtIndex:0];
        }
        else {
            if(leafNode != nil){
                [subNodes addObject:leafNode];
            }
            retNode = [[[GGCNode alloc] initWithSubNodes:subNodes
                                       inSolid:_mainSolid
                                     inManager:manager] autorelease];
            
        }
    }
    [self _destroySubNodes];
    NSAssert(retNode!=nil, @"we should have build a return value");

    return retNode;
}
@end
