//
//  GGCLinkedList.h
//  elite
//
//  Created by Frederic De Jaeger on 02/06/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define NBRBIT_PER_BUCKET 32

struct ggFastIndexSet
{
    unsigned     size;
    uint32_t     *bitFlags;
    unsigned    count;
};

void FIS_Init(struct ggFastIndexSet *set);
void FIS_removeAllIndices(struct ggFastIndexSet *set);
void FIS_Add(struct ggFastIndexSet *set, unsigned index);
static inline BOOL FIS_member(struct ggFastIndexSet *set, unsigned index)
{
    div_t result = div(index,  NBRBIT_PER_BUCKET);
    if(result.quot >= set->size){
        return NO;
    }
    
    return (set->bitFlags[result.quot] & (1 << result.rem)) != 0;
}
NSArray* FIS_allElementsInSet(struct ggFastIndexSet *set, NSArray* allObjects);

struct FIS_enumerator
{
    unsigned i;
    unsigned base;
    unsigned j;
    unsigned mask;
    struct ggFastIndexSet *set;
};

@interface  GGCLinkedListItem : NSObject
{
    // temporary states uses at each run.
    
    struct ggFastIndexSet       _guys;
    struct ggFastIndexSet       _guysPreviousRun;
    
    GGCLinkedListItem*          _previous;
    GGCLinkedListItem*          _next;
    unsigned                    _currentIndex;    
    
    id                          _value; //retained
}
- (id) initWithObject:(id)object;
- (id) init;

- (id) object;
- (void) setObject:(id)object;

- (BOOL) addIndexNodeForCollision:(unsigned)otherNode
                        firstLoop:(BOOL)firstLoop;
- (void) endOfLoop;

- (void) clearAllCollisionStates;
- (BOOL) inList;
- (NSArray*) collectFriendsWithNodes:(NSArray*)nodes;

- (unsigned) countOfFriends;
- (void) initIteratorOnFriends:(struct FIS_enumerator*)iteratorPtr;

- (void) setCurrentIndex:(unsigned)index;
- (unsigned) currentIndex;

#ifdef DEBUG
- (void) verifyNeutralState;
#endif

@end

unsigned FIS_nextInteger(struct FIS_enumerator *iterator);

struct GCCNode_LinkedList
{
    GGCLinkedListItem* first;
    GGCLinkedListItem* last;
};



static inline void GGCNLL_init(struct GCCNode_LinkedList *llist)
{
    llist->first = llist->last = nil;
}
GGCLinkedListItem* GGCNLL_next(GGCLinkedListItem* node);
BOOL GGCNLL_contains(struct GCCNode_LinkedList *llist, GGCLinkedListItem *guy);
void GGCNLL_append(struct GCCNode_LinkedList *llist, GGCLinkedListItem *guy);
void GGCNLL_remove(struct GCCNode_LinkedList *llist, GGCLinkedListItem *guy);
