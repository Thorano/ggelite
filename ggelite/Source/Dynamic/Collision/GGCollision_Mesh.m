//
//  GGCollision_Mesh.m
//  elite
//
//  Created by Frédéric De Jaeger on 8/30/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGCollision_Mesh.h"

#import "GGLineSorter.h"
#import "GGCLinkedList.h"
#import "GGMMesh.h"
#import "GGSolid.h"
#import "GGCTriangle.h"
#import "GGLog.h"

@interface GGCollisionManager (MeshCollisionPrivate)
- (void) _deepComputeCollisionsWithNode:(id<GGCollisionNode> )otherNode otherNode:(id<GGCollisionNode> )otherNode;

#ifdef DEBUG
- (void) _checkTriangles:(Vect3D[6])vertices;
#endif                                       

@end



@implementation GGCollisionManager (MeshCollision)
//#define USE_LEGACY_COLLISION 1
#if USE_LEGACY_COLLISION
- (void)  _computeIntersectionsWithNode:(id<GGCollisionNode>)node
                              otherNode:(id<GGCollisionNode>)otherNode 
                            faceToProbe:(NSIndexSet*)faceToProbe
{
    if(faceToProbe != nil && [faceToProbe count] == 0){
        // no face intersect the other node, skip it.
        return;
    }
    int i;
    int nface = [node numberOfFaces];
    GGPlan *facesCache = [node planeFacesCache];
    [otherNode _preComputeCacheVerticesIfNeeded];
    CacheVertex* otherNodeVerticesCache = [otherNode verticesCache];
    int nVerticesOther = [otherNode numberOfVertices];
    for(i = 0; i < nVerticesOther; ++i){
        Vect3D pos = otherNodeVerticesCache[i].pos;
        
        if(!AABB_contains([node tranformedBoundingBoxCache], pos)){
            // avoid false stuff
            continue;
        }
        unsigned  j;
        BOOL outside = NO;
        GGReal closestAlt = -1e20;
        int indexClosest = -1;
        if(faceToProbe){
            j = [faceToProbe firstIndex];
        }
        else{
            j = 0;
        }
        
        while(j < nface && !outside){
            GGReal alt = altituteFromPlanOfPoint(facesCache[j], pos);
            if(alt >= 0.0){
                outside = YES;
            }
            else if(indexClosest < 0 || alt >= closestAlt){
                closestAlt = alt;
                indexClosest = j;
            }
            
            if(faceToProbe){
                j = [faceToProbe indexGreaterThanIndex:j];
            }
            else{
                j++;
            }
        }
        if(!outside){
            NSParameterAssert(indexClosest >= 0);
            //            NSDebugMLLogWindow(@"Collision", @"collision for vertices %d", i);
            [self addNewCollisionWithObjectAtIndex:[[otherNode solid] indexForCollision]
                                     andObjectAtIndex:[[node solid] indexForCollision]
                                              atPoint:pos
                                           withNormal:facesCache[indexClosest].v
                                          penetration:-closestAlt];
        }
        else{
            //            NSDebugMLLogWindow(@"Collision", @"no collision for  vertices %d", i);
        }
    }
    
}

- (void) _computeCollisionsWithCacheWithNode:(id<GGCollisionNode> )node
                                   otherNode:(id<GGCollisionNode> )otherNode
                                boxTestFirst:(BOOL)boxTestFirst
{
    BoundingBox otherBox;
    if(boxTestFirst){
        [otherNode computeCacheTransformed];
        otherBox = [otherNode tranformedBoundingBoxCache];
        if(!AABB_intersect([node tranformedBoundingBoxCache],otherBox)){
            //            NSLog(@"skipping");
            return;
        }
    }
    int i;
    int nface = [node numberOfFaces];
    int subNodesCount = [[otherNode subNodes] count];
    [self _deepComputeCollisionsWithNode:node otherNode:otherNode];
    
    int nVerticesOther = [otherNode numberOfVertices];
    NSAssert(nVerticesOther == 0 || subNodesCount == 0, @"can't have subnodes and vertices at the same time");
    
    GGPlan *faceCaches = [node planeFacesCache];
    if(nVerticesOther){
        NSMutableIndexSet* faceToProbe;
        if(boxTestFirst){
            faceToProbe = [[NSMutableIndexSet alloc] init];
            for(i = 0; i < nface; ++i){
                if(AABB_intersectPlane(&otherBox,faceCaches[i])){
                    [faceToProbe addIndex:i];
                }
            }
        }
        else{
            faceToProbe = nil;
        }
        
        [self _computeIntersectionsWithNode:node 
                                  otherNode:otherNode 
                                faceToProbe:faceToProbe];
        //        [self _computeIntersectionsWithNode:otherNode 
        //                                     otherNode:node 
        //                                   faceToProbe:nil];
    }
}
#else

#ifdef DEBUG
- (void) _checkTriangles:(Vect3D[6])vertices
{
    if(_drawTriangles){
        if(nil == _triangles){
            _triangles = [[NSMutableData alloc] init];
        }
        [_triangles appendBytes:&vertices[0] length:sizeof(Vect3D[6])];
    }
}
#endif

- (void) _computeIntersectionsWithTriangle:(Vect3D[6])vertices
                                   atIndex:(int)i
                                   forNode:(id<GGCollisionNode>)node
                                 otherNode:(id<GGCollisionNode>)otherNode 
{
    [otherNode _preComputeCacheVerticesIfNeeded];
    CacheVertex* otherNodeVerticesCache = [otherNode verticesCache];
    FaceIndices* otherFaces = [otherNode faces];
    int nFacesOther = [otherNode numberOfFaces];
    unsigned  j;
#ifdef DEBUG
    _trianglePredicateUsage += nFacesOther;
#endif
    for(j = 0; j < nFacesOther; ++j){
        vertices[3] = otherNodeVerticesCache[otherFaces[j].A].pos;
        vertices[4] = otherNodeVerticesCache[otherFaces[j].B].pos;
        vertices[5] = otherNodeVerticesCache[otherFaces[j].C].pos;
        Vect3D norm, point;
        GGReal depth;
        BOOL firstIsTheFace;
        if(GGC_TestTriangleCollision(vertices,&norm,&depth,&point,&firstIsTheFace)){
            NSAssert(depth <= 0, @"hu");
            unsigned index1 = [[node solid] indexForCollision];
            unsigned index2 = [[otherNode solid] indexForCollision];
            [self addNewCollisionWithObjectAtIndex:firstIsTheFace ? index2 : index1
                                  andObjectAtIndex:firstIsTheFace ? index1 : index2
                                           atPoint:point
                                        withNormal:norm
                                       penetration:-depth];
        }
#ifdef DEBUG
        [self _checkTriangles:vertices];
#endif                                       
    }
}

- (void) _deepComputeCollisionsWithTriangle:(Vect3D[6])vertices
                                    atIndex:(int)indexTriangle
                                    forNode:(id<GGCollisionNode>)node
                                  otherNode:(id<GGCollisionNode>)otherNode
{
    NSArray* otherSubNodes = [otherNode subNodes];
    int subNodesCount = [otherSubNodes count];
    NSAssert(subNodesCount < 500, @"too many subnodes for the stack");
    int i;
    if(subNodesCount > 0){
        id<GGCollisionNode> subNodes[subNodesCount];
        [otherSubNodes getObjects:subNodes];
        
        for(i = 0; i < subNodesCount; ++i){
            id<GGCollisionNode> currentNode = subNodes[i];
            [otherNode computeCacheTransformed];
            BoundingBox otherBox = [otherNode tranformedBoundingBoxCache];
#ifdef DEBUG
            _triangleBox++;
#endif
            if(!AABB_intersectTriangle(&otherBox,vertices)){
                continue;
            }
            [self _deepComputeCollisionsWithTriangle:vertices
                                             atIndex:indexTriangle
                                                forNode:node
                                            otherNode:currentNode];            
        }
    }
    else {
        [self _computeIntersectionsWithTriangle:vertices
                                        atIndex:indexTriangle
                                        forNode:node
                                      otherNode:otherNode];
    }

}

- (void) _computeCollisionsWithCacheWithNode:(id<GGCollisionNode> )node
                                   otherNode:(id<GGCollisionNode> )otherNode
                                boxTestFirst:(BOOL)boxTestFirst
{
    BoundingBox otherBox;
    if(boxTestFirst){
        [otherNode computeCacheTransformed];
        otherBox = [otherNode tranformedBoundingBoxCache];
        if(!AABB_intersect([node tranformedBoundingBoxCache],otherBox)){
            //            NSLog(@"skipping");
            return;
        }
    }
    
    [node _preComputeCacheVerticesIfNeeded];
    int i;
    int nface = [node numberOfFaces];
    CacheVertex* nodeVerticesCache = [node verticesCache];
    FaceIndices* faces = [node faces];

    [otherNode _preComputeCacheVerticesIfNeeded];
    NSAssert([otherNode numberOfVertices] == 0 || [[otherNode subNodes] count] == 0, @"can't have subnodes and vertices at the same time");
#ifdef DEBUG
    _nodeNodeCollision++;   
#endif
    NSDebugMLLogWindow(@"NodeNode", @"\n(%p)%@, (%p)%@", node, node, otherNode, otherNode);
//    GGPlan *faceCaches = [node planeFacesCache];
    for(i = 0; i < nface; ++i){
        Vect3D vertices[6];
        vertices[0] = nodeVerticesCache[faces[i].A].pos;
        vertices[1] = nodeVerticesCache[faces[i].B].pos;
        vertices[2] = nodeVerticesCache[faces[i].C].pos;
#ifdef DEBUG
        if(boxTestFirst) _triangleBox++;
#endif
        if(!boxTestFirst || AABB_intersectTriangle(&otherBox,vertices)){
            [self _deepComputeCollisionsWithTriangle:vertices 
                                             atIndex:i
                                             forNode:node
                                           otherNode:otherNode];
        }
    }
}
#endif

- (void) _deepComputeCollisionsWithNode:(id<GGCollisionNode> )node
                              otherNode:(id<GGCollisionNode> )otherNode
{
    int subNodesCount = [[otherNode subNodes] count];
    NSAssert(subNodesCount < 500, @"too many subnodes for the stack");
    int i;
    if(subNodesCount > 0){
        id<GGCollisionNode> subNodes[subNodesCount];
        [[otherNode subNodes] getObjects:subNodes];
        
        for(i = 0; i < subNodesCount; ++i){
            id<GGCollisionNode> currentNode = subNodes[i];
            [self _computeCollisionsWithCacheWithNode:node
                                               otherNode:currentNode
                                            boxTestFirst:YES];            
        }
    }
}


- (void) computeCollisionsWithNode:(id<GGCollisionNode>)node
                          forNodes:(GGCLinkedListItemLineSorter**)nodes
                       inContainer:(GGCLinkedListItemLineSorter*)container
{
    [node _preComputeCacheFacesIfNeeded];
    if([node numberOfFaces] > 0 && [container countOfFriends] > 0){
        [node _preComputeCacheFacesIfNeeded];
        struct FIS_enumerator guysEnum;
        [container initIteratorOnFriends:&guysEnum];
        unsigned myIndex = [container currentIndex];
        unsigned index;
        while((index = FIS_nextInteger(&guysEnum))!=NSNotFound){
            NSAssert(index != myIndex, @"hoho");
            if(myIndex < index){
                [self _computeCollisionsWithCacheWithNode:node
                                                   otherNode:[nodes[index] node] 
                                                boxTestFirst:NO];
            }
            else{
                [self _deepComputeCollisionsWithNode:node otherNode:[nodes[index] node]];
            }
        }
    }
}


@end
