//
//  GGCollision_Mesh.h
//  elite
//
//  Created by Frédéric De Jaeger on 8/30/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGCollision.h"

@class GGCLinkedListItemLineSorter;
@interface GGCollisionManager (MeshCollision)
- (void) computeCollisionsWithNode:(id<GGCollisionNode>)node
                          forNodes:(GGCLinkedListItemLineSorter**)nodes
                       inContainer:(GGCLinkedListItemLineSorter*)container;
@end
