//
//  GGCLineSorter.m
//  elite
//
//  Created by Frederic De Jaeger on 19/04/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGLineSorter.h"
#import "utile.h"
#import "GGCLinkedList.h"
#import "GGCNode.h"
#import "GGCollision_Mesh.h"
#import "GGCNodeBuilder.h"


#pragma mark -

@implementation GGCLineSorter
- (void) _destroyBounds
{
    int i;
    for(i = 0; i < SPACE_DIM; ++i){
        if(_dimData[i].boundaries){
            free(_dimData[i].boundaries);
            _dimData[i].boundaries = NULL;
        }
        if(_dimData[i].boundariesIndices){
            free(_dimData[i].boundariesIndices);
            _dimData[i].boundariesIndices = NULL;
        }
    }
    _allocatedBounds = 0;
    _nBounds = 0;
    DESTROY(_subSorters);
}

- (void) _setupBounds
{
    _nBounds = [_items count] * 2;
    
    _allocatedBounds = 2*_nBounds;  // keep safe.
    if(0 == _allocatedBounds){
        _allocatedBounds = 4;
    }
    int i;
    for(i = 0; i < SPACE_DIM; ++i){
        _dimData[i].boundaries = AllocType(_allocatedBounds, NodeBoundaryData);
        _dimData[i].boundariesIndices = AllocType(_allocatedBounds, int);
        
        int j;
        for(j = 0; j < _allocatedBounds; ++j){
            _dimData[i].boundariesIndices[j] = j;
        }
    }
}

+ (NSArray*) newNodesFromMetaNodes:(NSArray*)metaNodes
{
    // during a first pass, we find the maximum of all the diameter of all the 
    // metanodes (node of nodes)
    NSMutableArray* nodes = [[NSMutableArray alloc] init];
    NSEnumerator* metaNodesEnum = [metaNodes objectEnumerator];
    GGCLinkedListItemLineSorter* item;
    GGReal sizeMax = 0.0;
    int i, n = [metaNodes count];
    int nbrNonLeafNode = 0;
    for(i = 0; i < n; ++i){
        item = [metaNodes objectAtIndex:i];
        id<GGCollisionNode> node = [item node];
        if([node subNodes] && [[node subNodes] count] > 0){
            nbrNonLeafNode++;
            GGReal aSize = [node diameter];
            if(aSize > sizeMax){
                sizeMax = aSize;
            }

        }
    }
    
    if(nbrNonLeafNode < 2){
        // we're done
        return nil;
    }

    //  we only split nodes bigger than a thresold
    // (this way, bigger get split first)
    
    GGReal thresold = sizeMax * RELATIVE_SIZE_SUB_NODE;
    metaNodesEnum = [metaNodes objectEnumerator];
    BOOL finalNode = YES;
    while((item = [metaNodesEnum nextObject])!=nil){
        id<GGCollisionNode> node = [item node];
        if([node subNodes] && [[node subNodes] count] > 0){
            /* we ignore all other nodes.  This is the important point.  When going deeper, we don't use again payload nodes (that contains vertices), because they were fully exploited at the current level.  That strategy might be bad, to improve.
            */
            if([node diameter] >= thresold){
                finalNode = NO;
                // We should refine the split strategy to
                // handle the situation where a big node collide with a small
                // node.  In that case, only the big one should be split.
                [nodes addObjectsFromArray:[node subNodes]];            
#ifdef DEBUG
                [item dontDraw];
#endif
            }
            else{
                [nodes addObject:node];
            }
        } 
    }
    
    NSAssert(!finalNode, @"we should have found someone to split");

    if(finalNode){
        [nodes release];
        return nil;
    }
    else{
        return nodes;        
    }
}

#pragma mark -
- (void) _setupItemsFromNodes:(NSArray*)nodes
{
    int oldSize = [_items count];
    int newSize = [nodes count];
    int i;
    if(nil == _items){
        _items = [[NSMutableArray alloc] initWithCapacity:[nodes count]];
    }
    
    if(newSize < oldSize){
        [_items removeObjectsInRange:NSMakeRange(newSize,oldSize-newSize)];
    }
    
    for(i = oldSize; i < newSize; ++i){
        GGCLinkedListItemLineSorter* item = [[GGCLinkedListItemLineSorter alloc] init];
        [item setCurrentIndex:i];
        [_items addObject:item];
        [item release];
    }
    
    NSAssert([_items count] == newSize, @"bou");
    for(i = 0; i < newSize; ++i){
        GGCLinkedListItemLineSorter* item = [_items objectAtIndex:i];
        [item setObject:[nodes objectAtIndex:i]];
    }
}

- (id) initWithCollisionNodes:(NSArray*)nodes
{
    self = [super init];
    if(self){
        [self _setupItemsFromNodes:nodes];
        [self _setupBounds];
    }
    
    return self;
}

- (id)initWithCollisionNodesInNodes:(NSArray*)metaNodes
{
    NSArray* nodes = [[self class] newNodesFromMetaNodes:metaNodes];
    self = [self initWithCollisionNodes:nodes];
    [nodes release];
    
#ifdef DEBUG
    _notRoot = YES;
#endif
    
    return self;
}

- (void) dealloc
{
    [self _destroyBounds];
    
    [_items release];
    [_subSorters release];
    
    [super dealloc];
}

- (void) _check_coherency
{
//    int i;
//    for(i = 0; i < SPACE_DIM; ++i){
//        NSIndexSet* set1 = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,_nBounds)];
//        NSMutableIndexSet* set2 = [NSMutableIndexSet indexSet];
//        int j;
//        for(j = 0; j < _nBounds; ++j){
//            [set2 addIndex:_dimData[i].boundariesIndices[j]];
//        }
//        if(![set1 isEqualToIndexSet:set2]){
//            ggabort();
//        }
//    }
}

- (NSArray*)nodes
{
    return _items;
}

- (NSArray*) subSorters
{
    if(nil == _subSorters){
        _subSorters = [[NSMutableArray alloc] initWithCapacity:3];
    }
    return _subSorters;
}


#pragma mark -
- (void) setNewCollisionNodes:(NSArray*)newNodes
{
    int dim;
    [self _check_coherency];
    [self _setupItemsFromNodes:newNodes];
    
    int oldNBounds = _nBounds;
    _nBounds = 2*[_items count];

    if(_nBounds > _allocatedBounds){
        _allocatedBounds = _nBounds;
        for(dim = 0; dim < SPACE_DIM; ++dim){
            _dimData[dim].boundaries = ReallocType(_dimData[dim].boundaries, _allocatedBounds, 
                                                   NodeBoundaryData);
            _dimData[dim].boundariesIndices = ReallocType(_dimData[dim].boundariesIndices,
                                                        _allocatedBounds, int);
        }
    }
    
    for(dim = 0; dim < SPACE_DIM; ++dim){
        int j;
        for(j = oldNBounds; j < _nBounds; ++j){
            _dimData[dim].boundariesIndices[j] = j;
        }

        if(_nBounds < oldNBounds){
            // we have to shift
            int i, index;
            for(i = 0, index = 0; i < oldNBounds; ++i){
                if(_dimData[dim].boundariesIndices[i] < _nBounds){
                    if(index < i){
                        _dimData[dim].boundariesIndices[index] = _dimData[dim].boundariesIndices[i];
                    }
                    index++;
                }
            }
        }        
    }
    [self _check_coherency];
}

- (void) setNewCollisionNodesInNodes:(NSArray*)metaNodes
{
    NSArray* nodes = [[self class] newNodesFromMetaNodes:metaNodes];

    [self setNewCollisionNodes:nodes];
    [nodes release];
}

static void insertionSort(int *boundariesIndices, int size, NodeBoundaryData* boundaries)
{
    int i, j;
    
    for(i = 0; i < size; ++i){
        for(j = i; j > 0 && boundaries[boundariesIndices[j]].value < 
            boundaries[boundariesIndices[j-1]].value; --j){
            int tmp = boundariesIndices[j];
            boundariesIndices[j] = boundariesIndices[j-1];
            boundariesIndices[j-1] = tmp;
        }
    }
}

- (void) computeIntersectionsInManager:(GGCollisionManager*)manager
{
    int i, n;
    if(0 == _nBounds) return;  // degenerated sorter.
    
//    if(_notRoot){
//        NSLog(@"not root");
//    }
    
    NSAutoreleasePool* pool = [NSAutoreleasePool new];
    [self _check_coherency];
#ifdef DEBUG
    [_items makeObjectsPerformSelector:@selector(verifyNeutralState)];
#endif
    n = [_items count];
    for(i = 0; i < n; ++i){
        GGCLinkedListItemLineSorter *ll = [_items objectAtIndex:i];
        id<GGCollisionNode> node = [ll node];;
        [node computeCacheTransformed];
        const BoundingBox box =  [node tranformedBoundingBoxCache];
        NSAssert(2*i+1 < _nBounds, @"oula");
        
        int j;
        for(j = 0; j < SPACE_DIM; ++j){
            _dimData[j].boundaries[2*i].value = box.min[j];
            _dimData[j].boundaries[2*i].isMin = YES;
            _dimData[j].boundaries[2*i+1].value = box.max[j];
            _dimData[j].boundaries[2*i+1].isMin = NO;
            _dimData[j].boundaries[2*i].item = ll;
            _dimData[j].boundaries[2*i+1].item = ll;
        }
    }
    
    struct GCCNode_LinkedList nodesInPresence;
    GGCNLL_init(&nodesInPresence);
    BOOL    firstLoop = YES;
    BOOL foundCollision = YES;
    for(i = 0; i < SPACE_DIM && foundCollision; ++i){
        NodeBoundaryData* boundaries = _dimData[i].boundaries;
        int *boundariesIndices = _dimData[i].boundariesIndices;
        insertionSort(boundariesIndices, _nBounds, boundaries);
        foundCollision = NO;
        
        int j;
        for(j = 0; j < _nBounds; ++j){
            NodeBoundaryData *nodeBound = boundaries+boundariesIndices[j];
            GGCLinkedListItemLineSorter* currentItem = nodeBound->item;
            id<GGCollisionNode> currentNode = [currentItem node];
            if(nodeBound->isMin){
                // scan the current nodes 
                
                GGCLinkedListItemLineSorter* otherItem = (GGCLinkedListItemLineSorter*)nodesInPresence.first;
                // directly compute all the direct intersection of a span.
                while(otherItem){
                    id<GGCollisionNode> otherNode = [otherItem node];
                    if([otherNode solid] != [currentNode solid]){
                        foundCollision = [otherItem addIndexNodeForCollision:[currentItem currentIndex]
                                                                   firstLoop:firstLoop] || foundCollision; 
                        foundCollision = [currentItem addIndexNodeForCollision:[otherItem currentIndex]
                                                                     firstLoop:firstLoop] ||
                            foundCollision;
                    }
                    otherItem = (GGCLinkedListItemLineSorter*)GGCNLL_next(otherItem);
                }
                GGCNLL_append(&nodesInPresence, currentItem);
            }
            else{
                NSParameterAssert(GGCNLL_contains(&nodesInPresence, currentItem));
                GGCNLL_remove(&nodesInPresence, currentItem);
            }
        }
        [_items makeObjectsPerformSelector:@selector(endOfLoop)];
        
        NSAssert(nodesInPresence.first == NULL, @"we should not have any node there");
        NSAssert(nodesInPresence.last == NULL, @"we should not have any node there");
        
        firstLoop = NO;
    }
    
    NSMutableArray* disjointUnion = nil;
    if(foundCollision){
        int n = [_items count];
        NSAssert(n > 0, @"there must be some nodes");
        NSAssert(n < 500, @"too many for the stack");
        GGCLinkedListItemLineSorter* items[n];
        [_items getObjects:items];
        
        for(i = 0; i < n; ++i){
            GGCLinkedListItemLineSorter* item = items[i];
            NSAssert([items[i] currentIndex] == i, @"gloups");
            [manager computeCollisionsWithNode:[item node]
                                      forNodes:items
                                   inContainer:item];
            if([item inList]){
                continue;
            }
            
            NSArray* friends = [item collectFriendsWithNodes:_items];
            if(friends){
                if(nil == disjointUnion){
                    disjointUnion = [[NSMutableArray alloc] initWithObjects:friends, nil];
                }
                else{
                    [disjointUnion addObject:friends];
                }
            }
        }
    }
    
    [self _check_coherency];
    [_items makeObjectsPerformSelector:@selector(clearAllCollisionStates)];
    
    if(disjointUnion && [disjointUnion count] > 0){
//        NSLog(@"we found some intersections");
        if(nil == _subSorters){
            _subSorters = [[NSMutableArray alloc] initWithCapacity:[disjointUnion count]];
        }
        
        n = MIN([_subSorters count], [disjointUnion count]);
        for(i = 0; i < n; ++i){
            GGCLineSorter* subSorter = [_subSorters objectAtIndex:i];
            [subSorter setNewCollisionNodesInNodes:[disjointUnion objectAtIndex:i]];
        }
    }

#ifdef DEBUG
    [_items makeObjectsPerformSelector:@selector(verifyNeutralState)];
#endif

    if(_subSorters){
#ifdef DEBUG
        BOOL shouldNotify = [_subSorters count] > [disjointUnion count] || i < n;
        if(shouldNotify){
            [self willChangeValueForKey:@"subSorters"];
        }
#endif
        if([_subSorters count] > [disjointUnion count]){
            [_subSorters removeObjectsInRange:NSMakeRange([disjointUnion count],
                                                          [_subSorters count] - [disjointUnion count])];
        }
        else{
            n = [disjointUnion count];
            for(; i < n; ++i){
                GGCLineSorter* subSorter = [[GGCLineSorter alloc] initWithCollisionNodesInNodes:[disjointUnion objectAtIndex:i]];
                [_subSorters addObject:subSorter];
                [subSorter release];
            }
        }
#ifdef DEBUG
        if(shouldNotify){
            [self didChangeValueForKey:@"subSorters"];
        }
#endif
    }
    [disjointUnion release];
    [pool release];
    if(_subSorters){
        [_subSorters makeObjectsPerformSelector:@selector(computeIntersectionsInManager:)
                                     withObject:manager];
    }
    
}

#pragma mark -

#ifdef DEBUG
- (void) preDrawStuff
{
    [_items makeObjectsPerformSelector:@selector(preDrawStuff)];
    [_subSorters makeObjectsPerformSelector:@selector(preDrawStuff)];
}

- (void) showNodes
{
    [_items makeObjectsPerformSelector:@selector(rasterizeIfNeeded)];
    [_subSorters makeObjectsPerformSelector:@selector(showNodes)];
}

- (void) deepDescriptionWithDepth:(int)depth
                  inMutableString:(NSMutableString*)result
{
    int i;
    for(i = 0; i < depth; ++i){
        [result appendString:@"--"];
    }
    NSArray* tmp;
    BOOL more;
    if([_items count] > 3){
        tmp = [_items subarrayWithRange:NSMakeRange(0,3)];
        more = YES;
    }
    else {
        tmp = _items;
        more = NO;
    }

    [result appendFormat:@"node = %@%s, _subSorters(%d)\n", tmp, 
    more ? ", ..." : "", [_subSorters count]];
    NSEnumerator* subSortersEnum = [_subSorters objectEnumerator];
    GGCLineSorter* lineSorter;
    while((lineSorter = [subSortersEnum nextObject])!=nil){
        [lineSorter deepDescriptionWithDepth:depth+1
                             inMutableString:result];
    }
}

- (NSString*) deepDescription
{
    NSMutableString* result = [NSMutableString  string];
    [self deepDescriptionWithDepth:0
                   inMutableString:result];
    
    return result;
}
#endif
@end

@implementation GGCLinkedListItemLineSorter
#pragma mark -
#pragma mark Debug stuff
#ifdef DEBUG
- (void) preDrawStuff
{
    _shouldDraw = YES;
}

- (void) dontDraw
{
    _shouldDraw = NO;
}

- (void) rasterizeIfNeeded
{
    if(_shouldDraw){
        rasterizeTransformedBoxForNode([self node], NO);
    }
}
#endif

- (id<GGCollisionNode>) node
{
    return [self object];
}
@end
