//
//  GGCollision.m
//  elite
//
//  Created by Frederic De Jaeger on 25/03/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGCollision.h"

#import "GGSolid.h"
#import "ModelBlend.h"
#import "GGMMesh.h"
#import "GGLog.h"
#import "GGCNode.h"
#import "GGLineSorter.h"
#import "GGCNodeBuilder.h"
#import "utile.h"
#import "GGDynamicSimulator.h"


int GGCMGeneration;

@implementation GGCollisionManager
- (id) init
{
#ifdef DEBUG
    _drawIntersections = YES;
#endif
    return self;
}

- (void) dealloc
{
    [_objects release];
    [_collisionNodes release];
    [_allCollisionsData release];
    [_topLevelSorter release];
    
    [super dealloc];
}

#ifdef DEBUG
- (void) Check_coherency
{
    NSAssert([_objects count] == [_collisionNodes count], @"oula");
    int i;
    int n = [_objects count];
    for(i = 0; i < n; ++i){
        GGSolid* solid = [_objects objectAtIndex:i];
        NSAssert([solid indexForCollision] == i, @"bad index for collision");
        GGCNode* node = [_collisionNodes objectAtIndex:i];
        NSAssert([node solid] == solid, @"decoherence between _objects and _collisionNodes");
    }
}
#define CHECK_COHERENCY [self Check_coherency]
#else
#define CHECK_COHERENCY 
#endif

- (void) addObject:(GGSolid*)object
{
    CHECK_COHERENCY;
    if(nil == _objects){
        _objects = [[NSMutableArray alloc] initWithCapacity:10];
    }
    [object setIndexForCollision:[_objects count]];
    [_objects addObject:object];
    
    if(nil == _collisionNodes){
        _collisionNodes = [[NSMutableArray alloc] initWithCapacity:10];
    }
    [_collisionNodes addObject:[object topLevelCollisionNodeInManager:self]];
    CHECK_COHERENCY;
}

- (void) removeObject:(GGSolid*)object
{
    CHECK_COHERENCY;
    int index = [_objects indexOfObjectIdenticalTo:object];
    NSAssert(index != NSNotFound, @"unknown object");
    
    NSAssert([(GGCNode*)[_collisionNodes objectAtIndex:index] solid] == object,
             @"decoherency in collision manager");
    [_objects removeObjectAtIndex:index];
    [_collisionNodes removeObjectAtIndex:index];
    
    int count = [_objects count];
    int i ;
    for(i = index; i < count; ++i){
        [(GGSolid*)[_objects objectAtIndex:i] setIndexForCollision:i];
    }
    CHECK_COHERENCY;
}

- (unsigned) numberOfObjects
{
    CHECK_COHERENCY;
    return [_objects count];
}

#pragma mark -
static NSComparisonResult compareCollisions(const CollisionData* first, const CollisionData* second)
{
    if(first->depth < second->depth){
        return NSOrderedDescending;
    }
    else if(first->depth > second->depth){
        return NSOrderedAscending;
    }
    else{
        return NSOrderedSame;        
    }
}

- (void) findCollisionsFaster
{
    _currentGeneration++;
#ifdef DEBUG
    _trianglePredicateUsage = 0;
    _triangleElaged = 0;
    _triangleBox = 0;
    _nodeNodeCollision = 0;
    [_triangles setLength:0];
#endif
    GGCMGeneration = _currentGeneration;
    [_allCollisionsData setLength:0];
    if(nil == _topLevelSorter){
        _topLevelSorter = [[GGCLineSorter alloc] initWithCollisionNodes:_collisionNodes];
    }
    else{
        [_topLevelSorter setNewCollisionNodes:_collisionNodes];
    }
#ifdef DEBUG
    [_topLevelSorter preDrawStuff];
#endif
    [_topLevelSorter computeIntersectionsInManager:self];
    qsort([_allCollisionsData mutableBytes],[_allCollisionsData length] / sizeof(CollisionData),
          sizeof(CollisionData), (int (*)(const void *, const void *))compareCollisions);
#ifdef DEBUG
    NSDebugMLLogWindow(@"Collision", @"triangle/triangle  = %d elague=%d triangle/box=%d\n nodeNodeCollision=%d", _trianglePredicateUsage, _triangleElaged, _triangleBox, _nodeNodeCollision);
#endif

}

- (CollisionData*) collectedCollisions:(int*)sizePtr
{
    int size = 0;
    const CollisionData* dataPtr = NULL;
    if(_allCollisionsData){
        size = [_allCollisionsData length] / sizeof(CollisionData);
        dataPtr = [_allCollisionsData bytes];
    }
    
    if(sizePtr){
        *sizePtr  = size;
    }
    return (CollisionData*)dataPtr;
}


- (GGSolid*) objectInCollidableAtIndex:(int)index
{
    GGSolid* ret = nil;
    if(_objects){
        ret = [_objects objectAtIndex:index];
    }
    
    return ret;
}

- (void)_addCollision:(CollisionData*)collisionData
{
    if(nil == _allCollisionsData){
        _allCollisionsData = [[NSMutableData alloc] initWithCapacity:3*sizeof(CollisionData)];
    }
    
    [_allCollisionsData appendBytes:collisionData length:sizeof(CollisionData)];
}

static inline CollisionData mkCollisionData(int index1, int index2, Vect3D location, Vect3D normal,
                                            GGReal depth)
{
    CollisionData data;
    data.index1 = index1;
    data.index2 = index2;
    data.location = location;
    data.normal = normal;
    data.depth = depth;
    
    return data;
}


- (void) addNewCollisionWithObjectAtIndex:(unsigned int)index1
                         andObjectAtIndex:(unsigned int)index2
                                  atPoint:(Vect3D)location
                               withNormal:(Vect3D)normal
                              penetration:(GGReal)depth
{
    GGReal normeNormal = 0.0;
    NSParameterAssert(depth >= 0);
    if(normaliseVectAndGetNorm(&normal, &normeNormal)){
        CollisionData data = mkCollisionData(index1,index2,location,normal,depth/normeNormal);
        [self _addCollision:&data];
    }
    else{
        [NSException raise:NSInternalInconsistencyException
                    format:@"degenerated normal"];
    }
}

- (unsigned) currentGeneration
{
    return _currentGeneration;
}

- (NSArray*) collisionNodes
{
    return _collisionNodes;
}

- (GGCLineSorter*) sorter
{
    if(nil == _topLevelSorter){
        _topLevelSorter = [[GGCLineSorter alloc] initWithCollisionNodes:[NSArray array]];
    }
        
    return _topLevelSorter;
}

#ifdef DEBUG

void rasterizeTransformedBoxForNode(id<GGCollisionNode> node, BOOL selected)
{
    Vect3D pts[8];
    [node computeCacheTransformed];
    BoundingBox domain = [node tranformedBoundingBoxCache];
    GGReal xmin = domain.min[0];
    GGReal ymin = domain.min[1];
    GGReal zmin = domain.min[2];
    GGReal xmax = domain.max[0];
    GGReal ymax = domain.max[1];
    GGReal zmax = domain.max[2];
    
    initVect(&pts[0], xmin, ymin, zmin);
    initVect(&pts[1], xmin, ymin, zmax);
    initVect(&pts[2], xmin, ymax, zmin);
    initVect(&pts[3], xmin, ymax, zmax);
    initVect(&pts[4], xmax, ymin, zmin);
    initVect(&pts[5], xmax, ymin, zmax);
    initVect(&pts[6], xmax, ymax, zmin);
    initVect(&pts[7], xmax, ymax, zmax);
    
    VectCol col = {
        0, 0, 0, 0,
    };
    float selectionFactor = selected ? 0.5 : 0.0;
    if([node isKindOfClass:[GGCLeaf class]]){
        col.r = 0.9+selectionFactor;
        col.g = 0.2+selectionFactor;
        col.b = 0.2+selectionFactor;
    }
    else{
        col.r = 0.9+selectionFactor;
        col.g = 0.7+selectionFactor;
        col.b = 0.2+selectionFactor;
    }
    if(selected){
        glPushAttrib(GL_LIGHTING_BIT);
        VectCol emit = {0.5, 0.1, 0.5, 1};
        glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GGReal*)&emit);
    }
    ggPlotBoxCol(pts, col);
    if(selected){
        glPopAttrib();
    }
}

void rasterizeTrianglesForNode(id<GGCollisionNode> node)
{
    unsigned faceIndex, nbrFace;
    [node computeCacheTransformed];
    [node _preComputeCacheFacesIfNeeded];
    nbrFace = [node numberOfFaces];
    FaceIndices* faces = [node faces];
    struct __cacheVertex* vertices = [node verticesCache];
    NSDebugMLLogWindow(@"Node", @"faces for collision: %d", nbrFace);

    glPushAttrib(GL_LIGHTING_BIT | GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);
    VectCol emit = {0.1, 0.6, 0.5, 1};
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GGReal*)&emit);
    glDisable(GL_CULL_FACE);
    glDisable(GL_LIGHTING);
    glDepthMask(YES);

    glBegin(GL_TRIANGLES);
    glColor4v(&emit);
    for(faceIndex = 0; faceIndex < nbrFace; ++faceIndex){
        NSCAssert(faces[faceIndex].A < [node numberOfVertices], @"vertice index too big");
        NSCAssert(faces[faceIndex].B < [node numberOfVertices], @"vertice index too big");
        NSCAssert(faces[faceIndex].C < [node numberOfVertices], @"vertice index too big");
        glVertex3v(&(vertices[faces[faceIndex].A].pos));
        glVertex3v(&(vertices[faces[faceIndex].B].pos));
        glVertex3v(&(vertices[faces[faceIndex].C].pos));
    }
    glEnd();
    
    glPopAttrib();
}


- (void) rasterizeTranformedBox:(id<GGCollisionNode>) node
                           path:(NSIndexPath*)path
                 selectionPaths:(NSArray*)selectionPaths
{
    if([path length] == [self rasterizationDepth]){
        rasterizeTransformedBoxForNode(node, [selectionPaths containsObject:path]);
    }
    else{
        NSArray* subNodes = [node subNodes];
        int i, n = [subNodes count];
        for(i = 0; i < n; ++i){
            NSIndexPath* subPath = [path indexPathByAddingIndex:i];
            id<GGCollisionNode> subNode = [subNodes objectAtIndex:i];
            [self rasterizeTranformedBox:subNode
                                   path:subPath
                          selectionPaths:selectionPaths];
        }
    }
}

// this code rastering all the node at a selected depth/or show the intersections.
- (void) rasterise: (Camera *)cam
    selectionPaths:(NSArray*)selectionPaths
{
    NSDebugMLLogWindow(@"Collision", @"levelSorter:\n%@", [_topLevelSorter deepDescription]);

    if([self drawIntersections]){
        [_topLevelSorter showNodes];
    }
    else{
        int i, n = [_collisionNodes count];
        for(i = 0; i < n; ++i){
            NSIndexPath* path = [[NSIndexPath alloc] initWithIndex:i];
            id<GGCollisionNode> node = [_collisionNodes objectAtIndex:i];
            [self rasterizeTranformedBox:node path:path selectionPaths:selectionPaths];
            [path release];
        }
    }
}

//this code draw the selection in the outline view of tree node.
- (void) drawSelection
{
#if DEBUG_COCOA
    NSArray* nodes  = [[GGDynamicDebugController controller] selectedNodes];
    int i, n = [nodes count];
    
    for(i = 0; i < n; ++i){
        id<GGCollisionNode> node = [nodes objectAtIndex:i];
        rasterizeTransformedBoxForNode(node, NO);
    }
#endif
}

- (void) rasterise: (Camera *)cam
{
#if DEBUG_COCOA
    if(_showBoxes){
        [self rasterise:cam selectionPaths:[[GGDynamicDebugController controller] selectionIndexPaths]];
    }
//    [self drawSelection];
#endif
    if(_drawTriangles && _triangles && [_triangles length] > 0){
        Vect3D  (*triangles)[6] = (Vect3D  (*)[6])[_triangles bytes];
        unsigned i;
        unsigned count = [_triangles length]/sizeof(*triangles);
        NSAssert(count > 0, @"hoho");
        glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_ENABLE_BIT | GL_LIGHTING_BIT |
                     GL_POLYGON_BIT | GL_CURRENT_BIT);
        glPolygonMode(GL_FRONT, GL_FILL);
        glDepthMask(GL_FALSE);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonOffset(0, -10);
//        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glShadeModel(GL_FLAT);
        glColor4f(0.8, 0.5, 0.4, 0.3);
        glDisable(GL_LIGHTING);
        glDisable(GL_CULL_FACE);
        
        for(i = 0; i < count; ++i){
            Vect3D *aTriangle = *triangles;
            glBegin(GL_TRIANGLES);
            {
                int j;
                for(j=0; j < 6;++j){
                static const MapCol mapCol[] =
                {
                  { 0, {  0.7, 0.7, 0.7, 1}},
                  { 0.2, {  0.2, 0.2, 0.2, 1}},
                  { 0.4, {  0.3, 0.3, 0.3, 1}},
                  { 0.6, {  0.9, 0.9, 0.9, 1}},
                  { 0.8, {  0.3, 0.3, 0.3, 1}},
                  { 0.9, {  0.2, 0.2, 0.2, 1}},
                  { 1.0, {  0.5, 0.8, 0.9, 1}}
                };
                    VectCol col = genColFromMapHeight(mapCol,sizeof(mapCol)/sizeof(MapCol),(float)i/(float)count);
                    glColor4v(&col);
                    glVertex3v(aTriangle+j);
//                    NSLog(@"(%d, %d, %p): %@", i, j, aTriangle+j, stringOfVect2(aTriangle[j]));
                }
            }
            glEnd();
            triangles++;
        }
        glEnable(GL_LIGHTING);
        
        glPopAttrib();
    }
}


- (int)rasterizationDepth {
    return _rasterizationDepth;
}

- (void)setRasterizationDepth:(int)value {
    if (_rasterizationDepth != value) {
        _rasterizationDepth = value;
    }
}

- (BOOL)drawIntersections {
    return _drawIntersections;
}

- (void)setDrawIntersections:(BOOL)value {
        _drawIntersections = value;
}

#endif

@end

