//
//  GGCTriangle.m
//  elite
//
//  Created by Frederic De Jaeger on 30/07/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#ifdef UTEST
#import <SenTestingKit/SenTestingKit.h>
#endif

#import "GGCTriangle.h"

static int allConfigurations[][6] = 
{
{0,1,2,3,4,5},

{0,1,3,2,4,5},
{0,1,4,2,3,5},
{0,1,5,2,3,4},

{0,2,3,1,4,5},
{0,2,4,1,3,5},
{0,2,5,1,3,4},

{1,2,3,0,4,5},
{1,2,4,0,3,5},
{1,2,5,0,3,4},
  
{0,3,4,1,2,5},
{0,3,5,1,2,4},
{0,4,5,1,2,3},

{1,3,4,0,2,5},
{1,3,5,0,2,4},
{1,4,5,0,2,3},

{2,3,4,0,1,5},
{2,3,5,0,1,4},
{2,4,5,0,1,3},
    
{3,4,5,0,1,2},

};

static inline int positionOfPoint(GGPlan plane, Vect3D point){
    GGReal alt = altituteFromPlanOfPoint(plane, point);
    if(alt > EPS){
        return +1;
    }
    else if(alt > -EPS){
        return 0;
    }
    else{
        return -1;
    }
}

static inline BOOL positionCompatible(int pos1, int pos2)
{
    if(pos1 == 0 || pos2 == 0) return YES;
    return pos1 == pos2;
}

static inline BOOL positionAntiCompatible(int pos1, int pos2)
{
    if(pos1 == 0 || pos2 == 0) return YES;
    return pos1 != pos2;
}

static inline int positionFromCompatiblePositions(int pos1, int pos2)
{
    NSCParameterAssert(positionCompatible(pos1,pos2));
    if(0 == pos1) return pos2;
    return pos1;
}

static inline GGReal chooseDeepest(GGPlan plan, const Vect3D vertices[3], Vect3D* pointRef)
{
    GGReal val = altituteFromPlanOfPoint(plan,vertices[0]);
    GGReal tmp = altituteFromPlanOfPoint(plan,vertices[1]);
    if(tmp < val){
        val = tmp;
        *pointRef = vertices[1];
    }
    else{
        *pointRef = vertices[0];
    }
    
    tmp = altituteFromPlanOfPoint(plan,vertices[2]);
    if(tmp<val){
        val = tmp;
        *pointRef = vertices[2];
    }
    
    return val;
}

#define PositionOfVertexNumber(i) positionOfPoint(plane, vertices[configuration[i]])

BOOL GGC_TestTriangleCollision(const Vect3D vertices[6], Vect3D* normRef, GGReal* depthRef, Vect3D* pointRef, BOOL* isFirstTheFaceRef)
{
    int i;
    BOOL degenerated = NO;
    
    for(i = 0; i < 20; ++i){
        int *configuration = allConfigurations[i];
        Vect3D normTriangle;
        {
            Vect3D tmp1, tmp2;
            diffVect(&vertices[configuration[1]],&vertices[configuration[0]],&tmp1);
            diffVect(&vertices[configuration[2]],&vertices[configuration[0]],&tmp2);
            prodVect(&tmp1,&tmp2,&normTriangle);
        }
        
        if(prodScal(&normTriangle,&normTriangle) <= EPS*EPS){
            // degenerated triangle, 
            continue;
        }
        
//        GGReal offset = -prodScal(&normTriangle,vertices[configuration[0]]);
        GGPlan plane = mkPlanFromPointAndNormal(vertices[configuration[0]],normTriangle);
        // iterate on all remaining vertices.
        int pos1 = 0;
        int pos2 = 0;
        int j;
        for(j = 3; j < 6; ++j){
            int tmpPos = PositionOfVertexNumber(j);
            if(configuration[j]<3){
                // a vertice from T1.
                if(positionCompatible(pos1,tmpPos)){
                    pos1 = positionFromCompatiblePositions(pos1, tmpPos);
                }
                else{
                    // the triangle split T1.
                    goto next;
                }
            }
            else{
                // a vertice from T2.
                if(positionCompatible(pos2,tmpPos)){
                    pos2 = positionFromCompatiblePositions(pos2, tmpPos);
                }
                else{
                    goto next;
                }
            }
        }

        if(0 == pos1 && 0 == pos2){
            degenerated = YES;
            break;
        }
        // at that point, the triangle does not split T1, nor T2.
        if(positionAntiCompatible(pos1, pos2)){
            // that triangle splits T1 and T2!
            return NO;
        }
next:
        {
            
        }
    }
    
    // XXX handle the degenerated situation there:
    Vect3D normTriangle1, normTriangle2;
    {
        Vect3D tmp1, tmp2;
        diffVect(&vertices[1],&vertices[0],&tmp1);
        diffVect(&vertices[2],&vertices[0],&tmp2);
        prodVect(&tmp1,&tmp2,&normTriangle1);

        diffVect(&vertices[4],&vertices[3],&tmp1);
        diffVect(&vertices[5],&vertices[3],&tmp2);
        prodVect(&tmp1,&tmp2,&normTriangle2);        
    }
    
    // who is the widest.
    GGPlan plan;
    const Vect3D* verts = NULL;
    if(prodScal(&normTriangle1,&normTriangle1) > prodScal(&normTriangle2,&normTriangle2))
    {
        // 1 is the widest.
        plan = mkPlanFromPointAndNormal(vertices[0],normTriangle1);
        verts = vertices+3;
        *normRef = normTriangle1;
        *isFirstTheFaceRef = YES;
    }
    else{
        plan = mkPlanFromPointAndNormal(vertices[3],normTriangle2);
        verts = vertices;
        *normRef = normTriangle2;
        *isFirstTheFaceRef = NO;
    }
    *depthRef = chooseDeepest(plan,verts, pointRef);
//    NSDebugLLog(@"Triangle", @"normal: %@", stringOfVect(normRef));
    
    // none of the 20 triangles split T1 and T2.  This means they collide.
    return YES;
}

#ifdef UTEST

@interface GGTriangleTest : SenTestCase {
    
}

@end

@implementation GGTriangleTest
- (void) testConfigurations
{
    int i;
    STAssertTrue(sizeof(allConfigurations)/sizeof(int[6]) == 20, @"bad size of all conf: %d", sizeof(allConfigurations)/sizeof(int[6]));
    for(i = 0; i < sizeof(allConfigurations)/sizeof(int[6]); ++i){
        int marker[6];
        bzero(marker,sizeof(marker));
        int j;
        for(j = 0; j < 6; ++j){
            marker[allConfigurations[i][j]] = 1;
        }
        
        // verify;
        for(j = 0; j < 6; ++j){
            STAssertTrue(marker[j] == 1, @"bad configuration at rank %d", i);
        }
    }
}
@end
#endif

