//
//  GGCTriangle.h
//  elite
//
//  Created by Frederic De Jaeger on 30/07/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GG3D.h"

BOOL GGC_TestTriangleCollision(const Vect3D vertices[6], Vect3D* normRef, GGReal* depthRef, Vect3D* pointRef, BOOL* isFirstTheFaceRef);
