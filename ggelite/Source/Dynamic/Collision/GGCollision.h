//
//  GGCollision.h
//  elite
//
//  Created by Frederic De Jaeger on 25/03/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GG3D.h"

@class GGSolid;

typedef struct __CollisionData
{
    int                     index1;
    int                     index2;
    Vect3D                  location;
    Vect3D                  normal;
    GGReal                  depth;
}CollisionData;

@class GGCLineSorter;

extern int GGCMGeneration;

@interface GGCollisionManager : NSObject {
    NSMutableArray*         _objects;
    NSMutableArray*         _collisionNodes;
    
    NSMutableData*          _allCollisionsData;
    
    GGCLineSorter*          _topLevelSorter;
    unsigned                _currentGeneration;

#ifdef DEBUG
    unsigned                _trianglePredicateUsage;
    unsigned                _triangleElaged;
    unsigned                _triangleBox;
    unsigned                _nodeNodeCollision;
    int                     _rasterizationDepth;
    BOOL                    _drawIntersections;
    NSMutableData           *_triangles;
    BOOL                    _drawTriangles;
    BOOL                    _showBoxes;
#endif
}
- (id) init;
- (void) addObject:(GGSolid*)object;
- (void) removeObject:(GGSolid*)object;
- (unsigned) numberOfObjects;

#pragma mark -
- (void) findCollisionsFaster;
- (CollisionData*) collectedCollisions:(int*)sizePtr;

// used by object to add collision (called from inside "findCollisions)
- (void) addNewCollisionWithObjectAtIndex:(unsigned int)index1
                         andObjectAtIndex:(unsigned int)index2
                                  atPoint:(Vect3D)location
                               withNormal:(Vect3D)normal
                              penetration:(GGReal)depth;
//- (void)addCollision:(CollisionData*)collisionData;
- (GGSolid*) objectInCollidableAtIndex:(int)index;

- (unsigned) currentGeneration;
- (NSArray*) collisionNodes;

#ifdef DEBUG
- (void) rasterise: (Camera *)cam;
- (void) rasterise: (Camera *)cam
    selectionPaths:(NSArray*)selectionPaths;
- (int)rasterizationDepth;
- (void)setRasterizationDepth:(int)value;
- (BOOL)drawIntersections;
- (void)setDrawIntersections:(BOOL)value;


#endif
@end

typedef struct __faceIndices
{
    uint16_t A, B, C;
}FaceIndices;

@protocol GGCollisionNode <NSObject>
- (GGSolid*)solid;

- (GGReal) diameter;
- (NSArray*)subNodes;

    // for the bounding box computation.
    // XXX needs to be cleaned
- (void) computeCacheTransformed;
- (const struct __BoundingBox) tranformedBoundingBoxCache;

- (void)_preComputeCacheVerticesIfNeeded;
- (void) _preComputeCacheFacesIfNeeded;

    // mesh access
- (unsigned) numberOfVertices;
- (struct __cacheVertex*) verticesCache;

- (unsigned) numberOfFaces;
- (FaceIndices*) faces;
- (GGPlan*) planeFacesCache;

@end

void rasterizeTransformedBoxForNode(id<GGCollisionNode> node, BOOL selected);
void rasterizeTrianglesForNode(id<GGCollisionNode> node);
