//
//  GGCNode.m
//  elite
//
//  Created by Frederic De Jaeger on 26/03/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "GGCNode.h"
#import "GGCollision.h"
#import "GGMMesh.h"
#import "GGSolid.h"
#import "utile.h"
#import "GGLineSorter.h"
#import "GGCLinkedList.h"
#import "GGIdGenerator.h"

@implementation GGCNodeCommon
- (void) _computeDiameter
{
    int i;
    _diameter = 0.0;
    for(i = 0; i < SPACE_DIM; ++i){
        _diameter += _domain.max[i]-_domain.min[i];
    }
    
}

- (id) initWithSolid:(GGSolid*)solid
              domain:(BoundingBox)domain
           inManager:(GGCollisionManager*)manager
{
    _mainSolid = solid;
    _domain = domain;
    _manager = manager;
    
    
    [self _computeDiameter];
    
    return self;
}

- (NSString*) descriptionWithDepth:(int)depth
{
    [self subclassResponsibility:_cmd];
    return nil;
}

- (NSString*) deepDescription
{
    return [self descriptionWithDepth:0];
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"GGCNode(%@)", _mainSolid];
}

- (BOOL)isEqual:(id)anObject
{
    return anObject == self;
}

- (BoundingBox) domain
{
    return _domain;
}

- (GGSolid*)solid
{
    NSParameterAssert(_mainSolid);
    return _mainSolid;
}

- (NSArray*)subNodes
{
    return nil;
}

- (GGReal) diameter
{
    return _diameter;    
}


#pragma mark -
#pragma mark runtime stuff

- (void) computeCacheTransformed
{
    ggTransformBox(&_domain, &_mainSolid->_position, &_transformedDomain);
}

- (const BoundingBox) tranformedBoundingBoxCache
{
    return _transformedDomain;
}

- (void)_preComputeCacheVerticesIfNeeded
{
    
}

- (void) _preComputeCacheFacesIfNeeded
{
    
}

- (GGCollisionManager*)manager
{
    return _manager;
}

- (struct __cacheVertex*) verticesCache
{
    [self subclassResponsibility:_cmd];
    return NULL;
}

- (unsigned) numberOfVertices
{
    return 0;
}

- (unsigned) numberOfFaces
{
    return 0;
}

- (GGPlan*) planeFacesCache
{
    [self subclassResponsibility:_cmd];
    return NULL;
}

- (FaceIndices*) faces
{
    return NULL;
}


- (BOOL) nodeIntersectBox:(const struct __BoundingBox*)box
{
    //conservative answer
    return YES;
}


@end

@implementation GGCNode
- (id) initWithSubNodes:(NSArray*)nodes
                inSolid:(GGSolid*)solid
              inManager:(GGCollisionManager*)manager
{
    BoundingBox domain;
    if(nodes == nil || [nodes count] == 0){
        [self release];
        return nil;
    }
    
    BOOL first = YES;
    NSEnumerator* nodeEnum = [nodes objectEnumerator];
    GGCNode* node;
    while((node = [nodeEnum nextObject])!=nil){
        if(first){
            domain = node->_domain;
            first = NO;
        }
        else{
            extendDomainByArray(&domain,node->_domain.min);
            extendDomainByArray(&domain,node->_domain.max);
        }
    }
    self = [super initWithSolid:solid
                         domain:domain
                      inManager:manager];
    if(self){
        _subNodesAsArray = [nodes copy];
    }

    return self;
}

- (void) dealloc
{
    RELEASE(_subNodesAsArray);
    
    [super dealloc];
}

- (NSArray*)subNodes
{
    NSParameterAssert(_subNodesAsArray);
    return _subNodesAsArray;
}

- (NSString*) descriptionWithDepth:(int)depth
{
    NSMutableString* string = [NSMutableString string];
    int i;
    for(i = 0; i < depth; ++i){
        [string appendString:@"--"];
        
    }
    [string appendFormat:@"GGCNode{face indices=%d %d subnodes\n", [_subNodesAsArray count]];
    
    NSEnumerator* subNodeEnum = [_subNodesAsArray objectEnumerator];
    GGCNodeCommon* subNode;
    while((subNode = [subNodeEnum nextObject])!=nil){
        [string appendString:[subNode descriptionWithDepth:depth+1]];
    }
    for(i = 0; i < depth; ++i){
        [string appendString:@"--"];
    }
    [string appendString:@"}\n"];
    
    return string;
}
@end 


@implementation GGCLeaf
NSMutableIndexSet* sIdGenerator;
+ (void) initialize
{
    if(self == [GGCLeaf class]){
        sIdGenerator = [NSMutableIndexSet new];
    }
}

+ (int) nextAvailableIdentifier
{
    NSAssert(sIdGenerator, @"not correctly initialized");
    return [sIdGenerator nextAvailableIdentifier];
}

+ (void) releaseIdentifier:(unsigned)identifier
{
    NSAssert(sIdGenerator, @"not correctly initialized");
    [sIdGenerator releaseIdentifier:identifier];
}

static inline uint16_t mapIndices(NSDictionary* map, unsigned index, unsigned max)
{
    NSNumber* output = [map objectForKey:[NSNumber numberWithInt:index]];
    NSCAssert2(output, @"no output for  %d in %@", index, map);
    
    unsigned res = [output unsignedIntValue];
    NSCAssert(res < 65536, @"too big");
    NSCAssert(res < max, @"too big too");
    return (uint16_t)res;
}

- (void) _setupFaces
{
    GGMMesh* mesh = [_meshNode mesh];
    NSMutableDictionary* map = [[NSMutableDictionary alloc] initWithCapacity:[_vertexIndices count]];
    NSAssert(_vertexIndices != nil, @"we should have indices");
    // first compute the mapping.
    unsigned index = [_vertexIndices firstIndex];
    unsigned i = 0;
    while(index != NSNotFound){
        [map setObject:[NSNumber numberWithUnsignedInt:i] forKey:[NSNumber numberWithUnsignedInt:index]];
        index = [_vertexIndices indexGreaterThanIndex:index];
        i++;
    }
    NSAssert([map count] == [_vertexIndices count], @"wrong count");
    
    NSAssert(_faces == NULL, @"setup twice!?");
    NSAssert(_faceIndices != nil, @"we should have faces");
    _faces = AllocType([_faceIndices count], FaceIndices);
    index = [_faceIndices firstIndex];
    FaceBlend* faces = [mesh faces];
    NSAssert([_faceIndices count] <= [mesh numberOfFaces], @"too big");
    i = 0;
    BOOL orientationFlipped = [_meshNode orientationFlipped];
    while(index != NSNotFound){
        uint16_t A, B, C;
        A = mapIndices(map, faces[index].A, [_vertexIndices count]);
        B = mapIndices(map, faces[index].B, [_vertexIndices count]);
        C = mapIndices(map, faces[index].C, [_vertexIndices count]);
        NSAssert(i < [_faceIndices count], @"index too big");
        _faces[i].A = A;
        _faces[i].B = orientationFlipped ? C : B;
        _faces[i].C = orientationFlipped ? B : C;
        index = [_faceIndices indexGreaterThanIndex:index];
        i++;
    }
    [map release];
    NSAssert(i == [_faceIndices count], @"mismatch in indices");
    // 
}

- (id) initWithMeshNode:(GGMNode*)node
                inSolid:(GGSolid*)solid
                 domain:(BoundingBox)domain
            faceIndices:(NSIndexSet*)faceIndices
          vertexIndices:(NSIndexSet*)vertexIndices
              inManager:(GGCollisionManager*)manager
{
    self = [super initWithSolid:solid
                         domain:domain
                      inManager:manager];
    if(self){
        _faceIndices = [faceIndices copy];
        _vertexIndices = [vertexIndices copy];
        _meshNode = [node retain];
        _nVertex = [_vertexIndices count];
        _identifier = [GGCLeaf nextAvailableIdentifier];
        [self _setupFaces];
    }
    
    return self;
}

- (void) dealloc
{
    [GGCLeaf releaseIdentifier:_identifier];
    RELEASE(_faceIndices);
    RELEASE(_vertexIndices);
    RELEASE(_meshNode);
    if(_verticesCache){
        free(_verticesCache);
    }
    
    if(_facesCache){
        free(_facesCache);
    }
    
    if(_faces){
        free(_faces);
    }
    
    [super dealloc];
}

- (NSString*) descriptionWithDepth:(int)depth
{
    NSMutableString* string = [NSMutableString string];
    int i;
    for(i = 0; i < depth; ++i){
        [string appendString:@"--"];
        
    }
    [string appendFormat:@"GGCNode{face indices=%d\n", [_faceIndices count]];
    return string;
}

- (void) _preComputeCacheFacesIfNeeded
{
    if(_facesCacheGeneration == [_manager currentGeneration]){
        return;
    }
    NSAssert(_faceIndices && [_faceIndices count] > 0, @"should have some faces when updating cache");
    if(NULL == _facesCache){
        _facesCache = AllocType([_faceIndices count], GGPlan);
    }
    
    [_meshNode computeFaceEquationsForIndices:_faceIndices
                            forTransformation:&_mainSolid->_position
                                    inResults:_facesCache];
    
    _facesCacheGeneration = [_manager currentGeneration];
}

- (void) _preComputeCacheVerticesIfNeeded
{
    if(0 == _nVertex || _verticesCacheGeneration == [_manager currentGeneration]){
        return;
    }
    
    NSParameterAssert([_vertexIndices count] > 0);
    if(NULL == _verticesCache){
        _verticesCache = AllocType([_vertexIndices count],CacheVertex);
    }
    
    [_meshNode computeVerticesAtIndices:_vertexIndices
                      forTransformation:&_mainSolid->_position
                              inResults:_verticesCache];
    _verticesCacheGeneration = [_manager currentGeneration];
}

- (unsigned) numberOfVertices
{
    return [_vertexIndices count];
}

- (struct __cacheVertex*) verticesCache
{
    NSAssert(_verticesCache, @"cache not computed");
    return _verticesCache;
}

- (unsigned) numberOfFaces
{
    return [_faceIndices count];
}

- (FaceIndices*) faces
{
    NSAssert(_faces, @"not computed");
    return _faces;
}


- (GGPlan*) planeFacesCache
{
    return _facesCache;
}

- (BOOL) nodeIntersectBox:(const struct __BoundingBox*)box
{
    [self _preComputeCacheFacesIfNeeded];
    [self _preComputeCacheVerticesIfNeeded];
#ifdef DEBUG
    //  check that the cache is correct.  Then
    NSParameterAssert(_facesCacheGeneration == GGCMGeneration);
    NSParameterAssert(_verticesCacheGeneration == GGCMGeneration);
#endif
    
    int nface = [_faceIndices count];
    int nvert = [self numberOfVertices];
    int i;
    NSAssert(_facesCache!=nil, @"face cache not initialized");
    // first test if all the vertices are outside the box, separated by a plane.
    if(nvert > 0){
        uint16_t sig = AABB_SignatureOfPoint(box,_verticesCache[0].pos);
        for(i = 1; i < nvert; ++i){
            sig &= AABB_SignatureOfPoint(box,_verticesCache[i].pos);
        }
        if(sig != 0 && sig != SigInBox){
            // it means all the point are in the same side of one of the box border.
            return NO;
        }
    }
    
    // then, test if the box does not cross any of the faces.
    
    for(i = 0; i < nface; ++i){
        if(AABB_intersectPlane(box,_facesCache[i])){
            return YES;
        }
    }
    
    // if we reach that point, none of the face cross the box.
    return NO;
    //iteration on all the faces.
}
@end