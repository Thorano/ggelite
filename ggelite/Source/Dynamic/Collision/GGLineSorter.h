//
//  GGCLineSorter.h
//  elite
//
//  Created by Frederic De Jaeger on 19/04/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GG3D.h"
#import "GGCLinkedList.h"
#import "GGAABB.h"

@class GGSolid;
@class GGCollisionManager;
@protocol GGCollisionNode;

@interface GGCLinkedListItemLineSorter :  GGCLinkedListItem
{
#ifdef DEBUG
BOOL                    _shouldDraw;
#endif
}
#ifdef DEBUG
- (void) preDrawStuff;
- (void) dontDraw;

- (void) rasterizeIfNeeded;
#endif
    // to enforce type safety.
- (id<GGCollisionNode>) node;
@end




typedef struct __nodeBoundaryData
{
    GGReal                      value;
    BOOL                        isMin;  // else it is max.
    GGCLinkedListItemLineSorter*          item;
}NodeBoundaryData;

typedef struct __ByLineData
{
    NodeBoundaryData*           boundaries;
    int*                        boundariesIndices;
}ByLineData;

@interface GGCLineSorter : NSObject {
    struct __ByLineData         _dimData[SPACE_DIM];
    int                         _nBounds;
    int                         _allocatedBounds;
    NSMutableArray*             _items;
    
    NSMutableArray*             _subSorters;
    
#ifdef DEBUG
    BOOL                        _notRoot;
#endif
}
- (id) initWithCollisionNodes:(NSArray*)nodes;

- (void) setNewCollisionNodes:(NSArray*)newNodes;
- (void) computeIntersectionsInManager:(GGCollisionManager*)manager;
- (NSArray*) subSorters;


#ifdef DEBUG
- (void) preDrawStuff;
- (void) showNodes;
- (NSString*) deepDescription;
#endif
@end

