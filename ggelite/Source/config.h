/*
 *  config.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: Mars 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __config_h
#define __config_h

#if GNUSTEP_BASE_LIBRARY
#define GLPath(header) <GL/header>
#else
#define GLPath(header) <OpenGL/header>
#endif

#define id __id
#import GLPath(gl.h)
#import GLPath(glu.h)
#undef id

//typedef GLdouble GGReal;

typedef GLfloat GGReal;

typedef short int int16;

/* various constant used everywhere...
 */

#define NBRFLARES 12
#define NTEXTUREFLARE 6
//#define GGDefaultFontName @"lynx.ttf" un peu nul
#define GGDefaultFontName @"c0648bt_.pfb"
#define Fixed6x12 @"6x12.pcf"
#define Fixed7x14 @"7x14.pcf"
#define UnicodeFontName @"cu12.pcf"

#define HauteurTableauBord 100
#define Focal 45

#define ScaleFactorForShipView 1.5
#define SIZETEXTURESKY 512

#define NBRSEGMENTINCIRCLE 50

#define distanceCameraRepere2  1e12

#define BorderColor {0.4, 1, 0.4}

#ifndef GGBEGINDEC 
#define GGBEGINDEC 
#endif
#ifndef GGENDDEC 
#define GGENDDEC 
#endif

#define MassePerPower 0.01
#define MassePerPowerCollision 0.003

//size of a zone in 0.01 LY
#define StarsZoneSize  2000.0

#ifndef RETAIN
#define RETAIN(x) [(x) retain]
#define ASSIGN(x, y) do{\
    id __tmp = y;       \
    if( x != __tmp){    \
      if ( x ) [x release];\
      x = [__tmp retain];\
    }\
  } while(0)
#define RELEASE(x) [(x) release]
#define AUTORELEASE(x) [(x) autorelease]
#define DESTROY(x) do{\
        if ( x ) [x release];\
        x = nil;\
        }while(0)
#define TEST_RELEASE(x) do{\
            id __tmp = (x);\
            if ( __tmp ) [__tmp release];\
            }while(0)
#endif

#ifdef MACOSX
#import "macos.h"
#endif

#if defined(DEBUG) && defined(HAVE_COCOA)
#define DEBUG_COCOA 1
#endif

#endif
