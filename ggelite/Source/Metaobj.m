/*
 *  Metaobj.m
 *
 *  Copyright (c) 1999 Free Software Foundation, Inc.
 *  
 *  Author: Frédéric De Jaeger
 *  Date: May 2000
 * 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSObject.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDebug.h>
#import <Foundation/NSLock.h>
#import <Foundation/NSThread.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSHashTable.h>
#import "Metaobj.h"
#import <stdio.h>
#define MAXOBJ 10000

//NSObject *listobj[MAXOBJ];
static int mode;
static int state;
static NSRecursiveLock *monLock = nil;


static NSString *ggdescribe(NSHashTable *t, const void * obj)
{ 
  id b = (id)obj;
  return [b description];
}

static NSHashTable *objects;

static NSHashTableCallBacks hashcb = 
  { NULL, NULL, NULL, NULL, ggdescribe };


@interface ObjectControl : NSObject
+ alloc;
- (void) dealloc;
@end

int setMode(int newmode)
{
  int old = mode;
  mode = newmode;
  return old;
}
/*
void erreur(const char *format, ...)
{
     va_list theva;
     va_start(theva, format);
     fprintf(stderr, "Erreur : ");
     vfprintf(stderr, format, theva);
     fprintf(stderr, "\n");
     va_end(theva);
     abort();
//     exit(1);
}
*/

void initMetaObj()
{
     [ObjectControl poseAsClass: [NSObject class]];
     setMode(CheckMarked);
     fprintf(stderr, "Beginning of memory controller\n");
     objects = NSCreateHashTable(hashcb, 100);
}

BOOL exist(NSObject *p)
{
  int res;
  [monLock lock];
  
  res = (NSHashGet(objects, p) != NULL);
  
  [monLock unlock];
  return res;
}

void add(NSObject *p)
{
     if( state || exist(p) )
       {
	 //	 puts("Création");
	 return;
       }
     [monLock lock];
     NSHashInsertIfAbsent(objects, p);

     [monLock unlock];
}

void clearObject(NSObject *p)
{
  int count;
     if( state )
       {
	 //	 puts("collision de déstruction");
	 return;
       }
     [monLock lock];
     state = 1;

     count = NSCountHashTable(objects);
     NSHashRemove(objects, p);
//      if ( NSCountHashTable(objects) >= count )
//        printf("strange, nothing is deallocated for %p\n", p );
       {
	 if( mode & LogFree )
	   fprintf(stderr, "bidule libéré : %s\n", [[[p class] 
						      description]
						     UTF8String]);

       }
     state = 0;
     [monLock unlock];
}

void checkMetaObj(void)
{
  NSHashEnumerator en;
  NSObject *p;
     int count = 0;
     NSAutoreleasePool *pool;
     puts("Start controlling memory block");
     [monLock lock];
     state = 1;
     pool = [[NSAutoreleasePool alloc] init];
     
     en = NSEnumerateHashTable(objects);

     while ((p = NSNextHashEnumeratorItem(&en)) != NULL)
       {
         count ++;
         fprintf(stderr, "Error, object not deallocated\n");
         fprintf(stderr, " : %s.\n", [[p description] UTF8String]);
	      //	      NSLog(@"Objet non deruit : %@\n", [listobj[i] class]);
       }
     puts("End of control");
     if( count > 0 )
       {
	 fprintf(stderr, "%d objects were not deallocated\n", count);
       }
     state = 0;
     [pool release];
     [monLock unlock];
}

// int getIndice(NSObject *p)
// {
//   int i;
//   for( i = 0; i < MAXOBJ; ++i)
//     if( listobj[i] == p )
//       return i;
//   return -1;
// }


@implementation ObjectControl
+ (void) initialize
{
  static BOOL first = YES;
  if( self == [ObjectControl class] && first)
    {
      first = NO;
      [[NSNotificationCenter defaultCenter] 
	addObserver: self 
	selector: @selector(becomeMultiThread)
	name: NSWillBecomeMultiThreadedNotification
	object: nil];
    }
}

+ (void) becomeMultiThread
{
  NSLog(@"on est multithreade!!!");
  monLock = [NSRecursiveLock new];
}

+ alloc
{
  id obj;
  obj = [super alloc];
  if( mode & CheckAll )
    add(obj);
#ifdef HardAllocDebug
  fprintf(stderr, "%s a été alloué\n", [obj name]);
#endif
  return obj;
}

- markObject
{
  if( mode & CheckMarked )
    add(self);
  return self;
}

- copy
{
  ObjectControl *obj;
  obj = [super copy];
  if( mode & CheckAll )
    add(obj);
  return obj;
}

- (void)dealloc
{
#ifdef HardAllocDebug
  fprintf(stderr, "%s a été libéré\n", [self name]);
#endif
  clearObject(self);
  [super dealloc];
}
  
@end

