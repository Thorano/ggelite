/* 	-*-ObjC-*- */
/*
 *  Metaobj.h
 *
 *  Copyright (c) 1999 Free Software Foundation, Inc.
 *  
 *  Author: Frédéric De Jaeger
 *  Date: May 2000
 * 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __Metaobj_h
#define __Metaobj_h

typedef enum 
{
  CheckAll = 0x1,
  CheckMarked = 0x2,
  Dontcheck = 0x4,
  LogFree = 0x8
}memCheckMode;

@protocol Metaobj
- markObject;
@end

//returns the old mode
int setMode(int newmode);
//void freeObject( id obj );


void initMetaObj(void);
void checkMetaObj(void);

#ifdef DebugMemory
#define START 	initMetaObj()
#define END 	checkMetaObj()
#define addToCheck(p) [(id<Metaobj>)(p) markObject]
#else
#define START
#define END
#define addToCheck(p)
#endif

#endif
