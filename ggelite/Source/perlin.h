#ifndef __perlin_h
#define __perlin_h

typedef  double ggdouble;

double noise1(double);
double noise2(double *);
ggdouble noise3(ggdouble vec[3]);

double noise2PeriodiqueX(double vec[2], unsigned int periode);
double noise2PeriodiqueXY(double vec[2], unsigned int periode);


/*
   In what follows "alpha" is the weight when the sum is formed.
   Typically it is 2, As this approaches 1 the function is noisier.
   "beta" is the harmonic scaling/spacing, typically 2.
*/


double PerlinNoise1D(double x,double alpha,double beta,int n);
double PerlinNoise2D(double x,double y,double alpha,double beta,int n);
ggdouble PerlinNoise3D(ggdouble x,ggdouble y,ggdouble z,double alpha,double beta,int n);
double PerlinNoise2DWithGrad(double x,double y,double alpha,double beta,int n,
			     double grad[2]);
double PerlinNoise2DWithGradscale(double x,double y,double alpha,double beta,
				  int n, double grad[2], double scale);

ggdouble PerlinNoise3DWithGradscale(ggdouble x,ggdouble y, ggdouble z,
				    double alpha,double beta,
				    int n, double grad[3], double _scale);

double PerlinNoise2DPeriodiqueXWithGradscale(double x,double y,
					     double alpha,
					     int n, double xper,
					     double grad[2], double scale);
double PerlinNoise2DPeriodiqueX(double x,double y,double alpha, int n,
				double xper);
#endif
