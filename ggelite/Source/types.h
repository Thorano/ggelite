/*
 *  type.h
 *
 *  Copyright (c) 2001 by Frédéric De Jaeger
 *  
 *  Date: April 2001
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __type_h
#define __type_h

#import "config.h"

/*hack if using with C++*/
#ifdef __cplusplus
typedef bool BOOL;
#define YES 1
#define NO 0
#else
#ifndef __OBJC__
typedef unsigned char BOOL;
#define YES 1
#define NO 0
#endif
#endif



typedef struct __vect3D
{
  GGReal x, y, z;
}Vect3D;

typedef struct __vect4D
{
  Vect3D 	v;
  GGReal		w;
}Vect4D;

typedef Vect4D GGPlan;

typedef struct __Couleur4D
{
  GLfloat r, g, b, a;
}VectCol;

static inline void glVertex3v(Vect3D *x){
    glVertex3fv((GGReal*)(x));
}
//#define glVertex3v(x) glVertex3fv((GGReal*)(x))
#define glVertex2v(x) glVertex3fv((GGReal*)(x))
#define glNormal3v(x) glNormal3fv((GGReal*)(x))
#define glColor4v(x) glColor4fv((GGReal*)(x))
#define glTexCoord3v(x) glTexCoord3fv((GGReal*)(x))
#define glTexCoord2v(x) glTexCoord2fv((GGReal*)(x))

static inline void ggVertexAttrib3v(GLuint index, Vect3D v){
    glVertexAttrib3fv(index, (GLfloat*)&v);
}

static inline void ggLightPosition(GLenum light, Vect4D light_position){
    glLightfv(light, GL_POSITION, (GGReal*)&light_position);
}


GGBEGINDEC

/*%%%
 type NSRange;
 type NSRect;
 type NSComparisonResult;
 type Class = Id;
 type Protocol;
 type IMP;
 type NSZone;
 %%%*/


/*-=
@class NSNumber;
@class NSSet;
@class NSData;
@class NSEnumerator;
@class NSCoder;
@class NSInvocation;
@class NSMethodSignature;
@class NSArchiver;
@class NSPortCoder;
@class NSURL;
@class NSNotification;
@interface NSArray : NSObject <NSCoding, NSCopying, NSMutableCopying>
{}
+ (id) array;
+ (id) arrayWithArray: (NSArray*)array;
+ (id) arrayWithContentsOfFile: (NSString*)file;
+ (id) arrayWithObject: (id)anObject;
+ (id) arrayWithObjects: (id)firstObject, ...;
+ (id) arrayWithObjects: (id*)objects count: (unsigned)count;

- (NSArray*) arrayByAddingObject: (id)anObject;
- (NSArray*) arrayByAddingObjectsFromArray: (NSArray*)anotherArray;
- (BOOL) containsObject: anObject;
- (unsigned) count;						// Primitive
- (void) getObjects: (id*)aBuffer;
- (void) getObjects: (id*)aBuffer range: (NSRange)aRange;
- (unsigned) indexOfObject: (id)anObject;
- (unsigned) indexOfObject: (id)anObject inRange: (NSRange)aRange;
- (unsigned) indexOfObjectIdenticalTo: (id)anObject;
- (unsigned) indexOfObjectIdenticalTo: (id)anObject inRange: (NSRange)aRange;
- (id) initWithArray: (NSArray*)array;
- (id) initWithContentsOfFile: (NSString*)file;
- (id) initWithObjects: firstObject, ...;
- (id) initWithObjects: (id*)objects count: (unsigned)count;	// Primitive

- (id) lastObject;
- (id) objectAtIndex: (unsigned)index;				// Primitive

- (id) firstObjectCommonWithArray: (NSArray*)otherArray;
- (BOOL) isEqualToArray: (NSArray*)otherArray;

- (NSData*) sortedArrayHint;
- (NSArray*) sortedArrayUsingFunction: (int (*)(id, id, void*))comparator 
			      context: (void*)context;
- (NSArray*) sortedArrayUsingFunction: (int (*)(id, id, void*))comparator 
			      context: (void*)context
				 hint: (NSData*)hint;
- (NSArray*) sortedArrayUsingSelector: (SEL)comparator;
- (NSArray*) subarrayWithRange: (NSRange)aRange;

- (NSString*) componentsJoinedByString: (NSString*)separator;
- (NSArray*) pathsMatchingExtensions: (NSArray*)extensions;

- (NSEnumerator*) objectEnumerator;
- (NSEnumerator*) reverseObjectEnumerator;
@end

@interface NSMutableArray : NSArray

+ (id) arrayWithCapacity: (unsigned)numItems;

- (void) addObject: (id)anObject;				// Primitive
- (void) addObjectsFromArray: (NSArray*)otherArray;
- (void) exchangeObjectAtIndex: (unsigned)i1
	     withObjectAtIndex: (unsigned)i2;
- (id) initWithCapacity: (unsigned)numItems;			// Primitive
- (void) insertObject: (id)anObject atIndex: (unsigned)index;	// Primitive
- (void) removeObjectAtIndex: (unsigned)index;			// Primitive
- (void) replaceObjectAtIndex: (unsigned)index
		   withObject: (id)anObject;			// Primitive
- (void) replaceObjectsInRange: (NSRange)aRange
	  withObjectsFromArray: (NSArray*)anArray;
- (void) replaceObjectsInRange: (NSRange)aRange
	  withObjectsFromArray: (NSArray*)anArray
			 range: (NSRange)anotherRange;
- (void) setArray: (NSArray *)otherArray;

- (void) removeAllObjects;
- (void) removeLastObject;
- (void) removeObject: (id)anObject;
- (void) removeObject: (id)anObject inRange: (NSRange)aRange;
- (void) removeObjectIdenticalTo: (id)anObject;
- (void) removeObjectIdenticalTo: (id)anObject inRange: (NSRange)aRange;
- (void) removeObjectsInArray: (NSArray*)otherArray;
- (void) removeObjectsInRange: (NSRange)aRange;
- (void) removeObjectsFromIndices: (unsigned*)indices 
		       numIndices: (unsigned)count;

- (void) sortUsingFunction: (NSComparisonResult (*)(id,id,void*))compare 
		   context: (void*)context;
- (void) sortUsingSelector: (SEL)comparator;

@end

@interface NSString : NSObject <NSCoding, NSCopying, NSMutableCopying>
{}

- (unsigned) length;
- (BOOL) isEqualToString: (NSString*)otherString;
- (int)intValue;

-  (NSString *)uppercaseString;
-  (NSString *)lowercaseString;
	 
@end

@interface NSObject <NSObject>
{
}
+ (id) allocWithZone: (NSZone*)z;
+ (id) alloc;
+ (Class) class;
+ (NSString*) description;
+ (void) initialize;
+ (IMP) instanceMethodForSelector: (SEL)aSelector;
+ (NSMethodSignature*) instanceMethodSignatureForSelector: (SEL)aSelector;
+ (BOOL) instancesRespondToSelector: (SEL)aSelector;
+ (id) new;
+ (void) poseAsClass: (Class)aClassObject;
+ (id) setVersion: (int)aVersion;
+ (Class) superclass;
+ (int) version;

- (id) autorelease;
- (id) awakeAfterUsingCoder: (NSCoder*)aDecoder;
- (Class) class;
- (Class) classForArchiver;
- (Class) classForCoder;
- (Class) classForPortCoder;
- (BOOL) conformsToProtocol: (Protocol*)aProtocol;
- (id) copy;
- (void) dealloc;
- (NSString*) description;
- (void) doesNotRecognizeSelector: (SEL)aSelector;
- (void) forwardInvocation: (NSInvocation*)anInvocation;
- (unsigned) hash;
- (id) init;
- (BOOL) isEqual: anObject;
- (BOOL) isKindOfClass: (Class)aClass;
- (BOOL) isMemberOfClass: (Class)aClass;
- (BOOL) isProxy;
- (IMP) methodForSelector: (SEL)aSelector;
- (NSMethodSignature*) methodSignatureForSelector: (SEL)aSelector;
- (id) mutableCopy;
- (id) performSelector: (SEL)aSelector;
- (id) performSelector: (SEL)aSelector
	    withObject: (id)anObject;
- (id) performSelector: (SEL)aSelector
	    withObject: (id)object1
	    withObject: (id)object2;
- (void) release;
- (id) replacementObjectForArchiver: (NSArchiver*)anArchiver;
- (id) replacementObjectForCoder: (NSCoder*)anEncoder;
- (id) replacementObjectForPortCoder: (NSPortCoder*)aCoder;
- (BOOL) respondsToSelector: (SEL)aSelector;
- (id) retain;
- (unsigned) retainCount;
- (id) self;
- (Class) superclass;
- (NSZone*) zone;
@end

@interface NSEnumerator : NSObject
- (id)nextObject;
- (NSArray *)allObjects;
@end


@interface NSDictionary : NSObject
{}
- (unsigned)count;
- (NSEnumerator *)keyEnumerator;
- (id)objectForKey:(id)aKey;

- (NSArray *)allKeys;
- (NSArray *)allKeysForObject:(id)anObject;    
- (NSArray *)allValues;
- (NSString *)description;
- (NSString *)descriptionInStringsFileFormat;
- (NSString *)descriptionWithLocale:(NSDictionary *)locale;
- (NSString *)descriptionWithLocale:(NSDictionary *)locale indent:(unsigned)level;
- (BOOL)isEqualToDictionary:(NSDictionary *)otherDictionary;
- (NSEnumerator *)objectEnumerator;
- (NSArray *)objectsForKeys:(NSArray *)keys notFoundMarker:(id)marker;
- (BOOL)writeToFile:(NSString *)path atomically:(BOOL)useAuxiliaryFile;
- (BOOL)writeToURL:(NSURL *)url atomically:(BOOL)atomically; // the atomically flag is ignored if url of a type that cannot be written atomically.

- (NSArray *)keysSortedByValueUsingSelector:(SEL)comparator;


+ (id)dictionary;
+ (id)dictionaryWithContentsOfFile:(NSString *)path;
+ (id)dictionaryWithContentsOfURL:(NSURL *)url;
+ (id)dictionaryWithObjects:(NSArray *)objects forKeys:(NSArray *)keys;
+ (id)dictionaryWithObjects:(id *)objects forKeys:(id *)keys count:(unsigned)count;
+ (id)dictionaryWithObjectsAndKeys:(id)firstObject, ...;
- (id)initWithContentsOfFile:(NSString *)path;
- (id)initWithContentsOfURL:(NSURL *)url;
- (id)initWithObjects:(NSArray *)objects forKeys:(NSArray *)keys;
- (id)initWithObjects:(id *)objects forKeys:(id *)keys count:(unsigned)count;
- (id)initWithObjectsAndKeys:(id)firstObject, ...;
- (id)initWithDictionary:(NSDictionary *)otherDictionary;

+ (id)dictionaryWithDictionary:(NSDictionary *)dict;
+ (id)dictionaryWithObject:(id)object forKey:(id)key;
- (id)initWithDictionary:(NSDictionary *)otherDictionary copyItems:(BOOL)aBool;

@end

@interface NSMutableDictionary : NSDictionary

- (void)removeObjectForKey:(id)aKey;
- (void)setObject:(id)anObject forKey:(id)aKey;

- (void)addEntriesFromDictionary:(NSDictionary *)otherDictionary;
- (void)removeAllObjects;
- (void)removeObjectsForKeys:(NSArray *)keyArray;
- (void)setDictionary:(NSDictionary *)otherDictionary;

+ (id)dictionaryWithCapacity:(unsigned)numItems;
@end

@interface NSNotificationCenter : NSObject {
}

+ (id)defaultCenter;

- (void)addObserver:(id)observer selector:(SEL)aSelector name:(NSString *)aName object:(id)anObject;

- (void)postNotification:(NSNotification *)notification;
- (void)postNotificationName:(NSString *)aName object:(id)anObject;
- (void)postNotificationName:(NSString *)aName object:(id)anObject userInfo:(NSDictionary *)aUserInfo;

- (void)removeObserver:(id)observer;
- (void)removeObserver:(id)observer name:(NSString *)aName object:(id)anObject;

@end


@interface NSNotification : NSObject <NSCopying, NSCoding>

- (NSString *)name;
- (id)object;
- (NSDictionary *)userInfo;

+ (id)notificationWithName:(NSString *)aName object:(id)anObject;
+ (id)notificationWithName:(NSString *)aName object:(id)anObject userInfo:(NSDictionary *)aUserInfo;

@end



typedef float GGReal;
typedef float GLfloat;
typedef unsigned int GLuint;
void RETAIN(id obj);
=-*/

typedef struct __deplacement
{
  Vect3D	gauche;
  GGReal		nul2;
  Vect3D	haut;
  GGReal		nul3;
  Vect3D	direction;
  GGReal	 	nul1;
  Vect3D	position;
  GGReal		unite;
}GGMatrix4;



typedef struct __GGGalileen
{
  GGMatrix4 	matrice;	//position
  Vect3D	speed;	//try to guess !
}GGGalileen;


typedef struct __PetitGalileen
{
  Vect3D	point, speed;
}PetitGalileen;

typedef struct __projection_param
{
    GLfloat 		xFoc, yFoc, ratio, zmin, zmax;
    GLfloat     fovy;
    int			x, y, width, height;
    BOOL			label;
	BOOL			shadowRaster;
    BOOL            displayBackFace;
    BOOL            ignoreClip;
}ProjectionParam;

#ifdef DEBUG
@class GGSpaceObject;
#endif

typedef struct __camera
{
    ProjectionParam	*param;
    GGMatrix4		modelView;
    struct __camera	*prev;
#ifdef DEBUG
    GGSpaceObject		*owner;
#endif
}Camera;

GGENDDEC

static inline void glLoadMatrix(GGMatrix4* ptr){
    glLoadMatrixf((GGReal*)ptr);
}

static inline void glMultMatrix(GGMatrix4* ptr){
    glMultMatrixf((GGReal*)ptr);
}





typedef struct __interLeavedT2V3
{
  GLfloat s, t, x, y, z;
}InterLeavedT2V3;



typedef   struct __box
{
  short	top, bottom;
}box;

typedef   struct __GGBox
{
    Vect3D min;
    Vect3D max;
}GGBox;

typedef struct __frustum
{
  GGPlan	devant, droite, gauche, haut, bas;
  unsigned	idev, idro, igau, ihau, ibas;
}Frustum;

#endif
