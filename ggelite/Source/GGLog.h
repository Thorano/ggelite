/*
 *  GGLog.h
 *  elite
 *
 *  Created by Frederic De Jaeger on 9/26/04.
 *  Copyright 2004 __MyCompanyName__. All rights reserved.
 *
 */

#import <Foundation/Foundation.h>

@class StringArray;
@interface GGLog : NSObject
{
    StringArray			*_logArray;
}
+ (void) logFunction:(const char *)func 
     fileName:(const char *)fileName 
     lineNo:(unsigned)lineNo
     format:(NSString*)format, ...;
+ (void) setLogger:(GGLog*)logger;


- (void)logFunction:(const char *)func 
     fileName:(const char *)fileName 
     lineNo:(unsigned)lineNo
     format:(NSString*)format
     arguments:(va_list)list;
- (void) logAndCleanAtPoint:(NSPoint)point;
@end

#if defined(DEBUG) && defined(GGElite)
#define NSDebugMLLogWindow(level, fmt, args...) do { if(GSDebugSet(level) == YES) {\
    [GGLog logFunction:__func__ fileName:__FILE__ lineNo:__LINE__ format:fmt , ## args];\
    }}while(0)

#define NSDebugLLogWindow(level, fmt, args...) do { if(GSDebugSet(level) == YES) {\
    [GGLog logFunction:__func__ fileName:__FILE__ lineNo:__LINE__ format:fmt , ## args];\
    }}while(0)
#else
#define NSDebugMLLogWindow(level, format, args...)
#define NSDebugLLogWindow(level, format, args...)
#endif

