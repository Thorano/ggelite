open Utile
open Gencode
open Ggtree

let rec my_iter f c = function
    a :: ((b :: l) as x) -> f a; c () ; my_iter f c x
  | a :: [] -> f a
  | [] -> ()

let formatter_m = ref Format.std_formatter
let formatter_h = ref Format.std_formatter

let f_m x  = Format.fprintf !formatter_m x
and f_h x  = Format.fprintf !formatter_h x

module OExp = 
  struct
    open Variable
    open GGExp
      
    let rec ggconcat f sep = function
	a::((b::_) as l) ->
	  f a;
	  sep ();
	  ggconcat f sep l
      |	a :: [] ->
	  f a
      |	[] -> ()
	    

    let rec is_in_frame var_hash e = 
      (* This function test if an expression (which is supposed to
	 be a lvalue) is in the saved frame, or not.  This function is
	 mandatory to statically if one should add a value to the gc*) 

      let sub e = is_in_frame var_hash e in
      match e.descr with 
      | Int _
      | Nil 
      | String _
      | Bool _
      | Float _
      | Symbol _ -> false
      | Var { localisation = MediumLoc _ } -> true
      | Var _ -> false
(*	  let id = v.Symbol.tag in
	  let l = (Hashtbl.find_all  var_hash id) in
	  if (List.length l > 1) then
	    begin
	      let rec iter = function
		  {varname = n} :: l ->
		    Printf.eprintf "varname = %s\n" n;
		    iter l
		| [] -> () in
	      iter l
	    end;
	  begin
	    try 
	      match Hashtbl.find var_hash id with
		{varloc = MediumLoc _} ->  true
	      | _ -> false
	    with
	      _ -> 
		Printf.eprintf "val = %s\n" v.Symbol.name;
		assert false
	  end
*)
      | No_Exp
      |	ArrayBuild _
      | StructBuild _
      | Address _  
      | MessageCall _ 
      | Binop  _
      | Monop _
      | Function _ -> assert false
      | ArrayAccess (tab, ind) -> sub tab
      | StructAccess (s, ind) -> sub s

    let var_name = function
      |	{ name = s;
	  localisation = MediumLoc _ } -> "(self->" ^ s ^ ")"
      | { name = s } -> s

    let var_name_from_hash var_hash id =
      try
	var_name (Hashtbl.find var_hash id)
      with
	_ -> 
	  Printf.eprintf "id = %d\n" (Stamp.hash id);
	  assert false
      

(*    let var_name var_hash (id : Stamp.t)  =
(*      let l = (Hashtbl.find_all  var_hash id) in
      if (List.length l  <= 1);*)
      try
	match Hashtbl.find var_hash id with
	  {varname = s;varloc = MediumLoc _} ->  
	|	{varname = s; varloc = ComplexExternalFunc} 
	|	{varname = s; varloc = 
		 Variable.Function({ComplexProc.cont_list = _ :: _})} ->
		   "Complex_" ^ s
	| {varname = s} -> s
      with
	_ -> 
	  Printf.eprintf "id = %d\n" (Stamp.hash id);
	  assert false
*)

    let to_string_formatter formatter var_hash = 
      let f_m x = Format.fprintf formatter x in
      let rec to_string  =  function 
	| No_Exp -> f_m "/*gros BUG*/"
	| ArrayBuild _
	| StructBuild _ -> assert false
	| Int(x) -> f_m "%d" x
	| Nil -> f_m "nil"
	| String s -> f_m "@@\"%s\"" s 
	| Bool(x) -> f_m "%s" (if x then "YES" else "NO")
	| Float(x) -> f_m "%f" x
	| Symbol s -> f_m "%s" s
	| Var v -> f_m "%s" (var_name  v)
	| Function(s, l) -> 
	    to_string  s.GGExp.descr;
	    f_m "(@[";
	    ggconcat (fun x -> to_string x.descr) (fun _ -> f_m ",@ ") l;
	    f_m "@])"
	| Binop(op, e1, e2) -> 
	    f_m "(@["; 
	    to_string  e1.descr;
	    f_m "@ %s@ "  (string_of_op op);
	    to_string e2.descr;
	    f_m "@])"
	| Monop(op, e) -> 
	    f_m "(@[";
	    f_m "%s@ " (string_of_op op);
	    to_string e.descr;
	    f_m "@])"
	| ArrayAccess (tab, ind) -> 
	    f_m "(@[";
	    to_string (tab.descr);
	    f_m "[";
	    to_string (ind.GGExp.descr);
	    f_m "]@])"
	| StructAccess (s, ind) when is_object s -> 
	    to_string  (s.descr);
	    f_m "->%s"ind
	| StructAccess (s, ind) -> 
	    to_string  (s.descr);
	    f_m ".%s" ind
	| MessageCall(e,name,None) -> 
	    f_m "[@[" ;
	    to_string e.descr;
	    f_m "@ %s@]]" name
	| MessageCall(e,name,Some(e',l)) ->
	    f_m "[";
	    to_string  e.descr;
	    f_m  " @[%s:@ " name;
	    to_string  e'.descr;
	    f_m "@\n";
	    ggconcat  
	      (fun (n,e)  -> 
		f_m "%s:@ " n;
		to_string e.descr
	      ) (fun _ -> f_m "@\n") l;
	    f_m "@]]"
	| Address e -> 
	    f_m "&(@[";
	    to_string e.descr;
	    f_m "@])"
      in to_string

    let to_string var_hash e = 
      Format.pp_open_hovbox !formatter_m 2;
      to_string_formatter !formatter_m var_hash e;
      Format.pp_close_box !formatter_m ()

    module TrackObjects = struct
      open GGType

	(*we dont track ListeningBlock*)

      let rec apply f (acces: GGExp.t) (letype: GGType.t) = 
	let iter acces t = apply f acces t in
	match letype with
	| Objet { contents = Faux "ListeningBlock" }
	| Objet { contents = Vrai { name = "ListeningBlock" } }
	| Unit
	| Bool
	| Integer 
	| Float
	| Enum _ -> ()
	| Objet _
	| Id  
	| String ->
	    f acces;
	| Array (_,t) | Pointer t-> 
	    if GGType.contains_object t then
	      Utile.warn_with_loc acces.GGExp.loc 
		"This assignement is dangerous for the gc";
	| Struct l ->
	    List.iter (fun (n,t) ->
	      let acces:GGExp.t = GGExp.mk ~loc: acces.GGExp.loc 
		  (StructAccess(acces, n))  t in
	      iter acces t) l
	| Function _ -> 
	    Utile.debug "not functional yet";
	    assert false
        | Abstract _ -> failwith "unexpected abstract type"
	| Name (_,t) -> iter acces t
	      
    end
	

    let gc_track ?(release=false) var_hash  (e:t) =
      let f e = 
	f_m "gg_add_gc(";
	to_string var_hash e.descr;
	f_m ", NO);@\n"
      in

      assert (is_no_compute e);
      if GGType.is_object_trackable e.thetype then
	begin
	  f_m "gg_add_gc(";
	  to_string var_hash e.descr;
	  f_m ", NO);@\n"
	end
      else
	begin
	  TrackObjects.apply f e e.thetype;
	  let f e =
	    f_m "RELEASE(";
	    to_string var_hash e.descr;
	    f_m ");@\n" in
	  if release then
	    TrackObjects.apply f e e.thetype
	end
  end

module OCom = 
  struct
    open CommandC
    open Label
    open Continuation


    let to_buf lab_hash var_hash   complex  l = 
      let out_exp e = OExp.to_string var_hash  (e.GGExp.descr) in
      let name_of_cont id = 
	match Hashtbl.find lab_hash id with
	  {labelCont = Some {cont_name = x}} -> (String.concat "" x)
	| _ -> failwith "grosse merde" (* warning here !!!*)
      and name_of_lab id = 
	match Hashtbl.find lab_hash id with
	  {labelName = x} -> x  

      and do_clean l = 
	  List.iter 
	    begin
	      fun id -> 
		match Hashtbl.find var_hash id with
		  {Variable.name = x;
		   Variable.localisation = Variable.MediumLoc _}   -> 
		     f_m "self->%s = nil;@\n" x
		| _ -> ()
	    end l
      in

      let rec to_string  = function
      	  If(e, b, c) ->
	    f_m "if( ";
	    out_exp e;
	    f_m " )@ {  @[<v 0>@ ";
	    to_string   b;
	    (match c with 
	      Nop -> f_m "@]@ }@ "
	    | _ ->  
		f_m "@]@ }@ ";
		f_m "else@ {  @[<v 0>@\n";
		to_string   c;
		f_m "@]@ }"
	    )
	| Expr(e) -> 
	    out_exp e;
	    f_m ";";
	| CheckObject(e,t) ->
	    let s = match !t with
	      GGType.Vrai {GGType.name = x} 
	    | GGType.Faux x -> x
	    in
	    f_m "{@\n@[id blo;@\n";
	    f_m "blo = ";
	    OExp.to_string var_hash  e.GGExp.descr;
	    f_m ";@\n";
	    f_m "NSCAssert(blo == nil@ ||@ [blo isKindOfClass:@ [%s" s;
	    f_m " class]],@ @@\"glouglou\");@]@\n}"
	| Assign( e1, e2) -> 
	    let sub () = 
	      OExp.to_string var_hash  (e1.GGExp.descr);
	      f_m " = ";
	      OExp.to_string var_hash  (e2.GGExp.descr);
	      f_m ";"
	    in
	    if OExp.is_in_frame var_hash e1 then
	      begin
		f_m "{@\n    @[";
		sub ();
		f_m "@\n";
		OExp.gc_track var_hash e1;
		f_m "@]@\n}"
	      end
	    else
	      sub ()
	| Label id ->
	    begin
	      match Hashtbl.find lab_hash id with
		{
		 labelName = name;
		 label_is_top = is_top;
		 used = used
	       } ->
		 let res = (not used) || is_top in
		 if res then f_m "/*   ";
		 f_m "%s:" (name_of_lab id);
		 if res then f_m "   */";
	    end
	| Jump id ->
	    begin
	      match Hashtbl.find lab_hash id with
		{ labelName = labelName;
		  label_is_top = true; 
		  labelCont = Some{cont_name=name}} ->
		    f_m "{@[@\n%s(self);   /* label %s */@\n" 
		      (String.concat "" name) labelName;
		    f_m "return;@]@\n" ; 
		    f_m "}"
	      |	
		{Label.labelName = name}  -> 
		  f_m "goto %s;" name

	    end
	| Partial_call(fn,l,id) ->
	    let name = name_of_cont id
	    in
	    f_m "{@[@\n" ;
	    f_m "GGHandle = [" ;
	    let comp_name () = (OExp.to_string var_hash fn.GGExp.descr) in
	    comp_name ();
	    f_m "@ allocInstance];@\n" ;
	    f_m "[GGHandle registerContinuation:@ self@[@\n" ;
	    f_m "at:@ @@selector(%s" (name) ;
	    f_m ")@]];@\n" ;
	    f_m "[GGHandle _setProcess: self->_process];@\n";
	    f_m "[(";
	    comp_name ();
	    f_m " *)GGHandle@[ runWith" ;
	    my_iter 
	      (
	       let count = ref 1 in 
	       fun x  -> 
		 f_m "arg%d:@[@ " !count;
		 OExp.to_string var_hash  x.GGExp.descr;
		 f_m "@]";
		 incr count)  (fun () -> f_m "@\n") l;
	    f_m "@]];@\n" ;
	    f_m "return;@]@\n}" ;
	| RegisterCont (id,level) ->
	    begin
	      match level with
		Delay e ->
		  let name = name_of_cont id in
		  f_m "[@[self sleepDuring:@ @[ ";
		  OExp.to_string var_hash e.GGExp.descr;
		  f_m "@]@\nwakeUpAt:@@selector(%s)@]];" (name)
	      | Stand_Wait level ->
		  let name = name_of_cont id in
		  f_m "gg_register_continuation(self, @@selector(%s),@ " name;
		  f_m "%s);" level 
(*	      |	Exit -> ()*)
	    end;
	      
	| Return (Some x) ->  
	    let s () = OExp.to_string var_hash  x.GGExp.descr in
	    if complex then
	      begin
		f_m "{  @[@\n" ;
(*		f_m "NSInvocation *inv = GGCallBack;@\n";
		f_m "RETAIN(self);@\n";
		f_m "[self killAllBlock];@\n";
		f_m "RETAIN(inv);@\n";
		f_m "[@[inv setArgument:@ &@[";
		s ();
		f_m "@]@\natIndex: 3@]];@\n";
		f_m "[GGCallBack invoke];@\n";
		f_m "RELEASE(inv);@\n";
		f_m "[self invalidate];@\n";
		f_m "RELEASE(self);@\n";
*)
		f_m "[self returnToContinuationWithArg: &@[";
		s ();
		f_m "@]];@\n";
		f_m "return;@]@\n}"; 
	      end
	    else
	      begin
		f_m "return ";
		s ();
		f_m ";"
	      end
	| Return None -> 
	    if complex then
	      begin
		f_m "{  @[@\n" ;
(*		f_m "NSInvocation *inv = GGCallBack;@\n";
		f_m "RETAIN(self);@\n";
		f_m "[self killAllBlock];@\n";
		f_m "RETAIN(inv);@\n";
		f_m "[GGCallBack invoke];@\n";
		f_m "RELEASE(inv);@\n";
		f_m "[self invalidate];@\n";
		f_m "RELEASE(self);@\n";
*)
		f_m "[self returnToContinuation];@\n";
		f_m "return;@]@\n}" ;
	      end
	    else
	      f_m "return;@\n"
	| SimpleReturn (Some x) ->
	    f_m "return ";
	    OExp.to_string var_hash x.GGExp.descr;
	    f_m ";"
	| SimpleReturn None ->
	    f_m "return;"
	| Set_timer (id, e) ->
	    begin
	      let cn = OExp.var_name_from_hash var_hash id in
	      let exp_s () = OExp.to_string var_hash  e.GGExp.descr
	      in
	      f_m "{@[@\n" ;
	      f_m "if( %s == nil )@   @[@\n" cn;
	      f_m "%s = [self @[scheduledTimerForDate:@[@  " cn;
	      exp_s ();
	      f_m "@]@\n";
	      f_m "selector: @@selector(%s" (name_of_cont id);
	      f_m ")@]];@]@\n" ;
	      f_m "else@[@\n";
	      f_m "[%s rescheduleForDate:@ " cn;
	      exp_s ();
	      f_m "];@]@]@\n}"
	    end
	| Reschedule_timer (id, e) ->
	    begin
	      let cn = OExp.var_name_from_hash var_hash id in
	      f_m "[%s  rescheduleForDate:@[@ " cn;
	      OExp.to_string var_hash  e.GGExp.descr;
	      f_m "@]];";
	    end

	| SetNotVar (v, name) ->
	    f_m "{@[@\n";
	    f_m "id val = [[aNotification userInfo] objectForKey:@@\"%s\"];@\n" 
	      name;
	    f_m "NSAssert(val != nil,  @@\"je sais pas trop\");@\n";
	    let name = OExp.var_name v in
	    begin
	      match v with
		{Variable.thetype = t; 
		 Variable.localisation = loc;} ->
		   if GGType.is_object t  then
		     f_m "%s = val;" name
		   else
		     begin
		       f_m "[val getValue:@ &%s];" name;
		     end;
		   begin
		     match loc with
		     | Variable.MediumLoc _ ->
			 let e = Symbol.to_exp v in
			 OExp.gc_track var_hash e
		     | _ -> ()
		   end
	    end;
	    f_m "@]@\n}"

	| Destroy_Callee ->
(*	    f_m "{  @[@\n[GGHandle invalidate];@\n";
	    f_m "DESTROY(GGHandle);@]@\n}"
*)
	    f_m "DESTROY(GGHandle);@\n"
	| Clean l ->
	    f_m "{ @[/* clean */@\n";
	    do_clean  l;
	    f_m "@]@\n}"
	    
	| Destroy_Job (id,l) -> 
	    f_m "{  @[@\n[GGHandle invalidate];@\n";
	    f_m "DESTROY(GGHandle);@\n";
	    f_m "gg_unregister_continuation(self);@\n";
	    let name = OExp.var_name_from_hash var_hash id in
	    f_m "NSDebugMLLog(@@\"GGRunTime\", @@\"self = %%@@\", self);@\n";
	    f_m "NSAssert(%s != nil, @@\"je sais pas trop\");@\n" name;
	    f_m "[self removeListeningBlock:@ %s];@\n" name;
(*	    f name;
	    f " = nil;";
	    f (retWith lvl);*)
	    do_clean  l;
	    f_m "@]@\n}";
	| RegisterMessage (lst,id,_) ->
	    begin
	      let n = OExp.var_name_from_hash var_hash id in
	      f_m "{@[@\n" ;
	      f_m "%s  = [ListeningBlock newWithCallback: self];@\n" n;
	      List.iter (
	      function 
		  {message_event = message;
		   action_label = id;
		   source_event = s_e;
		 } ->
		     let name = name_of_cont id in
		     f_m "[MessageManager @[addObserver:@ %s@\n" n;
		     f_m "selector: @@selector(%s)@\n" name;
		     f_m "name:@ @@\"%s\"@\n" message;
		     f_m "object:@ ";
		     let () = match  s_e with 
		       Some x ->  OExp.to_string var_hash  x.GGExp.descr
		     | None -> f_m "nil"
		     in 
		     f_m "@]];@\n";
	      ) lst;
	      f_m "@\n[self addListeningBlock: %s];@\n" n;
	      f_m "RELEASE(%s);@]@\n}" n;
	    end
	    
	| UnregisterMessage lst -> 
	    f_m "{@[@\n[self popListeningBlock];@\n";
	    f_m "NSDebugLLog(@@\"GGRunTime\", @@\"self = %%@@\", self);@\n";
	    do_clean lst;
	    f_m "@]@\n}";

	| Let v -> f_m " /* Let %s  */" v.Symbol.name
	| Nop  -> f_m " /*  NOOOOOP */; "
	| Abort -> f_m "[NSException @[<v 0>raise: @@\"GGRunTime\"@ format:@@\"abnormal condition\"@]];"
	| (Seq (c1, c2)) as e -> 
	    let rec to_list = function
		Seq(c1, c2) ->
		  to_list c1 @ to_list c2
	      | x -> [x]
	    in
	    to_string_list (to_list e)

      and to_string_list   = function 
	  (a::l) as x  ->
	    f_m "{/*block*/@   @[<v 0>";
	    my_iter to_string (fun () -> f_m "@ ") x;
	    f_m "@]@ }"
	|	_ -> f_m "{} /* GROS BUG */ " (*f_m "gros caca"*)
      in to_string  l
	      
  end

module OVar = 
  struct
    open Variable
    let to_string lvl = function
	{name = name;
	 thetype = t} -> 
	   (insert_space lvl) ^
	   (GGType.to_C_type lvl name t) ^ ";"

    open Symbol 
    let to_string_simple lvl  = function
	{name = name;
	 thetype = t} -> 
	   (insert_space lvl) ^
	   (GGType.to_C_type lvl name t) ^ ";"

	   
  end

module OCont =
  struct 
    open Continuation
    open Symbol
      
    let print_dec t  =
      begin
	Hashtbl.iter 
	  (fun key -> function 
	      {name = name;
	       thetype = t} -> 
		 f_m "@\n%s;" (GGType.to_C_type 1 name t)
	  )
	  t;
	f_m "@\n";
      end


    let to_string_cont_dec formatter name = 
     function
	 { 
	   cont_name = n;
	   arg = arg;
	   message = message;
	   rettype = rettype;
	 } -> 
	   let f_m s = Format.fprintf formatter s in
	   if message then
	     let rec iter name arg = match name,arg with
	     | name :: l, {Symbol.name =s;Symbol.thetype=t} :: m ->
		 f_m "%s (%s) %s" name (GGType.to_C_type 1 "" t) s;
		 if l <> [] then
		   f_m "@\n";
		 iter l m
	     | name :: [], [] ->
		 f_m "%s" name
	     | [], [] -> ()
	     | _ -> compiler_bug "internal here"
	     in
	     begin
	       let t = match rettype with
		 Some t -> GGType.to_C_type 0 "" t
	       | None -> "void"
	       in
	       f_m  "- (%s) @[" t;
	       iter n arg;
	       f_m "@]"
	     end
	   else
	     begin
	       let n = List.hd n in
	       f_m  "static void %s (Complex_%s * self@[" n  name;
	       let rec iter = function
		 |{Symbol.name=name; Symbol.thetype=t;} :: l ->
		     f_m ",@ %s"  (GGType.to_C_type 1 name t);
		     iter l
		 | [] -> ()
	       in
	       iter arg;
	       f_m "@])";
	     end


   let to_string lab_table var_table  name =   
     function
	 { comlist = comlist;
	   cont_name = n;
	   arg = arg;
	   local_vars = lv;
	   message = message;
	 } as cont -> 
(*	   Printf.eprintf "print_cont : '%s'\n" (String.concat "~" n);*)
	   to_string_cont_dec !formatter_m name  cont;
	   f_m "@\n{  @[@\n";
	   print_dec  lv;
	   OCom.to_buf lab_table var_table true   comlist;
	   f_m "@]@\n}@\n"

    let to_string_top_cont_dec formatter = function 
	GGType.Function(_,l) ->   
	  let f x = Format.fprintf formatter x in
	  f "- (void) runWith@[";
	  my_iter (let count = ref 1  in   function 
	      (name,t)  -> 
		f "arg%d:@ (%s)@ %s"  !count (GGType.to_C_type 5 "" t) name  ;
		incr count ) (fun () -> f "@\n") l;
	  f "@]"
      |	_ -> failwith "Not of function type"


    let to_string_main_cont lab_table var_table t = function 
	{comlist = comlist;
	 local_vars = lv
       } ->
	 
	 to_string_top_cont_dec !formatter_m t;
	 f_m "@\n";
	 f_m "{  @[@\n";
	 print_dec  lv;
	 OCom.to_buf lab_table var_table true comlist;
	 f_m "@]@\n}@\n@\n"
  end

module OProc =
  struct
    open ComplexProc
    open Continuation

    type fun_data =
	{
	 data : ComplexProc.t;
	 name : string;
	 thetype : GGType.t
       } 

    let to_string  var_table  = 
      function
	  {data = 
	   {
	    cont_list = [];
	    main_cont = 
	    { 
	      comlist = l;
	      local_vars = lv
	    };
	    lab_hash = lab_hash;
	  };
	   thetype = t;
	   name = name; } ->
	     f_m "@\n/*simple function %s  */@\n%s" name 
	       (GGType.to_C_type 0 name t);
	     f_h "@\nGGBEGINDEC@\n";
	     f_h "@\n/*simple function %s  */@\n%s" name 
	       (GGType.to_C_type 0 name t);
	     f_h ";@\n@\n";
	     f_h "@\nGGENDDEC@\n";
	     f_m "@\n{@[";
	     OCont.print_dec lv;
	     (OCom.to_buf lab_hash var_table false l);
	     f_m "@]@\n}@\n"
	| {data =  
	   { 
	     global_vars = gv;
	     main_cont = cont;
	     cont_list = cont_list;
	     lab_hash = lab_hash;
	   };  	 
	   thetype = GGType.Function(_,l) as t;
	   name = name;   } ->

(*	     Continuation.dump cont_list; *)

	     f_h "\nGGBEGINDEC\n";
	     f_h "%s" "/*%%%\n";
	     f_h "extern complex %s%s" name (GGType.to_string 0 t);
	     f_h "%s" ";\n%%%*/";
	     f_h "\nGGENDDEC\n";

	     f_h "\n/*Complex function from %s " name ;
	     f_h " compiled as a class\n */\n" ;
	     f_h "@@interface Complex_%s"  name;
	     f_h " : ExtendedProcedure\n{  @[@\n";
	     Hashtbl.iter 
	       (fun blu x -> 
		 f_h "%s@\n" (OVar.to_string_simple 5 x);
	       ) gv ;
	     f_h "\n}\n" ;
	     OCont.to_string_top_cont_dec !formatter_h t;
	     f_h ";\n";
	     f_m "\n@@implementation Complex_%s@\n"  name ;
	     List.iter (function {Continuation.message = false} as x -> 
	       OCont.to_string_cont_dec !formatter_m name x;
	       f_m ";@\n"
	       | _ -> ()) cont_list;
	     f_m "@\n@\n";
	     OCont.to_string_main_cont lab_hash  var_table  t cont;
	     f_m "@\n@\n";
	     List.iter	(fun x -> OCont.to_string lab_hash var_table
		 name x;
	       f_m "@\n")    cont_list;
	     
	     (*we build encoders and decoders*)

	     f_m "- (void)encodeWithCoder:(NSCoder *)encoder@\n{  @[@\n";
	     f_m "[super encodeWithCoder: encoder];@\n";
	     Hashtbl.iter
	       (fun blu ->
		 function
		     {Symbol.name = name} ->
		       f_m "GGEncode(%s);@\n" name
	       ) gv;
	     f_m "@]@\n}@\n@\n";

	     f_m "- (id)initWithCoder:(NSCoder *)decoder@\n{  @[@\n";
	     f_m "[super initWithCoder: decoder];@\n";
	     Hashtbl.iter
	       (fun id ->
		 function
		     {Symbol.name = name;
		      Symbol.thetype = t;
		    } as v->
		      if not (GGType.is_object t) then
			begin
			  f_m "GGDecode(%s);@\n" name;
			end
		      else
			begin
			  f_m "GGDecodeObject(%s);@\n"  name;
			end;
		      OExp.gc_track ~release: true var_table
			(Symbol.to_exp v)
	       ) gv;
	     f_m "return self;@]@\n}@\n@\n";

	     f_m "- (void) markForGC@\n{  @[@\n";
	     Hashtbl.iter
	       (fun key v -> 
                    let t = v.Symbol.thetype in
		    OExp.TrackObjects.apply
		      (fun e -> 
			f_m "gg_mark(";
			OExp.to_string var_table e.GGExp.descr;
			f_m ");@\n";)
		      (Symbol.to_exp v) t;
	       ) gv;
	     f_m "[super markForGC];@]@\n}@\n@\n";

	     f_m "+ @[%s_" name;
	     f_h "+ @[%s_" name;

	     my_iter 
	       (fun (name,t)  -> 
		 let s = name ^  
		   ": (" ^
		   (GGType.to_C_type 5 "" t) ^
		   ") " ^ name
		 in ( f_m "%s" s; f_h "%s" s )) 
	       (fun () -> f_m "@\n"; f_h "@\n") l;
	     f_m "@]";
	       
	     f_h "@];@\n";
	     List.iter( 
	     function 
		 { rettype = Some _ } as cont ->
		   OCont.to_string_cont_dec !formatter_h "err eur" cont;
		   f_h ";@\n"
	       | _ -> ()) cont_list;
	     f_h "@@end\n\n" ;

	     f_m "@\n{  @[@\n";
	     f_m "Complex_%s *obj = [self allocInstance];@\n" name;
	     f_m "[obj runWith@[";
	     
	     my_iter 
	       (let count = ref 1  in   function 
		   (name,t)  -> 
		     f_m "arg%d:@ %s@\n"  !count  name;
		     incr count ) 
	       (fun () -> f_m "@\n") l;
	     f_m "@]];@\n";
	     f_m "return AUTORELEASE(obj);@]@\n}@\n@@end\n"
	| _ -> failwith "not a function type"

  end

module Programme = 
  struct
    open ComplexProc
    open Compile
    open Variable
    open OProc


    type t =
      |	DecType of GGTypeDecl.t
      |	Function of fun_data
      |	Import of string
      |	Extern of GGProg.symbol_extern
      |	Const of string * GGType.t * GGExp.t
      | Enum of (string * (string list)) list
      | LetGlobal of string * GGType.t

    let rec dump_enum (s,l) = 
      f_m "@\nstatic const char *%s[] = @\n{  @[@\n" s;
      List.iter (fun x -> f_m "\"%s\",@\n" x) l;
      f_m "@]@\n};@\n"

    type env = (Stamp.t, Variable.t) Hashtbl.t
    let proc_table = Hashtbl.create 5 
    let cont_table = Hashtbl.create 5 

    let  compile (env : env) l  = 
      let rec iter decg ret = function
	| GGProg.Nothing :: l -> iter decg ret l
        (* we skip unused enum *)
	| a :: l ->
	    let a, decg = 
	      match a with 
	      | GGProg.DecType x -> DecType x, decg
	      |	GGProg.Function f  -> 
		  let proc = Transform.compile env f in
		  ComputeVariable.manage_var env proc;
		  Hashtbl.add proc_table proc.ComplexProc.tag proc;
		  List.iter (fun c -> 
		    Hashtbl.add cont_table c.Continuation.tag c) 
		    (proc.ComplexProc.main_cont :: proc.ComplexProc.cont_list);
		  Function 
		    {
		     data = proc;
		     name = f.GGFonction.name;
		     thetype = f.GGFonction.thetype
		   } , decg
	      |	GGProg.Extern v -> Extern v , decg
	      |	GGProg.Nothing -> assert false
	      |	GGProg.Import s -> Import s, decg
	      |	GGProg.Const (a,b,c) -> Const (a,b,c), decg
	      | GGProg.LetGlobal (v,s,e) ->
(*		  let v = Variable.mkvar ~loc: Variable.GlobalVar  
		      a.Symbol.name t a.Symbol.tag in
*)
		  v.localisation <- Variable.GlobalVar;
		  Hashtbl.add env v.tag v;
		  LetGlobal(v.Symbol.name, v.Symbol.thetype), (e :: decg)
            in
	    iter decg (a::ret) l
      |	[] -> List.rev ret, decg
      in 
      (* first process enum helpers *)
      
      let lsttop = List.map 
        (fun (name, {GGType.EnumString.vals = l}) -> (name, l)) (EnvironmentSimple.enums_list ()) in
      
      iter [] [ Enum lsttop ] l
	  
    let basename = ref ""
    let filename = ref ""
    let headername = ref ""

    let to_string var_table (c_m : out_channel) (c_h : out_channel) _l = 
      formatter_m := Format.formatter_of_out_channel c_m;
      formatter_h := Format.formatter_of_out_channel c_h;
      let sub_to_string   = function
	  DecType(d) -> 
	    f_h "GGBEGINDEC\n";
	    f_h "@\n%s" (GGTypeDecl.to_string 0 (d.GGTypeDecl.descr));
	    f_h "@\n";
	    f_h "GGENDDEC@\n"
	| Function (fn) -> OProc.to_string var_table  fn 
	| Import s -> 
	    f_h "@\n#include \"%s.h\"@\n@\nGGBEGINDEC@\n" s;
	    f_h "%s" "/*%%%";
	    f_h "@\nimport %s ;@\n" s ;
	    f_h "%s" "%%%*/";
	    f_h "@\n@\nGGENDDEC@\n" 
	      
	| Const (n,t,e) ->
	    f_h "GGBEGINDEC@\n";
	    f_h "static const %s" (GGType.to_C_type 0 n t);
	    f_h " = @[";
	    OExp.to_string_formatter !formatter_h var_table  e.GGExp.descr;
	    f_h "@]";
	    f_h ";@\n";
	    f_h "GGENDDEC@\n"
	| Extern {GGProg.name = name; 
		  GGProg.thetype = thetype; 
		  GGProg.cname = cname} ->
	    f_h "GGBEGINDEC@\n";
	    f_h "%s" "/*%%%";
	    f_h "@\nextern  %s@ :@ %s@ =@ \"%s\";@\n" name
		      (GGType.to_string 0 thetype) cname;
	    f_h "%s" "%%%*/";
	    f_h "@\nGGENDDEC@\n"
	| LetGlobal(s, t) ->
	    f_m "@\n%s;@\n" (GGType.to_C_type 0 s t)
	| Enum l -> List.iter dump_enum l
	      
      in
      let g s = (f_m "%s@\n" s) in
      let g' s = (f_h "%s@\n" s) in
      let out_preambule g =
	begin
	  g "/* DO NOT EDIT !!!";
	  g " * This file is automatically generated from the file ";
	  g !filename;
	  g " * modify this file instead";
	  g " */\n\n";
	end

      in
      out_preambule g;
      out_preambule g';
      f_h "#ifndef __%s__h@\n" !basename;
      f_h "#define __%s__h@\n" !basename;

      f_h "#include \"GGRunTime.h\"@\n@\n" ;
      f_m "#include \"%s\"@\n@\n" !headername;

      let (l,glob_init) = compile var_table _l in

      let next = 
	if List.length glob_init > 0 then
	  begin
	    let c = GGCommand.flatten glob_init in
	    if c.GGCommand.atomicity = GGCommand.NonAtomic then
	      failwithloc c.GGCommand.loc 
		"initializer is not an atomic expression";
	    let myfun = GGFonction.mk 
		true
		GGType.Unit
		"initModule"
		[]
		c in
	    let proc = Transform.compile var_table myfun in
	    ComputeVariable.manage_var var_table proc;
	    let c = proc.ComplexProc.main_cont in
	    Hashtbl.add cont_table c.Continuation.tag c;
	    
	    fun () ->
	      let ps s = f_m "%s@\n" s in
	      let n = "Module__" ^ !basename in
	      f_m "@@interface %s : NSObject @\n" n;
	      ps "{";
	      ps "}";
	      ps "@end";
	      ps "";
	      f_m "@@implementation %s@\n" n;
	      ps "+ (void) load";
	      ps "{";
	      f_m "  gg_add_module(@@\"%s\");@\n" n; 
	      (*ps "  gg_add_module(self);"; *)
	      ps "}";
	      ps "";
	      ps "+ (void) initModule";
	      f_m "@\n{  @[";
	      OCont.print_dec proc.main_cont.Continuation.local_vars;
	      OCom.to_buf proc.lab_hash var_table false 
		proc.main_cont.Continuation.comlist;
	      f_m "@]@\n}@\n";
	      ps "@end";
	  end
	else
	  fun () -> () in


      ComputeVariable.finish var_table cont_table proc_table;
      List.iter sub_to_string l;
      next ();
      g' "#endif";
  end
