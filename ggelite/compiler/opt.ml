open Utile
open Ggtree
open Decgen

module MutableSet(Ord : Set.OrderedType) =
  struct
    module M = Set.Make (Ord)
    include M

    let make () = ref (M.empty)
    let add_if_absent t x = 
      t := M.add x !t

    let remove t x =
      t := M.remove x !t

  end


module MutableStampSet = MutableSet (Stamp)

module MakeOpt = 
  struct
    open CommandC

    type cont_type =
	Has_cont
      |	No_cont

    module ExpAnalize =
      struct
	open GGExp
	  
	let bool_eff = function
	    { descr = Bool(true) } -> Some true
	  | { descr = Bool(false) } -> Some false
	  | _ -> None

      end

    let do_job (proc : ComplexProc.t)  =
      
      let already = MutableStampSet.make ()
      and todo : ContSet.t ref = ref (ContSet.empty)
      in 
      let manage ?(cont) ?(idlabel) () =
	let cont = 
	  match cont with
	    Some cont -> cont
	  | None ->  
	      let idlabel = match idlabel with
		Some x -> x
	      |	None -> failwith "internal error"
	      in
	      ComplexProc.get_cont proc idlabel
	in
(*	Printf.printf "manage cont_name : %s\n" cont.Continuation.cont_name;*)
	let idcont = cont.Continuation.tag in 
	if not (MutableStampSet.mem idcont !already) then 
	    todo := ContSet.add cont !todo
      in

	  
      let rec skip_label_coms = function
	  Seq(Label _, l) -> skip_label_coms l
	| l -> l
      and head = function 
	  Seq(a, _) -> head a
	| x -> x
      in
      let rec compute_target idlabel =
	let label = ComplexProc.get_label proc idlabel in
	match label.Label.labelCont with
	  Some { Continuation.comlist = l } ->
	    begin
	      match head (skip_label_coms l) with 
		Jump idlabel when ComplexProc.is_label_global proc idlabel ->
		  compute_target idlabel
	      | _ -> idlabel
	    end
	| _ -> idlabel
      in
	      
      let rec iter flag c = 
	if flag then
	  match c with
	    If (e, c1, c2) -> 
	      begin 
		match ExpAnalize.bool_eff e with
		  Some true -> iter true c1
		| Some false -> iter true c2
		| None -> 
		    let l1,e1 = iter true c1
		    and l2,e2 = iter true c2 in
		    If(e, l1, l2), e1 || e2
	      end
	  | Jump lab  -> 
	      let labelid = compute_target lab in
	      let label = ComplexProc.get_label proc labelid in
	      if label.Label.label_is_top 
	      then
		manage ~idlabel: labelid ();
	      Jump labelid, false
	  | RegisterMessage (cl,_, pcl) -> 
	      List.iter (function {action_label=l}-> manage ~idlabel:l ()) 
		cl; 
	      List.iter (fun idlabel -> manage ~idlabel ()) pcl;
	      c, true
	  | Clean [] -> Nop, true
	  | Expr { GGExp.descr = GGExp.No_Exp | GGExp.Var _ } -> Nop, true
	  | Set_timer (id, e) -> manage ~idlabel:id (); c,true
	  | Reschedule_timer (id, e)  -> manage ~idlabel:id  (); c,true
	  | RegisterCont (id, _)  -> manage ~idlabel: id (); c, true
	  | Return _  
	  | Abort
	  | SimpleReturn _ -> c, false
	  | Partial_call (_,_,id) -> manage ~idlabel: id (); c,false
	  | Expr _
	  | Assign _
	  | Label _
	  | Let _
	  | Destroy_Callee
	  | Destroy_Job _
	  | UnregisterMessage _
	  | SetNotVar _
	  | Clean _
	  | CheckObject _
	  | Nop  -> c,true
	  | Seq (c1, c2) ->
	      let c1, t = iter true c1 in
	      let c2, t = iter t c2 in
	       c1 @@ c2, t
	else
	  match c with
	  | Label id when (ComplexProc.get_label proc id).Label.used  ->
	      c,true
	  | Seq(c1, c2) ->
	      let c1, t = iter false c1 in
	      let c2, t = iter t c2 in
	      c1 @@ c2, t
	  | _ -> Nop, false

      in
      let opt_cont = function
	  { Continuation.comlist = l } as cont ->
	    cont.Continuation.comlist <- fst (iter true l)
      in

      let rec my_loop () =
	if not (ContSet.is_empty !todo) then
	  let cont = ContSet.choose !todo in
	  opt_cont cont;
	  todo  := ContSet.remove cont !todo;
	  MutableStampSet.add_if_absent already cont.Continuation.tag;
	  my_loop ()
      in
      match proc with
	  { ComplexProc.main_cont = cont } ->
	    manage ~cont ();
	    my_loop ();
(*	    Continuation.dump proc.ComplexProc.cont_list;*)
	    let l = List.fold_left
		begin
		  fun l x -> 
		    if MutableStampSet.mem x.Continuation.tag !already then
		      x :: l
		    else
		      l
		end 
		[] proc.ComplexProc.cont_list
	    in
	    proc.ComplexProc.cont_list <- l;
(*	    Continuation.dump proc.ComplexProc.cont_list *)

  end
