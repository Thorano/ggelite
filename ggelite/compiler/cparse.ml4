open Utile
open Ggtree
open Compile
open Camlp4.PreCast
open Syntax

let string_of_loc = GGStdpp.string_of_loc

module Lexer = struct end

module Gram = MakeGram(Lexer)
let prog_eoi = Gram.Entry.mk "Declaration"

let main_parser : (char Stream.t -> EnvironmentSimple.t -> (EnvironmentSimple.t * 
							 (GGProg.t list))) ref =
  ref (fun s e -> (e,[]))

open GGType

let hack t env = fst(t env)


EXTEND Gram
  GLOBAL: prog_eoi;

prog_eoi:
    [
     [ l = LIST0 decl; EOI -> 
       fun env -> 
	 let (newenv, comlst) = 
	   List.fold_left   
	     begin 
	       fun (env,ol) f ->  
		 let e,o = f env in
		 (e, o :: ol)
	     end
	     (env,[])  l in
	 newenv, List.flatten (List.rev comlst)
     ]
    ] ;

  number:
    [[
     IDENT | INTEGER | FLOAT
   ] ];

cdecmod:
    [[ l = LIST0 [ STATIC -> true | CDECMOD -> false] ->
      List.fold_left (||) false l
     ] ];

decl:
    [[ 
     lvalue = cdecmod; y = c_decl ; OPT [ "=" ; number] ;";" -> 
       fun env -> 
	 let (x,t), env = y env in
	 let v = Symbol.mk 
	     ~alpha: false ~lvalue: false
	     ~name: x t in
	 {env with EnvironmentSimple.symbols = 
	  StringMap.add x v env.EnvironmentSimple.symbols},[]
   | s = INLINE -> 
       fun env ->
	 let str = Stream.of_string s in
	 !main_parser str env
       
   | TYPEDEF ; y = c_decl ; ";" ->
       fun env -> let (x,t),env = y env in
       {env with EnvironmentSimple.types = 
	StringMap.add x (GGTypeDecl.mk (x,t)) env.EnvironmentSimple.types},[]
   | BEGININTERFACE ; obj = objc_interface ; ENDINTERFACE ->
       fun env -> Objc.compile obj loc env, []
   | CLASS ; x = IDENT; ";" -> 
       fun env -> Objc.compile_forward loc x env, []
     ]];

objc_interface:
    [[
     x = IDENT ; herite = OPT [":" ; x = IDENT ->  x]  ; p = OPT protocol  ;
     lo = OPT  ["{" ; l = LIST0 objc_variable ; "}" -> l ] ;
     lm = LIST0 objc_methode -> 
       let l = match lo with
	 Some l -> l
       | None -> []
       in
       (x, herite, l, lm, p)
   ] ];

protocol:
    [[ "<"; l = LIST1 [x = IDENT -> x] SEP "," ; ">"  -> `Protocol l
     | "("; IDENT; ")" -> `Category
     ]];

objc_variable:
	 [[ x = PUBLIC -> `Public
          | y = PRIVATE -> `Private
	  | v = c_struct_decl -> `Instance_variable  v
	  ]];

objc_methode:
   [[
    "+" ; m = real_method -> `Class m
   |"-"; m = real_method -> `Instance m
  ] ];   

real_method:
  [[
   t = OPT ["(" ; t = c_decl ; ")" -> hack t,loc] ; 
   x = IDENT ; 
   o = OPT [ ":" ; t = arg_method_dec ; 
	     l = LIST0 [x = IDENT; ":" ; t' = arg_method_dec -> (x,t')] -> (t,l)] ;
   ";" -> (t,x,o)
 ] ];


arg_method_dec:
 [[
  t = OPT ["(" ; t = c_decl ; ")" -> hack t,loc]; IDENT ; OPT[","; "..."] -> t
] ];


c_struct_decl:
    [[
     t = c_basic_type; l = LIST1 [x = c_name ; OPT [":" ; INTEGER] -> x] SEP ","; ";" ->
       fun env -> 
	 let t,env = t env in
	  List.map (fun x -> fst (x env t)) l, env
   ] ];

c_basic_type:
    [[
     UNIT   ->  fun env -> Unit, env
   | OPT [x = UNSIGNED -> x]; INT  ->  fun env -> Integer, env
   | UNSIGNED -> fun env -> Integer, env
   | BOOL  ->  fun env -> Bool, env
   | FLOAT ->  fun env -> Float, env
   | ID ; OPT protocol ->  fun env -> Id, env
   | STRUCT ; x = OPT [x = IDENT->x];  
       lp = OPT ["{" ; l = LIST1 [t = c_struct_decl -> t] ; "}" -> l]  -> 
	 fun env ->    
	   begin
	     match lp with 
	       Some l ->
		 let l0, (env : EnvironmentSimple.t) =  (List.fold_left 
				   (fun (res, env) f   -> 
				     let a, env = f env in
				     (a:: res) , env ) ([], env) l) in
		 Struct   (List.flatten (List.rev l0 )), env
	     | None ->
		 match x with 
		   Some n -> Name(n, Unit), env
		 | None -> GGStdpp.raise_with_loc loc 
		       (Failure "syntax error")
	   end
   | ENUM ; x = OPT [x = IDENT->x]; lp = OPT[ "{" ; l = LIST1 
	   [ x = IDENT ; OPT ["=" ; dimension] -> x] SEP ",";
	 OPT [ "," ] ; "}" -> l ] -> 
	   fun env -> 
	     begin
	       match lp with
		 Some l ->
		   let t = GGType.Enum l
		   in 
		   let enums = 
		     List.fold_left 
		       begin 
			 fun enums x ->  StringMap.add x t enums
		       end  env.EnvironmentSimple.enums l
		   in t, {env with EnvironmentSimple.enums = enums}
	       | None -> 
		   match x with 
		     Some n -> Name(n, Unit), env
		   | None -> GGStdpp.raise_with_loc loc 		       
			 (Failure "syntax error")
	     end
   | x = IDENT ; OPT protocol -> 
       fun env ->
	 try
	   Compile.EnvironmentSimple.lookupType dummy_loc x env, env
	 with
	   _ -> Name(x, Unit), env
   ] ];
c_decl:
  [[
        OPT [x = CDECMOD -> x] ; 
   t = c_basic_type ;  x = c_name -> fun env -> let t,env = t env in
   let x, env = x env t in
   x, env
 ] ]
;

c_name:
    [
  "lvl1" 
    [ "*" ; x = c_name -> fun env a -> x env (Pointer(a))
    ] 
    | "lvl2"
    [
      x = IDENT -> fun env a -> (x,a), env
    | "(" ; x = c_name ; ")" -> x
    | x = c_name ; "(" ; l = LIST0 c_decl SEP "," ; ")" ->
	fun env a -> 
	  let l',env = List.fold_right (fun f (res, env) -> 
	    let a,env = (f env) in
	    (a :: res, env) ) l ([], env) in
	  let l' =  match l' with
	    [_,Unit] -> []
	  | l -> l in
	  let a = GGType.ctype_to_gg a in
	  let t = Function(a,l') in
	  x env t 
	  
    | x = c_name ; "[";  n = OPT [n = dimension -> n] ; "]" ->
	fun env a ->
(*	  let n = match n with
	    Some x -> x
	  | None -> 0
	  in*)
	  let t = Array(ref n, a) in
	  x env t
   ] 
|      "lvl2" LEFTA
       [ -> fun env a -> ("", a),env]
     
   ];

dimension:
[[
  x = INTEGER -> int_of_string x
| x = IDENT -> 50
] ];
  
END

let lstdir = ref []


let rec open_in_list n = function
    a :: l ->
      begin
	try 
	  let name = Filename.concat a n in
	  open_in name
	with
	  Sys_error _ -> open_in_list n l
      end
  | [] -> raise (Sys_error ("cannot find file " ^ n ^ " in "))

module S = EnvironmentSimple.StringSet
module Env = EnvironmentSimple

let import recursif (x:string) loc (env : EnvironmentSimple.t) =
  if not (S.mem x env.Env.imported) then
    begin
      let list = "." :: !lstdir in
      let name = x ^ ".h" in
      try
(*	Printf.eprintf "[parsing %s\n" name;*)
	let in_file = open_in_list  name list in
	let s = Stream.of_channel in_file in
	let (e,l) = Grammar.Entry.parse prog_eoi s env in
	let e = { e with Env.imported = 
		  S.add x e.Env.imported} in
(*	Printf.eprintf "parsing %s end]\n" name;*)
	let l = if false && recursif then l else GGProg.Import x :: l in
	(e, l)
with 
  Sys_error s -> 
    GGStdpp.raise_with_loc loc (Failure (s ^ (String.concat ":" list)))
| GGStdpp.Exc_located (loc,exn) as e ->
    let s = Printexc.to_string exn in
    Printf.printf "\nerreur dans %s\n%s\n%s" name s (string_of_loc loc);
(*    print_string name;
    print_string "\n";
    print_string s;
    Printf.printf "\ndebut : %d fin : %d\n" i j;*)
    raise e
    end
  else
    begin
(*    Printf.eprintf " --> parsing of %s already done\n" x;*)
    (env, [])
    end
      
