open Utile

type location = GGStdpp.localisation

let dummy_loc = GGStdpp.dummy_loc

module GGType = 
  
  struct
    module EnumString =
        struct
            type t = {
                stamp : Stamp.t;
                mutable used : bool;
                vals : string list;
            }
            
            let mk l = { used = false; vals = l; stamp = Stamp.mk ()}
            
            let equal enum1 enum2 = Stamp.equal enum1.stamp  enum2.stamp
            let hash enum1 = Stamp.hash enum1.stamp
        end
    
    type t = 
	Unit
      |	Id
      |	Bool
      | Integer 
      |	String
      | Float
      |	Enum of EnumString.t
      |	Array of (int option ref)*t
      |	Struct of (string*t)list
      |	Function of (t*((string*t) list))
      |	Name of (string*t)
      | Abstract of string
      |	Pointer of t
      |	Objet of obj_int ref

    and objet =
	{
	 name : string;
	 herite : obj_int ref option;
	 var_i : (string * t) list;
	 mutable methods_i : methode list; (* mutable because of category *)
	 mutable methods_c : methode list;
       } 
    and methode = t ProtoTree.GGInterface.typed_method
    and obj_int = 
	Vrai of objet
      |	Faux of string

    let mk_objet s =
      Objet(ref (Faux s))

    let rec effectiveType = function
	Name(_,t) -> effectiveType t
      | Pointer ((Objet _) as obj) -> obj
      |	x -> x

    let rec isEqual t1 t2 = 
      let list_type_eq_strict = List.for_all2 
	    (fun (s1,t1) (s2,t2) -> s1 = s2 && isEqual (t1) (t2)) 
      and list_type_eq = List.for_all2 
	    (fun (s1,t1) (s2,t2) -> isEqual (t1) (t2)) 
      in
      match (effectiveType t1,effectiveType t2) with
	Id,Id -> true
      |	Id,_ -> false

      |	Objet o, Objet o' -> o == o'
      |	Objet o, _ -> false

      |	Enum x, Enum y -> x == y
      |	Enum x, _ -> false

      | Bool,Bool -> true
      |	Bool,_ -> false

      | Integer,Integer -> true
      |	Integer,_ -> false

      |	String,String -> true
      |	String,_ -> false

      | Float,Float -> true
      |	Float,_ -> false

      | Array(n,t),Array(n',t') ->
	  n = n' && (isEqual (t) (t'))
      |	Array _,_ -> false

      | Struct(l),Struct(l') -> list_type_eq_strict l l'
      |	Struct _,_ -> false

      |	Function(t1,l1),Function(t2,l2) -> isEqual (t1) (t2) && 
	  list_type_eq l1 l2
      |	Function _,_ -> false

      |	Pointer t, Pointer t' -> isEqual t t'
      |	Pointer _,_ -> false

      |	Unit,Unit -> true
      |	Unit,_ -> false
      
      | Abstract _, _
      | _, Abstract _  -> failwith "no abstract type allowed there"

      |	Name _,_ -> failwith "impossible"

    let rec inherits a b = 
      if a == b then true
      else match a with
	{contents = Vrai { herite = Some a'}} -> inherits a' b
      |	_ -> false

    let is_sub_type t1 t2 = match effectiveType t1, effectiveType t2 with
      Id, Objet _ -> true
    | Objet _, Id -> true (*FIXME is unsafe....*)
    | Objet o, Objet o' ->
	inherits o' o
    | a,b -> isEqual a b

    let merge_cat o1 cat =
      o1.methods_i <- o1.methods_i @ cat.methods_i;
      o1.methods_c <- o1.methods_c @ cat.methods_c
      

    let rec to_string lvl = function
	  Unit -> "Unit"
	| Id -> "Id"
	| Bool -> "Bool"
	| Integer -> "Int"
	| Float -> "Float"
	| String -> "String"
	| Enum {EnumString.vals = l} -> "Enum {\n" ^
	    (ggconcat (lvl+4) 
	       (fun lvl n ->(insert_space lvl) ^ n)
	       ("," ^ (insert_space (lvl+1)) ^ "\n") 
	       l) ^ 
	    "\n" ^ (insert_space lvl) ^ "}"
	| Array ({contents=Some n},t) -> "array[" ^ (string_of_int n) ^
	    "] of " ^ (to_string (lvl + 4) t)
	| Array ({contents=None},t) -> "array[] of " ^
	    (to_string (lvl + 4) t)
	| Struct(s) -> "struct of\n" ^
	    (insert_space lvl) ^
	    "{\n" ^
	    (ggconcat (lvl+4) 
	       (fun lvl (n,t)->(insert_space lvl) ^ (to_string_name lvl n t))
	       (";" ^ (insert_space (lvl+1)) ^ "\n") 
	       s) ^ 
	    ";\n" ^ (insert_space lvl) ^ "}"
	| Name(s,t) -> s
	| Function (t, l) -> "(" ^
	    (ggconcat (lvl+4)
	       (fun lvl (n,t) -> (to_string_name lvl n t))
	       ", " l) ^
	    ")" ^ ":" ^ (to_string (lvl+4) t)
	| Pointer(t) -> (to_string lvl t) ^ " ref"
	| Objet {contents = Vrai {name = n}} -> n
	| Objet {contents = Faux s} -> s
        | Abstract s -> s
    and to_string_name lvl name t =
      name ^ " : " ^ (to_string lvl t)



    let rec to_string_debug lvl a = 
      let rec ancestors s = function
	  None -> s
	| Some {contents = Vrai {name = x; 
				 herite = h
			       }} ->
				 ancestors (s ^ " << " ^ x) h
	| Some {contents = Faux x} -> (s ^ " << " ^ x)
      in
	    
      let to_string x = to_string_debug (lvl+2) x in
      let to_string_name lvl name t =
	name ^ " : " ^ (to_string t)
      in
      match a with
	Unit -> "Unit"
      | Id -> "Id"
      | Bool -> "Bool"
      | Integer -> "Int"
      | Float -> "Float"
      | String -> "String"
      | Enum {EnumString.vals = l} -> "Enum {\n" ^
	  (ggconcat (lvl+4) 
	     (fun lvl n ->(insert_space lvl) ^ n)
	     ("," ^ (insert_space (lvl+1)) ^ "\n") 
	     l) ^ 
	  "\n" ^ (insert_space lvl) ^ "}"
      | Array ({contents=Some n},t) -> "array[" ^ (string_of_int n) ^
	  "] of " ^ (to_string  t)
      | Array ({contents=None},t) -> "array[] of " ^
	  (to_string  t)
      | Struct(s) -> "struct of\n" ^
	  (insert_space lvl) ^
	  "{\n" ^
	  (ggconcat (lvl+4) 
	     (fun lvl (n,t)->(insert_space lvl) ^ (to_string_name lvl n t))
	     (";" ^ (insert_space (lvl+1)) ^ "\n") 
	     s) ^ 
	  ";\n" ^ (insert_space lvl) ^ "}"
      | Name(s,t) -> "Name " ^ s ^ " (" ^ (to_string t) ^ ")"
      | Function (t, l) -> "(" ^
	  (ggconcat (lvl+4)
	     (fun lvl (n,t) -> (to_string_name lvl n t))
	     ", " l) ^
	  ")" ^ ":" ^ (to_string t)
      | Pointer(t) -> (to_string  t) ^ " ref"
      | Objet {contents = Vrai {name = n; herite = h}} -> "vrai (" ^ 
	  (ancestors n h) ^ ")"  ^ n
      | Objet {contents = Faux s} -> "faux " ^ s
      | Abstract s -> "<abstract:" ^ s ^ ">"

    let rec csize = function
	| Unit -> 0
	| Id -> 4
	| Bool -> 1
	| Integer -> 4
	| Float -> 8
	| String -> 4
	| Array ({contents = Some n},t) -> 
	    n * (csize t)
	| Array ({contents = None},t) -> 
	    csize t
	| Struct(s) -> 
	    List.fold_left (fun x (_,t) -> x + (csize t)) 0 s
	| Function (t, l) -> 4
	| Name(_,t) -> csize t
	| Pointer(t) -> 4
	| Objet _ -> 4
	| Enum l -> 4
        | Abstract _ -> failwith "size undefined"


    let rec to_C_type lvl _name = 
      let name = if _name <> "" then "(" ^ _name ^ ")" else _name in 
      function
	  Unit -> "void " ^ name
	| Id -> "id " ^ name 
	| Bool -> "BOOL " ^ name
	| Integer -> "int " ^ name
	| Float -> "double "^name
	| String -> "NSString *" ^ name
	| Array ({contents = Some n},t) -> to_C_type (lvl+1) (name ^ "[" ^
					  (string_of_int n) ^
					  "]") t
	| Array ({contents = None},t) -> to_C_type (lvl+1) (name ^ "[]") t
	| Struct(s) -> 
	    let s = List.sort (fun (_,a) (_,b) -> 
	      compare (csize b)  (csize a)) s in
	    "struct \n" ^
	    (insert_space lvl) ^
	    "{\n" ^
	    (ggconcat (lvl+4) 
	       (fun lvl (n,t)->(insert_space lvl) ^
		 (to_C_type lvl n t))  
	       (";" ^ (insert_space (lvl+1)) ^ "\n") 
	       s) ^ 
	    ";\n" ^ (insert_space lvl) ^ "}" ^  name
	| Function (t, l) -> 
	    let s = name ^ "(" ^ (ggconcat (lvl+1)  
				    (fun lvl (name,t) -> to_C_type lvl name t)
				    ", " l) ^
	      ")" in
	    (to_C_type (lvl+1) s t) 
	    
	| Name(s,_) -> s ^ " " ^ name
	| Pointer(t) -> to_C_type (lvl+1) ("*" ^ name) t
	| Objet {contents = Vrai {name = n}} 
	| Objet {contents = Faux n } -> n ^ " *" ^ name
	| Enum {EnumString.vals = l} -> 
	    "enum {\n" ^
	    (ggconcat (lvl+4) 
	       (fun lvl n ->(insert_space lvl) ^ n)
	       ("," ^ (insert_space (lvl+1)) ^ "\n") 
	       l) ^ 
	    "\n" ^ (insert_space lvl) ^ "}" ^ name
        | Abstract s -> s


    let rettype_of_funtype = function
	Function(t,_) -> t
      |	_ -> raise (Invalid_argument "blabla")


    let argtype_of_funtype = function
	Function(_,l) -> l
      |	_ -> raise (Invalid_argument "blabla")

    let type_of_label t l = match effectiveType t with
    | Struct s ->
	let (_,t) = List.find (fun (l',_) -> l = l') s in
	t
    | _ -> raise (Invalid_argument "type_of_label")


    let is_object = function
	Objet _ | Id | String -> true
        | Abstract _ -> failwith "Abstract not handle there"
      |	_ -> false

    let is_object_trackable = function
      | Objet { contents = Faux "ListeningBlock" }
      | Objet { contents = Vrai { name = "ListeningBlock" } } -> false
      |	Objet _ | Id | String -> true
      |	_ -> false

    let is_string = function
      |	String 
      |	Objet { contents = Faux "NSString" }
      |	Objet { contents = Vrai { name = "NSString" } } -> true
      |	_ -> false


    let rec contains_object = function 
	(* are there any object hidden in this type ?*)
      | Objet _
      | Id -> true
      | Unit
      | Bool
      | Integer 
      | String
      | Float
      | Enum _ -> false
      | Array (_,t) -> contains_object t
      | Struct (l) ->
	  List.exists (fun (_,t) -> contains_object t) l
      | Pointer _ (*FIXME*)
      | Function _ -> assert false
      | Name (_,t) -> contains_object t
      | Abstract x -> failwith "not handle for abstract"

    let rec ctype_to_gg = function
      | Pointer (Objet _ as x) -> x
      | (Objet _
      | Id 
      | Unit
      | Bool
      | Integer 
      | String
      | Float
      | Pointer _ 
      | Abstract _
      | Enum _) as x -> x
      | Array (x,t) -> Array(x, ctype_to_gg t)
      | Struct (l) ->
	  let l = List.map (fun (n,t) -> (n, ctype_to_gg t)) l in
	  Struct l
      | Function (t, l) -> 
	  let l = List.map (fun (n,t) -> (n, ctype_to_gg t)) l in
	  Function (ctype_to_gg t, l)
      | Name (n,t) -> Name (n, ctype_to_gg t)
  end

module ProtoSymbol = 
  struct
    type varloc =
	Undefined
      |	GlobalVar
      |	ComplexExternalFunc
      |	ArgumentLoc
      | MediumLoc of Stamp.t
      |	LocalLoc of Stamp.t
      | Function of Stamp.t

    type t =
	{
	 name : string; 
	 thetype : GGType.t;
	 tag : Stamp.t;
	 lvalue : bool;
	 complex : bool; (* is it an atomic function *)
	 mutable localisation: varloc
       } 

    let mk ?(loc=Undefined) ?(tag  = Stamp.mk ()) ?(complex=false) ~alpha ~name ~lvalue
	  thetype =
      let name = 
	if alpha then
	  Stamp.fresh_name ~tag ~prefix: name () 
	else
	  name
      in
      {
       localisation = loc;
       name = name;
       thetype = thetype;
       tag = tag;
       complex = complex;
       lvalue = lvalue;
     } 

  end

module GGExp = 
  struct
    type binop = ProtoTree.GGExp.binop = 
	Plus 
      |	Prod 
      |	ProdVect
      | Minus 
      | Div 
      | Equal 
      |	Notequal
      | Lesser 
      | LesserEq 
      | GreaterEq 
      | Greater
      |	And
      |	Or
      |	Not

    type   t = { descr :   exp; 
		 loc : GGStdpp.localisation ; 
		 thetype : GGType.t ;
		 lvalue : bool ; 
		 (*owner : bool*)
		 }
    and   exp = 
	No_Exp
      |	Int of int
      | Float of float
      |	Bool of bool
      |	String of string
      |	Nil
      | Var of ProtoSymbol.t
      |	Symbol of string
      | Function of  t *( t list)
      |	Binop of binop *  t *  t
      |	Monop of binop * t
      |	ArrayAccess of t *  t
      |	StructAccess of  t * string
      |	MessageCall of t * string * 
	    ((t * ((string *  t) list)) option)
      | StructBuild of (string * t) list
      |	ArrayBuild of t list
      |	Address of t

    let mk ?(loc=GGStdpp.dummy_loc) descr ?(lvalue=false) (*?(owner=false)*) t  = {
      descr = descr ;
      loc = loc ;
      thetype = t;
      lvalue = lvalue;
(*      owner = owner;*)
    }


    let string_of_op = function
	Plus -> "+"
      | Prod -> "*"
      | Minus -> "-"
      | Div -> "/" 
      |	Equal -> "=="
      |	Notequal -> "!="
      | Lesser -> "<"
      | LesserEq -> "<="
      | GreaterEq -> ">="
      | Greater -> ">"
      |	Or -> "||"
      |	And -> "&&"
      |	Not -> "!"
      |	ProdVect -> compiler_bug "should not translate this to string"


    let is_lvalue = function
	{ lvalue = a } -> a

    let is_simple e = match e.descr with
    | Int _
    | No_Exp
    | Bool _
    | String _
    | Nil 
    | Float _
    | Var _ -> true
    | Symbol _
    | Function _
    | Binop _
    | Monop _
    | ArrayAccess _
    | StructAccess _
    | MessageCall _
    | StructBuild _
    | ArrayBuild _
    | Address _ -> false

    let rec is_no_compute e = match e.descr with
      (* Is there any computation involved in this expression *)
      (* FIXME: What to do with symbols ? *)
    | Int _
    | No_Exp
    | Bool _
    | String _
    | Nil 
    | Float _
    | Symbol _
    | Var _ -> true
    | Function _
    | Binop _
    | MessageCall _
    | Monop _ -> false
    | ArrayAccess (e1, e2) -> is_no_compute e1 && is_no_compute e2
    | StructAccess (e, _) -> is_no_compute e
    | Address e -> is_no_compute e
    | StructBuild l ->	List.for_all (fun (s,e) -> is_no_compute e) l
    | ArrayBuild l -> List.for_all is_no_compute l


    let is_object x = GGType.is_object x.thetype

    let mk_int ?(loc=GGStdpp.dummy_loc) i =
      mk ~loc (Int i) (GGType.Integer)

    let build_field_access e n = 
      let t = GGType.type_of_label e.thetype n in
      mk ~loc: e.loc ~lvalue: e.lvalue (StructAccess(e, n)) t

    let build_array_acces ~loc e n = 
      let t = match e.thetype with
      |	GGType.Array(_,t) -> t
      |	_ -> assert false 
      in
      let n = mk_int ~loc n in
      mk ~loc ~lvalue: e.lvalue (ArrayAccess(e,n)) t


    module DefaultVal = struct
      module T = GGType
	
      let build ?(loc=GGStdpp.dummy_loc) t = 
	let rec iter t = 
	  let pe = match t with
	    T.Unit -> No_Exp 
	  | T.Id -> Nil 
	  | T.Bool -> Bool false
	  | T.Integer -> Int 0
	  | T.Float -> Float 0.0
	  | T.String -> String ""  (*FIXME*)
	  | T.Enum {T.EnumString.vals = l} -> Symbol (List.hd l)
	  | T.Array (n,t) -> assert false
	  | T.Struct(s) -> 
	      let l = List.map (fun (n,t) -> 
		let e = iter t in
		(n,e)) s in
	      StructBuild l
	  | T.Name(s,t) -> (iter t).descr
	  | T.Function (t, l) -> assert false
          | T.Abstract _ -> failwithloc loc "can't make a default val of an Abstract type"
	  | T.Pointer(t) -> Nil
	  | T.Objet _ -> Nil in
	  mk ~loc pe t
	in
	iter t
    end
  end


module GGTypeDecl =
  struct
    type t = {descr : typedecl; loc : GGStdpp.localisation}
    and typedecl = string*GGType.t


    let mk ?(loc=GGStdpp.dummy_loc) descr = {
      descr = descr ;
      loc = loc }


    let make_var_dec s t loc=
      {
       descr =  s,t;
       loc = loc}

    let to_string lvl = function
	(n, t) -> (insert_space lvl)^
	  "typedef "^(GGType.to_C_type lvl n (t)) ^ ";"

    let type_of = function 
	{descr = s,t} -> GGType.Name(s,t)

  end


module GGCommand =
  struct	  
    type wait_type =
	Delay of GGExp.t
      |	Stand_Wait of string

    type priv_msg_t = 
      | Nope
      | TimerB
      | ContReturn 

	    (*the stamps in the jumps instructions have the following 
	       meaning:
	       the first is the adress of destination.
	       the second is the adress of the block we break;
	     *)

    type atomicity = Atomic | NonAtomic
    type   t = {descr :    command ; loc : GGStdpp.localisation; atomicity : atomicity }
    and command = 
	If of GGExp.t * t  * t 
      |	Expr of  GGExp.t
      |	Assign of GGExp.t * GGExp.t
      |	Set_timer of string * GGExp.t * timer_msg_data
      |	Reschedule_timer of GGExp.t * timer_msg_data
      |	Wait of wait_type
      |	Jump of Stamp.t * Stamp.t
      |	Label of Stamp.t
      |	Let of ProtoSymbol.t
      |	Complex_call of GGExp.t * (GGExp.t list) * ProtoSymbol.t
      |	Return of GGExp.t option
      |	Listen of listen_block
      |	SetNotVar of ProtoSymbol.t * string
      |	Clean of Stamp.t list
      |	CheckObject of GGExp.t * (GGType.obj_int ref)
      |	BlockToPop of Stamp.t list ref list
      | Return_Message of GGExp.t option
      | Answer_and_jump of GGExp.t * Stamp.t * Stamp.t
      | Destroy_Job of Stamp.t  * Stamp.t list ref
      | Abort
      | Seq of t * t
      | Nop
    and glb_msg_data =
	{
	 source_event :  (GGExp.t) option;
	 cond_event : (GGExp.t * t) option;
	 message_event : string;
	 args_event : (string * GGType.t * Stamp.t) list;
	 com_event : t;
       } 
    and timer_msg_data = 
	{
	 timer_msg_name : string;
	 mutable timer_name : string option;
	 mutable timer_com_list : t;
	 mutable timer_used : priv_msg_t;
	 timer_id : Stamp.t;
	 timer_args : (string * GGType.t * location) list;
       } 
    and prv_msg_data =
	{
	 prv_name : string list;
	 prv_args : ProtoSymbol.t list ;
	 prv_com : t;
	 prv_rettype : GGType.t
       }
    and  listen_block =
	{
	 listen_com : t;
	 glb_msg_list :  glb_msg_data list;
	 prv_msg_list : prv_msg_data list;
	 timer_msg_list : timer_msg_data list;
	 listen_id : Stamp.t;

(*variable that should be set to nil when one exits such a block (by a signal)
*)
	 to_erase : Stamp.t list;
(*
  variable that should be set to nil when on exits the command block 
   classicaly (by reaching its end)
*)
	 clean_after : Stamp.t list;
       } 

    let to_string c = match c.descr with
    |	If _ -> "if"
    |	Expr _ -> "expr"
    |	Assign _ -> "assign"
    |	Set_timer _ -> "set_timer"
    |	Reschedule_timer _ -> "Reschedule_timer"
    |	Wait _ -> "Wait"
    |	Jump _ -> "Jump"
    |	Label _ -> "Label"
    |	Let _ -> "Let"
    |	Complex_call _ -> "Complex_call"
    |	Return _ -> "Return"
    |	Listen _ -> "Listen"
    |	SetNotVar _ -> "SetNotVar"
    |	Clean _ -> "Clean"
    |	CheckObject _ -> "CheckObject"
    |	BlockToPop  _ -> "BlockToPop"
    | Return_Message _ -> "Return_Message"
    | Answer_and_jump _ -> "Answer_and_jum"
    | Destroy_Job _ -> "Destroy_Job"
    | Abort -> "Abort"
    | Seq  _ -> "Seq"
    | Nop -> "Nop"


    let mk ?(loc=GGStdpp.dummy_loc)  descr = 
      let (+) a b =
	match a,b with
	  Atomic, Atomic -> Atomic
	| _ -> NonAtomic
      in
      let atomicity = 
	match descr  with
	  Wait _ -> NonAtomic
	| Listen { listen_com = c } -> c.atomicity
	| Complex_call _ -> NonAtomic
	| Answer_and_jump _ -> NonAtomic
	| If(_,c1, c2) -> c1.atomicity + c2.atomicity
	| Seq (c1,c2) -> c1.atomicity + c2.atomicity
	| _ -> (*FIXME *) Atomic

      in
      {
      descr = descr ;
      loc = loc;
      atomicity = atomicity;
    }

    let rec convert res = function
	a::l -> convert (List.rev_append !a res) l
      |	[] -> res



    module Std = 
      struct
	let (@@) c1 c2 = 
	  match c1, c2 with
	    {descr=Nop}, c2 -> c2
	  | c1, {descr=Nop} -> c1
	  | _  -> 
	      mk ~loc:(GGStdpp.cat c1.loc c2.loc)  (Seq(c1, c2))
	let nop = mk Nop
      end
	
    include Std

    let rec flatten = function
	[] -> mk  Nop
      | a :: [] -> a
      | a :: l ->  a @@ (flatten l)

    let mk_assert e =
      mk (If(GGExp.mk (GGExp.Monop   (GGExp.Not, e)) GGType.Bool,
	     mk Abort, nop))

  end

module Symbol = 
  struct 
    include ProtoSymbol

    let to_exp ?(loc=GGStdpp.dummy_loc) ({thetype = t; lvalue=lvalue} as v) = 
      GGExp.mk ~loc ~lvalue (GGExp.Var v) t

    let __tag = ref 0

    let temp_name () =
      incr __tag;
      "__tmp_" ^ (string_of_int !__tag) 

    let mk_exp ?name  t = 
(*      Stamp.print_stamp ();*)
      let name = match name with 
	Some x ->  x
      |	None -> "tmp" in
      let v = mk 
	  ~alpha: true
	  ~lvalue: true ~name t in
(*      Printf.eprintf "mk_exp %s %d\n" n (Stamp.hash id);*)
      (to_exp v,  GGCommand.mk  (GGCommand.Let v))

    let build_temp_dec t =
      let v = mk
	  ~alpha: true
	  ~lvalue: true 
	  ~name: "tmp"
	  t 
      in
      GGCommand.mk (GGCommand.Let v), v

    let save_exp ?(name="tmp") e =
      let (@@) = GGCommand.(@@) in
      let t = e.GGExp.thetype 
      and loc = e.GGExp.loc in
      let v = mk ~alpha: true ~lvalue: true ~name t in
      let var = GGExp.mk ~loc (GGExp.Var v) t 
      in
      (GGCommand.mk ~loc  (GGCommand.Let v)) @@
      (GGCommand.mk  ~loc (GGCommand.Assign (var, e))), var


  end


module GGFonction =
  struct 
    type  t = 
	{
	 loc : location;
	 rettype :  GGType.t ;
	 thetype : GGType.t ;
	 name : string ;
	 args : (string * GGType.t * Stamp.t) list ;
	 mutable command :  GGCommand.t ;
	 tag : Stamp.t;
	 atomic : bool;
       }
    let mk ?(loc=dummy_loc)  ?(tag=Stamp.mk ()) atomic rettype name args coms = 
      let arg_type = List.map (fun (a,b,c) -> (a,b)) args in
      let thetype = GGType.Function(rettype, arg_type) in
      {
       loc = loc;
       rettype = rettype;
       thetype = thetype;
       args = args;
       command = coms;
       tag = tag;
       name = name;
       atomic = atomic;
     } 
  end

(* this module record the corresponding string description for each enum value.

For example, with this:

    Var x : Enum { Bla, Foo } = Bla;
    log("x = ", x);
    
    we will be able to log
      >   x = Bla
 *)

module GGProg =
  struct

    type symbol_extern = 
	{
	 name : string;
	 cname : string;
	 thetype : GGType.t;
	 complex : bool;
       }

    type  t = 
	DecType of GGTypeDecl.t
      |	Function of GGFonction.t
      |	Extern of symbol_extern
      |	Import of string
      |	Const of string * GGType.t * GGExp.t
      | LetGlobal of Symbol.t * string * GGCommand.t
      |	Nothing
  end

module Abbreviation = 
  struct
    module C = GGCommand
    module E = GGExp
    module T = GGType
  end
  
