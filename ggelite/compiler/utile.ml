

let internal_debug = ref true
let debug_code = ref false

module GGStdpp =
  struct
    open Lexing
    open Camlp4.PreCast
    type localisation = Loc.t
    
    let dummy_loc = Loc.ghost
    
    let mk (start_pos: Lexing.position) (end_pos: Lexing.position) = Loc.merge (Loc.of_lexing_position start_pos) (Loc.of_lexing_position end_pos)
    let cat loc1 loc2 = Loc.merge loc1 loc2
    exception Exc_located of localisation * exn
    let raise_with_loc (loc:localisation) (e:exn) = raise (Exc_located(loc, e))

    let string_of_loc  loc = 
        let name = Loc.file_name loc
        and pos = (Loc.start_pos loc).pos_cnum
        and pos2 = (Loc.stop_pos loc).pos_cnum in
        "\"" ^ name ^ "\" (" ^ (string_of_int pos ) ^ ", " ^ (string_of_int pos2) ^ ")"

    (* only used to transmit the loc between lexer and parser.  This is a shame we have to go through a global in caml *)
    let loc_ref = ref dummy_loc

    let localized_msg = function  (loc:localisation) ->
        let fileName = Loc.file_name loc
        and lineNum = (Loc.start_pos loc).pos_lnum
        and start_col = (Loc.start_pos loc).pos_cnum
        and end_col = (Loc.stop_pos loc).pos_cnum in
    (*  Printf.printf "Erreur pos abs %d end %d\n" b e;*)
      Printf.sprintf "File \"%s\", line %d, characters %d-%d:" fileName lineNum start_col end_col

  end

let string_of_loc = GGStdpp.string_of_loc
let warnings:(GGStdpp.localisation*string) list ref  = ref []


let failwithloc loc s = GGStdpp.raise_with_loc loc (Failure s)

let warn_with_loc (loc:GGStdpp.localisation)     (s:string) = 
   warnings := (loc,s) :: !warnings

let debug ?loc s = 
  if !internal_debug then
    match loc with
    | Some loc ->
	warn_with_loc loc ("DEBUG: " ^ s)
    | None ->
	Printf.eprintf "DEBUG: %s\n" s; flush stderr

let warn ?loc (s : string) =
  match loc with
    Some x ->  warn_with_loc x s
  | None -> Printf.printf "warning: %s\n" s


let split_option =   function
    Some (a,b) -> Some a, Some b
  | None -> None,None

let rec merge_option = function
    Some a :: l -> a :: merge_option l
  | None :: l -> merge_option l
  | [] -> []


let compiler_bug (s : string) =
  failwith s


module Stamp =
  (struct 
    type t = int

    let count = ref 0

    let mk () = 
      let _ = incr count in
      !count

    let compare (x:int) (y:int) = Pervasives.compare x y
    
    let equal x y = x = y

    let hash (x:t) = x

    let print_stamp () = Printf.eprintf "current val = %d\n" !count

    let fresh_name ?(tag = mk ()) ?(prefix="") () = 
      "__" ^ prefix ^ "_" ^ (string_of_int tag)
  end : 
     sig
       type t 

       val mk : unit -> t
       val compare : t -> t -> int
       val hash : t -> int
       val equal : t -> t -> bool
       val print_stamp : unit -> unit
       val fresh_name : ?tag: t -> ?prefix: string -> unit -> string
     end
  )

module StampMap =
  struct
    include Map.Make (Stamp) 

    let replace key t map =
      add key t (remove key map)
  end

module StringMap = Map.Make (
  struct 
    type t = string
	  
    let compare = Pervasives.compare
  end
 )
 
module Log =
    struct
        let printf x = Printf.ksprintf (fun res -> prerr_endline res) x
        
        let crash x = Printf.ksprintf (fun res -> prerr_endline res; exit 2) x
    end
    

let insert_space lvl = 
  String.make lvl ' '

let retWith lvl =   "\n"^(insert_space lvl)

let ggconcat lvl f s =
  let rec ggconcat_more = function
      [] -> ""
    | b::m -> let s' = 	s^(f lvl b) in
      s' ^ (ggconcat_more m)
  in function
      [] -> ""
    | a::l -> let s' = (f lvl a) in
      s' ^ (ggconcat_more l)

