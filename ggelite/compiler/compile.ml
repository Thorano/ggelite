open Utile
open Ggtree
open Gencode
open GGCommand.Std

exception Not_a_variable

module Inline =
  struct
    type t = 
	{ loc : GGStdpp.localisation; 
	  thetype : GGType.t;
	  e : GGExp.t;
	  coms : GGCommand.t;
	  args : Stamp.t list;
	  tag : Stamp.t;
	  name : string;
	}

    let mk ?(loc=dummy_loc) ?(tag=Stamp.mk ()) (e,coms) args t name =
      {
       loc = loc;
       tag = tag;
       e = e;
       coms = coms;
       args = args;
       thetype = t;
       name = name;
     } 

    module Exp = 
      struct
	open GGExp

	let rec eval env (e : GGExp.t) = 
	  let eval e = eval env e in
	  {e with
	   descr = match e.descr with
	     Int _
	   | No_Exp
	   | Bool _
	   | String _
	   | Nil 
	   | Symbol _
	   | Float _ as x -> x
	   | Var v as x ->
	       let id = v.Symbol.tag in
	       begin
		 try
		   (Hashtbl.find env id).descr
		 with
		   Not_found -> 
		     x
	       end
	   | Function (f,l) -> Function(eval f, List.map eval l)
	   | Binop(op, e1, e2)  -> Binop(op, eval e1, eval e2)
	   | Monop(op, e) -> Monop(op, eval e)
	   | ArrayAccess (e1,e2) -> ArrayAccess(eval e1, eval e2)
	   | StructAccess (e1, name) -> StructAccess(eval e1, name)
	   | MessageCall (e1, name, None) -> 
	       MessageCall(eval e1, name, None)
	   | MessageCall (e1, name, Some(e2, l)) ->
	       MessageCall(eval e1, name, Some(eval e2, List.map 
						(fun (n,e) -> (n, eval e)) l))
		
	  | Address e -> Address (eval e)
	  | ArrayBuild _
	  | StructBuild _ -> assert false
	 } 
      end

    module Com = 
      struct 
	open GGCommand

	let rec eval ((env,env_label,env_listen) as blo) c =
	  let eval l = (eval blo) l 
	  and evale = Exp.eval env 
	  and lazy_map f table a = 
	    try Hashtbl.find table a with
	      Not_found ->
		let res = f a in
		Hashtbl.add table a res;
		res  in 
	  let map_stamp id = lazy_map (fun a -> Stamp.mk()) env_label id in
	  let map_var v =
	    { v with Symbol.tag = map_stamp v.Symbol.tag} in
	  let eval_event = function
	      {
	       source_event = s_e;
	       message_event = message_event;
	       args_event = args_event;
	       com_event = com_event;
	       cond_event = c_e;
	     } ->
	       let blu_map =  function
		   Some (e) -> Some (evale e)
		 | None -> None
	       in 
	       let s_e = blu_map s_e
	       and c_e = match c_e with
		 Some (e,l) -> Some (evale e, eval l)
	       | None -> None
	       and map_args (s,t,id) = (s,t, map_stamp id)
	       in
	       {
		source_event = s_e;
		message_event = message_event;
		args_event = List.map map_args args_event;
		com_event = eval com_event;
		cond_event = c_e
	      }
	  and eval_timer = function
	      	{
		 timer_name = timer_name;
		 timer_msg_name = priv_msg_name;
		 timer_com_list = priv_com_list;
		 timer_used = priv_used;
		 timer_id =  priv_id;
		 timer_args = timer_args;
	       } ->
		 {
		  timer_name = timer_name;
		  timer_msg_name = priv_msg_name;
		  timer_com_list = eval priv_com_list;
		  timer_used = priv_used;
		  timer_id =  map_stamp priv_id;
		  timer_args = timer_args;
		}  
	  and eval_priv = function
	      { 
		prv_name = prv_name;
		prv_args = prv_args;
		prv_com = prv_com;
		prv_rettype = prv_rettype;
	      } ->
		let prv_args = List.map 
		    (fun v -> map_var v) prv_args in
		{ 
		  prv_name = prv_name;
		  prv_args = prv_args;
		  prv_com = prv_com;
		  prv_rettype = prv_rettype 
		}
	  in 
(*FIXME: what's the fuck is this ?
*)
	  let map_listen li = lazy_map eval_timer env_listen li in

	  let eval_listen = function
	      {
	       listen_com = coms;
	       glb_msg_list = ev_list;
	       timer_msg_list = timer_list;
	       prv_msg_list = prv_msg_list;
	       listen_id = listen_id;
	       to_erase = id_list;
	       clean_after = clean_after} ->
		 {
		  listen_com = eval coms;
		  glb_msg_list = List.map eval_event ev_list;
		  timer_msg_list = List.map map_listen timer_list;
		  prv_msg_list = List.map eval_priv prv_msg_list;
		  listen_id = map_stamp listen_id;
		  to_erase = List.map map_stamp id_list;
		  clean_after = List.map map_stamp clean_after;
		} 
	  in
	  {c with 
	   descr = match c.descr with
	     If (e, c1, c2) -> If(evale e, eval c1, eval c2)
	   | Expr e -> Expr( evale e)
	   | Assign (e1,e2) -> Assign (evale e1, evale e2)
	   | Complex_call (e1, args, id) ->
	       Complex_call( evale e1, List.map evale args, id)
	   | Let v ->
	       let oldid = v.Symbol.tag in
	       let newid = Stamp.mk () in
	       let name = Stamp.fresh_name () in
	       let v = { v with Symbol.tag = newid ; Symbol.name = name} in
(*	       Printf.eprintf "reecrit dec %s %d, newid %d\n"
		 s (Stamp.hash id) (Stamp.hash newid);*)
	       let newexp = Symbol.to_exp v in
	       let () = Hashtbl.add env oldid newexp in
	       Let v
	   | CheckObject (e,t) -> CheckObject( evale e,t)
	   | Listen lb -> Listen (eval_listen lb)
	   | SetNotVar (v, s) -> 
	       let v = { v with Symbol.tag = map_stamp v.Symbol.tag} in
	       SetNotVar(v, s)
	   | Set_timer (s,e,pm) ->  Set_timer(s, evale e, map_listen pm)
	   | Clean l -> Clean (List.map map_stamp l)
	   | Abort -> Abort
	   | BlockToPop (lst) -> 
	       let lst =  List.map 
		   begin 
		     fun a -> ref (List.map map_stamp !a)
		   end
		   lst in
	       BlockToPop(lst)
	   | Reschedule_timer (e,pm) -> 
	       Reschedule_timer(evale e, map_listen pm)
	   | (Wait s) as x -> x
	   | Jump (id,id1) -> Jump (map_stamp id, map_stamp id1)
	   | Label id -> Label (map_stamp id)
	   | Seq(c1, c2) -> 
	       let c1 = eval c1 in
	       Seq(c1, eval c2)
	   | Nop -> Nop
	   | Answer_and_jump _
	   | Return_Message _
	   | Destroy_Job _
	   | Return _ -> GGStdpp.raise_with_loc c.loc
		 (Failure "return not allowed in inline function")
	 } 
      end

	

    let eval f (args : GGExp.t list) =
      let env = Hashtbl.create 5
      and env_label = Hashtbl.create 5
      and env_listen = Hashtbl.create 5
      in
      let () = 
	try 
	  List.iter2 (fun id exp -> Hashtbl.add env id exp) f.args args
	with
	  Invalid_argument s -> failwith "impossible"
      in
      let l = Com.eval (env, env_label, env_listen) f.coms in
      (Exp.eval env f.e, l)
  end


module EnvironmentSimple =
  struct
    open Operator
      
      (* List of system variable attached to the current listening block.
	 When the control flow exits such a block.  We need to 
	 clear those variables.
       *)
    type answer = HasAnswer | NoAnswer
    type handler_data = 
	{
	 mutable hd_do_continue : bool;
	 mutable hd_do_break : bool;
	 hd_id : Stamp.t;
	 hd_listening_var : Symbol.t;
	 hd_type : answer;
	 mutable hd_answer_type : GGType.t option;
       } 
    type function_block =
	{
	 mutable use_self : GGStdpp.localisation option;
	 return_type : GGType.t;
	 fn_id : Stamp.t;
	 fn_return_var : GGExp.t option;
       }

    type block_type =
	ListeningBlock of (Stamp.t list ref)
      |	CommonBlock of Stamp.t
      |	HandlerBlock of handler_data
      |	LoopBlock of Loop.t
      |	FunctionBlock of function_block

    type break = Sbreak | Snobreak

    type handler_state =
	{ 
	  hs_break : break;
	  hs_answer : (GGExp.t * GGCommand.t) option
  }

    module StringSet = Set.Make(String)

    type t =
	{
        (* map method name to possible object type.
           for objective C class, we add all the method we see anywhere.
           for complex function, we add all the local "ears"
        *)
         methods : ( GGType.objet list ) StringMap.t;
	 symbols : Symbol.t StringMap.t;
	 types : GGTypeDecl.t StringMap.t;
	 operators : Operator.t list ref Operator.OpMap.t;

	 (* get the timer data from the message name: *)
	 timer_msg : (string, GGCommand.timer_msg_data) Hashtbl.t list;

	 (* get the timer data from the timer name: *)
	 timers : (GGCommand.timer_msg_data) StringMap.t;
	 pseudo_functions : (t -> GGStdpp.localisation -> GGExp.t list -> 
	   GGExp.t * GGCommand.t) StampMap.t;
	 enums : GGType.t StringMap.t;
	 name_func : string option;

	 (* list of system  variables (timers and block listening control)
	    that are under the scoping of the current 
	    environment and need to be destroy in case of an exception
	  *)
	 to_erase : (Stamp.t list ref) list;

	 (* each block has a stamp...
	  *)
	 block_stack : (block_type * Stamp.t) list;
	 handler_state : handler_state StampMap.t;
	 unreachable_code : bool;

	 (*imported modules*)
	 imported : StringSet.t
       } 

    let empty = 
      { 
        methods = StringMap.empty;
	symbols = StringMap.empty;
	types = StringMap.empty;
	operators = Operator.OpMap.empty;
	timer_msg = [];
	timers = StringMap.empty;
	pseudo_functions = StampMap.empty;
	enums = StringMap.empty;
	name_func = None;
	to_erase = [];
	block_stack = [];
	handler_state = StampMap.empty;
	unreachable_code = false;
	imported = StringSet.empty;
      } 
      
    (* will map enum to the string ivar *)

    let enums_to_dump : (Ggtree.GGType.EnumString.t, string) Hashtbl.t = Hashtbl.create 10

    let add_methods env (methodList:string list) (obj:GGType.objet) = 
      let rec sub_add map = function
         method_name :: l ->
            let map = 
            try  
             let objects = StringMap.find method_name map in
             let objects = obj::objects in
             StringMap.add method_name objects map
            with Not_found -> StringMap.add method_name [obj] map
            in
            sub_add map l
        | [] -> map
      in
      { env with methods = sub_add env.methods methodList }
      
    let priv_add_enum enum = 
      let name = Stamp.fresh_name ~prefix: "enum_string_" () in
      Hashtbl.add enums_to_dump enum name;
      name
      
    let get_strings_enum enum = 
        try Hashtbl.find enums_to_dump enum with
        Not_found -> priv_add_enum enum

    let enums_list () = Hashtbl.fold (fun key v o -> (v,key) :: o) enums_to_dump []

    let add_type d env = 
      let newenv = {env with types = StringMap.add 
		      (fst d.GGTypeDecl.descr) d env.types}
      in (newenv, [GGProg.DecType d])


    open GGFonction

    let has_break hs = 
      match hs.hs_break with
	Sbreak -> true
      | Snobreak -> false
      
    let add_function f env = 
      let complex = not f.atomic in
      let name = 
	if complex then
	  "Complex_" ^ f.name 
	else 
	  f.name
      in
      let v = Symbol.mk ~tag: f.tag ~lvalue: false ~complex ~alpha: false ~name
	  f.thetype in
      let newenv = 
	{env with 
	 symbols = (StringMap.add f.name  v  env.symbols ) 
       }
      in
      (newenv, [GGProg.Function f])

    open Inline

    let add_erase env id =
      let rec sub = function
	  a :: l -> 
	    a := id :: !a;
	    sub l
	| [] -> ()
      and find = function 
	  (ListeningBlock a, _)  :: l -> a
	| (( CommonBlock _ 
	| LoopBlock _
	| FunctionBlock _ 
	| HandlerBlock _ ),_)  :: l -> find l
	| [] -> failwith "internal compiler error, add_erase"
      in 
      let refl = find env.block_stack in
      refl := id :: !refl;
      sub env.to_erase

    let add_inline f env =
      let name = f.name in
      let default env loc args = 
	match f.Inline.thetype with
	  GGType.Function(_,l) ->
	    let l = Operator.Default.check_apply_args loc 
		snd (fun x -> x) l args in
	    Inline.eval f l
	| _ ->  failwith "impossible"
      in
      let newenv = 
	{env with 
	 symbols = (StringMap.add name
		      (Symbol.mk ~name ~tag: f.tag ~lvalue: false ~alpha: true
			   f.thetype ) env.symbols ) ;
	 pseudo_functions = StampMap.add f.tag default env.pseudo_functions
	 }
      in
      (newenv, [])
	
	(* FIXME. should optimize this a lot!!!!*)

    exception JAITROUVE of Symbol.t

    let lookup_var_from_hash env id =
      try 
	StringMap.iter (fun _ var ->
	  if var.Symbol.tag = id then
	    raise (JAITROUVE(var))) env.symbols;
	raise Not_found
      with
	JAITROUVE var -> var


    let get_meta_func env  f =
      let need_split = function 
	  {GGExp.descr = GGExp.Var v} ->
	    v.Symbol.complex 
	| {GGExp.descr = GGExp.Symbol _ } -> false
	| _ -> true
      in


      match f.GGExp.thetype with
	GGType.Function (ret, l) -> 
	  let default  env loc args =
	    let args = Operator.Default.check_apply_args  loc
		snd (fun x -> x) l args in
	    let c,e = 
	      if need_split f then
		match f.GGExp.thetype with
		  GGType.Function(GGType.Unit,_) ->
		    let v = Symbol.mk
			~alpha: false
			~lvalue: false
			~name: "blasdf"
			GGType.Unit in
		    
		    (GGCommand.mk ~loc
		      (GGCommand.Complex_call 
			 (f, args, v))), 
		    (GGExp.mk ~loc GGExp.No_Exp GGType.Unit) 
		| GGType.Function _ -> 
		    let var = Symbol.mk ~alpha: false ~lvalue: false
			~name: "retval" ret in
(*		      let () = Printf.printf "need split %d\n" var in*)
		    let res = Symbol.to_exp var in
		    GGCommand.mk ~loc
		      (GGCommand.Complex_call(f, args, var)),res
		| _ -> failwith "impossible42"
	      else
		GGCommand.mk ~loc GGCommand.Nop, 
		GGExp.mk ~loc (GGExp.Function(f, args)) (ret)
	    in
	    e,c
	  in
	  begin
	    match f with
	      {GGExp.descr = GGExp.Var v} ->
		begin
		  try 
		    StampMap.find v.Symbol.tag env.pseudo_functions 
		  with
		    Not_found -> default
		end
	    | _ -> default
	  end
      | _ ->  GGStdpp.raise_with_loc (f.GGExp.loc)
	    (Failure ("expr is not of function type and so cannot be  applied"))

    type what_break =
	BreakingHandler of handler_data * (Stamp.t list ref list) * handler_data list
      | BreakingLoop of Loop.t * (Stamp.t list ref list) * handler_data list

    let  lookupBreakable loc   =  function
	{ block_stack = l } ->
	  let rec iter  res1 res2 = function 
	      (LoopBlock a, _) :: l -> BreakingLoop (a, res1, res2)
	    | (HandlerBlock a,_) :: l -> BreakingHandler (a, res1, res2)
	    | (ListeningBlock b,_) :: l -> iter (b :: res1) res2 l
	    | ((CommonBlock _  | FunctionBlock _ ),_) :: l -> 
		iter res1 res2 l
	    | [] -> GGStdpp.raise_with_loc loc 
		  (Failure ("No loop or handler to break"))
	  in
	  iter [] [] l

    let  lookup_loop loc   =  function
	{ block_stack = l } ->
	  let rec iter  res1  = function 
	      (LoopBlock a, _) :: l -> (a, res1)
	    | (ListeningBlock b,_) :: l -> iter (b :: res1)  l
	    | ((HandlerBlock _ | CommonBlock _  | FunctionBlock _ ),_) :: l -> 
		iter res1 l
	    | [] -> GGStdpp.raise_with_loc loc 
		  (Failure ("No loop to continue"))
	  in
	  iter []  l


    let lookup_rettype loc = function
	{ block_stack = l } ->
	  let rec iter  res1 res2 = function 
	      (FunctionBlock fnb,_) :: l -> (fnb, res1, res2)
	    | (ListeningBlock b,_) :: l -> iter (b :: res1) res2 l
	    | (HandlerBlock b,_) :: l -> iter res1 (b :: res2) l
	    | ((CommonBlock _  | LoopBlock _ ),_) :: l -> 
		iter res1 res2 l
	    | [] -> GGStdpp.raise_with_loc loc 
		  (Failure ("Not in a function declaration!!!"))
	  in
	  iter [] [] l


    let  lookupVar loc var env = 
      if var = "self" then
	let (fnb, _, _) =  lookup_rettype loc env  in
	fnb.use_self <- Some loc;
	let e = GGExp.mk ~loc (GGExp.Symbol "self->_process") (GGType.Id) in
	e
      else
	try
	  let v =  Symbol.to_exp ~loc (StringMap.find var env.symbols) in
(*	Printf.printf "lookup of %s of type %s\n" var 
   (GGType.to_C_type 0 "bluu" v.GGExp.thetype );*)
	  v
	with
	  Not_found -> 
	    try 
	      let enum = StringMap.find var env.enums in
	      GGExp.mk ~loc  (GGExp.Symbol var)   enum 
	    with
	Not_found -> 
		GGStdpp.raise_with_loc loc 
		  (Failure ("unbound variable " ^ var))

    let  lookupType loc var env = 
      try
	match (StringMap.find var env.types) with
	  {GGTypeDecl.descr = _, (GGType.Objet _ as x)} -> x
	| {GGTypeDecl.descr = _, x} -> GGType.Name(var,x)
      with
	Not_found -> 
            GGStdpp.raise_with_loc loc (Failure ("unbound type " ^ var))

    let  lookupOp loc op env = 
      try
	OpMap.find op env.operators
      with
	Not_found -> GGStdpp.raise_with_loc loc (Failure ("unknown operator " ^ 
							(GGExp.string_of_op op)))
	    
    let lookup_handler loc = function
	{ block_stack = l } ->
	  let rec iter res = function
	    | (HandlerBlock x,_) :: l -> (x, res)
	    | (ListeningBlock b,_) :: l -> iter (b :: res) l
	    | ((FunctionBlock _ | CommonBlock _ | LoopBlock _
		),_) :: l -> iter res l
	    | [] ->  failwithloc loc "not in handler"
	  in
	  iter [] l

    let lookup_state loc hd env  = 
      try
	StampMap.find hd.hd_id env.handler_state
      with
	Not_found ->
	  failwithloc loc "internal18"

    let  lookup_timer_msg loc name env =
      let rec sub_lookup = function
	  a :: l -> 
	    begin
	      try Hashtbl.find a name
	      with
		Not_found -> sub_lookup l
	    end
	| [] -> GGStdpp.raise_with_loc loc (Failure 
					    ("Undefined target message \"" ^
					     name ^ "\"."))
      in sub_lookup env.timer_msg

    let lookup_timers loc name env =
      try StringMap.find name env.timers with
	Not_found -> GGStdpp.raise_with_loc loc (Failure
						 ("Undefined timer name \"" ^
						    name ^ "\"."))

    open Abbreviation

    let save_symb ?loc name t env =
      let v = Symbol.mk ~lvalue: true ~alpha: true ~name t in
      let cdec = C.mk ?loc (C.Let v) in
      let assign = C.mk ?loc (C.Assign(Symbol.to_exp v, 
				       E.mk ?loc (E.Symbol name) t)) in
      let env = { env with symbols = 
		  StringMap.add name v env.symbols} in
      cdec @@ assign, env

  end



let compile_list l env = 
  let a,b = List.fold_left
      (fun (env, l) f -> let (res, env') = f env
      in (env', (res::l)))  (env, []) l in
  (a, List.rev b)





let typeFromDecType loc = function
    {GGTypeDecl.descr = (a,t)} -> (GGType.Name(a,t))



let abstract = function
  Some x -> fun env -> [x env]
  | None -> fun env -> []


let list_split l = List.fold_right (fun  (e,l) (a,b) -> (e::a,l::b))
     l ([], [])



let build_fun_call _f _args loc env =

  let args, coms = list_split (List.map (fun f -> f env) _args)
  and f,com = _f  in
  let coms = GGCommand.flatten coms in
  let (@@) = GGCommand.(@@) in

  match f.GGExp.thetype with
    GGType.Function (_, _) -> 
      begin
	try
	  let op = EnvironmentSimple.get_meta_func env f in
	  let e,com2 = op env loc args in
	  (e, com @@ coms @@ com2)
	with
	  GGStdpp.Exc_located _ as e -> raise e
	|	e -> GGStdpp.raise_with_loc loc e
      end
  | _ ->  GGStdpp.raise_with_loc (f.GGExp.loc)
	(Failure ("expr is not of function type and so cannot be  applied"))


include 
  struct 
    open Operator
    open GGType

    let (@@) = GGCommand.(@@)

    let rec lookupOp op l = function
	a :: l1 -> 
	  begin
	    match a with
	      { check_args = check } as x when check l -> x
	    | _ -> lookupOp op l l1
	  end
      |	[] -> raise Not_found

    let build_generic  op args loc env l =
      try
	let a = lookupOp op args !(EnvironmentSimple.lookupOp loc op env) in
	let e,n = a.rewrite loc args in
	(e, l @@ n)
      with
	Not_found -> GGStdpp.raise_with_loc loc
	    (Failure ("no matching version of " ^ 
		      (GGExp.string_of_op op) ^
		      " for the types : (" ^
		      (String.concat ", " 
			 (List.map 
			    (function {GGExp.thetype = t}->GGType.to_string 0 t)
			    args)) ^
		      ")"))

    let binop_expr op _x _y loc env = 
      let x,l = _x env 
      and y,m = _y env in
      let args = [x; y] in
      build_generic op args loc env (l @@ m)

    let monop_expr op _x loc env =
      let x,l = _x env in
      let t1 = x.GGExp.thetype in
      let arg = [x] in
      build_generic op arg loc env l
  end
				
  


include 
  struct
    open GGType
      
    let mk_op_Type t1 t2 t3 = Function (t3, ["",t1 ;"",t2])
  end



let check_type theType e = function
    {GGExp.thetype = t},_ as exp when GGType.isEqual t  theType -> exp
  | {GGExp.loc = loc},_ -> failwithloc loc e

let check_float (x: GGExp.t * GGCommand.t) = 
  check_type GGType.Float "float expected here" x
  
let check_bool = function
    {GGExp.thetype = t},_ as exp when GGType.isEqual t GGType.Bool -> exp
  | {GGExp.loc = loc},_ -> 
      GGStdpp.raise_with_loc loc (Failure ("Bool type was expected here"))

let check_int  t = check_type GGType.Integer "Integer was expected" t
  

let check_array = function 
    {
     GGExp.thetype = GGType.Array (_,t);
     GGExp.lvalue = lval 
   },_ as exp -> exp,t,lval
  | {GGExp.loc = loc},_ -> 
      GGStdpp.raise_with_loc loc (Failure ("expr is not of array type"))

let build_array_acces _x _ind loc env =
  let ind,l1 = check_int (_ind env) 
  and (x,l2),t,lval = check_array (_x env) in
  ( GGExp.mk ~loc (GGExp.ArrayAccess(x, ind)) (t) ~lvalue:true,
    l1@@l2 )
  
(*thus function returns a command and an expression.  The expression does 
   not have any side effect*)
let check_lvalue _e loc env = 
  let e = _e env in
  match e with
    {GGExp.lvalue = true},_  -> e
  | {GGExp.loc = loc},_ -> GGStdpp.raise_with_loc loc 
	(Failure ("expr is not an lvalue"))

  
module CompileLval = struct
  open Abbreviation
  open GGExp

    let default_assign e1 e2 loc =  C.mk ~loc (C.Assign(e1, e2))

    let save_struct ?(build=default_assign)  e1 l = 
      let rec iter c = function
	| (n,e) :: l ->
	    let loc = e.E.loc in
	    let e1 = E.build_field_access e1 n 
	    in
	    let e2 = e in
	    let c' = build e1 e2 loc in
	    iter (c @@ c') l
	| [] -> c in
      iter GGCommand.nop l

  let save_array ?(build=default_assign) e1 l =
    let rec iter n c = function
      | e :: l ->
	  let loc = e.E.loc in
	  let e1 = E.build_array_acces ~loc e1 n in
	  let e2 = e in
	  let c' = build e1 e2 loc in
	  iter (n+1) (c @@ c') l
      | [] -> c  in
    iter 0 GGCommand.nop l


    (* This function convert an expression to a pair (command, expr) where expr
       has no side effect and the same value as the argument*)
  let rec make e = 
    match e.descr with 
    | Int _
    | No_Exp
    | Bool _
    | String _
    | Nil 
    | Float _
    | Var _ 
    | Symbol _ -> nop,e
    | Function _
    | Binop _
    | Monop _
    | MessageCall _ -> Variable.save_exp e
    | ArrayAccess (e1, e2) ->
	let c2,e2 = make e2
	and c1,e1 = make e1 in
	c1 @@ c2, {e with descr =   ArrayAccess(e1, e2)}
    | StructAccess (e1, n) -> 
	let c,e1 = make e1 in 
	c, {e with descr = StructAccess(e1,n)}
    | Address e1 -> 
	let c, e1 = make e1 in
	c, {e with descr = Address e1}
    | ArrayBuild l ->
	let e,c = Variable.mk_exp ~name: "tmp_array" e.thetype in
	let c' = save_array e l in
	c @@ c', e
    | StructBuild l -> assert false
(*	let e,c = Variable.mk_exp ~name: "tmp_struct" e.thetype in
	let c' = save_array e l in
	c @@ c', e
*)
	

end
  
module Assign = struct
  open Abbreviation
  open GGCommand
  open GGType
  let rec build _e1 _e2 loc env = 
    let build e1 e2 loc =
      fst(build (fun x -> e1,nop) (fun x -> e2,nop) loc env) in
    let (@@) = GGCommand.(@@) in
    let e1,com1 = check_lvalue _e1 loc env
    and e2,com2 = _e2 env
    in
    if Gencode.Operator.Default.can_assign  e1 e2 then
      let com3, e1 = CompileLval.make e1 in
      let c2, e2 = 
	match e1.GGExp.thetype, e2.GGExp.thetype with
	| (Objet { contents =  Vrai { name = "NSArray" }}) as o,
	  Array({contents = Some n}, t) when GGType.is_object t ->
	    let c, e2 = CompileLval.make e2 in

	    let earr = 
	      let loc = e2.E.loc in
	      let class_array = 
		E.mk ~loc (E.Symbol "NSMutableArray") o in
	      E.mk ~loc (E.MessageCall(class_array, "arrayWithObjects",
				       Some(e2,
					    ["count", E.mk_int ~loc n])))
		o in
	    let c1, e2 = CompileLval.make earr in
	    c @@ c1, e2

	| _ -> nop, e2 in
      let prefix,e2 = 
	match e1.GGExp.thetype with
	  GGType.Objet (x) when !debug_code  ->
	    let c,e2 =  Variable.save_exp e2 in
	    c @@ (GGCommand.mk ~loc
		    (GGCommand.CheckObject (e2,x))), e2
	|	   _ -> GGCommand.nop, e2
      in
      (*special case of structure and arrays*)
      let c = 
	match e2.GGExp.descr with
	| GGExp.StructBuild l -> 
(*	    let rec iter c = function
	      | (n,e) :: l ->
		  let e1 = GGExp.mk ~loc ~lvalue: true (GGExp.StructAccess(e1,n)) 
		      (GGType.type_of_label e1.GGExp.thetype n)
		  in
		  let e2 = e in
		  let c' = build e1 e2 loc
(*		      (fun env -> e1,nop)
		      (fun env -> e2,nop)
		      loc env *)in
		  iter (c @@ c') l
	      | [] -> c in
	    iter GGCommand.nop l*)
	    CompileLval.save_struct ~build e1 l
	| GGExp.ArrayBuild l ->
	    CompileLval.save_array ~build e1 l
	| _ -> (GGCommand.mk ~loc  (GGCommand.Assign(e1, e2))) in
      c2 @@ com1 @@ com2 @@ com3 @@ prefix @@ c,env
    else 
      GGStdpp.raise_with_loc loc 
	(Failure ("incompatible type in assignement: " ^
		  (GGType.to_string 0 e1.GGExp.thetype) ^ " " ^
		  (GGType.to_string 0 e2.GGExp.thetype)))

end

(* we need to type object message dispatch, otherwise it will be bad
*)

module Message = 
  struct
    open GGType
    open ProtoTree.GGInterface

    let rec lookup_msg name opt obj = 

      let  equal_meth n o = 
	name = n &&
	match opt,o with
	  Some (_, l), Some(_,l') -> 
	    begin
	      try
		List.for_all2 
		  (fun (s,_) (s',_) -> s = s') l l'
	      with
		Invalid_argument _ -> false
	    end
	| None, None -> true
	| _ -> false
      in

      let search l = 
	List.find 
	  (function
	      {first_component = n;
	       next_components = o} -> equal_meth n o) l
      in

      match obj with
	Some {contents = Vrai {methods_i = m_i;  methods_c = m_c;  herite = father;}} ->
	  begin
	    try
	      search m_i
	    with
	      Not_found -> 
		begin
		  try
		    search m_c
		  with
		    Not_found ->
		      lookup_msg name opt father
		end
	  end
      |	_ -> raise Not_found

	    
    let compile loc  msg name o opt =

      match msg with
	{method_ret_type = tr;
	 next_components = opt2} ->
	   let opt' = 
	     match opt, opt2 with
	       Some (e,le), Some(t,lt) ->
		 let e' = Operator.Default.rewrite_arg t e in
		 let le' = List.map2 (fun (n,e) (_,t) -> 
		   (n,Operator.Default.rewrite_arg t e))
		     le lt in
		 Some(e',le')
	     | None,None -> None
	     | _ -> failwith "impossible547"
	   in
	   GGExp.mk ~loc (GGExp.MessageCall(o,name,opt')) tr


    let build _o name opt loc env =
      let canonize = function
	  Pointer (Objet _ as t)  -> t
	| t -> t
      in

      let o,com1 =   (_o env) in
      let  opt',com2 =
	match opt with
	  Some (e,_l) ->
	    let e', c2 = e env in
	    let l = List.map (function a,b -> a,(b env)) _l in
	    let l1,com = List.fold_right (fun (a,(b,c)) (l,m) -> ((a,b)::l,c::m)) 
		l ([],[]) in
	    Some(e', l1), c2 @@ (GGCommand.flatten com)
	| None -> None, nop in
      
     let obj = 
       begin
          match canonize (o.GGExp.thetype) with
            Id -> 
              (* XXX lookup in the untype method environment *)
              begin
                  let lookup_name = ProtoTree.GGExp.full_name_from_message name opt in
                  try 
                    let obj_class = List.hd (StringMap.find lookup_name env.EnvironmentSimple.methods) in
                    ref (Vrai (obj_class))
                  with Not_found -> 
                    (GGStdpp.raise_with_loc loc 
                          (Failure 
                             ("Could not find a matching message for " ^ lookup_name)))  
              end
    (*	  ( GGExp.mk ~loc (GGExp.MessageCall(o,name,opt')) (GGType.Id),
                com1 @@ com2 ) *)
          | Objet obj -> obj
          | _ ->
              (GGStdpp.raise_with_loc o.GGExp.loc 
                (Failure("method call with something wich is not of object type")))
        end
      in
      begin
	    try
	      let m = lookup_msg  name opt' (Some obj) in
	      let e = compile loc m name o opt' in
	      (e, com1 @@ com2)
	    with
	      Not_found ->
		GGStdpp.raise_with_loc loc 
		  (Failure 
		     ("Could not find a matching message for the type " ^
		      (GGType.to_string 0 o.GGExp.thetype)))
	    | GGStdpp.Exc_located _ as e -> raise e
	    | e -> GGStdpp.raise_with_loc loc e
	  end


  end



let build_field_access _x name loc env =
  let x,com = _x env in
  match x with 
    {GGExp.thetype = t ;
     GGExp.lvalue = lval;
     GGExp.loc = locx
   } -> 
     let build l =
       let (s,t) = List.find (fun (s,_) -> Utile.Log.printf "field = %s" s;  s = name) l in
       (GGExp.mk ~loc (GGExp.StructAccess(x, s)) ~lvalue:lval  
	  t,
	com) in
      match GGType.effectiveType t with
	GGType.Struct l ->
	  begin
	   try build l
	   with
	     Not_found -> GGStdpp.raise_with_loc locx
		 (Failure ("struct '"^
		  (GGType.to_C_type 0 "" t)^
		  "' has no member named '"^name^"'"))
	  end
      |	GGType.Objet o ->
	  let rec find_field = function
	      Some{contents = 
		   GGType.Vrai { GGType.var_i = m_i;
			  GGType.name = n;
			  GGType.herite = father;
			}} ->
		      
			  begin
			    try build m_i
			    with
			      Not_found -> 
				find_field father
			  end
	    | _ ->
		GGStdpp.raise_with_loc locx
		  (Failure ("object '"    ^
			    "' has no ivar named '"^name^"'"))
	  in
	  find_field (Some o)

      | _ -> GGStdpp.raise_with_loc locx
		 (Failure "expr is not a structure")

module ListenBock =
  struct
    open GGCommand
    open EnvironmentSimple
    
    type t = 
	TimerMessage of string * (GGCommand.t list)
      |	PublicMessage of GGCommand.glb_msg_data
      | PrivateMessage of prv_msg_data

    type echap_type =
	Resume | Break | Both | Unknown
    type sure =
	Safe | Unsafe

    let merge_echap a b = 
      match a,b with
	x,y when x = y -> x
      |	Resume,Break
      |	Break,Resume 
      |	Both,_
      |	_,Both -> Both
      |	Unknown,x
      |	x,Unknown -> x
      |	_ -> compiler_bug "iciic"

    let merge_seq loc (a,sa) (b,sb) = 
      let ans = merge_echap a b in
      let safe =    match sa,sb with
      |	Safe, Safe -> Safe
      |	Safe, Unsafe ->
	  warn_with_loc loc "strangety here";Unsafe
      |	Unsafe, Safe -> Safe
      |	Unsafe, Unsafe -> Unsafe
       in
      ans,safe


    let merge_par loc (a,sa) (b,sb) = 
      let ans = merge_echap a b in
      let safe =    match sa,sb with
      |	Safe, Safe -> Safe
      |	Safe, Unsafe 
      |	Unsafe, Safe 
      |	Unsafe, Unsafe -> Unsafe
      in
      ans,safe

    let is_block_parent env id_block =
      let rec sub = function
	| (_,id) :: l when id = id_block -> true
	| _ :: l -> sub l
	| [] -> false
      in
      sub env.block_stack

    let analyse_echap env =
      let is_block_parent = is_block_parent env in
      let rec analyse c = match c.descr with
      |	If (e,c1, c2) -> 
	  merge_par c.loc (analyse c1) (analyse c2)
      |	Expr _
      |	Assign _
      |	Set_timer _
      |	Reschedule_timer _
      | Nop
      |	Label _
      |	Let _
      |	Complex_call _
      |	SetNotVar _
      |	Clean _
      |	CheckObject _
      |	BlockToPop _
      |	Listen _ 
      | Destroy_Job _
      | Abort
      |	Wait _ -> Unknown,Unsafe
      | Answer_and_jump (_,_,id_bl)
      |	Jump (_, id_bl) ->
	  if is_block_parent id_bl then
	    Break,Safe
	  else
	    Unknown,Unsafe
      |	Return _ -> Break,Safe
      | Return_Message _ -> Resume,Safe
      | Seq (c1, c2) -> merge_seq c.loc (analyse c1) (analyse c2)
      in
      analyse

	    
    let recompile_handler destroy env c =
      let rec sub c = match c.descr with
	If(e, c1, c2) ->
	  {c with descr = If(e, sub c1, sub c2)}
      |	Seq(c1, c2) ->
	  (*sanity check*)
	  if c1.atomicity =   NonAtomic then
	    failwithloc c1.loc "non atomic command occurs before I know where to branch";
	  let c1 =   match analyse_echap env c1 with
	  | _,Safe -> warn_with_loc c1.loc "this should not happen"; c1
	  | _, Unsafe -> sub c1
	  and c2 = match analyse_echap env c2 with
	  | _,Unsafe 
	  | Both,Safe  -> sub c2
	  | Unknown, Safe -> compiler_bug  "???" 
	  | Resume, Safe -> c2
	  | Break, Safe -> destroy @@ c2
	  in
	  {c with descr = Seq(c1, c2)}
      |	x when c.atomicity = Atomic -> c
      |	x (* when not atomic...*) ->
	  failwithloc c.loc 
	    "non atomic command occurs before I know where to branch"
      in

      match analyse_echap env c with
	_,Unsafe ->
	  failwithloc c.loc  ( "can't figure all the path of this handler " ^
			       (GGCommand.to_string c))
      |	Resume, Safe -> c
      |	Break, Safe -> 
	  destroy @@ c
      |	Both, Safe -> sub  c
      |	Unknown, Safe -> compiler_bug  "humpf?"

    let compile loc c lg lt lp env = 
      let table = Hashtbl.create 5  (*hash table related to the name of the timer*)

      and listen_var = Symbol.mk 
	  ~name: "block" 
	  ~alpha: true 
	  ~lvalue: true (GGType.mk_objet "ListeningBlock") in
      let listen_id = listen_var.Symbol.tag in
      let newenv ans env =
	let handler_data =
	  { hd_do_continue = false;
	    hd_do_break = false;
	    hd_id = Stamp.mk ();		
	    hd_listening_var = listen_var;
	    hd_type = ans;
	    hd_answer_type = None
	  }	in
	let env =
	  {  env with 
	     block_stack = 
	     (HandlerBlock handler_data, handler_data.hd_id)  :: 
	     env.block_stack;
	     handler_state = StampMap.add handler_data.hd_id
	       { hs_break = Snobreak;
		 hs_answer = None } env.handler_state
	   } 
	in env,handler_data
      in

      let rec sub_compile_priv env destroy res = function
	| a :: l ->
	    let env, hd = newenv HasAnswer env in
	    let (name, args, rettype, coms) = a env in
	    let coms = recompile_handler destroy env coms in
	    let x = {
	      prv_name = name;
	      prv_args = args;
	      prv_com = coms;
	      prv_rettype = rettype;
	    }
	    in
	    sub_compile_priv env destroy  (x :: res) l
	| [] -> res
      in
      let rec sub_compile_timer env destroy res = 
	function 
	  |  a :: l -> 
	      let env,_ = newenv NoAnswer env in
(*	    match hd with  
   { hd_do_break = false;hd_do_continue = true} ->*)
	      let (s, c) = a in
	      let bl = Hashtbl.find table s in
	      let env, c1 = match bl.timer_args with
		(n,t,loc) :: l  ->
		  if not (GGType.isEqual (GGType.Id) t) then
		    failwithloc loc "first arg is the continuation, thus it should be of type Id";
		  begin
		    match l with
		    | _ :: (_,_,loc) :: _ ->
			failwithloc loc "only the return value is expected"
		    | _ -> ()
		  end;
		  let rec iter com env = function
		    | (n, t, loc) :: l ->
			let c,env = save_symb ~loc n t env in
			iter (c @@ com) env l
		    | [] -> (env, com) in
		  iter nop env bl.timer_args 
	      | [] -> env, nop
	      in
	      let c = c env in
	      let loc = c.loc in
	      begin
		match bl.timer_used, bl.timer_args with 
		| ContReturn, _ :: _ -> ()
		| ContReturn, [] ->
		    failwithloc loc "The handler is for a continuation return, it should have arguments"
		| TimerB, [] -> ()
		| TimerB, _ -> failwithloc loc "The handler is for a timer, it should NOT have any argument"
		| Nope,_ -> warn_with_loc loc "This handler is never used"
	      end;
	      let c = recompile_handler destroy env c in
	      bl.timer_com_list <- c;
	      sub_compile_timer env destroy (bl :: res) l
	  | [] -> res 
      in
      let rec sub_compile_notif env destroy res1 res2 = function
	|  a :: l -> 
	    let env,hd = newenv NoAnswer env in
	    let (cond, from, name, c, args),copt = a env in
	    let c = recompile_handler destroy env c in
	    let bl = 
	      {
	       cond_event = cond;
	       source_event = from;
	       message_event = name;
	       com_event = c;
	       args_event = args} in
	    let res2 =   match copt with
	      Some c -> c @@ res2
	    | None -> res2 in
	    sub_compile_notif env destroy (bl :: res1) res2 l
	| [] -> (res1, res2)
      in

      let rec first_pass res env  = function
	| a :: l ->
	    let (s, arg, f) = a env in
	    let bl = { timer_msg_name = s;
		       timer_name = None;
		       timer_com_list = nop;
		       timer_used = Nope;
		       timer_id = Stamp.mk ();
		       timer_args = arg;
		     } in
	    Hashtbl.add table s bl;
	    let res = (s,f) :: res in
	    first_pass  res env l
	  | [] -> res
      in
      let lt = first_pass [] env lt in
      let to_erase = ref [] 
      and clean_after = ref [] 
      in
      let destroy = mk ~loc   (GGCommand.Destroy_Job(listen_var.Symbol.tag, to_erase))  in

      let dec = GGCommand.mk ~loc (Let listen_var) in

      let newenv = { env with 
		     timer_msg = table :: env.timer_msg;
		     to_erase = to_erase :: env.to_erase;
		     block_stack = (ListeningBlock clean_after, listen_var.Symbol.tag)
				    :: env.block_stack
		   } in
      let () = EnvironmentSimple.add_erase newenv listen_var.Symbol.tag
      in
      let c,_ = c newenv in
      let env_for_handler = 
	let envtimer = 
	  Hashtbl.fold 
	    (fun key bl envtimer ->
	      match bl.timer_name with
	      | Some x -> StringMap.add  x bl envtimer
	      | None -> envtimer) table env.timers in
	{ env with timers = envtimer }
      in
      let l2 = sub_compile_timer env_for_handler destroy [] lt in
      let l3 = sub_compile_priv env_for_handler destroy [] lp in
      let (l1,c_prefix) = sub_compile_notif env_for_handler destroy [] nop lg in


      if c.GGCommand.atomicity = Atomic then
	Printf.eprintf "bizarre\nla commande %s est atomique" (string_of_loc c.loc);
(*	  (fst c.loc) (snd c.loc); *)
      GGCommand.flatten 
	[dec;
	 c_prefix;
	 GGCommand.mk ~loc 
	   (Listen({listen_com = c;
		    glb_msg_list = l1;
		    prv_msg_list = l3;
		    timer_msg_list = l2;
		    listen_id = listen_id;
		    GGCommand.to_erase = !to_erase;
		    clean_after = !clean_after;
		  })
	   );
	 GGCommand.mk ~loc (Label listen_id); 
	 GGCommand.mk ~loc (Wait (Stand_Wait "NOP"))
       ],env




    let add_timer loc msg_name _exp _timer_name env =
      let timer_name = 
	let prefix = "_timer_" in
	match _timer_name with
	  Some x ->  x
	| None -> prefix
      in 
      let bl = lookup_timer_msg loc msg_name env in
      let (exp,com) = check_type GGType.Float 
	  "date should be of type float" (_exp) in
      if bl.timer_used <> Nope
      then
	GGStdpp.raise_with_loc loc (Failure ("target message \"" ^
				  bl.timer_msg_name ^ "\" already used by" ^
				  " another timer"))
      else
	begin
	  bl.timer_used <- TimerB;
	  let ggtimer = EnvironmentSimple.lookupType dummy_loc "GGTimer" env in
	  EnvironmentSimple.add_erase env bl.timer_id;
	  let newenv,v = match _timer_name with
	    Some x -> 
	      bl.timer_name <- Some x;
	      let var = Symbol.mk ~alpha: true ~tag: bl.timer_id ~lvalue: true
		  ~name: x ggtimer 
	      in
(*	      Printf.eprintf "timername = %s\ntimerid = %d\n" var.Symbol.name
		(Stamp.hash var.Symbol.tag);*)
	      { env with 
		timers = StringMap.add x bl env.timers;
		symbols = StringMap.add x var env.symbols
	      },var
	  | None -> 
	      env, 
	      Symbol.mk ~alpha: true ~tag: bl.timer_id ~lvalue: true
		~name: timer_name ggtimer 
	  in
	  com @@ (
		GGCommand.mk ~loc (Let v)) @@ 
		(GGCommand.mk ~loc (Set_timer(msg_name, exp, bl))), newenv
	end

    let reschedule loc timer_name _exp env =
      let bl = lookup_timers loc timer_name env in
      let exp,com = check_type GGType.Float 
	  "date should be of type float" (_exp) in
      com @@ (GGCommand.mk ~loc (Reschedule_timer(exp, bl))),env
      

  end

open EnvironmentSimple
open GGCommand.Std

let build_while _e _c loc env =
  let m c = GGCommand.mk ~loc  c in 
  let flat = GGCommand.flatten in
  let _,breakLabel = Gencode.Label.mk () 
  and _,continueLabel = Gencode.Label.mk ()
  and _,loopLabel = Gencode.Label.mk () in
  let e,com = check_type GGType.Bool "Bool expected in while clause"
      _e
  and bleuh = Loop.newLoopData breakLabel continueLabel  in
  let newenv = {env with block_stack = 
		(LoopBlock bleuh, bleuh.Loop.loopid) :: env.block_stack } 
  in
  let c',_ = _c newenv in
  let c = flat [m (GGCommand.Label loopLabel);
		com;
		m (GGCommand.If(e, 
				flat 
				  [c'; 
				   m (GGCommand.Label continueLabel);
				   m (GGCommand.Jump (loopLabel,bleuh.Loop.loopid))]
				  , m GGCommand.Nop));
		m (GGCommand.Label breakLabel)] in
  c,env 
    


let build_dec x _t owner initVal loc l env =
  let (@@) = GGCommand.(@@) in
  let t,env = _t env in
  let () = 
    if owner && not (GGType.is_object t) then
      GGStdpp.raise_with_loc loc 
	(Failure ("owner flag can only be applied to object variable"))
  in
(*  let new_name  = ("__" ^ (string_of_int (Stamp.hash id)) ^ "_" ^ x) in*)
  let newvar = Symbol.mk ~alpha: true ~lvalue: true  ~name: x t in
(*  Printf.eprintf "name = %s et %s\n" x newvar.VariableSimple.name;*)
  let  newenv = { env with symbols  = StringMap.add x newvar env.symbols } 
  and  declaration = GGCommand.mk
      (GGCommand.Let newvar)
  in
  let valeur = 
    match initVal with
      Some valeur -> valeur
    | None -> (fun env -> GGExp.DefaultVal.build ~loc t, nop)
  in
  let blob,_= Assign.build
      (fun env -> (Symbol.to_exp ~loc newvar, GGCommand.nop ))
      valeur
      loc
      env in
  declaration @@ blob @@ l, newenv

let build_let l loc env = 
  List.fold_left (fun (l,e) f -> f l e) (nop,env) l


(*FIXME
Il faut absolument penser ‡ flinguer la continuation
*)
let build_return e loc env =
  let (t,id,ret, lst, lst_handler) = 
    match lookup_rettype loc env with
      { 
	 return_type = rt;
	 fn_id = id;
	 fn_return_var = ret }, a, b ->
	   rt, id, ret, a, b
  in
  let check_handler  = function 
      hd :: l ->
	hd.hd_do_break <- true;
(*	let () = 
	  try 
	    match StampMap.find hd.hd_id env.handler_state with
	      Sbreak -> 
		GGStdpp.raise_with_loc loc (Failure "erreur chiante a dÈcrire")
	    | Snobreak -> () 
	  with Not_found -> GGStdpp.raise_with_loc loc (Failure "internal compiler error")
	in ()
*)
    | [] -> () in
  check_handler lst_handler;
  let env = {env with unreachable_code = true} in
  let rettcode =
    GGCommand.mk ~loc (GGCommand.BlockToPop lst) @@
     GGCommand.mk ~loc (GGCommand.Jump (id,id)) in
  match e,ret with
    Some x, Some ret -> 
      let (e,com) = x env in
      if GGType.is_sub_type t e.GGExp.thetype  then
	(com @@ ( GGCommand.mk ~loc (GGCommand.Assign(ret,e)) @@ rettcode ),env)
      else
	GGStdpp.raise_with_loc loc (Failure("expected return type is " ^
				    (GGType.to_C_type 0 "" t) ^
				    "\n"))
  | Some x, None -> 
      GGStdpp.raise_with_loc loc 
	(Failure(
	 "This function is declared as returning void "^
	 "but it returns something"))

  | None, Some _ ->
      GGStdpp.raise_with_loc loc 
	(Failure(
	 "This function is declared as returning a value "^
	 "but it returns nothing"))

  | None,None ->
      (rettcode, env)

let build_match loc (e,c)   _l cdef env =
  let el = List.map 
      begin
	fun ((e',c'),c,loc) -> 
	  (e',c', c, loc)
      end _l
  in
  let c', e  = CompileLval.make e in
  let c = c @@ c' in
  let equal y =  
    build_generic GGExp.Equal  [e;y] y.GGExp.loc env nop
  in
  let res = 
    List.fold_right 
      begin
	fun (e',cl,clause,loc) blub ->
	  let test,c' = equal e' in
	  let thecom = GGCommand.mk ~loc 
	      (GGCommand.If(test, clause, blub)) in
	  (cl @@ c' @@ thecom)
      end
      el cdef
  in
  ( c @@ res, env)
	

  
let build_function loc f_decl c  (env : EnvironmentSimple.t) =
  let (x,l,t),env,_ = f_decl  in
  let (symbols,l', targ,lstvar) = List.fold_right (fun (a,owner,b) (e,l2,l4,l3) -> 
    let v = Symbol.mk ~lvalue: true ~alpha: true ~name: a b in
    (StringMap.add a v  e, 
     (a,b,v.Symbol.tag) :: l2, 
     (a,b) :: l4,
     (v, a)::l3))
      l (env.EnvironmentSimple.symbols,[],[],[]) 
  and rettype = t in
  let retlabel = Stamp.mk ()
  and retdec, ret, e = 
    match GGType.effectiveType rettype with
      GGType.Unit ->
	nop, GGCommand.mk 		 
	  (GGCommand.Return None), None
    | _ -> 
	let e,c = Variable.mk_exp ~name: "return_val" rettype in
	c, 
	GGCommand.mk 
	  (GGCommand.Return (Some e)),
	Some e
  in
  let fn_block = 
    {
     EnvironmentSimple.use_self = None;
     EnvironmentSimple.fn_return_var = e;
     EnvironmentSimple.fn_id = retlabel;
     EnvironmentSimple.return_type = rettype } in
  let comdec = GGCommand.flatten 
      (List.map 
	 (fun (v,w) -> 
	   
	   GGCommand.mk  (GGCommand.Let v) @@
	   GGCommand.mk 
	     (GGCommand.Assign(Symbol.to_exp v,
			       GGExp.mk 
				  (GGExp.Symbol w)
				  v.Symbol.thetype
			      )
	     )
	 )  lstvar )
  and newenv = {env with 
		EnvironmentSimple.block_stack =
		(EnvironmentSimple.FunctionBlock fn_block, 
		 retlabel)  :: 
		env.EnvironmentSimple.block_stack;
		EnvironmentSimple.name_func = Some x;
		EnvironmentSimple.symbols = symbols;
		EnvironmentSimple.to_erase = (ref []) ::
		env.EnvironmentSimple.to_erase
	      } 
  in 
  let c,_ = c newenv in
  let atomicity = match c.GGCommand.atomicity  with
    GGCommand.Atomic -> 
      let () = match fn_block.use_self with
      |	Some loc -> failwithloc loc "This function is atomic.  Thus it cannot use 'self'"
      | None -> () in
      true
  | GGCommand.NonAtomic -> false in
  let clean = GGCommand.mk 
		 (GGCommand.Label retlabel) @@
(*	       GGCommand.mk 
		 (GGCommand.Clean(!(List.hd newenv.EnvironmentSimple.to_erase)));*)
	       ret
  in
  GGFonction.mk ~loc  atomicity rettype x  l' 
    (retdec @@ comdec @@ c @@ clean)

module Detach = 
  struct
    open Abbreviation
    open GGType
    open EnvironmentSimple
    open Symbol
      
    let build loc f args o env = 
      try 
	let ans = match StringMap.find f env.symbols  with
	  { complex = true; 
	    tag = id ;
	    thetype = GGType.Function(t, l)
	  } -> 
	    let args = Operator.Default.check_apply_args  loc
		snd (fun x -> x) l args in
	    let classfun = 
	      GGExp.mk ~loc (GGExp.Symbol 
			       ("Complex_" ^ f)) Id in
	    let e_call = 
	      let rem,n = 
		match l with
		| [] -> None,f ^ "_"
		| (n,_) :: l -> 
		    let rec iter res l1 l2 = 
		      match l1, l2 with
		      | ((n,_) :: l1), e :: l2 ->
			  (n, e) :: res
		      | [], [] -> List.rev res
		      | _ -> failwithloc loc "Impossibleeeee"
		    in
		    let x = iter [] l (List.tl args) in
		    Some (List.hd args, x), f ^ "_" ^ n
	      in
	      (E.mk ~loc (E.MessageCall (classfun, n, rem)) Unit)
	    in
	    let a = 
	      match o with
	      | None ->
		  GGCommand.mk ~loc (GGCommand.Expr e_call), env
	      | Some s -> 
		  let tag = Stamp.mk () in
		  let var = Symbol.mk ~name:s ~tag ~alpha: true ~lvalue: true Id
		  in
		  let exp = Symbol.to_exp var in
		  let c = 
		    C.mk ~loc (C.Let var) @@
		    C.mk ~loc (C.Assign(exp, e_call))
		  and env = { env with
			      symbols = StringMap.add s var env.symbols} in
		  (c, env)
	    in
	    a
	| _ ->
	    failwithloc loc ("function " ^ f ^ " is not complex")
	in
	ans
       with
  Not_found ->
    failwithloc loc ("unknown symbol "^f)
  end

module Register =
  struct
    open Abbreviation


    let build loc e name env = 
(*      let cont_name = "_continuation_return_" ^ name
      in 
      let bl = lookup_timer_msg loc name env in
      if bl.timer_used <> Nope
      then
	GGStdpp.raise_with_loc loc (Failure ("target message \"" ^
				  bl.timer_msg_name ^ "\" already used by" ^
				  " another truc"))
      else
	begin
	  bl.timer_used <- ContReturn;
	  let ggtimer = EnvironmentSimple.lookupType (-1,-1) "GGTimer" env in
	  EnvironmentSimple.add_erase env bl.timer_id;
	  in
	  com @@ (
		GGCommand.mk ~loc (Let (timer_name, ggtimer,
					bl.timer_id))) @@ 
		(GGCommand.mk ~loc (Set_timer(msg_name, exp, bl))), newenv
	end
*)

      failwithloc loc "not implemented"
  end
