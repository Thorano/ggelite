/*top level stuff */
%{
  open ProtoTree
  open Utile
  open GGStdpp
  
%}

%token EOI

/*keyword*/
%token  TYPE
%token  FUNCTION
%token  INLINE
%token  IMPORT
%token  EXTERN
%token  LET
%token  CONST
%token  COMPLEX

/* type element */
%token  FLOAT
%token  INT
%token  BOOL
%token  STRING
%token  UNIT
%token  ID
%token  ENUM
%token  TABULAR
%token  STRUCT

/* parenthesis and such */
%token  LPAREN RPAREN
%token  LBRACE RBRACE
%token  LANGLE RANGLE
%token  RBOX LBOX
%token  ASSIGN

/* other */
%token EQ

/* atomic element */
%token <int> INTEGER_CST
%token <bool> BOOLEAN_CST
%token <string> STRING_CST
%token <float> FLOAT_CST
%token <string> IDENT


/*operators*/

%token PLUS MINUS TIMES DIV PRODVECT EQUAL NOT_EQUAL LESSER LESSER_EQUAL GREATER GREATER_EQUAL AND OR NOT

%left PLUS MINUS        /* lowest precedence */
%left TIMES DIV         /* medium precedence */
%nonassoc UMINUS        /* highest precedence */
%start main             /* the entry point */
%type <ProtoTree.GGProg.t list> main
%%
main:
  fonction_or_typedecl_list EOI { List.rev $1 }
;

fonction_or_typedecl_list:
    { [] }
  | fonction_or_typedecl_list fonction_or_typedecl { $2::$1 };
  
fonction_or_typedecl:
  TYPE IDENT EQ ggtype { GGProg.DecType (GGTypeDecl.make_var_dec $2 $4 !loc_ref) }
;

ggtype: 
  FLOAT { GGType.Float }
| INT  { GGType.Integer }
| BOOL  { GGType.Bool }
| STRING  { GGType.String }
| UNIT {GGType.Unit}
| ID  {GGType.Id}
;
