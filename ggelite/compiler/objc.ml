open Utile
open Compile
open Ggtree
open GGType
open ProtoTree.GGInterface

(* AST for objc declaration *)

let c_cast loc = function 
    ("", t) -> t
  | _ -> GGStdpp.raise_with_loc loc (Failure "cast expected")

let get_opt_type env  = function
    Some (t, loc ) -> c_cast loc (t env)
  | _ -> Id

let canonize = function
    Pointer (Objet _ as t)  -> t
  | t -> t
  
let add_class_def loc env name class_def = 
    let decl = {GGTypeDecl.descr = name, Objet (ref class_def);
                GGTypeDecl.loc = loc} in
    let env_types = StringMap.add   name decl env.EnvironmentSimple.types 
    and env_symbols = 
      let class_var = Symbol.mk ~name 
          ~alpha: false ~lvalue: false
          (snd (decl.GGTypeDecl.descr)) in
      StringMap.add name class_var env.EnvironmentSimple.symbols
    in
    { env with EnvironmentSimple.types = env_types;
      EnvironmentSimple.symbols = env_symbols }


let compile_forward ?(allowRedefinition=true) loc x env =
  try 
    let obj = StringMap.find x env.EnvironmentSimple.types in
    begin
      match obj with
	{GGTypeDecl.descr = _,Objet { contents = Vrai _ }; GGTypeDecl.loc = old_loc} -> 
            if allowRedefinition then
                env
            else
                GGStdpp.raise_with_loc loc (Failure ("redefinition of \"" ^ x ^ "\", previous definition is " ^
                        (string_of_loc old_loc)))
      | {GGTypeDecl.descr = _,Objet _} -> env
      |	{GGTypeDecl.loc = old_loc} -> GGStdpp.raise_with_loc loc 
	      (Failure ("type is already defined as non object here "  ^
			(string_of_loc old_loc))
	      )
      end;
  with
    Not_found -> add_class_def loc env x (Faux x)

let compile (compile_type : EnvironmentSimple.t -> ProtoTree.GGType.t -> GGType.t) loc objc_interface   env =
    let x = objc_interface.name
    and herite = objc_interface.inherit_from 
    and cat:bool = 
        let rec iter = function
            Category _ :: l -> true
            | _ :: l -> iter l
            | [] -> false
        in iter objc_interface.protocols
    in
(*  x, herite, l, lm, cat *)

    (* first, we need to add the current symbol to the environment to handle recursive definition *)
    let env = 
        if cat then 
            env
        else
            compile_forward ~allowRedefinition:false loc objc_interface.name env
    in
    
  let herite,env = 
    match herite with
      Some x -> 
	begin
	  try 
	    begin
	      match StringMap.find x env.EnvironmentSimple.types with
		{GGTypeDecl.descr = _,Objet o } -> Some o,env
	      | _ -> GGStdpp.raise_with_loc loc 
		    (Failure ("cannot inherit from a non-object type " ^
			      x))
	    end
	  with Not_found -> 
(*	    warn ~loc ("ancestor '" ^ x ^ "' is undefined");*)
	    None, compile_forward ~allowRedefinition:false  loc x env

	end
    | None -> None, env
  in
  
  (* feed the list of visible ivar *)
  let  eval_method  a_method =
      let  next_components,fullName = 
        match a_method.next_components with
           None -> None,a_method.first_component
           | Some (t,l) -> 
                let names, l = 
                  let rec iter names lstout = function 
                    (n,t) :: l -> iter (names ^ n ^ ":") ((n, compile_type env t)::lstout) l
                    | [] -> (names, List.rev lstout) 
                  in
                  iter (a_method.first_component ^ ":") [] l
                in
                Some (compile_type env t, l), names 
      in
      { method_ret_type = compile_type env  a_method.method_ret_type;
        first_component = a_method.first_component;
        next_components = next_components},fullName
  in
    let rec iter_method names list_res = function
      m :: l -> let m, name = eval_method m in iter_method (name::names) (m::list_res) l
        | [] -> (list_res, names)
    in
    
  let ivar = List.map (fun {ProtoTree.GGVarDecl.descr = (s,t)} -> (s, compile_type env t)) objc_interface.public_ivars
  and i_m,names = iter_method [] [] objc_interface.class_methods in
  let c_m, names = iter_method names [] objc_interface.instance_methods in

  let o = {
    GGType.name = x;
    GGType.herite = herite;
    GGType.var_i = ivar;
    GGType.methods_i = i_m;
    GGType.methods_c = c_m;
  } in

  let new_env,obj = 
    try
      let obj = StringMap.find x env.EnvironmentSimple.types in
      begin
	match obj with
	  {GGTypeDecl.descr = _,(Objet ({ contents = Faux _ } as pobj ) as t)} ->
	    pobj := Vrai o;
	    let class_var = Symbol.mk ~lvalue: false 
		~name: x 
		~alpha: false t in
	    { env with EnvironmentSimple.symbols = StringMap.add x class_var
		env.EnvironmentSimple.symbols }, o
	| {GGTypeDecl.descr = _,Objet ({ contents = Vrai truc } )} -> 
	    begin
                if cat then
                begin
                    GGType.merge_cat truc o;
                    Utile.Log.printf "find category for %s\n" o.GGType.name
                end
	    end;
	    env,truc
	|_ -> GGStdpp.raise_with_loc loc 
	      (Failure "type already exists")
      end;
    with
      Not_found -> 
        add_class_def loc env x (Vrai o), o 
  in
  EnvironmentSimple.add_methods new_env names obj
	

let lstdir = ref []


let rec open_in_list n = function
    a :: l ->
      begin
	try 
	  let name = Filename.concat a n in
	  open_in name
	with
	  Sys_error _ -> open_in_list n l
      end
  | [] -> raise (Sys_error ("cannot find file " ^ n ^ " in "))

module S = EnvironmentSimple.StringSet
module Env = EnvironmentSimple

let parse_proto_tree: ((ProtoTree.GGProg.t list) -> EnvironmentSimple.t -> (EnvironmentSimple.t * 
							 (GGProg.t list))) ref  = ref(fun l env -> (env,[]))

let import recursif (x:string) loc (env : EnvironmentSimple.t) =
  if not (S.mem x env.Env.imported) then
    begin
      let list = "." :: !lstdir in
      let name = x ^ ".h" in
      try
(*	Printf.eprintf "[parsing %s\n" name;*)
	let in_file = open_in_list  name list in
        let lexbuf = Lexing.from_channel in_file in
        let module Lexer = Cdec_lexer.Lexer (Cdec_parser) in 
        let lst = Cdec_parser.main (Lexer.myentry (ref false)) lexbuf in
        let (e,l) = !parse_proto_tree lst env in
	let e = { e with Env.imported = 
		  S.add x e.Env.imported} in
(*	Printf.eprintf "parsing %s end]\n" name;*)
	let l = if false && recursif then l else GGProg.Import x :: l in
	(e, l)
with 
  Sys_error s -> 
    GGStdpp.raise_with_loc loc (Failure (s ^ (String.concat ":" list)))
| GGStdpp.Exc_located (loc,exn) as e ->
    let s = Printexc.to_string exn in
    Printf.printf "\nerreur dans %s\n%s\n%s" name s (string_of_loc loc);
(*    print_string name;
    print_string "\n";
    print_string s;
    Printf.printf "\ndebut : %d fin : %d\n" i j;*)
    raise e
    end
  else
    begin
(*    Printf.eprintf " --> parsing of %s already done\n" x;*)
    (env, [])
    end
      
