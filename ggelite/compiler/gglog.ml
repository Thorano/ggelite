open Utile
open Ggtree


module MyFormat = 
  struct
    open GGType
    open GGCommand

    let rec build e = 
      let nop = mk Nop in
      match GGType.effectiveType(e.GGExp.thetype) with
	Unit -> "()", [], nop
      | Id -> "%@",[e],nop
      | Bool -> 
	  let dec,v = Gencode.Variable.build_temp_dec GGType.String in
	  let var = Symbol.to_exp v in
	  let cl = 
	    dec @@
	    (mk (If(e, mk (Assign(var, 	GGExp.mk 
				   (GGExp.String("true")) GGType.String)),
		   mk (Assign(var, GGExp.mk 
				(GGExp.String("false")) GGType.String))
		  )
	       )
	    )
	in
	"%@", [var], cl
    | Integer -> "%d", [e], nop
    | Float -> "%g", [e], nop
    | String -> 
	begin
	  match e.GGExp.descr with
	    GGExp.String s -> s, [], nop
	  | _ -> "%@", [e], nop
	end
    | Enum ({EnumString.vals = l} as myenum) -> 
	begin
        (* only when used we will output the corresponding strings *)
          myenum.EnumString.used <- true;
	  match e.GGExp.descr with
	    GGExp.Symbol s -> "%@", [GGExp.mk (GGExp.String s) GGType.String], nop
	  | _  -> 
	      begin
		try
		  let s = Compile.EnvironmentSimple.get_strings_enum myenum in
		  let tab = GGExp.mk (GGExp.Symbol s) (Array(ref None, String)) in
		  let exp = GGExp.mk (GGExp.ArrayAccess(tab, e)) String in
		  "%s", [exp], nop
		with
		  Not_found ->
		    warn_with_loc e.GGExp.loc "internal, cannot find enums...";
		    "%d", [e], nop
	      end
	end
    | Array (n,t) -> "",[],nop
    | Struct(s)  -> 
	let t = e.GGExp.thetype in
	let dec,id = Gencode.Variable.build_temp_dec t in
	let var = GGExp.mk (GGExp.Var id) t in
	let save = dec @@  
	  (GGCommand.mk (Assign(var, e))) in
	let e = var in
	let (format, el, cl) = 
	  List.fold_left (fun (f,exp,cl) (n,t) ->
	    let field = GGExp.mk (GGExp.StructAccess(e,n)) t in
	    let f',e',cl' = build field in
	    let f' = n ^ ":" ^ f' in
	    let newfor = 
	      if f = "" then f'
	      else f ^ ", " ^ f'
	    in
	    (newfor, e' :: exp, cl' @@ cl)) 
	    ("",[],nop) s
	in
	("(" ^ format ^ ")", List.flatten(List.rev el), save @@ cl)

    | Name _  
    | Abstract _ -> failwith "impossible"
    | Function (t, l) -> "<fun>",[],nop
    | Pointer(t) -> "%p", [e], nop
    | Objet _ -> 
	match e.GGExp.descr with
	  GGExp.String s -> s,[],nop
	| _ ->"%@", [e], nop

    let mk loc args =
      let (format, args', coms) = 
	List.fold_left (fun (s, args, cl) e ->
	  let (s',args',cl') = build e in
	  (s ^ s', args @ args', cl @@ cl')) ("",[],nop) args
      in
      GGExp.mk ~loc (GGExp.String(format)) GGType.String ::  args',  coms

  end

open GGExp
open Compile.EnvironmentSimple

let add_functions env = 
  let apply symb ret = 
    let f = 
      GGExp.mk (Symbol symb)   (GGType.Function(ret,[]))
    in fun env loc args ->
      let (arg', com) = MyFormat.mk loc args in
      GGExp.mk ~loc (Function(f, arg')) ret, com
	
  and apply_debug symb ret =
    let f = 
      GGExp.mk (Symbol symb)   (GGType.Function(GGType.Unit,[]))
    in fun env loc args ->
      let name_func =    match env.name_func with
	Some x -> x
      |	None -> "Default"
      in
      let (arg', com) = MyFormat.mk loc args in
      GGExp.mk ~loc (Function(f, (GGExp.mk ~loc (GGExp.String name_func) 
				    GGType.String) :: arg')) GGType.Unit, 
      com

  and apply_assert symb ret= 
    let f = 
      GGExp.mk (Symbol symb)   (GGType.Function(GGType.Unit,[]))
    and rem = [GGExp.mk (GGExp.String "blop")
		 GGType.String]
    and argt = [GGType.Bool] 
    in fun env loc args ->
      let arg' = Gencode.Operator.Default.check_apply_args loc (fun x -> x)
	  (fun x -> x) argt args in
      GGExp.mk ~loc (Function(f, arg' @ rem)) GGType.Unit, GGCommand.nop
	

  and apply_size_array bid1 bid2 env loc = function 
    | a :: [] ->
	let e = 
	  match a.GGExp.thetype with
	  | GGType.Array({contents = Some n}, _) ->
	      let e = GGExp.mk ~loc (GGExp.Int n) GGType.Integer in
	      e
	  | GGType.Array _ ->
	      failwithloc a.GGExp.loc "cannot guess the size of this array"
	  | _ ->
	      failwithloc a.GGExp.loc "expr is not of array type"
	in
	e,GGCommand.nop
    | _ ->
	failwithloc loc "too many arguments in call to \"size\""
	

  in
  let add_env env (symb1,stub,apply, ret) = 
    let v = Symbol.mk ~alpha: false ~name: symb1 ~lvalue: false
	(GGType.Function(ret, []))
    in
    {env with 
     symbols = StringMap.add stub v env.symbols;
     pseudo_functions = StampMap.add v.Symbol.tag
       (apply symb1 ret) env.pseudo_functions}
  in


  let l =
    ["NSLog", "display", apply, GGType.Unit;
    "displayOnGGEliteConsole", "log", apply, GGType.Unit;
     "consoleLog", "consoleLog", apply, GGType.Unit;
     "NSDebugLLog", "debug", apply_debug, GGType.Unit;
     "NSDebugLLogWindow", "debugw", apply_debug, GGType.Unit;
     "NSCAssert", "assert", apply_assert, GGType.Unit;
     "ggbuild_format", "format", apply, (Compile.EnvironmentSimple.lookupType dummy_loc "NSString" env);
     "cacaboudin", "size", apply_size_array, GGType.Integer;
   ]
  in
  List.fold_left add_env env l 

