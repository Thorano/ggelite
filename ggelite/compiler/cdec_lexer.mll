{
   open Lexing
   
   module Lexer (Token:Ggtoken.TokenSig) = struct
    open Token
} 

let ident = ['a'-'z''A'-'Z''_']['a'-'z''A'-'Z''0''1'-'9''_']*
let integer = ['0''1'-'9']+
let hexint = '0' ('x' | 'X') ['a'-'f''0'-'9''A'-'F']+
let float_s = (integer '.' integer?) | ('.' integer)
let float_e = (integer | float_s) 'e' ['+' '-']? integer
let float = float_s | float_e

let blank1 = [' ' '\t']+
let blank2 = '\n' | ("\n#" [^ '\n']*)
let blank = blank1 | blank2
   

rule  entry = parse
   blank              {entry lexbuf}
   | ident   as x     {IDENT x}
   | integer          {INTEGER_CST (int_of_string (lexeme lexbuf))}
   | hexint as s       {INTEGER_CST (Scanf.sscanf s "%x" (fun x -> x))}
   | float            {FLOAT_CST (float_of_string (lexeme lexbuf))}
   | '('            {LPAREN}
   | ')'            {RPAREN}
   | '{'            {LBRACE}
   | '}'            {RBRACE}
   | '<'            {LANGLE}
   | '>'            {RANGLE}
   | '['            {RBOX}
   | ']'            {LBOX}
   | ','            {COMA}
   | '&'            {AMPERSAND}
   | "->"           {DEREF}
   | "\\\n"         {entry lexbuf}

   | ';'            {SEMICOLON}
   | ':'            {COLON}
   | '/'            {DIV}
   | '*'            {TIMES}
   | '+'            {PLUS}
   | '-'            {MINUS}
   | '='            {EQUAL}
   | '!'            {EXCLAM}
   | '|'            {PIPE}
   | '.'            {DOT}
   | "..."            {CDOT}
   | '@' ident        {IDENT (lexeme lexbuf)}
   | eof              {EOI}
   | "/*"             {comment lexbuf; entry lexbuf}
   | "/*->"           {BEGINDEC}
   | "/*-="           {entry lexbuf}
   | "=-*/"           {entry lexbuf}
   | "/*%%%"[^'%']*"%%%*/" { let s = lexeme lexbuf in
                             let s' = String.sub s 5 (String.length s - 10) in
			     INLINE (s')}
   | "<-*/"           {ENDDEC}
   | "//"[^'\n']*     {entry lexbuf}
   | '"'[^'"']*'"'    {STRING_CST(lexeme lexbuf)}
   | _               {entry lexbuf}

and comment = parse
       "/*"           {comment lexbuf; comment lexbuf}
   | "*/"             {}
   | _                {comment lexbuf}
   | eof              {failwith "end of file in a comment"}

       {

let table_type = Hashtbl.create 20
	    
let () = List.iter (fun (a,b) -> Hashtbl.add table_type a b)
    ["void",UNIT;
     "int", INT;
     "unsigned", UNSIGNED;
     "short", INT;
     "long", INT;
     "char", INT;
     "float", FLOAT;
     "double", FLOAT;
     "BOOL", BOOL;
     "Bool", BOOL;
     "id",ID;
     "struct", STRUCT;
     "enum", ENUM;
     "extern", CDECMOD;
     "static", CDECMOD;
     "const", CDECMOD;
     "inline", CDECMOD;
     "typedef", TYPEDEF;
     "@interface", BEGININTERFACE;
     "@end", ENDINTERFACE;
     "@public", PUBLIC;
     "@private", PRIVATE;
     "@class", CLASS;
     "GGBEGINDEC", BEGINDEC;
     "GGENDDEC", ENDDEC;
   ] 
       
let normalize_lexeme = function
    (IDENT x) as s-> 
      begin
        try Hashtbl.find table_type x with
          Not_found -> s
      end
  | s -> s
  
open Utile
let dump s = function
    IDENT x -> Log.printf "IDENT %s" x
    | COLON -> Log.printf "COLON"
    | LBRACE -> Log.printf "LBRACE"
    | BEGININTERFACE -> Log.printf "BEGININTERFACE"
    | _ -> Log.printf "Not handle: %s" s


let myentry (actif:bool ref) lexer = 

  let rec sub_lex lexer = 
      let s = normalize_lexeme (entry lexer) in
      match s with
        EOI -> s
      | BEGINDEC ->
          actif := true;
          sub_lex lexer
      | ENDDEC ->
          actif := false;
          sub_lex lexer;
      | _ ->
          if !actif  then 
            s
          else 
            sub_lex lexer

    in
    let s = sub_lex lexer in
(*    dump (Lexing.lexeme lexer) s; *)
    s
    
end
} 
