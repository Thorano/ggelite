open ProtoTree
open Utile
open Camlp4.PreCast
open Syntax

let prog_eoi = Gram.Entry.mk "Programme"


let opt_to_bool = function
    Some _ -> true
  | None -> false

let opt_to_list = function
    Some l -> l
  | None -> []

let rec extract_listen gm tm pm = 
  function
      `GlobalMsg x :: l -> extract_listen (x :: gm) tm pm l
    | `LocalMsg y :: l -> extract_listen gm tm (y :: pm) l 
    | `TimerMsg t :: l -> extract_listen gm (t :: tm) pm l
    | [] -> (gm, tm, pm)

let mkc = GGCommand.mk
let mke = GGExp.mk
module C = GGCommand
module T = GGType
module E = GGExp

EXTEND Gram
  GLOBAL: prog_eoi;

prog_eoi:
    [
     [ r = LIST0 fonction_or_typedecl   -> r	   
     ]
    ] ;

ident:
  [[x = LIDENT -> x
  | x = UIDENT -> x
] ];

fonction_or_typedecl:
  [[ "type" ; x = ident; "="; t = ggtype ; OPT [";"] -> 
	  GGProg.DecType (GGTypeDecl.make_var_dec x t _loc)
  | "type" ; x = ident; ";"  -> 
	  GGProg.DecType (GGTypeDecl.make_var_dec x (GGType.Abstract (x,_loc)) _loc)
  | "function" ; f = fonction -> 
          GGProg.Function f

  | "inline" ; (f,e) = fonction_simple -> 
        GGProg.Inline (f,e, _loc)

  | "import" ; x = ident ; ";" -> 
        GGProg.Import (x, _loc)
  | "extern" ; x = extern ->
        GGProg.Extern x
  | "let" ; x = declist ; ";" ->
      (match x with
      |	(x, t, Some i, _) -> GGProg.LetGlobal (x,t,i)
      | _ -> failwithloc _loc "missing initializer")
  | "const"; x = ident ; ":"; t = ggtype; "=" ; e = expr ; ";" ->
      GGProg.Const (x,t,e)
     ] ];

extern:
    [[ 
    "complex" ;  f_decl = fonction_decl; ";" ->
       ExternFunction
       { complex_ef = true;
	 f_decl_ef = f_decl;
	 external_name_ef = None;
       } 
  | f_decl = fonction_decl; cn = OPT ["=" ; cn = STRING -> cn]; ";" ->
       ExternFunction
       { complex_ef = false;
	 f_decl_ef = f_decl;
	 external_name_ef = cn;
       } 
  | x = LIDENT; ":" ; t = ggtype ; cn = OPT ["=" ; x = STRING -> x]; ";" -> 
      ExternVariable
      {
       name_vt = x;
       type_vt = t;
       external_name_vt = cn
     } 
     ] ];
fonction_decl:
  [[  x = ident ; "(" ; l = LIST0  arg_dec SEP ","; ")" ; ":" ; t = ggtype 
	-> 
	  {name_fun = x;
	   args_fun = l;
	   rettype_fun = t;
	   loc_fun = _loc;
	 } 
    ] ];

fonction_simple:
  [[ f_decl = fonction_decl ;  c = ["{" ; e = expr ; "}" -> e] -> f_decl,c
   ] ];


fonction:
  [[  f_decl = fonction_decl ;    c = block ->   GGFonction.mk _loc f_decl c
  ]];
ggtype:
  [[  "Float" -> GGType.Float
    | "Int" -> GGType.Integer
    | "Bool" -> GGType.Bool
    | "String" -> GGType.String
    | "Unit" -> GGType.Unit
    | "Id" -> GGType.Id
    | "enum" ; "{"; l = LIST1 ident SEP ","; "}" -> GGType.Enum l
    | "tabular" ; "["; x = OPT [x = INT -> x] ; "]" ; "of" ; t = ggtype -> 
	let x = match x with 
	| Some x -> Some (int_of_string x)
	| None -> None in
	GGType.Array(x, t)
    |  "struct" ; "of"; "{"; l = LIST1 [x = arg_dec; ";"->x] ; "}" -> 
	  let l = List.map
	      begin
		fun  (x,_,t,_)  -> (x,t)
	      end  l 
	  in
	  GGType.Struct l
    | x = UIDENT -> GGType.Name (x,_loc)
    ]];
arg_dec:
  [[  x = ident ; o = OPT ["<"; "owner"; ">"]; 
      ":" ; t = ggtype -> (x, opt_to_bool o,t , _loc) ]];
block:
  [[ "{" ; l = LIST0 command ; "}" -> List.flatten l
   ]];
declist:
    [[ x = ident ; o = OPT ["<"; "owner"; ">"]; ":"; t = ggtype ; initVal =  
	 OPT ["=" ; e = expr -> e] -> (x,t,  initVal, opt_to_bool o)
     ]];
cond_expr:
	[[
	 "when" ; e = expr -> e
       ] ];
one_arg:
   [[
    t = OPT [ "("; t = ggtype; ")" -> t]; s = ident ->
      let t = match t with
	Some x -> x
      |	None -> GGType.Id
      in
      (t,s,_loc)
  ] ];

args_method:
[[
   o = OPT [ ":" ; a = one_arg; l = LIST0 [n = LIDENT; ":" ; a = one_arg -> 
     (n,a)] ->   (a,l)] -> o
] ];
listen_block:
    [["+" ; name = UIDENT ; args = OPT ["(" ; l = LIST1 arg_dec SEP "," ; ")" -> l] ;
      from = OPT ["from" ; e = expr -> e] ; cond = OPT [cond_expr];
      "->" ; c = command ->
	`GlobalMsg{GGCommand.message_event = name;
		   GGCommand.source_event = from;
		   GGCommand.cond_event = cond;
		   GGCommand.args_event = List.map (fun (x,o,t,loc) -> 
		     (x,t,o,loc))
		     (opt_to_list args);
		   GGCommand.com_event = c;
		 } 
  | "-" ; name = LIDENT; o = args_method ; "->" ; c = command ->
      `LocalMsg {
      GGCommand.pub_msg_name = name;
      GGCommand.pub_msg_args = o;
      GGCommand.pub_msg_com = c;
    } 
  | "*" ; name = LIDENT ; o = OPT [ "("; l = LIST1 
				      [n = ident; ":"; t = ggtype; ")" -> (n,t,_loc)] 
				       -> l] 
	; "->" ; c = command ->
      `TimerMsg {
      GGCommand.priv_msg_name = name;
      GGCommand.priv_com_list = c;
      GGCommand.priv_args = match o with Some l -> l | None -> [];
    } 
      
    ] ];
wait_time:
[[
  e = expr; u =  [ [ | "seconds"] -> `Seconds | 
            [ "day" | "days"] -> `Days | 
	    [ "hours" | "hour"] -> `Hours | 
	    [ "min"] -> `Minute  ] -> 
	      let m f = mke _loc (E.Float f) in 
	      let e = match u with
	      | `Seconds -> e
	      | `Days ->  mke _loc (E.Binop (E.Prod, e, m (24.0 *. 3600.0)))
	      | `Hours ->  mke _loc (E.Binop (E.Prod, e, m (3600.0)))
	      | `Minute ->  mke _loc (E.Binop (E.Prod, e, m (60.0))) in
	      e
]];
    
sleep:
  [
   [ "until" ; e = expr -> `Until e 
    | "during" ; e = wait_time -> `During e
]];

command:
  [
   [ "if" ; "(" ; e = expr ; ")" ; c = command ; elseclause = OPT 
       ["else" ; c1 = command -> c1 ] -> 
	 [GGCommand.mk _loc 
	    ( GGCommand.If(e, c, 
			   match elseclause with 
			     Some(x) -> x
			   | None -> []))]
   | "match" ; e = expr; "with"; "{"; 
	 l = LIST1 [e = expr ; "->"; c = command -> (e,c,_loc)] SEP "|";
	 o = OPT ["+"; "->"; c = command -> c] ;
	 "}" -> [GGCommand.mk _loc (GGCommand.Match  (e,l, opt_to_list o))]
   | "wait" ;"next" ; "frame";  ";" -> 
       [GGCommand.mk _loc (GGCommand.Wait 
			    (GGCommand.Stand_Wait "NEXT_FRAME"))]
   | "wait" ;"some" ; "time";  ";" -> 
       [GGCommand.mk _loc (GGCommand.Wait 
			     (GGCommand.Stand_Wait "SOME_TIME"))]
   | "sleep";  e = OPT sleep ; ";" -> 
	 begin
	   match e with
	     Some e -> 
	       let e = match e with
	       | `Until e -> (*date expected*)
		   let loc = e.E.loc in
		   mke loc (E.Binop(E.Minus, e,
				    mke loc (E.Variable "now")))
	       | `During e -> e in
	       [GGCommand.mk _loc (GGCommand.Wait (C.Delay e))]
	   | None ->
	       [GGCommand.mk _loc (GGCommand.Wait 
				     (GGCommand.Stand_Wait "FOREVER"))]
	 end
   | "detach"; f = ident; "(" ; l = parse_arg; ")"; 
       o = OPT [ "->" ; n = ident -> n]; ";" ->
	 [mkc _loc (C.Detach(f, l, o))]
   | "on" ; "return"; "from" ; e = expr ; "do" ; n = ident ->
       [mkc _loc (C.RegisterCont(e,n))]
   | "set_timer_for_date" ; "(" ; e = expr ; "," ; n = LIDENT;
       t = OPT [";" ; x = LIDENT -> x] ; ")" ; ";" -> 
	 [GGCommand.mk _loc (GGCommand.Set_timer (t, e, n))]
   | "reschedule_timer_for_date" ; "(" ; e = expr ; ";" ; n = LIDENT ; ")" ; ";" ->
       [GGCommand.mk _loc (GGCommand.Reschedule_timer (e, n))]
   | "loop" ; timer = OPT ["<"; x = LIDENT; ";" ; t = LIDENT; ">" -> (x,t,_loc)];
       c = command; "while" ; "listening"; "{";
       l = LIST1 listen_block; "}" -> 
	 let lg,lt,lp = extract_listen [] [] [] l in
	 let c = match timer with
	   Some (x,t,loc) -> 
	     GGCommand.mk loc 
	       (GGCommand.Set_timer(Some t, 
				    (GGExp.mk loc 
				       (GGExp.Symbol  ("GGDistantFuture",
						      GGType.Float))),
				    x)) :: c
	 | None -> c in
	 [GGCommand.mk _loc
	    (GGCommand.Listen
	       {
		GGCommand.listen_com = 
		[GGCommand.mk _loc 
		   (GGCommand.While(GGExp.mk _loc (GGExp.Bool true),
				    c))];
		GGCommand.event_list = lg;
		GGCommand.timer_list = lt;
		GGCommand.msg_list = lp;
	      }	)]
   | "do" ; c = command; "while" ; "listening" ; "{";
       l = LIST1 listen_block; "}" -> 
	 let lg, lt, lp = extract_listen [] [] [] l in
	 [GGCommand.mk _loc (GGCommand.Listen 
			      {
			       GGCommand.listen_com = c;
			       GGCommand.event_list = lg;
			       GGCommand.timer_list = lt;
			       GGCommand.msg_list = lp;
			     } )]
   | "for" ; i = ident; "="; e1 = expr; "to"; e2 = expr; "do"; c = command ->
       let index = mke _loc (E.Variable i) in
       [mkc _loc (
	C.Block(
	[mkc _loc (C.Let [i, T.Integer, (Some e1), false]);
	 mkc _loc (C.While(mke _loc (E.Binop(E.LesserEq, 
					   index,
					   e2)),
			  c @ 
			   [mkc _loc (C.Assign(index,
					      mke _loc (E.Binop (E.Plus, index,
								mke _loc (E.Int 1)
								)
						      )
					     )
				    )])
		 )]))]
   | "while" ; "(" ; e = expr ; ")"; c = command -> 
       [GGCommand.mk _loc (GGCommand.While(e, c))]
   | "break" ; ";"  -> [GGCommand.mk _loc GGCommand.Break]
   | "continue" ; ";"  -> [GGCommand.mk _loc GGCommand.Continue]
   | "resume" ; ";" -> [GGCommand.mk _loc GGCommand.Resume]
   | "answer" ; e = expr; ";" -> [GGCommand.mk _loc 
					    (GGCommand.Answer e)]
   | "return" ; ";" -> [GGCommand.mk _loc (GGCommand.Return None)]
   | "return" ; e = expr ; ";" -> [GGCommand.mk _loc (GGCommand.Return (Some e))]
   | "let" ; l = LIST1 declist SEP "and" ; ";"  -> 
       [GGCommand.mk _loc (GGCommand.Let l)]
   | b = block -> b
   | x = expr ; ";" -> [GGCommand.mk _loc (GGCommand.Expr(x))]
   | x = expr ; ":=" ; e = expr ; ";"  -> 
       [GGCommand.mk _loc (GGCommand.Assign(x,e))]
   ]

 ];

expr:
    [ 
       "or" LEFTA
	[ x = expr; "||" ; y = expr -> GGExp.binop_expr GGExp.Or x y _loc]

  |     "and" LEFTA
	[ x = expr; "&&" ; y = expr -> GGExp.binop_expr GGExp.And x y _loc]

  |      "not" 
      [ "!"; x = expr -> GGExp.monop_expr GGExp.Not x _loc]
        
  |    "compare" LEFTA
	[ x = expr; "<" ; y = expr -> GGExp.binop_expr GGExp.Lesser x y _loc
        | x = expr; "<=" ; y = expr -> GGExp.binop_expr GGExp.LesserEq x y _loc
        | x = expr; ">=" ; y = expr -> GGExp.binop_expr GGExp.GreaterEq x y _loc
        | x = expr; ">" ; y = expr -> GGExp.binop_expr GGExp.Greater x y _loc
        | x = expr; "==" ; y = expr -> GGExp.binop_expr GGExp.Equal x y _loc
        | x = expr; "<>" ; y = expr -> GGExp.binop_expr GGExp.Notequal x y _loc
	] 
    | "add" LEFTA
	[ x = expr; "+"; y = expr -> GGExp.binop_expr GGExp.Plus x y _loc
        | x = expr; "-"; y = expr -> GGExp.binop_expr GGExp.Minus x y _loc
        ] 
    | "mult" RIGHTA
	[ x = expr; "*"; y = expr -> GGExp.binop_expr GGExp.Prod x y 
	    _loc
        | x = expr; "/"; y = expr -> GGExp.binop_expr GGExp.Div x y 
	      _loc
        | x = expr; "^"; y = expr -> GGExp.binop_expr GGExp.ProdVect x y 
	      _loc
	| "-" ; x = expr -> GGExp.monop_expr GGExp.Minus x _loc
        ]
    | "funcal" LEFTA
	[
	 x = expr ; "(" ; l = parse_arg ; ")"  -> 
	   GGExp.mk _loc (GGExp.Function(x,l))
         | x = expr ; "[" ; ind = expr ; "]" -> 
	     GGExp.mk _loc (GGExp.ArrayAccess(x, ind))
	 | x = expr ; "." ; field = LIDENT -> 
	     GGExp.mk _loc (GGExp.StructAccess(x, field))
         ] 
    | "simple" NONA
	[ 
	  x = INT -> 
	    GGExp.mk _loc (GGExp.Int(int_of_string x ))
        | x = FLOAT -> 
	    GGExp.mk _loc (GGExp.Float(float_of_string x)) 
	| x = STRING -> 
	    GGExp.mk _loc (GGExp.String (x)) 
	| "true" -> 
	      GGExp.mk _loc (GGExp.Bool true) 
	| "nil" -> 
	      GGExp.mk _loc GGExp.Nil 
	| "NULL" -> 
	    GGExp.mk _loc (GGExp.Symbol ("NULL", GGType.Pointer GGType.Unit))
	| "false" ->
	      GGExp.mk _loc (GGExp.Bool false) 
	| "@"; "selector"; "("; n = LIDENT; o = OPT [ ":"; l = LIST0 [ n = LIDENT; ":" -> n]
					    -> l] ; ")" -> 
					      GGExp.mk _loc (GGExp.Selector(n,o))
	| "("; e = expr; ")" -> e  
	| x = ident -> 
	    GGExp.mk _loc (GGExp.Variable x)
	| "{" ; l = LIST1 [ n = LIDENT; "="; e = expr -> (n,e) ] SEP ";" ; "}" ->
	    GGExp.mk _loc (GGExp.StructBuild l)
	| "[" ; e = expr ; ";" ; l = LIST0 [ e = expr -> e ] SEP ";" ; "]" ->
	    GGExp.mk _loc (GGExp.ArrayBuild (e::l))
	| "[" ; o = expr ; name = LIDENT; 
	    l = OPT [":" ;e = expr ; 
		     l = LIST0 [name = LIDENT ; ":" ; e = expr -> name,e] 
		     -> e,l
		   ] ; "]" -> 
	    GGExp.mk _loc (GGExp.MessageCall(o,name, l))
	]

    ] 
;
parse_arg:
[
 [r = LIST0 expr SEP "," -> r]
]
;
END




let apply (fileName:string) (s:char Stream.t)  = 
  Gram.parse prog_eoi (Camlp4.PreCast.Syntax.Gram.Loc.mk fileName)  s 

let parse_string (s:string) = 
    apply "inline" (Stream.of_string s)