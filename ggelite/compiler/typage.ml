open Utile
open Ggtree
open Compile
open Gencode

module MProg = ProtoTree.GGProg
module MTypeDecl = ProtoTree.GGTypeDecl
module MExp = ProtoTree.GGExp
module MType = ProtoTree.GGType
module MCommand = ProtoTree.GGCommand

let apply_opt f x =
  match f with
    Some blu -> Some (blu x)
  | None -> None

let opt_apply f x =
  match x with
    Some blu -> Some (f blu)
  | None -> None

include 
    struct
      open GGType
       
      let rec ggtype t env = match t with
	MType.Unit -> Unit, env
      |	MType.Id -> Id, env
      |	MType.Bool -> Bool, env
      | MType.Integer -> Integer, env
      |	MType.String -> 
	  let t = EnvironmentSimple.lookupType dummy_loc "NSString" env in
	  t, env
      | MType.Float -> Float,env
      |	MType.Array (n, t) -> 
	  let t,env = ggtype t env in
	  Array (ref n, t),env
      |	MType.Struct l -> 
	  let l,newenv = List.fold_right
	      begin
		fun  (s,t) (l,env)->
		  let t,env' = ggtype t env in
		  (s,t) :: l, env'
	      end  l ([], env)
	  in
	  (GGType.Struct l), newenv
      |	MType.Name (x, loc) -> EnvironmentSimple.lookupType loc x env, env 
      |	MType.Abstract (x, loc) ->
            begin
                try EnvironmentSimple.lookupType loc x env, env  with
                GGStdpp.Exc_located _ -> Abstract x, env
            end
      |	MType.Pointer x -> 
	  let t,env = ggtype x env in
	  Pointer t,env
      |	MType.Enum l -> 
            let my_enum = EnumString.mk l in
	  let t = GGType.Enum my_enum
	  in 
	  let enums = 
	    List.fold_left 
	      begin 
		fun enums x ->  StringMap.add x t enums
	      end  env.EnvironmentSimple.enums l
	  in t, {env with EnvironmentSimple.enums = enums}
        | MType.Function (l, t) -> 
          let empty_name = "" in 
	  let l,newenv = List.fold_right
	      begin
		fun  t (l,env)->
		  let t,env' = ggtype t env in
		  (empty_name,t) :: l, env'
	      end  l ([], env)
	  in            
          let ret_type,newenv = ggtype t newenv in
          Function (ret_type, l), newenv
    end


module TypeExpr = 
  struct
    open GGType
    open GGExp
      
    let rec expr e env = 
      let loc = e.MExp.loc in
      let nop = GGCommand.mk GGCommand.Nop in
      let b descr t = 
	mk ~loc  descr t in
      match e.MExp.descr with
      	MExp.Int x -> b (Int x) Integer, nop
      | MExp.Float x -> b (Float x) GGType.Float, nop
      | MExp.Bool v -> b (Bool v) GGType.Bool, nop
      | MExp.Nil -> b Nil Id, nop
      | MExp.String (s) -> 
	  b (String s)  (EnvironmentSimple.lookupType loc "NSString" env), nop
      | MExp.Symbol (x,t) -> 
	  let t,_ = (ggtype t env) in
	  b (Symbol x) t , nop
      | MExp.Variable x ->
	  let v = (EnvironmentSimple.lookupVar loc x env) in
	  v, nop
      | MExp.Function (f, l) ->
	  Compile.build_fun_call (expr f env)
	    (List.map (fun e -> expr e) l ) loc env
      | MExp.Binop (op, x, y) ->
	  Compile.binop_expr op (expr x) (expr y) loc env
      | MExp.Monop (op, x) ->
	  Compile.monop_expr op (expr x) loc env
      | MExp.ArrayAccess(x, ind) ->
	  Compile.build_array_acces (expr x) (expr ind) loc env
      | MExp.StructAccess (x, field) ->
	  Compile.build_field_access (expr x)  field loc env
      | MExp.Selector (x,o) ->
	  let s = match o with
	  |Some l -> 
	      let rec iter s = function
		| a::l -> 
		    let s = s ^ ":" ^ a in
		    iter s l
		| [] -> s ^ ":"
	      in 
	      iter x l
	  | None -> x in
	  let t = EnvironmentSimple.lookupType loc "SEL" env in
	  b (GGExp.Symbol ("@selector(" ^ s ^ ")")) t, nop
      | MExp.MessageCall (o, name, opt ) -> 
	  let opt = match opt with 
	    Some (e, l) ->
	      let l = List.map (fun (s,e) -> (s, expr e)) l in
	      Some (expr e, l)
	  | None -> None
	  in
	  Compile.Message.build (expr o) name opt loc env
      | MExp.StructBuild l ->
	  let (c, l, lt) = 
	    let rec iter c le lt = function
	      | (n,e) :: l ->
		  let e,c' = expr e env in
		  let t = (n, e.thetype) in
		  iter (c @@ c') ((n, e) :: le) (t :: lt) l
	      | [] -> (c, le, lt)
	    in
	    let (c, l, lt) = iter nop [] [] l in
	    (c, List.rev l, List.rev lt)
	  in
	  b (StructBuild l) (GGType.Struct lt), c
      |	MExp.ArrayBuild (a :: l) ->
	  let e,c = expr a env in
	  let t = e.thetype in
	  let rec iter c le = function
	    | a :: l ->
		let e,c' = expr a env in
		if not (GGType.isEqual t e.thetype) then
		  failwithloc e.loc ("inconsistent type in array " ^ (GGType.to_string_debug 0 t) ^ " different from " ^ (GGType.to_string_debug 0 e.thetype));
		let c = c @@ c' 
		and le = e :: le in
		iter c le l
	    | [] -> (c,le) in
	  let (c, le) = iter nop [] l in
	  b (ArrayBuild(e :: (List.rev le))) (GGType.Array(
					      ref (Some ((List.length le)+1)),
					      t)), c
      |	MExp.ArrayBuild [] ->
	  failwithloc loc "impossible d'avoir un tableau de taille nulle"

  end



include 
    struct 
      open EnvironmentSimple
      open GGCommand
      open TypeExpr

	(*
	   Cette fonction va faire des choses un peu space.
	 *)

      let add_break env = 
	let handler_state = StampMap.map
	    (function 
		{ hs_break = Snobreak } as x ->
		  { x with hs_break = Sbreak }
	      |	x -> x) env.handler_state in
	{env with handler_state = handler_state}

      let check_empty_break loc hd =
	match hd.hd_answer_type with
	| Some  GGType.Unit  -> ()
	| Some _  -> failwithloc loc "returned value expected in this handler"
	| None -> hd.hd_answer_type <- Some( GGType.Unit)

      let rec command_s c env = 
	let loc = c.MCommand.loc in
	match c.MCommand.descr with
	| MCommand.If (e, c1, c2) ->
	    let e1,com1 = Compile.check_bool
		(expr e env) in
	    let c1, env1 = command_list c1 env
	    and c2, env2 = command_list c2 env in
	    let com = 
	      com1 @@ mk ~loc  ( If(e1, c1, c2))
	    in
	    let env =
	      { env with unreachable_code = 
		env1.unreachable_code && env2.unreachable_code }
	    in
	    com, env
	| MCommand.Block l ->
	    let c, _ = command_list  l env in
	    c, env
	| MCommand.While(e, c) ->
	    Compile.build_while (expr e env) (command_list c) loc env

	| MCommand.Break ->
	    let com,lst = match EnvironmentSimple.lookupBreakable loc env with
	    | BreakingLoop (loop, lst, _) ->
		GGCommand.mk ~loc 
		   (GGCommand.Jump (loop.Loop.breakLabel, loop.Loop.loopid)),
		lst
	    | BreakingHandler (hd, lst, _) ->
		hd.hd_do_break <- true;
		let hs = EnvironmentSimple.lookup_state loc hd env in
		let com = match hs with
		| { hs_answer = Some (e,c)} ->
		    if has_break hs then
		      failwithloc loc "cannot deal with non-atomic handler.  Wait for next release";
		    c @@
		    mk ~loc (Answer_and_jump (e, hd.hd_listening_var.Symbol.tag, 
				hd.hd_id))
		| _ ->
		    check_empty_break loc hd;
		    mk ~loc (Jump (hd.hd_listening_var.Symbol.tag, 
				   hd.hd_id))
		in
		com,lst
	    in
	    let cl = mk ~loc (GGCommand.BlockToPop lst)  @@ com in
	    let env = { env with unreachable_code = true } in
	    cl,env
	| MCommand.Resume ->
	    let com,lst = 
	      let hd,lst = EnvironmentSimple.lookup_handler loc env  in
	      hd.hd_do_continue <- true;
	      let hs = EnvironmentSimple.lookup_state loc hd env in
	      if has_break hs then
		failwithloc loc "continue after a non-atomic block";
	      let c = 
		match hs.hs_answer with
		  Some (e,c) -> 
		     c @@ GGCommand.mk ~loc (Return_Message (Some e))
		| None -> 
		    check_empty_break loc hd;
		    GGCommand.mk ~loc  (Return_Message None)
	      in
	      c,lst
	    in
	    let cl = GGCommand.mk ~loc (BlockToPop lst) @@  com in
	    let env = { env with unreachable_code = true } in
	    cl,env

	| MCommand.Detach(f, l, o) ->
	    let (l, c) = 
	      let rec iter l1 c = function
		| e :: l ->  
		    let (e,c') = expr e env in
		    iter (e :: l1) (c @@ c') l
		| [] -> (List.rev l1, c) in
	      iter [] nop l in
	    let c',env =  
	      try Compile.Detach.build loc f l o env with
		Not_found -> assert false
	    in
	    c @@ c', env
	| MCommand.RegisterCont (e, n) ->
	    let (e,c) = expr e env in
	    let c',env = Compile.Register.build loc e n env in
	    c @@ c', env
	| MCommand.Continue ->
	    let com,lst = 
	      let loop,lst = EnvironmentSimple.lookup_loop loc env in
	      GGCommand.mk ~loc 
		(Jump (loop.Loop.continueLabel, loop.Loop.loopid)),
	      lst
	    in
	    let cl = GGCommand.mk ~loc (BlockToPop lst) @@  com in
	    let env = { env with unreachable_code = true } in
	    cl,env

	| MCommand.Answer e -> 
	    let (e,c) as ans = expr e env in
	    let hd, pop = EnvironmentSimple.lookup_handler loc env in
	    let hs = EnvironmentSimple.lookup_state loc hd env in
	    let () = 
	      match hs,hd with
	      | { hs_break = Sbreak },_->
		  failwithloc loc "answer after a non atomic command"
	      | { hs_answer = Some _ },_ ->
		  failwithloc loc "answer already given in this path"
	      | _,{hd_type = NoAnswer} ->
		  failwithloc loc "answer not allowed in such handler"
	      | _,{hd_answer_type = None} -> 
		  hd.hd_answer_type <- Some e.GGExp.thetype
	      | _,{hd_answer_type = Some t} when not 
		    (GGType.isEqual t e.GGExp.thetype) ->
		      failwithloc loc "incompatible type in answer"
	      | _ -> ()
	    in
	    let env = {env with handler_state =
		       StampMap.replace hd.hd_id 
			 { hs with hs_answer = Some ans }
			 env.EnvironmentSimple.handler_state} in
	    mk ~loc Nop, env
	| MCommand.Expr (e) -> 
	    let e,c = expr e env in
	    c @@ (mk ~loc (Expr(e))), env
	| MCommand.Assign (x, e) -> 
	    Compile.Assign.build (expr x) (expr e) loc env
	| MCommand.Set_timer (t, e, n) ->
	    ListenBock.add_timer loc n (expr e env) t env
	| MCommand.Reschedule_timer (e,n)  ->
	    ListenBock.reschedule loc n (expr e env) env
	| MCommand.Wait (MCommand.Delay(e)) ->
	    let e,c = expr e env in
	    c @@ (mk ~loc (Wait(Delay e))),env
	| MCommand.Wait (MCommand.Stand_Wait(s)) ->
	    mk ~loc (Wait(Stand_Wait s)),
	    env
	| MCommand.Let l ->
	    let l = List.map 
		(fun (s,t,oe,owner) -> 
		  let oe = opt_apply expr oe in
		  Compile.build_dec s (ggtype t) owner ( oe) loc) l in
	    Compile.build_let l loc env
	| MCommand.Return e ->
	    let e = opt_apply  expr e in
	    Compile.build_return e loc env
	| MCommand.Listen 
	    {
	     MCommand.listen_com = coms;
	     MCommand.event_list = el;
	     MCommand.timer_list = pl;
	     MCommand.msg_list = ml;
	   } -> 
	     ListenBock.compile loc (command_list coms) 
	       (List.map compile_global_msg  el)
	       (List.map compile_timer_msg  pl) 
	       (List.map compile_private_msg  ml) 
	       env

	| MCommand.Match (e, l, def) ->
	    let l = List.map (fun (e,c,loc) -> (expr e env, 
						fst(command_list c env), loc)) l in
	    let def = fst(command_list def env) in
	    Compile.build_match loc (expr e env) l def env

      and command_list l env = 
	let stamp = Stamp.mk () in
	let env = {env with block_stack = 
		   (CommonBlock (stamp), stamp) ::
		   env.block_stack} in
	let rec iter env = function
	    a :: [] -> command_s a env
	  | [] -> nop, env
	  | a :: l -> 
	      let a,env = command_s a env in
	      if env.unreachable_code then
		let x = List.hd l in
		warn_with_loc x.MCommand.loc "unreachable code";
		a,env
	      else
		let env = match a.atomicity with
		  Atomic -> env
		| NonAtomic -> add_break env
		in
		let b,env = iter env l in
		a @@ b, env
	in iter env l

      and compile_timer_msg a env  = match a with 
	  {MCommand.priv_msg_name = s;
	   MCommand.priv_com_list = c;
	   MCommand.priv_args = args} -> 
	     let args = 
	       List.map (fun (n, t, loc) -> 
		 let t,_ = ggtype t env in (n,t,loc)) args in
	     (s, args, (fun env -> fst (command_list c env)))

      and compile_global_msg a env = match a with
	{MCommand.source_event = from;
	 MCommand.cond_event = c_e;
	 MCommand.message_event = name;
	 MCommand.args_event = args;
	 MCommand.com_event = coms} ->

	   let from,c_s = split_option (apply_opt (opt_apply expr from) env)
	   and cond = apply_opt (opt_apply expr c_e) env in
	   let l,env,comdec = List.fold_left
	       begin
		 fun (l,env,coms) (x,t,owner,loc) ->
		   let t,env = ggtype t env in
		   let () = 
		     if owner then
		       failwithloc loc  "not implemented yet"
		   in
		   let v = Symbol.mk ~lvalue: true 
		       ~alpha: true 
		       ~name: x t in
		   let newenv = {env with 
				 EnvironmentSimple.symbols = StringMap.add
				   x v env.EnvironmentSimple.symbols}
		   in
		   let dec = GGCommand.mk ~loc 
		       (GGCommand.Let v) in
		   let init = GGCommand.mk ~loc 
		       (GGCommand.SetNotVar(v,x)) in
		   let c = init @@ coms in
		   let c = dec @@  c in
		   (x,t,v.Symbol.tag) :: l, newenv, c
	       end
	       ([],env,nop) args in
	   let coms,_ = command_list coms env in
	   let com_event = comdec @@ coms in
	   (cond, from, name, com_event, l), c_s
      and compile_private_msg a env = match a with
	{
	 MCommand.pub_msg_name = main_name;
	 MCommand.pub_msg_args = pers_msg_args;
	 MCommand.pub_msg_com = pers_msg_com;
       } ->

(* we generate the code that will forward the message if it is 
   possible*)
	 let module M =      
	   struct
	     open GGType
	     open GGExp
	     open GGCommand

	     module E = GGExp
	     module T = GGType
	     let mke = E.mk
	     let mks s = mke (GGExp.Symbol s) Unit
	     let handle = mke (GGExp.Symbol "GGHandle") Id

	     let hd,_ = EnvironmentSimple.lookup_handler dummy_loc env 

	     let forward args names = 
	       let opts = match args,  names with
	       | [], [_] -> None
	       | a :: l, n::l' -> 
		   let s = Symbol.to_exp a in
		   let l = List.map2 (
		     fun name v ->
		       let e = Symbol.to_exp v in
		       (name, e)) l' l in
		   Some(s,l)
	       | _ -> assert false
	       in
	       mk (Return_Message (
		   Some (mke (MessageCall(handle, main_name, opts)) Unit)))
		 
	     let test args names com = 
	       let loc = com.loc in
	       let mk = mk ~loc 
	       and mke = mke ~loc in
	       let com2 = 
		 match hd.hd_answer_type with
		 | Some Unit ->
		     let debug_com = 
		       if true then
			 mk (Expr( mke (Symbol("NSWarnLog(@\"personal message to a dead object\")")) T.Unit)) 
		       else
			 nop in
		     mk 
		       (If(mke (Symbol "! [self isAlive]") T.Bool,
			   debug_com @@ 
			   mk (Return_Message None), nop))
		 | _ -> 
		     nop in
	       let c = mk  (
		 If(mke (Monop (Not, mke (Var hd.hd_listening_var) Id)) T.Bool,
		    mk (If(handle, forward args names, mk Abort)),
		    nop))
	       in
	       com2 @@ c @@ com
	   end   in

	 let name, args, env= 
	   match pers_msg_args with
	     Some ((t, s,loc) as first_arg, l) ->
	       let rec iter ln la  env  = function
		 | (name, (t,s,loc)) :: l -> 
		     let name = name ^ ":" in
		     let t, env = ggtype t env in
		     let v = Symbol.mk ~alpha: true ~lvalue: true ~name: s t in
		     let id = v.Symbol.tag in
		     let env = {env with 
				EnvironmentSimple.symbols = StringMap.add
				  s v env.EnvironmentSimple.symbols}
		     in
(*je ne veux pas mettre de declaration ici*)

(*		   let dec = GGCommand.mk ~loc 
   (GGCommand.Let(s,t,id,false)) in
   let coms = coms @@ dec in
 *)
		     iter  (name :: ln) (v :: la)  env l
		 | [] -> (ln, la, env)
	       in
	       
	       
	       let l = (main_name, first_arg) :: l in
	       let (names, args, env) = iter [] [] env l in
	       let names = List.rev names
	       and args = List.rev args in
	       names, args, env
	   | None ->
	       [main_name], [], env in
	 let com2,_ = command_list pers_msg_com env in
	 let coms = M.test   args name com2 in
	 let rettype = match M.hd.hd_answer_type with
	 | Some t -> t
	 | None -> GGType.Unit in
	 (name, args, rettype, coms)
    end




(*let arg_dec (x, o, t, loc) env =  (x, o, ggtype t, loc) *)


let fonction_decl a env = match a with
  {
   ProtoTree.name_fun = x;
   ProtoTree.args_fun = l;
   ProtoTree.rettype_fun = t;
   ProtoTree.loc_fun = loc;
 } -> 
   let l', newenv = 
     List.fold_right 
       begin
	 fun (x, o, t, loc)  (l,env) ->
	   let t, env = ggtype t env in
	   (x,o,t) :: l, env
       end 
       l ([],env)
   in
   let (t, newenv) = ggtype t newenv in
   (x,l',t), newenv,loc


let extern recursif a env = 
  let f e = if recursif then [] else [e] in
  match a with
    ProtoTree.ExternFunction 
      {
       ProtoTree.complex_ef = complex;
       ProtoTree.f_decl_ef = f_decl;
       ProtoTree.external_name_ef = cn;
     } ->
       let ((x,l,t), env', _) = fonction_decl f_decl env in
       let l = List.map (fun (a,_,b) -> (a,b)) l in
       let cn = match cn with
	 Some y -> y
       | _ -> if complex then "Complex_" ^ x else x in
       let t' = (GGType.Function(t, l)) in
       let v = Symbol.mk ~complex ~alpha: false ~name: cn ~lvalue: false t' in
       let newenv = {env' with EnvironmentSimple.symbols = StringMap.add
		       x v env.EnvironmentSimple.symbols} in
       let data = 
	 {
	  GGProg.name = x;
	  GGProg.cname = cn;
	  GGProg.thetype = t';
	  GGProg.complex = complex;
	} in
       (newenv, f (GGProg.Extern data))
  | ProtoTree.ExternVariable 
      {
       ProtoTree.name_vt = x;
       ProtoTree.type_vt = t;
       ProtoTree.external_name_vt = cn;
     } ->
       let cname = match cn with
	 Some y -> y
       | None -> x in
       let t,env = ggtype t env in
       let v = Symbol.mk ~alpha: false ~name: cname ~lvalue: true t  in
       let newenv = {env with EnvironmentSimple.symbols = StringMap.add
		       x v env.EnvironmentSimple.symbols} in
       let data = 
	 {
	  GGProg.name = x;
	  GGProg.cname = cname;
	  GGProg.thetype = t;
	  GGProg.complex = false;
	} in
       (newenv, f (GGProg.Extern data))

let fonction_simple (f, e, loc) env = 
       let (x,l,t),env,_ = fonction_decl f env in
       let (symbols, stamps, targ) = List.fold_right (fun (a,_,b) (e,l2,l4) -> 
	 let v = Symbol.mk ~alpha: true 
	     ~lvalue: true ~name:a b in
	 (StringMap.add a v e,
	  (v.Symbol.tag) :: l2, 
	  (a,b) :: l4))
	   l (env.EnvironmentSimple.symbols,[],[]) 
       and rettype = t  in
       let newenv = { env with 
		     EnvironmentSimple.symbols = symbols} in
       Compile.Inline.mk 
	 ~loc  (TypeExpr.expr e newenv) stamps (GGType.Function(rettype, targ))  x


include
    struct
      open ProtoTree.GGFonction
      let fonction f env = match f with
	{
	 loc = loc;
	 f = f_decl;
	 command = c;
       } ->
	  Compile.build_function loc (fonction_decl f_decl env) (command_list c)
	  env
    end


include 
  struct
    open ProtoTree.GGProg
    open Abbreviation

    let compile_type env t = fst (ggtype t env )

    let fonction_or_typedecl recursif x env =   match x with
      DecType {ProtoTree.GGTypeDecl.descr = x, t;
	       ProtoTree.GGTypeDecl.loc = loc} ->
	let t,env = ggtype t env 
	in
	EnvironmentSimple.add_type (GGTypeDecl.mk ~loc (x, t))  env
      |	Function f ->
        EnvironmentSimple.add_function (fonction f env) env
      |	Inline f -> EnvironmentSimple.add_inline  (fonction_simple f env) env
      |	Extern x -> extern recursif x env
      |	Import (x,loc) ->
        fst (Objc.import recursif x loc env), [GGProg.Import x]
      |	Const (x,t,e) -> 
	  let t,env = ggtype t env in
	  let e,l = TypeExpr.expr e env in
	  let v = Symbol.mk ~alpha: false ~name: x ~lvalue: false  t in
	  {env with EnvironmentSimple.symbols =  
	   StringMap.add x v env.EnvironmentSimple.symbols},
	  (if recursif then [] else [ GGProg.Const (x,t,e)])
      | LetGlobal (x,t,e) ->
	  let t,env = ggtype t env in
	  let v = Symbol.mk ~lvalue: true ~alpha: true ~name:x t in
	  let loc = e.MExp.loc in
	  let f env = 
	    let (e,c) as ret = TypeExpr.expr e env in
	    if c.C.atomicity = C.NonAtomic then
	      failwithloc e.E.loc "expression should be atomic in global initialisation";
	    ret in
	  let c,_ = Compile.Assign.build
	      (fun env -> Symbol.to_exp ~loc v,  GGCommand.nop)
	      f
	      loc
	      env in
	  {env with EnvironmentSimple.symbols =  
	   StringMap.add x v env.EnvironmentSimple.symbols},
	  (if recursif then [] else [ GGProg.LetGlobal (v,x,c)])
      | ClassForward (className,loc)  -> Objc.compile_forward loc className env, []
      | ClassInterface (objc_interface, loc) ->
        Objc.compile  compile_type loc objc_interface env, []
  end


let compile_prog (recursif:bool) l env = 
  List.fold_left 
    (fun (env,ol) f -> 
      let e,o = fonction_or_typedecl recursif f env in
      (e, o :: ol)) 
    (env,[]) l

let priv_parser_proto_tree recursif l env = 
  let env,l = compile_prog recursif l env in
  env, (List.flatten (List.rev l))
        
let () = Objc.parse_proto_tree := priv_parser_proto_tree true

let priv_parser fileName recursif s env = priv_parser_proto_tree recursif (Parse.apply fileName s) env 

let the_parser fileName s env =  priv_parser fileName false s env

let () = 
  Objc.parse_proto_tree := priv_parser_proto_tree true
