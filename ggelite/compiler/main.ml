open Utile
open Ggtree
open Gencode
open Compile
open Camlp4.PreCast




(*s Fichier source courant. *)


let set_file f = Output.Programme.filename := f

(*s Transformation d'une position absolue en une position du type [t]. *)


let vectType = 
  GGTypeDecl.mk 
    ("Vect3D", ( (GGType.Struct(["x",  GGType.Float;
				 "y",  GGType.Float;
				 "z",  GGType.Float;
			       ]))))

let envType = 
  let add_obj s env =
    let t = GGType.mk_objet s in
    let decl = GGTypeDecl.mk (s, t) in
    StringMap.add s decl env
  in
  let env =  StringMap.add "Vect3D" vectType (StringMap.empty) in 
  let env = add_obj "GGTimer" env in
  let env = add_obj "NSString" env in
  let env = 
    let s = "SEL" in
    let t = GGType.Pointer (GGType.Unit) in
    let decl = GGTypeDecl.mk (s, t) in
    StringMap.add s decl env in
  env



let vect3D = GGTypeDecl.type_of vectType
						       
include 
  struct
    open GGExp
    open GGType
    open Operator

    let def_op = 
      let l = [mk Plus Float;
	       mk Minus Float;
	       mk Prod Float;
	       mk Div Float;
	       mk_mono Minus Float;
	       mk Lesser ~tres: Bool Float;
	       mk LesserEq ~tres: Bool Float;
	       mk Greater ~tres: Bool Float;
	       mk GreaterEq ~tres: Bool Float;

	       mk Plus Integer;
	       mk Minus Integer;
	       mk Prod Integer;
	       mk Div Integer;
	       mk_mono Minus Integer;
	       mk_mono Not Bool;

	       mk And Bool;
	       mk Or Bool;
	       mk Equal ~tres: Bool Integer;
	       mk Notequal ~tres: Bool Integer;
	       mk Lesser ~tres: Bool Integer;
	       mk LesserEq ~tres: Bool Integer;
	       mk Greater ~tres: Bool Integer;
	       mk GreaterEq ~tres: Bool Integer;

	       mk Equal ~check: Operator.Default.check_hack  ~tres: Bool Id;
	       mk Notequal ~check: Operator.Default.check_hack  ~tres: Bool Id;


	       build_op_rewrite Plus "addVect" (Reference,vect3D);
	       build_op_rewrite Minus "diffVect" (Reference,vect3D);
	       build_op_rewrite ProdVect "prodVect" (Reference,vect3D);
	       build_op_rewrite Prod "my_mulScalVect" 
		 ~arg2:(Reference,vect3D) ~ret:(Reference,vect3D) (Direct, Float);

	       build_op_rewrite Prod "prodScal" 
		 ~ret:(Direct, Float) (Reference,vect3D);
	       build_op_rewrite Div "divScalVect"
		 ~ret:(Reference, vect3D) ~arg2:(Direct,Float)
		 (Reference,vect3D)
		      
 
	     ] in
      List.fold_left (fun env (op,a) -> 
	try 
	  let l = OpMap.find op env in
	  l := a :: !l;
	  env
	with
	  Not_found -> OpMap.add op (ref [a]) env) OpMap.empty l
    
  end

include
    struct
      open EnvironmentSimple
	
      let def_env = 
	Gglog.add_functions
	  {EnvironmentSimple.empty with types = envType; operators = def_op}

      open Symbol
      let build_env_stamp env =
	let table = Hashtbl.create 10 in
	StringMap.iter (fun key v ->
(*	  Printf.eprintf "val = %s id = %d\n" v.name v.tag;*)
	  v.localisation <- (if v.complex then Variable.ComplexExternalFunc
	  else Variable.GlobalVar);
	  Hashtbl.add table v.tag v
		       ) env.symbols;
	table
    end
	       

let localized_msg = GGStdpp.localized_msg

let print_warning () =
  let rec sub = function
      (loc,s) :: l ->
	let sloc = localized_msg loc in
	let s = "Warning: "^s in
	Printf.eprintf "%s\n%s\n" sloc s;
	sub l
    | [] -> ()
  in
  sub (List.rev !Utile.warnings)
	

let input_file = ref None
let output_file = ref None
let output_file_dec = ref None

module M = 
  struct
    open Arg
    open Filename

    let my_chop_extension s = 
      try 
	chop_extension s
      with
	Invalid_argument _ -> s

    let blob = ref (fun () -> ())
    let blu = ref false
	
    let _out_file (s : string) = 
      let base = my_chop_extension s in
      Output.Programme.basename := base;
      Output.Programme.headername := (base ^ ".h");
      fun () ->
	output_file_dec := Some (open_out !Output.Programme.headername);
	output_file := Some (open_out (base ^ ".m"))

    let out_file s =
      blob := _out_file s;
      blu := true

    let read_file (s : string)  = 
      begin
	set_file s;
	input_file := Some ((open_in s), s);
	if not !blu then
	  out_file ((chop_extension s) ^ ".m")
      end

    let add_dir (s : string) =
      Objc.lstdir := s :: !Objc.lstdir

    let l = [ "-o", String out_file, "name of the output file" ;
	      "-I", String add_dir, "will search in this directory too";
	      "-d", Set debug_code, "add more debugging code in the output"
	    ]

    let parse_arg () = parse l read_file "blabla"
  end

let _ = 
  M.parse_arg ();
  match !input_file  with
    Some (inp, fileName) -> 
      let s = Stream.of_channel inp 
      in 
      begin
	try 
          let env,olist = Typage.the_parser fileName s def_env in
	  let new_env = build_env_stamp env in
	  let () = !M.blob ()
	  in
	  let c_m, c_h = 
	    match !output_file, !output_file_dec with
	      Some c, Some c' -> c, c'
	    | _ -> failwith "impossible"
	  in
	  Output.Programme.to_string new_env c_m c_h  olist;
	  close_out c_m;
	  close_out c_h;
	  print_warning ();
	with 
	  GGStdpp.Exc_located(loc, exn) -> 
	    let s = 
	      match exn with
		Failure s -> s
	      | e -> Printexc.to_string e
	    in
	    print_warning ();
	    let s' = localized_msg loc in
	    Printf.printf "%s\n%s\n" s' s;
	    exit 1
        | Loc.Exc_located (a, b) -> Printf.eprintf "error: %s  %s" (string_of_loc a) (Printexc.to_string b); exit 1
      end
  | _ -> prerr_string "no file name given\n"
	

  
