open Utile
open Ggtree
include Decgen



module Hashtbl2 = 
  struct
    include Hashtbl

    let add t (a: Stamp.t)  b =
      if mem t a then
	Utile.Log.printf "%d is already there\n" (Stamp.hash a);
      add t a b
  end

module ExpandConti = 
  struct
    open CommandC
    open Continuation


    let transform_aux transform c1 c2  = 
      let c1 = transform c1 in
      let c2 = transform c2 in
      match c1, c2 with
      | Atom c1, Atom c2 -> Atom (c1 @@ c2)
      | Atom c1, (Multi block as m) -> 
	  block.debut <-  c1 @@ block.debut;
	  m
      | (Multi block) as m, c ->
	  let () = match c with
	    Atom a -> Continuation.add block.fin a
	  | Multi b2 -> 
	      Continuation.add block.fin b2.debut;
	      block.block_list <- b2.block_list @ block.block_list;
	      block.fin <- b2.fin 
	  in
	  m

    let manage_if (lab_hash : (Stamp.t,Label.t) Hashtbl2.t) tr e tclause fclause =
      let tres = tr  tclause
      and fres = tr  fclause in
      match tres,fres with
	Atom tres' ,Atom  fres' -> 
	  Atom (If (e, tres',fres'))
      | tt,ff ->
	  let fin = Continuation.mk () in
	  let lab,id = Label.mk ~name: "finif" ~top:true ~cont:fin 
	      ~hash_table:lab_hash ()
	  in
	  let jump = Jump id in
	  let blobi l = function
	      Atom l' -> l ,  l' @@ jump
	    | Multi block -> 
		Continuation.add block.fin jump ;
		(block.block_list@l, block.debut) in
	  let blt, ttl = blobi [] tt in
	  let blf, ffl = blobi blt ff in
	  let debut = If(e, ttl, ffl) in
	  let block = {debut=debut;
		       fin = fin;
		       block_list = fin::blf} in
	  fin.comlist <- Label id;
	  Multi block

    let type_conti = GGType.mk_objet "NSNotification"

    let type_block = GGType.mk_objet "ListeningBlock"

    let type_timer = GGType.mk_objet "GGTimer"
      
    let transformation (lab_hash : (Stamp.t,Label.t) Hashtbl2.t) 
	(l : GGCommand.t)  = 
(*      let rec transform_list x =  transform_list_aux transform x *)

      let rec transform_event  loc (lst1,lst2) = function
	  {
	   GGCommand.args_event = a_e;
	   GGCommand.source_event = s_e;
	   GGCommand.cond_event = cont_event;
	   GGCommand.message_event = m_e;
	   GGCommand.com_event = c_e } ->
	     let arg = Symbol.mk ~alpha: false
		 ~lvalue: false 
		 ~name: "aNotification" type_conti in
	     let new_cont = Continuation.mk 
		 ~message:("notification_" ^  m_e ^ ":") 
		 ~arg
		 () in
	     let label,id = Label.mk ~top:true ~name:"message" ~cont: new_cont
		 ~hash_table: lab_hash () in

	     let cl,lst = 
	       match transform c_e
	       with
		 Atom l ->  l , [new_cont]
	       | Multi m -> 
		   m.debut, new_cont :: m.block_list
	     in

	     let coms = match cont_event with
	       Some(e,l) ->
		 let l = match transform l with
		   Atom x -> 
		     x
		 | Multi _ -> 
		     Printf.printf "la\n";
		     GGStdpp.raise_with_loc e.GGExp.loc
		       (Failure "expr is too complicated")
		 in
		 Label id @@ l @@ If(e, cl, SimpleReturn None)
	     |  None -> Label id @@ cl in
		     
	     Continuation.add new_cont coms;
	     {
	      message_event = m_e;
	      action_label = id;
	      source_event = s_e} :: lst1, lst @ lst2
	      
      and transform_timer   loc lst1 = function
	  {GGCommand.timer_used = GGCommand.Nope} :: l -> 
	    warn ~loc "block unreachable because no timer can fire to there";
	    transform_timer  loc lst1  l
	| {GGCommand.timer_com_list = com_list;
	   GGCommand.timer_id = id;
	 } :: l ->
	   let arg = Symbol.mk 
	       ~alpha: false
	       ~lvalue: false
	       ~name: "timer"
	       type_timer in
	   let new_cont = Continuation.mk ~message:("timer:") 
	       ~arg
	       () in
	   let label,id = 
	     Label.mk ~id  ~top:true ~name:"message" ~cont: new_cont
	       ~hash_table: lab_hash () in
	   Continuation.add new_cont ( Label id);
	   let cl, lst =
	     match transform com_list with
	       Atom l ->  l , [new_cont]
	     | Multi m ->
		 m.debut, new_cont :: m.block_list
	   in
	   Continuation.add new_cont cl;
	   transform_timer  loc 
	     (lst @ lst1) l
	| [] -> lst1

      and transform_prv loc res lst_id = function
	  {
	   GGCommand.prv_name = prv_name;
	   GGCommand.prv_args = prv_args;
	   GGCommand.prv_com = prv_com;
	   GGCommand.prv_rettype = rettype;
	 } :: l ->
	   let new_cont = Continuation.mk_message 
	       ~name:prv_name  
	       ~arg:prv_args 
	       ~rettype () in
	   let label,id = 
	     Label.mk  ~top:true ~name:"private_message" ~cont: new_cont
	       ~hash_table: lab_hash  () in
	   Continuation.add new_cont (Label id);
	   let cl, lst =
	     match transform prv_com with
	       Atom l ->  l , [new_cont]
	     | Multi m ->
		 m.debut, new_cont :: m.block_list
	   in
	   Continuation.add new_cont cl;
	   transform_prv  loc 
	     (lst @ res) (id :: lst_id) l
	| [] -> (res, lst_id)

      and	transform c = 
	match c.GGCommand.descr with
      	  GGCommand.Wait x -> 
	    let fin = Continuation.mk ~message: "resume" () in
	    let label,id = Label.mk ~top: true ~cont:fin ~hash_table:lab_hash ()
	    in
	    let jump = RegisterCont (id,to_wait x) @@ SimpleReturn None in
	    let block = {debut = jump;
			 fin = fin ;
			 block_list = []} in
	    block.block_list <- [block.fin];
	    fin.comlist <- Label id;
	    Multi block
	| GGCommand.If (e, tclause, fclause) ->
	    manage_if lab_hash transform e tclause fclause
	| GGCommand.Listen ({GGCommand.listen_com = listen_com;
			     GGCommand.glb_msg_list = event_list;
			     GGCommand.prv_msg_list = priv_list;
			     GGCommand.timer_msg_list = timer_list;
			     GGCommand.listen_id = idblock;
			     GGCommand.to_erase = to_erase;
			     GGCommand.clean_after = clean_after;
			   }) ->
			     let x =  transform listen_com in
			       
			       (match x with
				 Atom _ -> 
				   prerr_string "block d'ecoute inutile a ";
				   prerr_string (string_of_loc c.GGCommand.loc);
(*				   prerr_int (fst c.GGCommand.loc);
				   prerr_string " ";
				   prerr_int (snd c.GGCommand.loc);*)
				   prerr_string "\n"
			       | Multi block ->
				   let _ = Label.mk ~id: idblock
				       ~hash_table:lab_hash () 
				   in
				   let  lm,cont_lst = List.fold_left 
				       (transform_event   c.GGCommand.loc)
				       ([],[]) event_list 
				   in
				   let cont_lst, prv_lst =
				     transform_prv c.GGCommand.loc
				       cont_lst [] priv_list in
				   let cont_lst = transform_timer  
				       c.GGCommand.loc cont_lst timer_list in
(*				   Continuation.dump cont_lst;*)
				   let debut =  (RegisterMessage (lm, idblock, 
								 prv_lst))
				   and fin = UnregisterMessage clean_after in
				   block.debut <-  debut @@ block.debut;
				   block.block_list <- 
				     cont_lst @ block.block_list;
				   Continuation.add block.fin 
				     fin

			       );
			       x
	| GGCommand.Destroy_Job (id, l) -> Atom (Destroy_Job (id, !l))
	| GGCommand.Assign(e,f) -> Atom (Assign(e,f))
	| GGCommand.SetNotVar (a,b) -> Atom (SetNotVar(a,b))
	| GGCommand.Clean (l) -> Atom (Clean l)
	| GGCommand.Expr e -> Atom (Expr e)
	| GGCommand.Jump (l,_) -> Atom (Jump l)
	| GGCommand.Label id -> Atom (Label id)
	| GGCommand.Return e -> Atom (Return e)
	| GGCommand.CheckObject(a,b) -> Atom (CheckObject (a,b))
	| GGCommand.Set_timer (s, e, bl) -> 
	    Atom (Set_timer (bl.GGCommand.timer_id, e))
	| GGCommand.Reschedule_timer (e,bl) ->
	    Atom (Reschedule_timer (bl.GGCommand.timer_id, e))
	| GGCommand.Let v -> Atom (Let v)
	| GGCommand.Complex_call (f,args,res) -> 
(*	    let fin = 
	      match GGType.rettype_of_funtype f.GGExp.thetype with
		GGType.Unit -> Continuation.mk ~message: "partialReturn" ()
	      |	t -> Continuation.mk ~message: "partialReturn:" 
		    ~arg:(Variable.temp_name (), t, res) () 
	    in
*)
	    let fin =
	      match GGType.rettype_of_funtype f.GGExp.thetype with
	      | GGType.Unit -> Continuation.mk_cps  ()
	      |	t -> Continuation.mk_cps  ~arg: res () 
	    in

	    let label,id = Label.mk ~top: true ~cont: fin ~hash_table:lab_hash ()
	    in
	    let debut = Partial_call (f,args,id) in
	    let block = { debut = debut;
			  fin = fin;
			  block_list = [fin]} in
	    fin.comlist <- Destroy_Callee ;
	    Multi block
	| GGCommand.BlockToPop (lst) -> 
	    let n = List.length lst in
	    let coms = UnregisterMessage [] in
	    let rec build  res = function
		0 -> res
	      |	n -> build ( coms @@ res) (n-1) in
	    let coml = build Nop n in
	    Atom ( coml @@  Clean (GGCommand.convert [] lst))
	| GGCommand.Nop -> Atom Nop
	| GGCommand.Seq(c1, c2) -> transform_aux transform c1 c2
	| GGCommand.Abort -> Atom Abort
	| GGCommand.Return_Message(e) -> Atom (SimpleReturn e)
	| GGCommand.Answer_and_jump (e,l,_) ->
	    Atom(RegisterCont (l, Stand_Wait "NOP") @@
		 SimpleReturn (Some e))
      in
      transform l


    let transformation2 (lab_hash : (Stamp.t,Label.t) Hashtbl2.t) l = 
      let rec transform  = function
	  If (e, tclause, fclause) ->
	    manage_if lab_hash transform e tclause fclause
	| Label id as x ->
	    let l = Hashtbl2.find lab_hash id in
	    if l.Label.labelCorrect then 
	      Atom (x)
	    else
	      let fin = Continuation.mk () in
	      begin
		l.Label.labelCorrect <- true;
		l.Label.labelCont <- Some fin;
		l.Label.label_is_top <- true;
		fin.comlist <- x;
		let debut = Jump id in
		let block = {debut = debut;
			     fin = fin;
			     block_list = [fin]} in
		Multi block
	      end
	| Seq (c1, c2) -> transform_aux transform c1 c2
	| (Assign _
	| Nop
	| CheckObject _
	| SetNotVar _
	| Clean _
	| RegisterMessage _
	| UnregisterMessage _
	| Partial_call _
	| RegisterCont _
	| Destroy_Callee
	| Destroy_Job _
	| Expr _ 
	| Set_timer _
	| Reschedule_timer _
	| Jump _ 
	| Return _ 
	| SimpleReturn _
	| Abort
	| Let _) as x -> Atom (x)
      in
      transform l



    let mark_bad_label (lab_hash : (Stamp.t,Label.t) Hashtbl2.t) theset  = 
      function
	  { comlist = comlist } as cont ->
	  let rec iter_com set c = 
	    match c with
	      If (_,l1,l2) ->
		let s' = iter_com set l1 in
		iter_com s' l2
	    | SetNotVar _
	    | CheckObject _
	    | Clean _
	    | Return _
	    | SimpleReturn _
	    | Expr _
	    | Assign _
	    | Label _
	    | Partial_call _
	    | RegisterMessage _
	    | UnregisterMessage _
	    | Set_timer _
	    | Reschedule_timer _
	    | RegisterCont _
	    | Let _
	    | Nop
	    | Abort
	    | Destroy_Job _
	    | Destroy_Callee  -> set
	    | Seq (c1, c2) -> 
		let s = iter_com set c1 in
		iter_com s c2
	    | Jump id ->
		(match Hashtbl2.find lab_hash id with
		  ({ Label.labelCont = Some c } as label) ->
		    label.Label.used <- true;
		    if c != cont && not label.Label.label_is_top then
		      begin
			label.Label.labelCorrect <- false;
			ContSet.add c set
		      end
		    else
		      set
		| _ -> raise (Failure ("label with no cont: " ^ 
				      (string_of_int (Stamp.hash id))))
		)
	  in
	  iter_com  theset comlist


    let mark_used (lab_hash : (Stamp.t,Label.t) Hashtbl2.t) = 
      function
	  { comlist = comlist }  ->
	  let rec iter_com c = 
	    match c with
	      If (_,l1,l2) ->
		iter_com l1;
		iter_com l2
	    | SetNotVar _
	    | Clean _
	    | Return _
	    | SimpleReturn _
	    | CheckObject _
	    | Expr _
	    | Assign _
	    | Label _
	    | Partial_call _
	    | RegisterMessage _
	    | UnregisterMessage _
	    | Set_timer _
	    | Reschedule_timer _
	    | RegisterCont _
	    | Let _
	    | Nop
	    | Abort
	    | Destroy_Job _
	    | Destroy_Callee  -> ()
	    | Seq (c1, c2) -> iter_com c1; iter_com c2
	    | Jump id ->
		let label = Hashtbl2.find lab_hash id in
		label.Label.used <- true;
	  in
	  iter_com  comlist 


    let update_label (lab_hash : (Stamp.t,Label.t) Hashtbl2.t) = function
	{ comlist = comlist } as cont ->
	  let rec iter_com c = 
	    match c with
	      If (_,l1,l2) ->
		iter_com l1;
		iter_com l2
	    | SetNotVar _
	    | Clean _
	    | Expr _
	    | Assign _
	    | Return _
	    | SimpleReturn _
	    | CheckObject _
	    | Partial_call _
	    | Destroy_Callee 
	    | Destroy_Job _
	    | RegisterMessage _
	    | UnregisterMessage _
	    | Set_timer _
	    | Reschedule_timer _
	    | Let _
	    | RegisterCont _
	    | Nop 
	    | Abort
	    | Jump _  -> ()
	    | Seq (c1, c2) -> iter_com c1; iter_com c2
	    | Label id -> 
		let l = Hashtbl2.find lab_hash id in
		l.Label.labelCont <- Some cont
	  in
	  iter_com comlist

    open Variable
    open ComplexProc
    let  split_more = function
	{ cont_list = lst ; main_cont =  main ;
	  lab_hash = lab_hash} as proc ->
	  let rec sub_analyze cont_list =
	    List.iter (update_label lab_hash) cont_list;
	    let bad_cont = List.fold_left (mark_bad_label lab_hash) 
		ContSet.empty cont_list
	    in
	    if not (ContSet.is_empty bad_cont) then
	      let remaining = ContSet.fold
		  (fun e llcont -> 
		    match e with
		      { comlist = comlist } as the_cont ->
			match transformation2 lab_hash comlist with
			  Multi {debut = inst_list;
				 block_list = lst} ->
				   the_cont.comlist <- inst_list;
				   proc.cont_list <- proc.cont_list @ lst;
				   (the_cont :: lst) :: llcont
			| Atom _ -> raise (Failure "cont should be bad")
		  )  bad_cont []
	      in
	      sub_analyze (List.flatten remaining)
	  in
	  sub_analyze (main :: lst)

	    
    include 
	struct
	  open GGCommand
	    
	  let rec init_label (lab_hash : (Stamp.t,Label.t) Hashtbl2.t) 
	      (l:GGCommand.t) =
	    let rec set c = 
	      match c.descr with
		Label id -> 
		  if not (Hashtbl2.mem lab_hash id) then
		    let _ = Label.mk ~id ~hash_table:lab_hash () in ()
	      | If (_, a, b) -> set a; set b
	      |	Listen {listen_com = listen_com;
			glb_msg_list = glb_msg_list;
			timer_msg_list = timer_msg_list;
			prv_msg_list = prv_msg_list;} ->
			  init_label lab_hash listen_com;
			  List.iter
			    (
			     function
				 { com_event = l } ->
				   init_label lab_hash l
			    )
			    glb_msg_list;
			  List.iter
			    (function {timer_com_list = cl} -> 
			      init_label lab_hash cl)
			    timer_msg_list;
			  List.iter
			    (function {prv_com = c} -> init_label lab_hash c)
			    prv_msg_list

	      |	BlockToPop _
	      |	SetNotVar _
	      |	CheckObject _
	      |	Clean _
	      |	Assign _
	      |	Wait _
	      |	Expr _
	      |	Jump _
	      |	Let _
	      | Nop
	      |	Set_timer _
	      | Abort
	      |	Reschedule_timer _
	      |	Return_Message _ 
	      |	Answer_and_jump _ 
	      |	Complex_call _
	      | Destroy_Job _
	      |	Return _  -> ()
	      | Seq(c1, c2) -> set c1; set c2
	    in set l
	    
	end
	    
	    

    open GGFonction
    let expand  = function 
	{
	 command = command;
	 tag = tag
       }  -> 
	 
	 let proc = ComplexProc.mk tag in
	 init_label proc.ComplexProc.lab_hash command;
	 begin
	  match transformation proc.ComplexProc.lab_hash command with
	    Atom (l) -> 
	      proc.main_cont <- Continuation.mk ();
	      proc.main_cont.comlist <- l;
	  | Multi { debut = deb;
		    block_list = lst; } ->
		      let main_cont = Continuation.mk () in
		      proc.main_cont <- main_cont;
		      main_cont.comlist <- deb;
		      proc.cont_list <- lst;
		      split_more proc;
	 end;
	 List.iter (mark_used proc.ComplexProc.lab_hash) 
	   (proc.main_cont :: proc.cont_list);
	 Opt.MakeOpt.do_job proc;
	 proc

  end

module Transform =
  struct
    module MakeHashCom = 
      struct
	open GGCommand
	  
	  (* cette fonction peuple la table de hashage avec les declarations
	     de variables.

	     Elle extrude certaines commandes.
	   *)

	let run (env : (Stamp.t, Variable.t) Hashtbl2.t) =
	  
	  let rec  expand_glob_msg   = function 
	      {source_event = eopt;
	       cond_event = copt;
	       com_event = com_event;
	       args_event = args_event;
	     }  :: lst ->
	       let () = match copt with
		 Some(e,l) -> 
		   expand_exp  l
	       | x -> ()
	       in
	       expand_exp com_event;
	       expand_glob_msg  lst
	    | [] -> ()

	  and expand_priv_msg = function
	      {timer_com_list = com_list} ->
		expand_exp com_list

	  and expand_exp c = match c.descr with
	    If (e,l1,l2) -> 
	      expand_exp  l1;
	      expand_exp  l2 
	  | Let v  -> 
(* 	      let v = Variable.mkvar (\* ~owner *\) *)
(* 		  ("__" ^ (string_of_int (Stamp.hash id)) ^ *)
(* 		   "_" ^ s) t id in *)
(*	      Printf.eprintf "adding (%d ->  %s)\n"  
		(Stamp.hash v.Variable.vartag)
		v.Variable.varname;*)
	      Hashtbl2.add env v.Variable.tag v
	  | Listen ({ listen_com = listen_com;
		      glb_msg_list = glb_msg_list;
		      timer_msg_list = timer_msg_list;
		      prv_msg_list = prv_msg_list;
		      listen_id = id;
		   } )  ->
		     expand_glob_msg  glb_msg_list;
		     List.iter expand_priv_msg timer_msg_list;
		     List.iter (function {prv_com=c} -> expand_exp c) 
		       prv_msg_list;
		     expand_exp listen_com
	  | Expr _ 
	  | CheckObject _ 
	  | Assign _
	  | Return _
	  | Set_timer _
	  | Reschedule_timer _ 
	  | Abort
	  | SetNotVar _
	  | Clean _
	  | BlockToPop _
	  | Complex_call _
	  | Wait _
	  | Nop
	  | Jump _
	  | Return_Message _
	  | Answer_and_jump _
	  | Destroy_Job _
	  | Label _ -> ()
	  | Seq (c1, c2) -> expand_exp c1; expand_exp c2
	  in expand_exp



      end

    open GGFonction

    let transform env = function
	{command = l} -> 
	  MakeHashCom.run env l 

    let compile env f = 
      begin
	transform env f;
	let proc = ExpandConti.expand f in
	let var = Symbol.mk ~loc: (Variable.Function proc.ComplexProc.tag)
	    ~lvalue: false ~name: f.name ~tag:f.tag ~alpha: false
	    f.thetype  in
	Hashtbl2.add env var.Symbol.tag var;
	List.iter (function
	    { Continuation.arg = l }	(*    Some (name, t, id)*)  ->
	      List.iter (fun v ->
		v.Symbol.localisation <- Variable.ArgumentLoc;
		Hashtbl2.add env v.Symbol.tag var) l)
	  proc.ComplexProc.cont_list;

	proc 
      end
  end

module ComputeVariable =
  struct

    module WorkOnExp =
      struct 
	open Variable
	open GGExp

	let mark_label_var proc cont var_hash id =
	  try 
	    begin
	      match Hashtbl2.find var_hash id with
(*	      	{Variable.owner = true;
		 varloc = LocalLoc _ | Undefined  } as v ->
		   v.varloc <- MediumLoc proc*)
	      | {localisation = LocalLoc c} as v  when c <> cont.Continuation.tag ->
		  v.localisation <- MediumLoc proc.ComplexProc.tag
	      |	{localisation = Undefined} as v ->
		  v.localisation <- LocalLoc cont.Continuation.tag
	      | _ -> ()
	    end
	  with
	    Not_found -> ()


	let iter proc cont  (var_hash : (Stamp.t,Variable.t) Hashtbl2.t) 
	    expr =
	  let rec sub_iter e = 
	    match e.descr with
	      No_Exp
	    | Int _ 
	    | Float _ 
	    | String _
	    | Nil
	    | Symbol _
	    | Bool _ -> ()
	    | Var v -> 
		mark_label_var proc cont var_hash v.Symbol.tag
	    | Function(s, l) -> 
		let () = sub_iter  s in
		List.iter sub_iter  l
	    | Binop(op, e1, e2) -> 
		sub_iter e1;
		sub_iter e2
	    | Monop (_,e) -> sub_iter e
	    | ArrayAccess (e1, e2) -> 
		sub_iter e1;
		sub_iter e2
	    | StructAccess (e, _) -> 
		sub_iter e
	    | MessageCall(s,_,None) -> 
		sub_iter s;
	    | MessageCall(s,_,Some(e,l)) ->
		sub_iter s;
		sub_iter e;
		List.iter (fun  (_,e) -> sub_iter  e) l
	    | Address e -> sub_iter  e
	    | StructBuild _
	    | ArrayBuild _  -> assert false
	  in sub_iter expr
      end
		
    open CommandC
    open Continuation
    open Variable

    let  mark_variable (var_hash : (Stamp.t,Variable.t) Hashtbl2.t)  proc = function
	{comlist = comlist;
       } as cont -> 
	 let exp_iter = WorkOnExp.iter proc cont var_hash in
	 let rec iter_com  = function
	   | If (e, tclause, fclause) ->
	       exp_iter  e;
	       iter_com  tclause;
	       iter_com  fclause
	   | Assign(e1,e2) -> 
	       exp_iter  e1;
	       exp_iter  e2
	   | Partial_call(e,l,_) -> 
	       exp_iter  e;
	       List.iter exp_iter  l
	   | Expr e -> exp_iter  e
	   | CheckObject (e,_) -> exp_iter e
	   | Return (Some e) 
	   | SimpleReturn (Some e) -> exp_iter  e
	   | UnregisterMessage lst ->
	       List.iter (WorkOnExp.mark_label_var proc cont var_hash) lst
	   | RegisterMessage (l,id,lpv) ->
	       WorkOnExp.mark_label_var proc cont var_hash id;
	       List.iter
		 (function {source_event = Some e} -> exp_iter e
		   | _ -> ()) l
	   | SetNotVar(v,_) -> WorkOnExp.mark_label_var proc cont var_hash 
		 v.Symbol.tag
	   | Let v -> 
	       WorkOnExp.mark_label_var proc cont var_hash v.Symbol.tag
	   | Set_timer (id, e) -> 
	       exp_iter e; 
	       WorkOnExp.mark_label_var proc cont var_hash id
	   | Reschedule_timer (id, e) -> 
	       exp_iter e;
	       WorkOnExp.mark_label_var proc cont var_hash id
	   | Destroy_Job (id,_) -> 
	       WorkOnExp.mark_label_var proc cont var_hash id
	   | RegisterCont (_,(Delay e)) -> exp_iter  e
	   | RegisterCont _
	   | Return None
	   | SimpleReturn None
	   | Jump _ 
	   | Clean _
	   | Label _
	   | Nop 
	   | Abort
	   | Destroy_Callee -> ()
	   | Seq (c1, c2) -> iter_com c1; iter_com c2
	 in
	 iter_com  comlist


    open ComplexProc

    let manage_var var_hash  = function
	{
	 main_cont = cont;
	 cont_list = [];
       } as proc ->
	 mark_variable var_hash proc cont;
      | {main_cont = cont;
	 cont_list = lst } as proc ->
	   let l = cont::lst in
	   begin
	     List.iter (mark_variable var_hash proc) l;
	   end

    let finish (var_hash : (Stamp.t, Variable.t) Hashtbl2.t ) cont_hash proc_hash = 
      Hashtbl2.iter 
	(fun x -> function
	    {localisation = LocalLoc id;
	   } as v ->
	     let c = Hashtbl2.find cont_hash id in
	     Hashtbl2.add c.local_vars v.Symbol.tag v
	     | {localisation = MediumLoc id} as v -> 
		 let p = Hashtbl2.find  proc_hash id in
		 Hashtbl2.add p.global_vars v.Variable.tag v
	     | {localisation = Undefined;
		name = name} -> 
		 warn (name ^ " is declared but never used.")
	     | _ -> ()
	) var_hash
  end



