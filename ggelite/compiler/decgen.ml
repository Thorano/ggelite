open Ggtree
open Utile

module CommandC = 
  struct

    type wait_type = 
	Delay of GGExp.t
      |	Stand_Wait of string

    let to_wait = function
	GGCommand.Delay x -> Delay x
      |	GGCommand.Stand_Wait s -> Stand_Wait s

    type t = 
      |	If of GGExp.t * t * t
      |	Expr of  GGExp.t
      |	Assign of GGExp.t * GGExp.t
      |	Jump of Stamp.t
      |	Label of Stamp.t
      |	Let of Symbol.t
      |	Set_timer of Stamp.t * GGExp.t 
      |	Reschedule_timer of Stamp.t * GGExp.t 
      |	Partial_call of GGExp.t * GGExp.t list * Stamp.t
      |	Destroy_Callee
      |	Destroy_Job of Stamp.t * (Stamp.t list)
      |	RegisterCont of Stamp.t * wait_type
      |	Return of GGExp.t option
      | SimpleReturn of GGExp.t option
      |	RegisterMessage of  event list * Stamp.t * Stamp.t list
      |	UnregisterMessage of Stamp.t list
      |	SetNotVar of Symbol.t * string
      |	Clean of Stamp.t list
      |	CheckObject of GGExp.t * (GGType.obj_int ref)
      | Seq of t * t
      | Abort
      | Nop

    and event =
	{
	 source_event : GGExp.t option;
	 message_event : string;
	 action_label : Stamp.t;
       } 


    let (@@) c1 c2 = 
      match c1, c2 with
	Nop, c2 -> c2
      | c1, Nop -> c1
      | c1, c2 -> Seq(c1, c2)

    let rec flatten = function
	[] -> Nop
      | a :: [] -> a
      | a :: l ->  a @@ (flatten l)

  end

module Continuation =
  struct

    open CommandC
      
    type t =
	{
	 mutable comlist :  CommandC.t  ;
	 arg : Symbol.t list;
	 cont_name : string list;
	 local_vars : (Stamp.t, Symbol.t) Hashtbl.t;
	 tag : Stamp.t;
	 message : bool;
	 rettype : GGType.t option;
       } 

    type top_block = {
	mutable debut : CommandC.t;
	mutable fin :   t ;
	mutable block_list :   t list
      }


    type  blob =
	Atom of CommandC.t
      | Multi of top_block

    let cont = ref 0
	  
    let mk ?arg ?(tag=Stamp.mk ()) ?message  () = 
      begin
	incr cont;
	let name,message = 
	  match message with
	    Some x -> 
	      let x = ["msg_" ^ (string_of_int !cont) ^ x] in
	      x,true
	  | None ->
	      ["cont_" ^ (string_of_int !cont)], false
	in
	let arg = match arg with
	| Some x -> [x]
	| None -> []
	in
	    
	{ comlist = CommandC.Nop ;
	  arg = arg;
	  cont_name = name;
	  local_vars = Hashtbl.create 10;
	  tag = tag;
	  message = message;
	  rettype = None;
	}
      end

    let mk_cps ?arg () =
      begin
	incr cont;
	let bid = Symbol.mk 
	    ~alpha: false 
	    ~lvalue: false
	    ~name: "__obj"  GGType.Id in
	let tag = Stamp.mk () in
	let basename = "partialReturn_" ^ (string_of_int !cont) ^ ":" in
	let arg, name = 
	  match arg with
	  | Some x  -> [bid ; x], [basename; "arg:"]
	  | None -> [bid], [basename] in
	{ comlist = CommandC.Nop ;
	  arg = arg;
	  cont_name = name;
	  local_vars = Hashtbl.create 10;
	  tag = tag;
	  message = true;
	  rettype = None;
	}
      end

    let mk_message ?(tag=Stamp.mk ()) ~name ~arg ~rettype () =
      	{ comlist = CommandC.Nop ;
	  arg = arg;
	  cont_name = name;
	  local_vars = Hashtbl.create 10;
	  tag = tag;
	  message = true;
	  rettype = Some rettype;
	}

    let dump l =
      Printf.eprintf "debut dump\n";
      let  rec dump = function
	| {cont_name = n} :: l ->
	    Printf.eprintf "dump '%s'\n" (String.concat "~" n);
	    dump l
	| [] -> ()
      in
      dump l;
      Printf.eprintf "fin dump\n"

    let add c l = c.comlist <- c.comlist @@ l
    let compare x y = Stamp.compare x.tag y.tag
  end

module ContSet = 
  struct
    include  Set.Make (Continuation)

    let rec add_list l s =
      match l with
	a :: l' -> 
	  add_list  l' (add a s)
      | [] -> s
  end


module Label =
  struct

    type t = 
	{ 
	  mutable labelName : string ;
	  mutable labelCont : Continuation.t option ;
	  mutable labelCorrect : bool;
	  mutable label_is_top : bool;
	  mutable used : bool;
	}

    let tmp = ref 0

    let mk ?(top = false) ?(name="") ?(cont) ?(id=Stamp.mk ())
	?(hash_table : (Stamp.t, t) Hashtbl.t option) () = 
      begin
	incr tmp;
	let lab = { 
	  labelName = "label_" ^ name ^ (string_of_int (!tmp)) ;
	  labelCont = cont;
	  labelCorrect = true;
	  label_is_top = top;
	  used = false;
	} in
	begin
	  match hash_table with
	    Some hash_table -> Hashtbl.add hash_table id lab;
	  | None -> ()
	end;
	(lab,id)
      end

  end



module Loop =
  struct
    type  t = {
	breakLabel :  Stamp.t ;
	continueLabel : Stamp.t;
	loopid : Stamp.t;
      }

    let newLoopData lab1 lab2 = 
      { breakLabel = lab1;
	continueLabel = lab2;
	loopid = Stamp.mk ();
      }
  end

module ComplexProc =
  struct
    type  t = 
	{
	 mutable global_vars : (Stamp.t, Symbol.t) Hashtbl.t;
	 mutable main_cont :  Continuation.t;
	 mutable cont_list :  Continuation.t list;
	 lab_hash : (Stamp.t, Label.t) Hashtbl.t;
	 tag : Stamp.t
       } 

    let mk ?(main_cont = Continuation.mk ()) tag = 
      {
       global_vars = Hashtbl.create 10;
       main_cont = main_cont;
       cont_list = [];
       lab_hash = Hashtbl.create 5;
       tag = tag
     } 

    let get_label x id = Hashtbl.find x.lab_hash id
    let is_label_global x id = 
      let label = get_label x id in
      label.Label.label_is_top
    let get_cont x id = 
      let label = Hashtbl.find x.lab_hash id in
      match label with
	{Label.labelCont = Some c} -> c
      |	_ -> failwith "compiler error"
  end

(*
module Variable =
  struct
    type varloc =
	Undefined
      |	GlobalVar
      |	ComplexExternalFunc
      |	ArgumentLoc
      | MediumLoc of ComplexProc.t
      |	LocalLoc of Continuation.t
      | Function of ComplexProc.t


    type t = 
	{
	 mutable varloc : varloc ;
	 vartype : GGType.t ;
	 varname : string;
	 vartag : Stamp.t;
	 owner : bool
       }

    let tag = ref 0

    let mkvar ?(owner=false) ?(loc= Undefined) name t tag =
      {
       varloc = loc;
       vartype = t;
       varname = name;
       vartag = tag;
       owner = owner;
     } 

    let fromsymbol v = 
      mkvar v.Symbol.name v.Symbol.thetype v.Symbol.tag
      

    let temp_name () =
      incr tag;
      "__tmp_" ^ (string_of_int !tag) 

    let mk_exp ?name  t = 
(*      Stamp.print_stamp ();*)
      let name = match name with 
	Some x ->  x
      |	None -> "tmp" in
      let v = Symbol.mk 
	  ~alpha: true
	  ~lvalue: true ~name t in
(*      Printf.eprintf "mk_exp %s %d\n" n (Stamp.hash id);*)
      (Symbol.to_exp v,  GGCommand.mk  (GGCommand.Let v))

    let build_temp_dec t =
      let v = Symbol.mk
	  ~alpha: true
	  ~lvalue: true 
	  ~name: "tmp"
	  t 
      in
      GGCommand.mk (GGCommand.Let v), v

    let save_exp ?(name="tmp") e =
      let (@@) = GGCommand.(@@) in
      let t = e.GGExp.thetype 
      and loc = e.GGExp.loc in
      let v = Symbol.mk ~alpha: true ~lvalue: true ~name t in
      let var = GGExp.mk ~loc (GGExp.Var v) t 
      in
      (GGCommand.mk ~loc  (GGCommand.Let v)) @@
      (GGCommand.mk  ~loc (GGCommand.Assign (var, e))), var


    let lookup key (env : t StampMap.t)  =
      StampMap.find key env

    let simplify v =  (*FIXME*)
      Symbol.mk ~tag: v.vartag 
	~alpha: false 
	~lvalue: false 
	~name: v.varname v.vartype
  end

*)

module Variable = Symbol

module Operator = 
  struct
      
    open GGCommand.Std
    type myexp = GGExp.t

    type t = 
	{
(*	 op : GGExp.binop;*)
	 thetype : GGType.t;
	 check_args : GGExp.t list -> bool;
	 rewrite : (GGStdpp.localisation -> myexp list ->  myexp *  GGCommand.t) 
       } 

    type arg_transfert = 
	Direct 
      |	Reference 


    module OpMap = Map.Make (
      struct
	type t = GGExp.binop
	let compare = Pervasives.compare
      end
     )



    module Default = 
      struct
	open GGCommand
	open GGType

	let canonize = function
	    Pointer (Objet _ as t)  -> t
	  | t -> t

	let can_assign  e1 e2 =
	  
	  match canonize e1.GGExp.thetype, canonize e2.GGExp.thetype with
	    a,b when is_sub_type a b -> true
	  | (Objet { contents =  Vrai { name = "NSArray" }}),
	      Array({contents = Some _}, t) when GGType.is_object t ->
		true
	  | (Id | Objet _), (Id |Objet _) -> true
	  | (Id | String), (Id | String) -> true
	  | Array ({contents = None} as x,t), Array({contents = Some n},t')
	    when GGType.isEqual t t' -> 
	      x := Some n;
	      true
	  | a,b when GGType.is_string a && GGType.is_string b -> true
	  | _ -> false

	let check_hack = function 
	    [e1; e2] -> can_assign e1 e2 || can_assign e2 e1
	  | _ -> failwith "impossible1985"
	

	let rewrite_bin op t = 
	  let thetype = rettype_of_funtype t in
	  fun loc -> function
	      [x; y] -> GGExp.mk ~loc (GGExp.Binop (op, x, y)) thetype, nop
	    | _ -> failwith "impossible1984"

	let rewrite_mono op t = 
	  let thetype = rettype_of_funtype t in
	  fun loc -> function
	      [x] -> GGExp.mk ~loc (GGExp.Monop (op, x)) thetype, nop
	    | _ -> failwith "impossible1984"

	let rewrite_arg  t = function
	    {GGExp.thetype = t';
	     GGExp.loc = loc
	   } as e ->
	     let tc' = canonize t' in
	     match (GGType.effectiveType t,e.GGExp.descr) with 
	       t,_ when is_sub_type t tc' -> e
	     | Pointer  (Objet _ as t),_ when is_sub_type t tc' -> e
	     | Pointer _ , GGExp.Symbol(s) when s = "NULL" -> e
	     | Pointer u, _ when is_sub_type u t'  ->
		 if GGExp.is_lvalue e then
		   GGExp.mk ~loc (GGExp.Address e) t
		 else
		   GGStdpp.raise_with_loc loc 
		     (Failure "call by reference impossible because expr is not a left value")
	     | _ ->
		 GGStdpp.raise_with_loc loc
		   (Failure 
		      ("type of expr does not match the type of the argument :\"" ^ 
		       (GGType.to_string_debug 0 t ) ^ "\" and \"" ^ 
		       (GGType.to_string_debug 0 t') ^ "\""
		      )
		   )
	let check_apply_args loc p1 p2 l1 l2 =  
	  try
	    List.map2 (fun _a _b -> 
	      let t, e = p1 _a, p2 _b in
	      rewrite_arg t e)
	      l1 l2 
	  with
	    Invalid_argument _ -> failwithloc loc "Wrong number of arguments"

	let check the_type l = 
	  match the_type with
	    Function(_, l1) -> 
	      begin
		try
		  let _l  = check_apply_args dummy_loc snd (fun x -> x)  l1 l in
		  true
		with
		  _ -> false
	      end
	  | _ -> false

		  
      end

    let mk ?check ?rewrite  ?t2 ?tres op t1  = 
      let ty2 =  match t2 with
	Some x -> x
      | None -> t1
      and tyres = match tres with
	Some x -> x
      | None -> t1
      in
      let thetype = GGType.Function(tyres, ["",t1;
					    "",ty2])
      in
      let check = match check with
	Some x -> x
      |	None -> Default.check thetype
      and rewrite = match rewrite with
	Some x -> x
      |	None -> Default.rewrite_bin op thetype
      in
      op,
      {
(*       op = op;*)
       thetype = thetype;
       rewrite = rewrite;
       check_args = check;
     } 


    let mk_mono ?check ?rewrite  ?tres op t1  = 
      let tyres = match tres with
	Some x -> x
      | None -> t1
      in
      let thetype = GGType.Function(tyres, ["",t1])
      in
      let check = match check with
	Some x -> x
      |	None -> Default.check thetype
      and rewrite = match rewrite with
	Some x -> x
      |	None -> Default.rewrite_mono op thetype
      in
      op,
      {
(*       op = op;*)
       thetype = thetype;
       rewrite = rewrite;
       check_args = check;
     } 


    let build_op_rewrite the_op name ?arg2  ?ret (r1,t1) =

      let mk_lvalue tref t l    = function
	  {GGExp.lvalue = true; GGExp.loc = loc } as e -> 
	    (GGExp.mk ~loc (GGExp.Address e) tref,l)
	| {GGExp.loc = loc} as e -> 
	    let v,c = Variable.mk_exp t in
	    (GGExp.mk ~loc (GGExp.Address v) tref,
	    GGCommand.mk ~loc
	       (GGCommand.Assign(v,e)) @@ c @@ l)
      in

      let gen_arg_type r t =
	match r with
	  Direct -> (t, fun l x -> x,l )
	| Reference -> 
	    let tref = GGType.Pointer (t) in
	    (tref, mk_lvalue tref t)
      in

      let pf1,fun1 = gen_arg_type r1 t1 in
      let r2,t2,(pf2,fun2) = 
	match arg2 with
	  Some (r,t) -> r,t, gen_arg_type r t
	| None -> r1,t1,(pf1,fun1)
      and rr,tr =
	match ret with
	  Some (r,x) -> r,x
	| None -> r1,t1 
      in
      let rewrite_function = 
	match rr with
	  Reference -> 
	    let ppfr = GGType.Pointer (tr) in
	    let pfr = ppfr  in

	    let t = GGType.Function( GGType.Unit, ["", pf1;
						   "", pf2;
						   "", pfr]) in
	    let v = GGExp.mk (GGExp.Symbol(name)) t  in
	    let rewrite_function loc = function 
		[_e1; _e2] ->
		  let pe1,l1 = fun1 nop  _e1 in
		  let pe2,l2 = fun2 l1  _e2 in
		  
		  let res,c = Variable.mk_exp tr in
		  let pres = GGExp.mk ~loc (GGExp.Address res) ppfr in

		  res, l2 @@ c @@ (GGCommand.mk ~loc 
				     (GGCommand.Expr(
				      GGExp.mk (GGExp.Function(v,[pe1;pe2;pres]))
					GGType.Unit)))
	      | _ -> failwith "impossible1985"

	    in
	    rewrite_function
	| Direct -> 
	    let t = GGType.Function(t1, ["",pf1 ; "", pf2]) in
	    let v = GGExp.mk (GGExp.Symbol(name)) t  in
	    let rewrite_function loc = function 
		[_e1; _e2] ->
		  let pe1,l1 = fun1 nop  _e1 in
		  let pe2,l2 = fun2 l1  _e2 in
		  let e = GGExp.mk ~loc (GGExp.Function(v, [pe1;pe2])) tr in
		  e, l2
	      | _ -> failwith "impossible1985"

	    in
	    rewrite_function
      in
      mk ~rewrite: rewrite_function ~t2 ~tres:tr the_op t1
  end
    


