{
   open Lexing
    open Ggparser
    open Utile
    
    let mk_loc (lexer:lexbuf) = 
        { 
            GGStdpp.start_pos = lexeme_start_p lexer;
            GGStdpp.end_pos = lexeme_end_p lexer;
        }

            
    let mk_ident = 
        let table_type = Hashtbl.create 10 in 
        let () = List.iter (fun (a,b) -> Hashtbl.add table_type a b)
        ["type", TYPE;
         "function", FUNCTION;
         "inline", INLINE;
         "import", IMPORT;
         "extern", EXTERN;
         "let", EXTERN;
         "const", CONST;
         "complex", COMPLEX;
         
         "Float", FLOAT;
         "Bool", BOOL;
         "Int", INT;
         "Id", ID;
         "struct", STRUCT;
         "enum", ENUM;
       ] 
       in 
       fun s  ->
        try Hashtbl.find table_type s with
		  Not_found -> IDENT(s)

} 

let ident = ['a'-'z''A'-'Z''_']['a'-'z''A'-'Z''0''1'-'9''_']*
let integer = ['0''1'-'9']+
let float_s = (integer '.' integer?) | ('.' integer)
let float_e = (integer | float_s) 'e' ['+' '-']? integer
let float = float_s | float_e

let blank1 = [' ' '\t']+
let blank2 = '\n' | ("\n#" [^ '\n']*)
let blank = blank1 | blank2
   

rule  entry = parse
   blank              {entry lexbuf}
   | ident            {mk_ident (lexeme lexbuf)}
   | integer          {INTEGER_CST (int_of_string (lexeme lexbuf))}
   | float            {FLOAT_CST(float_of_string (lexeme lexbuf))}
   | '('            {LPAREN}
   | ')'            {RPAREN}
   | '{'            {LBRACE}
   | '}'            {RBRACE}
   | '<'            {LANGLE}
   | '>'            {RANGLE}
   | '['            {RBOX}
   | ']'            {LBOX}
   | eof              {EOI}
   | "/*"             {comment lexbuf; entry lexbuf}
   | "//"[^'\n']*     {entry lexbuf}
   | '"'[^'"']*'"'    {STRING_CST(lexeme lexbuf)}

and comment = parse
       "/*"           {comment lexbuf; comment lexbuf}
   | "*/"             {}
   | _                {comment lexbuf}
   | eof              {failwith "end of file in a comment"}

{


let rec myentry blu = 
  let s = entry blu in
  Utile.GGStdpp.loc_ref := mk_loc blu;
  s
} 
