/*top level stuff */
%{
    open Utile
	open ProtoTree
    open Compile
    
	let ivars_from_list l = 
		let rec parse_instance_var private_ivars public_ivars flag = function
			  `Public :: l -> parse_instance_var private_ivars  public_ivars true l
			| `Private :: l -> parse_instance_var private_ivars  public_ivars false l
			| `Instance_variable v :: l -> 
			if flag then
			  parse_instance_var private_ivars (v :: public_ivars) flag l
			else
			  parse_instance_var (v::private_ivars) public_ivars flag l
			| [] -> ( private_ivars, public_ivars)
		in parse_instance_var [] [] false l
        
    let make_c_dec loc ((opt_name:string option), (t:GGType.t)) =
      match opt_name with
        Some name -> GGVarDecl.make_var_dec name t loc 
      | None -> GGStdpp.raise_with_loc loc (Failure "expect name there")
      
    let make_loc (position:int) = 
        GGStdpp.mk (Parsing.rhs_start_pos position) (Parsing.rhs_end_pos position)

%}

%token EOI

/*keyword*/
%token  TYPEDEF
%token  FUNCTION
%token  <string> INLINE
%token  IMPORT
%token  EXTERN
%token  LET
%token  CONST
%token  COMPLEX
%token CDECMOD STATIC
%token CLASS PUBLIC PRIVATE
%token BEGINDEC
%token ENDDEC

/* type element */
%token  FLOAT
%token  INT UNSIGNED
%token  BOOL
%token  STRING
%token  UNIT
%token  ID
%token  ENUM
%token  TABULAR
%token  STRUCT

/* parenthesis and such */
%token  LPAREN RPAREN
%token  LBRACE RBRACE
%token  LANGLE RANGLE
%token  RBOX LBOX
%token  ASSIGN
%token SEMICOLON
%token BEGININTERFACE
%token ENDINTERFACE

/* other */
%token EQUAL
%token COLON SEMICOLON COMA DOT CDOT EXCLAM PIPE QUOTE 
%token AMPERSAND DEREF
%token TIMES PLUS MINUS DIV

/* atomic element */
%token <int> INTEGER_CST
%token <bool> BOOLEAN_CST
%token <string> STRING_CST
%token <float> FLOAT_CST
%token <string> IDENT
%start main             /* the entry point */
%type <ProtoTree.GGProg.t list> main
%type <GGType.t -> string option * GGType.t>c_name

%left TIMES
%left RBOX LBOX RPAREN LPAREN

%%

main: 
    declaration_list EOI       { List.flatten (List.rev $1)  }
;

declaration_list:
    {[]}
  | declaration_list declaration { $2 :: $1 }
;

declaration:
 c_decl {List.map (fun x -> GGProg.Extern (ExternVariable(make_dec x))) $1}
 
 | INLINE {  Parse.parse_string $1 }
 
 | TYPEDEF c_decl  { List.map (fun x -> GGProg.DecType x) $2}
 | BEGININTERFACE objc_interface ENDINTERFACE {[GGProg.ClassInterface ($2, make_loc 2)] }
 | CLASS ident_list SEMICOLON {let loc = make_loc 2 in List.map (fun s -> GGProg.ClassForward (s, loc))$2 }
;

cdecmod:
 {}
| cdecmod CDECMOD {}
;


objc_interface:
 IDENT opt_inherit opt_protocol opt_ivars method_lists {
   let (priv_ivars, public_ivars) = $4 
   and (instance_methods, class_methods) = $5 in 
   GGInterface.mk $1 $2 $3 public_ivars priv_ivars instance_methods class_methods
 }
;

opt_inherit:
  COLON IDENT { Some $2 }
  | { None }
;

opt_protocol:
  { [] }
  | LANGLE ident_list RANGLE { [GGInterface.Protocol $2] }
  | LPAREN IDENT RPAREN { [GGInterface.Category $2] }
;

opt_protocol2:
 {[ ] }
 | LANGLE ident_list RANGLE { [GGInterface.Protocol $2 ] }
;

ident_list:
  IDENT { [$1] }
  | IDENT COMA ident_list { $1 :: $3 }
;

opt_ivars:
 { [], [] }
| LBRACE ivar_list RBRACE { ivars_from_list (List.flatten $2) }
;

ivar_list:
 { [] }
| ivar ivar_list { $1 :: $2 }
;

ivar:
 PUBLIC { [`Public] }
 | PRIVATE { [`Private] }
 | c_decl { List.map (fun x -> `Instance_variable x) $1 }
;

method_lists:
{[],[] } 
| a_method method_lists { 
	let (ims, cms) = $2 in
	match $1 with
	  `Class m -> (ims, m :: cms)
	 | `Instance m -> (m::ims, cms)
  }
;

a_method:
PLUS a_method_sig SEMICOLON{`Class $2}
| MINUS a_method_sig SEMICOLON{`Instance $2}
;

a_method_sig:
type_ret_opt IDENT extra_args_opt  {
  {
    GGInterface.method_ret_type = $1;
    GGInterface.first_component = $2;
    GGInterface.next_components = $3;
 }
}
;

type_ret_opt:
  {GGType.Id}
| LPAREN c_type RPAREN {$2}
;

extra_args_opt:
 {None}
| COLON type_ret_opt IDENT extra_args_list supplementary_args { Some ($2, $4) }
;

extra_args_list:
 {[]}
 | IDENT COLON type_ret_opt IDENT extra_args_list { ($1,$3) :: $5 }
;

supplementary_args:
 {}
 | COMA  CDOT {}
;

/* approximation of ctype with a simple dec */
c_type:
cdecmod c_basic_type c_name { match $3 $2 with 
   None, t -> t | _ -> GGStdpp.raise_with_loc (make_loc 2)  (Failure "non expected  name in ctype") }
;

/* FIXME cdecmod should not be used in every context 
   FIXME orphan type (that is a type without a name) are allowed, for instance:
   
    enum __foo{Bla};
    
    for the moment, this does not match anything in our abstract syntax
   
   */
c_decl:
cdecmod c_basic_type c_half_dec_list SEMICOLON { List.map (fun f -> make_c_dec (make_loc 2) (f $2)) $3 }
;


c_half_dec_list:
 c_half_dec { [$1] }
 | c_half_dec COMA c_half_dec_list { $1 :: $3 }
;

c_half_dec:
   c_name opt_size opt_initializer  { 
    let fn:(GGType.t -> string option * GGType.t) = $1 in 
    fn}
;

opt_size:
 {}
 | COLON INTEGER_CST {}
 ;
 
opt_initializer:
 {}
 | EQUAL number {}
;

number:
 IDENT {}
 | INTEGER_CST {}
 | FLOAT_CST {}
;
 
c_basic_type:
 UNIT  {GGType.Unit}
 | INT  {GGType.Integer}
 | UNSIGNED INT {GGType.Integer}
 | UNSIGNED {GGType.Integer}
 | BOOL {GGType.Bool}
 | FLOAT {GGType.Float}
 | ID opt_protocol2 {GGType.Id}
 | IDENT opt_protocol2 {GGType.Name ($1, make_loc 1)}
 | struct_dec { $1 }
 | enum_dec {$1}
;

struct_dec:
 STRUCT opt_ident LBRACE c_struct_decl_list RBRACE {GGType.Struct (List.map (fun dec -> dec.GGVarDecl.descr) (List.flatten $4)) }  
 | STRUCT IDENT  { GGType.Abstract ($2, make_loc 2) }
;

c_struct_decl_list:
{[]}
| c_decl c_struct_decl_list { $1 :: $2 }
;

opt_ident:
 { None }
 |  IDENT { Some $1 }
;

enum_dec:
ENUM opt_ident LBRACE enum_item_list RBRACE { GGType.Enum $4 }
| ENUM IDENT { GGType.Abstract($2, make_loc 2) }
;

enum_item_list:
 { [] }
 | enum_item_list_non_empty { $1 }
;

enum_item_list_non_empty:
  enum_item { [$1] }
  | enum_item COMA { [$1] }
  | enum_item COMA enum_item_list_non_empty      { $1 :: $3 }
;

enum_item:
IDENT opt_enum_value { $1 }
;

opt_enum_value:
 {}
 | EQUAL dimension  {}
;

dimension:
 INTEGER_CST { Some $1 }
 | IDENT { None }
 | {None}
;

c_name:
  { fun x -> None, x }
| non_empty_c_name LPAREN func_args_list RPAREN {
    let args = match $3 with
        [ GGType.Unit ] -> []
        | l -> l
    in 
    fun x -> $1 (GGType.Function (args, x)) }
| c_name RBOX dimension LBOX { fun x -> $1 (GGType.Array ($3, x)) }
| non_empty_c_name { $1 }
;

non_empty_c_name:
 IDENT {fun x -> Some $1, x}
| LPAREN c_name RPAREN { $2 } 
| TIMES c_name { fun x -> $2 (GGType.Pointer x) }
;

func_args_list:
 {[]}
 | c_basic_type c_name supplementary_args { let _,t = $2 $1 in [t] }
 | c_basic_type c_name COMA func_args_list { let _,t = $2 $1 in t :: $4 }
;