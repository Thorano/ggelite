module type TokenSig = 
sig
type token =
  | EOI
  | TYPEDEF
  | FUNCTION
  | INLINE of (string)
  | IMPORT
  | EXTERN
  | LET
  | CONST
  | COMPLEX
  | CDECMOD
  | STATIC
  | CLASS
  | PUBLIC
  | PRIVATE
  | BEGINDEC
  | ENDDEC
  | FLOAT
  | INT
  | UNSIGNED
  | BOOL
  | STRING
  | UNIT
  | ID
  | ENUM
  | TABULAR
  | STRUCT
  | LPAREN
  | RPAREN
  | LBRACE
  | RBRACE
  | LANGLE
  | RANGLE
  | RBOX
  | LBOX
  | ASSIGN
  | SEMICOLON
  | BEGININTERFACE
  | ENDINTERFACE
  | EQUAL
  | COLON
  | COMA
  | DOT
  | CDOT
  | EXCLAM
  | PIPE
  | QUOTE
  | AMPERSAND
  | DEREF
  | TIMES
  | PLUS
  | MINUS
  | DIV
  | INTEGER_CST of (int)
  | BOOLEAN_CST of (bool)
  | STRING_CST of (string)
  | FLOAT_CST of (float)
  | IDENT of (string)

val main :
  (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> ProtoTree.GGProg.t list

end