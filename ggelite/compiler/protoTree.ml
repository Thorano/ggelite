open Utile

module GGType = 
  
  struct
    
    type t = 
	Unit
      |	Id
      |	Bool
      | Integer 
      |	String
      | Float
      |	Array of (int option)*t
      |	Struct of (string*t)list
      |	Name of string * GGStdpp.localisation
      |	Enum of string list
      |	Pointer of t
      | Function of (t list) * t
      | Abstract of string * GGStdpp.localisation
  end


module GGExp = 
  struct
    type binop = 
	Plus 
      |	Prod 
      |	ProdVect
      | Minus 
      | Div 
      | Equal 
      |	Notequal
      | Lesser 
      | LesserEq 
      | GreaterEq 
      | Greater
      |	And
      |	Or
      |	Not

    type   t = { descr :   exp; loc : GGStdpp.localisation  }
    and   exp = 
      	Int of int
      | Float of float
      |	Bool of bool
      |	Nil
      |	String of string
      |	Symbol of string * GGType.t
      |	Variable of string
      | Function of  t *( t list)
      |	Binop of binop *  t *  t
      |	Monop of binop * t
      |	ArrayAccess of t *  t
      |	StructAccess of  t * string
      | StructBuild of (string * t) list
      |	ArrayBuild of t list
      | Selector of string * (string list option)
      |	MessageCall of t * string * 
	    ((t * ((string *  t) list)) option)

    let mk loc descr   = {
      descr = descr ;
      loc = loc ;
    }

    let binop_expr op x y loc = 
      mk loc (Binop(op, x, y))

    let monop_expr op x loc =
      mk loc (Monop (op, x))
      
    let full_name_from_message main_name = function
       Some (_, l) ->
          let rec onList res = function 
             (n,_) :: l -> onList ( res ^ n ^ ":" ) l
             | [] -> res
          in
          onList (main_name ^ ":") l
        | None -> main_name


  end


module GGTypeDecl =
  struct
    type t = {descr : typedecl; loc : GGStdpp.localisation}
    and typedecl = string*GGType.t


    let mk ?(loc=GGStdpp.dummy_loc) descr = {
      descr = descr ;
      loc = loc }


    let make_var_dec s t loc=
      {
       descr =  s,t;
       loc = loc}
       
    let getType x = snd x.descr
  end

module GGVarDecl = GGTypeDecl

module GGCommand =
  struct	  
    type wait_type =
      | Delay of GGExp.t
      |	Stand_Wait of string

    type   t = {descr :    command ; loc : GGStdpp.localisation }
    and command = 
	If of GGExp.t * t list * t list
      |	While of GGExp.t * t list
      |	Break
      |	Continue
      | Block of t list
      | Resume
      |	Answer of GGExp.t
      |	Expr of  GGExp.t
      |	Assign of GGExp.t * GGExp.t
      |	Set_timer of (string option) * GGExp.t * string
      |	Reschedule_timer of GGExp.t * string
      |	Wait of wait_type
      |	Let of (string * GGType.t * (GGExp.t option) * bool) list
      |	Return of GGExp.t option
      |	Listen of listen_block
      |	Match of (GGExp.t * ((GGExp.t * t list * GGStdpp.localisation) list) * 
		    (t list))
      | Detach of string * (GGExp.t list) * (string option)
      | RegisterCont of GGExp.t * string
    and event =
	{
	 source_event :  GGExp.t option;
	 cond_event : GGExp.t  option;
	 message_event : string;
	 args_event : (string * GGType.t * bool * GGStdpp.localisation) list;
	 com_event : t list;
       } 
    and private_msg = 
	{
	 priv_msg_name : string;
	 priv_com_list : t list;
	 priv_args : (string * GGType.t * GGStdpp.localisation) list
       } 
    and public_msg =
	{
	 pub_msg_name : string;
	 pub_msg_args : ((GGType.t * string*GGStdpp.localisation) * (string * (GGType.t*string*GGStdpp.localisation)) 
			    list) option;
	 pub_msg_com : t list;
       } 
    and  listen_block =
	{
	 listen_com : t list;
	 event_list :  event list;
	 timer_list : private_msg list;
	 msg_list : public_msg list;
       } 

    let mk loc  descr = {
      descr = descr ;
      loc = loc }


  end


type arg_dec_type = string * bool * GGType.t * GGStdpp.localisation

type function_decl_type = {
    name_fun : string;
    args_fun : arg_dec_type list;
    rettype_fun :   GGType.t;
    loc_fun : GGStdpp.localisation
  } 

module GGFonction =
  struct 
    type  t = 
	{
	 loc : GGStdpp.localisation;
	 f : function_decl_type;
	 command :  GGCommand.t list ;
       }
    let mk loc f coms = 
      {
       loc = loc;
       f  = f;
       command = coms;
     } 
  end
  
module GGInterface =
struct
    
    type 'a typed_method = {
    method_ret_type : 'a;
    first_component : string;
    next_components : ('a * ((string*'a) list)) option;
  }
  
   type a_method = GGType.t typed_method

   type t = { 
        inherit_from :  string option;
        name : string;
        protocols : protocol list;
        public_ivars: objc_variable list;
        private_ivars: objc_variable list;
        class_methods : a_method list;
        instance_methods : a_method list;
   }
   and  protocol = 
     Protocol of (string list)
     | Category of string
  and objc_variable = GGVarDecl.t
  
  let mk name inherit_from_opt protocols ivars priv_ivars  class_methods instance_methods =
    {
        inherit_from = inherit_from_opt;
        name = name;
        protocols = protocols;
        public_ivars = ivars;
        private_ivars = priv_ivars;
        class_methods = class_methods;
        instance_methods = instance_methods;
    }
end


type extern_function_t =
    {
     complex_ef : bool;
     f_decl_ef : function_decl_type;
     external_name_ef : string option;
   } 

type extern_variable_t =
    {
     name_vt : string;
     type_vt : GGType.t;
     external_name_vt : string option;
   } 

let  make_dec   = function 
    { GGVarDecl.descr = name,t } -> {name_vt = name; type_vt = t;external_name_vt = None}

type extern_t = 
    ExternFunction of extern_function_t
  | ExternVariable of extern_variable_t

module GGProg =
  struct
    type  t = 
	DecType of GGTypeDecl.t
      |	Function of GGFonction.t
      | ClassForward of (string*GGStdpp.localisation)
      | ClassInterface of (GGInterface.t*GGStdpp.localisation)
      |	Inline of (function_decl_type * GGExp.t * GGStdpp.localisation)
      |	Extern of extern_t
      |	Import of string * GGStdpp.localisation
      |	Const of string * GGType.t * GGExp.t
      | LetGlobal of string * GGType.t * GGExp.t
    
    open Log
    let dump = function
	DecType {GGTypeDecl.descr = (name,_)} -> printf "type dec %s" name
      |	Function f-> printf "function %s" f.GGFonction.f.name_fun;
      | ClassForward (name, _) -> printf "class forward of %s" name
      | ClassInterface ({GGInterface.name = name},_) -> printf "interface %s" name
      |	Inline _ -> printf "inline"
      |	Extern _ -> printf "extern"
      |	Import (name,_) -> printf "import %s" name
      |	Const (name, _, _) -> printf "const %s" name
      | LetGlobal (name, _, _) -> printf "let %s" name
      
  end

