#!/usr/bin/perl -w

use strict;
use POSIX;
#use Getopt::Std;


#getopts('f:i:nhv');

my $mode=0;
my (@couleur, @face, @vertex);

my $monfile = "";
my $i;
my %hash;

$hash{'scale'} = 1;
$hash{'scaleX'} = 1;
$hash{'scaleY'} = 1;
$hash{'scaleZ'} = 1;
$hash{'X'} = 'X';
$hash{'Y'} = 'Y';
$hash{'Z'} = 'Z';

for $i (@ARGV)
{
    if( $i =~ /(.*)=(.*)/ )
    {
	$hash{$1} = $2;
    }
    else
    {
	$monfile = $i;
    }
}

open(FILE, $monfile) or die("can't open $monfile\n");
unlink("/tmp/titi");
open(OUT, ">/tmp/titi") or die("can't open tempory work file");

#die("Nihil ex nihilo\n");
while(<FILE>)
{
    my $out = $_;
    if( $mode == 0 )
    {
	if( /Vertex list/ )
	{
	    $mode = 1;
	}
    }
    elsif ($mode == 1)
    {
	if( /^Vertex\s*(\d*):\s*X:([-0-9.]+)\s*Y:([-0-9.]+)\s*Z:([-0-9.]+)/ )
	{
	    my %val;
	    my $nvert;
	    ($nvert, $val{X}, $val{Y}, $val{Z}) = ($1, $2, $3, $4);
	    my $A = $val{$hash{'X'}}*$hash{'scale'}*$hash{'scaleX'};
	    my $B = $val{$hash{'Y'}}*$hash{'scale'}*$hash{'scaleY'};
	    my $C = $val{$hash{'Z'}}*$hash{'scale'}*$hash{'scaleZ'};
	    $out =  "Vertex $nvert:  X:$A     Y:$B     Z:$C\n";
	}
	elsif( /Face list/ )
	{
	    $mode = 0;
	}
	else
	{
	    die "fuck parse error $_\n";
	}
    }
    print OUT $out;
}

close(OUT);

#link ("/tmp/titi", $monfile) or die ("can't rename");
system("cp $monfile $monfile.old");
unlink($monfile) or die("can't unlink $monfile");
system("cp -f /tmp/titi $monfile");
unlink("/tmp/titi");
