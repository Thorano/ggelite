#!/usr/local/bin/perl -w

use strict;
use POSIX;

my (@fl, @sl, @tl);
my $i;
my $count = 1;

while(<>)
{
    my @a = split /\t/;
    push @fl, [@a[0..2]];
    push  @sl, [@a[3..5]] if $#a >= 4;
    push @tl,  [@a[6..8]] if $#a >= 7;

#    print $#a."\n";


#    print "@first\n";
}

my @res = (@fl, @sl, @tl);

foreach $i (@res)
{
    my @lst = @{$i};
    &printBlah(@lst);
}

sub parseVal
{
    my $val = $_[0];
    my $res = 0;

    if( $val =~ /(\d*)/ )
    {
	$res = $1;
    }
    if( $val =~/k/ )
    {
	$res *= 1000;
    }
    if( $val =~ /-/ )
    {
	$res = 0;
    }
    $res;
}


sub printBlah
{
    map chomp, @_;
    my ($a, $b, $c) = @_;
    $b = &parseVal($b);
    $c = &parseVal($c);
    print "\t{$count, \@\"$a\", $b, $c},\n";
    $count++;
}
