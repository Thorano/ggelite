#!/usr/bin/perl

use strict;
use POSIX;

my $mode=0;
my (@couleur, @face, @vertex);

while(<>)
{
    if( $mode == 0 )
    {
	if( /color *\[/ )
	{
	    $mode = 1;
	}
    }
    elsif ($mode == 1)
    {
	if( /^\s*([0-9.]+),\s*([0-9.]+)\s*,\s*([0-9.]+)/ )
	{
	    push @couleur, [ $1, $2, $3 ];
	}
	elsif( /\]/ )
	{
	    $mode = 2;
	}
	else
	{
	    die "parse error";
	}
    }
    elsif( $mode == 2 )
    {
	if( /coordIndex\s*\[/  )
	{
	    $mode = 3;
	}
    }
    elsif ($mode == 3)
    {
	if( /\]/ )
	{
	    $mode = 4;
	}
	else
	{
	    push @face, [&parseFace($_)];
	}
    }
    elsif( $mode == 4 )
    {
	if( /\[/ )
	{
	    $mode = 5;
	}
    }
    elsif ($mode == 5 )
    {
	if( /^\s*([-0-9.]+)\s*,\s*([-0-9.]+)\s*,\s*([-0-9.]+)/ )
	{
	    push @vertex, [ $1, $2, $3];
	}
	elsif( /\]/ )
	{
	    $mode = 6;
	}
	else
	{
	    print $_;
	    die "parse error!!";
	}
    }
}

my $i;


print STDERR "$#couleur $#face $#vertex\n";

my $tp = $#vertex+1;
my $tp2 = $#face+1;


print <<"FIN";
Named object: "NoName1"
Tri-mesh, Vertices: $tp    Faces: $tp2
Vertex list:
FIN

for( $i = 0; $i <= $#vertex; ++$i)
{
    print "Vertex $i:  X:$vertex[$i][0]    Y:$vertex[$i][1]  Z:$vertex[$i][2]\n";
}

print "Face list:\n";

for( $i = 0; $i <= $#face; ++$i)
{
    print "Face $i:    A:$face[$i][0] B:$face[$i][2] C:$face[$i][1] ".
	" AB:1 BC:1 CA:1\n";
    print "Material: ";
    &printCouleur(@{$couleur[$i]});
    print "\n";
}

sub parseFace
{
    my ($arg) = @_;
    my @ret = ();

    my $cont = 1;

    while( $cont )
    {
	if( $arg =~ /^,?\s*([0-9-]+),?(.*)/ )
	{
	    push @ret, $1;
	    $arg = $2;
	}
	else
	{
	    $cont = 0;
	}
    }
    @ret;
}

sub normalise
{
    my ($arg) = @_;
    ceil($arg*255);
}

sub printCouleur
{
    my ($r, $g, $b) = map { &normalise($_) ;} @_;
    print "\"r".$r."g".$g."b".$b."a0\"";
}
