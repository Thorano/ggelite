#include <lib3ds/file.h>
#include <lib3ds/chunk.h>
#include <lib3ds/node.h>
#include <lib3ds/mesh.h>
#include <lib3ds/vector.h>
#include <lib3ds/quat.h>
#include <lib3ds/matrix.h>
#include <lib3ds/material.h>
#include <lib3ds/camera.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

Lib3dsFile *file;
FILE *out;

void
outPutNode(Lib3dsNode *node)
{
  Lib3dsNode *p;
  for (p=node->childs; p!=0; p=p->next) {
    outPutNode(p);
  }

  if (node->type==LIB3DS_OBJECT_NODE) 
    {
      unsigned p;
      Lib3dsMesh *mesh;
      Lib3dsMatrix invMeshMatrix;
      if (strcmp(node->name,"$$$DUMMY")==0) 
	{
	  return;
	}

      mesh = lib3ds_file_mesh_by_name(file, node->name);
      ASSERT(mesh);

      lib3ds_matrix_copy(invMeshMatrix, mesh->matrix);
      lib3ds_matrix_inv(invMeshMatrix);

      fprintf(out, "Named object: \"%s\"\nTri-mesh, Vertices: %ld"
	      "   Faces: %ld\n", node->name, 
	      mesh->points, mesh->faces);
      
      fprintf(out, "Vertex list:\n");

      for( p = 0; p < mesh->points; ++p)
	{
          Lib3dsVector v;
	  lib3ds_vector_transform(v, invMeshMatrix, 
				  mesh->pointL[p].pos);	  
	  fprintf(out, "Vertex %d: X:%g Y:%g Z:%g\n", 
		  p, v[0], v[1], v[2]);
		  
	}

      fprintf(out, "Face List:\n");

      for (p=0; p<mesh->faces; ++p) 
	{
	  Lib3dsFace f=mesh->faceL[p];
	  Lib3dsMaterial *mat=0;
	  fprintf(out, "Face %d:	A:%d B:%d C:%d AB:1 BC:1 CA:1\n", p,
		  f.points[0], f.points[1], f.points[2]);
	  if (f.material[0]) {
	    mat=lib3ds_file_material_by_name(file, f.material);
	    fprintf(out, "Material:  \"%s\"\n", f.material);
	  }
	  /*          if (mat) { */
	  /*            s[0]=1.0; */
	  /*            glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient); */
	  /*            glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse); */
	  /*            glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular); */
	  /*            glMaterialf(GL_FRONT, GL_SHININESS, 11.0-0.1*mat->shininess); */
	  /*          } */
	  /*          else { */
	  /*            Lib3dsRgba a={0.2, 0.2, 0.2, 1.0}; */
	  /*            Lib3dsRgba d={0.8, 0.8, 0.8, 1.0}; */
	  /*            Lib3dsRgba s={0.0, 0.0, 0.0, 1.0}; */
	  /*            glMaterialfv(GL_FRONT, GL_AMBIENT, a); */
	  /*            glMaterialfv(GL_FRONT, GL_DIFFUSE, d); */
	  /*            glMaterialfv(GL_FRONT, GL_SPECULAR, s); */
	  /*            glMaterialf(GL_FRONT, GL_SHININESS, 0.0); */
	  /*          } */
	}
      fprintf(out, "\n");

    }
}


void
dump()
{
  Lib3dsNode *p;
  for (p=file->nodes; p!=0; p=p->next)
    {
      outPutNode(p);
    }
}

int main(int argc, char **argv)
{
  char *filename;
  if( argc != 2)
    {
      fprintf(stderr, "no filename found\n");
      return 1;
    }

  filename = argv[1];

  file=lib3ds_open(filename);
  if (!file) {
    fprintf(stderr, "***ERROR***\nLoading file %s failed\n", filename);
    exit(1);
  }

  fprintf(stderr, "parse is succesful\n");
  out = stdout;
  dump();
  
  lib3ds_close(file);
  return(0);
}
