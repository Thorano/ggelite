#!/bin/sh


DATE=`date "+%Y-%m-%d"`
DIRE=elite-$DATE
GGP=`pwd`/../compiler/ggp

mkdir /tmp/$DIRE
cd ..
cp -R CVS /tmp/$DIRE
cd /tmp/$DIRE/  || exit
cvs -z 4 update -AdP
echo cleaning some stufs $GGP
rm -rf `find . -name CVS`
rm -rf outils

cd Source/scripts
make objcfiles GG=$GGP
cd /tmp

echo archiving in $DIRE.tar
#tar cvjf $DIRE.tar.bz2 $DIRE
tar cf $DIRE.tar $DIRE

ncftpput -f $HOME/.free  /snap/ $DIRE.tar.bz2
