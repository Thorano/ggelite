//
//  mainLoop.m
//  elite
//
//  Created by Fr�d�ric De Jaeger on Mon Jun 07 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "unistd.h"
#import "GGRunTime.h"
#import "GGMainLoop.h"
#import "mainLoop.h"
#import "farouk.h"


BOOL theEnd = NO;

@interface Monitor : NSObject
{
    
}
@end

@implementation Monitor
- (void) endContract: c
{
    NSLog(@"this is the end for %@", c);
    theEnd = YES;
}
@end

int main(int argc, char *argv[])
{
    NSAutoreleasePool*          pool;
    pool = [NSAutoreleasePool new];
    GGMainLoop *loop = [GGMainLoop new: argc
                               arg: argv];
    [loop beforeRunningLoop];

    Monitor* monitor = [Monitor new];
    Complex_principal *farouk = [Complex_principal principal_];
    [farouk  registerContinuation: monitor
                               at: @selector(endContract:)];

    
    while(!theEnd){
        NSAutoreleasePool *pool2;
        pool2 = [NSAutoreleasePool new];
        [loop runTimers];
        
        [pool2 release];
        usleep(100000);
    }
    [monitor release];
    [pool release];
    NSLog(@"fini");
    return 0;
}

void displayOnGGEliteConsole(NSString* format, ...)
{
    va_list ap;
    
    va_start (ap, format);
    
    NSString*str = [[NSString alloc] initWithFormat: format
                                          arguments: ap];
    
    printf("%s\n", [str UTF8String]);
    fflush(stdout);
    [str release];
    va_end (ap);
}
